PGDMP         7                 x         	   beone_byi    11.6    11.6 h   g           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false            h           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false            i           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                       false            j           1262    16789 	   beone_byi    DATABASE     g   CREATE DATABASE beone_byi WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'C' LC_CTYPE = 'C';
    DROP DATABASE beone_byi;
             postgres    false            �            1259    16790    beone_adjustment    TABLE     =  CREATE TABLE public.beone_adjustment (
    adjustment_id bigint NOT NULL,
    adjustment_no character varying(20),
    adjustment_date date,
    keterangan character varying(100),
    item_id integer,
    qty_adjustment bigint,
    posisi_qty integer,
    update_by integer,
    update_date date,
    flag integer
);
 $   DROP TABLE public.beone_adjustment;
       public         postgres    false            �            1259    16793 "   beone_adjustment_adjustment_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_adjustment_adjustment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 9   DROP SEQUENCE public.beone_adjustment_adjustment_id_seq;
       public       postgres    false    196            k           0    0 "   beone_adjustment_adjustment_id_seq    SEQUENCE OWNED BY     i   ALTER SEQUENCE public.beone_adjustment_adjustment_id_seq OWNED BY public.beone_adjustment.adjustment_id;
            public       postgres    false    197            �            1259    16795 	   beone_coa    TABLE     S  CREATE TABLE public.beone_coa (
    coa_id integer NOT NULL,
    nama character varying(50),
    nomor character varying(50),
    tipe_akun integer,
    debet_valas double precision,
    debet_idr double precision,
    kredit_valas double precision,
    kredit_idr double precision,
    dk character varying,
    tipe_transaksi integer
);
    DROP TABLE public.beone_coa;
       public         postgres    false            �            1259    16801    beone_coa_coa_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_coa_coa_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.beone_coa_coa_id_seq;
       public       postgres    false    198            l           0    0    beone_coa_coa_id_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE public.beone_coa_coa_id_seq OWNED BY public.beone_coa.coa_id;
            public       postgres    false    199            �            1259    16803    beone_coa_jurnal    TABLE     �   CREATE TABLE public.beone_coa_jurnal (
    coa_jurnal_id integer NOT NULL,
    nama_coa_jurnal character varying(100),
    keterangan_coa_jurnal character varying(1000),
    coa_id integer,
    coa_no character varying(50)
);
 $   DROP TABLE public.beone_coa_jurnal;
       public         postgres    false            �            1259    16809 "   beone_coa_jurnal_coa_jurnal_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_coa_jurnal_coa_jurnal_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 9   DROP SEQUENCE public.beone_coa_jurnal_coa_jurnal_id_seq;
       public       postgres    false    200            m           0    0 "   beone_coa_jurnal_coa_jurnal_id_seq    SEQUENCE OWNED BY     i   ALTER SEQUENCE public.beone_coa_jurnal_coa_jurnal_id_seq OWNED BY public.beone_coa_jurnal.coa_jurnal_id;
            public       postgres    false    201            �            1259    16811    beone_country    TABLE     �   CREATE TABLE public.beone_country (
    country_id integer NOT NULL,
    nama character varying(50),
    country_code character varying(10),
    flag integer
);
 !   DROP TABLE public.beone_country;
       public         postgres    false            �            1259    16814    beone_country_country_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_country_country_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 3   DROP SEQUENCE public.beone_country_country_id_seq;
       public       postgres    false    202            n           0    0    beone_country_country_id_seq    SEQUENCE OWNED BY     ]   ALTER SEQUENCE public.beone_country_country_id_seq OWNED BY public.beone_country.country_id;
            public       postgres    false    203            �            1259    16816    beone_custsup    TABLE     b  CREATE TABLE public.beone_custsup (
    custsup_id integer NOT NULL,
    nama character varying(100),
    alamat character varying(200),
    tipe_custsup integer,
    negara integer,
    saldo_hutang_idr double precision,
    saldo_hutang_valas double precision,
    saldo_piutang_idr double precision,
    saldo_piutang_valas double precision,
    flag integer,
    pelunasan_hutang_idr double precision,
    pelunasan_hutang_valas double precision,
    pelunasan_piutang_idr double precision,
    pelunasan_piutang_valas double precision,
    status_lunas_piutang integer,
    status_lunas_hutang integer
);
 !   DROP TABLE public.beone_custsup;
       public         postgres    false            �            1259    16819    beone_custsup_custsup_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_custsup_custsup_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 3   DROP SEQUENCE public.beone_custsup_custsup_id_seq;
       public       postgres    false    204            o           0    0    beone_custsup_custsup_id_seq    SEQUENCE OWNED BY     ]   ALTER SEQUENCE public.beone_custsup_custsup_id_seq OWNED BY public.beone_custsup.custsup_id;
            public       postgres    false    205            �            1259    16821    beone_export_detail    TABLE     �  CREATE TABLE public.beone_export_detail (
    export_detail_id integer NOT NULL,
    export_header_id integer,
    item_id integer,
    qty double precision,
    satuan_qty integer,
    price double precision,
    pack_qty double precision,
    satuan_pack integer,
    origin_country integer,
    doc character varying(50),
    volume double precision,
    netto double precision,
    brutto double precision,
    flag integer,
    sales_header_id integer
);
 '   DROP TABLE public.beone_export_detail;
       public         postgres    false            �            1259    16824 (   beone_export_detail_export_detail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_export_detail_export_detail_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ?   DROP SEQUENCE public.beone_export_detail_export_detail_id_seq;
       public       postgres    false    206            p           0    0 (   beone_export_detail_export_detail_id_seq    SEQUENCE OWNED BY     u   ALTER SEQUENCE public.beone_export_detail_export_detail_id_seq OWNED BY public.beone_export_detail.export_detail_id;
            public       postgres    false    207            �            1259    16826    beone_export_header    TABLE       CREATE TABLE public.beone_export_header (
    export_header_id integer NOT NULL,
    jenis_bc integer,
    car_no character varying(50),
    bc_no character varying(50),
    bc_date date,
    kontrak_no character varying(50),
    kontrak_date date,
    jenis_export integer,
    invoice_no character varying(50),
    invoice_date date,
    surat_jalan_no character varying(50),
    surat_jalan_date date,
    receiver_id integer,
    country_id integer,
    price_type character varying(10),
    amount_value double precision,
    valas_value double precision,
    insurance_type character varying(10),
    insurance_value double precision,
    freight double precision,
    flag integer,
    status integer,
    delivery_date date,
    update_by integer,
    update_date date,
    delivery_no character varying(50),
    vessel character varying(100),
    port_loading character varying(100),
    port_destination character varying(100),
    container character varying(10),
    no_container character varying(25),
    no_seal character varying(25)
);
 '   DROP TABLE public.beone_export_header;
       public         postgres    false            �            1259    16832 (   beone_export_header_export_header_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_export_header_export_header_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ?   DROP SEQUENCE public.beone_export_header_export_header_id_seq;
       public       postgres    false    208            q           0    0 (   beone_export_header_export_header_id_seq    SEQUENCE OWNED BY     u   ALTER SEQUENCE public.beone_export_header_export_header_id_seq OWNED BY public.beone_export_header.export_header_id;
            public       postgres    false    209            �            1259    16834    beone_gl    TABLE     �  CREATE TABLE public.beone_gl (
    gl_id bigint NOT NULL,
    gl_date date,
    coa_id integer,
    coa_no character varying(20),
    coa_id_lawan integer,
    coa_no_lawan character varying(20),
    keterangan character varying(100),
    debet double precision,
    kredit double precision,
    pasangan_no character varying(50),
    gl_number character varying(50),
    update_by integer,
    update_date date
);
    DROP TABLE public.beone_gl;
       public         postgres    false            �            1259    16837    beone_gl_gl_id_seq    SEQUENCE     {   CREATE SEQUENCE public.beone_gl_gl_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.beone_gl_gl_id_seq;
       public       postgres    false    210            r           0    0    beone_gl_gl_id_seq    SEQUENCE OWNED BY     I   ALTER SEQUENCE public.beone_gl_gl_id_seq OWNED BY public.beone_gl.gl_id;
            public       postgres    false    211            �            1259    16839    beone_gudang    TABLE     �   CREATE TABLE public.beone_gudang (
    gudang_id integer NOT NULL,
    nama character varying(100),
    keterangan character varying(200),
    flag integer
);
     DROP TABLE public.beone_gudang;
       public         postgres    false            �            1259    16842    beone_gudang_detail    TABLE     �  CREATE TABLE public.beone_gudang_detail (
    gudang_detail_id bigint NOT NULL,
    gudang_id integer,
    trans_date date,
    item_id integer,
    qty_in double precision,
    qty_out double precision,
    nomor_transaksi character varying(50),
    update_by integer,
    update_date date,
    flag integer,
    keterangan character varying(100),
    kode_tracing character varying(50)
);
 '   DROP TABLE public.beone_gudang_detail;
       public         postgres    false            �            1259    16845 (   beone_gudang_detail_gudang_detail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_gudang_detail_gudang_detail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ?   DROP SEQUENCE public.beone_gudang_detail_gudang_detail_id_seq;
       public       postgres    false    213            s           0    0 (   beone_gudang_detail_gudang_detail_id_seq    SEQUENCE OWNED BY     u   ALTER SEQUENCE public.beone_gudang_detail_gudang_detail_id_seq OWNED BY public.beone_gudang_detail.gudang_detail_id;
            public       postgres    false    214            �            1259    16847    beone_gudang_gudang_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_gudang_gudang_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 1   DROP SEQUENCE public.beone_gudang_gudang_id_seq;
       public       postgres    false    212            t           0    0    beone_gudang_gudang_id_seq    SEQUENCE OWNED BY     Y   ALTER SEQUENCE public.beone_gudang_gudang_id_seq OWNED BY public.beone_gudang.gudang_id;
            public       postgres    false    215            �            1259    16849    beone_pemakaian_bahan_header    TABLE     T  CREATE TABLE public.beone_pemakaian_bahan_header (
    pemakaian_header_id bigint NOT NULL,
    nomor_pemakaian character varying(50),
    keterangan character varying(250),
    tgl_pemakaian date,
    update_by integer,
    update_date date,
    nomor_pm character varying(50),
    amount_pemakaian double precision,
    status integer
);
 0   DROP TABLE public.beone_pemakaian_bahan_header;
       public         postgres    false            �            1259    16852 4   beone_header_pemakaian_bahan_pemakaian_header_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_header_pemakaian_bahan_pemakaian_header_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 K   DROP SEQUENCE public.beone_header_pemakaian_bahan_pemakaian_header_id_seq;
       public       postgres    false    216            u           0    0 4   beone_header_pemakaian_bahan_pemakaian_header_id_seq    SEQUENCE OWNED BY     �   ALTER SEQUENCE public.beone_header_pemakaian_bahan_pemakaian_header_id_seq OWNED BY public.beone_pemakaian_bahan_header.pemakaian_header_id;
            public       postgres    false    217            �            1259    16854    beone_hutang_piutang    TABLE     �  CREATE TABLE public.beone_hutang_piutang (
    hutang_piutang_id bigint NOT NULL,
    custsup_id integer,
    trans_date date,
    nomor character varying(50),
    keterangan character varying(100),
    valas_trans double precision,
    idr_trans double precision,
    valas_pelunasan double precision,
    idr_pelunasan double precision,
    tipe_trans integer,
    update_by integer,
    update_date date,
    flag integer,
    status_lunas integer
);
 (   DROP TABLE public.beone_hutang_piutang;
       public         postgres    false            �            1259    16857 *   beone_hutang_piutang_hutang_piutang_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_hutang_piutang_hutang_piutang_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 A   DROP SEQUENCE public.beone_hutang_piutang_hutang_piutang_id_seq;
       public       postgres    false    218            v           0    0 *   beone_hutang_piutang_hutang_piutang_id_seq    SEQUENCE OWNED BY     y   ALTER SEQUENCE public.beone_hutang_piutang_hutang_piutang_id_seq OWNED BY public.beone_hutang_piutang.hutang_piutang_id;
            public       postgres    false    219            �            1259    16859    beone_import_detail    TABLE     �  CREATE TABLE public.beone_import_detail (
    import_detail_id integer NOT NULL,
    item_id integer,
    qty double precision,
    satuan_qty integer,
    price double precision,
    pack_qty double precision,
    satuan_pack integer,
    origin_country integer,
    volume double precision,
    netto double precision,
    brutto double precision,
    hscode character varying(50),
    tbm double precision,
    ppnn double precision,
    tpbm double precision,
    cukai integer,
    sat_cukai integer,
    cukai_value double precision,
    bea_masuk character varying(100),
    sat_bea_masuk integer,
    flag integer,
    item_type_id integer,
    import_header_id integer
);
 '   DROP TABLE public.beone_import_detail;
       public         postgres    false            �            1259    16862 (   beone_import_detail_import_detail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_import_detail_import_detail_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ?   DROP SEQUENCE public.beone_import_detail_import_detail_id_seq;
       public       postgres    false    220            w           0    0 (   beone_import_detail_import_detail_id_seq    SEQUENCE OWNED BY     u   ALTER SEQUENCE public.beone_import_detail_import_detail_id_seq OWNED BY public.beone_import_detail.import_detail_id;
            public       postgres    false    221            �            1259    16864    beone_import_header    TABLE     C  CREATE TABLE public.beone_import_header (
    import_header_id integer NOT NULL,
    jenis_bc integer,
    car_no character varying(50),
    bc_no character varying(50),
    bc_date date,
    invoice_no character varying(50),
    invoice_date date,
    kontrak_no character varying(50),
    kontrak_date date,
    purpose_id character varying(100),
    supplier_id integer,
    price_type character varying(10),
    "from" character varying(10),
    amount_value double precision,
    valas_value double precision,
    value_added double precision,
    discount double precision,
    insurance_type character varying(10),
    insurace_value double precision,
    freight double precision,
    flag integer,
    update_by integer,
    update_date date,
    status integer,
    receive_date date,
    receive_no character varying(50)
);
 '   DROP TABLE public.beone_import_header;
       public         postgres    false            �            1259    16867 (   beone_import_header_import_header_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_import_header_import_header_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ?   DROP SEQUENCE public.beone_import_header_import_header_id_seq;
       public       postgres    false    222            x           0    0 (   beone_import_header_import_header_id_seq    SEQUENCE OWNED BY     u   ALTER SEQUENCE public.beone_import_header_import_header_id_seq OWNED BY public.beone_import_header.import_header_id;
            public       postgres    false    223            �            1259    16869    beone_inventory    TABLE     �  CREATE TABLE public.beone_inventory (
    intvent_trans_id bigint NOT NULL,
    intvent_trans_no character varying(50),
    item_id integer,
    trans_date date,
    keterangan character varying(100),
    qty_in double precision,
    value_in double precision,
    qty_out double precision,
    value_out double precision,
    sa_qty double precision,
    sa_unit_price double precision,
    sa_amount double precision,
    flag integer,
    update_by integer,
    update_date date
);
 #   DROP TABLE public.beone_inventory;
       public         postgres    false            �            1259    16872 $   beone_inventory_intvent_trans_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_inventory_intvent_trans_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ;   DROP SEQUENCE public.beone_inventory_intvent_trans_id_seq;
       public       postgres    false    224            y           0    0 $   beone_inventory_intvent_trans_id_seq    SEQUENCE OWNED BY     m   ALTER SEQUENCE public.beone_inventory_intvent_trans_id_seq OWNED BY public.beone_inventory.intvent_trans_id;
            public       postgres    false    225            �            1259    16874 
   beone_item    TABLE     S  CREATE TABLE public.beone_item (
    item_id integer NOT NULL,
    nama character varying(100),
    item_code character varying(20),
    saldo_qty double precision,
    saldo_idr double precision,
    keterangan character varying(200),
    flag integer,
    item_type_id integer,
    hscode character varying(50),
    satuan_id integer
);
    DROP TABLE public.beone_item;
       public         postgres    false            �            1259    16877    beone_item_item_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_item_item_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.beone_item_item_id_seq;
       public       postgres    false    226            z           0    0    beone_item_item_id_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE public.beone_item_item_id_seq OWNED BY public.beone_item.item_id;
            public       postgres    false    227            �            1259    16879    beone_item_type    TABLE     �   CREATE TABLE public.beone_item_type (
    item_type_id integer NOT NULL,
    nama character varying(100),
    keterangan character varying(200),
    flag integer
);
 #   DROP TABLE public.beone_item_type;
       public         postgres    false            �            1259    16882     beone_item_type_item_type_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_item_type_item_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 7   DROP SEQUENCE public.beone_item_type_item_type_id_seq;
       public       postgres    false    228            {           0    0     beone_item_type_item_type_id_seq    SEQUENCE OWNED BY     e   ALTER SEQUENCE public.beone_item_type_item_type_id_seq OWNED BY public.beone_item_type.item_type_id;
            public       postgres    false    229            �            1259    16884    beone_kode_trans    TABLE     �   CREATE TABLE public.beone_kode_trans (
    kode_trans_id integer NOT NULL,
    nama character varying,
    coa_id integer,
    kode_bank character varying(20),
    in_out integer,
    flag integer
);
 $   DROP TABLE public.beone_kode_trans;
       public         postgres    false            �            1259    16890 "   beone_kode_trans_kode_trans_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_kode_trans_kode_trans_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 9   DROP SEQUENCE public.beone_kode_trans_kode_trans_id_seq;
       public       postgres    false    230            |           0    0 "   beone_kode_trans_kode_trans_id_seq    SEQUENCE OWNED BY     i   ALTER SEQUENCE public.beone_kode_trans_kode_trans_id_seq OWNED BY public.beone_kode_trans.kode_trans_id;
            public       postgres    false    231            &           1259    17197    beone_komposisi    TABLE     �   CREATE TABLE public.beone_komposisi (
    komposisi_id integer NOT NULL,
    item_jadi_id integer,
    item_baku_id integer,
    qty_item_baku double precision,
    flag integer
);
 #   DROP TABLE public.beone_komposisi;
       public         postgres    false            %           1259    17195     beone_komposisi_komposisi_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_komposisi_komposisi_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 7   DROP SEQUENCE public.beone_komposisi_komposisi_id_seq;
       public       postgres    false    294            }           0    0     beone_komposisi_komposisi_id_seq    SEQUENCE OWNED BY     e   ALTER SEQUENCE public.beone_komposisi_komposisi_id_seq OWNED BY public.beone_komposisi.komposisi_id;
            public       postgres    false    293            �            1259    16892    beone_konfigurasi_perusahaan    TABLE     G  CREATE TABLE public.beone_konfigurasi_perusahaan (
    nama_perusahaan character varying(100),
    alamat_perusahaan character varying(500),
    kota_perusahaan character varying(100),
    provinsi_perusahaan character varying(100),
    kode_pos_perusahaan character varying(50),
    logo_perusahaan character varying(1000)
);
 0   DROP TABLE public.beone_konfigurasi_perusahaan;
       public         postgres    false            *           1259    17213    beone_konversi_stok_detail    TABLE     �   CREATE TABLE public.beone_konversi_stok_detail (
    konversi_stok_detail_id integer NOT NULL,
    konversi_stok_header_id integer,
    item_id integer,
    qty double precision,
    qty_real double precision,
    satuan_qty integer
);
 .   DROP TABLE public.beone_konversi_stok_detail;
       public         postgres    false            )           1259    17211 6   beone_konversi_stok_detail_konversi_stok_detail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_konversi_stok_detail_konversi_stok_detail_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 M   DROP SEQUENCE public.beone_konversi_stok_detail_konversi_stok_detail_id_seq;
       public       postgres    false    298            ~           0    0 6   beone_konversi_stok_detail_konversi_stok_detail_id_seq    SEQUENCE OWNED BY     �   ALTER SEQUENCE public.beone_konversi_stok_detail_konversi_stok_detail_id_seq OWNED BY public.beone_konversi_stok_detail.konversi_stok_detail_id;
            public       postgres    false    297            (           1259    17205    beone_konversi_stok_header    TABLE       CREATE TABLE public.beone_konversi_stok_header (
    konversi_stok_header_id integer NOT NULL,
    item_id integer,
    konversi_stok_no character varying(255),
    konversi_stok_date date,
    flag integer,
    qty double precision,
    satuan_qty integer
);
 .   DROP TABLE public.beone_konversi_stok_header;
       public         postgres    false            '           1259    17203 6   beone_konversi_stok_header_konversi_stok_header_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_konversi_stok_header_konversi_stok_header_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 M   DROP SEQUENCE public.beone_konversi_stok_header_konversi_stok_header_id_seq;
       public       postgres    false    296                       0    0 6   beone_konversi_stok_header_konversi_stok_header_id_seq    SEQUENCE OWNED BY     �   ALTER SEQUENCE public.beone_konversi_stok_header_konversi_stok_header_id_seq OWNED BY public.beone_konversi_stok_header.konversi_stok_header_id;
            public       postgres    false    295            �            1259    16898 	   beone_log    TABLE     �   CREATE TABLE public.beone_log (
    log_id bigint NOT NULL,
    log_user character varying(100),
    log_tipe integer,
    log_desc character varying(250),
    log_time timestamp without time zone
);
    DROP TABLE public.beone_log;
       public         postgres    false            �            1259    16901    beone_log_log_id_seq    SEQUENCE     }   CREATE SEQUENCE public.beone_log_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.beone_log_log_id_seq;
       public       postgres    false    233            �           0    0    beone_log_log_id_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE public.beone_log_log_id_seq OWNED BY public.beone_log.log_id;
            public       postgres    false    234            �            1259    16903    beone_opname_detail    TABLE       CREATE TABLE public.beone_opname_detail (
    opname_detail_id integer NOT NULL,
    opname_header_id integer,
    item_id integer,
    qty_existing double precision,
    qty_opname double precision,
    qty_selisih double precision,
    status_opname integer,
    flag integer
);
 '   DROP TABLE public.beone_opname_detail;
       public         postgres    false            �            1259    16906 (   beone_opname_detail_opname_detail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_opname_detail_opname_detail_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ?   DROP SEQUENCE public.beone_opname_detail_opname_detail_id_seq;
       public       postgres    false    235            �           0    0 (   beone_opname_detail_opname_detail_id_seq    SEQUENCE OWNED BY     u   ALTER SEQUENCE public.beone_opname_detail_opname_detail_id_seq OWNED BY public.beone_opname_detail.opname_detail_id;
            public       postgres    false    236            �            1259    16908    beone_opname_header    TABLE     �  CREATE TABLE public.beone_opname_header (
    opname_header_id integer NOT NULL,
    document_no character varying(50),
    opname_date date,
    total_item_opname integer,
    total_item_opname_match integer,
    total_item_opname_plus integer,
    total_item_opname_min integer,
    update_by integer,
    flag integer,
    keterangan character varying(100),
    update_date date
);
 '   DROP TABLE public.beone_opname_header;
       public         postgres    false            �            1259    16911 (   beone_opname_header_opname_header_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_opname_header_opname_header_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ?   DROP SEQUENCE public.beone_opname_header_opname_header_id_seq;
       public       postgres    false    237            �           0    0 (   beone_opname_header_opname_header_id_seq    SEQUENCE OWNED BY     u   ALTER SEQUENCE public.beone_opname_header_opname_header_id_seq OWNED BY public.beone_opname_header.opname_header_id;
            public       postgres    false    238            �            1259    16913    beone_peleburan    TABLE     !  CREATE TABLE public.beone_peleburan (
    peleburan_id integer NOT NULL,
    peleburan_no character varying(20),
    peleburan_date date,
    keterangan character varying(100),
    item_id integer,
    qty_peleburan bigint,
    update_by integer,
    update_date date,
    flag integer
);
 #   DROP TABLE public.beone_peleburan;
       public         postgres    false            �            1259    16916     beone_peleburan_peleburan_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_peleburan_peleburan_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 7   DROP SEQUENCE public.beone_peleburan_peleburan_id_seq;
       public       postgres    false    239            �           0    0     beone_peleburan_peleburan_id_seq    SEQUENCE OWNED BY     e   ALTER SEQUENCE public.beone_peleburan_peleburan_id_seq OWNED BY public.beone_peleburan.peleburan_id;
            public       postgres    false    240            �            1259    16918    beone_pemakaian_bahan_detail    TABLE     �   CREATE TABLE public.beone_pemakaian_bahan_detail (
    pemakaian_detail_id bigint NOT NULL,
    pemakaian_header_id integer,
    item_id integer,
    qty double precision,
    unit_price double precision
);
 0   DROP TABLE public.beone_pemakaian_bahan_detail;
       public         postgres    false            �            1259    16921 4   beone_pemakaian_bahan_detail_pemakaian_detail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_pemakaian_bahan_detail_pemakaian_detail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 K   DROP SEQUENCE public.beone_pemakaian_bahan_detail_pemakaian_detail_id_seq;
       public       postgres    false    241            �           0    0 4   beone_pemakaian_bahan_detail_pemakaian_detail_id_seq    SEQUENCE OWNED BY     �   ALTER SEQUENCE public.beone_pemakaian_bahan_detail_pemakaian_detail_id_seq OWNED BY public.beone_pemakaian_bahan_detail.pemakaian_detail_id;
            public       postgres    false    242            $           1259    17187    beone_pm_detail    TABLE     �   CREATE TABLE public.beone_pm_detail (
    pm_detail_id integer NOT NULL,
    item_id integer,
    qty double precision,
    satuan_id integer,
    pm_header_id integer
);
 #   DROP TABLE public.beone_pm_detail;
       public         postgres    false            #           1259    17185     beone_pm_detail_pm_detail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_pm_detail_pm_detail_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 7   DROP SEQUENCE public.beone_pm_detail_pm_detail_id_seq;
       public       postgres    false    292            �           0    0     beone_pm_detail_pm_detail_id_seq    SEQUENCE OWNED BY     e   ALTER SEQUENCE public.beone_pm_detail_pm_detail_id_seq OWNED BY public.beone_pm_detail.pm_detail_id;
            public       postgres    false    291            �            1259    16923    beone_pm_header    TABLE     �   CREATE TABLE public.beone_pm_header (
    pm_header_id bigint NOT NULL,
    no_pm character varying(50),
    tgl_pm date,
    customer_id integer,
    qty double precision,
    keterangan_artikel character varying,
    status integer
);
 #   DROP TABLE public.beone_pm_header;
       public         postgres    false            �            1259    16929     beone_pm_header_pm_header_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_pm_header_pm_header_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 7   DROP SEQUENCE public.beone_pm_header_pm_header_id_seq;
       public       postgres    false    243            �           0    0     beone_pm_header_pm_header_id_seq    SEQUENCE OWNED BY     e   ALTER SEQUENCE public.beone_pm_header_pm_header_id_seq OWNED BY public.beone_pm_header.pm_header_id;
            public       postgres    false    244            �            1259    16931    beone_po_import_detail    TABLE     �   CREATE TABLE public.beone_po_import_detail (
    purchase_detail_id bigint NOT NULL,
    purchase_header_id integer,
    item_id integer,
    qty double precision,
    price double precision,
    amount double precision,
    flag integer
);
 *   DROP TABLE public.beone_po_import_detail;
       public         postgres    false            �            1259    16934 -   beone_po_import_detail_purchase_detail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_po_import_detail_purchase_detail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 D   DROP SEQUENCE public.beone_po_import_detail_purchase_detail_id_seq;
       public       postgres    false    245            �           0    0 -   beone_po_import_detail_purchase_detail_id_seq    SEQUENCE OWNED BY        ALTER SEQUENCE public.beone_po_import_detail_purchase_detail_id_seq OWNED BY public.beone_po_import_detail.purchase_detail_id;
            public       postgres    false    246            �            1259    16936    beone_po_import_header    TABLE     3  CREATE TABLE public.beone_po_import_header (
    purchase_header_id bigint NOT NULL,
    purchase_no character varying(50),
    trans_date date,
    supplier_id integer,
    keterangan character varying(250),
    grandtotal double precision,
    flag integer,
    update_by integer,
    update_date date
);
 *   DROP TABLE public.beone_po_import_header;
       public         postgres    false            �            1259    16939 -   beone_po_import_header_purchase_header_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_po_import_header_purchase_header_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 D   DROP SEQUENCE public.beone_po_import_header_purchase_header_id_seq;
       public       postgres    false    247            �           0    0 -   beone_po_import_header_purchase_header_id_seq    SEQUENCE OWNED BY        ALTER SEQUENCE public.beone_po_import_header_purchase_header_id_seq OWNED BY public.beone_po_import_header.purchase_header_id;
            public       postgres    false    248            �            1259    16941    beone_produksi_detail    TABLE     �   CREATE TABLE public.beone_produksi_detail (
    produksi_detail_id bigint NOT NULL,
    produksi_header_id integer,
    pemakaian_header_id integer
);
 )   DROP TABLE public.beone_produksi_detail;
       public         postgres    false            �            1259    16944 ,   beone_produksi_detail_produksi_detail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_produksi_detail_produksi_detail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 C   DROP SEQUENCE public.beone_produksi_detail_produksi_detail_id_seq;
       public       postgres    false    249            �           0    0 ,   beone_produksi_detail_produksi_detail_id_seq    SEQUENCE OWNED BY     }   ALTER SEQUENCE public.beone_produksi_detail_produksi_detail_id_seq OWNED BY public.beone_produksi_detail.produksi_detail_id;
            public       postgres    false    250            �            1259    16946    beone_produksi_header    TABLE     x  CREATE TABLE public.beone_produksi_header (
    produksi_header_id bigint NOT NULL,
    nomor_produksi character varying(20),
    tgl_produksi date,
    tipe_produksi integer,
    item_produksi integer,
    qty_hasil_produksi double precision,
    keterangan character varying(250),
    flag integer,
    updated_by integer,
    udpated_date date,
    pm_header_id integer
);
 )   DROP TABLE public.beone_produksi_header;
       public         postgres    false            �            1259    16949 ,   beone_produksi_header_produksi_header_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_produksi_header_produksi_header_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 C   DROP SEQUENCE public.beone_produksi_header_produksi_header_id_seq;
       public       postgres    false    251            �           0    0 ,   beone_produksi_header_produksi_header_id_seq    SEQUENCE OWNED BY     }   ALTER SEQUENCE public.beone_produksi_header_produksi_header_id_seq OWNED BY public.beone_produksi_header.produksi_header_id;
            public       postgres    false    252            �            1259    16951    beone_purchase_detail    TABLE     �   CREATE TABLE public.beone_purchase_detail (
    purchase_detail_id bigint NOT NULL,
    purchase_header_id integer,
    item_id integer,
    qty double precision,
    price double precision,
    amount double precision,
    flag integer
);
 )   DROP TABLE public.beone_purchase_detail;
       public         postgres    false            �            1259    16954 ,   beone_purchase_detail_purchase_detail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_purchase_detail_purchase_detail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 C   DROP SEQUENCE public.beone_purchase_detail_purchase_detail_id_seq;
       public       postgres    false    253            �           0    0 ,   beone_purchase_detail_purchase_detail_id_seq    SEQUENCE OWNED BY     }   ALTER SEQUENCE public.beone_purchase_detail_purchase_detail_id_seq OWNED BY public.beone_purchase_detail.purchase_detail_id;
            public       postgres    false    254            �            1259    16956    beone_purchase_header    TABLE     �  CREATE TABLE public.beone_purchase_header (
    purchase_header_id bigint NOT NULL,
    purchase_no character varying(50),
    trans_date date,
    supplier_id integer,
    keterangan character varying(200),
    update_by integer,
    update_date date,
    flag integer,
    ppn integer,
    subtotal double precision,
    ppn_value double precision,
    grandtotal double precision,
    realisasi integer DEFAULT 0 NOT NULL
);
 )   DROP TABLE public.beone_purchase_header;
       public         postgres    false                        1259    16959 ,   beone_purchase_header_purchase_header_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_purchase_header_purchase_header_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 C   DROP SEQUENCE public.beone_purchase_header_purchase_header_id_seq;
       public       postgres    false    255            �           0    0 ,   beone_purchase_header_purchase_header_id_seq    SEQUENCE OWNED BY     }   ALTER SEQUENCE public.beone_purchase_header_purchase_header_id_seq OWNED BY public.beone_purchase_header.purchase_header_id;
            public       postgres    false    256                       1259    16961    beone_received_import    TABLE     �  CREATE TABLE public.beone_received_import (
    received_id bigint NOT NULL,
    nomor_aju character varying(50),
    nomor_dokumen_pabean character varying(10),
    tanggal_received date,
    status_received integer,
    flag integer,
    update_by integer,
    update_date date,
    nomor_received character varying(50),
    tpb_header_id integer,
    kurs double precision,
    po_import_header_id integer
);
 )   DROP TABLE public.beone_received_import;
       public         postgres    false                       1259    16964 %   beone_received_import_received_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_received_import_received_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 <   DROP SEQUENCE public.beone_received_import_received_id_seq;
       public       postgres    false    257            �           0    0 %   beone_received_import_received_id_seq    SEQUENCE OWNED BY     o   ALTER SEQUENCE public.beone_received_import_received_id_seq OWNED BY public.beone_received_import.received_id;
            public       postgres    false    258                       1259    16966 
   beone_role    TABLE     �  CREATE TABLE public.beone_role (
    role_id integer NOT NULL,
    nama_role character varying(50),
    keterangan character varying(200),
    master_menu integer,
    pembelian_menu integer,
    penjualan_menu integer,
    inventory_menu integer,
    produksi_menu integer,
    asset_menu integer,
    jurnal_umum_menu integer,
    kasbank_menu integer,
    laporan_inventory integer,
    laporan_keuangan integer,
    konfigurasi integer,
    import_menu integer,
    eksport_menu integer
);
    DROP TABLE public.beone_role;
       public         postgres    false                       1259    16969    beone_role_role_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_role_role_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.beone_role_role_id_seq;
       public       postgres    false    259            �           0    0    beone_role_role_id_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE public.beone_role_role_id_seq OWNED BY public.beone_role.role_id;
            public       postgres    false    260                       1259    16971    beone_role_user    TABLE     �  CREATE TABLE public.beone_role_user (
    role_id integer NOT NULL,
    nama_role character varying(50),
    keterangan character varying(200),
    master_menu integer,
    user_add integer,
    user_edit integer,
    user_delete integer,
    role_add integer,
    role_edit integer,
    role_delete integer,
    customer_add integer,
    customer_edit integer,
    customer_delete integer,
    supplier_add integer,
    supplier_edit integer,
    supplier_delete integer,
    item_add integer,
    item_edit integer,
    item_delete integer,
    jenis_add integer,
    jenis_edit integer,
    jenis_delete integer,
    satuan_add integer,
    satuan_edit integer,
    satuan_delete integer,
    gudang_add integer,
    gudang_edit integer,
    gudang_delete integer,
    master_import integer,
    po_add integer,
    po_edit integer,
    po_delete integer,
    tracing integer,
    terima_barang integer,
    master_pembelian integer,
    pembelian_add integer,
    pembelian_edit integer,
    pembelian_delete integer,
    kredit_note_add integer,
    kredit_note_edit integer,
    kredit_note_delete integer,
    master_eksport integer,
    eksport_add integer,
    eksport_edit integer,
    eksport_delete integer,
    kirim_barang integer,
    menu_penjualan integer,
    penjualan_add integer,
    penjualan_edit integer,
    penjualan_delete integer,
    debit_note_add integer,
    debit_note_edit integer,
    debit_note_delete integer,
    menu_inventory integer,
    pindah_gudang integer,
    stockopname_add integer,
    stockopname_edit integer,
    stockopname_delete integer,
    stockopname_opname integer,
    adjustment_add integer,
    adjustment_edit integer,
    adjustment_delete integer,
    pemusnahan_add integer,
    pemusnahan_edit integer,
    pemusnahan_delete integer,
    recal_inventory integer,
    menu_produksi integer,
    produksi_add integer,
    produksi_edit integer,
    produksi_delete integer,
    menu_asset integer,
    menu_jurnal_umum integer,
    jurnal_umum_add integer,
    jurnal_umum_edit integer,
    jurnal_umum_delete integer,
    menu_kas_bank integer,
    kas_bank_add integer,
    kas_bank_edit integer,
    kas_bank_delete integer,
    menu_laporan_inventory integer,
    menu_laporan_keuangan integer,
    menu_konfigurasi integer
);
 #   DROP TABLE public.beone_role_user;
       public         postgres    false                       1259    16974    beone_role_user_role_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_role_user_role_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 2   DROP SEQUENCE public.beone_role_user_role_id_seq;
       public       postgres    false    261            �           0    0    beone_role_user_role_id_seq    SEQUENCE OWNED BY     [   ALTER SEQUENCE public.beone_role_user_role_id_seq OWNED BY public.beone_role_user.role_id;
            public       postgres    false    262                       1259    16976    beone_sales_detail    TABLE     �   CREATE TABLE public.beone_sales_detail (
    sales_detail_id bigint NOT NULL,
    sales_header_id integer,
    item_id integer,
    qty double precision,
    price double precision,
    amount double precision,
    flag integer
);
 &   DROP TABLE public.beone_sales_detail;
       public         postgres    false                       1259    16979 &   beone_sales_detail_sales_detail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_sales_detail_sales_detail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 =   DROP SEQUENCE public.beone_sales_detail_sales_detail_id_seq;
       public       postgres    false    263            �           0    0 &   beone_sales_detail_sales_detail_id_seq    SEQUENCE OWNED BY     q   ALTER SEQUENCE public.beone_sales_detail_sales_detail_id_seq OWNED BY public.beone_sales_detail.sales_detail_id;
            public       postgres    false    264            	           1259    16981    beone_sales_header    TABLE     �  CREATE TABLE public.beone_sales_header (
    sales_header_id bigint NOT NULL,
    sales_no character varying(50),
    trans_date date,
    customer_id integer,
    keterangan character varying(200),
    ppn double precision,
    subtotal double precision,
    ppn_value double precision,
    grandtotal double precision,
    update_by integer,
    update_date date,
    flag integer,
    biaya double precision,
    status integer,
    realisasi integer DEFAULT 0 NOT NULL
);
 &   DROP TABLE public.beone_sales_header;
       public         postgres    false            
           1259    16984 &   beone_sales_header_sales_header_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_sales_header_sales_header_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 =   DROP SEQUENCE public.beone_sales_header_sales_header_id_seq;
       public       postgres    false    265            �           0    0 &   beone_sales_header_sales_header_id_seq    SEQUENCE OWNED BY     q   ALTER SEQUENCE public.beone_sales_header_sales_header_id_seq OWNED BY public.beone_sales_header.sales_header_id;
            public       postgres    false    266                       1259    16986    beone_satuan_item    TABLE     �   CREATE TABLE public.beone_satuan_item (
    satuan_id integer NOT NULL,
    satuan_code character varying(5),
    keterangan character varying(100),
    flag integer
);
 %   DROP TABLE public.beone_satuan_item;
       public         postgres    false                       1259    16989    beone_satuan_item_satuan_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_satuan_item_satuan_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 6   DROP SEQUENCE public.beone_satuan_item_satuan_id_seq;
       public       postgres    false    267            �           0    0    beone_satuan_item_satuan_id_seq    SEQUENCE OWNED BY     c   ALTER SEQUENCE public.beone_satuan_item_satuan_id_seq OWNED BY public.beone_satuan_item.satuan_id;
            public       postgres    false    268                       1259    16991    beone_subkon_in_detail    TABLE     �   CREATE TABLE public.beone_subkon_in_detail (
    subkon_in_detail_id bigint NOT NULL,
    subkon_in_header_id integer,
    item_id integer,
    qty double precision,
    flag integer
);
 *   DROP TABLE public.beone_subkon_in_detail;
       public         postgres    false                       1259    16994 .   beone_subkon_in_detail_subkon_in_detail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_subkon_in_detail_subkon_in_detail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 E   DROP SEQUENCE public.beone_subkon_in_detail_subkon_in_detail_id_seq;
       public       postgres    false    269            �           0    0 .   beone_subkon_in_detail_subkon_in_detail_id_seq    SEQUENCE OWNED BY     �   ALTER SEQUENCE public.beone_subkon_in_detail_subkon_in_detail_id_seq OWNED BY public.beone_subkon_in_detail.subkon_in_detail_id;
            public       postgres    false    270                       1259    16996    beone_subkon_in_header    TABLE     ~  CREATE TABLE public.beone_subkon_in_header (
    subkon_in_id bigint NOT NULL,
    no_subkon_in character varying(50),
    no_subkon_out character varying(50),
    trans_date date,
    keterangan character varying(250),
    nomor_aju character varying(50),
    biaya_subkon double precision,
    supplier_id integer,
    flag integer,
    update_by integer,
    update_date date
);
 *   DROP TABLE public.beone_subkon_in_header;
       public         postgres    false                       1259    16999 '   beone_subkon_in_header_subkon_in_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_subkon_in_header_subkon_in_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 >   DROP SEQUENCE public.beone_subkon_in_header_subkon_in_id_seq;
       public       postgres    false    271            �           0    0 '   beone_subkon_in_header_subkon_in_id_seq    SEQUENCE OWNED BY     s   ALTER SEQUENCE public.beone_subkon_in_header_subkon_in_id_seq OWNED BY public.beone_subkon_in_header.subkon_in_id;
            public       postgres    false    272                       1259    17001    beone_subkon_out_detail    TABLE     �   CREATE TABLE public.beone_subkon_out_detail (
    subkon_out_detail_id bigint NOT NULL,
    subkon_out_header_id integer,
    item_id integer,
    qty double precision,
    unit_price double precision,
    flag integer
);
 +   DROP TABLE public.beone_subkon_out_detail;
       public         postgres    false                       1259    17004 0   beone_subkon_out_detail_subkon_out_detail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_subkon_out_detail_subkon_out_detail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 G   DROP SEQUENCE public.beone_subkon_out_detail_subkon_out_detail_id_seq;
       public       postgres    false    273            �           0    0 0   beone_subkon_out_detail_subkon_out_detail_id_seq    SEQUENCE OWNED BY     �   ALTER SEQUENCE public.beone_subkon_out_detail_subkon_out_detail_id_seq OWNED BY public.beone_subkon_out_detail.subkon_out_detail_id;
            public       postgres    false    274                       1259    17006    beone_subkon_out_header    TABLE     5  CREATE TABLE public.beone_subkon_out_header (
    subkon_out_id bigint NOT NULL,
    no_subkon_out character varying(50),
    trans_date date,
    supplier_id integer,
    keterangan character varying(250),
    nomor_aju character varying(50),
    flag integer,
    update_by integer,
    update_date date
);
 +   DROP TABLE public.beone_subkon_out_header;
       public         postgres    false                       1259    17009 )   beone_subkon_out_header_subkon_out_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_subkon_out_header_subkon_out_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 @   DROP SEQUENCE public.beone_subkon_out_header_subkon_out_id_seq;
       public       postgres    false    275            �           0    0 )   beone_subkon_out_header_subkon_out_id_seq    SEQUENCE OWNED BY     w   ALTER SEQUENCE public.beone_subkon_out_header_subkon_out_id_seq OWNED BY public.beone_subkon_out_header.subkon_out_id;
            public       postgres    false    276                       1259    17011    beone_tipe_coa    TABLE     i   CREATE TABLE public.beone_tipe_coa (
    tipe_coa_id integer NOT NULL,
    nama character varying(50)
);
 "   DROP TABLE public.beone_tipe_coa;
       public         postgres    false                       1259    17014    beone_tipe_coa_tipe_coa_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_tipe_coa_tipe_coa_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 5   DROP SEQUENCE public.beone_tipe_coa_tipe_coa_id_seq;
       public       postgres    false    277            �           0    0    beone_tipe_coa_tipe_coa_id_seq    SEQUENCE OWNED BY     a   ALTER SEQUENCE public.beone_tipe_coa_tipe_coa_id_seq OWNED BY public.beone_tipe_coa.tipe_coa_id;
            public       postgres    false    278                       1259    17016    beone_tipe_coa_transaksi    TABLE     �   CREATE TABLE public.beone_tipe_coa_transaksi (
    tipe_coa_trans_id integer NOT NULL,
    nama character varying(50),
    flag integer
);
 ,   DROP TABLE public.beone_tipe_coa_transaksi;
       public         postgres    false                       1259    17019 .   beone_tipe_coa_transaksi_tipe_coa_trans_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_tipe_coa_transaksi_tipe_coa_trans_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 E   DROP SEQUENCE public.beone_tipe_coa_transaksi_tipe_coa_trans_id_seq;
       public       postgres    false    279            �           0    0 .   beone_tipe_coa_transaksi_tipe_coa_trans_id_seq    SEQUENCE OWNED BY     �   ALTER SEQUENCE public.beone_tipe_coa_transaksi_tipe_coa_trans_id_seq OWNED BY public.beone_tipe_coa_transaksi.tipe_coa_trans_id;
            public       postgres    false    280                       1259    17021    beone_transfer_stock    TABLE       CREATE TABLE public.beone_transfer_stock (
    transfer_stock_id bigint NOT NULL,
    transfer_no character varying(50),
    transfer_date date,
    coa_kode_biaya integer,
    keterangan character varying(200),
    update_by integer,
    update_date date,
    flag integer
);
 (   DROP TABLE public.beone_transfer_stock;
       public         postgres    false                       1259    17024    beone_transfer_stock_detail    TABLE     u  CREATE TABLE public.beone_transfer_stock_detail (
    transfer_stock_detail_id bigint NOT NULL,
    transfer_stock_header_id integer,
    tipe_transfer_stock character varying(10),
    item_id integer,
    qty double precision,
    gudang_id integer,
    biaya double precision,
    update_by integer,
    update_date date,
    flag integer,
    persen_produksi integer
);
 /   DROP TABLE public.beone_transfer_stock_detail;
       public         postgres    false                       1259    17027 8   beone_transfer_stock_detail_transfer_stock_detail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_transfer_stock_detail_transfer_stock_detail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 O   DROP SEQUENCE public.beone_transfer_stock_detail_transfer_stock_detail_id_seq;
       public       postgres    false    282            �           0    0 8   beone_transfer_stock_detail_transfer_stock_detail_id_seq    SEQUENCE OWNED BY     �   ALTER SEQUENCE public.beone_transfer_stock_detail_transfer_stock_detail_id_seq OWNED BY public.beone_transfer_stock_detail.transfer_stock_detail_id;
            public       postgres    false    283                       1259    17029 *   beone_transfer_stock_transfer_stock_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_transfer_stock_transfer_stock_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 A   DROP SEQUENCE public.beone_transfer_stock_transfer_stock_id_seq;
       public       postgres    false    281            �           0    0 *   beone_transfer_stock_transfer_stock_id_seq    SEQUENCE OWNED BY     y   ALTER SEQUENCE public.beone_transfer_stock_transfer_stock_id_seq OWNED BY public.beone_transfer_stock.transfer_stock_id;
            public       postgres    false    284                       1259    17031 
   beone_user    TABLE     �   CREATE TABLE public.beone_user (
    user_id integer NOT NULL,
    username character varying(50),
    password character varying(50),
    nama character varying(100),
    role_id integer,
    update_by integer,
    update_date date,
    flag integer
);
    DROP TABLE public.beone_user;
       public         postgres    false                       1259    17034    beone_user_user_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_user_user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.beone_user_user_id_seq;
       public       postgres    false    285            �           0    0    beone_user_user_id_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE public.beone_user_user_id_seq OWNED BY public.beone_user.user_id;
            public       postgres    false    286                       1259    17036    beone_voucher_detail    TABLE     B  CREATE TABLE public.beone_voucher_detail (
    voucher_detail_id bigint NOT NULL,
    voucher_header_id integer,
    coa_id_lawan integer,
    coa_no_lawan character varying(20),
    jumlah_valas double precision,
    kurs double precision,
    jumlah_idr double precision,
    keterangan_detail character varying(200)
);
 (   DROP TABLE public.beone_voucher_detail;
       public         postgres    false                        1259    17039 *   beone_voucher_detail_voucher_detail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_voucher_detail_voucher_detail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 A   DROP SEQUENCE public.beone_voucher_detail_voucher_detail_id_seq;
       public       postgres    false    287            �           0    0 *   beone_voucher_detail_voucher_detail_id_seq    SEQUENCE OWNED BY     y   ALTER SEQUENCE public.beone_voucher_detail_voucher_detail_id_seq OWNED BY public.beone_voucher_detail.voucher_detail_id;
            public       postgres    false    288            !           1259    17041    beone_voucher_header    TABLE     ;  CREATE TABLE public.beone_voucher_header (
    voucher_header_id bigint NOT NULL,
    voucher_number character varying(20),
    voucher_date date,
    tipe integer,
    keterangan character varying(200),
    coa_id_cash_bank integer,
    coa_no character varying(20),
    update_by integer,
    update_date date
);
 (   DROP TABLE public.beone_voucher_header;
       public         postgres    false            "           1259    17044 *   beone_voucher_header_voucher_header_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_voucher_header_voucher_header_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 A   DROP SEQUENCE public.beone_voucher_header_voucher_header_id_seq;
       public       postgres    false    289            �           0    0 *   beone_voucher_header_voucher_header_id_seq    SEQUENCE OWNED BY     y   ALTER SEQUENCE public.beone_voucher_header_voucher_header_id_seq OWNED BY public.beone_voucher_header.voucher_header_id;
            public       postgres    false    290            �           2604    17046    beone_adjustment adjustment_id    DEFAULT     �   ALTER TABLE ONLY public.beone_adjustment ALTER COLUMN adjustment_id SET DEFAULT nextval('public.beone_adjustment_adjustment_id_seq'::regclass);
 M   ALTER TABLE public.beone_adjustment ALTER COLUMN adjustment_id DROP DEFAULT;
       public       postgres    false    197    196            �           2604    17047    beone_coa coa_id    DEFAULT     t   ALTER TABLE ONLY public.beone_coa ALTER COLUMN coa_id SET DEFAULT nextval('public.beone_coa_coa_id_seq'::regclass);
 ?   ALTER TABLE public.beone_coa ALTER COLUMN coa_id DROP DEFAULT;
       public       postgres    false    199    198            �           2604    17048    beone_coa_jurnal coa_jurnal_id    DEFAULT     �   ALTER TABLE ONLY public.beone_coa_jurnal ALTER COLUMN coa_jurnal_id SET DEFAULT nextval('public.beone_coa_jurnal_coa_jurnal_id_seq'::regclass);
 M   ALTER TABLE public.beone_coa_jurnal ALTER COLUMN coa_jurnal_id DROP DEFAULT;
       public       postgres    false    201    200            �           2604    17049    beone_country country_id    DEFAULT     �   ALTER TABLE ONLY public.beone_country ALTER COLUMN country_id SET DEFAULT nextval('public.beone_country_country_id_seq'::regclass);
 G   ALTER TABLE public.beone_country ALTER COLUMN country_id DROP DEFAULT;
       public       postgres    false    203    202            �           2604    17050    beone_custsup custsup_id    DEFAULT     �   ALTER TABLE ONLY public.beone_custsup ALTER COLUMN custsup_id SET DEFAULT nextval('public.beone_custsup_custsup_id_seq'::regclass);
 G   ALTER TABLE public.beone_custsup ALTER COLUMN custsup_id DROP DEFAULT;
       public       postgres    false    205    204            �           2604    17051 $   beone_export_detail export_detail_id    DEFAULT     �   ALTER TABLE ONLY public.beone_export_detail ALTER COLUMN export_detail_id SET DEFAULT nextval('public.beone_export_detail_export_detail_id_seq'::regclass);
 S   ALTER TABLE public.beone_export_detail ALTER COLUMN export_detail_id DROP DEFAULT;
       public       postgres    false    207    206            �           2604    17052 $   beone_export_header export_header_id    DEFAULT     �   ALTER TABLE ONLY public.beone_export_header ALTER COLUMN export_header_id SET DEFAULT nextval('public.beone_export_header_export_header_id_seq'::regclass);
 S   ALTER TABLE public.beone_export_header ALTER COLUMN export_header_id DROP DEFAULT;
       public       postgres    false    209    208            �           2604    17053    beone_gl gl_id    DEFAULT     p   ALTER TABLE ONLY public.beone_gl ALTER COLUMN gl_id SET DEFAULT nextval('public.beone_gl_gl_id_seq'::regclass);
 =   ALTER TABLE public.beone_gl ALTER COLUMN gl_id DROP DEFAULT;
       public       postgres    false    211    210            �           2604    17054    beone_gudang gudang_id    DEFAULT     �   ALTER TABLE ONLY public.beone_gudang ALTER COLUMN gudang_id SET DEFAULT nextval('public.beone_gudang_gudang_id_seq'::regclass);
 E   ALTER TABLE public.beone_gudang ALTER COLUMN gudang_id DROP DEFAULT;
       public       postgres    false    215    212            �           2604    17055 $   beone_gudang_detail gudang_detail_id    DEFAULT     �   ALTER TABLE ONLY public.beone_gudang_detail ALTER COLUMN gudang_detail_id SET DEFAULT nextval('public.beone_gudang_detail_gudang_detail_id_seq'::regclass);
 S   ALTER TABLE public.beone_gudang_detail ALTER COLUMN gudang_detail_id DROP DEFAULT;
       public       postgres    false    214    213            �           2604    17056 &   beone_hutang_piutang hutang_piutang_id    DEFAULT     �   ALTER TABLE ONLY public.beone_hutang_piutang ALTER COLUMN hutang_piutang_id SET DEFAULT nextval('public.beone_hutang_piutang_hutang_piutang_id_seq'::regclass);
 U   ALTER TABLE public.beone_hutang_piutang ALTER COLUMN hutang_piutang_id DROP DEFAULT;
       public       postgres    false    219    218            �           2604    17057 $   beone_import_detail import_detail_id    DEFAULT     �   ALTER TABLE ONLY public.beone_import_detail ALTER COLUMN import_detail_id SET DEFAULT nextval('public.beone_import_detail_import_detail_id_seq'::regclass);
 S   ALTER TABLE public.beone_import_detail ALTER COLUMN import_detail_id DROP DEFAULT;
       public       postgres    false    221    220            �           2604    17058 $   beone_import_header import_header_id    DEFAULT     �   ALTER TABLE ONLY public.beone_import_header ALTER COLUMN import_header_id SET DEFAULT nextval('public.beone_import_header_import_header_id_seq'::regclass);
 S   ALTER TABLE public.beone_import_header ALTER COLUMN import_header_id DROP DEFAULT;
       public       postgres    false    223    222            �           2604    17059     beone_inventory intvent_trans_id    DEFAULT     �   ALTER TABLE ONLY public.beone_inventory ALTER COLUMN intvent_trans_id SET DEFAULT nextval('public.beone_inventory_intvent_trans_id_seq'::regclass);
 O   ALTER TABLE public.beone_inventory ALTER COLUMN intvent_trans_id DROP DEFAULT;
       public       postgres    false    225    224            �           2604    17060    beone_item item_id    DEFAULT     x   ALTER TABLE ONLY public.beone_item ALTER COLUMN item_id SET DEFAULT nextval('public.beone_item_item_id_seq'::regclass);
 A   ALTER TABLE public.beone_item ALTER COLUMN item_id DROP DEFAULT;
       public       postgres    false    227    226                        2604    17061    beone_item_type item_type_id    DEFAULT     �   ALTER TABLE ONLY public.beone_item_type ALTER COLUMN item_type_id SET DEFAULT nextval('public.beone_item_type_item_type_id_seq'::regclass);
 K   ALTER TABLE public.beone_item_type ALTER COLUMN item_type_id DROP DEFAULT;
       public       postgres    false    229    228                       2604    17062    beone_kode_trans kode_trans_id    DEFAULT     �   ALTER TABLE ONLY public.beone_kode_trans ALTER COLUMN kode_trans_id SET DEFAULT nextval('public.beone_kode_trans_kode_trans_id_seq'::regclass);
 M   ALTER TABLE public.beone_kode_trans ALTER COLUMN kode_trans_id DROP DEFAULT;
       public       postgres    false    231    230            "           2604    17200    beone_komposisi komposisi_id    DEFAULT     �   ALTER TABLE ONLY public.beone_komposisi ALTER COLUMN komposisi_id SET DEFAULT nextval('public.beone_komposisi_komposisi_id_seq'::regclass);
 K   ALTER TABLE public.beone_komposisi ALTER COLUMN komposisi_id DROP DEFAULT;
       public       postgres    false    294    293    294            $           2604    17216 2   beone_konversi_stok_detail konversi_stok_detail_id    DEFAULT     �   ALTER TABLE ONLY public.beone_konversi_stok_detail ALTER COLUMN konversi_stok_detail_id SET DEFAULT nextval('public.beone_konversi_stok_detail_konversi_stok_detail_id_seq'::regclass);
 a   ALTER TABLE public.beone_konversi_stok_detail ALTER COLUMN konversi_stok_detail_id DROP DEFAULT;
       public       postgres    false    297    298    298            #           2604    17208 2   beone_konversi_stok_header konversi_stok_header_id    DEFAULT     �   ALTER TABLE ONLY public.beone_konversi_stok_header ALTER COLUMN konversi_stok_header_id SET DEFAULT nextval('public.beone_konversi_stok_header_konversi_stok_header_id_seq'::regclass);
 a   ALTER TABLE public.beone_konversi_stok_header ALTER COLUMN konversi_stok_header_id DROP DEFAULT;
       public       postgres    false    295    296    296                       2604    17063    beone_log log_id    DEFAULT     t   ALTER TABLE ONLY public.beone_log ALTER COLUMN log_id SET DEFAULT nextval('public.beone_log_log_id_seq'::regclass);
 ?   ALTER TABLE public.beone_log ALTER COLUMN log_id DROP DEFAULT;
       public       postgres    false    234    233                       2604    17064 $   beone_opname_detail opname_detail_id    DEFAULT     �   ALTER TABLE ONLY public.beone_opname_detail ALTER COLUMN opname_detail_id SET DEFAULT nextval('public.beone_opname_detail_opname_detail_id_seq'::regclass);
 S   ALTER TABLE public.beone_opname_detail ALTER COLUMN opname_detail_id DROP DEFAULT;
       public       postgres    false    236    235                       2604    17065 $   beone_opname_header opname_header_id    DEFAULT     �   ALTER TABLE ONLY public.beone_opname_header ALTER COLUMN opname_header_id SET DEFAULT nextval('public.beone_opname_header_opname_header_id_seq'::regclass);
 S   ALTER TABLE public.beone_opname_header ALTER COLUMN opname_header_id DROP DEFAULT;
       public       postgres    false    238    237                       2604    17066    beone_peleburan peleburan_id    DEFAULT     �   ALTER TABLE ONLY public.beone_peleburan ALTER COLUMN peleburan_id SET DEFAULT nextval('public.beone_peleburan_peleburan_id_seq'::regclass);
 K   ALTER TABLE public.beone_peleburan ALTER COLUMN peleburan_id DROP DEFAULT;
       public       postgres    false    240    239                       2604    17067 0   beone_pemakaian_bahan_detail pemakaian_detail_id    DEFAULT     �   ALTER TABLE ONLY public.beone_pemakaian_bahan_detail ALTER COLUMN pemakaian_detail_id SET DEFAULT nextval('public.beone_pemakaian_bahan_detail_pemakaian_detail_id_seq'::regclass);
 _   ALTER TABLE public.beone_pemakaian_bahan_detail ALTER COLUMN pemakaian_detail_id DROP DEFAULT;
       public       postgres    false    242    241            �           2604    17068 0   beone_pemakaian_bahan_header pemakaian_header_id    DEFAULT     �   ALTER TABLE ONLY public.beone_pemakaian_bahan_header ALTER COLUMN pemakaian_header_id SET DEFAULT nextval('public.beone_header_pemakaian_bahan_pemakaian_header_id_seq'::regclass);
 _   ALTER TABLE public.beone_pemakaian_bahan_header ALTER COLUMN pemakaian_header_id DROP DEFAULT;
       public       postgres    false    217    216            !           2604    17190    beone_pm_detail pm_detail_id    DEFAULT     �   ALTER TABLE ONLY public.beone_pm_detail ALTER COLUMN pm_detail_id SET DEFAULT nextval('public.beone_pm_detail_pm_detail_id_seq'::regclass);
 K   ALTER TABLE public.beone_pm_detail ALTER COLUMN pm_detail_id DROP DEFAULT;
       public       postgres    false    291    292    292                       2604    17069    beone_pm_header pm_header_id    DEFAULT     �   ALTER TABLE ONLY public.beone_pm_header ALTER COLUMN pm_header_id SET DEFAULT nextval('public.beone_pm_header_pm_header_id_seq'::regclass);
 K   ALTER TABLE public.beone_pm_header ALTER COLUMN pm_header_id DROP DEFAULT;
       public       postgres    false    244    243                       2604    17070 )   beone_po_import_detail purchase_detail_id    DEFAULT     �   ALTER TABLE ONLY public.beone_po_import_detail ALTER COLUMN purchase_detail_id SET DEFAULT nextval('public.beone_po_import_detail_purchase_detail_id_seq'::regclass);
 X   ALTER TABLE public.beone_po_import_detail ALTER COLUMN purchase_detail_id DROP DEFAULT;
       public       postgres    false    246    245            	           2604    17071 )   beone_po_import_header purchase_header_id    DEFAULT     �   ALTER TABLE ONLY public.beone_po_import_header ALTER COLUMN purchase_header_id SET DEFAULT nextval('public.beone_po_import_header_purchase_header_id_seq'::regclass);
 X   ALTER TABLE public.beone_po_import_header ALTER COLUMN purchase_header_id DROP DEFAULT;
       public       postgres    false    248    247            
           2604    17072 (   beone_produksi_detail produksi_detail_id    DEFAULT     �   ALTER TABLE ONLY public.beone_produksi_detail ALTER COLUMN produksi_detail_id SET DEFAULT nextval('public.beone_produksi_detail_produksi_detail_id_seq'::regclass);
 W   ALTER TABLE public.beone_produksi_detail ALTER COLUMN produksi_detail_id DROP DEFAULT;
       public       postgres    false    250    249                       2604    17073 (   beone_produksi_header produksi_header_id    DEFAULT     �   ALTER TABLE ONLY public.beone_produksi_header ALTER COLUMN produksi_header_id SET DEFAULT nextval('public.beone_produksi_header_produksi_header_id_seq'::regclass);
 W   ALTER TABLE public.beone_produksi_header ALTER COLUMN produksi_header_id DROP DEFAULT;
       public       postgres    false    252    251                       2604    17074 (   beone_purchase_detail purchase_detail_id    DEFAULT     �   ALTER TABLE ONLY public.beone_purchase_detail ALTER COLUMN purchase_detail_id SET DEFAULT nextval('public.beone_purchase_detail_purchase_detail_id_seq'::regclass);
 W   ALTER TABLE public.beone_purchase_detail ALTER COLUMN purchase_detail_id DROP DEFAULT;
       public       postgres    false    254    253                       2604    17075 (   beone_purchase_header purchase_header_id    DEFAULT     �   ALTER TABLE ONLY public.beone_purchase_header ALTER COLUMN purchase_header_id SET DEFAULT nextval('public.beone_purchase_header_purchase_header_id_seq'::regclass);
 W   ALTER TABLE public.beone_purchase_header ALTER COLUMN purchase_header_id DROP DEFAULT;
       public       postgres    false    256    255                       2604    17076 !   beone_received_import received_id    DEFAULT     �   ALTER TABLE ONLY public.beone_received_import ALTER COLUMN received_id SET DEFAULT nextval('public.beone_received_import_received_id_seq'::regclass);
 P   ALTER TABLE public.beone_received_import ALTER COLUMN received_id DROP DEFAULT;
       public       postgres    false    258    257                       2604    17077    beone_role role_id    DEFAULT     x   ALTER TABLE ONLY public.beone_role ALTER COLUMN role_id SET DEFAULT nextval('public.beone_role_role_id_seq'::regclass);
 A   ALTER TABLE public.beone_role ALTER COLUMN role_id DROP DEFAULT;
       public       postgres    false    260    259                       2604    17078    beone_role_user role_id    DEFAULT     �   ALTER TABLE ONLY public.beone_role_user ALTER COLUMN role_id SET DEFAULT nextval('public.beone_role_user_role_id_seq'::regclass);
 F   ALTER TABLE public.beone_role_user ALTER COLUMN role_id DROP DEFAULT;
       public       postgres    false    262    261                       2604    17079 "   beone_sales_detail sales_detail_id    DEFAULT     �   ALTER TABLE ONLY public.beone_sales_detail ALTER COLUMN sales_detail_id SET DEFAULT nextval('public.beone_sales_detail_sales_detail_id_seq'::regclass);
 Q   ALTER TABLE public.beone_sales_detail ALTER COLUMN sales_detail_id DROP DEFAULT;
       public       postgres    false    264    263                       2604    17080 "   beone_sales_header sales_header_id    DEFAULT     �   ALTER TABLE ONLY public.beone_sales_header ALTER COLUMN sales_header_id SET DEFAULT nextval('public.beone_sales_header_sales_header_id_seq'::regclass);
 Q   ALTER TABLE public.beone_sales_header ALTER COLUMN sales_header_id DROP DEFAULT;
       public       postgres    false    266    265                       2604    17081    beone_satuan_item satuan_id    DEFAULT     �   ALTER TABLE ONLY public.beone_satuan_item ALTER COLUMN satuan_id SET DEFAULT nextval('public.beone_satuan_item_satuan_id_seq'::regclass);
 J   ALTER TABLE public.beone_satuan_item ALTER COLUMN satuan_id DROP DEFAULT;
       public       postgres    false    268    267                       2604    17082 *   beone_subkon_in_detail subkon_in_detail_id    DEFAULT     �   ALTER TABLE ONLY public.beone_subkon_in_detail ALTER COLUMN subkon_in_detail_id SET DEFAULT nextval('public.beone_subkon_in_detail_subkon_in_detail_id_seq'::regclass);
 Y   ALTER TABLE public.beone_subkon_in_detail ALTER COLUMN subkon_in_detail_id DROP DEFAULT;
       public       postgres    false    270    269                       2604    17083 #   beone_subkon_in_header subkon_in_id    DEFAULT     �   ALTER TABLE ONLY public.beone_subkon_in_header ALTER COLUMN subkon_in_id SET DEFAULT nextval('public.beone_subkon_in_header_subkon_in_id_seq'::regclass);
 R   ALTER TABLE public.beone_subkon_in_header ALTER COLUMN subkon_in_id DROP DEFAULT;
       public       postgres    false    272    271                       2604    17084 ,   beone_subkon_out_detail subkon_out_detail_id    DEFAULT     �   ALTER TABLE ONLY public.beone_subkon_out_detail ALTER COLUMN subkon_out_detail_id SET DEFAULT nextval('public.beone_subkon_out_detail_subkon_out_detail_id_seq'::regclass);
 [   ALTER TABLE public.beone_subkon_out_detail ALTER COLUMN subkon_out_detail_id DROP DEFAULT;
       public       postgres    false    274    273                       2604    17085 %   beone_subkon_out_header subkon_out_id    DEFAULT     �   ALTER TABLE ONLY public.beone_subkon_out_header ALTER COLUMN subkon_out_id SET DEFAULT nextval('public.beone_subkon_out_header_subkon_out_id_seq'::regclass);
 T   ALTER TABLE public.beone_subkon_out_header ALTER COLUMN subkon_out_id DROP DEFAULT;
       public       postgres    false    276    275                       2604    17086    beone_tipe_coa tipe_coa_id    DEFAULT     �   ALTER TABLE ONLY public.beone_tipe_coa ALTER COLUMN tipe_coa_id SET DEFAULT nextval('public.beone_tipe_coa_tipe_coa_id_seq'::regclass);
 I   ALTER TABLE public.beone_tipe_coa ALTER COLUMN tipe_coa_id DROP DEFAULT;
       public       postgres    false    278    277                       2604    17087 *   beone_tipe_coa_transaksi tipe_coa_trans_id    DEFAULT     �   ALTER TABLE ONLY public.beone_tipe_coa_transaksi ALTER COLUMN tipe_coa_trans_id SET DEFAULT nextval('public.beone_tipe_coa_transaksi_tipe_coa_trans_id_seq'::regclass);
 Y   ALTER TABLE public.beone_tipe_coa_transaksi ALTER COLUMN tipe_coa_trans_id DROP DEFAULT;
       public       postgres    false    280    279                       2604    17088 &   beone_transfer_stock transfer_stock_id    DEFAULT     �   ALTER TABLE ONLY public.beone_transfer_stock ALTER COLUMN transfer_stock_id SET DEFAULT nextval('public.beone_transfer_stock_transfer_stock_id_seq'::regclass);
 U   ALTER TABLE public.beone_transfer_stock ALTER COLUMN transfer_stock_id DROP DEFAULT;
       public       postgres    false    284    281                       2604    17089 4   beone_transfer_stock_detail transfer_stock_detail_id    DEFAULT     �   ALTER TABLE ONLY public.beone_transfer_stock_detail ALTER COLUMN transfer_stock_detail_id SET DEFAULT nextval('public.beone_transfer_stock_detail_transfer_stock_detail_id_seq'::regclass);
 c   ALTER TABLE public.beone_transfer_stock_detail ALTER COLUMN transfer_stock_detail_id DROP DEFAULT;
       public       postgres    false    283    282                       2604    17090    beone_user user_id    DEFAULT     x   ALTER TABLE ONLY public.beone_user ALTER COLUMN user_id SET DEFAULT nextval('public.beone_user_user_id_seq'::regclass);
 A   ALTER TABLE public.beone_user ALTER COLUMN user_id DROP DEFAULT;
       public       postgres    false    286    285                       2604    17091 &   beone_voucher_detail voucher_detail_id    DEFAULT     �   ALTER TABLE ONLY public.beone_voucher_detail ALTER COLUMN voucher_detail_id SET DEFAULT nextval('public.beone_voucher_detail_voucher_detail_id_seq'::regclass);
 U   ALTER TABLE public.beone_voucher_detail ALTER COLUMN voucher_detail_id DROP DEFAULT;
       public       postgres    false    288    287                        2604    17092 &   beone_voucher_header voucher_header_id    DEFAULT     �   ALTER TABLE ONLY public.beone_voucher_header ALTER COLUMN voucher_header_id SET DEFAULT nextval('public.beone_voucher_header_voucher_header_id_seq'::regclass);
 U   ALTER TABLE public.beone_voucher_header ALTER COLUMN voucher_header_id DROP DEFAULT;
       public       postgres    false    290    289            �          0    16790    beone_adjustment 
   TABLE DATA               �   COPY public.beone_adjustment (adjustment_id, adjustment_no, adjustment_date, keterangan, item_id, qty_adjustment, posisi_qty, update_by, update_date, flag) FROM stdin;
    public       postgres    false    196   ��                 0    16795 	   beone_coa 
   TABLE DATA               �   COPY public.beone_coa (coa_id, nama, nomor, tipe_akun, debet_valas, debet_idr, kredit_valas, kredit_idr, dk, tipe_transaksi) FROM stdin;
    public       postgres    false    198   �                0    16803    beone_coa_jurnal 
   TABLE DATA               q   COPY public.beone_coa_jurnal (coa_jurnal_id, nama_coa_jurnal, keterangan_coa_jurnal, coa_id, coa_no) FROM stdin;
    public       postgres    false    200   �                0    16811    beone_country 
   TABLE DATA               M   COPY public.beone_country (country_id, nama, country_code, flag) FROM stdin;
    public       postgres    false    202   �                0    16816    beone_custsup 
   TABLE DATA               4  COPY public.beone_custsup (custsup_id, nama, alamat, tipe_custsup, negara, saldo_hutang_idr, saldo_hutang_valas, saldo_piutang_idr, saldo_piutang_valas, flag, pelunasan_hutang_idr, pelunasan_hutang_valas, pelunasan_piutang_idr, pelunasan_piutang_valas, status_lunas_piutang, status_lunas_hutang) FROM stdin;
    public       postgres    false    204   3                0    16821    beone_export_detail 
   TABLE DATA               �   COPY public.beone_export_detail (export_detail_id, export_header_id, item_id, qty, satuan_qty, price, pack_qty, satuan_pack, origin_country, doc, volume, netto, brutto, flag, sales_header_id) FROM stdin;
    public       postgres    false    206   �      
          0    16826    beone_export_header 
   TABLE DATA               �  COPY public.beone_export_header (export_header_id, jenis_bc, car_no, bc_no, bc_date, kontrak_no, kontrak_date, jenis_export, invoice_no, invoice_date, surat_jalan_no, surat_jalan_date, receiver_id, country_id, price_type, amount_value, valas_value, insurance_type, insurance_value, freight, flag, status, delivery_date, update_by, update_date, delivery_no, vessel, port_loading, port_destination, container, no_container, no_seal) FROM stdin;
    public       postgres    false    208                    0    16834    beone_gl 
   TABLE DATA               �   COPY public.beone_gl (gl_id, gl_date, coa_id, coa_no, coa_id_lawan, coa_no_lawan, keterangan, debet, kredit, pasangan_no, gl_number, update_by, update_date) FROM stdin;
    public       postgres    false    210   �                 0    16839    beone_gudang 
   TABLE DATA               I   COPY public.beone_gudang (gudang_id, nama, keterangan, flag) FROM stdin;
    public       postgres    false    212   f#                0    16842    beone_gudang_detail 
   TABLE DATA               �   COPY public.beone_gudang_detail (gudang_detail_id, gudang_id, trans_date, item_id, qty_in, qty_out, nomor_transaksi, update_by, update_date, flag, keterangan, kode_tracing) FROM stdin;
    public       postgres    false    213   �#                0    16854    beone_hutang_piutang 
   TABLE DATA               �   COPY public.beone_hutang_piutang (hutang_piutang_id, custsup_id, trans_date, nomor, keterangan, valas_trans, idr_trans, valas_pelunasan, idr_pelunasan, tipe_trans, update_by, update_date, flag, status_lunas) FROM stdin;
    public       postgres    false    218   �$                0    16859    beone_import_detail 
   TABLE DATA                 COPY public.beone_import_detail (import_detail_id, item_id, qty, satuan_qty, price, pack_qty, satuan_pack, origin_country, volume, netto, brutto, hscode, tbm, ppnn, tpbm, cukai, sat_cukai, cukai_value, bea_masuk, sat_bea_masuk, flag, item_type_id, import_header_id) FROM stdin;
    public       postgres    false    220   r%                0    16864    beone_import_header 
   TABLE DATA               ]  COPY public.beone_import_header (import_header_id, jenis_bc, car_no, bc_no, bc_date, invoice_no, invoice_date, kontrak_no, kontrak_date, purpose_id, supplier_id, price_type, "from", amount_value, valas_value, value_added, discount, insurance_type, insurace_value, freight, flag, update_by, update_date, status, receive_date, receive_no) FROM stdin;
    public       postgres    false    222   �%                0    16869    beone_inventory 
   TABLE DATA               �   COPY public.beone_inventory (intvent_trans_id, intvent_trans_no, item_id, trans_date, keterangan, qty_in, value_in, qty_out, value_out, sa_qty, sa_unit_price, sa_amount, flag, update_by, update_date) FROM stdin;
    public       postgres    false    224   �%                0    16874 
   beone_item 
   TABLE DATA               �   COPY public.beone_item (item_id, nama, item_code, saldo_qty, saldo_idr, keterangan, flag, item_type_id, hscode, satuan_id) FROM stdin;
    public       postgres    false    226   A'                0    16879    beone_item_type 
   TABLE DATA               O   COPY public.beone_item_type (item_type_id, nama, keterangan, flag) FROM stdin;
    public       postgres    false    228   �                 0    16884    beone_kode_trans 
   TABLE DATA               `   COPY public.beone_kode_trans (kode_trans_id, nama, coa_id, kode_bank, in_out, flag) FROM stdin;
    public       postgres    false    230         `          0    17197    beone_komposisi 
   TABLE DATA               h   COPY public.beone_komposisi (komposisi_id, item_jadi_id, item_baku_id, qty_item_baku, flag) FROM stdin;
    public       postgres    false    294   }�      "          0    16892    beone_konfigurasi_perusahaan 
   TABLE DATA               �   COPY public.beone_konfigurasi_perusahaan (nama_perusahaan, alamat_perusahaan, kota_perusahaan, provinsi_perusahaan, kode_pos_perusahaan, logo_perusahaan) FROM stdin;
    public       postgres    false    232   ��      d          0    17213    beone_konversi_stok_detail 
   TABLE DATA               �   COPY public.beone_konversi_stok_detail (konversi_stok_detail_id, konversi_stok_header_id, item_id, qty, qty_real, satuan_qty) FROM stdin;
    public       postgres    false    298   �      b          0    17205    beone_konversi_stok_header 
   TABLE DATA               �   COPY public.beone_konversi_stok_header (konversi_stok_header_id, item_id, konversi_stok_no, konversi_stok_date, flag, qty, satuan_qty) FROM stdin;
    public       postgres    false    296   �      #          0    16898 	   beone_log 
   TABLE DATA               S   COPY public.beone_log (log_id, log_user, log_tipe, log_desc, log_time) FROM stdin;
    public       postgres    false    233   %�      %          0    16903    beone_opname_detail 
   TABLE DATA               �   COPY public.beone_opname_detail (opname_detail_id, opname_header_id, item_id, qty_existing, qty_opname, qty_selisih, status_opname, flag) FROM stdin;
    public       postgres    false    235   ��      '          0    16908    beone_opname_header 
   TABLE DATA               �   COPY public.beone_opname_header (opname_header_id, document_no, opname_date, total_item_opname, total_item_opname_match, total_item_opname_plus, total_item_opname_min, update_by, flag, keterangan, update_date) FROM stdin;
    public       postgres    false    237   آ      )          0    16913    beone_peleburan 
   TABLE DATA               �   COPY public.beone_peleburan (peleburan_id, peleburan_no, peleburan_date, keterangan, item_id, qty_peleburan, update_by, update_date, flag) FROM stdin;
    public       postgres    false    239   ��      +          0    16918    beone_pemakaian_bahan_detail 
   TABLE DATA               z   COPY public.beone_pemakaian_bahan_detail (pemakaian_detail_id, pemakaian_header_id, item_id, qty, unit_price) FROM stdin;
    public       postgres    false    241   �                0    16849    beone_pemakaian_bahan_header 
   TABLE DATA               �   COPY public.beone_pemakaian_bahan_header (pemakaian_header_id, nomor_pemakaian, keterangan, tgl_pemakaian, update_by, update_date, nomor_pm, amount_pemakaian, status) FROM stdin;
    public       postgres    false    216   ��      ^          0    17187    beone_pm_detail 
   TABLE DATA               ^   COPY public.beone_pm_detail (pm_detail_id, item_id, qty, satuan_id, pm_header_id) FROM stdin;
    public       postgres    false    292   T�      -          0    16923    beone_pm_header 
   TABLE DATA               t   COPY public.beone_pm_header (pm_header_id, no_pm, tgl_pm, customer_id, qty, keterangan_artikel, status) FROM stdin;
    public       postgres    false    243   �      /          0    16931    beone_po_import_detail 
   TABLE DATA               {   COPY public.beone_po_import_detail (purchase_detail_id, purchase_header_id, item_id, qty, price, amount, flag) FROM stdin;
    public       postgres    false    245   �      1          0    16936    beone_po_import_header 
   TABLE DATA               �   COPY public.beone_po_import_header (purchase_header_id, purchase_no, trans_date, supplier_id, keterangan, grandtotal, flag, update_by, update_date) FROM stdin;
    public       postgres    false    247   A�      3          0    16941    beone_produksi_detail 
   TABLE DATA               l   COPY public.beone_produksi_detail (produksi_detail_id, produksi_header_id, pemakaian_header_id) FROM stdin;
    public       postgres    false    249   ԥ      5          0    16946    beone_produksi_header 
   TABLE DATA               �   COPY public.beone_produksi_header (produksi_header_id, nomor_produksi, tgl_produksi, tipe_produksi, item_produksi, qty_hasil_produksi, keterangan, flag, updated_by, udpated_date, pm_header_id) FROM stdin;
    public       postgres    false    251   ��      7          0    16951    beone_purchase_detail 
   TABLE DATA               z   COPY public.beone_purchase_detail (purchase_detail_id, purchase_header_id, item_id, qty, price, amount, flag) FROM stdin;
    public       postgres    false    253   N�      9          0    16956    beone_purchase_header 
   TABLE DATA               �   COPY public.beone_purchase_header (purchase_header_id, purchase_no, trans_date, supplier_id, keterangan, update_by, update_date, flag, ppn, subtotal, ppn_value, grandtotal, realisasi) FROM stdin;
    public       postgres    false    255   k�      ;          0    16961    beone_received_import 
   TABLE DATA               �   COPY public.beone_received_import (received_id, nomor_aju, nomor_dokumen_pabean, tanggal_received, status_received, flag, update_by, update_date, nomor_received, tpb_header_id, kurs, po_import_header_id) FROM stdin;
    public       postgres    false    257   ��      =          0    16966 
   beone_role 
   TABLE DATA               	  COPY public.beone_role (role_id, nama_role, keterangan, master_menu, pembelian_menu, penjualan_menu, inventory_menu, produksi_menu, asset_menu, jurnal_umum_menu, kasbank_menu, laporan_inventory, laporan_keuangan, konfigurasi, import_menu, eksport_menu) FROM stdin;
    public       postgres    false    259   ��      ?          0    16971    beone_role_user 
   TABLE DATA               �  COPY public.beone_role_user (role_id, nama_role, keterangan, master_menu, user_add, user_edit, user_delete, role_add, role_edit, role_delete, customer_add, customer_edit, customer_delete, supplier_add, supplier_edit, supplier_delete, item_add, item_edit, item_delete, jenis_add, jenis_edit, jenis_delete, satuan_add, satuan_edit, satuan_delete, gudang_add, gudang_edit, gudang_delete, master_import, po_add, po_edit, po_delete, tracing, terima_barang, master_pembelian, pembelian_add, pembelian_edit, pembelian_delete, kredit_note_add, kredit_note_edit, kredit_note_delete, master_eksport, eksport_add, eksport_edit, eksport_delete, kirim_barang, menu_penjualan, penjualan_add, penjualan_edit, penjualan_delete, debit_note_add, debit_note_edit, debit_note_delete, menu_inventory, pindah_gudang, stockopname_add, stockopname_edit, stockopname_delete, stockopname_opname, adjustment_add, adjustment_edit, adjustment_delete, pemusnahan_add, pemusnahan_edit, pemusnahan_delete, recal_inventory, menu_produksi, produksi_add, produksi_edit, produksi_delete, menu_asset, menu_jurnal_umum, jurnal_umum_add, jurnal_umum_edit, jurnal_umum_delete, menu_kas_bank, kas_bank_add, kas_bank_edit, kas_bank_delete, menu_laporan_inventory, menu_laporan_keuangan, menu_konfigurasi) FROM stdin;
    public       postgres    false    261   t�      A          0    16976    beone_sales_detail 
   TABLE DATA               q   COPY public.beone_sales_detail (sales_detail_id, sales_header_id, item_id, qty, price, amount, flag) FROM stdin;
    public       postgres    false    263   ��      C          0    16981    beone_sales_header 
   TABLE DATA               �   COPY public.beone_sales_header (sales_header_id, sales_no, trans_date, customer_id, keterangan, ppn, subtotal, ppn_value, grandtotal, update_by, update_date, flag, biaya, status, realisasi) FROM stdin;
    public       postgres    false    265   �      E          0    16986    beone_satuan_item 
   TABLE DATA               U   COPY public.beone_satuan_item (satuan_id, satuan_code, keterangan, flag) FROM stdin;
    public       postgres    false    267   /�      G          0    16991    beone_subkon_in_detail 
   TABLE DATA               n   COPY public.beone_subkon_in_detail (subkon_in_detail_id, subkon_in_header_id, item_id, qty, flag) FROM stdin;
    public       postgres    false    269   #�      I          0    16996    beone_subkon_in_header 
   TABLE DATA               �   COPY public.beone_subkon_in_header (subkon_in_id, no_subkon_in, no_subkon_out, trans_date, keterangan, nomor_aju, biaya_subkon, supplier_id, flag, update_by, update_date) FROM stdin;
    public       postgres    false    271   @�      K          0    17001    beone_subkon_out_detail 
   TABLE DATA               }   COPY public.beone_subkon_out_detail (subkon_out_detail_id, subkon_out_header_id, item_id, qty, unit_price, flag) FROM stdin;
    public       postgres    false    273   ]�      M          0    17006    beone_subkon_out_header 
   TABLE DATA               �   COPY public.beone_subkon_out_header (subkon_out_id, no_subkon_out, trans_date, supplier_id, keterangan, nomor_aju, flag, update_by, update_date) FROM stdin;
    public       postgres    false    275   z�      O          0    17011    beone_tipe_coa 
   TABLE DATA               ;   COPY public.beone_tipe_coa (tipe_coa_id, nama) FROM stdin;
    public       postgres    false    277   ��      Q          0    17016    beone_tipe_coa_transaksi 
   TABLE DATA               Q   COPY public.beone_tipe_coa_transaksi (tipe_coa_trans_id, nama, flag) FROM stdin;
    public       postgres    false    279   �      S          0    17021    beone_transfer_stock 
   TABLE DATA               �   COPY public.beone_transfer_stock (transfer_stock_id, transfer_no, transfer_date, coa_kode_biaya, keterangan, update_by, update_date, flag) FROM stdin;
    public       postgres    false    281   �      T          0    17024    beone_transfer_stock_detail 
   TABLE DATA               �   COPY public.beone_transfer_stock_detail (transfer_stock_detail_id, transfer_stock_header_id, tipe_transfer_stock, item_id, qty, gudang_id, biaya, update_by, update_date, flag, persen_produksi) FROM stdin;
    public       postgres    false    282   �      W          0    17031 
   beone_user 
   TABLE DATA               n   COPY public.beone_user (user_id, username, password, nama, role_id, update_by, update_date, flag) FROM stdin;
    public       postgres    false    285   $�      Y          0    17036    beone_voucher_detail 
   TABLE DATA               �   COPY public.beone_voucher_detail (voucher_detail_id, voucher_header_id, coa_id_lawan, coa_no_lawan, jumlah_valas, kurs, jumlah_idr, keterangan_detail) FROM stdin;
    public       postgres    false    287   ��      [          0    17041    beone_voucher_header 
   TABLE DATA               �   COPY public.beone_voucher_header (voucher_header_id, voucher_number, voucher_date, tipe, keterangan, coa_id_cash_bank, coa_no, update_by, update_date) FROM stdin;
    public       postgres    false    289   t�      �           0    0 "   beone_adjustment_adjustment_id_seq    SEQUENCE SET     Q   SELECT pg_catalog.setval('public.beone_adjustment_adjustment_id_seq', 10, true);
            public       postgres    false    197            �           0    0    beone_coa_coa_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.beone_coa_coa_id_seq', 1, false);
            public       postgres    false    199            �           0    0 "   beone_coa_jurnal_coa_jurnal_id_seq    SEQUENCE SET     Q   SELECT pg_catalog.setval('public.beone_coa_jurnal_coa_jurnal_id_seq', 12, true);
            public       postgres    false    201            �           0    0    beone_country_country_id_seq    SEQUENCE SET     J   SELECT pg_catalog.setval('public.beone_country_country_id_seq', 3, true);
            public       postgres    false    203            �           0    0    beone_custsup_custsup_id_seq    SEQUENCE SET     K   SELECT pg_catalog.setval('public.beone_custsup_custsup_id_seq', 34, true);
            public       postgres    false    205            �           0    0 (   beone_export_detail_export_detail_id_seq    SEQUENCE SET     W   SELECT pg_catalog.setval('public.beone_export_detail_export_detail_id_seq', 38, true);
            public       postgres    false    207            �           0    0 (   beone_export_header_export_header_id_seq    SEQUENCE SET     W   SELECT pg_catalog.setval('public.beone_export_header_export_header_id_seq', 33, true);
            public       postgres    false    209            �           0    0    beone_gl_gl_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.beone_gl_gl_id_seq', 2511, true);
            public       postgres    false    211            �           0    0 (   beone_gudang_detail_gudang_detail_id_seq    SEQUENCE SET     Y   SELECT pg_catalog.setval('public.beone_gudang_detail_gudang_detail_id_seq', 6196, true);
            public       postgres    false    214            �           0    0    beone_gudang_gudang_id_seq    SEQUENCE SET     H   SELECT pg_catalog.setval('public.beone_gudang_gudang_id_seq', 6, true);
            public       postgres    false    215            �           0    0 4   beone_header_pemakaian_bahan_pemakaian_header_id_seq    SEQUENCE SET     c   SELECT pg_catalog.setval('public.beone_header_pemakaian_bahan_pemakaian_header_id_seq', 39, true);
            public       postgres    false    217            �           0    0 *   beone_hutang_piutang_hutang_piutang_id_seq    SEQUENCE SET     Z   SELECT pg_catalog.setval('public.beone_hutang_piutang_hutang_piutang_id_seq', 430, true);
            public       postgres    false    219            �           0    0 (   beone_import_detail_import_detail_id_seq    SEQUENCE SET     W   SELECT pg_catalog.setval('public.beone_import_detail_import_detail_id_seq', 36, true);
            public       postgres    false    221            �           0    0 (   beone_import_header_import_header_id_seq    SEQUENCE SET     W   SELECT pg_catalog.setval('public.beone_import_header_import_header_id_seq', 30, true);
            public       postgres    false    223            �           0    0 $   beone_inventory_intvent_trans_id_seq    SEQUENCE SET     U   SELECT pg_catalog.setval('public.beone_inventory_intvent_trans_id_seq', 1200, true);
            public       postgres    false    225            �           0    0    beone_item_item_id_seq    SEQUENCE SET     G   SELECT pg_catalog.setval('public.beone_item_item_id_seq', 6001, true);
            public       postgres    false    227            �           0    0     beone_item_type_item_type_id_seq    SEQUENCE SET     O   SELECT pg_catalog.setval('public.beone_item_type_item_type_id_seq', 16, true);
            public       postgres    false    229            �           0    0 "   beone_kode_trans_kode_trans_id_seq    SEQUENCE SET     Q   SELECT pg_catalog.setval('public.beone_kode_trans_kode_trans_id_seq', 25, true);
            public       postgres    false    231            �           0    0     beone_komposisi_komposisi_id_seq    SEQUENCE SET     O   SELECT pg_catalog.setval('public.beone_komposisi_komposisi_id_seq', 1, false);
            public       postgres    false    293            �           0    0 6   beone_konversi_stok_detail_konversi_stok_detail_id_seq    SEQUENCE SET     e   SELECT pg_catalog.setval('public.beone_konversi_stok_detail_konversi_stok_detail_id_seq', 1, false);
            public       postgres    false    297            �           0    0 6   beone_konversi_stok_header_konversi_stok_header_id_seq    SEQUENCE SET     e   SELECT pg_catalog.setval('public.beone_konversi_stok_header_konversi_stok_header_id_seq', 1, false);
            public       postgres    false    295            �           0    0    beone_log_log_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.beone_log_log_id_seq', 391, true);
            public       postgres    false    234            �           0    0 (   beone_opname_detail_opname_detail_id_seq    SEQUENCE SET     Y   SELECT pg_catalog.setval('public.beone_opname_detail_opname_detail_id_seq', 6037, true);
            public       postgres    false    236            �           0    0 (   beone_opname_header_opname_header_id_seq    SEQUENCE SET     W   SELECT pg_catalog.setval('public.beone_opname_header_opname_header_id_seq', 13, true);
            public       postgres    false    238            �           0    0     beone_peleburan_peleburan_id_seq    SEQUENCE SET     N   SELECT pg_catalog.setval('public.beone_peleburan_peleburan_id_seq', 6, true);
            public       postgres    false    240            �           0    0 4   beone_pemakaian_bahan_detail_pemakaian_detail_id_seq    SEQUENCE SET     c   SELECT pg_catalog.setval('public.beone_pemakaian_bahan_detail_pemakaian_detail_id_seq', 56, true);
            public       postgres    false    242            �           0    0     beone_pm_detail_pm_detail_id_seq    SEQUENCE SET     N   SELECT pg_catalog.setval('public.beone_pm_detail_pm_detail_id_seq', 1, true);
            public       postgres    false    291            �           0    0     beone_pm_header_pm_header_id_seq    SEQUENCE SET     O   SELECT pg_catalog.setval('public.beone_pm_header_pm_header_id_seq', 14, true);
            public       postgres    false    244            �           0    0 -   beone_po_import_detail_purchase_detail_id_seq    SEQUENCE SET     ]   SELECT pg_catalog.setval('public.beone_po_import_detail_purchase_detail_id_seq', 100, true);
            public       postgres    false    246            �           0    0 -   beone_po_import_header_purchase_header_id_seq    SEQUENCE SET     \   SELECT pg_catalog.setval('public.beone_po_import_header_purchase_header_id_seq', 98, true);
            public       postgres    false    248            �           0    0 ,   beone_produksi_detail_produksi_detail_id_seq    SEQUENCE SET     [   SELECT pg_catalog.setval('public.beone_produksi_detail_produksi_detail_id_seq', 50, true);
            public       postgres    false    250            �           0    0 ,   beone_produksi_header_produksi_header_id_seq    SEQUENCE SET     [   SELECT pg_catalog.setval('public.beone_produksi_header_produksi_header_id_seq', 51, true);
            public       postgres    false    252            �           0    0 ,   beone_purchase_detail_purchase_detail_id_seq    SEQUENCE SET     [   SELECT pg_catalog.setval('public.beone_purchase_detail_purchase_detail_id_seq', 96, true);
            public       postgres    false    254            �           0    0 ,   beone_purchase_header_purchase_header_id_seq    SEQUENCE SET     [   SELECT pg_catalog.setval('public.beone_purchase_header_purchase_header_id_seq', 65, true);
            public       postgres    false    256            �           0    0 %   beone_received_import_received_id_seq    SEQUENCE SET     U   SELECT pg_catalog.setval('public.beone_received_import_received_id_seq', 220, true);
            public       postgres    false    258            �           0    0    beone_role_role_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.beone_role_role_id_seq', 6, true);
            public       postgres    false    260            �           0    0    beone_role_user_role_id_seq    SEQUENCE SET     I   SELECT pg_catalog.setval('public.beone_role_user_role_id_seq', 3, true);
            public       postgres    false    262            �           0    0 &   beone_sales_detail_sales_detail_id_seq    SEQUENCE SET     U   SELECT pg_catalog.setval('public.beone_sales_detail_sales_detail_id_seq', 25, true);
            public       postgres    false    264            �           0    0 &   beone_sales_header_sales_header_id_seq    SEQUENCE SET     U   SELECT pg_catalog.setval('public.beone_sales_header_sales_header_id_seq', 19, true);
            public       postgres    false    266            �           0    0    beone_satuan_item_satuan_id_seq    SEQUENCE SET     M   SELECT pg_catalog.setval('public.beone_satuan_item_satuan_id_seq', 3, true);
            public       postgres    false    268            �           0    0 .   beone_subkon_in_detail_subkon_in_detail_id_seq    SEQUENCE SET     \   SELECT pg_catalog.setval('public.beone_subkon_in_detail_subkon_in_detail_id_seq', 8, true);
            public       postgres    false    270            �           0    0 '   beone_subkon_in_header_subkon_in_id_seq    SEQUENCE SET     U   SELECT pg_catalog.setval('public.beone_subkon_in_header_subkon_in_id_seq', 4, true);
            public       postgres    false    272            �           0    0 0   beone_subkon_out_detail_subkon_out_detail_id_seq    SEQUENCE SET     _   SELECT pg_catalog.setval('public.beone_subkon_out_detail_subkon_out_detail_id_seq', 37, true);
            public       postgres    false    274            �           0    0 )   beone_subkon_out_header_subkon_out_id_seq    SEQUENCE SET     X   SELECT pg_catalog.setval('public.beone_subkon_out_header_subkon_out_id_seq', 18, true);
            public       postgres    false    276            �           0    0    beone_tipe_coa_tipe_coa_id_seq    SEQUENCE SET     M   SELECT pg_catalog.setval('public.beone_tipe_coa_tipe_coa_id_seq', 1, false);
            public       postgres    false    278            �           0    0 .   beone_tipe_coa_transaksi_tipe_coa_trans_id_seq    SEQUENCE SET     ]   SELECT pg_catalog.setval('public.beone_tipe_coa_transaksi_tipe_coa_trans_id_seq', 20, true);
            public       postgres    false    280            �           0    0 8   beone_transfer_stock_detail_transfer_stock_detail_id_seq    SEQUENCE SET     h   SELECT pg_catalog.setval('public.beone_transfer_stock_detail_transfer_stock_detail_id_seq', 644, true);
            public       postgres    false    283            �           0    0 *   beone_transfer_stock_transfer_stock_id_seq    SEQUENCE SET     Z   SELECT pg_catalog.setval('public.beone_transfer_stock_transfer_stock_id_seq', 294, true);
            public       postgres    false    284            �           0    0    beone_user_user_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.beone_user_user_id_seq', 22, true);
            public       postgres    false    286            �           0    0 *   beone_voucher_detail_voucher_detail_id_seq    SEQUENCE SET     Z   SELECT pg_catalog.setval('public.beone_voucher_detail_voucher_detail_id_seq', 215, true);
            public       postgres    false    288            �           0    0 *   beone_voucher_header_voucher_header_id_seq    SEQUENCE SET     Z   SELECT pg_catalog.setval('public.beone_voucher_header_voucher_header_id_seq', 142, true);
            public       postgres    false    290            &           2606    17094 &   beone_adjustment beone_adjustment_pkey 
   CONSTRAINT     o   ALTER TABLE ONLY public.beone_adjustment
    ADD CONSTRAINT beone_adjustment_pkey PRIMARY KEY (adjustment_id);
 P   ALTER TABLE ONLY public.beone_adjustment DROP CONSTRAINT beone_adjustment_pkey;
       public         postgres    false    196            *           2606    17096 &   beone_coa_jurnal beone_coa_jurnal_pkey 
   CONSTRAINT     o   ALTER TABLE ONLY public.beone_coa_jurnal
    ADD CONSTRAINT beone_coa_jurnal_pkey PRIMARY KEY (coa_jurnal_id);
 P   ALTER TABLE ONLY public.beone_coa_jurnal DROP CONSTRAINT beone_coa_jurnal_pkey;
       public         postgres    false    200            (           2606    17098    beone_coa beone_coa_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.beone_coa
    ADD CONSTRAINT beone_coa_pkey PRIMARY KEY (coa_id);
 B   ALTER TABLE ONLY public.beone_coa DROP CONSTRAINT beone_coa_pkey;
       public         postgres    false    198            ,           2606    17100     beone_country beone_country_pkey 
   CONSTRAINT     f   ALTER TABLE ONLY public.beone_country
    ADD CONSTRAINT beone_country_pkey PRIMARY KEY (country_id);
 J   ALTER TABLE ONLY public.beone_country DROP CONSTRAINT beone_country_pkey;
       public         postgres    false    202            .           2606    17102 ,   beone_export_detail beone_export_detail_pkey 
   CONSTRAINT     x   ALTER TABLE ONLY public.beone_export_detail
    ADD CONSTRAINT beone_export_detail_pkey PRIMARY KEY (export_detail_id);
 V   ALTER TABLE ONLY public.beone_export_detail DROP CONSTRAINT beone_export_detail_pkey;
       public         postgres    false    206            0           2606    17104    beone_gl beone_gl_pkey 
   CONSTRAINT     W   ALTER TABLE ONLY public.beone_gl
    ADD CONSTRAINT beone_gl_pkey PRIMARY KEY (gl_id);
 @   ALTER TABLE ONLY public.beone_gl DROP CONSTRAINT beone_gl_pkey;
       public         postgres    false    210            4           2606    17106 ,   beone_gudang_detail beone_gudang_detail_pkey 
   CONSTRAINT     x   ALTER TABLE ONLY public.beone_gudang_detail
    ADD CONSTRAINT beone_gudang_detail_pkey PRIMARY KEY (gudang_detail_id);
 V   ALTER TABLE ONLY public.beone_gudang_detail DROP CONSTRAINT beone_gudang_detail_pkey;
       public         postgres    false    213            2           2606    17108    beone_gudang beone_gudang_pkey 
   CONSTRAINT     c   ALTER TABLE ONLY public.beone_gudang
    ADD CONSTRAINT beone_gudang_pkey PRIMARY KEY (gudang_id);
 H   ALTER TABLE ONLY public.beone_gudang DROP CONSTRAINT beone_gudang_pkey;
       public         postgres    false    212            6           2606    17110 >   beone_pemakaian_bahan_header beone_header_pemakaian_bahan_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.beone_pemakaian_bahan_header
    ADD CONSTRAINT beone_header_pemakaian_bahan_pkey PRIMARY KEY (pemakaian_header_id);
 h   ALTER TABLE ONLY public.beone_pemakaian_bahan_header DROP CONSTRAINT beone_header_pemakaian_bahan_pkey;
       public         postgres    false    216            8           2606    17112 .   beone_hutang_piutang beone_hutang_piutang_pkey 
   CONSTRAINT     {   ALTER TABLE ONLY public.beone_hutang_piutang
    ADD CONSTRAINT beone_hutang_piutang_pkey PRIMARY KEY (hutang_piutang_id);
 X   ALTER TABLE ONLY public.beone_hutang_piutang DROP CONSTRAINT beone_hutang_piutang_pkey;
       public         postgres    false    218            :           2606    17114 ,   beone_import_detail beone_import_detail_pkey 
   CONSTRAINT     x   ALTER TABLE ONLY public.beone_import_detail
    ADD CONSTRAINT beone_import_detail_pkey PRIMARY KEY (import_detail_id);
 V   ALTER TABLE ONLY public.beone_import_detail DROP CONSTRAINT beone_import_detail_pkey;
       public         postgres    false    220            <           2606    17116 ,   beone_import_header beone_import_header_pkey 
   CONSTRAINT     x   ALTER TABLE ONLY public.beone_import_header
    ADD CONSTRAINT beone_import_header_pkey PRIMARY KEY (import_header_id);
 V   ALTER TABLE ONLY public.beone_import_header DROP CONSTRAINT beone_import_header_pkey;
       public         postgres    false    222            >           2606    17118 $   beone_inventory beone_inventory_pkey 
   CONSTRAINT     p   ALTER TABLE ONLY public.beone_inventory
    ADD CONSTRAINT beone_inventory_pkey PRIMARY KEY (intvent_trans_id);
 N   ALTER TABLE ONLY public.beone_inventory DROP CONSTRAINT beone_inventory_pkey;
       public         postgres    false    224            @           2606    17120    beone_item beone_item_pkey 
   CONSTRAINT     ]   ALTER TABLE ONLY public.beone_item
    ADD CONSTRAINT beone_item_pkey PRIMARY KEY (item_id);
 D   ALTER TABLE ONLY public.beone_item DROP CONSTRAINT beone_item_pkey;
       public         postgres    false    226            B           2606    17122 $   beone_item_type beone_item_type_pkey 
   CONSTRAINT     l   ALTER TABLE ONLY public.beone_item_type
    ADD CONSTRAINT beone_item_type_pkey PRIMARY KEY (item_type_id);
 N   ALTER TABLE ONLY public.beone_item_type DROP CONSTRAINT beone_item_type_pkey;
       public         postgres    false    228            D           2606    17124 &   beone_kode_trans beone_kode_trans_pkey 
   CONSTRAINT     o   ALTER TABLE ONLY public.beone_kode_trans
    ADD CONSTRAINT beone_kode_trans_pkey PRIMARY KEY (kode_trans_id);
 P   ALTER TABLE ONLY public.beone_kode_trans DROP CONSTRAINT beone_kode_trans_pkey;
       public         postgres    false    230            �           2606    17202 $   beone_komposisi beone_komposisi_pkey 
   CONSTRAINT     l   ALTER TABLE ONLY public.beone_komposisi
    ADD CONSTRAINT beone_komposisi_pkey PRIMARY KEY (komposisi_id);
 N   ALTER TABLE ONLY public.beone_komposisi DROP CONSTRAINT beone_komposisi_pkey;
       public         postgres    false    294            �           2606    17218 :   beone_konversi_stok_detail beone_konversi_stok_detail_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.beone_konversi_stok_detail
    ADD CONSTRAINT beone_konversi_stok_detail_pkey PRIMARY KEY (konversi_stok_detail_id);
 d   ALTER TABLE ONLY public.beone_konversi_stok_detail DROP CONSTRAINT beone_konversi_stok_detail_pkey;
       public         postgres    false    298            �           2606    17210 :   beone_konversi_stok_header beone_konversi_stok_header_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.beone_konversi_stok_header
    ADD CONSTRAINT beone_konversi_stok_header_pkey PRIMARY KEY (konversi_stok_header_id);
 d   ALTER TABLE ONLY public.beone_konversi_stok_header DROP CONSTRAINT beone_konversi_stok_header_pkey;
       public         postgres    false    296            F           2606    17126    beone_log beone_log_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.beone_log
    ADD CONSTRAINT beone_log_pkey PRIMARY KEY (log_id);
 B   ALTER TABLE ONLY public.beone_log DROP CONSTRAINT beone_log_pkey;
       public         postgres    false    233            H           2606    17128 ,   beone_opname_detail beone_opname_detail_pkey 
   CONSTRAINT     x   ALTER TABLE ONLY public.beone_opname_detail
    ADD CONSTRAINT beone_opname_detail_pkey PRIMARY KEY (opname_detail_id);
 V   ALTER TABLE ONLY public.beone_opname_detail DROP CONSTRAINT beone_opname_detail_pkey;
       public         postgres    false    235            J           2606    17130 ,   beone_opname_header beone_opname_header_pkey 
   CONSTRAINT     x   ALTER TABLE ONLY public.beone_opname_header
    ADD CONSTRAINT beone_opname_header_pkey PRIMARY KEY (opname_header_id);
 V   ALTER TABLE ONLY public.beone_opname_header DROP CONSTRAINT beone_opname_header_pkey;
       public         postgres    false    237            L           2606    17132 $   beone_peleburan beone_peleburan_pkey 
   CONSTRAINT     l   ALTER TABLE ONLY public.beone_peleburan
    ADD CONSTRAINT beone_peleburan_pkey PRIMARY KEY (peleburan_id);
 N   ALTER TABLE ONLY public.beone_peleburan DROP CONSTRAINT beone_peleburan_pkey;
       public         postgres    false    239            N           2606    17134 >   beone_pemakaian_bahan_detail beone_pemakaian_bahan_detail_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.beone_pemakaian_bahan_detail
    ADD CONSTRAINT beone_pemakaian_bahan_detail_pkey PRIMARY KEY (pemakaian_detail_id);
 h   ALTER TABLE ONLY public.beone_pemakaian_bahan_detail DROP CONSTRAINT beone_pemakaian_bahan_detail_pkey;
       public         postgres    false    241            ~           2606    17192 $   beone_pm_detail beone_pm_detail_pkey 
   CONSTRAINT     l   ALTER TABLE ONLY public.beone_pm_detail
    ADD CONSTRAINT beone_pm_detail_pkey PRIMARY KEY (pm_detail_id);
 N   ALTER TABLE ONLY public.beone_pm_detail DROP CONSTRAINT beone_pm_detail_pkey;
       public         postgres    false    292            P           2606    17136 $   beone_pm_header beone_pm_header_pkey 
   CONSTRAINT     l   ALTER TABLE ONLY public.beone_pm_header
    ADD CONSTRAINT beone_pm_header_pkey PRIMARY KEY (pm_header_id);
 N   ALTER TABLE ONLY public.beone_pm_header DROP CONSTRAINT beone_pm_header_pkey;
       public         postgres    false    243            R           2606    17138 2   beone_po_import_detail beone_po_import_detail_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.beone_po_import_detail
    ADD CONSTRAINT beone_po_import_detail_pkey PRIMARY KEY (purchase_detail_id);
 \   ALTER TABLE ONLY public.beone_po_import_detail DROP CONSTRAINT beone_po_import_detail_pkey;
       public         postgres    false    245            T           2606    17140 2   beone_po_import_header beone_po_import_header_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.beone_po_import_header
    ADD CONSTRAINT beone_po_import_header_pkey PRIMARY KEY (purchase_header_id);
 \   ALTER TABLE ONLY public.beone_po_import_header DROP CONSTRAINT beone_po_import_header_pkey;
       public         postgres    false    247            V           2606    17142 0   beone_produksi_detail beone_produksi_detail_pkey 
   CONSTRAINT     ~   ALTER TABLE ONLY public.beone_produksi_detail
    ADD CONSTRAINT beone_produksi_detail_pkey PRIMARY KEY (produksi_detail_id);
 Z   ALTER TABLE ONLY public.beone_produksi_detail DROP CONSTRAINT beone_produksi_detail_pkey;
       public         postgres    false    249            X           2606    17144 0   beone_produksi_header beone_produksi_header_pkey 
   CONSTRAINT     ~   ALTER TABLE ONLY public.beone_produksi_header
    ADD CONSTRAINT beone_produksi_header_pkey PRIMARY KEY (produksi_header_id);
 Z   ALTER TABLE ONLY public.beone_produksi_header DROP CONSTRAINT beone_produksi_header_pkey;
       public         postgres    false    251            Z           2606    17146 0   beone_purchase_detail beone_purchase_detail_pkey 
   CONSTRAINT     ~   ALTER TABLE ONLY public.beone_purchase_detail
    ADD CONSTRAINT beone_purchase_detail_pkey PRIMARY KEY (purchase_detail_id);
 Z   ALTER TABLE ONLY public.beone_purchase_detail DROP CONSTRAINT beone_purchase_detail_pkey;
       public         postgres    false    253            \           2606    17148 0   beone_purchase_header beone_purchase_header_pkey 
   CONSTRAINT     ~   ALTER TABLE ONLY public.beone_purchase_header
    ADD CONSTRAINT beone_purchase_header_pkey PRIMARY KEY (purchase_header_id);
 Z   ALTER TABLE ONLY public.beone_purchase_header DROP CONSTRAINT beone_purchase_header_pkey;
       public         postgres    false    255            ^           2606    17150 0   beone_received_import beone_received_import_pkey 
   CONSTRAINT     w   ALTER TABLE ONLY public.beone_received_import
    ADD CONSTRAINT beone_received_import_pkey PRIMARY KEY (received_id);
 Z   ALTER TABLE ONLY public.beone_received_import DROP CONSTRAINT beone_received_import_pkey;
       public         postgres    false    257            `           2606    17152    beone_role beone_role_pkey 
   CONSTRAINT     ]   ALTER TABLE ONLY public.beone_role
    ADD CONSTRAINT beone_role_pkey PRIMARY KEY (role_id);
 D   ALTER TABLE ONLY public.beone_role DROP CONSTRAINT beone_role_pkey;
       public         postgres    false    259            b           2606    17154 $   beone_role_user beone_role_user_pkey 
   CONSTRAINT     g   ALTER TABLE ONLY public.beone_role_user
    ADD CONSTRAINT beone_role_user_pkey PRIMARY KEY (role_id);
 N   ALTER TABLE ONLY public.beone_role_user DROP CONSTRAINT beone_role_user_pkey;
       public         postgres    false    261            d           2606    17156 *   beone_sales_detail beone_sales_detail_pkey 
   CONSTRAINT     u   ALTER TABLE ONLY public.beone_sales_detail
    ADD CONSTRAINT beone_sales_detail_pkey PRIMARY KEY (sales_detail_id);
 T   ALTER TABLE ONLY public.beone_sales_detail DROP CONSTRAINT beone_sales_detail_pkey;
       public         postgres    false    263            f           2606    17158 *   beone_sales_header beone_sales_header_pkey 
   CONSTRAINT     u   ALTER TABLE ONLY public.beone_sales_header
    ADD CONSTRAINT beone_sales_header_pkey PRIMARY KEY (sales_header_id);
 T   ALTER TABLE ONLY public.beone_sales_header DROP CONSTRAINT beone_sales_header_pkey;
       public         postgres    false    265            h           2606    17160 (   beone_satuan_item beone_satuan_item_pkey 
   CONSTRAINT     m   ALTER TABLE ONLY public.beone_satuan_item
    ADD CONSTRAINT beone_satuan_item_pkey PRIMARY KEY (satuan_id);
 R   ALTER TABLE ONLY public.beone_satuan_item DROP CONSTRAINT beone_satuan_item_pkey;
       public         postgres    false    267            j           2606    17162 2   beone_subkon_in_detail beone_subkon_in_detail_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.beone_subkon_in_detail
    ADD CONSTRAINT beone_subkon_in_detail_pkey PRIMARY KEY (subkon_in_detail_id);
 \   ALTER TABLE ONLY public.beone_subkon_in_detail DROP CONSTRAINT beone_subkon_in_detail_pkey;
       public         postgres    false    269            l           2606    17164 2   beone_subkon_in_header beone_subkon_in_header_pkey 
   CONSTRAINT     z   ALTER TABLE ONLY public.beone_subkon_in_header
    ADD CONSTRAINT beone_subkon_in_header_pkey PRIMARY KEY (subkon_in_id);
 \   ALTER TABLE ONLY public.beone_subkon_in_header DROP CONSTRAINT beone_subkon_in_header_pkey;
       public         postgres    false    271            n           2606    17166 4   beone_subkon_out_detail beone_subkon_out_detail_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.beone_subkon_out_detail
    ADD CONSTRAINT beone_subkon_out_detail_pkey PRIMARY KEY (subkon_out_detail_id);
 ^   ALTER TABLE ONLY public.beone_subkon_out_detail DROP CONSTRAINT beone_subkon_out_detail_pkey;
       public         postgres    false    273            p           2606    17168 4   beone_subkon_out_header beone_subkon_out_header_pkey 
   CONSTRAINT     }   ALTER TABLE ONLY public.beone_subkon_out_header
    ADD CONSTRAINT beone_subkon_out_header_pkey PRIMARY KEY (subkon_out_id);
 ^   ALTER TABLE ONLY public.beone_subkon_out_header DROP CONSTRAINT beone_subkon_out_header_pkey;
       public         postgres    false    275            r           2606    17170 "   beone_tipe_coa beone_tipe_coa_pkey 
   CONSTRAINT     i   ALTER TABLE ONLY public.beone_tipe_coa
    ADD CONSTRAINT beone_tipe_coa_pkey PRIMARY KEY (tipe_coa_id);
 L   ALTER TABLE ONLY public.beone_tipe_coa DROP CONSTRAINT beone_tipe_coa_pkey;
       public         postgres    false    277            v           2606    17172 <   beone_transfer_stock_detail beone_transfer_stock_detail_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.beone_transfer_stock_detail
    ADD CONSTRAINT beone_transfer_stock_detail_pkey PRIMARY KEY (transfer_stock_detail_id);
 f   ALTER TABLE ONLY public.beone_transfer_stock_detail DROP CONSTRAINT beone_transfer_stock_detail_pkey;
       public         postgres    false    282            t           2606    17174 .   beone_transfer_stock beone_transfer_stock_pkey 
   CONSTRAINT     {   ALTER TABLE ONLY public.beone_transfer_stock
    ADD CONSTRAINT beone_transfer_stock_pkey PRIMARY KEY (transfer_stock_id);
 X   ALTER TABLE ONLY public.beone_transfer_stock DROP CONSTRAINT beone_transfer_stock_pkey;
       public         postgres    false    281            x           2606    17176    beone_user beone_user_pkey 
   CONSTRAINT     ]   ALTER TABLE ONLY public.beone_user
    ADD CONSTRAINT beone_user_pkey PRIMARY KEY (user_id);
 D   ALTER TABLE ONLY public.beone_user DROP CONSTRAINT beone_user_pkey;
       public         postgres    false    285            z           2606    17178 .   beone_voucher_detail beone_voucher_detail_pkey 
   CONSTRAINT     {   ALTER TABLE ONLY public.beone_voucher_detail
    ADD CONSTRAINT beone_voucher_detail_pkey PRIMARY KEY (voucher_detail_id);
 X   ALTER TABLE ONLY public.beone_voucher_detail DROP CONSTRAINT beone_voucher_detail_pkey;
       public         postgres    false    287            |           2606    17180 .   beone_voucher_header beone_voucher_header_pkey 
   CONSTRAINT     {   ALTER TABLE ONLY public.beone_voucher_header
    ADD CONSTRAINT beone_voucher_header_pkey PRIMARY KEY (voucher_header_id);
 X   ALTER TABLE ONLY public.beone_voucher_header DROP CONSTRAINT beone_voucher_header_pkey;
       public         postgres    false    289            �      x������ � �          �	  x�}Yێ�8}��B�AH<�|�ۚn�,��e����cI��Dw�O��S,֩*٤h�T��Ɏ!�Z��
RT%D��*�Ͻ �m��<"�z����@Yq�}[��,�4���f�P�\9��G�H�T.�=��#��U
�?2E �ͷ�����;P鶦�$�4���u�G�H�)��ܰ,3��PN��y�i��~ٹ�����=Ϙm�-�E�f����쇋�H�A4%\ɍ�W�V�}r1�������}`��*D]H����ܗG�>94��yx���X�ai�ǩ�7���{p���Lv�	e�b�1h�E���a��g���N&["U%A�)�2�fxj��= %�O���"*�n�s��^��iǺ��\���l�T*(C�"�|�&^���om������DQ�U3l��R+��g���Z*�MHx�s�у���i�M�!��Fɔ@����nä6�BW>�a��`D��ؽ4D]�nw�V��i��C?���&��#Z+�2��uuԔ�����-�z��y��fTڍ�b�(Ğ����������B���
%��$��Ͼ�촄PI*R�*%�5�.�"�����}�p��xVe��V�|��������ew���ޒ�2C�6|+��ڴ �Bя��05役�7����+:�9T�E�Pu1տ�K�^���ϕ�F�
O,�Hߗ��LӳAW?#�nhuaU�����َ��{�患��<�R_3T�<��ʮ�vrXKQdrg�̪�;*d�+����.v��RkOAI�n�f�E}������A�����.}-ֳa'��r��mwu��k�\�I�C���@QF��쪩�Cl��l�l�[z:.��(W��ʪ��x;Ȟv���<���~ph14��!f��`Ig��D�D����l9�z�^�`�PmUM47hTWNxo�W�-�����^���~��0�z]ؓV�Ae���%a�~`^���҉�(.����<��i񑶨��b4NZt��ZA_�兯0���pv�7�s��|ɐ���{b*����FIㄵ�E�X�S��&��L�c����N��ے��w�� �N��T�g)��R]���f�Ǧ�Y�f���z��`�q��E�pR+_�k��}l�,PX�,�MV>���13��Hᑢ9E�`Pɱ������sqi�<uO#��X��S�;?]�~d[םh��)6�Re��a.�,v��(o���&�@H**��}]�����9{$�m��a	�"¹'�\W���V�1��X��WJ����Ѯ���U�N]t��t��,ۜ"�q��}��0����??���K�n������3��Й�>,`�_����$(᮫Q���֪�$	6��S'($��;2lh�6�3��նH�X����[��d\��ɴ�ￖ�-�����W��0嗑D_�DB:��*˨�z}���@��3�*�Cn:�g�c�Զ/�^y����x�H\/�hF2���7GN�g��ʿ7cӥ[TeP/���H�O<��hZ�r��l����vv���﯁	>۩�A���a�൸�Et��E�XFwÌFB���rW�y�?~4��)�+��J�:$�XWJ�!��_70�7����\�Y��]WL���O��z��n�,�5=�ֶ��,�,��#U���ٓ^�Y����Y`Vy]}Z�ߡ��*zo1k����<�SB��M��O/���H�R�T��j�^�`�+n�O�j|Uy����REF��	��z=��P�~@�P��P�n�7Yg�3L�%ׂ�<�'�de{��j��c�Xyƪշ="V��:I����A�$�o�B�����[��'(��>/���M�Ns�F�ɰ�^���iH���3�N�����~��n�QX�ʾ�pB�س����4�r��柪6�`�Q����\�¨�sş��ԍ����Æ�-4H�G���|@#�>��!Ag	�p���t�~�]�ʹ�
���n3��=l�C7���L;�j��??����L.��Wp;���H�N3y��dW?Fw�M/�'�ʿG&����Q2���cBy��hI�I�?ݼ��mz���)�p��z���gCe��W��}q1�C7�M�04S�woK�IF�T�^�IUN�X�,�ν��n��3�܃�FtgC��e�hԽ�`�}�e*!�a��F#.��t=��{M�7������2e�:*ޙ6x����<��/��yx�_�Gܬ�קn���b� �6�=B��0wf��u�y�Tt�ќw�{?<�i,�eS-ӄ�W^n�s-�jڱ����#P*@�q4,�4��n%I󓋰�X���_2-�=鋳˽�����aD?�yu��"�	�� ��
GrQ�G����#�E{s���g8s^7�;C!�K���/��Zۻ#:ޛ�X���s��(�e�"�e@��?3��/4"If$_��%��Hǹ��E��_�<�<	�Q\g�0�m����	�������i��           x�}RKn�0]�O1'@�K����c�-�j��9j��R	y1�7�&���+���tuw��rl|�����9(r)I�D����7��4GxY@N��(h������S�Q�H5�Ӎ�n�@��3j�8E�wg��t��tq�S���;���%����g���4T��)�/��2o���ޢ|R��pſ@�ܕJPP�C]i���C�X�����2�U�����V!�>����>��*�|��\n\��W�tZ|��DN�b`�k��D�e��_         >   x�3���quQqq��4�2�t
r����t�t��9=�\��\���.@�=... �pd            x��\�n�H�}���|�� J�E+�S�L�)��Ų
�E��ؚ*[/����{"�Z�J���Q�,��Eĉk೰Y�&�c�Tk^�jg3Y��-��G<	s��y!��L�<��<��k%�\d�Ï�����A��76B���O|U\��JYs�7<,�K���=O�0k>�U!"g?�ʣ�n*%R^�*q�H�yl݊�ױ�Y�f�W]$_Ȭ,h���Z��2ĐE���3O����h$-�s=���"Eo�ơ{��0V����,z:���s�mR��I���E��x*r���P���c��q6z�Q����2��"-�)m�Lػ%`^�`]��a�t�t����_���^��vs�O������j��ux��6O��c�ģݓe�{�8ƛy��EѬ��xYQ6�u=UbQh��Y-�W�,� ����Z}�]�\.U��a\���w�ʬ�'z�TZ��>$N�
5���UU7{�ZsQI�A�gE<Ъ�s`�=�'�Ӝ�N*�*^E�����Z��`WV��<�
�L�03��U�W�ń���BV�f�#ict<aIg[V��S�J��}/�Z����T�]VE}'k܅U�i�a���$��$?�jdļ��3Ej�"��+�M�Sг�tb��LM��q�;����1\țb�d윗�ۑ�"���v"�E�ժ4Zc�e}���R`Z�|�#hI�t����i��'��w=�^+sh�J#�
4��D�J���v�7�KY�8�J��,U�mN�dN|��ns�&���Z��ލ�e2:������G5Q�x%�܃-�x�琢m��q��C����>��cR�u+�tE���hF�|�g�n4d�d�����84�>�&��Z���s��@R�W���m,�8b�v���7�hLŦ"kZɊ�$QT� �>�ȸ���U�}����V��9p,%�<�)���4Ի��b�#�@xj�F�b5���[m�_���3��d�7�I8����0�JC{�X5�_��ߨH����zg+�����.���� ��C��&����*�8�D���P�܂���2�.�"������h<�>�D�Ȥ���!��F,��� C�s������O�	X<`�����E�ߏa�o��02}�?ÄGe0�}l�#�(
����"_[/c��:�2���n��Z&�:�*A�U
�\��6���yCW[�2��i/<.Q+N��'��Dqr1�ط�=q�-JQ����p�����V6�pw�7�a"+͑�ݵ֛|�Z ����ܓ �� I`*5/�sJ�,q��e��	� �"$�&S�k�\P�B�uEvd�^H"�Q��F<ĹYL���U��5%��CN0|@��M8�N۱.l���{R�c��]�J�5AL0����M��;��w�皜�B�Gǁ� 59�� Ck0�T�Rb*oU��iQT|�p�F&�d�tM�@+�2�C�K���fy��"I.b҆��˫����^��q�;l';u���c�����u�7����Tj�:6:V��A��]��@e�6�'����������|'�f�!�
�k�ef�}�FͧـQm��=/���<-�����FA �N�Ԃ�TGu��l�h�_D�*�C:��sA&c:d%����I&�E����CO�D\��m��k�e	��l��c�i��#!�(8
$��JV��/��F�+{�33�(N^�nTǬ��G����)���x�g�U��sR�l3N�R����,C�8��rO�s=Ǥ1���p L�Y�j��	1��Lx:Ŕu��W�9"�_в�C��U>�_�,z�bo�n�a`��Yy�{��?�:�ǖm���15�m��>����6h�v5˔8U$h`	�J���B�ϮG@�d8l�YnD��$p����\B�y�%�xH�6J� <Ǭ	���u	�j���;8�_����l�����NR"gp�3Ց��<��{BU�J�Ay��7n3�ԭV>��&&AB2�a���sGl	�P���l�-��7�y:��;:c�M$H��@�.Iex�A�:y�RR��7�J�	<�6M���N��;�y�<����-�6nx��m����3/����v��]2�����uX(M ����N��[=�%r���	�>�/O���e�0IG)�y�1Q�,��H��`��^��$j��M�DT:��5�����T�6`	���/bp�e��F�8i���1�$,T^�m������g2�͗"L�1��F�>x���"�b��]�==o�!T}2"D1��ƈ�o N'S��/1����+��T����}��+"�
H'3Y�MɌT���	��|QT����XNֱ=&~mԗ��}�{���wk���}R���{�Zn��`@�wY���7^o��<�V��7����m��e5+?�簳�)*Vn^`���q����o\}�=�m""7k��Zا^�l���m˟7��[>��5dm��t@R��<�xZ�:��:�7����5^����f"�Z�Z�^�z�������U�F�<�n�p�[B5>�.cB�M%]�`�
iT�2ۼ�>o')O��ƛBG3�BP7W��8�t��ǯwϏo���]4f���6LN%L��s��6���\����!=2�M��C��1o_�^7Z�������+��u p�cį!� 3"�,��`��q�<p���i��Q�0�����g�ZG�X�Jr�ʻ�;쎫k��{�l}~���K�L�}�MaQDY������؟����y�㷗���)�L͂!�D�:�Q�珢�\8Ԉ5T���[�%47/n��H��)G�{��|0f5 $]�Ge������*-1�8��Z��9{������=n0�����|0@~�Ҧ4�ZiҦ)�V|�fj�><�ȁ��b�������o�Z�����e��Y�\;K���^�S�l���$�k��d�E~I%Ń���f�#�'��c�h���k�
�7Ā��1�{�?�J�F������ j>o�h�"�Z~�6��.�ǟ��	gǳH����=��L*��ŧ��� q��,��n��@�3Z�X�(�ȱ��Բ���Eb�2�]�>ͣ�����u���i�:Tо��-�r������
���O'��_E���P��.�Sp��fǽ�K�BI�^bZ�j�ZZi��;U�cޥ*����C�Ӯ����=��������x���xzy��k6�?�F=��iT�o����i�.���׹��7�_��n�+���Y��X����B(
h�mĪ"��3)Bo8�u�\��
�CEVJʧ�d{Jhyǯ�7�M�X]\˼�T:�����$S��6���X�mU�h6k_ޔĄIH�2�c�P����G�_X�,-0�|��$?�f��<F�~.~��wF�.��|�;�D�v�8?��g�>
X�4]SR6ө6V=�t}�<�e�؇�7��T^�}V��N�&E��aq4`��a�~R����4���Ӽ=��S�[��wϯ<ھ�:�Ь��ѐ2Θ���:mJ� _�A�˴j��x����礒ʥ�vI�Luu�����N8���
�ltzMQz�,���t��=r�N�����f˿�m���<!�B��_���?��9|�١(��"6E�f�P�Kޘw<�2���/Ֆ�ij��$5�'@5-��"n��N;%���X�iZ�G;��=�X���m������[����a���D�DE�Y�3�u|ƞ� ��������H[zS�I��/tuW7�6�X'����M	�n�� '�'�e0䡦�i���nUn���RQ���=�~�����e��E�	�%v�Z�������#�OҝOP{�!a���c6eO����S���doA�i`=:鼢��.���.���1��"PӮs�ț��q~E\�ȸ��Q��O�k��3�?���L����P2��1qA�V0��.��b��gtt�"�ӧ�1S��>Zp�T+:auU�g8���_�x5m�0ϻ�0W^>l^�ק����p����þգM�?�{����%5Ho2dm�#U�ε6�FQ�lvC2��D;r�����"�Lw�� H  e�4a`rӥc	�u�«=Q����}��Y�/B&S���+�[��?�kU�*<F���%]�=�@i�MZ��q�2���a��W�>��Z� �@�ǫ�d#���OGqպ C�}��q�|h���
9I�';"O}�n}vf�G�u	A�?Y���w: g�{I[ޥ_�e ���'z3Dkw�d:dkQD⤳�jO#.�M�#��V�
N-WJdc,K��%~(��C7�����:�o��l��N5�Tm�:S}�N Xi���-��d���ⴐ�ﴚy�)��Yo�[�,���˾HA.������'#���*�k]���(+U�uoa7M�G>���@��R��/�ꁊ%"�����j������� ����|WU�h�o���U��˪c&�H�l��ә�j�
��0DVݫ�`o�J�X51�ф�$����`���h�R�#>Z��cJ:T��5SJ!�ޡ����=��V�\Ո�!����&���ExScGH�6����t�g�:E�IrJ9�R�R�X��f
"��e����nD�"�ٗ3���]AM3@oڧ)�Xw��g�H��Bm~]�1�[�׀�ͷԈ�����W�����h����	��H�v�wy�w���^��p���`���0�ʷ��ֻo���e��@�2�8|V��y޽�R���3��^}�N6f�̬M��+���n̓&���ێ����-h�2���)�.�`�	��:�D��P4���L4�ʩ�`VTqQDu1]��V�P�g��=8������%Y:�%pY54�!�֥�O�!�>������r�wȄ_�M�ij��~ͨ��R�UY�F���l]�g����=�����)�!.�Gh:�{��W?���N�޿ިJ`�x~�i���&��~�wA��ج*t��ķ뼦�b��y�K)�K�P��`<�ޛ�u��6Ր̈́Jc~��w��׵*)z�@8��1uw�bk`r�S-�H��8���z��
�t@<z��\%��߿�};�����k����~aR���H�.LnX���K���]��9���	]�s�0�Qp7�92�c^T���4�$�a����#�NY��m |��E�ik8==��]�Ӭ�QPe�?�>f禾�fԘ��9�
ֵP͊Xs>�A])�J9�������5W仦	��=���ƅ�S0���"�qe�.�=߰�Eo�/�վ��~tg�IH�v�a��2}�>����U�A�\����!v��\�Sih�Ro����=B��ڦ~��	hv͌�=> �]���IΊs��z�i�)�l�W�O5�+����X�|��T�	*p��yP
��e��"�r�ϋZ7ͩ͞�C&�RtMR5FQ����@jgm�����?"{�J
�a�M����ؑK�Pg�.�:eA��#�C��4O4^@��ǧ�?<'�՚L�#��0����6����d{.џ0�Ȓ�M�P���Ԥ�y�O�9ӎ9�L����������D�M,��՗j;���85����ֹ��2p��G���=�e�j�.Im�]�;�Qg�t:Te	������l�y�<���0�sլ�'� k�e�*��=m��\����X���-$��wS���2����ࠃ��d�����C�uy���8�^:��-�5������G<)���w�DA"�k��2��[j'4��q'��C�"�ԧ�{毗����~��!u �ҍ��|���8���}5H(,\���F�Z;b)�d�~9 � ����(|����{Cu0e�N������&bi�2|�T70�y[��i�n��\VKj�6���lfc�fEݾ뜶��N������?, .S��{X����Y-�"S�ٽ�Ǩ���?�1�h�E�FΩt.(p�_��_~�u         s   x�����0Ϗb2�c�&� ��!�3����;KP�vH�X�dk��%�Ȯb̃t{�̗ȣE�xÚh�%hN���x��C����,I�q�C55�D� ���و�5�-;      
   �   x����
�0���S��鬹�RXǠ�S��9f���?���L #� ������P�@1�geTJg��[�*�Aӭ�]�@j�x��g�sC��KT��x	��cs�'��@i�>�����%��Q�{6����
�B)�T4>@3�c4�M:�����P�n>��L���E��*Ck�o���__O         v  x���M��0��ί��=@�c�G��ZD7%ڭ*��!�*�j���Wmd�|� ���f�F2nS0)Ƒq�g�ݗI���3����5mp%G��;4�)!s9� �����y*`��(���H�ȧh�g:�)i �0����䧉Ac�Zs�Vl�����A?1ל>�Ȝ�r��8\3�l��/ծL��E��<�W\�l�㦨�jF%���rT p�*y�B�mnr� ��e96��lQ?��z^�^�������e9_TՃQ�?�^���n����u�\�=ɟ��%۬�V��oŶ(�I�Z/�3�?����aU���!�t�F9��Q1�ح;���j��ܕ_�(xQ���y�)��wy!6PԀR��?z��"�0��o۞3�������O���Ԯ��?���4Rr��i:�Q@��h����̰���Ŀ�֠����1d��1�߉x�9��V~����$�Ѩ2aܞ)�6�!C�e��� �*t)2���"���vy�qg��3�&e� [����i���n���hv�&e����Sx�#h˰����I�sJx����/���IQqG��ޔ3!��Mk�!(�-��[�������f����Xy         �   x�m�K� D������'��`e���#T%m�����7��$Ƣ���^Kc��1�l�(<'�nN.6
E�N@�m���~��Jg���&G�&Yp��`��v�F.[$���\��4^�k����_��y ���Ec         �   x���An� ����.To�e�n���E���?GǡIJ�(�����M��$`��A����)�	`�)�P���ZH�`�w����D�a�\�'�F�Z��P�Y��V�iy?V��(��9�沟����}hc��1�6�\+^�|т �vi����᫿��x��zt��<����_�����)O�3ɲQ��|���4r�����~~��s?#&�ʸ��<�z�������         r   x�u�11k�/�vm'v�@�@���w܅C�����z���2D�Y���v�0R	2�v>^B�#WY��i!�����[�n�_7�̏�Hoy��tqwpp��'��VJ�&@            x������ � �            x������ � �         �  x��S;n�0�黈�?wK/E`$Ҥ��ϑ�(?��,$����ѣ@ T#� '`�V�����'���6|&P���@��
��4(X���81�09��ۨ@�����Bbִ�4�>h<�Bq e�~�l`5����S�ڌQ[�m�q��7���AG�
+qpm�����b ����|��H��c�V旷���z~�":.��0#j �H�#�E<З�~Ѓ3��*M���� ���i_�X�{�����-����LNKw���ݦ��stl�P��f\��N�=�󚨣��ʚv�m��U,ךnt��e��wW�"aO���UX�{�(����`�����!<��N�#�T�7V Z��*![��~��{hY��d�z�|׉�z:�~ ����            x���َ$ǚ&v�|� �dG��E�� 23r��%:#���]O��s�͞�i6F�ћ�B7� @���b�?�p����Ý�"X$��m�j�/��ݿ��?m��?��p�
���}�|2�[��Rs>��]w#�����W'��|��}��"
[~�����эR��ALb�������u����~�8�6���J�oN��tJsW��*Hc��h����e��M�����猢���������ϛO��E�����h�?���Ah�C��_z\��q�<n�<>�Gu^Hcw.���촉�j�wR\~hF��P����tg���:���aryv��i��t^;�:��W������HQ.�v�����NzhJ2x}�;5,�e(YA}y<?������I'������P�A�rKd(�F�������p �:�t�J��x2��Y0)�����d�H�ژ�:/�R;�t~����H':I��ʟ���񹥕��)㬶2``sq�0�R�Ӵ�D��xo�7+�ƨ>bM
�tQEE�H��R���m3J�Q**P���EZ��H��k�r�����r#]�����P�p;e̼�J����X2D���[
C�wv��o����[�[�h��-G�A��G�6^����?8"C5r�ݏJw_77O����}<mn��oO^��{"��2�蔍t�4�;�y���\EUST�P�f ��A�b���F��
�a��<�U�=� �$�ٍ�3[M,�[��D֝���A��4j����=���&��d�/�rMĔ��U �MJ�h���42ȝ�Ӄ��aGzϰ��6v��Yɝ��M$r4�c��4͐������y�:� JP�z:p�S��`�&���Sb�G�Ɠ8�43��t��Ti�e�4�6����A�7������@�G&.}EGz�I��f�!�*d��pIj��tXc?o�E���J�Ow��x#v^�pl��,^CB��%K��3D�;7@6��Bl�\c;��%��Ԇ,mj�G��Z��+&��!��&��p�y�F̡��Y�D�X�qrP��e�9�ڽ@���(+�&�0�qjQ�Q���]!�N�:,�
d��	�&�\D�!�@���s���Z���L��M�MT��#�d)�H�ZC
J��X�� I�4>}H&Z� �.�Q�d�1-�/R �.Y�.X��q�"�`�Cz��[bAZH�`e;ݐ��sH��&{H8�cI\�QҔ[&^lO�AC�H��I��`��L�V̡6�قY���~ 	FGݘ��W�9�l��cBg=M��]H3�&�44 �N3d�����Egn���)d�V&�!�NJ������c;��f$m%�!;��h�YngAa'��gL��o��"Gu��TV؉16C����Б�aŎ�+sF̦�I$��j���I��E���ۗ��a~<�o>�?���<^6�S���U�MC�ϑ���uƙ��ž�I���
@�b��%�v�8H�Avx�A�A��>D�(:~��7O��O��~
����4�Nw-���"��)1R[�����1�&���ꨒ�s�(6�ZZ]ԡ\���A��p!I�J�T���@za��dH����,;��dHh����8�� %��_�˿�������o?��!��_�z�����_Nյ���8RWo�g�����������li43̳���/i]�7���[��&����Ģn<�Q�J���sXfg�vg4�Ŏ��$C���B=!0�0趞������"g��>�g`q�D�6��A�n�'@�G����[)�킂Y��/i���9���$��JY�ϑ�$�C$Zu;;���,"�b��,}C��qr�OOO���v�!���U&I(E�3j\����������ZZ(���U]�����'Xs C�[��H�Þ����`A;�Ц\1��vo}1?�t��\t������8e�0l#�d�����H���_��hg���D�Ï[��nC��7#���	b�荳F�k����?����8�f��5X���l���a�O�c��<��c�4��K6�0�,�m�'�&������D�C�I�H�K��秗����]���+E�l���Ć!��,�����9i8P�?$����Nb��tӅ��A6/���˿����?���ÌQ���$O�+�q+j��$�G��l(ˇzyz~&�l�?��@��r9d8:��+�.-����英��pW�0�Յ�z�v��gC��'�e� ���t����(� ED][ցm�zw�tb�VW�p4�������P|�X4	��;;�F4F�~��r��@�T4L����X���(��!�F	U�A�B�+0
�x73���5̍gc��1��xRړԩ�Eo�2��U��2����Yv��Ɛx(O�H�X���am��z,X���yK��o`�b �)�pc1�ΒN�"sGv�1P��?��4���t<bQ���-/O���w;_B��"v'?q�Fr��k�T�D�Γ5W�@N�ViM�����]�>t�9W�E�:R"�T���
?����8.���+����N��8a�O/ /��������r�|ب��I���<6�~����ؽ�B�/��)�_Qx��&o`<fp8�������"*���̦�5x6���Q0�O�t��鞧�42���ઙR�D8�ԿS�q�#���NTAuO�)
��ȝ�9;�>��m�����"nO/ɣ�?�/�H|��DS�8I×>�����y�|����m��xN:���P;��"JG�K�s�n�6�2��8��H�
�`K|���d�����֌T˝4.P�.U���Ӂ�y�|4�f4F�i R��Db+ntx�&޳�'�4R����gc�=?���a2��B�c��HN?o�Q"����n^�������������2~�|9Į"V�B��N�R\%ɛ�_~ٿ�����&� �d���9�'wb*�p�j��.a��&{�AM6� �Ѻ�36�Z���5V���wI;7���CdHo���c��&���^)Q�dM�
�����%X��I;���[��^����3��*�3�}5RHPA �����%��|�Y�R�ˊR$�#Ry�}�RNY�5x���5+S���(7��i<K��g4�vr�$~V�4Fv]4dT�,Zy�c�tF�X�5+Bʕ�Q{�xx��Ɓ*mP�F�3�K.@Ԭ y?<��0U��P(�送|$=&1��Z��jVp��^�hf�B�;I{�oVǄ%�Y!1�V�B��E��2)�-c�N�QA�Q�K��X��6Ź@�
(/&@u֓ek���=��b@s	���/=�%8Ҫ�6�,M�+́�Y̝��Ƿ��V��01%�͔�H~�7(����EZ)� ���D�&�3��>iF�)_�萄A��p�&�BY��Bw����y�����z�!1���Od?+�Ƨǻ���]�`~��|��p�u�L�=���t��o�D�v'Li
 �P,��Z�sui��!��;uy�+��~�^_{X�:��.:�(��������-ћ(��F5I3[�I���;��h���緯�$�/d���M`s,��/7o�����`�>����OK�q�~"/����yx&s���'�$}��_�\���@�������7����h��9�;h�a�������y�Ј�Ԍx�ӣ!é,\�Z�r1�)K�zT�8��Z�6��<��$W�/�҈�@��1��]���U�d��\v�8��\�wx=��:`Rg�=�J�� 4_�L��O~d�U�d�'&u��"h���o�����~�>��Uq	O>���7���7ff���]����.�bk�$���	;�n�7�mۆo��]Ɏf�4��Y'��#�Vwx�5$���#��pN�Ć>���wV6�c�Pu��NӞ0T�/�,y~��pO��\��էVt�o���'Dho�����ޤ/���Oy����>M�����$���؝rӾV�����᯿��/4���0���#s�����.hR��[�~�+�
U#j:1y�`���NT����C�� my%cͰ2��19�    ��G(�H����9�zM��?&�����j�.P�~_���8"߫d�i*M65cA�[)�1����Zp����Y��|%\�K��o��o�۟������ߍ��!��i�.�C'���_��ߑ� c��4�#�"22t��KH��1�;�����K��]�R����ِ�Q[2|�g�����y��ƕƌ��-��?��������d�D�I�ޙ�
<������R����ᙆ}4P��E��x8�x�v�|_��:�2#M�aT�ex�tw�+�҈�4#�23����\h��&i������f��t���IU}�C&��G���jջ��4q˕#�f~#YC�A&ګs�;�'�����M-;l�{��=�I~����9��,���X�o3�p�{�������@jB�&�Q�\��>�<?�j�I�y��
���$�M	o>��A�iv�����!�Qy+>�_O�����tm��}�5�-��́��9�Y�^���U����i���|���1���Y%�BV����뇣ϟ/��������M9�����:����?Im:U�N$�G���ҫ���[���y���׿����"��� M���F��i�4.�Ͻ�O�)Q�6��K��_�7���H������_��4���Oǡ�;ĩ'�b��JH
gt�8	��N�R�v�l�u��E�t٧6	�����|�Q�H��#2�M������"y�� ���i��rSȞ1Z�y7y���i/����=NO?��������m�!���B��/����;@n��f���%<��[��c)�$�^�uLq�Bܾ|O��ˆ��`�
�M`�cX�²+��y=�e�>��m"p4�B�=no�;�P�k�-�BqJ����}W��^*ZZ�,&ҽ������_9-��������V�'�o��P��T��`��D�:�-� ui�+���R�H4����!�rڒnaY�"�K��?�w".qJe������|o��M����i�=�2z��}n��������IdV��,�bɖDu��������zz|z�X�vk��۟�?>=?�}�[~��@�%����!�O���x��<-#��x����?~�i z+�-�������1�=�<\��%�f��h�p}��d� ݑ�b~W���Ɵ<vO�������s�-��V@C|+ͅ��Kp9���
�4R�A��L+T#�!tnqd*�zO�x�0�xD&Q+]�
��cM�����'�F�La��l�q͖��<8b_?ϡ+\�:DE��-<�/<�7�^Xd�~/�c�!H���{A��J�M#��F���#���+�wrrU��]���wG���R�Q���t���jX#]@F3�s��RB'b��{|<�є>��f�������ׇ��kCv��b< ��j��NXZh5_V�~}���d�j:SR(�hK��F�9��$�%Aޜ�m�x�i��][#��!�A:��Q�!��_��G>Ąe�!���!�M�� n�B2Ze�*j�-$����Z�6� \��O#���|�tm �W?|y�#�д�srL|��Y��@���-����c�mĴ6:� ��%�}�)_��ܕ�Q�!�y:7�t�l�qf/�V8��E��e���2 �TyתiAhqu��N��i]�O"I��4-�r?I�Z�����d�
kHK�Y�| �L-%��rķd9'�%���Czy5����:�{C�'L��` 33��{m �z�`d�5�0��]�%o��y�٣��,˜5|	s,��]�+B2�#�Ș�<��`��p��N7��$MR�2X둏:���V�?6�/�V���"�:l�E��p���4�L��������p��5������Mk��I@,F��#8�,�a�H}�g򨌉��!����8.�,8���E�~�F����8/+dJ�L*����HN�{�ȣ*�>Q�����s!��9y�ʳ����go���|)�׶x��H��2E��`�Kv1�5�^�z�ؒ~~�[꘧���^vln�=���gm���ǝ��^}{���ا�M�kg�Ec+����}�X\�n�p���/��x�����'9��x�G+�Q��ϯO���ns��y��<h�.��D�o���|3��he���g܈��f�뿵}�,�<DӐ��S3x9 ��J@�I����o�3s�[�,��u��y�t�G+
�y���V���vn��E��@$�LAR$gY^-��"���#
ǝP.`0��	�0:��"��asD?�x|9:-M�Î��	ҔS�� 3`dJ�����+xMg�*b���[�����AC�
*��$>��/z<�9��"Z�h��nB���Z-uɊ3�Q��._D�68�̀x��盧�7�*7!�/n�M�:�K!��F�	��sn���.y�#(o�d1����-�ٺ	WxfK�Zp9���I�j�v!GV���2E���UW�n��s�m�e��wʬF9;�9@ ���֫J�9��=�$ �Z
b��bn?K�4���ҨI��y> ��yp~��I�G<��46�� �it�4�,��	_�^����Ř_��;��%�������9�$>�B�>iȴ���)+'�k�޼��ǖ,O��~��ԓ�ɹB� ��B����~����VT�Oh��<�����Ef2��a&�a��=O@�L�ೝE�.�RN�j-�vb�P����({VH��3�#�m���p��9�#�Т�e$��x�$~Y<�
�T;&�$��b�Q����pI`Z��O�`�T�Cx+�������>n�%2�_�>��nN������j�]E�aB�30e���+��̨Ʒ�K6��qb��L4���_�j��(|mpmC L(|
A�\�!�;b
�+c�	UO��[�+ն%M`���	�K�6�FR�c��x�w՜&4���}���;Z�*q&D^�flq��z��6HRe���u�)����昮�r�<��� �_�fbnLOjL
�X�Zτ�[(��l���`�8�Vr��
�eJ�(����a�d�.2ˆq���{�I���ABw��m���.�A3���m Y6{d&/~S�h��(���!
wtuX�\1e�Q��"&���#Rr��=x���d�%�O��Z3�x��h�
����_sP�t.mD!��=-*������";Rݺ9��!��}���R�#ONG�8�'����-X��z<��9�E�Y�Ax5�x쵡B%�@@͏�f��#I��&�%=;1�����d��r�p�#�]>ܣTNnr.T�O������˹Z3;��k�'�s(㪣a�<B�,�	I��� $������(%<-�M��U������C�B$�r�MV�����ٴ��h�H#�s� '��1�9��xJ	J�op� �ldR��qȾ`MJd��3P�������R��VG-��1cQ��t04I�ZC�,�L���5JP25�Vj�\j���.����V;��jJ�Ŷ��=LLe�5Y�����g���SѓN$ߡ#�b~�f�Y�©��[3�-t����Q'�ވ�'�)7Ȫ�5���D��8B���)�_CK6NF���\y�r��F�)[�E��Y��M�'�V��S&��K��Kր�4�1>I�N#LYffclc���6�,�Y��B$ "˵����渥��E(}H�NU�:J_F�۔�ګ��ME�A"ZKR������z=e�AN_� ���CR7���ʙ�N������{��!��XT��zG�WT�	�Q1���L����UJg��*��HXS-�MP�9Rǭ5�Ny��$�v,X�4C���q�����/�)�=�P��sl�EN��N�����+�;��b��=���j��>������,:uv�"Rd�f�A�Yw�Q���<��mږر!�-�*�Đ
k��h3咋ih�kD� T��Y�L�����0J�����
������|�jp�S6]��$�����=&zT��eoa�6    ˼�����ȥ&���H?�Y{F(f�S��ZX�@'K]�i7µ��e�&�Ԥ$�@��nT	�Y���tC�؀�Ap̂1N��`�0�L��uJ��F�2����o܄���Y�r28�)C�vxs�~�m��ɻd��> 0��^�`զ�FLO^��d�1P���s���dL}e��6y��ۺzV�gڣ�@0�b]�WLƆ�r�LH�[�����Tӓ��R�>!���H0���q;.]&/�5����1�RH��r���JO�ͧ����e��0���G�r�'��"����ʎ�
A�%i&��J*;�$̔��u��}�z��З�#z�ރm�g%�RQ2bU<�+ՌDгO�}�.�Y�}����<�c��������,�3Ig��Y�yZ�s\xSe W��O�KȐf��/d�9���קK�Q%����|�n
�����[�Ml
�S� ����(b6AƢ�"rep��T)l	&��X�z�T�	�cH�{oP/5�Ee&
{C�C�d4������nDڗ�-}�2��p��kӨ�3���ӥ/�%�!.��g�Y���ϜM���3������0�� P��&.�ޑo@~�.�H&�ٓ�p6�N}o�E��Ts�]v�����j��U>�j��->$=�wP���u� �2Ј�"�J�)�(4���+�e�*Bf	�Es�0-�ft��3�2�Pe�z��H�n�9�Y�_��	Qu��D]�V�p���Jw�~4hR���:�2���Z8��cӄ�)��2[!���ƅq��"�)Ot�vn�
�&+=ߟ�[aՖ��?"�L!�1�b� ��E�d�x�V�����!j�����PĤ �&�ո��qx�q<iEuH�T���W�2���D3�G�W��K{�ڍ>C�����툯Ѳ�6i1�����qlr��}O���{th}�Q�,�r���&U-24m����]���y�`���b�$A�QU�k���,��"�vkd��k���[�Z�,u\%�uY>@�Y�0L�{+��X��7����������?����g�o�����SD�ا����{P�����?�%"�qb���o��?�ӿ�����&���7���nχ���y{{��ߥԬYܲ���|�k��hҜE��yh���j�W����c��&&�ĉ~�62�ۑ�Rk�e�L��������Z��b!:��>PC9���m�=;ʖx=<^�����q	ެ��T�#��/����+�96D�}$�P��4�]���$y?�n��``����+@3����>Z9Z�׏Fn��U�TT/Ul�I���e��e�*R�)h�hM�����gi��fYeU�~4T�n"9�4j.&�ƈ]��|��mH��r�FM*��d�@h{\�f�r�V6�%��z�2��oW+�1(?�ʬ0���������y��;l�%�D�|_8������^?�Ƿ秄��5q�Y �[c�'�[�(��y�ۯ���C��[�\:P����1��)ӎg1��_�H�ڥ�r�U<�T�`�:H�w�Θ�a]�Q��Rm@s��$�A�`�h��ذ*X��P}��d��nX���{2l��Q.��Sx���X�F��<ȟ~�d@=I$��Ʌ����	A#��U�$h3ʠ�}{���g��g���E� ��D
�_;�&l�RԨ�;�&5d���lY<e���(5��v�p��"b8!���L�4;�k�����+މ�@l��xCU�<�)ƨ�s�5����W?�汏Q՚l'�"O�EF>e��*`kվC�Y�AzTP��JN#�;���_��?o>��_~G���_����yo/V@N9�N �-�&aM�ɇjC��Ag�T�g?vd�kU|\��=�qq��\Q8qf�K)�-@���?_����s�Η�Ť�����Y����1�_��D�e��4�/)iS|E����-K��T����A)]����j�RS�C��Qh�:����M6(�L�lQL�:�o����8����W�@U(�2�;}#�������T�/���KS�a�������W�;��Hf�I����M#h���w�0�LU)fc�����t�ɇ|~��p7���y���:��f���[�޿�f����=��aH�o7?�p���x�\:�M�Y�����V�V*&\G�����7q	�>��l���;��*�y+������j���d�,`4�Q�T��1&������x�Q��X<9�24�()��j�iqF%��Z���Sz.����82vN���<7���]sU�?�'���+��_����eF���Fm�K,��T���ך�d0�@��R<_&{H��pY"�n(�_n����ѿ�δ���ἵ
���R��:��+�s�;>�a��Al>}�.�Ɋ�\bOw����T�+)����F�}uvΐ�&����C�����cs&h�?�_�eA�A��1�NJ��3��2�gP��8Ρ�h�@}�Mz\�^���A�[�愷+4#��S�4�o$[b'eܷ�P]���;��s|?s�b/�*�x����
M�Lt6�Upp-�c�N��̈'���/6lU55U��ݥ*����dt�ԯ�4�٤����J6��y
',�Rr������+��RY���ҕ�1�8G��E�bz��U�,�����-�B�J�>>#�p�	��ؿ�"��v~#-����q�(.���%�B`�0����2�����&I3����3��@�F��$z��'�@���w���ö�^�VDYe�`���wl�Jѩd�!*��E�7�d�v�4Ӧw�7��ޟ�"��f���Ό7N\'_ήOa%d�\\=�� 6/,�˔�'�C�ez>f����y˫!�w�p���a3e_�y���P�P�r*�!��C�G��"���.�x��@0�z*���I�����H5j���!T֠q��ݛ�ьP�
z�j���>2�qZ5P�����y�� IW�YU*��l^&�10�s�jK}"_��-Ұ��;=i�8?.\�toǕ����N���)m#�@�s|��2�v}������U�3N��y� �]
�C�ކC�L|rS��c,aݣ��i?��	@$���Ka����u�l
�6�Ⱥ�|���ܡԮm�̳\u�K�ҖL/�e(�+.|�=��.��K��M8\�1<^A38�DR�L0�iWRcŐVZi��A7+M���ڡ��&�ͤ$Ps�\�@�������s�r�y ���H$�!�1��A^gJg�vF�F��8���ިJ��p��ш�lA;C�n#�ܕ<с����s%�tؚ��ܟNK����_]Ѵ�eZ[�t�U�B�wU��;�LY�9��5# NW+ �����w����fA��[���vAcX��E�F���"kA!P�YG�7�1P^�q( ��(R�೹2Ls����a�4�X�2dQr�rM���a�<ZI�sf��ҟ�:6E�,W7���B�E���Q��j�z�:lF2���,"�qG�"��:�+��+"2[�U��[�R�[�� �5�zW*��i�	3���21����Vs���6<{��ũ�U�d�# c������k+E���I���
��U�U���`�=�Z�J�.�C�q��΋��U�1�k�U��p�i�/�BDan2î���9l:�O/o���ۗ��\��jAٞU���>u��f���Usp3G�_j�rC�ε����g����XE���A�\�d��Te���f�]鉡;B�� ەL�XgэV۩�S���8ݍpHT�}�:�j�L��EO�D��x�y��ng����������Z_A!OZ��B�Ui�!T|�ƹ-�C��/4V�E���|0��F�է��pH�/�s�Oɦ2�H	���#ఫ���&z�!eH�2Nծu}a�,���/�4��v���!� U�K��0����\��>ktM�P�b�+��&WH�&U����u'X^�S���4� ��gR�n$�A�J�Ä5��2X×�K�M��2uJn��%�>�-PR�����`�r��Z�?i�&��L��i -F`��zFyxѧB+o��Qq���Y<H����.    g`�#o="���+��P<�U�!��+��;5�G�d�U�꯫�������dj���E�Vf�e'��A�?`F��$1���ڥ��H��8Dzҽ��6[��c��g�`귷*�Y��(�֯��7�� �E�Fv���D��&;�q�g���F>���p���Ys(��.��m>�?vוZg��􍗍NT��D
�����Rd�y�MN� �y���m�Vz`3�Y�u���%���\岪���TU�n�B��h��Ǚ]2�0�+�fw���+�<��<�%�Q-h�v�k_�A%�d�E^��:�M2N�t�^���AR�:�x������>n�N*m�mS�JG����0r�'	�{ C��^?���x��%��ӬtӘ�O"�^J_hZ%�V�MS�q{���fv�Q�Ky>r�kDo���8�7�Q��x�"3N���o�L5&).`�x�%�^w|��${�0!n�2�,!\j�H-[���c)a9�3BT�sƍ���D�u����Y�����pD�4�\���`�:�܇�n��P�)�d�AK�h�X�F���M|���0k@T��Y����%�l-7��H�5�.\f�~ ��T���9tK�ō���R!;Y9�N��ٷ��W+�ʜ���&�F���D�~�l>;�CC�Ocu���Y�M�'������H���-2�T��H��e͠�LJ��� ��r��������jh��{�"Aҵ�Wl�ݴ`��f�n@�g�0����f��)�Uخw)�Ef�v�h?̸'�6Q�U<����.�	��aH��QV�2��V�z��&��l2�al�{Ui�	�p2&M��M��E1���<\�꾇��dp�f�
KVG���2b{q��i]�˃�C��{��)��>Y�(��g5�]��0Z�r`q6�:>��*��%�,��=1�N�f;�A��ll�ul�E䳢	PT��?=�}^5�Bw3>���y?�Z�b,m�B�CaS���}�G�+KI|`]wl��W[7M*K�W#UQ��'��W�͚~��k��ΡL��e�ڗG��'}y:?���>����nM
�}�|�2+Sv]����p�|ؼ~��|"�cs��/%��ݧ䎤�&�뾾�u�RF%����#&1��W#<M���O��1c�+�D�n�h���O��^���׀/�0�Ȋh�$G����F���N����Ǌ�i��x��G���������MGK
˪��)-%���N*6a,�+�#ڎ��̷?ns:q?^����@���9�t7M��N�aT5\̥�� ���Pc\|5�$rehOSɀ;ŝy��C�/�������M�r�c�3�K��0����r	j"I�����eZ`�$;�6��Z���k'���L�+[�7g��7|���P���n���JV ?]r��U��7�[;����ᱨ+���d��+�NO����":Ʉ��
��uAbW쒕2�e�8�c��+,������K-���d#Ia?�q�'�R/�.�MHl���S��Jμ��2��+��"�+E^�ө+�UJ����< �񁬿�X��S���뀙ކ3$Q\���Z��z�������u2��q�
ˈ�XLF$7�K�&�
gM�H��(��o�� ��Sy~D�UV�x���ٓ�ʙA]�)���I�=ljU��U�`Z{��z�7�����������%[h��%�߹@��7���CN-*�� g��4��+�;X���^f�a�6Ya�Z���^a��5iա~��q_�ב4����aГq�uP�nWy�hBtD��|��
�MA��������+����N_a�Kx��������%���,�$/��6��9}Ů������U�iQ�,�]>$9�����,R�x��ėG�<`H� �X�{�AC�?x�V��D�ĪJiX̎�W8��	�-c�һ��=�������_6V~���@�`�
6�M���I��Ijn.փ�:۸���x��d�cj�i�ڴ��,3!�|��'D�e�^�,�_�;Ϊ���S�V�|'w����%㫾���FzE��u��̀�{E/�\�%�3WX+���o��G���uJ�F#�R��9s����� h,�g���}'.#�F^�7���y�rx=oN�/��>?��w�^��n�(M�I��tN�l�zmqaں�U4�k�\oL�����H��x��W]�;s��';3!���#aA;#�MW]���
��K�K"T`�(�8��������`/A���dT����n�ޡ���ԟ�8R�7Jo���k�ov����W�@h�;(R�-��u����y s�~�<KS������i��v���-�y���2�U�󒐿��v��"*X��7�v�>.K���Pfx?Bbi�R�6^.��K�N���}D�_;J<�� �,FtA����f��&)���.'�>����z'ⶎ8c��A��
t��@O�#:���.�h�]����
t!�]!�-�dx ��9�;��������}��=�Č�3*�"Nǈi�C���:>:)cŶ�4�B��%����O�#�+���/	���}G�RiL�0�ݚ3u?�3EzR�Ȝ�!�`���M�kNT���=�M�ҁ.X�;>��:�}"tt��I�#GS�مy䳾΢R1=!
w���X��$��T��W1h�����8��AQ�E�f�`�aO��E�&�{2ё,�6��5��`Q�,�_bM�*��E5���u�� �%ƴm�N����%gL�]��{�+m[��~�[d��m2�_bJ��������a��o�D���%~Č_K��)t���W�tL+�%V$T�����W���T���<�ĄzR�s�J��Ɋ��/#+�}a��,���3h�/����S�tXb;���b�LA�;2�nX�7U_� 3+��=�Q��Aq�LL��&L��1����TF������[X�n��@O9d���(>��ZpD���א�!2d쐔�i�֐ڞ�� u�	�X�2�`&i#,+�#9�
6��d	���˃Z��m8�a���j�
�����j��#�2�V0���}2-�5B�IGgf����M���̖߷��BBb!(T�o���la*�!'u^P!�ᐁu�+3����!!��եLR.ֻ�e@�8�@?kl��_����3%�8+��u�PDO���S����`���V��dd���6�0�d�E��!-�Uh/����F�zXӆ��^�-�X��oTꛪ�rd%�O���D��Z�h�^4��"���)��Y�)l^��/�&t�t�I��ߙ���Ƌ%�"|��]T�8w(ߢ�&��F��v������5jw�+x����Wx;ˠN:��S������}����{��	~�����_�*af�.��T�	���`��.�C�hD�"�U��D�z�o�4l�5.�/"0j�;M�Ҏu��̋5����Y��ly�Ə;x��2BH��I�؆�	�s[���yh��$uFB"ȸs�U�����}{~޿o>^������U}�ё�����4�:����\�ݡ�]��[:��%#���}7m�<��.��]sU�qs0�%��5W$�����qq�\�'��ԥ3ԑ@�w���я\�@����/������><f7�����5��d�������5��`~�3xx�A{)Z�優�\}|6�Uvvi�jgE)�TQ�E�%�D���0`���g�>dX�������M���,��1[��<��0�#��FP/�Z�Y$��5��xt���k����:8nƢ ����+2�٥���C*�@�˭8�a�Ϥ-j�+����b�Y��G�b�#*�l���_c�5��nO\3�o�����>K�7�/��᫏�>��A8vVҮ�V�x��a�;'�~�qQ`�5���?�?��Y�� K����$��kXK�[~\�<�.���I���AYK�b��]Au��G�Z�v��U6��Yg�e��kX�$��E�Qr���ZO	լ���/��
G(�j���b�l��ˬa��(�zCAK��km�X�s�b�Y�bƾ    ��2$f��;5ذ�)��Ō��Q,j��Ǆ{�m$��gl4(&�\����b�Ы^�>-f�����g�<3�:���GT��hf�Y�X̕�W��E�����Y�ht9��^@8*��B}�Iz5`W=��rc��������C�D�ygQ�?#��--�R\rU�*ݥ<p�Uޒزk���_�\���%}�?����ƂU�2��@���Q�ۊ֮2������@�vG=D˧�p��$��ڙ��a� ��
�t���oװ��#Qmt�F]r����M[�.�����+��,\��

A����}�D�S�I�ӯ
I7�tV#..E�զr������3c�BA�z�LAދAMݺ�u�kx�!�|%��6x�rJ��g�ů
��}$[��.���\Z`�[�<\���@j��u�"�S�md{ˉp,6�-I�U���cr��ˤ"P�PG�&�5��E9g���3.��W�{�x*`v�Y%�4����*�zpX��5���^*�Q*TDS�#�S�6�^�]�rCA�!-A)�a�❹�CII,�F�5� 1�[��(}�|��k:������9�څ��&����d�-�5�Q��xK�t��Dr4�̤J��%����˛5��h-��������ekb:NZ����|���V0Ú����$_3�^�i�b:n����B�{ji�+�Q��JZ�& �y�ґ^���ף,��J���6u:�lG�P��%Pgi���mOv����9�q�������9f�j%��(�F���>-�5Z�Cb�#�ZR�A��[�2�h��
�|'��!��H.v��A9^R��hť�Š����ff��]���2X"�tɻ_�VX�H{�_@_��~�o1�Е,��.�"�Q	�:��#ljs�IY�J�����F]��n;��a���\X �5l��v٭X���}:!�^~�a�i�#����M}<��t�,�#a1��@?���-,Fv�/"x�@��L���O�:���CX�aV�tLP$�5Fcvf�:	�+XlrVq�k"9>�Eu�E�#�fk'���i���>���Q%�� �њVm��`�O�7�`�7��5z�R&��� mIF��� ��iZj�Rk��j: GU�{�%妁�B���$�[����xM��z|zt�I6,�f�U�Q��ldr��;��VX�'��3@�E��8��U7��d�l�->�U!"����'O5�11X����*��<�I����H�`3�^�q��1�&�3[���3��U��(R�h@&�Sڛї��~�X����0h{?��T��\D�M�J��@�xL9X2g��A4d$�I����Ƅt��c�l?��i���zVE��w�r#s�J�A�!8ߌ

�bB���/4L�xLD�y�t��t.ª�!A+��H�z�UR�er�T����|w"OjS%J��W��&+ó2��WE�������#�%G��*�{�=$K�rym3�����-'��`�0-0	���5���������7���[X?���ĞZ�����K�1��]+o!��#����E�(��ґ�P�� ��y�b��A�~�<Cꋜ{�-����o�ˀ%�7(;�(��i�}���ۀ�&:?"�i3䚐R>Y;�x����f�VN�
v�N��qZ���%�FiJ9Ck��K���_&i���I��]�5!&v�����~/�|!�Eyr+)3{��7��8u�t��(�ZojD���ˠW0�S�&�j�9O�o�S!�51'�"���h��"A�	m��aUԉ��Z��Mj��oɠ&�i�ԫ"O�=�qt�BsE��o_��U�''�m�0�1��<�ꦏQH�W����)[uy28Z�ʈp˚a_g����i��=�_�ɇ�d��z��ǅ �o�{���7�JÐĢ����b\����`koR�"�YFf�s9�"(%ͼx��׻����?�J>�e�L����I�Ϻ �u��=4G6zEpJ�n\+<���Qh�J.ɤ�1���?�$�*!YD�����<��]]X���O-dT4F O���V�&�/�h�o)	�I05��Yf��2`�`�hS|[��eI���j���} HN%ˊ��lHXG�|�r{�������a��{M�Mۘv�SX�����s%��Ј���W�(g���ibm�+�&��Ӯ��oX�҃O�4�2�$�*0m�"��W[� "!�.�䮶7{E�K�o��Hs&�C���MY�pX���{�D�m~\��C6�W?��HD��7�l�eC��[��Mu�pۧ-��9�J�l\���!���H�L.����/=~�lsO^������7��_ɚ����fѣ�����1�S�v��q�ʓA�6����^j��Qh+�k����a+_�x��o�d���͋�Z�c��b�K[Γ؂8�\��z'J�|���� �їy��E��;�����9��݈C7G��@�$�&/a!��Y�R4v~$K�`Ƀ�hߊ:�-}���+�����%{xdp���B@�u�����}�U�T��1^~\T��[�EO�\(��\�c8�haf��i����;l�12�;2���!��Ӂl ���u�2�z�G_Y���=A��:�>#΅ș+\�T7�H/X�羊e[l���%�=n;K�3���;Qs�+ɂGʹ��Z}���I؅#�y^'˅x��vHV�d���&▤�c͍R�-D�\'�X�����.�\�9$�d�#h�5��c��|�5s �I�������]���t�d�uu�us](��B���'�;�1��R��<���|x��&�/w}IC�{��ּ��oNh%���V������)i�>��K� ���}�T�U� �Ӳ��Pff��-��FB�
��fë<��.�wL�&�7D�4\���+�ˋO�D�%�� �Njǀ~x�8l>mNO[r�T����_F|�5��!mj~Wm�q�P�pj%`@�L�{0j����m�1T����9&0���÷�!m�LJ���vbz3�`N{�$H�L�'��6��(�h��F#W��gm����7�q��ڥ����&]�U���呈	�~�L��b��2:j�A�|��d#I��_��͕g@���?�m�y��FQ�)�4QO�[+EH!���).zځG��1d;3_��;mؓ��C��
Z=��Z�?���P`�NńshvҘs��6��\6������q�f�w�с<VQ�6�D�J����8��c��P�Q	���uU�p���e�d�q��������V�R-VRy�T<>���n2��K�����]t��|2�С��C���7� m2�g�J\�	x�z��;�o�O?�"����G�'��rm�5����t�t�&�??�~������#-��w�?��@K�(����,��t�vjkM�z>�̠-Td��q28��~Hu�Og�F?B�Hf�"���!��[�F���h*ui��X���]\}i��� a�
b'8�X[�H?\�������#�Q��ǟ(�J� w[����$"S�Y�Y&*��R뻀K�Ѫ�h&Ȕ��CcP%C���M��+r�vY���$�i�3�f+�w,q1�5rR���O���O��}j��S�D�~_8bOJ�q��9�����]z:�9�!![�B�3Y�] _0z���c�s�2,������a~SxK�#��������f����#^�U@r�l�4�����d��a�Vf@u���`=�/#�ᕼ��zG�,�F=�(��۷_�n��
'ʥ#?��tjnbf��1z�c����J�krA=�?a-:g1��Pg�&l��� t��jX�P˪�9��d��v����E>h#�xs�s���ms$�����LB� 0Z΢ۄ� =�Q7�g:�����˾֕:��1�l)��q��J�j���f ���hBmݫg����������}?����.�M"��.� KB7s,%Ֆ<�R9��D�J��t�B����r��Nl�/���dCt!3������,{���RE����7F̬.�[��p���J8��5	���Ts>N�BjN�"�x�    {��N���?;[�d�a�Iy��R����q��2a��"VF�ek�Pik���ȳ*�!�4�9��w�IN��L�j��ޞD������q�rЕ4`���E�U0���N���+M�i,�>���CJ=���,+}lNo�O/�ѐ[���HYJ.0*�s���e_�.}t���,�\����q�O`ȆA[�f2��0���"Mѐ�iZ�@����:$�!~��7k�%�,Q�l	o�/Bˣ0,����z���۽���՚��cb0�*�E�=V SF�3�fY�1EV0f��ͱ�5�O��ՙIr3)�f�A��lI��dۣA�4�X��A�=YX�|R�����&�֡OF]����B���ZQ�@����O.�n�R��2���
7�)�#���//7MxM��j��kצM7�:�כb��&���, �1IXxb%�QYAn��0�9�����3�`����ʰ�4�/��j�iqޢy+!(���16�s|3@�~y�B�:�~қ\+G�"��}�~|'�{�%��*w����j�'��|r��.�O�E�UO?�������M2�9V����h�H1$��-��]�
f����{S�>�&F�Rj�b�ehR�%�F������V�H�{�[��O�&��$qo�����#:$���T$=ӂ�rِ�$�2�I�چ,m�}%�0ǉ��I�#��[Ma��<oI�`���K�qx��)�+R;z�/n�b�f9�l���"��Ĺl�q�\�A#�~�x!hB~���	���d�h����tlԆ ��U�%I�S�N�c<�d���X.=�l?o��M����4&4hjI{���R�#�n��<;�"L��02ume��n�
����z~:�����Ut�Ӳ�"a�V|����/�3���{P|������,0�?���^7�OX	�&G���+���\�ȍ��`�i'Ƞ�y��]x�������#m.�QPx�D��"p�9�{?�?�fȘ��Q**ӌZ��p2B�Rb/��N{Ef�.�1A�c����\�����/$�� Q�J�l5v��1�Vø��c��ʇ�4^����
q���6��O�-�0U��)+��x��� ��������+M�pw���t:���u��M�Κ�SS7��[ST�^t��͚�Sc��-����|j-�o`��C�F�T�lX�ī�	=�<N����T墘XU���*1�~�����:�f8�{�y�f�_B�]7s�kT}ƤSP�eK�����	x���v�M@}���^�^�s��΄:�I	�+1A��!!��9gTC�K�� �;8O�<��G!.؝�.�-G�c�bWsw��� ��I�I�Yu�8��n���*�5���h����M�s�T?�T;mR-O��*�a2��'���~�W'��9H�]��n�[4'���~Ӈ�\:Z�o3��d�iD9kD^v8�-��¹4��8����xf��;��x������+?IN8����G�j�O�b%{��bvB6Ug�H�� HKNZ' WM^�C��K0�v��"�m�o�P�]x�Cw��
�:�h��B������"�*r?�w�*1�hd~N�� 8!X����
�E��
�bH�C�5�����uM���|�{�f�,���aw�B���;��o���8�c]}�G+��{�E 8҃|�������2ql(&n+�����LTPW�w�"OdB������Qc��
����=�?�e��� CjG�����]S�"G�u(���_~�� �^R�3M��6j�$�9i��k�d�J/�g}��7<R\ g��j�s���<�d���˼���Ô0�4��tu���t�Մ�ڄ�6�p��$�~�Z�h��)q��E�r����')�i���ƬP�M �Β��DO�wox�	D�LL!HF{[��5�*��j��j��a�a$0?�[�V`)��_���0e+�pb�AF���RF]^���$�`螽I̩����o�/��lPd��1�L\���ҿ�3����� k=@4���P/E�R���y0W��{|�a��T���cQ�M���m���ޥteM���1��$t�_�@�>Gp5��ٴF��I�"ߺQE$���M/Ɂ`l$oҰI�z��)zʛ��<��ɋ%�)f�ej(GrO�'߿�W���_N�}n���}/��S��n{��mݦխR��1��Q�,Q��28�IΟ��y����f](CU,Ȕ�O�T��O'�`]ݦ5�إ����'Y�E!c�W��5�n<����D�3.����E�J\^��I��$u�&��d��6�l�Be\^�9D�y�GK��rT$`����F��M�@ȣ&�Ƒ�4�H/��%ZzV��|��@�:
�P[�3�+\����M���Ì�qK>�� �������%��⦋��(�;*%EP��nx.���i��oD^~!�F�(w��DBwөC�L���ᑐYn�������#'E9��!������>T��u=.�*� ����Q�9����q��j�ᙹ��s�����wÖe��J�0���(���
c���6���{s#���<���姍j��m�C�#Θq�<�#��<^�(k=\JQ�://`�V� ы{M����\�̾=��Ñ�8G���y \�����G���m����$���C_��Y`�\j8l����o�'n��"���7�A0A�)C����~ (^Qg��lU��y���-�g��F(�ȡ&�رw���{�I�� �.�4��#FQ/*��Q~�"�N�v��}����c���}ɞE�<�h�|�$Hٶ�>��=?B0s,''r���:�У2q٠M+S�N0�@�m�����[�U�{;Ǡm�d�d4M�v���A�bC��e�<�kҍ!�B7�>Q���v4�ڹ���~T\dT���T�G�{��M�ڿ>���=��Ģ�-�,�!��x�����s#p	bkv�}��ј��"0Xm$,����i��4�l>Ws�����:G�[n�<?��'�� %���k�R89���!�����I�����O�pdHѿ7��SM��?7�y�ߩ�,�6Kw����:�L��UO��k�hE.�C��!j���k��o��'ÿ��֏0ǲ��h:�)��������$:	� wq��)(4έ�O9l��G���yvr���BA�x�  m���u��]�Ӵ%� �f^������pK���K�[l̬ѻf\! 2��U���e���G蓐��H�ۉ1~�0˺k��TT҇]�����f*\gXd� ����Fq�Y�m�K%>yvi%Z�~���j���W��]3>Z(9��4�P����,϶�d|�v�w��U�Y�moP9@j���WR*zT*����XQ<gq���:��"Mm��8썈�fٷ5BQ�Z	�[ii[ФP_F����;CF\�����T��ǜ&�� ��� =E�H��=H?�֠	��g�� ��d�3�8nZ�7��,��>��h�v�����ס'I��;�1� ��2G�M���������K �ގ=�.�Іd���l̓��	���{}��v>t�,eM~�G!~19��gӸ�+���:G��Z�]�,���6�� �R�D�e�}���o�V�u�8��$�fYw�|#��g���m%gI�fYw��L��t���)���#���C�>ڹ�#��k�u^�`S�L���ڦ��e�ǥ8f�^�\���D�K�5����2'�܇�km�5=HهTܾ�χ�*��`���9��2FO�]y따���/!��؊+�0&���D^�:�?�u52� O+��GQ�Q��gD��#��)�2p)M:���Ɯ�l��#�+#�;EB.��D���Tf~zIڏ`Z#�un�2:ڗ[3����5�3�
l7,��xG*�#���F��5��A���a<�I�$�`���i�~e�j�Q�w��)$S�ji%dhA^�N=iM�&�F5g�U�ُ��PĒ�1p�ʻg� ��hgv��U�ɻ�1ꍱ�ک���h�wn����d�q�����HP��C�(9�Y�~q�ɴ鹪>    �[��u!�(�!��ES�7Y6��[7�P�W��GpīDK��R�76�Ow�a���ó#u��c�C1@�c_r+���U�?9 Q�5ٵ�¾$�����R��49��Ԩ�rgJ���R{T�'e���~�B��d�
�:A�N���������	�o�@"�H�U���/(�q�����t���׏�����⾯fj�F�=r*|5{h�>�:���{��-5���"���h8D��1WQ7���*�1��s)RJ"��@��l����{DB����
�	���x���SZ|�`@D璅����E�������������zH��t$RH5l��9�.���~�	��H�?r/g5>E�4I��;&r��?�_7�<�󽋷//��~�����h���:1��6M�t&�+-�r�����B
!4��U^.�
�n6~�)��*���N�M�1���W�Qm3:���:���q̰�I�G��Uؗl�&xd�A����c����?0@��1Bym�<G��$:�t�77?�=��ܪ��o��U����'���u�@�Ez(��@�,
֒�j>�"侂w�|~��D6�����g���mg� ��],1�@ܡ �+�����[��j�2?������l��l�|=��'���B��S��.�V�s=tH]qe�{��~t��'���h��V�x�p:��!�F�h����Rf&~GB˳.s�zt�BGj��Hj�����s�'�5�iRh��5�Q�_�4�)�OX��>9$�
#�ק(�]h_��	�Mw�>%\��;0a��[G�g�H>bL�4�BlޏmT|����0'L4�y)_�1QP!�?�_P`NX�c�k�LgH�jㅳ&����L�gf���]��.CT�qOX��ګ�F�f�t岶8�	̀6f���,�(�����n�?m(��2�\��vDc��G	�f�
2'd\\.����d�-ݨ�������7ێ#9��c�"���V��d��~1�Ob!���D&��%Q%�JU���1�>�Yl�Edjԭ�
�������ݖ�c���8�34�7Ѳ�r��s���Me<*"��D��Z��է�%,�����"%��/�?Z�W���4l����ACZuH���%+�
%D�C޴����ծ֎����p�N
ip�r�}C�me-ҕ���ÄƚO�����,-�״�u��թ/$+�v��E�ɡ9Pp�aY�AM5bFŶ5�>�o�
NRA)F���D���ޕ���@�hp@�Jgצv1��k~ʶ�ށ(�3�o(6��7��#z�fQ��D>��M��l(6��7�VG(��z���՚�U(����PK��#8A0F*��9C�f���G���1j҂���Ļ���f7��2��GI��"�@��A���j�z�m���_���Fd�HRb*���$qh>4�4[2U��/��8F�9��A'� [MFK�1�(77�k��T��B6�ۛ��_����J��䈥�m��y�hI��4v���#xd��P����>t�����+s���k5|G�t������͋���$f�$�+�!/�}�]������h"�������p}�A�R�Th8:^AVd�e��2�6��]cD�΂r3 ���Ķ�#�������S�2*����D�?�B�%:Щ��C�������0��U'�u�֔�(�����d�9����g���ܪU��J-J�J	�V����o��CNKƯUq3I���w�Q�Qr�R[�w;X=��	"���ܲS.�uk��/�����m��ȧ����̲۹ϟ�-'��@�+ڒ��e2l$T����7�_�X�i��W�Z����Q�K�z�)6�?UF�d�X�i�g΁�,}?�Q~\�B{U�����
��ǐ�zͰ�X�J�$�Rᒾ,���%ب�ͯ���_�F���B]&� ���:��(�G�q�]BTϷ��gC��7��8���@�b���}�M���v�i׮�:h�=��Jgģö��9ҵ���/��g��}I�\��.e�E�<&�	U�C/�k?#�rQ���s�M
�z���Z-�a�5J�(\�9YFt$��C�.YBF����&�Ctt�� ��i# V��I�p��A�P���0�V����-���OA�U�	7!�N3���EbGO�I�����Q�:��t��)��;qh)��	z����d$;WǋC� �1t�+f�VN#�i.� #g�%���p�M	ӆ�\p\�fq�v>�x9���&rUH�,�nς��둇#�@��Jq�V�"؀��	�zY�T�p�9��$��S�k+ZWb\u��p7�mQ>IO[�O���`���+qށ�\2	f�S�S��Z�hp��hG��t|��`�S��B��96l�2B�T�5u���5�aT߇h����5�;�Y�#/��=�%�3Mf{���2��]ʔ�$X�Z.%;c��y��a�u��0*
jO L�K�Mf^�b�x�B�l�q��H4���)�.fm1d{��8鬎n�S�e�e*ƍ�!�?�,��R��┸Ý�7������\v�P�o���JV��m�!�zK�a8��dG0�ҕ�F̋�a�h���=3&�߿�'�-W�#�Hm���Se<6�}Ű�����%f7��}M�Ӣ��s�:��e���K�����FѴ�!H4�6a]��}M�*��
�\Eyɒ�yA��fdK��8d�I���-|�`v^�b\�6����YIŸ�I���n�5�n`v^�����,0w��Ԣ�h-T�q��,ƍg�5
�bu@�ꜻb�mg�"�rN�Xn&�GW��$i#nº��3�Ƌ���ӻh'�����*�K�,a17/gS/�d�ə /�+�MOvݼ�Űњ	���_L�q-�:<��q��FKF"n��V8暑7/gl�b����af�ܼ�Ű�ǀp'��Ox���V��&�d;�C��%M̬	�lŸ�:9\r`�cڢ�x�;/[1n�P���6e�L_�h�b4��2{��X�2E�CL�jOUw�P�\i��СV;���b<D��<��9qkU����A>z:�@�</�����2�<>���]N�}��@E�D^��B��S�;�:�W�Rp��,������'֡��~4���hfp��o��f	}��`��pա�mD��Dlg��!�#��w-�i�ב�8ޝ��b���ƕ��2.����}�w�J������j��љD���pp�������d�C;�;\�ē����t�5J�RZS,cα��uM��V�J��}A���X���AAQ9��FsX��v�tΜ�taA�"�x�=g���0Rm��ΞO�ޗ|��H2������Z����0{�)�hQH�"ס]��7�S�Ѯ�ls22�AW<1J�O�w����:ɤ�R�� .��|�U+N�'�x�i�pj�N��b4�%M�6��鵝j�)�h�	�')[�ݥ�ȉ*N4�8�Ϧj��U.8Z���������u"����EW8����_�_���_>����K����@724��ʷ:�;�O�gm�Ȧ֟B����ʩ[�T�CUDi���qm.�F���� Մ*����4�@K�yR��f��8	�"H�r�hu5{���(���31�0w��"6;0D��%∇�ؕ��lJ��ġ$�ٝ��%��VG)����!|6#vUb[��Ad���\�i3dڒ־��،���>9�3Nĭ׏�7�ڵ	k��1. �`<�yF qXA
�2H.�[�Lb��Q,Kǈ��
L���L��}�ȟ�W�_.�Wt���d�����:�I����a�	�>�}�%3�!`ݠpɸ%M��+��¹�ګ4l`����G�$q�i�����2��vŨSMB�]�01)��B����q�{>\�G�^4���q����u:�R�Oa!,�O0l���~gM����-�L��L� �� �9b4%E���30*�pUò�c[:�ח�Դ&٘��r[/�0����à�1:S㠄��s*��b�R���I�C��K��Gh+A�    p$]C�a|�@
t�Th#�6E�t��M�h�M���ט>>��萘�"�Mƈ��㠹�T��1���)S��0U]�!d�+#�t��"M�X�"����Y�8U-F[�|9V�N���+��К�
�+y�e���@����Т�i��Q�@�.d��M@�����Aڵ�eI�*�Ԧ��M�p�M����ɏL�[���w��J��"\W��`���q^
{!��&m����#�	{����&y���ݹ�'�)��
��/�֏�{�-_P��12WC���N��MQ���[�L��bP�Y�9!�^�	hu���[����%ߖ���B*u��af�_'hҪN�	�F���]'�`ړ��j�%�����z�Ġ��7��Šq�y�N)W��^c�S�U�+Gp���Zk�^�[��e��1��Bfa>>}@E � 0s C�R:S �� �l������i]���+0�9*�_`�F����T�0l{Ƥ�5G�bV�� �ݻ�뢴Ee�,}��)�D�4�(�Q��P��@ٹ}��Ң�R�OPG�����ktt�mv���ec2�G�D)=�b��^���L��`n�n"�xdd&y�}���°��xdC�Zе����c����������%��2j�E�ڼ �� ���ܡx)�����2�GEҡ~���|F(��ሀ����]�/Gc��%��2�\�d��n�+x����=��Z�r/�g� ��Ձj.�ad���''�pw�^Q`~vԨJ�]�ɖ4�]KW���j�<�n��FΉ7�̿���t:������ӷ�,,	��k{C�����`}V�N�G��v��N�o/d�d�3��"�[�t���շ��f	��(Z�^���E]Ւ��CC0�躛�Wq�n�*n�缎��̫���C�@~H��b�X\��:	���G�=Gƃ�hQ��D��I�^��� �v�ȹVC�Zz���9��5�nx)p��[ʙ9�����r8�4?�^��K�J�N�W��r]%m:��H�?��M��3�G�*���zn���8�0p6�'n��(0_4�d�p�8VX��9dX�(��TH���Y���O����'}�<!B�0���v��(,6�K�챂\�\i�Fn�����=+�YM%M�����5eѵ��ΈY͘�D� ��6آz|�jgQuE�jKJ
)F�c�ή[͇�s�i����?eh?[�VL�nH�5�jnZ�2h>VɚA\�V� U��,��L+����{N옹�����*�J;�æ)a�K���|�T9�Z�U�+�$��'��u��Q�۱銈����.AMO.߾]Z��q2��*Q��5u<��e$��D��V�x�4���|ZH.S������z(���92*~�	��=Eq�y���Ύ�+�:��F�����S!�=�p�c��I��C__�Nla݅,��,l0�$��1��UX17Z��y
OHs�DVżT���Za��-}���YZ$�s�v!L5������]#�l<�W)��5C�%���O�Zы�[��Z"/tD�iػ���/��cW��F�D��Ÿ@�.�trL�9�40ԯml@�ϱ�)�|�=�"���?�TƢ����Z�Y�Yu�X�BW�|���E�OC�KE[�)�,@TA=A����h�Ƣ���.;Nײ=�2��a�R�?%�
/��g=9b�:�dБ��X�Φ�o�0\p���Z<+�r���B~�a�Tr�{V���!�:�sLK�I�K�yscj��^��=�Z�r�E�J��ǰv�����"��(IZ�E�~Fu3���$䨎4����b��&GZ�g`,B܁��m'u\DY%���d���\o�����"��O@�J"F�m���:L�Iv�(�"�������	X�!�p ��ȱ�z��.�I�砆�=���u�U%^��թ��A#F�Q��h�2ՠ�;��bc���t�m���q�pX�3�q-X�b�֭Q-)��Q��O���Q��t�m"�f�@�y�07j��R(�B6��t�P��|�-r��Ɛ]B����	��a�j��zux�m�7Y�(��ՎLq��RE�}%� *��NZ%�Gj��>��iusD!�����r<�c��c��Q*����y2q��0JHq�Z��
r���g<���$���(�4�	P#jq
���pȯ���h�1��F�+�k�:� 'Y�%Z�m&���ceϰɡ�1vC>ȷ�('��3���1���d(�'mg�ŷR�A�>T�px� t����I�����oz�b�L���p�1a���놐�.�k:Cn	:�Nqq'���+V�;�?D��/Q��I(c��~�Юn��G㓷���h`�~���G-#�G�k�^�|b4�W�ɋ��4�d�~0HB�[��)��ѫ�	����'Q��e����A�5}�� +o�3opմqT0�
�<<����ߠ���7���(��p�m $D�z�hfF�ձ?l��O�݄N���2�K,�[-hܹct�03���7�.�E�#�D�(���J�����v&SD��k�����Z�+fR[V��im�[�"˶�c|F�sC�
�L��Qr�\�N�����o.��9I��@���n�&g
Yj�8M"x��E5\��H:z����'��Kn�G%bU�z+ʩ�������h�&[�ˑ�\b3	���LȞ{�cƉ�+�#��.�c�"rI�������EpJw��u1�}0�L�82	�!�\)������edS �s� ���-�#��>e�YL;Bt�$�N������ �l��M_�%���,ٓBA��rq�!'T��KV�+�LP�q}���GѢŪ�>�=�%�����3E����P��4�i>p�K�WJ��oH�qm���F�F.$h��M�Y&4�[2�4棄#Ʃ킽z�(ʝo��`�8�7����H��w�����P�h�VU\G��j,��rq�
�L`8&�qN+��2�K�Lٌ�C�ĭ�[*��h�ɽ�+a�c@3�t���}�J{8b?$"�r�ʜLȜȑ%3�q9��2U��˅m*�1!��!'�h��7*��z�A�L�&9cv�t/B'�<�6\�Z�]���ɖ��E�������j���L��n�&�1a�Rd}T��u5ku�u�����p��I��ȣ&�m��M��ω��/�����ⲛ\�TN��S����w��:��x'Y�X-�OS�|.�����&,b�������5ٖ�*��L��K��2���k;�ZEYn��ŗ�,�?�{�k~��jCB��W�K$��<Z��t�������9K�#��"&�ܽ�Q|�
�m��>l�ً�l�pqeuG �|sT�i��$�����Q�n��n(7Ji2���}��0�HC�Y��c��<M�]12g��l:l��e=��K�T���"��>?P�4I�
��d!�� ���l�3�i����T�/��g�ȥLg�AW�
��H,��B2���t&������i.�_m\�C&�WO�Wuņ�8XP4�I����}{��ш+)�(�j�#EF�E��`Pa�0�[�S�p�?��S�����Z}׾�4����n'��;��v�Rҗd�����&ҡ�@R.�u�&��~�'��%/��_�[���-���9+��[�~�ֶ��C"r*�j,�܀cp�|b��!��l�p@ٛ��~�v(�/̸�`)̅�r~�HƉnr^Zk���Ԧ�Ka<���AM�����%	��eò�5U�1cvhM��9�\&� �ґ��K��d=.��C�'��V�T�Q���A��n$�Em_ �|�#Hf�K��V��M�7z3��ln"�<�"SI�gݺbw"����(��L�|߫���E
��1{�n���mn��4��(>�H��%��	�\�*Ǌ�:jX��Μ�r����d�����pX�n^.���D+ZzC���mr)#���j��]��H������ť�������aw쑱��FD���F���Tív��� �T��4�rQ�ũZZ?�&?�/��=`����/�u8r�7�v��s��W��V�����~�    	G�����jF�d͡��8@���Vr�Jc� ��"yG&��)Tm��F'Q� 	��NŽ��#C�@ܔ��:�DoɹӜ����0���('[�|-���ÿ鬗��V����f�;���.�|��4����K@�VT��6A�M�!�9w!��=
�"2T~v��%r�
c�̠u-��f�0V��� +t醒�����E9�!� +�p@{Q�լ�ڢ���BEw�F����:� ��`Ѭ�wy
=�z�G&����p��js�]=�*q���ʦ|
���d,�����:��K�λN�Q�{jyF�(�5?r�`�5:��2P�&�v��q�B|��:��D_
1B'��ʅ3�6ad'�2Un�fiL��Cna�;��a��W���B�-O�"T���%�FU�PrRd8�"Fb��(����$�p�"��sU1�8�(d��<�9���Yݻ7�c��R}��!���������|�Ɗ(9G��wJO����"�15K��;�#�	��hg�[I�aT�����e�T9�!p�Urr�B����n��>�^�kSiH{�(�����ݷQ?�	�6d\��zJ-��9tݾ��t��/5����(p|��Ⱥ�.�n�~C��A��JdC���N�!~��{q��K���7+i��U��S�Τ?TN��-ãW{�9p�K�ߡ`��ΈdM�������� F�<�!x��n+)��<+��dtȵ�@KlT�n�v;�{��x3+�	|� ��>£�NĐ��=�ģ7�r��i��a[3�OF��Jcaڌb7�"١>>��j��zD�'Qg�ra����0(eQS`X�w��v-B#׽�dk[��@�c�k p&CJ�	�	��j�O�)л-)����1�g�K[ؿ�ޅ>`��������������|u�"��9�2��>�T��}�ly2�*��}�x_�����F~%y�� �����s��9��ͼ�W�&�2�CJ�-R~˲Vx%�lf�-�GΒ�V{D�G�+�хh���h�u��Q�C�W�A+4�nw��6	��@�b�L{q��`ҭ�h���Fƪ�^�|wsu�>>�����Ջ�WҶ�B���?g��o���6�E+�0���~�T�x�и�W��1�9��;���\>8�3| bSG�����Scz��	dc��yo�..���q	E~�誠%��g�m��N��E��GOF���(�m��m=I��2:i
kF�ܫ�pm�G+:V``��Im��L1���G�t^��N%icH�/��b.�¶��?("�>M��U�E�y6�tf	��hkRS���ЋrF<y��ȸ�mȡ	I&}"q�&q3�R���Yc	���H��59�Wb	^���D�E{�	=�i	�BI�U`JD�<_�\�Ih�X߲(��[�X�4Y��`�v�T&*ø����F嘒��D�����_���~2p�@�(�u��/
n6�dqI;"���B,Sc�_�~muۗ�J�B�(eE����ifjk���`��t�8�U����AD=Xe����;���Z��AGږ+T2�̤�cܟ�-�7$���%I֓�O�`q��=�!L�-B�*���K�/�o�, X�"��r�Y��)�SE99��<Cz����ޢ�+�8�8C-���w0cl�� �����&���k����Î~��zy�n�O��,�P�Q���M$ks��,��DT�;�r���n{�!�͵9ڿ�=���_�
|�lL�6����!Iz��-�����#E��aQ�^֘i�ң�'��q�(�e0rM�R��9�Ϊv��Q,C���u
�g�����'�G�r>�@/rx���O:o�B�&b2��	�5�g����
�}���F]�c�e#6AO��N�R�[D�6ß�9σ�73��͍@���6Q�lx�I��b�B�H�d�D)�VC�'�AN#U��0������$1=y��Vȯ�j}Ư�����~*M�/Ĝ��~��e���:¢7=#��rBu�s����h���=g�k�ݮ��'��hRt��H^F�Pm}xqr;����o`mF��+�mM\	+�8_�u�PMi2����6y�I{62���R�ޑw֢v�n�iQ���o��@/^�H�H�9��UX�?����� S܆8~�$�(�	z�� U'�f�j��W"b�X(}��_���������S��C)�`q�]��)�x���G���]ZNԊE[6GNO�^r�)����{"�;�A�.	��Go9�Z��5��M�R?����1�A�G_�]�l�߰쑦/HY&	�Z1��7���<+�8�O��j=i��ٮ�f��wZzU�"�0����v�A��s��U��U �#t(��)��8�'����ݰQ!��	��p�N\WC�8�������	�4�i��8@B7bf�F˱��K��MO� ��a�{� <�Z�C+��8W��pr\%ї��%p�(����;.*h�8�1����U'N���7�u�vĕC������E7;��U��1$��Bj�NM5�E�]8C= d�\������dzN��x[4v��F!�ҭ����.�C�$2`С��oK�q�4��1���8䝦�K1s�cլ�Ҹ���*sq_> 6QYvb�Y�e`�v�wD�.���䀵�c�	լ�v葶L�e��H���d-��M��Y���#�J�Q��=H��'��p|G����C�r��OJ�-�^�}S������\��s�����四�)�|4��j��9�g�h��� Vh�`b�5���~�	�y�o�(�� #*�n���X}�d�ѓ���f�����3:���������p�A~�u��Z�V�:�z�X�=���ԃh��E���#%�A#,cL�,�g�5/�(�К�V�a�l<$�����!�C��I���g���1 �q�2�t�6��|,���H#M��r��1tt�:'��>c��0L��o�̹��8�r�S��s"O�@)�`����G���͗��,}6V[��c���0����"�"� [���E�P97�7_.J�3���Cdc�G_.Ji���X��__.ʆ������rQ6T�ܶ�ܗs��r��5��\�2��A�`�	:(�1��q�>���a���i���I�ڣ�{��Nps	;;w6�	R���.#ݪ��YYq��\��"�*�*y:������YW�2c��Hk�w����)D:���c��H�i����1R�������V{����ǔ3�+�9C���iP���1P��Pu�s��J.@�R�9�[� m:�%;g�SH�6��!��x��|��ݧէ8�/|���'�F���GS2r	W����&�vOW�tWY7�����.&W;���&d쐌�H�d��[Wy�p�7��ю���1�<G�\���H�`H��� W V�~�����c�T��"Jw�
'�t�)Q���P8B���mh��#�+��9��j���P�������.Y;�ɵd(�{��\��e�����' �U�tz�m�
�GJ�W��p� �lgǗ�1^���	ϰ��:��c8a�ԗ�eK��� ��1�	�:
{��l>�>b{�p�&�l�r��ψ��]�D��i�)w�&�0'��ڧ���0
0��1�9�;A��N0ut>�����^�P��1�	>�Vcd\v�S��)�6��+93���p�7Y!�J�N����4Tl��'N��D�A1v�v�(�7{1L�vb���䓀KA��1%H'�L���.g;���;F0���$RW*0�#�pAJ�!���k#�q�����"�I;�l��)�m��4+�τ���0�Ia�q��6A
Z�J�4��I��}%��r��������P�B�8��E�&���'�]��"��::�I�n�܉��ɜN�|qES9�	h3�/4M@�2�Pբ�}4��ݢC�N�'$D_�����+suF@�QQ@��H/Ԕ�����G*\�OڞT��JM�4n$���t�Mk0�Ȏ��K<��o������q��Q��ԉ��&��t��F���wj�����M���N�i��:/�Bݢ\�Ѣ�[Љ�SQ��#ij    ��BkIf�,+b!�}G�0��;��u�*�V�����フ���$�X8N�*	揁J�O����	)D�Q��A�|�B�e9
+�����*��R3��?�3bdp��!�R���!1�k3�o��J�O�j_�� �KxA�0��x��H-w4�Qw��}<1}wKÝ���*-���O���d|�
1������.[�q[ױm�g�_,�*�-�C,]�͸3� ����R�X�����Ȱ�\,13��U�]X�ZN1�	���-���cUnVO�.nZ��'B���u��¯.?��\i=U�8�.�rɆed����z���m�.��W�����5Y�+����SQKu��v]�܅\�v,v���[�Ǝ��1@y?��W�.6�i�?=⾮Y<bu�)�dO�$��d�$~m�۬VZ��Xk�w�.yrQ�S���t���bW/x��@ag�iߝ�c���Y��6��X����4���1`� /p�J��+������D(!dPgS�
(F##!E;^2YW+c�0��#
�+��Q&qm��d�^ƙ��CC���(�}�.����_�G�+*�yrC��X6FS)ڇ���]�p����[���ֺ��<���m��ܡa�����\m�߬�BP�%�B�r���d�FS2��t���9���؆K���(h5Ĺ���%�K���|95=��m'@�
p)�6�#w瀏�XW��#>��ɸq��bc���3��!Wc��iұuٵ�*�$���N�p븣��Z~ŵ�LAe�=����T�`tq���D%��h���"/ɤ�w�%q:���I�=��5�t�Έ�ـ�%nK��hQڒy��=�ȑe+��%������EjG5PйE��9T� f:J�w��W�mT�0Y�E��5N[7H�B�j/.�^�Bp'l�V\��E��	=_�|�5�[d8Te5�Ӵ�����n��9�;2p��Y�R֟��8.�l\��K+n^�Z;�J�8>4CD�W����i�}Q!_�D�:� y1v�׸�N���g��b�Ea�F{���/����+T�)C�����П%�ck�	\7���m]���������n����JIG���r㧄a���K�n㼟�,�YbJV�U��vrpVｍ�y9CN/@����Lĉl��5yyd<�u-�M��b��dM�m_��vj��N�L������h�`(h#U�e�/z��ng��J7Y/���C/n��v�@ë#-�rU�i簱�g������wBFc�d�*���5"\8CR/xG\�B��o
��-�p���O�˱k����	�Eѻ�V&|r�����~�-6T��~���
��g�+>B�D�� ��v*З3��[k�(qG�^�ö(y<bӬ�7A�e_u��� 4H�wZƩV�]��3$�Ӿ��;�/�I��pE��o8Ǡ������I�l�K�d��2n��E�l�u�X.�>G`��t�jP��  [����]Y/�� 5�Hl�ZQ����OǮ&6.R-�?i�ڕg�~��N��7G%�23UW1�R)�&%�Nƌ�!(@�ђn)/��g����bB'`�x�P��R� �bY.i�q��8;p��+;C^,��ݻ���сL����F���
��{W�j�JK����?*y���TRr:�!�~�6B����0	Cm^U�o]'i��H�p��]봱2��t�s�H�F�o���1];�I���g�d���0D�M�o���_%p�l�����WV�\]��f�TbT�V��c�<a���C��{'�����/���24��CI,�����i�W1����L FG�I\�B�ڢ�5���ae8lv΀R
�%YB�9��N���t!:�MNl�S�g�4���<��.���PLb���ލM�I����,��VI9y	�*�m��m�gV�uٟG�9B�='1,>-�u��E@}��ΞS|C�����K6����I��a5�b&���\���k�9u�7gT�{d0u%;��B�Yg�YqI6t[g��%U�tj�����!�N-�4o��
��yk�p�ޜ����!G��?m�s&4=k*�z�=h-l#O��3�@�vs[�"��`Cr�'����>ޮmPޅ�;�\|>�u����t�=�a4�-"6�mf��6*�!*M��	�y�=3���X�\���{rō�����s<:�S��S�3Wo�Pt�gg`qU���`�U\���lܴEO��7:��:X�E-���S��MZ��L�O��\^�N���C\f��O�ef�X��!o�!L��ѿ��ic�ӶzeHєG��m;�R>���3���aI8<T֠�X�����ԘsXG��{Yg�P3����<�'��'���u[���m}��ߩ�y��P�BGG�}�[7��qᓣs
J	ik͒W/�5\}�]��C��[h���UG�0ruP��x�%�C�����:X�+�|�m�zF�l]�p=d�w����)y[w3p�(���J��.�ǵ��)����Q~F�]]�p��4&����� ���-qx��֥����UݕS(D�Ze��${z-�V����<D��m���BAҠ���8�������$8�Y��S�V�ժ>�U-���hI�B�%b�5���ڝq7�\�:T���-Ztܘ� c=�6�"�o�A�2��TA{W��E� ۍo.�og�.��WTs*<g�c4��#�������w\t��	F_�*_��4����CI�v-�.�=s~0�5��"P��8��~F̕���`5J���#!$#T�Sy�'�(�<�QH	Wz�0��Z�})Һ 8�,�AZ���zY7�N�2f���R ISڸKu����C�<Z��-z5��r9dye�D*�g\�Y���`���Q�6	،XM'P���e#E�4��)-�GSxtW��aF�N����M#٥+{3��`�@Gd
�%�$�%Ԛv�u3F��u�7���h���z�[f��N���b9��4Eb��Fz��<�N�L��Z�g*'�0������mDG��řB���Hp�:9x�����f��or{y�䙡2�@��LYT����4��B����D��66���f�����&9f݇�x0��e���s���`2w�:��"�i�A��7�,��{��&(A^�:�aYJ��b~~��]��8sG?۱w�u�]��i�ߣe�~�G���fs�yX����%���?q��-���������G�
�w�`I�X�����2���RXޤXH�m�vl����~��o)��
!��	�]�"�4���M��Qo!��â�c0��ن�4�"�ez四������zu��7���U`�vH� �S�W8EX � ���:��a ��D.��~�c��9v)uq�h2.�D���k�aЮ_���~�󗯫��_?��ڌ��ێѢH����R``�������맘�]G�%AM��2���p��R�[ݗ�|�v�y869�BW_i[���hQMD�+�k:�in�nVۇ�#	�����4Q���m۵֠������"o΍�]�������QT��+�����K0�	C�����:"!�Br�1���n�Z�J@�	�Ց�L��}0Qh��//�q�y��姟P�<��z���c� � �0�������E����z$ �#]IT��J"}�Lax�ap����Z�Az��7)����F��ٍ]����!a+A�)J7�/?�������(P�[z#��"��q��,��������B=!.L#��gS���)�0	�W�v�kG6�u�3���I�����	��M�lA*D&%r9���#�?b`��3�^��Q1>z��p�G�`J��L5ã��jnoH�]m���h�p����������~h_��u��M/�9���kЧ]!�d���1��f�{$��<��<L��#Y�h�pΫ3j��͛���lo���6�V����>�������{D�M��~�R���q��<�v��Y�8��L���vH&VڅJU�$�Ӝ��4K�	��2��8�)������R4��_ݕć;    d*��gUR�L2�:�$�m`Le@o�w���qU>r�Po��5b|�9)��&��������2	GdDof�c���'ܓ�O���aB��9�0������� �����s����w �L����6~������n�U#-��s���H>:��G��i~Xf.ۀ ��IPxy*:�Dz)5:u��FM�J�9��'*!��(3}�F&�O,4(� s6��ٛR¹7����������?���C<�)�M� U�	S	y��-��k����l}dOc�x�G;L��n�d�_��/���f��36��tnmF�	�ۄc
�7[����V)���;��w����45��	��|)/eJ'%Ļj�H�^����~�^��گ�Pv s����e���%N��zZ�%^��P*�x[�L�6o~����O������Ͽ�������Ͽ]}�80:�����j�zxא�E[�U!�j� g��O<iE��Xr ʴNJ8�>�qЫn�Qe���J�Y��|$�y��k.7���VlV���4C#0��\�4��������~��9V����t2�8��� ��᝶��^�]�z��}����#�R7�j�9�.�fD��)z��H~�x�����M!R:F��o^� �#��.�T��P��<r�=8L>~/f��#1�ȕ���n�7�o��%����&v�UBAZ��}�zC��������f���KT%F���&�����=ׯQ��x��/to���=�#μ��\3�m����o��'��y�e/�}��{�>|��woכ5�r�!^:>Q�ׂ����<�F�P��	l���������D+w�[�@\[І����~O#����m��$��-����)�v�^,�]K�D���*�J��Q�K ��݁}"�VUj����q�\/O�.O�Y�:�_�:��O��� sE8��h�jaF������䏪N��$�L��]����S��3$Ա��4��d^]�X^boH�;o5&�zb��M�ā�,�v���~n�$N繦�_��öy��g�!/K_�h��>�.}<���",�s}O��jU���٧�D�I:'_�o��t�%�6�/L�Ș������1!��P�2t!��ܵ�����̽Y-,4S�� ��@�|ޅ^X0&?v�H۶F��U嶉�1�z��vi�{��<y���_v|��ߓ'ء�Ѱ��;��!ߗ���mY�.E�X�,���F8Gf� ȶ�n���;2�UȿD���'q2R:ⷍd�^P˙y ���t��]�1ҡ�q��4��%�(d<�I�#��	�+޸r��f�M�ʀ1 D��\@�5X��qnD9�Ƣ����,XK�L��DΝ�iU蝢Xݜ7�7�7�]m��� 5�6�ڏ�.U���}�ܭn>��n���q�au}s�)���.�9\Co"�Y�k�D5�`v�`)�X�W�nT�6J���?��m
�;8����Sܨ! ��W��>;2�8_�7��̨Lk�|P�nP-s$��Tj�vYb���,Q�=�ۣ�c����DF�8����{|��B��O��>�7�(�eɕ�������������C5�����`� ����_:=�ڀYi�?����ňo��q%ύKѸ`H�&�dm� ���{#�Ӱ>|�R�(��������D��(�9Y��>c�i��o�y�@|?�/f{��c��T�sc�?S42yj
�4%g���dM��Y���nrp�R,�Yf�FI�T��<C����T�d���>f��.��<�%;C©�3�.�3R�]s���x��<��� ����y��ؒs;���;���8�>v��(��iv�%�����;�Uro�G&l��d�6�]�[�c�X��-�-����%�iKN\�,8�n�;__����
��Ne�cc�����ń�����2W2q�!����5ݼ�!�۫~�+z7a�h�r��s%�V�F��<!���|;AT$]̢�c����η������n�u/����t���o.�{}n
�y��)� u+�p��B�\Uq��1�B]�l�G����y{"�s��1U�R_r6�$�g�?Fn2oN��o�j���%oc01���y꒟���W#�W����(\��}��9���%��\��郉�yw��7��rm���QJ�]�em�k ���������W�@�>�{�Ğ^{7At�r�YNW�&߂�����p�nE��±����A���A�!l����+P��n��T��P��܎K{mU�y�P1�\��i��Z�Z|�lü1�����rO�Fv���HX+���ۇx|���7��d�O���WU8ǖx��ZWaق ��5�����\Չ���rЪ'C����8G����:��-9:�6�7��=	n�Zt0��0�=V�H�󜜐3u�9��-��zW��-ytƢ![�>��K�����w��u���q_�=D[�i~dsH�"�#�B�ʁ�<g�]T�6Qrm��L3�vW�$Qr(�����c,Č�P��ow��%� ��c�o�,N�z�|Dߓ1��Q\���U�i��By�H�W�}�ح��~���MhQ�QL��I�a�����h��]'[<�b��k�ܺ��� ,�-"w[������ ۔��O��#�������"%p8c���+z僔�Xw)~�&jIG��r��~s��^��O����7�*��)(�A�op�&�VC	���BZa��1��W��Y�!>(�3WE. �<�"��-��̵���O��O��7�77�?pH�ѐo։3[�O7�<{����Oh0����е�:��n ~�d��7��K�@Z�ڨ(���� ��X+p2��)��h�w���=��HTSrx���?��*pR��O�wp-��t��w�]�û��_@!�ߒH���qE�8I��*�f�-/�D�S]_��*�����P�m���ן�����>�ғ��}���`�q3	����y��U��*�|��>��i������_~�ɏdQ���k��ȡ�^m��W����֫W�	%٧B����4݇��=��r�v�)O�g������c�no�i9�������1'���m���OW7$���6��i�\���L�^��}��s$j���:�(&�c����_W�����/���_?�m���}u�󏚫ey9E����Յ������9tBO[�����q�]���xѶ�����}��l�J�n7���3�3	�m,I�Ve��!4׏�h�_��<]�x����ř��ډX��P���QMC&�G�;�C _��^�Zۜ�P&��:�9\����ڙ���@��O���ӧ��߾~�׊������0*���O+!�:y{�qҏ~'�I�� ez}�$�����_��Q��@f�X��TC'�cl��n7�_�Խ^[A�f*��Ξ��wBE �W��2�X)�
���@�2�x$$ �0 �J4����6�6a����f�R-i�HS�ld$pH�>n:�9�F&�棙�VS	�Zِ� ��i˻�+�4�rr-�4�d�P����U��V��v��D|���nF����
��n^�6�Ύ���1aM����p��^���W��}��+�8���rG�vD�iC����0ȪN?�E���:<�� ���֖|X�tq|�!��rܾ�U^r�/�U7���l6O:���J5�b��^owǡ(��|`��9Z��?_��g��BǨC1���պ|�<\�zMx{�dy�Jw��w{I�[}X6�u���g	p;7�.J��v�۫N�X2��z��[�1�$�Q��^=��i�U�Fݣ�'�(�;�g�ܓ?G;��K�%�CV���C���χͮC��mP��N{7��%0L}s�sQKJ��v,����K�)g�%1��~t����5�����T����$���Ev�x�b������ֻ�?��i�^�Ht��*���uy��xwX��?~/8���hII|2*��}����<�����a���F�Y��q�M�n�H�!�N�zEv$5����Wj(���n�w tޤ*p�=���+�d�=�šcd����[�^����0    򞷺�ێ��DP�IE�oS�$�=�u���q��(d��DF�H�9Dt���=>v�H��ȩ0��"��N_56	���|�vYT��Ǭ?b��/?���Z���?�p4��B���L�6�p��8�JN�ם:�=4\Y=~/� �<jx8�������>�����v>�ٸ���E:��WY9f�������J�����m�-Mk�m��;��8I=�R��Cf)7�qd"�v�!�o�z��gF��uA����^���3��v��/Z��[k���C����Fxwqx5��h��It:�#�͓��U��d�{���]cR
�|����^�����^]~��~��	}٥�~����f�`G��S�����Y0�]�u!��:���y)�M�7�5Q�ȒCR|�t��� &�=�k��brs�]����N�����K�r�$�V�U��.��
����~$:���j0_CQ����hȒꤺ1ZikR�PCp�FCf�����h�;u����+P�&E�P~"���T���p��ʼ�%=� ���H��d����|�������������"d��*}�{v�6}�u��Z������%wOP/�	�M����������z������r�aG�V��Pz������f���ns�{��[�12yM�Δ!+���|�H?�Rs�Led c�.������)�v�	5�J�a��Hݺ�����	��[1RragBFI|a�c/����p�h6ڒ1�҇���N�V0�ieg<1�A�2�k�6�)eG�9B��([m"�SLgm�GB�U�b ��1U2G����y����䒡���x������r|����������Ǐu�A�uUֿ$��0���sI%��R*��awk���8��6"��n܉|���[�6��4�|���9U�*�~z�A`j��6R�G��,��v|����I9X7���^�-���*�4��xNh�fl�Z˜��K��_,W�����EQE(������T�������H%�<yz���lW���dbJ��:�U_������Ys�옕�.��>٠؉���t�5wN��ZiW&�����=���I3.�q��#�ؒ0�&��ͨy$��E�>��`�TuΠ?~���/�×�>���.#��zL6A���q���*#���ps)H��$|��9n�4~x\{=�=�"��⌜J!���9|���_h ����H�ڑ)��E�_=��a3��i��9�*!u�vW#%�ّ����%os<;���v嚘$q}tnb�0�Χ�W����qh�Y�8~����/�������~����!էh�n�x����m���o�~�������#v�;~.�)�i���;��� �f�K�:̧��RR���3<�Di�~2=o��=mwۻ������ݵ��fH�ۨ�$�AҘ���;z�șŴ���Q�,��e�Z��ć�Mi3�a'YĔo�Bu�Iʑ1]ƥ��2��� 2�灄D� �,2�����@D�$�-/(
CyAT]�O*��|���Pi���LA�<��k�(Ӫd9����`u��(Hƺ9l9wQ:�lBe�����W[}����!j��"G1�-{u{%/$ ���]����H��>������j� ���W� 4B��]Hڭz���j�8�$B$,
�D'O�CaC����E�UL�ڌ� �"����%W�q�oY�+B�������́#�5��ݴz6�9���3/�_��nwሀmŽch��L�z��˭�����=mZvZ���2Ʋ'����:��s'	���7�������Uzr<��!��T�3ΎIn���_�����w\��ٲ��/�q����_�"��w�͑Q��K��ۋ�U{\{�����)_���韲}}|y5M��=���Mz,�l�7O�����sE�~Ew"�����h�z^�P�6��>����B����H�l�����R�{�ސ�mtzjF�N�v�kzYU��q$�>�6����se0��y�P���X�r�w!{�o8ђ߾��5�h>G�j	g��!#�`�Z�Go	��ȹN�#�\a�'a�����o����������ݍ�v�g�.ŷ>�`oq��}x������l�2��F��LP��Q%kcs:U灒cv�&����8���'�׏�ǻ����{����� ��k�am��4���N�4��������3�R3�I��c�m�yG~�����������������M��Y���j]|��4��o���UR����;>��!a����AZ����_Q���Ǖ����ΌiN�^`_�����"�K8"�9|���N�p<ň["�E���� 'k�2mW�@�rI�q�jU�6�BWYS��ޮu���t�Iq.�|��/}#i����MK���o][��4O�W�;�7�����맛���6�����E�����*m�h1��|S>)����Co$�O��[wٖi�qL�_��n�^�:�����n��<�w��^���Le'��f�㹁����r۹�6�"
�U�H�����C���y��]*�C��vE؜ƓO^��c��*��x�O?1{F��AE����{�����w�tO��W�yJ�f��g��y�}`�d��=w�ٷ��ȷ�ݮ^���H̸y�f$Q9�2��jw���;'�S��c�����3Gi����?���O����W��|�E�۵O���w���]wd�M��XR���ʲ{�xE[h��qF7��vO?W������Q�^�F�ȅ"n���̔��쭦�N���'Bڇ8⊽L�_3��r#��W�����s�Ȣ ����?-l�as�M�-��������oF��w�����J"�	��
7��W8���\�o��4��֧\��qR	ϫ緤���5�yдRmsw�D:�nC+{ az��E��J��nHz7�ͯ��M/tQ���q�x�Q����<�w�EwG��vX>R��vzQ�hی)eO�q���B��*�V�P���=U������]"J�Sj~'Q�%�H�J[�c��Su���E��M;�t.�[=�����G��,H�8o�f�O�O�9t������؝�P��ډl�H�)���%��E\��MD2%RѻT�.�����	&�CL�U���)��ޫ�|�� & ���g��gB��Ni5����g�d�����M�a��c���.�{����-G${S���������0��a˧�d�jzu=�
a5)��o�3`l�*�	������r�|JȦ��?`|#RL;�
�x|*��G�D�t��ǂ�ɩ�����1��t(�Y#>hog�ҕ1NT����}�H�L�!q@��.�YI��kH �q6x7����#�iF�HO���+j��$u�a��s��ؾ�4+�����O���A��yB2NI7����Ttf���c778Q��B0cG�6��WVD��j��@Ak/���j�W�LaԣalG��;fD_��Z˸�f�r�q�v��!�@������c�9�0q;�8Ě��7��i*j�0�<6p񵣏&M����L��N���ᔂ�U:%f��T��!�'xӥ0� �$g�{"��p�Z�I�����5tOɣ��7�K��i��A\t;�8�n��y�G��
i&-��'����+�!(�����IK��a��8����AT����dF�O�x����竻�!}�D�i�`fm,����ґ�%�����S�a=]���F��4s{����o�3���?|�m�����gr�.?��㧯��O�����闯����>�X��7�~=t�Q�b�,��`��Sx�Vţ�g���Rw���M���-z���K*�{�[|�՗��������,ы�w�[��ҹ�����%I����@�S
AЀ�ڛ�������\����o�5��+Z��m�32�����A��ZO�>����}�%�HS0�I���0H��[��	�F�A+[SC` �C4mr����[�R1�����_���*;kTxT5�yk�4ߑ���l/� ��M�� �E$v��aD�,��%���sM>b���<������q{    ;>[�q�	��=nnQb����9��"�7x���q��i��M�4B��>5:�"�)ۜ�UL�W�$�-H^�$?THDAr���B"�mL�G�*H��I�� #��]L�c㋁��d����6'��}L�slN�
����2r�r�c�/MV�є˿�i��X�� -'y���wNS��SL�+�r��1�o���iJ8�4_as���c��I�s����$�ݘ�-%|�I�U�n#0�����dv�A4������
M(hn���4�-h�	��4��iDA�]B���{dAs����$ÌF4���
�.h���SИ��!���Bc�Ǆ�K�
���	�?�s���$4�,��,�SB�kI#K>8$4�5>�Y��1����ь�����
M����&�+�)��CB�F41��f�6����_����������r�cL�)���9�sLAJ��!�xS��Ŧ��)���ݸ�;��)���S��Zɜ�CL�r���)�S��$ڠ����&������L�K~yS��؛�����*��3�2�|�/�c��V�V���_G�	�{2�|���)����D�>n0L�C��WI�����$+դ�W}�]��3w��H�e7&�{���X�����¾ԺX�]L�S��t���1�ϥP�b�b�_��.��1����E��������b����,w S,z�i/)����E�I
mm�5��"�#:�]S�y�i�Ũ�5���!%E��ʢ�����o�	��7\ IX��%��t�Y��Hw����"�3�H+�B��P,���֪�<���"ݻ��(˒�T-��"�c*}���E��x�5
�(�"�g�h��h'��"�Ў4��E��})p^ZK/�ن����E��MJ���Z�.��x�����ѕ��� �B	aC�ʔ��}�P�F
����Si�
����إ��$����n������.v �x�]�r�+#�	e�Rb[�'��K�ny���X)�GF�:�甌�!��}�ʠ�h�E��.�o�7�Miw?M�;�z A�f�M�9�1!���˜�o1�m��G�V9�V���OT;��|�A�M�7!�9ɏ1	
��%�2k?��OAL�s�ڠ(8(="=���)���9ȗ�A����26�.'�GL�o���� �ZW��� ��A�4����U�j]����YF2ˈ�XmF1�ɌS'A��1��S�C�ۘ�s�B�w1ŏ��tNqS�\z���1ŗ
��)�1�?*.�xS��B�����ƒ�S��'=�j|��Ł��7����? �:X$ߎ����e��v|�s#s������w��G�q��N�����)���.�����O��������?Gh&>��g�g)VĴ��=��~)�M���~|���hD�zo�g�Y�F���4>�k��=�����+�q��Ze��L�j��q>�㯦��Z�&>w��_O��P��&>r��o���V������N�F5��q�=����ߛP<�����(����|���M��Ը|��E~�~z��R�e����4�A����>N��=�/�~z��`1�|a�L�����f��+�4=��?_����o5�|m���_�#.#�}���19�ʗ�����Q�Ҿ���RbU�������X������a7����M?b�)�6�ӯ���Z
`|R��_OO�P����O�NO���5M|B����ӟ+�!}���鿗��� ������G��H��MO�TX-&>����ӥ�b�@<�0=�Ke$�Z>NOC��I��b���QN�����tyct��O�ӿV>3[����o��Bg�y���Z��d��<=�{�<&[̷��I6�b������T0�b~���W;Z�>��bO���V�zR�ґ�:�djH�R�W�U
�����;Q���`a5��BO͙E
�r�Qާ_ �����׾g��o�cK.��\�>%N�)T���;�C[�M�&�u1�����'�#�O��w�y�OAwd�]
�C]�.�V��j��/N�r�"��HU���ˬS(U�zH����
|��1��*c�~�S�G�T�qM�ֶʻ��~�}�����E�笭A�I���`��=�Tpf��n��&�ՠ�R�'�C$�1p�$���lT#3GH����4}�z��4)P���"$�(��$c���sBzN������Gڛ69������"l�֬�فX�g�LV&�L��Qח��Tw�ΩʒF����#Hf��Jv�{�pw8~�@	g�IJl����k�Q�,�p�����-�4 �N��]��󖱨;�0n*���>Ƌ�o���9�M��� ���9���b,r��*�F��ˀ�2����7�X�jC0�T�U��qDóT�w9Ƨ�,�=�c�c|��[��&��❿���m�R{x��6��9�8Ll����Y)J����{ʥl���i ���c�1p�qն��1�Ac2��v�u�0`%�j����S����9����U{�.��w��l�_�Z�[��ҹY�'��7p�e���U����
2/�*7ҁ�1!ȦF��@��{8�VHg���2�&�{A涶�s�cDz,��.�tX/�^V�;"#҇i�?�X1��Z��G�|,�B���N;�c��}��S�|W-��}��s����W�Q`LT�.�0ۗ���?o0��X�ױg%�n�Z��,� ��w���`�g�"���E�s�+�ꋔ�x
����
H�>4���U�v�S��ӌ��Q�uH��ӌ��Q�uH����Q�mTJҿ*�f�8'��x0[,ţ���^�x�@�(x��﷚Ee�w[��^�kV��-J����R��C�e���}�мb�[�x��Z�QqU"~�`�+�wW�}�4�����%��*�K�A�ٔx_*SL�A�ٖx`�P��a��H{���Q��;i=������s'��_f�ut�ν�~���lV��i=�]���ӹ��ï3�ϕ�S�Z���p:w�z�m��z]ѹ����5���ݚ�k=�.C�VkHAwx���^�>d�O�{�t�����ӧ����?��t��f�M�[�i����o�.�s�m��g�:w��9�ߪl�;q�M������X� �5��(/s��c�:�b(�2G=Jt��(��c� �s�n@��Q�[����c�c|�OgU��&��2��j߷9���|�ǡl����o�qh��=�]���-酿�r��%\Ԑ�E���H禠s3�$L�ͻ�d}a�>��c�
��	Ϗ��q����W_���	�ъ%[�yY�y��뤯9P׷�d܅I-}�O����Z�����*Ȭ|={i�<�K��s�}�~0�*�D�/*^�]��o��cZS�7E�q3�*�CR��3�[�G1�x�0详���m�c��J��~ր�!�Xbi�a)��c5��Y�)oě�Ǹahv���G��c=�LD�Qm��c3���m�=��al��(F����a�����o�n�NF���3��/;D0�=�0�K*�j�dG|��՞�#�xU;״�������nf�-���zmB�����bH�@7|9�T�����=_�g[���0O�u(KD����G����f���&�����ȩ��7Lm���v�bmWR�,���gN�5���j����6���ɩ���������_9�[<iG�[�b�SQ��R^Z��;��ksbHt9kG9j��}�v5�zM#�m3.�z+*�͖���S��f�#�d*�T���@b���U��˯9��{�`���]���[��y�S���[�pת�F�
���m�_��7���q�V�rW]Ŕ�1)qa��0m��T�C�����vZ���8n%����/�Ɯ�m��[{H,�*ȇ+������N �Z��LjŊ�1�q��3�e�=�mڊ5ocr�6�B3��XW�D���6f9n�/�nA�T���c�i+�������� ?jNP+6��I����� �����ikF������[�m�}ב��^�tj~�	����3�:��]�W�tj��������c7�֜�e�fY    ���w��Aa�$����55���'����xrwl��N����Oi`أS�r���;���/���M�
Lv�&�Xn�QL~��!Kg�t�)`r�� �ˈ�a�}���<��l��w$���̥�'h|8��7�Y̕gl��8��o���waW�O��#�=���k�([��\~�<mN"�����lZ�ZC�����_G`Y�8�\�;��e̝�?�3��ݾ�c0$��'��
 7\h��so����@����ހq�1~T�O�;{ƫ�Uv��ݽ�u��ρQ9��&���*���.߀�6��Wwmr�o�x�c�{�t�r��h�t���0~���������`ϻ��Xh�Y�g�Us7�u�;�6Ժ�yN�AL�0����_dŝ������!���	�bڄ9�z9�NжHF�A_���C�}�1��=L:�x^�Ѡc���0�m�6��|}�Ka��BS�S�,6I$�|�\h*@}7���|�a�h�Y�X��(��ݴ��M���+ެ����
'b��pIM�j��&+j��PS%5UQ3��+������Bm�D�IB��j���(5`��԰zy�PUwU�?FO���}U���S%�jg��c�\I��[���镻+��Uϑ��+�������=Yn����FNP��~�t�UI����HNЕ�=�ϓ�D��dYo�~���YI��e=��=�9zY ��QhV,6��_���99]��������r|f`|���$�K��/�'��`=[����P��$Y��`��nc,:��'!h.�@IДME��mI�6~0��IQt%�J��eI�#�XQ��X���BI��d���%���%�z%/�%�J��Z^���ddr��V��e��@�^�gJ�i��Z�g��i��Z�?`b	^o�'��L,���-������%xq�����>�\[�^� ^m�^h���^d����ea�HNg~N�ms�r��ea�Iz#@$� I;�F��$����7������_'#2�������ɰ�9I;@�2�$q�2�v�R�.���:��1�ڞ/����e>/��^��������9tM#�#�,�"/)�c�,��g[�a�A��A^�J��$�Q^>�6�=����3�����\ IG	�����9IV�Ҷ����z��0���f;=�ׇ�����wܶ�0�Γ�G���<Iz�E��H埓��au�d=�gV�I��aU����E�������IVg�0�Γ�&Ξs%9��X���/�*bG�~���9��@�m'��$YO�9ג4J�n���K��kI?JO�$Y�R$�o91H��:���<L��_��]M�����S|9�����3��p��3�����G�iy��������RX���r�d�v���$9��E=H~YX�S���/��9�r`��E�<�z��o��B1�v𒁰Է�eB�b�'T�����>f=s��w�Hl¿�n���|z��ލ^�:��ftجo�>��0����Y;a��P���r�a�.�l�xN�-3�
���a�Xn��
�t�pq�u�\��Z��W�lt�5����6ص��q�6�ª�_�哾c�[0Y! ��������ӗϿ�xh>>~~��c�+��j�(Zg&&��0����!W�u؍�:t*nG�h��
����W��C��������ȸ�j�T�Nژr�T����a�����I��pw7",p��.U׷�7̈́ES�Ŏ��|J�h��S�����c�-�>F&Ą?_�č����:�G�]��(GJ���*�m�"(�@a��������/����	+]6�&�a�=��U�VLk>a1�)�BDJ�$��ӂ_���,O!�Ռ�	ނ9!���.�R�).zR�7�\��7�,(��@���'W?�ԓ&�l$F�����}�?OҜ"���8z�Z;q�Y��dOQr�O[:6WC|w_f#���;�n�D���HI�'()#y��!��ϡ��t6���)�0�J�aofƜQZء+H�b󦣩5�����I�=�FH3)'"�q��Γ<��F�����٧�r˱O1�����������٘��\s�A�J���LH��;��t����|�@-�^|�6|��]�<�S2�D����ɷ���s^s�#&.�1Yn�)pr��,C���)�wj���/�J	+7�� �%���i�+���`؝�N5r��HL����O�Cl���4C��H�D ^���q8M�l_\@[�Oi�+T[�*��s���)j�%��t��d+�������X���b�)�M�r�T����l��j�����]�GT'$>Ti�q^wQH����&�h+ju����f����� ƥ�zK���v�jz�q^�������K<����f�Ms����s����ퟯ@8Q�)xZ�<���2��zf̢�=��?����O3Rʀ�0��^))�H�u���bp-1`��a�؈��_�3,��N�}������FF|ޡr�/"�e�&��e��V���@YY�mrQl(�Z�;������������1��j>��#n��le����tS�%?������
8{2�z��6�֔PoG�'��)C�F�J�X=�����b׎C�x�Զ=w�	[^!��>�g /l�*�W�7g�����!��cˀ�[���'��$g_b��jP@�狛�C��7�������o0�6m��K:�[������o�_@ɾ�������Ijn�ؗ�'(80e߸U"	l�����oPՖq_emE�.{:��%�lKl7��ζ�~b�^��Ѐ[Äŵ'��Ǭ!P����hG�-h��럯�\�w���zq�k|�ƺ$��oX`���NFd�	�D$�|ڼ���-\8��	&�1`@���=h�]���n�+��ȼBtE�]B����<���G�D�i+%��  Q�\&2-�e�aĎOL�EV����=��%2\�AQ�	�&<��|�/�M+0�t����fa��]'n���`5p��^فB����js���3ty����u$�H�>�(������O)�{S�D"<��06�� ��ª,�Ix�%�v2d�Ƅ僵�`*�7?���b8lf@�':��L�SI�1���&ґ�݌�Jy���С!P� sA0
^���M�E�W3����pv���ɵC9��������I�ſ H�5��;�+q��9:�y@ǿ�������e�Ue#MA���wsFF[0�z�Z�����I��:0mj���^��]v���<:#Q;I4.ON��:p��x�*���/-Jh��-����Fg`%V�����%	{���8�<w���!�	J�����I�
�)`|�eɞ�)J�׼ (pk�%�8��>�v�u6�]�*�BGy�s��jk�Ƃ���`�\܅l{��^�������c3�=��bɻو�)�g��o���w܎v���|u�7o���ɬKA0��K)I~���&:ί�`�L��Ŭ�3R ��G��9��I�f��s�NRP#��Pl;�&�3p-�ǀ�O�(h���k�&Gs?C@c���U	�)��%��Q���ԨS�����<����f%+VA�g�Ha�bh�yμ�^¡`��N��]�7��:O�a6<�FHWd1�T�Y�����X�X���h�9O��Ջ�J[;QtW|8=��� �����8i$a�.�S�����~;<8H����=�+pv�T���/�U��Q����Q��s� S��p��g��Z�#�dT8(l�f�2cǥ���:OD�3+�\;�4}������G �0�J�p��X���;8��q#�Rd@����^����D�o1  ��]��EP�g�`u�S��:N�۠XE�*"����.߈ᓭ������Pw�/��/����n��8ȧ`�U1Q��P�#>�H�:�3�K�)R�'!S�"�XaT�@լGP���b��#���U�9��� ��<hdi��U<�Yq>��)	�M���no`@g�`�S����	�>��	���y
B�fMJ    4vS��;�s
�ɡ�gIHt�&�[��	�����S�_d�S�!�+?\����
�1�[u~�D�V���z	0b	�(x�B����`A�c'�'Cl'N��ҔA}"Uvl�`�	���$�R̐�єC!��DM�1��
�8pZ���"+&a(3z:=��5D�k{��C����0�[�'6��+��P첾��`̙ϺH��{RJd��������fL�JHև������p�Wm^.&��J���0�J�#>���P $�'Q� TJ��MB xZ���@{��mQ��r�"�A7_M4)���w���W������3[à�kń�v��e�!Y,7ގ@0i���N��b�-�ۚ���a���;�T��̛ү)Q�����
l�ƞk�a���ߖ2���1�6A�����,].	��!R�b���15	�8�m�L�(�����9�^�'��-b���&d%J�/���W�K3Ⱦ{���g
�!�̀�8n�W�p�9�����H%n��)-;>�^b/!ժ�kL]7�Ɣ�4���/�#����]m
����40L���s91����z��G�F��a�RZ���X]�cSnNa����7>Gwˍ������O��I��(�&Ji�SPY��z9n��ի�n�#�O�j�c�
����@�s���hIViG��HCK��2��WLj8tE�F,��|�X�X��_|���@Y<z��M�K�*4���a����� ��]<p�DD��r�')���@��^�0��25��L�6>MN��kX3�`���ɺ�G��4=3C�?�J��:��Z�<�KЖ�#ph�����0$ZpA��5ʿ4�/��Y~xKik�(�D�r�rڋ��`��=\��D�2�z`q`�+�ɵ�uBN�QmM(f������4)'�̘Q8=B����$ƀ3!���5`�(��c_�P��������CQ	��(�yO��yLXL��-��^��#&jkL�+�EJ0$�l����t��0y�JLЫ�m���3_��<����%�\�a����V����'!v�B>#��N2�\��j-��'h�{gV[T��2�)Gi��Xp܎�E�φ�3�Z�Lbqd-�ɦ�O�C =xQH����f���,\Ȳ��+��'�	*j|Ȱ�1��C�I[��Eϕ%�C�!�:'��d�M
������pq.�!;�4M�i�EM�q8aQ'�$� T$�}�t���3ꕯm��1SF*\����F�
v��=L�$p9�7�ɀ���$֭tL�C�FŬ2Y�E��D�샤|eU�'��.y2atA�rz�������?����nONtph�r�'L�A������`,��rP��	;=������T9_���ĻL�B�&{GrV��#_�u��p)�V/�#q�H_�T{�		s��gf��%L����	�$Y�.��$c�h�X����igI�#� ��؞9 �b�2Y��w��M�8���X<۶�P����g� s1���֪�d-(�8���tg�ƘB���T��7p@�z6��7��T�ւ���JJ_M?\��ߚSb��с�!�v��o�����H�&Q�'�����~��o�$�%�9q��77�F�~b�M�K��t[�͵l��Ƣ�R*���Coc�-������On�<� X9���[�A��&�� �V�T����T=Q��0T���.#S�V;ӻ-�I��?<{q�{�⾊-vN���=#��3��:�bJ �4?7�D]�τ�?b�ވ��>�q@}�i5à�4hLߊ�Y4o|�o�����F�;wN�ϊM��I���8%���CBM�Zۤ�ꂔ$��@I���ğ99]�����3	�����g�%![R�^$W��l��;O	Qe��BT��y6��-��.�wV/��$j�<#�KR��K,����^��iس��KJs�\��2%��=�>�HْTŠF^\�T^Q�� *~0�Ҹ��)Y3��ϧ�KZ5G\���#d��戋<ߏK��j����=-SҪx¶ϧeKZS�K�=_/W�z����*�KV�e����=*O^{V�g�WKF�5#ڳL�S�%���Y����\<P2ϥTΎճ;��9�rv���;�ўT��
ZT��y^��%)MI�U�E�ےL_�!�Н��%-^����*�,�[p����]���V9ǚ�%n�h�s�9�]��^'�r�m-�����xI��%�ψ��o���'��iV��ڳj�+�Ym k�2�Տ_?�Usk�����2IK����ZqV��p�&E��L)�=o�b�$�(���z
�hRpIF��E=���-+,Kb��JM���M�l���Wm�y�_�s%9���P�.wBV;�.�B�,�RP���]����.�U{��!�lMI�n{�L�lI�Rz����+�U�{���L�����?���/	U��]0�!]���M���5�'P�S����6{
�iR$O���,w��7�+r�$G7��gpJ.;&����y����U�<{�aBM��(ʚ�yoM��t�>9EUS<��w��C��œ(Y[�Z?��)^��Ć��v$N$�V�T=.q��!�xI��8���PC�Z]��b��?@���걝5���*n�+���v��'�xI��Y�?���X16��vV�W�xI��EX���x9<>0���P��%�z|�aռ���WJ��yQ"A^�F(/JE �6�7 �hT$yI��3�#��{�ڜ䀔�gH�y��(/�J�'�utrh/�JA�����]��lS�4�����.
!�K��/J!(J��T�%�a'm9�����Eq�yI����(-��()ֳ>���z�S���Bp��������?u�-L���O��2�Q��ds�Ck~V�C�^��������ܞ}T�m���$NOF�x�8E��d��z�G��
ν����
��Y{����/��Q��[9;�
���~Ɋ�ν.%ƍ$)]x!�c���]ݔ+:�n���w:=�"uMЇcB1DW7}뚎Vٺ��u�帤�KZ����� �8�س�'�록��r�$gjr���9^��&{�>�L7)�:Q�D�=��W�!���X��x��M8�Sd�ϋ�3(Vc���c��Ю%R,��D��ݲ(�1҇�?NQP��Ee ��9E[��£�3(V��졽@���e�P�v$��I_vҞ'X����\&XM�����M)��/��%��ú��Q`�KSS�%E:F�H��L�Z$tN��c�E>Q�����<GƤW��,��w�=��L�^p ��X�����=G�ډgyeS���Is�Hp84�RQm	kO�R�� �U�>�m��a�5pz\�j� �AT4���z"Re����g��aT��I0��)� �
T1��1W9���p���p�컈�W��Z�p��1vj����m�܁I㘳�T_~Z�f���t�0�a��P�I�Q
P9�N`�^���
f��� G!.&�@�T��*�&N�J��
���8ָ5��5/y\��-fv��|,(=��xh�F��l8(����<���%���,�����jf��:V�G5�7�լ$��b�>�j������%�+ώ�#f�J�s�5'!*��Ź�]�7�/�Jj�"�<O���ILl,r8��	.���Ut䦜!�Z����Xl�IŊ2j�MU?c�jn�����57!*�����K��e��ɣ�g����Q���/���l"��:���<�3�I���Q��u;a~}��m�~���	4�l|p९V�P�ev;�j�qt	�b'�}O�|�f(��yj�牎���J���L;Tq�%$�&l-��_&��l��ı#�0.��bM �aRY���S\���q��g,�����`������B��˫� ����ȃY�S����J]X�U�w�)K����i��:qlI3��U�AT�?hG����M�@Y0�uø�E�Z��p�%�փµƛB�!�M�n���*��~��/NWUl%��,yQ�T�TB٠�/~��)�{�����T�e�RO\7�X�ߩ����n�S�    �r�1������pU�T��h3f��f�J�VLp���@U<Pe2�4v��P�U�K)손ml����"��p
 �&}	��"�����A"*��P+.�a��up��}��B�銍���J氞�ԃ��b�������(��p{p����.�p+.
�A̵��Z�jٗ7]qQ����p�9��p8vZ�Q�h ���پ�����R7(0`A�α3�`/7K\��l�̲(��YY��5D�"�j1X�P+�
��d��4�b�����<T}�c��U�P��U<�1�x��*������c����afk��r���`C�D��X
�ek�AT�/	VK����K�5�xT]�G1G[��G1'�֬b��N�~A��Z�*��JV�T9+��D��{	�Vg٘'�VLc��;;,K;\���
U�Ml,a�(;�7�*P+��ʖ�U�+�KZiש�����t��B��=Q֍��}��6J;<bW�O@M� �e"c��BY���L4�*V�6U]�O�B��n8`�V�u*�H&܊��|�g+�r.�&QB�rR��*~
�{�qL �pګ��R[.T�P7[(�3`~Q_wĔ�ocX�D_%uvG\���X5��}VPg7�Ӵ���쎨�:��ݡ�\�K_��(��K++��A۰'�@l�]5�t��.n���������6$��z�#�e�@PwD���H�A1��z�;�pU��`R�E�f!�cۣh��P	�^�!~��q9�,O�;��Կ-4k��6��s)���$�{;�>竔��*&;W\=��DU�ԻQ9^V����*�A��U<c��
;���D�<gڀ	.�.��z�;T�T����Cҋ��#��SE;lG�֎�������$�_[`K�xM^ ��08:�@b��ϳj�?`GF�����l@���t�Ԋ*��a�\��|����m��p��37R>�Չ�,S~�⤀˽�Lo@_�uiG\Q�ːG[P�vD՗��z�U(/.v��'r��ԥq�5��Z����/;⠛�@�c��A3PPg����o���ŉ]����_��z�m��D�Rڨ;;�
?O���ݓ̳�"e�}�M�<qC~4A�ؾ�����c{T4]D���j��p`���AԚ�<�3��^ka��O��|�;�D��p�����ϰ�P9��+]�k��~�n�<���.?3͊sd��%�BSXQ,�Ԧ�P��˺C��x�\b���k�`!g����u�:�7p+�	�������]D#n��p�eˤ�Tw�f�����Npl�� �׫ dv���W������q�N���;��w��a��P�e��\վ7Z�}���P/@�Yɣ�`�b�}�`�<�������pk�򸦜�ЉV��j�d:�P��ZK���Wk���t�`4dF�Z�@��P;�����XTn�W�x$�@�܉�<Q��;\؂k.�	#�r[w���V^k@Uϲ+�5b��gE��P�aBV�j-]�ЋlT��U���<�N�[��P�q�V�j@�*�@,����vV>kD��⣸t��|������5���SHQy�����^��kݡ>g}*��f���-@��n�NhQ��A�4�ޮ�׀�0b�1+�hP1T�k�Y�v,�p�qï���_#���9��;���#[Â�8�{?�Re�G��?���@jX{��\�n�-�NY��S�5Gy\�����?��ʥ�u܈�]���S�"Wnm�Q��*�L��DsE�P�����0�aW��\���p0�%Hް�R��Uw� G_�'�ʥݡ����)�؝ʥ��&���k����ʥݡ��3��+���������pu;�L;as��>����ˑ�\�.��x���f�*���ޒn��v�� j�
�R�	�N)���#+�6X����8<
����eOVNm�������]8j��/+�v���H@!s�#ء�xYy�;\�F���(�� m�Z���6H{�b��I9ɚ���$�aWb��Ϭ@�Y	,���q��`[Nҁ֜�Q�� �7�7���vk�1c�%����gzp_jN���lk�v@����	�G��m�'��� ����5�-k�v@E��,�O�Y��r�#fY��"@�g�E�_�	��.أN�J���Ҧ@��Gu
�@5&k�v�|��T�^`���ӥ;�n�O��� f�;>�%k?���u�衡T�Wo;��Yr��l2j~
9.�`w[X��d�����49"/�b���p)�J��}=rz��K.�ɩ��� 99L�]$�ɩAr��D.<V��09v�$gȉ�����"Aqb3�0�z3j����	�_EpxE;@P�oj�)��-l���)�c�9��7Z	�nݗ�N8��zkzCR��ezz�ސ�����}6'F8$)fp�5��1	�=A�l�=Ar@\�u��iF���ZS��h�C�Q����R9 0O48k�9]Қ�� ��y�4�g> 4Hsp�5����'�Y�QW��&: =v@��":<�Z�T����3L����Xoz�L��L�*k����t�'h�� O�ҽ�4����D+H��{�����J��� ;��%O��
�dO,B%MX���"�n���:��@y�gFKȞm%R��<���b����HU���c���١j���oa��|���1��d���A$�"�t����ɓeMrp���&�ةQ��;6<N]�V?�Ǚ��}���Bi�c�d)���~�����J�Y*K��ʒ�d)����i��Ǔ�d	낝]�W9Y{bm+�
dϬ-!;���\�g֖�^�J��3kK��m%_X2��kkO܂M%c��3G����VR�B��`٭D0U�	V��g�+���\�#,TK*�U���*�cMrx��Da��z�W�U*�ԓ�E�j�T�|u���u*��K��d�Dud��&;� Q����ms��,=�뫮T�Q��ʸы���z>�5���v��%9n�ؽ�ԕ��ݍ����0���3�����[���}��,�}��'3��H��5GZ��?�<-�r��$�M����3�����Mqi����f̼URN��-�/����ԧ���)�ߎg��mSk�	Ú������#F!��f�[�������n����H���q�����v���G�G:�b�ft�/���9�Ȝ��a�4�Ow�|�o�+A�e)�r>[�"<�X4��n5K9���I"�(����)`֑�!��4�|u�r��tI�PG[��D�wbV�`� ��ti�z�m9s_X�\����t}Ӏ�F<�Vy��w�P:%Q���|�X� ��IL�tb��{�*�ɚ�nzI`	Lj䆛	7�;QP��ç_�?4��{��a2Lu-�G��oO��o߿7_�}����z����Ɣb-�֩	K1��dFW�ñ��\�Q5�=�o">>�h��y�3�@G���mf�n����);7��f����(ɼ�?G���k�X�������^�e�,Ws�����0����.�7+�e��|�}�����b;�7�1�׭s޾�H�fh��������O�o7���	X�@j ����	�C�����(��#�c֐%pp
Η;��|������D��P��* ������%,�1
I��g"�c7
L;�-��*��>B��΁AՇ�u�ABu�w�Pz���W�%�ّ1�Pvb�k��9"�{?_o��䷧CU#���aٶ���x���'#|�b� �^/�\F.o���p���\���^M�H��/���ƒC��60S��.�N�`�g��a��7qb�O�t�_�B�V-�]�o�V�Ō��W�)�8�9�b�0s��' �j�[۠}�f�̖͋�!c8�(��'A�n���U�� %������t_� 1�rP`���1ۑ#tE�"��F)@�b�|�d������B3+�~�]��K�-�5��XA͖���n��wCn���E���R����hRT����d������e���� ���zW;+`��:A�*�J[�,6C�:H��%h���ŶE��@{:��X�����닻1>��    J�J��z`��/�4*��Q�QP4`6��7��kV��]D����=q�6o�"�]��FD��` ���W��Wx��S�D"��@���C�8%s+T�[�@��#5BY,ăMa��C�:=!��4<(�iVF �/(�x:�u�<N0��4���RA�.b��QG5׋�G���-�o��f�X-��u�Ġ2�����L��CdC��4��E����t�o�DE� `���Q4`>�6��KC��J��'�v���%+(�j�耑�&��?t�pX�D8���T ��F�h�lJ�ګ
xw�v1u�Ů�>����`@r	��������{ 6B`��0�
���HV�$��ӊ��1�t��
�e�o���P����z�S����5�ɖg� a�"�3{>��ݠ�Gc ��C��H�z���%����ѴG��#`U�:������V���ϔ�I<���.Ȳ��E�{j�:�e�O˻���[-@������6��* ��.v~+@(wǻ�%ÓX���
A�6�l�\M�$���,��6�j��*�D��®��7��E�f+��:�L��;�+8 Vǫ޺�#�20�������������5\��u�axHE�
��j>��iV���MG�W#[�5��W]W�%���2auW�%X�>�$�Kܕ}v#��ݚ��ռS��������S�B]%��K�� E�4�R�h���`��q���R%�^Eݽ�%��w�@
	� ���:g*�<Q4���t��[  5Kv�|�Rn�n�k
7d���l��y��|kk$=Z�����7)�0�Z���r�m�X6����vK8���8 �yB����|O �]D�K`*$�#�W�U��޵#$����@��&����Q^�2^𹨮� ,���x7?,f��5n�$JC/&W�"s7�Y���4�����6�~��aI�p���BP	`����U��K���d���1�G�qU"��6}��]mҺ1��[/��A��3i���<���`V4���c�����t��%�v�3�ٸuc;M8
ER֮�o]�0�#�a$�}�=�z��9fه��qI^��e��c�i�٣[���ε�BQ4��qE��	4ѫ���K(_���۬�5��Z��N���(�T4���l�_`��R���+���vo2z�9���[gس�+��1����ڈ�%<q�n}����@4�}b$�E@�V�a�2�W��NB�jpw��6u7[�)�북��uB�����q��i{���[��	��ѣ�ѣ�b�a��+A�;�I�)�m���� vU�d�6��|�;�OD�e؂��+<��;؅���X��`l�5�^�{���u�ۤ^&nIQ���F���Ƽ��]�2�%���n���̶����Ų�ޝSJDƘ��#K? ��4C�2|l���"Hn��X�W�́K<�Q�
�ጴ�4�F;gt�x��؋�t���W�$�p���\Q� 7��8�{� �O��V�������nӼ���N��`��#��Vx~���i���B�~����a�\m���JF3C����u�yg��<�DG C?�E�ɫ����~��fQdXXj�*NpL1�ռ����*M"���!����m1(� v�g�=}	�=[���[����؁[��G�����Wp���/�:�zD��\wc��Iz�IjYi0�1Ge�YG�#�H�|����{���������˟N='��x%�ShėCR�0�=��M�ƻ~����Z�Eg���7&#�������`A��c'<�8�0]-�7��S��5��O����#&O�s��ׇUgŠQ�����:!����X���<�@9/jL(�:�@�̚v`/�n~s?ާXp#	c+��������8�����Л�p-wX)�U�c����n���|={����3�܊�LI�Q5�[�v�� ��5���R�������qh�N�D�١�3��V:i�$]�1�8؛��Yg��;�(8�m�U`��_�d���L�����p���ݽ�ן^�<�U��M�qw~H&�����۷��lt ͷ�[�����H#2̿6/�`����g�>,��VgO�8~���tv��KG�&��Jte:F����W�+s��U��\U'��ֿ��Ȭ��O��D�Ç}����V
�i\Ի�����żY-^�"���d3��)�������i:IrC��Y�$jEݎ�
����4�֧5Oq�t��;������,;�>ũ�
��\��3_��H�-���vvU��Vѽ�2�f����i��]�� ��Ԁ��A�g�*_���t7��[����hx�����$7����x�o��n��V�#qD;U<�?,Vx̯�QZ�+)\�}�Ϲx�� �����	<7�r~�6�BU��j��7��� $H�:���
�GS�U\� �P1��x��D���;�3�OG0M�=�t�Y���8ui�������<c���M��*��=��e��������e�i��?4?M��p�i�/.FtY\\��V����T�ľT9JVzĽ�#;��p��P��o�9.Chs������ߚ폧�ߛ��4:l\�C^��EQ^�9�sj��mP�K&0�.}��xР �_�=��=5�+����me��r����h�j���A� ��e���A�C���{�}��Ѐr��M�k�\��][3Z����	�+�ks������ؼ�������/^�e�<�`�P�A���Y��?>��$D�x5��Ȼ��BVB�aex�.�9�������x�FF�δc~+������H�4M�Z������Ak��?<}������,�F�2�i���1c��b�+���	�rkt�=|��|}����k�뷇6QL�Q�Әk^���� ��@�z+.q&U���e%,}�3�XɃ#Y=\3��������1,J�A�J^iZ�������#��=x�;):����Q�5j�F��
�/0�f�*���	���?�"}��@������Vg{f(�C��)�b'��Ço?�6`���?�=m�0����[��������C�܎��؍��Q7��k�e��o����{�(�o�a�.tF� �U8�����ç{������=|}���{�vڂ)c�.���?fx�~|���c��+���0�?>>>�D���`��~�3��H�їSL�X�j��ަ���8�Gt�vDf�1F���Z�/��T��-{Nr���Jѯ�x;wK�.���n��Z8i�"w`�lS�q`@:�wUZe;�c��$����md;E�0��b�o`�i$���hɋR�0UL��$������0g����(��60܆YAG��&��?*�i��LMx�a�D�^=|L�R���a 70H��?(�\���7N��m�$�iEX�����<<�V����A+ays�*���P�"��%�زtl�I ͨ;�Eһ��c��5\�L�۫T���;�L����|��o�>%�D�����%�\��Mz��T�C4w�|5�r�יˉH�r�.�E
�F���#8��Mƫ-���͟�l���{H����o�F(n��
�}�/f&\��<�Ͷ�E�v���+%-��_&5nY2�m���jx)AO��Lи����X�
����4�F�j#e����U������[9N� �#�A�*�2k;��&{�� M#QߡW���l��IQ�v�|pf	ՉO�-�͈[�	�=���)����,X�h!Â�&�hΕ�l��+�"�tD��$��栻%N#����d���H���v�hW1H��l=�i0m���=�������6�mu��N1=׈�D����ԓl�ʩ��ݘѣD���m�*�`�R>�ds�u#l)f�Ϳ�Ê��.����(��7Mz��nτ�΢֯021	$��Oykm��
l6�9�`\�޹��7��v�ݾ�Dt��U���|��O3��n��E*����Oo*�D�~F|��з��qr�ۜE���Y�A;���7~C?T    �d� iR�����������}���5b�5�[_��.�]/��rvϵ�1�ݖ�F�������,����\n�Rǉ��[0Y���&gV���}�l����٬���ŋo�Z�ɿ�.��,�R1">og=6M�PΘA B�%Mۓ~}&}������&���
6D&}��0*��嘿���%���S�"�{a�c�Q�cЍ����F�9 �6|��a���V��p�O�,\5�S[������v���r�U��A�N�[�,];6�б��٫�-&�&���|��w1P��x^��X�� �x�n�9}3�US<����q=[�l�}����&ځʿ��(�1ڞUyt`KbP�r��܌/����a�ك �%��F�Nxz��lFLBQF���}�������E'��.yUT��`��DQ|D%����Y�Qc��Y]V�
փ�4�g!E����Z�������
���az���>��-�mDY�Un
��CC�j&#{��_r�&&=$�\��	�8Zv�e�ُ��ai�1�*��i��?<]�b��̎)��WI-o�����Ml��J߀O�YѓKS��H��+�s�澆�k:%�`â�)N�R�.��D���Tp�hf�|PhnI.I�֑��_�q���z��H�h[G�4�pEjAno��f{5k�nn6p���ϲm��%�b_G�X��=FF���A��bs��i3i���㝈��(4�p�ws<8\-Ǣmnv�h!��	�6�Sb
Z���T��ב�A����j_���T�R\N�M���t�+M��,3޶�y�_�xɧ�*"�z0���#z����\�*����iqjz��p���?wW�0��ǳ�����k8��%|&̭����Ct3=�&l��A}4���TŔ��oC�W%��-�����VE��Q%��B��&��l���Z����xP���0��qYҸ�ɂ��=�1xv�_���ՀlO{z+Ƣ�5/r���m`@os%�]ق��i��;�1��-�5	d�������b�� P̊J�7�kԪ�fw[�a�2��[E:X�d|��g���/8�RD�k�y�H)5�8�/�+��fj� O��ƣ�MFǧG/��ȊV�lA+Lt��*�KƂӅ���5ð��$�x�Kw^��a��L��_��)ۡ���H�t,�8\7�iJ��l؁��Z���/�X�k?�Y7��v��:D�|�E�I��	��:t���� ";�8e<0{BZz��Md�L���2[��S�	G<,�q�N8M"��b��8��>�a�$:�p	&�j� r�waSCʘ�(Z�A��<�v�'v�sT9���Ā�twGǦ,a�G��H��XS�ǥ�}�/�q����`��6��o1~6GW>`�Vx�kV��`�ty��$#꯲ʐ,�~�N �P�
�rݬ�[��3�$�\� ���Ϭ)�;�q�Rx�S	
��XcӉ���l0�X>aic���y�Uv���̫�:�d*}�d)"�<�\-�k��xj~y�x����o�/��~DpQ�K0m~A��?����|��*_?�z��0��	�W$`���o��7>E<�Y��O���շ���q]��u��>��?�ᢊ�3._�E,~�zqQ2���3aE��F�z�g�A�LV%�X0����9�w�Q�
÷`-���@x#�FXA��UI&�� h[��Y���jZ
N�fX0�fki���3�z��Rs�4l�2�L��]���(ј�b	t�oU�;�`� �bL��p_�����.�{���9܍�nu��rR~��Gmލ���_5�;&�9>���̇�=B�5c�1����,���^3�;|��F�r�v���MQ�b1F��G5�|��G�	Ci"���,���_������˧/OЋF�A�'z��������o�#N�3 ��?|���6�]�9`�t0Q\$-f�vyy���'�K����p�t�=�иN�KH��CW���3��1]	0`*�式u���W�?fS�8SV'���÷�_~��<=�����|����������_R�+l�I� G��W�ߟ�?��W��o%C�~|~��.O�?>����!_��}�����u�Z0\B�>^D�P^7'�=�]�`��=>�x��/���i�����
[��Ïo���l��>~o���z?ldd�L@l^��������/���I���ݗ��'��9������0;��q�_��$��>z��cΊ*f�p�`9���M�s磟p��% �GJi���ﰑ��&��������1L�}s\{/G���޶]^�p��b���V��t�H~%1�]t���G��Tp<����9����J,��C�q�����`��@�&ՅcDO���l���Vb��D��
�,�҈��O8߃��	�x�O
��W��1��������_�4E���Ę�m1�;�KC��9�\a�BD/2��w'_CW
�*��=����ٿ���fg4�O]l��[ƕ��Y�#v<,��X�2�;���Ԙ-��85��WZ")�賺�&,�^�f����G�0}K/LG���X��Ӗ�J
F������RG��Q,s��@��!��a�x6L�u��4\�b=,l�G��7#��f��ZrV:�����k���� ��X-�)+	��U~K��7���|{��lO��qMN��']%��!�"���Е6O���B��i�b(��|�FV�d0�%Zd�a3\?�F�u*(���m�W�c��j]a�ԧ?cʅ������ ��j��EA�}��<Vt�s��JL�SAk��-���z9��Q�F��v`� 8s޽��!l�@f�
������.BQ������rb>Oz�� ��3g�̩�'"�>�JuP�ݻy�=�My*x�
��t\�x��e���fW�H�#操�ڥX��Q혿�y���[��m\KX�����p�_)�g�Y��"l�]�t� ��(LܗNAc5��f�{��2jS`u�$^�9�՛6I0�?|����S�����÷O�׃ѣ����?}��T�2���LA���-.Ԅ��Y����f�|y��-�U�������������c_�F�,7�Ec^�o1�1�V�5��$Ht�"(}��4��4��U��:C�D��!tKkta�\�3�1T���;�߼��"�p`�쾼��;㯏#���ť�U};][.�	��<|���"h��'\����3O>=|��=��0�%v����1�����П���?E��[�|>���ƛ�g�_��3��m��Ǐ  �S�� �_�F����%��߼|���X�Zؒ�l XL��I�cGL�kޤ��>��DU��O_�7�^�"$l2���r�����oZ�(Y3�J߃p}�K�XV8qI! ��N ���z����6��N�c̀*PÁq7o���
����s$�H����wp��&;_I��WQ=)��u.���o_>>�
�����/�����CD��ulZ&E��*����������"T(�����x����� ��K��Q�1h���>��Ѫ�_x��\��_\�*��9�toW}1$��0A�;iS�Z���E�&��譏�+�$gHo�v;��h����L_�ٗO����l�ɻ��T�aHr�x:{���C� y�v���mWҬ�[��c��X�]�ʟ��.zgN���J���D�w�|z��D��ό��=q�I���G7�Ǔ�q�1|���{���//}�a�EPQ��D�Ӹ�NS狈�y3}=}�ұ�:��\��1�@ �[Lg�f�v�X���Cv�q�(w!g��j�6"��|t�m�ƛy�X����[�{��M\,���ٗ�O��A�j�_c�I�¬#q�J��v��ly#��H�w&�c��3�Z3¨.ֈiҚ�T31�o�́E���|pn ���]EPG����h��a,#{�ڤ����0_�e��A/~����/��L6��p����`�R�yKt7_�m�3��׳��    'iڍĊ
~�) oZ��c����P����*VŐ4!Gb���Dd��h�yo�~�>l���")�ˁ3w-�)�	�UǦ�ٶ]�6 !����ǧf��ǧ���d��1���*���76� n1�i�}��A%�[η�i���ك~+k�up��� ���屁�TO�&�����k�L���������	�8S��-�4���FiC��pym�~ ��u�`e����@%���+|� �U�,b�֥�`c�բ����VVdzR�fD�T��5�������O���mM~[�X��j���]��ZUMR���`m���_?5߽M��H��#\/1��{π��-��4Պ�ǳa��>4l��Zk���"/��WKE$�R��*'}n�Yޛ��A���
�+m̀�2�:�T��Y�[�L�����錂�q���;gP�2����!iA;��(�5��ƺ���=�Y.=��J��ӓ���T��I����"`�U�*��d{�ڤ���������e��Kx�p�3� t��|Z��6RHKf��u�#t�؞�돏�����?>��x^��c���r[��dE��#'���6�o�U>����Yɇ����vAGn�y���mE�ft�%�*��	%���f�G&�nR4O���>f��6�MZxQ�ؓ�靷3wS?��*�.%��������6�����"<���X ̫1X��&<��D�0��5��L�}�(U�]��ü\	]MK�����_����#����)�3�>�P����o�å�+Lt�Ly���h?��l����-yܑJf�α��l�.��ya*%+)�ؓ�M(٥�Ԃ���[7�.�$�FBH,ƴmVM/��)�J�q;݇V
�JUV{�᳞p��}({�NԶ�0�M����e�$��F���5�����k6"Jcd��Q��2�p[Xwi���KQ��
���.��*��aW�E���}g�.����/+��)V���}߭�!����۽.�����q�>@x=ˆe)��Y�w�X&�X;B�/�;�՚_��痏���wV<���_p��~c��mE�Z��՟-3+�]p�v-����b��v�܊�1�[U�/i�a�\5,w��@��Vo��l؛��]*8o��C�մ߾^\��Qz|%L���A�C�>b�k����H$�@by���f�/�n~b���0��2�9$O�T�a=�RDHC�x,�C�$ �vXe"'�$�����'m�;�.+pu¥�V�������.���oHڧ':%o��K���A=X*��M��|�&�*c�
J����D%/((B��o��$����s�ce�#	V�4�X����ѓ��������f5�ctئ�h0#Lp��ڎf��bb�?Zb)Ao���ݫ������&~�V�:h��j��-b9�"�[��ET9�� v��w^�`OE�P
�8I,����N����/P��+z ��4%z�Ʋ]��V�-�R+1�6���1���8$��(l�D���pn��"��[�
���6 ����m"�}���<T_	C���t����)�`P����naJL�������K,ŗM4��'�u���Jr�EC(�ƏŲ4X�����1",B1�"l���4�W�$���Lbl�@�c,�W�E(e,�02F������_�`�뱍`�×�Bx�GM�h�+���?�n�苉/h��0����N־�A��B8$������ED(F�"$�넜')��t�U>i��`��
*-=w=-Y��
���2���:H�����-Kb]�N21��T�SB��>v/�{�Mˮ�>)أ�ʜ
:ף{����:�ݹ#�L�\XZ���p*#`�������j����j�T��h��QB��(�i�@�[�>t+�w�=�l�b��� ]T0��RAoNb�1�;�WmZ���&�T���,��8nQ+}�p�Te���!�,���2�����(��-��uP��6I��D���X�n�$M��?H�	�3��Ȧy��半lkHc���V���D@�2I��IZ�Ib�J�Ib�	�\&j�&��.��_;ُ�^z�[�8�*	*��[�E��ʀJ,���_�JA[��pi�����E�V+�R���n�uRH����[7�S����	F�K�ӯ%E�@����UGU��W/;�Q��U���V�NE���^�<-X��L��U����hU碋��b�/֙���֜ǂXZ��I�(UJ���&.��������t� �u����i��}�5o�D��o��`��iP��C*��+�M�*CCX3�1b�LG �2��a��i)0�2*JЊ�R3A9�^��[��B&J4�]bA�0.U��*fFF�����T���B�Tej(��;��Rޜ�h�%�z��L��m�B��Y�b��֝�D*���t7V3#���g��^%(z��BfQ���k�n��@L��I-[CG�8i��$K��V֩�(4�_b�2�1��Y��)HZtlH�����V���3w�Z�6=\�yw�X?B9���RR������tY]2�P��x��z�M������F�͔G����"3+ca�qMw]$�>����l.��������]i�h������v�X�3/��=��gL|�0�\�$Jq��+��W�q��N��s��^��qd���V/%��b���x�h�V�;XO���{�5��j���p��j:M�Vu����բ��.� �LM�L����a��Dc'{"�V��;��K3�QeY@��=���N�ߵBs�۷׻�j�o��`�������v��ۗ��+>-�����������c�ct&��KÖ�;@D,���}�X2h�4�n�0�&�$-�'����e�d�A��6�`�g�阦3��u�gI,�u�ﷱt�����t��A�XkGѷ���&��f�W��Ī_��R���������/eF8�\�@Y��i��׍��>���{GLH������P�H��K5�h�煖�6OK�������w~XW s�<qzbV	�@&�D"������w�vL�kӜ����<���s��g�&u3?��:kuUM�)��5����G[@D׶�G��%�v���ON�K�@��ˁ���>Ǡ y�5𣫵�WEB���W�>��j�J��x3%8w8�)�@��g�"P@bK�Þi�������0��N���l�7}�ԐMj腴ȱ��#D~�!��sv����|���� �֦�H
ݽ�þ.�tM5���ىDfV蝹͋�����q�ww�s����_�P������񶉾�یB��>�� ~х	?Ɣs�Vc�v���=&ݣBSߑ�ȯ^=����sc���u����o�no�>:����WL � O�������������?==��m�=��֜2���>��iY�cĕoFC���@<~z�\��2�(L:7�Q��ÇO��_�>?4�Gf���~D(<�J^��s'��,���7�=?5�������uGb��j�)sb�:}~�'u���Ǐ���G`򖟴���B�>L3���K�[H��PZ���p,;�3�oO�|x�HSR��˻ے(d�_>< ͨ��˫i�5�ʯ�ͧ�ϟ!���߾���t���Rs����)����M
�&3?��Φ�'`�X��lw���/��f?�_`�!<���'�fo�`���>}��n�X{;�J���ɦ�%I)����s��%�~E^d�Q�+��B�Oy�||x���D����H	����:-�w��d>e$l�J1�5t����M�@����m�ʟS��|z~z���<���3r7�Aݹ0'"�|K�����C���Z�P���\��vl)5}�0��8�%=(nEs�b��hn���擔A��ٷ�*�T��Y��I>���`2r�2�Ȥ*Fܤ3:_�����/�G���*?a��vu���L&L����/�e$r��Ĩb�r6S�����F"d�誸�>�ho�ƻt�qN�����+�J��~    �y�)�<��ċ����hJ:Ձc��^��l��(���ݜ7?w@5���⺹)nw������@�=U������GxYU}���{����vû�z\�!��P����I-*��}���B�U��Ԧ{W����ꇶ���c.�2�8e�8`J�z��=�FE��84�Z��H=��-���~��#���A~�����1���uYk����ϔ��0 g^"Wy��3�Ϊ��[�뺾�@bp��Ne\\�1Er?`&����R~��T��i�\�כ�ۋ����a�x�r�J^�!��t�n�����B�Ks|�v���X�8��ّ��@�i��4��7f�D�4���Eb��C_.u�j�m6��&�F��<{3/K�ΘP'	�n�h�Ǐ�ͻ���+m�w��cx��3���@����6��b.�c��@
"����~5@������:܅1#�j6?��v��6k�g.��	�@�@"�JS�� �&PA���Va �	+�M� p�Ӈ4����x�����C�ށ<d<FWO�����@��w�:��w%��A��k0 �iʘ�MBU�W�~�{l�h�T8\�hri�kb�"�젲�{b�khO	C� �#ni5-�[.� �ؓeĿdn��*��,��6���jmr	 SǤK���mܸ�ERvV[ח��2�znN�OO[58a��h8�Я��΀�<T|*�h?x~�Jה�w��'4X��c�n�Ýw9�g���yqE�m�?�a�����s&��� 
�y��_��d3`���غh������n�'����m�K��𞹃��L
e}t"[�_n�\|���*���O
�|CJ�����7mv��;�8�`X��s�3���� r�?↾� �C��Bؾ��֝*�)p�~qlq��;�\]�8x��P�Q�zr���HU�ң,_�
��p�O��ݜ��W~��ds���Ëfhq��q�,��h��cH tq�|�w��o�N�Na��] 1tS�{��r�x3`����ԧY���]�l��^Ǚ79b���:c�A���>?�)�.�K�w�`R��y O�
 #nv����i2���ɀ�|~=�Z_g� gp��SGjNT�> r0��T��(�ݱu�uvz���=�m�Լ�x��<����e�{�: Vc��� ��HG��W��ӌ�e8����{�X��d�� 9�[#]�w��n>��4O���f�Ā?�z���/(_����٧�jƢ�y��2���<��sxuy�o�%u>K>�I;�>@��`)���������7/��>Zk�e*F�������Ys�]��^,B��I��݆͋�7�ێz�}r˹�b<?�>}~����1;CH��HHa��91D@F�%��������	Q�k��T��PE�6���HY�6�k��6!Y;8K ��\��1�K��%������������ @�t5� �������f��/�%Yg$�9�:� `����7�ͯ��_�����C3�1?�$S�l���r���4w����Q�޸c��ﾇ{'o�J�	�0�P��]���W���֌*�+T���UDx�������7�h�\ɜT��Pxk���8��^afm������>u�׉@���N���W�mN��+Ww0�$&6o�lX����=P�lG��o�����K�▀��zck��\�l7bHt$�������ڌ�s�p�Q/ޜ��9��=�C�����t��M���ڿ��SMwLi�&�`��.���$���������=d`*ծØF��?�.�o�<��0X� >+� ]F��|���Γ�>�s�A]���r���F_x�̷asm��Э�K��a�w� n�?��X�01�t���CbBwZZ�}�1^'�]_m;>��v��/o �b�WCV�����J�npf	d��F鷺�E4/<�d����쀩y~��g���ۮM���@g����6�0,]c
�ܝ�G�M\f�s$!���-5��4�p�eŪ�X q�>5�-��a|'�;O��r�^�q�+m�jg��7�9��W��"�k��430n��y+Կ��+bݜ�L��	�vm s�^�zE��Β�t*�Y������f;�ӜH�jo�.a�rѺnF�i��t�a�	I7=dJ`\���^��@�0�'o2�����3Cp�
�\k�=��ЭPV���f�"���K=���~�/��P�� kj'9`=� �����?J���?��D�s��S�~��Z֋��P���2�!2��N�U���/@����.8qV
�؂K��.zK���#�����f8>Z"s/㹩��#��׏��h���N�֧	�>��% ���J�����Fy�B�����Ɏ�j�gL:��0�4�M��h#�x�fq�hs��9��J��B}a�pAl@�]+\S�7:���`*�uc��n����ќ�Ŷٜ�'eאm�x\�EA��v�"ڮ���g켹��d�>չ$@�j#u��.���qh��%;05�Y������kh�C��e����n����vsr���o�6��뻋�,Y���^��t��Ƚn��q pW��_�Y�TjH��q.��Z2j�G�yqdY=-���N���r0�u%a@���?{����W�����u�j{��4���Ls���:����͐��c�¶�mm�.ݞ5'�g��^�����b�r@�"j����3�	P��ݐ-K����ߟ���<}&[h=hS�Y�� �[��&�l>�Js��"G�Q1&��K`�v�H2�C܀�Lml�?�԰k|����@����
��=P-P�Q.�3w����M˵̈́s�d�'63�����a����=�vx�B=��7��`��d�!`��v0�~<�?�s�������f;�g��mn���@�j%LG����$5��p; ��u����T6�P�M9����uzS��oo�i��Շ��>��Ȇlef3�,�g��W�y�D7�U��l��j�Ewq��\���w���lћ̘O�ݓ7��\�/�Wۛ��0����[�ۧFy��(��R�P]���.��������\l+��ӻ�˟?>� ��-3�h}w(U��S�u��o�i>e�6.���fC�'�iѭ�c��?��!dcm�X��\��*�������!�z�G����d���1�]*�_m��n�C���Jjݠ5�u9}M��G����;+  �����ջz��
�Ϲoc��+}6��i��G���0���t |������������}��Nr�P��V��s��\�hLJ�6�V4��yq�3/M�h�a[�?M���j�����-�c��^>g�R�� ��NK��[^C�ﴧ�G�͛����-�D��l}(��"	,L�-��pX ���ل4�=NKς���Ec,Q�3q vi�s:����,��z�J��_�vt[��2�w�
�x�/:�[ڂ�:���2\Gs���9�Bz����$�u"�N�X�*m^�g>��<���T7{+����W�'��_�>6�OL|l������5�0|I;I)h/�a�}@N���>u��?;'1m.�I�J�vVu�?�3IL9�6GOWN�t�4$I7c�ˢO�i�����/�!��0I���k�>e-�����:��.���8��ZN�;ѥ~���3�f�-IҶ���&N�f̟�-[l�wO�d�H�u���@'��p�$�&a��nAbj6 �$�&acA�aA�k6�.HbM�t	����W�xm�\��[��8� 8��/�qrA-2=��� ���=��"�p>Zݯ����zZ�L9Xg�C	{P�KuИr�@vJT�`!lB#[��ꡄMh��5=VQ'k�F4�5�\�>8��<��h�QMg6 /ӫ��/Fŵq��E�rU�_�ERJ�7-o<���e��e�r�f,x,(f>����t�H2��w�}��Y��=��hA3Gx�,�Rxa�g�guJl��zA[����hIK�f3���/����y,hje��<���Mͦq9- �����I\�V    �� e���V�~��*��U�yx�C�/��Z&6�)O=�J�f�ޜ����tW3;7t��s�H�ᛕ6��^R����~l�ڿ�Mv��p=������p�5��\���D��w���	��s�����g��䌤{�W���ج�(n�V�T(����}G��J>^��L◞L_�%��qoL1��g%�s��3��f����-TV�?�ϧ��c>�^`{{����I��J5Io���a"��@e���̍d�ѹ��!�J����6���U��>��t��{U�/�q�m#m�i���9�'�/L����5^|�xN>���p N���w���]��m�ogk.�0o�^�$��fh�|3�v�|�&$1�����#Y�f?'�6{�v��˱_n��l�>Ey`�&6>���l<�M+�/"F���C�UI#?��炰�D�qI�8��I���Dg�!����u�c���ݱy_��t�S���sY�M�wWY�8�Z"t��������T��˳���q1�!�	��t�2;�:.�	��ڢRi"{7���u�x���	�ۍ�~7�`B��n�o��������CY�:�8�cMύ /lr�n4
��	d��� Р��:���8gݰY�� Zt[A���4�*IVI������. L`�u�o�n�ر����l��v�)�`���t"�O�s"��'���!sF¥"3�u�p���&���O��|�? �(ct��0>B���4�SO+��x�`�zW�)��Kc�����qV���L^^@.^b���?����~��k�Z��B3b��s�+C�����+uݝ;���A�rCQ;������T�;
b`�f�ʮ�O��U�U!�k�����}'v;�W��2#QmwU햘�:����z�Q9�`�Me�0(��df��I!6I2��
�8�e'
Qz��W�ɒAmoי��G�)�&�q"�)o{������O�Q�	����I3-��I×@���L��Iz�s��f��d�;"R�e�ڎGdq&���+]�ɉW�����^a'�~���$��U�l<W�0�#��,��5��ZBy�N����.� �k�[(���O|�0�%����n�Ԯ����2�n����S��L$�$�F2m �/����w,������1�s��:��e� ��g�dv����4�ߟp]z�����S�r�9��_�u�^��1��v#(�)X�&ӶK�?�5����O�ُ7�w���۳3�3sl���B�}昍΋�$=Q�}��9�K}�P]�%Ӣ����N�Y�)�#��9�m� rӖ����o���9��
o��5Ҿ�_���[8�Q�@���c�dZZ�=-3?�'sO2��"�a�����������w^�r�(���X�@Q�@�1��*R�U1��r�X"ր�^�x��O�G<�i���}d������-�]5f��%}՗^&in���۵rNt�!q��qO��wf\�I��h�?�?F��B�S(�us�:�s���?��,_���Ť��A�UA��צ�\qae���<��CxAr�!Mw�n��A��l�+����8��}�1��lӟ�
�"�|I_{�ٗ^�ܜU2�Ԝ�n�>������8̄F	8�p$��ZV�*���l�����W��ʍGP�ҁZߞ�^d�/�koAQ|�����c9��
�#:	.�ICq䭍DZ�:�_���I�Yq=Ʒ�B��|�a� +(�;��� ��B؂��^O������tt8��x��Kˇ�u��I%=�3���TP,*Z�e/�|-r9z*�.M9G�Bʋ���Xμ�C�C{�*�C_�*�����?>7��������f�����4-/�S�W�^]���W�82�4��`7?����]�~RG7�$�=�	�IwoJ(Lr��#�[i(�w?�w��^R�w˯#R��Z�=N�b���ͫ����U���ۍ��M݀�/m��5b</K֯k%0z�����n^1^�Z*{y�����$%(:E�U��5����`n;c����Q�����xe���  N�dk�� E�ؼ����M�L�$�p@(����*�I���3� �K#��?%*cKY�߉3��6��:VX��4��JĂ�ҿ�θ��
R7�1��������e�]G��"��8�� ��Y����m6�
��^ֻ�n�\b�6k���3
c���z����	�|݁��'(��0��"�vY���+`J�l�ǐ��P5�5��fU�>l�;���L��jr?����RMx��Z��xA:y�cq���v�L����d��nGO�+�8���#�ZE<�2S ��F�2��[SN���H��mj��;=K�t�+��\���b.|sq9�I�@l��
]Zߜ��O��)��q���zX:�2� ��M��,�Ή8�h���r��+&Wy�0]j֡7ua|ZR6��ˬ�tU���~�$m�Y��a���?���h��.��%�v+[� ������/5n̮}�M2�F*�$�u���R�.�����W;!;��m�:ٲ����v����Q����쒕���t� ��0�2�0�Q[@�n�"��Nyϭ�#�z��:�;������O
��f��)�D�-�W����mw�@є���JP��K܎q���q^Fe�Y�KVe��.��B�*#[�F��V�z|�
 �ʌ
K��m���?��9�����L���W�ﳋ�����
ƿ��\\;����+\Tn��T�����E��𧺜����s�}C]k�>e�)J��6�������P׋NB i��P��L�����C�f=}$$����b��%#���[�$�[mH�v|�,8�?��14�N6I���ae`�ϸ��v�mg�\2۴H�j�*��_r${�:(R��1�TV{�&`%#�1����}
�v6��(��|�H0yn{�#��(ĲG�O�$���?�����-+��t6Ƕ����v�z"��*� [�n�$�*�K��mo0 J����0�ͯ˭ �����<*> Ԓt��-��s������(vu�L�M�a[qZ��o�S'�f����n��j��c��Z�>���l./�����2ǳ�a����U�4Z@���,U�k��Z�UbNc��Z���RJc��Z���䩘�*;�HN��dpn���T�uRA��z 7�'Ż��H�+(m8�j5�(�hg	�E>kM.%0�y��P�"P���BE��b��l�"��S��+�v!�ԧX~��2=�dꥰ �y�o�c*��)&��1=�b�"�E����ɥ��t�E��Աo��[�Q`&���x��F����P���H���2�r)��� dC�	��/��ٓOZґs-	b2�@n*rAXR�{[��.�M���9#��7�*��;-�N5 m���>��Y G���E�ju[�<E�er[��_�U�,�,,�@�j(�gߞB�{�$D����F[��9�
���",�8�r^��׺Ki�q��-������Y�k��Z�ʶ�9_���� �2��H/�)�RlS=l�h�9�m�oywYLYl+��V���[;�H-Rk����T/��E6��2�ԭBpƕ�Ky��{h�Z�El� &0���%C)@4���e.�TPT`e��*��-S��U�e�r�W���v������˨�_�
��`$}4l�6��,����B�d�M'3���u,6e`멛e]I�ze[
c�HX&[H�b�b��Y�_����9���I.+[)\�DX�֔�Q����f,�,�ǘ@HD^9UPK��g �,���s�\��\���E_���Y!ib �ij�4�&6s���"ﰢ�˜�)��U]�:Xd��+ͩu�9�fTPk&�aߛ�)�(^ksE(��î
��Yέ��y�R$��e�����X	Tѕ����Ê�$ϺcU5�}aMJ�2��@
!J����9P���~�]��1�K嫲	$�:�
k
��+��?�֔Zh���Ƀy�Z�r��e.ڶ���6�
���L ň�@�t�
5K�tnJ7t���<?.|�    ;U�b���Gb�>Nw���9��Xt�?� ��Z�k�z�_�Ł�%M�]uf4f]v��� :�E@*��,s���jzB�(maC,��!K�iH�pCj��d-�bCy�V9�EʡBЬN�����H
�U��/<�H9D���UЦ�3 ]�K�_��K
�.�i�EV!��(3����J�9�E
!��|[Z�!prW�ߘAQ�3�dl]0c�%%Ζ�
�8�����Eb ��5*�&Ӯ����D$8&&�1c��$[�$�!O���)	��˱T�*Q��T�?H�+��X��U�_���1$�j�Z7�>Q���~�p�d�2ϯH�C�f��R�bN�k�q��3�Q���h���P����2�����C:��v���$
3f�I5� *�p�kA��0�~ ~F���*�S�_�NH�թzs�R��̿����� I8�s�H��@Y,6
�|)�T)e��W���U�n��t�r�N�IX���l�ν*��H�Á��fSX�IP���h�(J�ҡ�b%�4�a�*���"���pD[k�2j^ ���"]D��]o�J
6q�$E��6��NL���j��"���]��b�=��9���QCzw~s����� Ci�{�"X�������>=�}u�oN.ovw�g]���[����PW�v͘Cw�Nd؅�(:6V�����N���";ɩo�_�z��
Rߝ�4Ǘ��7}��l�ߑ^猖7�����دv�>?~�wsy����F;:͚.��5�c��d��;�p�󇏚�rq����ۋ��Ĭy�]g"��u�eh��E�=�|ɯM���
z�����y�L��;(_-SL�IE�=���GA1��5E�ɝ�����oth~�]?[��ר��V���R��= eqӨQ�ۼ��� ��٢X\�v�����H#�=�
�K$τ��
.v#dR�\��ӏ��ݾ���,��&&����eh��[X���E=���k��DJ{��u��B�dhѵ`�%�9�w{@��RQ:�ֺ���K�T�VNa;T�yE�$A�.��
�5o�6�����zv�j7G�g���ݛ���;�B��`6Tl��'C����:ǖ�t~�?C������O�-�9��z�EjE�캞m�HW���n;&�_�>��}�jmČ����Ўr
x~��,�}��<�=��e�zw~�S�֩���}r�������Ί�oU4�Ȯ��T��L��e��1%u;R�ۋ�b7),k��� ��QZ��l���Rwʳ�qh:K�7"�Ks�Lv�7�l�im���4X�'�2�	�Q��`'���D�$������kD�+L���);j��Ց��깹��f|�
U\�+�n&�L��wT�eǨ�j ���K�r9���l]W`���?� �O>͢A��M���9bgPW��(ة�`�2���@q[ �D�Ӎ�<��.�˽Se�<���3�ѻbW�ed����T�.{2;�9_��hU�NM�"��y���w1��Z�)�&ve���.��tfgJv)��t�8�άzn�J����QI���ʹU���RQ� �/F~z-]�$�:3��Y�`|�,��p�м��<:Kv��_PfY�=5�#�ҥ��[j2�=׶�i�J��fU03�$^�۩ p��6΅	���F����Y����1�K��Z͆\2�ǹnG�I-W�;:���fYו���C}i2�~�5�,aRN��s"qj��#6Qd��.��	�
�F���!M�uY$�R����z��4�L��ͼ�uJS24���?Ztv��Bi�l�䌺��h��&�3���F�� S�II���w����K�V�Z�tG7[؏mf׍�����~�|0zY��lM`?Ϋ_{a=��lh�>� �%���"H+�[��yѷ��sӂ@�5mz

��6s��3q�6������@*�8B?����Km�1�#�?�����^��9�臩�N���	�na�Ж�3�>�pf~�+�e/�6İ�+�iE�ڜ�ݳ�2�?�Y!O#?��n;���3̦ű���RZb���ii�,-�nWjf�O0�Ur�"E�Jf�˩-�˫�O�b����Ǘ@%qe�cf���Okb^��a�^�+��eQTqR�r}�i5��0H+�+�ĕ�\�.5���$���Q�Ϭ�Ce��8TM/~������1,D~��������Y�﫨{��	D(/�b}J �U���Q�Ћ�ńORX�
�b��2�z �:�T�XC�31
,JoN�-���c�.ȎBA��f�RL��s?�3��W��G�"��I庪�ف^�����R��/�^k��If�*Ƽ"8<J�-U�ꌁ0�Ka�S�ꌞ=zG)��*�2�� o��ɥ@fѯ3\RNK��?�%4�*u��BP��Y�B��sH�.U$�D�CQWΚ"_.J�.Uݑ��e�t����@/��i)���2�w��WU@d�W����PM��t`!�r��/B1K�$��Tq���@J�K:�{��/��p������ݜF
�L��we4�@/�0��t�,�0�K(�I̊)c
��
��S�����>��9c�G�M�Cu�r�Gf�uE�V3e�p��e_i �L�Z�Lh����=��X(4��>�Xh�e��XB���LJ:���������f8��4}����u�_A#��%�����}g�0z�Y}���sG/��B,\�������۠��1��o�I	��U,�D�GTK�t�/1s�O�f7�a!.�Z��4����_Uܾ���}�
#�qd5��B׺б��4�|��f���}�U�{_&�/�j�%�T|	�<I��P�}�T��Z}4Ћ答'S��ϗqē�(SE�Т�s�v���%h�2Fh_	�=����JR�L�/�*h�qͽN>n(:p|����e�۞�\l.�����ɛf{q��y�w�_�?6W�,�+>�7��)aq��c��:�����6��:{{qr��>=�{��<~����ͧ�w_�~{����g��v&���D��`Z|B)�M�	��p�%Vw�w��6?��`��O�Q�/�ĝ"3�N�i,���L\�����a����o_鯏�����m�� ��'��g|y�Vn2�SS_w󌾬�x�9:���}����y�ҡ��ݝdvU_l�ď`��r��)/ȼt��z�{^�n�OΚ���ۋ��5Iz{s����`��=�OO�_������ww���Jί�Q��{�}�Ѯ~�����f����?��1?�� A���n��έhN?cN{m7�걫�u��rYW= �MQ%Y��3h����$�	�����8��B ?�4-(?�PuV۳ۻRm��Jb"�B����*­��!dstq}z���>�1?P�,��6R��&�xݹ�|Bsu��Bk3��`($j�$��h7�/�B x�����H{���ϏOI&zMk�9)8c��&g� )dzE,bZ����̴����6��êS�,v��p�b�*:^�^�o^�z4d����_; ��#ҥLZY�ܤۯ�I����	_{������˛�-��������`�J��j�VC�ؐjwZy{vZ-G��6��g�t���xA*��G9
f��A QT�r`w��ha8:?����N��[X��Ê���ߛ��c�������熦-xZ~:�BRQ%8l� �T��#��3�O_�tL���@�M@�=��Y�W�Nh:9
Y%�VǛۓ�ӳ懳-B��/�`�S.���U�l	��PU�Gq!<�n٦uf�#ctQ����]Lk?�&o�ah�'�<Ty����-������rּ�7?]� H��67�����FP���o�n�P��8l$;<u;��Tཏ�2�20����V�U~����A/O.7��w�2��!���?L�2 �WHh0k7��P�;d��j<L�t���~s�\_5t���C�f
���i9k��;;=��= �s@�Qj��d	�pj>�!�Aj�f�N���%16��6���G�\6?�64�L�P�Mqp͌��&0� &�� &�bL�	RMۃ|f�:L�U�&&�
}ld����ndb�ZMg�2��o���V��!_h��X�jQ�0�E�	�Z�zQ�0�E�	m+�C�t 2�PG>\aJ>Y�G�[:_��<�iF.\    �*.݆@[�Z��s��sI\�~$������5��v�4L�IR���I5I�S�@�x��x=MT1ۉ�SɃ�9I�TҰ%�$u*h�*M�>�4�{TmRk7��n�\jmڝ�b���(�E&�w�?J�uy?���Ա136��p��h&�|�)2��a�j��9򨕩����V4򨕋xk!:�V��o��q]&ݍ�j���
�*)G��Z�a`��S��cPӆ�hM�/p��1�į�݇�Ͽ7�� ��5��$޵7[�����3:�v*�Y��ÙS�����g����.\�

Z�q��E���HU0T�f���0\���+L!g%(n�Ie�C���*;�t�f���P��ߝ���<
_1�Z/9��#2�R˱���Z-9��r���6܌T��F>���`������+=�B���23��Z��:0d
�Jc)�Z)3����Z'�oFU+�Dk*]+�D�U�[=�L�bw���<Ija�բ�Ռ�iQ�j2L��a�M��T-*]͈Ϛ�c5�6~��l4�����')xғ7�X'��;�-����n���P��L�A7g���������wh�+�)\��M>�6_�a:+�������8�L�w}z갺=��0�d��,��p�e��|TV_:�CLӉ�W�Պ�3ڜL���c�`j�[����"٠���q���lj=�יB���p~��������nF/��j��!Mܜ��6�go��Ǿ�ou^�qFatJ�U�+9D���8Xp���z�nG�اM���//��?7WW�m,$t�a��o"�iqR*������艭us�6&�(Oê=t#�ZC�ǥ��m��%�_��:�N������ok]=��w;A[<���L�Xo��8��q
�N�h�zk=��9I�L�8:��R#�z�=������㦤��YF�Uk��z.ݤr���Z{ʉؗ\���8/D�Ⱥ"y��a�S�ć�Vȫ��l0��1�����n��K�����.� �/ymܺ��I���Y���Ϯ����F&ى�&(&�VO6;8��I��ďN�Q�A	�'َ��O4 ]�ƃ�+�Iv�2G�.���m�0X����Yp��k_�K���7�ٹ'�yu��H	Wg���!|;3�V��YaHן;�J��sct`�O18	B�\�E�lJ�@��t��|a�Z�{�����Nw�F�>�ГZ}Qs���[QO�F�|���`�zj��&������a�D]����{���_��[����ٍ-�uk;���?5��X[7{(��~�[��d߾��i(�Ω���k�X����Z�΢�u�]���E�:�L�۪�����u���ҷ���nX�S���ž0vd�k�ܭ��-4��]�� 0�.��Ly��ܐ��H�2?�6��쒍��!�J!Ch��� RC6�V�u��Վ�	�!�$JH���P��=�
�AC>j.���P���	�}/5d�sb��@��&6���Z.�ܰV3�r���>r���<�5#6�}�AC]/.�r�����j�d�"�0��ؼ��Qq�䆵GX).�ܰV\��dА�Zq����!�Ln�$��drC&�'<7d�؄�ذ���ꆤzRC6�]C6j�&<"�Z��&<"���Z�	���Rh6�naҧلw�`��`�JJK�ܐsLuC�4���2�0Jl虚-7��ܐ-.c�l73Nl���ܐsd��#o�$�c4º6L2�xI��IƸ(-W�$�H%a��L2]C��-�Ln�NWe�d��Lq-�Lא��I�k���2�8��w�X�JXԖ�%7��<����%7��!Kא��1�t��Kא-���5d�X����rL,�0H�ab���Ʊ	wL2]C&B�$�F�:�I�k���d��l�=�LאM�g���	�L2]C6�I&7L|�L2�R��%����ACn�y.�ܐMO�����.����\�
��AC�C&����>�$\�&��J�d�|v&��!�L`���	�L2A6("�YGA�cd���>F&���ۣ�L2]C6�I�k���$�5�}d������ףa��_�뇆����}C�{P��~h�汾���.az�{=5�A2��{�АI�����k�t�������K}�%�7�}�%8a�9��^?4�9��^O�����^?4��d������^O��f��������ɥ�{= ��=���9G&��`	��^o�%ܙu}���!	wf]��QI������CC6��^o�i�p*��^?4��d��|�L2]���Ե��\��=�`h��X; ����� 7���v ���d����\2.A)j 5�⚩ ���Ii� �r�\2�!�LL��� }C�i� @Ci]3@ߐsd��-,)ΑI�k�$� �0J�� }C�G&��!�p� ��&�f oU+��9 �����ୋҁ� }C6� ��?G.�ܐs���� }C6���5d�f> �:҄s�D����7d��| }C6=��7d�| }C6����+|�&:�!LnȻ���i�INڣ��o�曹 <��������\ ��RC&g�L0]C>&g�t2�w�K�sPC��`. �h�0 
Ns�\ ��VR
��.YI)��o�F�\ }C����}+�5sx�xI}d���>2�t���d  &qd��2�����d��o�92�x%�5s�P\�!��M
���>r�Ё+}�K �§�d��s�G����0@N�8r�Xqz���=iz��k�?�%�t
s@אs���`�d��`���kXs4�T|�� ]g%�L2���p@װ��.��O�92��M�] Q�!���������msrs���F����>�%ﶷ�<V��Y:d���b�{��y��BK�<�K:�����ߠ'���k�Vq�fV�8,4�_��R��_D7Kgt�`� ���i��A�}s�R��#@��`�2(ƭ혨�LA������b{ts�3�����i׵r~�Q2,�^�� BO85��0#� &`I�f��cFx�'U��*?:2�0 �E���J���)ꂁ�
12@���h��? � P�֬� U�Q��|�T��>AZ/�L�5%(�\*2j�H��l!e� �oFFH+�K�12j�H�t��~�|bY�WE� R���T��Yzװ+�g����3��d�1�a"3K�#	�Y�I���h�H�YX���˰m�B��b����%L�CΓ.V.K������Y>^��gXN�c���׳�][0Y���D�"1�0)�D-N��̒�8M� �0�0˺/�Y��uY�`+�eX��E˲�_�,���tK7$��L"�� eȈic�U�eV�0#M��qI�c�U=N�@�O�]�*�"���!�D<������c"Bϰ&<�bZ{�����/-bD���*��o�C,WK���f�x��Hj����k�"2���G�P����>��z	°L*�H�,�x�-�pð*�S����ŵ�r�˜�P�����|X�K�*�Q�xf���YAO�V:B�&���r�jF��BR(�c��ޕ\#F.`Y'���U�Y��`cSH(j�I[r9 ����u��Y�Ԉ1����JI�c7�%QՑ�%EITl�g�Ps�=ǈ��M#�t�_�z�!�����p#b$˹Ũ��M��baPp"'>m�@"F2>o�Ihζ}s�*l݀��t�!�5%�č��&4�ki:��$�v�UJ��+�0sF��+z�	�"�kMЯs�ܜ� 4q���q&�3L���e\)��a�.F���f�z�3A�hR���	ìذ��Cx�1z@��x ���Upz	h�0��ʮ�΍v���Z��=��K@��E�	=M j�����Ɖ��q"t�b@� ��)z�����KIB�9d�� 
4	�\S��#2�%�	�P�8�H�c����1Q��-�C��*hc~izt��n0���-��%P�a'@/�9ڄ�(�d�fC�;��u�/�Hю��0��C�`�Ѯ�J�#�z{=�0v�*:UTc-N�7@G�%�	� �I-(� !  �&�	��6a�)'�_:���h9C�tj��Qn�e�w#��e|��ߨŃR$m	�MY���&���iXv�̥h���Ŧ`V�b�\��@5UI7�1�f[y��N��ˬ| ^FOh��HcG��%aX��p���h}���U*<�$D�	��a��a�l��(ǩk����d�.s4�W�Qq�FĠ��d�r��aE̼jg���
�
=�8R.Y��'�
�y2��a��:�LkJ^�N4���KXV �	;k�X��y� �4���a��!j&Z�'CY�%,ˠ�����`�v��,��e�z�����F,���a�/�0�A,��C�°:X� �թ���KXִ�>��G����0,^@:,���a��1aa�CX�#�>��a�׋����a�7��KX�|�;���aM\�/pXv�����0�<luް���<luް���y��a�K���y���g�a���,����V��`<o(e3�y�ꬡ0=Qn�G-HF�°N����a����a����a����a����a����a����a����a����a����a=����a=����a=����a=����a=����a=����a=����a=����a=����a=����a�Ln��dD��dd��dd��dd��dd��dd��dd��dd��dd�+�,�fب䆜��ʠ6�!Ӟhņ,m�F'7��ܐ���lۋQl�@3lLrC�11�Ƞ61�Ƞ61�Ƞ61�Ƞ61�Ƞ61�Ƞ61�Ƞ61�Ƞ61�Ƞ61�Ƞ�e�@3\��"�f���E�p-���Z&4õL,2h�k�Xd��2�Ƞ�eb�A3\��"�f8��"�f8�$#�f8�$#�f8�$#�f8�$#�f8�$#�f8�$#�f8�$#�f8�$#�f8�$#�f8�%#�f8�%#�f8�%#�f8�%#�f8�%#�f8�%#�f8�%#�f8�$#G�;�$#ǘ;�$#�f8�$#�f8�$#�f8�$#�f8�$#�f8�$#�f8�$#�f8�$#�f8�$#�f8�$#�f�����j�h�� K��v ,�f�����j�h�� K��v ,�f�����j�h�� K��v ,�f�����j�h�� K��v ,�f�����j�h�� K��v ,�f�����j�h�� K��v ,�f�����j�h�� K��v ,�f�����j�h�� K��v ,�f�����j�h�� K��v ,�f�����j�h�� K��v ,�f�����`4�1�h�c��� ��9 @3s ,�f8� X �p�� ��`4�1�h�c��� ��9 @3s ,�f8� X �p�� ��`4�1�h�c��� ��9 @3<�,�fx�X ���� ��`4�3�h�g>���| 2h�g.���\ ��� @3<s,�fx�X ���� ��`4�3�h�g.���\ ��� @3<s,�fx�X ���� ��`4�3�h�g.���\ ��� @3<s,�fx�X ���� ��`4�3�h�g.���\ ��� @3<s,�fx�X ���� ��`4�3�h�g.���\ ��� @3<s,�fx�X ��� �fx��A3<wȠ�� d��] 2h��. 4�s���@��� �fx��A3<w��G�] 2h��. 4�s����� �fx������Ϛ����ݛ���ݶ�=Rտ H5�3"`���]�����'I�         �   x�]�AN1E��)|����L�7S'rR!$�܀�K8mU�"�����0�B
#�s����J�Y��$u�rO�i�Fڣ�.JY��S������`,���u����ռ�#4���=�$���|
+����L ��*n�?�0tm��V��δ����o{ּf�;|��?kN\E}�Z��7����4%��t8�:������K�_1�_�"K�          �   x�Mб�0����)x��E{+�`"��2���V
I��;MЖ��*�&����4q�W+��̒"�H��؅�dR�ft9 �ϚՕ؏����b���m�p�Q*��	a��v��,��a�fх�<fp2�S��!c�ɀt�$��o�.��|�?Un`�}RJ� ��Kk      `      x������ � �      "   A   x��Sp
��q�tQ��s��s�t��I���KO�C0��B2sK�8��-L9���b���� �J�      d      x������ � �      b      x������ � �      #   �  x���AO�@���+�֋���vawo��(��x��6-���}ԚF�J����2�lv��7O����(E^o���"��5���<~��AڠDRFB����IH�����Sխj/2��fq����?H��Ԥ��	J ���Cdc=P�T��I�i�5CcrҰqzXV�_��oD9-"����C2q@L�#9kw˪M+��0���U��сb�^������q:+��nzb�J6�?��.V��v����A�h�C����Q�({��,��r�d�ˁ+�n��N��e��N�����ɯ��Ն�ϥ|�����atJ~G������:�]3J��n40iGI���qMΒ��ٴ$ ����L���rnx���m�_���      %      x������ � �      '      x������ � �      )      x������ � �      +   j   x����@�ަ����^��:.��)�� ��(��0�5����B�`I~(�Ĥ{��`#�p4ߙ
�x��m��m���5	��^�O.ϝ��iS���~��;f����         �   x�}п
�0����@��s]��JK�������O-�.�,G~�;q�K��ZD$h��㽹���	��k ��@�.;O�/ɑB` #��1Z���!��3��q(bB�3^�_��PK>h�o�����^���*9���'I��$Kq��U1�gW���'Js� m�����{LZ��Jc�W�F9      ^      x�3�43�440�4�44����� k�      -   X   x�34����5��5��5000�42�pM�L#NC� g@��K�w�'Hސ�����@����12�!z�J2�SsJR�K��b���� �g�      /   J   x�-�Q AC��0�`�e���ɤ4�.t�_�)̋J�Ǣ\��7���d�,�Ŝ�1ͬa�������|L!      1   �   x�u�M
1���)�@m�����H�.FQ����瀠�#���K#�ճ��ji!R	o�� Mh��;ޖ��3�Л"	"~U]�6\�'A���|�8�$�f#�&P��,����ՠ�A�&��>�[�ι�l'~      3   �   x��э!C�o��N����Xǚ��� �H�[Q�k=�`��pf\|3�.<OU�� ��<h�9n�A�I~�����|��"�.b���T����
+R���b�P���#��R��XHȃ�<��/jCȳ�A\���P.�B�܄�7:!
r�e�@���:�kE�/�X��s\셞����I�>r      5   �  x����n�0�g�)�.�)�c�dp�؆� @�C���G/R"�'�#�S@K��x��(�}�a�H8�\I������y��?��'{"�/���;��"b�#�n����o�������q��=:0E� ,1ׁ������_�("HgYE�.!'z����N���pX
�S9��-�� ���s�4��"#��"�UN���|v�����l+�Zݾ4�,A��k�Hd����@����$!T��_�YMrh
�̨/��mbH%ԗ����Ĩ/�i}�O]��˕�OQ}P_���������
�xv�B����0��n_/0�q�-�?�t�u��]Jc�-�c$+�w��p�dH!�/x�q&�6�����Z�q?��G��d���<Hg"n�9!4W�fP�;ӡSC�S����q�m��ä9�%o26^���6��(Na�[�8�E4;W]T�Ţ7�Pj�R�VY*�QqKB�dki��b�Pk�f{Ӡ%��յ[�TB,����%�qp"K�rj9�t?+�f	�D\�#ZPD���I����%���QjU�Œ]���*Z�J���&gS%���h�ۂ)��M������}���Q:��F���BFì�L�Y\�cŉ���뿷��1\������'����V��/G�	eM�N�:l��˷�j����K      7      x������ � �      9      x������ � �      ;   g   x��̻�@�Z������l�"U���J\H���'�a!:�}
Ll�e�Nv#�u���|<��E�m̚�r����e��M�e��U�Y��1,��w� و �      =   e   x�3�p��
u�q�Cbb�1~@�eT�����	�c �!!,�SN�O_��dc��� � G_O?6v]@�e��������`�Ad���qqq �+�      ?   -   x�3�q��s�ӆ�rq:�::�z;z"�*�.����� �w-�      A   "   x�32�4��4�4��4�44�42�4����� 5��      C   <   x�M�� !�jg�m*������x���I���i&�$�lT�!T�S��[����=">߾K      E   �   x�E�A��0E�?�����M(q��4�?����H~����r��Qk:�#Fm����>�����x����$i�5��xJP��X3b�E�f��|5�kpFQ(g.
��Y�Ba�-)��?`�ʳ�;ʷ(���r��.�B�0K�|�Q��4+�W�ű�+{��!�Z{��
�;�Px�2�B�]�L��>�']�B�/��P,��&W����X��e���{T�      G      x������ � �      I      x������ � �      K      x������ � �      M      x������ � �      O   ^   x�-�M
� E��pA��+Y�r�E��בA����ٟD'�rq�^��0HZt+�FYS�$�󦡋�QM�%b��:8&暁^�&����	      Q   �   x�MP[�� �vN�t���9QPCC\����kh���񌙱mhB�a"��)��,A��� �#f���e[��ʑ<z(QT
*��
%��m8���guL���B���JcM�+���#�c�_�^����Ȫ�n�m�$U�ej��S~�ߤ�o�Ӗ��Bx��7n���lq�]WʒP��iD�7�F.<m.Ȯ^Sװ��1-�L���u]�тSr      S      x������ � �      T      x������ � �      W   W  x�e�Mn1FלS�HꏺB�ЍDQ���0/z�ʅ�&1$h���=�v����ں�8��������y�����h��U,��T��(�e���g���r��Y��st�fR
�P�;m9<���r)��}[�r�y�ʫ�2|�K�Oe��jS�Q���s���0��%3�1��^u���O�YF��~�Q1�ޙo=�U�����cE�T�<�ß���#��!�[��֡yj2z�#VFoY�Ǚv�����%���b��l��V�i|q�>r��k�>��d�.c���`�_��6�YIY�)�&q$P��u�QTO�`�`x_?�j��]	-Մh�����S�'�"��6M�o��4      Y   �  x��V�n�F<S_1��$����2b8�k�:�\FkƦ%Q�	���y���-�`T�Twu�Ph��0U|�	Ǐ*�?!�QJr]m����7�e˰g��g�	-��� _J<��Ȓ�Z4�+������	2�'�q=|���
@k�]�me�*�U	�tO�v�:�`��\u���OB\P6�pH����Du�H*�x�ԫ�5�_u{�f ���;��C *2��"��)��X�*X�9x�P�w$<�g(�P�rX%��?7����@��|ݟ���R�>��q>r|G-e�>�N���)�b�f����	�s(�H���� �@�z�V�E��5-��)W�y�(%��/@a"��������P������WZ�N-��۷��͋]���o�W}��m��ik`}�R�M2w�sg��ha[F(��BGQ��V\���1�c��2��
���q6l.;����W��7նɄe�}��c.6��V
�	yN1V	�|�� V����e�����i��6�J�A�Z]��.�l��N[#�
9��$�d�ՠZ�J{>�֖�W�#�c�0��.R&�7���e���~Ʀ�>�_����>��p����B��(s]o�Y5��غ���8�I�w$�O���Yn�ñ�L����^�B圫9�z]���Y�m�-�08.1%*+λO:C�O<������T<J�B�Η��ZRq�x���؆ϰd�a��t��ON����T�u@��X?��iV>;�N�ʑ��u��i`�2X��Ǜ���*5��)6c�YTc��M��e��*���$]!�')&��[z\	@$�����c���|��f/��H���S��Ej(��M���yb^
�����}�E_83�t)�PV�ɉj�������;���}�<y
.�x��og��o���e�n^���?�N_f`�.����o��%v�?�\�)�09ܭnX�t�{�8�m�<�P��b���vv�}
�>�E��XcH��ʼ��b�H;H5*)��$.3��F��c+E,p�DN�w��N�8Yn0+ғ�"��>�Rĭ~��$�`>�ח����n�:O��yٕ,�.�r�w)�Km�0<q(~�9#��B��Dj��6�I�F$x��/��_*�ͫ�q�-
z�ڱB�p��r�惁���wt��g��UQ>�n05l:{aO���'�	bqt��Le��zus���ۗ�gs���f�x'�K��t�`��>�!8��a��O_FLF��?0Q�3      [   L  x���ݎ�0��������$\�Q
�J�R�ޘ�7�E	���;��C��D$$۟�̌M0��1y��)�&#N#�1Ύ�ުRI��t���3bD��͏��v�(��<�W|��J^�����|(��������]-�ET�
u���~PL�Ă�bI�ߙ>�B�c@����20�Zꭑ���8I�߃`�,���ܹ'P�r��#�w9ī����ڨRZ�� ^�QȪ���Ҫ<r0/"p�.�3+�I�9��rH�tv@	���,ꉱ�����Td9��.�l`i�⣺2rY\��� ���ՀL*�E��MR��kVdêY���܏!�
1�i���������!��̝.gߦ3��b�3���9�!�N��35�Y��zj^
�?�3j{��f��s<aaif�y}onU?F����5����\�0��\��A]>�C#k�bZ!�����Y_L��MU���Ps��ؔDWƤ~��8���1i���|O�f�",1M���~^�����wf'(Ԥ�'�0�}�/柒���8^��|�T��}#�	�Dײ9Ѹ".?$����U��f����q���Ċ     