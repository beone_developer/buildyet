toc.dat                                                                                             0000600 0004000 0002000 00000450145 13607356547 0014470 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        PGDMP                            x            beone_marufuji    12.1    12.1 �   Z           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false         [           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false         \           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false         ]           1262    17309    beone_marufuji    DATABASE     �   CREATE DATABASE beone_marufuji WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'English_Indonesia.1252' LC_CTYPE = 'English_Indonesia.1252';
    DROP DATABASE beone_marufuji;
                postgres    false         A           1255    24586 3   get_stok(integer, integer, date, character varying)    FUNCTION     �  CREATE FUNCTION public.get_stok(p_item_id integer, p_gudang_id integer, p_trans_date date, p_intvent_trans_no character varying) RETURNS record
    LANGUAGE plpgsql
    AS $$
DECLARE
    res RECORD;
BEGIN
    select sum(x.stok_akhir) as stok, avg(hpp) hpp
    into res
    from (
             select 'saldo_awal' as type, coalesce(sum(it.saldo_qty), 0) stok_akhir, coalesce(sum(it.saldo_idr), 0) hpp
             from beone_item it
             where it.item_id = p_item_id
               and p_gudang_id = 1 --PATEN 1
             union
             select 'stok' as type, coalesce(sum(i.qty_in) - sum(i.qty_out), 0) stok_akhir, coalesce(avg(sa_unit_price),0) hpp
             from beone_inventory i
             where i.item_id = p_item_id
               and i.gudang_id = p_gudang_id
--                and i.trans_date <= p_trans_date
             union
             select 'transaksi' as type, coalesce(sum(i.qty_out) - sum(i.qty_in), 0) stok_akhir, coalesce(avg(sa_unit_price),0) hpp
             from beone_inventory i
             where i.item_id = p_item_id
--                and i.gudang_id = p_gudang_id
               and i.intvent_trans_no = p_intvent_trans_no) x;
    RETURN res;
END;
$$;
 �   DROP FUNCTION public.get_stok(p_item_id integer, p_gudang_id integer, p_trans_date date, p_intvent_trans_no character varying);
       public          postgres    false         B           1255    17312 |   set_stok(integer, integer, date, character varying, character varying, double precision, double precision, double precision) 	   PROCEDURE        CREATE PROCEDURE public.set_stok(p_item_id integer, p_gudang_id integer, p_trans_date date, p_intvent_trans_no character varying, p_keterangan character varying, p_qty_in double precision, p_qty_out double precision, price double precision)
    LANGUAGE plpgsql
    AS $$
DECLARE
    v_stok_akhir int;
    v_is_update  int;
    v_hpp float;
BEGIN
    --     SELECT get_stok(p_item_id, p_gudang_id, p_trans_date, p_intvent_trans_no) INTO v_stok_akhir;
    SELECT stok_akhir, hpp
    into v_stok_akhir, v_hpp
    FROM get_stok(p_item_id, p_gudang_id, p_trans_date, p_intvent_trans_no) AS (stok_akhir float, hpp float);

    IF (v_stok_akhir < p_qty_out) THEN
        RAISE EXCEPTION 'STOK TIDAK MENCUKUPI UNTUK ITEM (%) GUDANG (%) -> STOK SAAT INI (%), QTY INPUT (%)!', p_item_id, p_gudang_id, v_stok_akhir, p_qty_out;
    END IF;

    IF (p_qty_in > 0) AND (price > 0) THEN
        v_hpp = price;
    END IF;

    SELECT count(*)
    FROM beone_inventory i
    where i.intvent_trans_no = p_intvent_trans_no
--       and i.keterangan = p_keterangan
    INTO v_is_update;

    IF (v_is_update > 0) THEN
        IF (p_qty_out = 0 AND p_qty_in = 0) THEN
            delete from beone_inventory where intvent_trans_no = p_intvent_trans_no;
        ELSE
            update beone_inventory
            set qty_in    = p_qty_in,
                qty_out   = p_qty_out,
                gudang_id = p_gudang_id,
                sa_unit_price = v_hpp
            where intvent_trans_no = p_intvent_trans_no
              and item_id = p_item_id;
        END IF;
    ELSE
        INSERT INTO beone_inventory (intvent_trans_no, item_id, gudang_id, trans_date, keterangan, qty_in, qty_out, sa_unit_price)
        SELECT p_intvent_trans_no, p_item_id, p_gudang_id, p_trans_date, p_keterangan, p_qty_in, p_qty_out, v_hpp;
    END IF;
END
$$;
 �   DROP PROCEDURE public.set_stok(p_item_id integer, p_gudang_id integer, p_trans_date date, p_intvent_trans_no character varying, p_keterangan character varying, p_qty_in double precision, p_qty_out double precision, price double precision);
       public          postgres    false         D           1255    17313     trg_create_beone_export_detail()    FUNCTION     z  CREATE FUNCTION public.trg_create_beone_export_detail() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
    v_trans_id   int;
    v_trans_date date;
    v_trans_no   varchar(200);
    v_keterangan varchar(200);
BEGIN
    select export_header_id, delivery_no, delivery_date, 'EXPORT' as keterangan
    into v_trans_id, v_trans_no, v_trans_date, v_keterangan
    from beone_export_header
    where export_header_id = NEW.export_header_id;

    if (coalesce(v_trans_no, '') != '') then
        call set_stok(NEW.item_id, NEW.gudang_id, v_trans_date, v_trans_no, v_keterangan, 0, NEW.qty, 0);
    end if;

    RETURN NEW;
END;
$$;
 7   DROP FUNCTION public.trg_create_beone_export_detail();
       public          postgres    false         F           1255    17314     trg_create_beone_export_header()    FUNCTION     �   CREATE FUNCTION public.trg_create_beone_export_header() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
    update beone_export_detail set qty = qty where export_header_id = NEW.export_header_id;

    RETURN NEW;
END;
$$;
 7   DROP FUNCTION public.trg_create_beone_export_header();
       public          postgres    false         ?           1255    17315     trg_create_beone_import_detail()    FUNCTION     �  CREATE FUNCTION public.trg_create_beone_import_detail() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
    v_trans_id   int;
    v_trans_date date;
    v_trans_no   varchar(200);
    v_keterangan varchar(200);
BEGIN
    select import_header_id, receive_no, receive_date, 'IMPORT' as keterangan
    into v_trans_id, v_trans_no, v_trans_date, v_keterangan
    from beone_import_header
    where import_header_id = NEW.import_header_id;

    if (coalesce(v_trans_no, '') != '') then
        call set_stok(NEW.item_id, NEW.gudang_id, v_trans_date, v_trans_no, v_keterangan, NEW.qty, 0, NEW.price);
    end if;

    RETURN NEW;
END;
$$;
 7   DROP FUNCTION public.trg_create_beone_import_detail();
       public          postgres    false         G           1255    17316     trg_create_beone_import_header()    FUNCTION     �   CREATE FUNCTION public.trg_create_beone_import_header() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
    update beone_import_detail set qty = qty where import_header_id = NEW.import_header_id;

    RETURN NEW;
END;
$$;
 7   DROP FUNCTION public.trg_create_beone_import_header();
       public          postgres    false         H           1255    17317 '   trg_create_beone_konversi_stok_detail()    FUNCTION     �  CREATE FUNCTION public.trg_create_beone_konversi_stok_detail() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
    v_trans_id   int;
    v_trans_date date;
    v_trans_no   varchar(200);
    v_keterangan varchar(200);
BEGIN
    select konversi_stok_header_id, konversi_stok_no, konversi_stok_date, 'KONVERSI STOK' as keterangan
    into v_trans_id, v_trans_no, v_trans_date, v_keterangan
    from beone_konversi_stok_header
    where konversi_stok_header_id = NEW.konversi_stok_header_id;

    if (coalesce(v_trans_no, '') != '') then
        call set_stok(NEW.item_id, NEW.gudang_id, v_trans_date, v_trans_no, v_keterangan, 0, NEW.qty_real, 0);
    end if;

    RETURN NEW;
END;
$$;
 >   DROP FUNCTION public.trg_create_beone_konversi_stok_detail();
       public          postgres    false         J           1255    17318 '   trg_create_beone_konversi_stok_header()    FUNCTION     Q  CREATE FUNCTION public.trg_create_beone_konversi_stok_header() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
    v_trans_id   int;
    v_trans_date date;
    v_trans_no   varchar(200);
    v_keterangan varchar(200);
BEGIN
    v_trans_id = NEW.konversi_stok_header_id;
    v_trans_no = NEW.konversi_stok_no;
    v_trans_date = NEW.konversi_stok_date;
    v_keterangan = 'KONVERSI STOK IN';

    if (coalesce(v_trans_no, '') != '') then
        call set_stok(NEW.item_id, NEW.gudang_id, v_trans_date, v_trans_no, v_keterangan, NEW.qty_real, 0, 0);
    end if;

    RETURN NEW;
END;
$$;
 >   DROP FUNCTION public.trg_create_beone_konversi_stok_header();
       public          postgres    false         @           1255    17319 (   trg_create_beone_transfer_stock_detail()    FUNCTION     }  CREATE FUNCTION public.trg_create_beone_transfer_stock_detail() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
    v_trans_id   int;
    v_trans_date date;
    v_trans_no   varchar(200);
    v_keterangan varchar(200);
BEGIN
    select transfer_stock_id, transfer_no, transfer_date, 'TRANSFER STOCK' as keterangan
    into v_trans_id, v_trans_no, v_trans_date, v_keterangan
    from beone_transfer_stock
    where transfer_stock_id = NEW.transfer_stock_header_id;

    if (coalesce(v_trans_no, '') != '') then
        if (NEW.tipe_transfer_stock = 'BB') then
            call set_stok(NEW.item_id, NEW.gudang_id, v_trans_date, v_trans_no, v_keterangan, 0, NEW.qty, 0);
        elsif (NEW.tipe_transfer_stock = 'WIP') then
            call set_stok(NEW.item_id, NEW.gudang_id, v_trans_date, v_trans_no, v_keterangan, NEW.qty, 0, 0);
        end if;
    end if;

    RETURN NEW;
END;
$$;
 ?   DROP FUNCTION public.trg_create_beone_transfer_stock_detail();
       public          postgres    false         E           1255    17320     trg_delete_beone_export_detail()    FUNCTION     t  CREATE FUNCTION public.trg_delete_beone_export_detail() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
    v_trans_id   int;
    v_trans_date date;
    v_trans_no   varchar(200);
    v_keterangan varchar(200);
BEGIN
    select export_header_id, delivery_no, delivery_date, 'EXPORT' as keterangan
    into v_trans_id, v_trans_no, v_trans_date, v_keterangan
    from beone_export_header
    where export_header_id = OLD.export_header_id;

    if (coalesce(v_trans_no, '') != '') then
        call set_stok(OLD.item_id, OLD.gudang_id, v_trans_date, v_trans_no, v_keterangan, 0, 0, 0);
    end if;

    RETURN OLD;
END;
$$;
 7   DROP FUNCTION public.trg_delete_beone_export_detail();
       public          postgres    false         C           1255    17321     trg_delete_beone_import_detail()    FUNCTION     r  CREATE FUNCTION public.trg_delete_beone_import_detail() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
    v_trans_id   int;
    v_trans_date date;
    v_trans_no   varchar(200);
    v_keterangan varchar(200);
BEGIN
    select import_header_id, receive_no, receive_date, 'IMPORT' as keterangan
    into v_trans_id, v_trans_no, v_trans_date, v_keterangan
    from beone_import_header
    where import_header_id = OLD.import_header_id;

    if (coalesce(v_trans_no, '') != '') then
        call set_stok(OLD.item_id, OLD.gudang_id, v_trans_date, v_trans_no, v_keterangan, 0, 0, 0);
    end if;

    RETURN OLD;
END;
$$;
 7   DROP FUNCTION public.trg_delete_beone_import_detail();
       public          postgres    false         I           1255    17322 '   trg_delete_beone_konversi_stok_detail()    FUNCTION     �  CREATE FUNCTION public.trg_delete_beone_konversi_stok_detail() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
    v_trans_id   int;
    v_trans_date date;
    v_trans_no   varchar(200);
    v_keterangan varchar(200);
BEGIN
    select konversi_stok_header_id, konversi_stok_no, konversi_stok_date, 'KONVERSI STOK' as keterangan
    into v_trans_id, v_trans_no, v_trans_date, v_keterangan
    from beone_konversi_stok_header
    where konversi_stok_header_id = OLD.konversi_stok_header_id;

    if (coalesce(v_trans_no, '') != '') then
        call set_stok(OLD.item_id, OLD.gudang_id, v_trans_date, v_trans_no, v_keterangan, 0, 0, 0);
    end if;

    RETURN OLD;
END;
$$;
 >   DROP FUNCTION public.trg_delete_beone_konversi_stok_detail();
       public          postgres    false         2           1255    17323 '   trg_delete_beone_konversi_stok_header()    FUNCTION     F  CREATE FUNCTION public.trg_delete_beone_konversi_stok_header() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
    v_trans_id   int;
    v_trans_date date;
    v_trans_no   varchar(200);
    v_keterangan varchar(200);
BEGIN
    v_trans_id = OLD.konversi_stok_header_id;
    v_trans_no = OLD.konversi_stok_no;
    v_trans_date = OLD.konversi_stok_date;
    v_keterangan = 'KONVERSI STOK IN';

    if (coalesce(v_trans_no, '') != '') then
        call set_stok(OLD.item_id, OLD.gudang_id, v_trans_date, v_trans_no, v_keterangan, 0, 0, 0);
    end if;

    RETURN OLD;
END;
$$;
 >   DROP FUNCTION public.trg_delete_beone_konversi_stok_header();
       public          postgres    false         K           1255    17324 (   trg_delete_beone_transfer_stock_detail()    FUNCTION     �  CREATE FUNCTION public.trg_delete_beone_transfer_stock_detail() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
    v_trans_id   int;
    v_trans_date date;
    v_trans_no   varchar(200);
    v_keterangan varchar(200);
BEGIN
    select transfer_stock_id, transfer_no, transfer_date, 'TRANSFER STOCK' as keterangan
    into v_trans_id, v_trans_no, v_trans_date, v_keterangan
    from beone_transfer_stock
    where transfer_stock_id = OLD.transfer_stock_header_id;

    if (coalesce(v_trans_no, '') != '') then
        call set_stok(OLD.item_id, OLD.gudang_id, v_trans_date, v_trans_no, v_keterangan, 0, 0, 0);
    end if;

    RETURN OLD;
END;
$$;
 ?   DROP FUNCTION public.trg_delete_beone_transfer_stock_detail();
       public          postgres    false         �            1259    17325    beone_adjustment    TABLE     =  CREATE TABLE public.beone_adjustment (
    adjustment_id bigint NOT NULL,
    adjustment_no character varying(20),
    adjustment_date date,
    keterangan character varying(100),
    item_id integer,
    qty_adjustment bigint,
    posisi_qty integer,
    update_by integer,
    update_date date,
    flag integer
);
 $   DROP TABLE public.beone_adjustment;
       public         heap    postgres    false         �            1259    17328 "   beone_adjustment_adjustment_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_adjustment_adjustment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 9   DROP SEQUENCE public.beone_adjustment_adjustment_id_seq;
       public          postgres    false    202         ^           0    0 "   beone_adjustment_adjustment_id_seq    SEQUENCE OWNED BY     i   ALTER SEQUENCE public.beone_adjustment_adjustment_id_seq OWNED BY public.beone_adjustment.adjustment_id;
          public          postgres    false    203         �            1259    17330 	   beone_coa    TABLE     S  CREATE TABLE public.beone_coa (
    coa_id integer NOT NULL,
    nama character varying(50),
    nomor character varying(50),
    tipe_akun integer,
    debet_valas double precision,
    debet_idr double precision,
    kredit_valas double precision,
    kredit_idr double precision,
    dk character varying,
    tipe_transaksi integer
);
    DROP TABLE public.beone_coa;
       public         heap    postgres    false         �            1259    17336    beone_coa_coa_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_coa_coa_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.beone_coa_coa_id_seq;
       public          postgres    false    204         _           0    0    beone_coa_coa_id_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE public.beone_coa_coa_id_seq OWNED BY public.beone_coa.coa_id;
          public          postgres    false    205         �            1259    17338    beone_coa_jurnal    TABLE     �   CREATE TABLE public.beone_coa_jurnal (
    coa_jurnal_id integer NOT NULL,
    nama_coa_jurnal character varying(100),
    keterangan_coa_jurnal character varying(1000),
    coa_id integer,
    coa_no character varying(50)
);
 $   DROP TABLE public.beone_coa_jurnal;
       public         heap    postgres    false         �            1259    17344 "   beone_coa_jurnal_coa_jurnal_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_coa_jurnal_coa_jurnal_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 9   DROP SEQUENCE public.beone_coa_jurnal_coa_jurnal_id_seq;
       public          postgres    false    206         `           0    0 "   beone_coa_jurnal_coa_jurnal_id_seq    SEQUENCE OWNED BY     i   ALTER SEQUENCE public.beone_coa_jurnal_coa_jurnal_id_seq OWNED BY public.beone_coa_jurnal.coa_jurnal_id;
          public          postgres    false    207         �            1259    17346    beone_country    TABLE     �   CREATE TABLE public.beone_country (
    country_id integer NOT NULL,
    nama character varying(50),
    country_code character varying(10),
    flag integer
);
 !   DROP TABLE public.beone_country;
       public         heap    postgres    false         �            1259    17349    beone_country_country_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_country_country_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 3   DROP SEQUENCE public.beone_country_country_id_seq;
       public          postgres    false    208         a           0    0    beone_country_country_id_seq    SEQUENCE OWNED BY     ]   ALTER SEQUENCE public.beone_country_country_id_seq OWNED BY public.beone_country.country_id;
          public          postgres    false    209         �            1259    17351    beone_custsup    TABLE     b  CREATE TABLE public.beone_custsup (
    custsup_id integer NOT NULL,
    nama character varying(100),
    alamat character varying(200),
    tipe_custsup integer,
    negara integer,
    saldo_hutang_idr double precision,
    saldo_hutang_valas double precision,
    saldo_piutang_idr double precision,
    saldo_piutang_valas double precision,
    flag integer,
    pelunasan_hutang_idr double precision,
    pelunasan_hutang_valas double precision,
    pelunasan_piutang_idr double precision,
    pelunasan_piutang_valas double precision,
    status_lunas_piutang integer,
    status_lunas_hutang integer
);
 !   DROP TABLE public.beone_custsup;
       public         heap    postgres    false         �            1259    17354    beone_custsup_custsup_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_custsup_custsup_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 3   DROP SEQUENCE public.beone_custsup_custsup_id_seq;
       public          postgres    false    210         b           0    0    beone_custsup_custsup_id_seq    SEQUENCE OWNED BY     ]   ALTER SEQUENCE public.beone_custsup_custsup_id_seq OWNED BY public.beone_custsup.custsup_id;
          public          postgres    false    211         �            1259    17356    beone_export_detail    TABLE     �  CREATE TABLE public.beone_export_detail (
    export_detail_id integer NOT NULL,
    export_header_id integer,
    item_id integer,
    qty double precision,
    satuan_qty integer,
    price double precision,
    pack_qty double precision,
    satuan_pack integer,
    origin_country integer,
    doc character varying(50),
    volume double precision,
    netto double precision,
    brutto double precision,
    flag integer,
    sales_header_id integer,
    gudang_id integer DEFAULT 1 NOT NULL
);
 '   DROP TABLE public.beone_export_detail;
       public         heap    postgres    false         �            1259    17360 (   beone_export_detail_export_detail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_export_detail_export_detail_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ?   DROP SEQUENCE public.beone_export_detail_export_detail_id_seq;
       public          postgres    false    212         c           0    0 (   beone_export_detail_export_detail_id_seq    SEQUENCE OWNED BY     u   ALTER SEQUENCE public.beone_export_detail_export_detail_id_seq OWNED BY public.beone_export_detail.export_detail_id;
          public          postgres    false    213         �            1259    17362    beone_export_header    TABLE       CREATE TABLE public.beone_export_header (
    export_header_id integer NOT NULL,
    jenis_bc integer,
    car_no character varying(50),
    bc_no character varying(50),
    bc_date date,
    kontrak_no character varying(50),
    kontrak_date date,
    jenis_export integer,
    invoice_no character varying(50),
    invoice_date date,
    surat_jalan_no character varying(50),
    surat_jalan_date date,
    receiver_id integer,
    country_id integer,
    price_type character varying(10),
    amount_value double precision,
    valas_value double precision,
    insurance_type character varying(10),
    insurance_value double precision,
    freight double precision,
    flag integer,
    status integer,
    delivery_date date,
    update_by integer,
    update_date date,
    delivery_no character varying(50),
    vessel character varying(100),
    port_loading character varying(100),
    port_destination character varying(100),
    container character varying(10),
    no_container character varying(25),
    no_seal character varying(25)
);
 '   DROP TABLE public.beone_export_header;
       public         heap    postgres    false         �            1259    17368 (   beone_export_header_export_header_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_export_header_export_header_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ?   DROP SEQUENCE public.beone_export_header_export_header_id_seq;
       public          postgres    false    214         d           0    0 (   beone_export_header_export_header_id_seq    SEQUENCE OWNED BY     u   ALTER SEQUENCE public.beone_export_header_export_header_id_seq OWNED BY public.beone_export_header.export_header_id;
          public          postgres    false    215         �            1259    17370    beone_gl    TABLE     �  CREATE TABLE public.beone_gl (
    gl_id bigint NOT NULL,
    gl_date date,
    coa_id integer,
    coa_no character varying(20),
    coa_id_lawan integer,
    coa_no_lawan character varying(20),
    keterangan character varying(100),
    debet double precision,
    kredit double precision,
    pasangan_no character varying(50),
    gl_number character varying(50),
    update_by integer,
    update_date date
);
    DROP TABLE public.beone_gl;
       public         heap    postgres    false         �            1259    17373    beone_gl_gl_id_seq    SEQUENCE     {   CREATE SEQUENCE public.beone_gl_gl_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.beone_gl_gl_id_seq;
       public          postgres    false    216         e           0    0    beone_gl_gl_id_seq    SEQUENCE OWNED BY     I   ALTER SEQUENCE public.beone_gl_gl_id_seq OWNED BY public.beone_gl.gl_id;
          public          postgres    false    217         �            1259    17375    beone_gudang    TABLE     �   CREATE TABLE public.beone_gudang (
    gudang_id integer NOT NULL,
    nama character varying(100),
    keterangan character varying(200),
    flag integer
);
     DROP TABLE public.beone_gudang;
       public         heap    postgres    false         �            1259    17378    beone_gudang_detail    TABLE     �  CREATE TABLE public.beone_gudang_detail (
    gudang_detail_id bigint NOT NULL,
    gudang_id integer,
    trans_date date,
    item_id integer,
    qty_in double precision,
    qty_out double precision,
    nomor_transaksi character varying(50),
    update_by integer,
    update_date date,
    flag integer,
    keterangan character varying(100),
    kode_tracing character varying(50)
);
 '   DROP TABLE public.beone_gudang_detail;
       public         heap    postgres    false         �            1259    17381 (   beone_gudang_detail_gudang_detail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_gudang_detail_gudang_detail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ?   DROP SEQUENCE public.beone_gudang_detail_gudang_detail_id_seq;
       public          postgres    false    219         f           0    0 (   beone_gudang_detail_gudang_detail_id_seq    SEQUENCE OWNED BY     u   ALTER SEQUENCE public.beone_gudang_detail_gudang_detail_id_seq OWNED BY public.beone_gudang_detail.gudang_detail_id;
          public          postgres    false    220         �            1259    17383    beone_gudang_gudang_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_gudang_gudang_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 1   DROP SEQUENCE public.beone_gudang_gudang_id_seq;
       public          postgres    false    218         g           0    0    beone_gudang_gudang_id_seq    SEQUENCE OWNED BY     Y   ALTER SEQUENCE public.beone_gudang_gudang_id_seq OWNED BY public.beone_gudang.gudang_id;
          public          postgres    false    221         �            1259    17385    beone_pemakaian_bahan_header    TABLE     T  CREATE TABLE public.beone_pemakaian_bahan_header (
    pemakaian_header_id bigint NOT NULL,
    nomor_pemakaian character varying(50),
    keterangan character varying(250),
    tgl_pemakaian date,
    update_by integer,
    update_date date,
    nomor_pm character varying(50),
    amount_pemakaian double precision,
    status integer
);
 0   DROP TABLE public.beone_pemakaian_bahan_header;
       public         heap    postgres    false         �            1259    17388 4   beone_header_pemakaian_bahan_pemakaian_header_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_header_pemakaian_bahan_pemakaian_header_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 K   DROP SEQUENCE public.beone_header_pemakaian_bahan_pemakaian_header_id_seq;
       public          postgres    false    222         h           0    0 4   beone_header_pemakaian_bahan_pemakaian_header_id_seq    SEQUENCE OWNED BY     �   ALTER SEQUENCE public.beone_header_pemakaian_bahan_pemakaian_header_id_seq OWNED BY public.beone_pemakaian_bahan_header.pemakaian_header_id;
          public          postgres    false    223         �            1259    17390    beone_hutang_piutang    TABLE     �  CREATE TABLE public.beone_hutang_piutang (
    hutang_piutang_id bigint NOT NULL,
    custsup_id integer,
    trans_date date,
    nomor character varying(50),
    keterangan character varying(100),
    valas_trans double precision,
    idr_trans double precision,
    valas_pelunasan double precision,
    idr_pelunasan double precision,
    tipe_trans integer,
    update_by integer,
    update_date date,
    flag integer,
    status_lunas integer
);
 (   DROP TABLE public.beone_hutang_piutang;
       public         heap    postgres    false         �            1259    17393 *   beone_hutang_piutang_hutang_piutang_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_hutang_piutang_hutang_piutang_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 A   DROP SEQUENCE public.beone_hutang_piutang_hutang_piutang_id_seq;
       public          postgres    false    224         i           0    0 *   beone_hutang_piutang_hutang_piutang_id_seq    SEQUENCE OWNED BY     y   ALTER SEQUENCE public.beone_hutang_piutang_hutang_piutang_id_seq OWNED BY public.beone_hutang_piutang.hutang_piutang_id;
          public          postgres    false    225         �            1259    17395    beone_import_detail    TABLE     �  CREATE TABLE public.beone_import_detail (
    import_detail_id integer NOT NULL,
    item_id integer,
    qty double precision,
    satuan_qty integer,
    price double precision,
    pack_qty double precision,
    satuan_pack integer,
    origin_country integer,
    volume double precision,
    netto double precision,
    brutto double precision,
    hscode character varying(50),
    tbm double precision,
    ppnn double precision,
    tpbm double precision,
    cukai integer,
    sat_cukai integer,
    cukai_value double precision,
    bea_masuk character varying(100),
    sat_bea_masuk integer,
    flag integer,
    item_type_id integer,
    import_header_id integer,
    gudang_id integer DEFAULT 0 NOT NULL
);
 '   DROP TABLE public.beone_import_detail;
       public         heap    postgres    false         �            1259    17399 (   beone_import_detail_import_detail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_import_detail_import_detail_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ?   DROP SEQUENCE public.beone_import_detail_import_detail_id_seq;
       public          postgres    false    226         j           0    0 (   beone_import_detail_import_detail_id_seq    SEQUENCE OWNED BY     u   ALTER SEQUENCE public.beone_import_detail_import_detail_id_seq OWNED BY public.beone_import_detail.import_detail_id;
          public          postgres    false    227         �            1259    17401    beone_import_header    TABLE     C  CREATE TABLE public.beone_import_header (
    import_header_id integer NOT NULL,
    jenis_bc integer,
    car_no character varying(50),
    bc_no character varying(50),
    bc_date date,
    invoice_no character varying(50),
    invoice_date date,
    kontrak_no character varying(50),
    kontrak_date date,
    purpose_id character varying(100),
    supplier_id integer,
    price_type character varying(10),
    "from" character varying(10),
    amount_value double precision,
    valas_value double precision,
    value_added double precision,
    discount double precision,
    insurance_type character varying(10),
    insurace_value double precision,
    freight double precision,
    flag integer,
    update_by integer,
    update_date date,
    status integer,
    receive_date date,
    receive_no character varying(50)
);
 '   DROP TABLE public.beone_import_header;
       public         heap    postgres    false         �            1259    17404 (   beone_import_header_import_header_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_import_header_import_header_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ?   DROP SEQUENCE public.beone_import_header_import_header_id_seq;
       public          postgres    false    228         k           0    0 (   beone_import_header_import_header_id_seq    SEQUENCE OWNED BY     u   ALTER SEQUENCE public.beone_import_header_import_header_id_seq OWNED BY public.beone_import_header.import_header_id;
          public          postgres    false    229         �            1259    17406    beone_inventory    TABLE     *  CREATE TABLE public.beone_inventory (
    intvent_trans_id bigint NOT NULL,
    intvent_trans_no character varying(50) NOT NULL,
    item_id integer,
    trans_date date NOT NULL,
    keterangan character varying(100) NOT NULL,
    qty_in double precision,
    value_in double precision,
    qty_out double precision,
    value_out double precision,
    sa_qty double precision,
    sa_unit_price double precision,
    sa_amount double precision,
    flag integer,
    update_by integer,
    update_date date,
    gudang_id integer DEFAULT 1 NOT NULL
);
 #   DROP TABLE public.beone_inventory;
       public         heap    postgres    false         �            1259    17410 $   beone_inventory_intvent_trans_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_inventory_intvent_trans_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ;   DROP SEQUENCE public.beone_inventory_intvent_trans_id_seq;
       public          postgres    false    230         l           0    0 $   beone_inventory_intvent_trans_id_seq    SEQUENCE OWNED BY     m   ALTER SEQUENCE public.beone_inventory_intvent_trans_id_seq OWNED BY public.beone_inventory.intvent_trans_id;
          public          postgres    false    231         �            1259    17412 
   beone_item    TABLE     �  CREATE TABLE public.beone_item (
    item_id integer NOT NULL,
    nama character varying(100),
    item_code character varying(20),
    saldo_qty double precision,
    saldo_idr double precision,
    keterangan character varying(200),
    flag integer,
    item_type_id integer,
    hscode character varying(50),
    satuan_id integer,
    item_category_id integer DEFAULT 1 NOT NULL
);
    DROP TABLE public.beone_item;
       public         heap    postgres    false         �            1259    17416    beone_item_category    TABLE     �   CREATE TABLE public.beone_item_category (
    item_category_id integer NOT NULL,
    nama character varying(100),
    keterangan character varying(200),
    flag integer
);
 '   DROP TABLE public.beone_item_category;
       public         heap    postgres    false         �            1259    17419    beone_item_item_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_item_item_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.beone_item_item_id_seq;
       public          postgres    false    232         m           0    0    beone_item_item_id_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE public.beone_item_item_id_seq OWNED BY public.beone_item.item_id;
          public          postgres    false    234         �            1259    17421    beone_item_type    TABLE     �   CREATE TABLE public.beone_item_type (
    item_type_id integer NOT NULL,
    nama character varying(100),
    keterangan character varying(200),
    flag integer
);
 #   DROP TABLE public.beone_item_type;
       public         heap    postgres    false         �            1259    17424     beone_item_type_item_type_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_item_type_item_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 7   DROP SEQUENCE public.beone_item_type_item_type_id_seq;
       public          postgres    false    235         n           0    0     beone_item_type_item_type_id_seq    SEQUENCE OWNED BY     e   ALTER SEQUENCE public.beone_item_type_item_type_id_seq OWNED BY public.beone_item_type.item_type_id;
          public          postgres    false    236         �            1259    17426    beone_kode_trans    TABLE     �   CREATE TABLE public.beone_kode_trans (
    kode_trans_id integer NOT NULL,
    nama character varying,
    coa_id integer,
    kode_bank character varying(20),
    in_out integer,
    flag integer
);
 $   DROP TABLE public.beone_kode_trans;
       public         heap    postgres    false         �            1259    17432 "   beone_kode_trans_kode_trans_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_kode_trans_kode_trans_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 9   DROP SEQUENCE public.beone_kode_trans_kode_trans_id_seq;
       public          postgres    false    237         o           0    0 "   beone_kode_trans_kode_trans_id_seq    SEQUENCE OWNED BY     i   ALTER SEQUENCE public.beone_kode_trans_kode_trans_id_seq OWNED BY public.beone_kode_trans.kode_trans_id;
          public          postgres    false    238         �            1259    17434    beone_komposisi    TABLE     �   CREATE TABLE public.beone_komposisi (
    komposisi_id integer NOT NULL,
    item_jadi_id integer,
    item_baku_id integer,
    qty_item_baku double precision,
    flag integer
);
 #   DROP TABLE public.beone_komposisi;
       public         heap    postgres    false         �            1259    17437     beone_komposisi_komposisi_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_komposisi_komposisi_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 7   DROP SEQUENCE public.beone_komposisi_komposisi_id_seq;
       public          postgres    false    239         p           0    0     beone_komposisi_komposisi_id_seq    SEQUENCE OWNED BY     e   ALTER SEQUENCE public.beone_komposisi_komposisi_id_seq OWNED BY public.beone_komposisi.komposisi_id;
          public          postgres    false    240         �            1259    17439    beone_konfigurasi_perusahaan    TABLE     G  CREATE TABLE public.beone_konfigurasi_perusahaan (
    nama_perusahaan character varying(100),
    alamat_perusahaan character varying(500),
    kota_perusahaan character varying(100),
    provinsi_perusahaan character varying(100),
    kode_pos_perusahaan character varying(50),
    logo_perusahaan character varying(1000)
);
 0   DROP TABLE public.beone_konfigurasi_perusahaan;
       public         heap    postgres    false         �            1259    17445    beone_konversi_stok_detail    TABLE     E  CREATE TABLE public.beone_konversi_stok_detail (
    konversi_stok_detail_id integer NOT NULL,
    konversi_stok_header_id integer,
    item_id integer,
    qty double precision,
    qty_real double precision,
    satuan_qty integer,
    gudang_id integer DEFAULT 1 NOT NULL,
    qty_allocation integer DEFAULT 0 NOT NULL
);
 .   DROP TABLE public.beone_konversi_stok_detail;
       public         heap    postgres    false         �            1259    17449 6   beone_konversi_stok_detail_konversi_stok_detail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_konversi_stok_detail_konversi_stok_detail_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 M   DROP SEQUENCE public.beone_konversi_stok_detail_konversi_stok_detail_id_seq;
       public          postgres    false    242         q           0    0 6   beone_konversi_stok_detail_konversi_stok_detail_id_seq    SEQUENCE OWNED BY     �   ALTER SEQUENCE public.beone_konversi_stok_detail_konversi_stok_detail_id_seq OWNED BY public.beone_konversi_stok_detail.konversi_stok_detail_id;
          public          postgres    false    243         �            1259    17451    beone_konversi_stok_header    TABLE     W  CREATE TABLE public.beone_konversi_stok_header (
    konversi_stok_header_id integer NOT NULL,
    item_id integer,
    konversi_stok_no character varying(255),
    konversi_stok_date date,
    flag integer,
    qty double precision,
    satuan_qty integer,
    gudang_id integer DEFAULT 0 NOT NULL,
    qty_real integer DEFAULT 0 NOT NULL
);
 .   DROP TABLE public.beone_konversi_stok_header;
       public         heap    postgres    false         �            1259    17455 6   beone_konversi_stok_header_konversi_stok_header_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_konversi_stok_header_konversi_stok_header_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 M   DROP SEQUENCE public.beone_konversi_stok_header_konversi_stok_header_id_seq;
       public          postgres    false    244         r           0    0 6   beone_konversi_stok_header_konversi_stok_header_id_seq    SEQUENCE OWNED BY     �   ALTER SEQUENCE public.beone_konversi_stok_header_konversi_stok_header_id_seq OWNED BY public.beone_konversi_stok_header.konversi_stok_header_id;
          public          postgres    false    245         �            1259    17457 	   beone_log    TABLE     �   CREATE TABLE public.beone_log (
    log_id bigint NOT NULL,
    log_user character varying(100),
    log_tipe integer,
    log_desc character varying(250),
    log_time timestamp without time zone
);
    DROP TABLE public.beone_log;
       public         heap    postgres    false         �            1259    17460    beone_log_log_id_seq    SEQUENCE     }   CREATE SEQUENCE public.beone_log_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.beone_log_log_id_seq;
       public          postgres    false    246         s           0    0    beone_log_log_id_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE public.beone_log_log_id_seq OWNED BY public.beone_log.log_id;
          public          postgres    false    247         �            1259    17462    beone_opname_detail    TABLE       CREATE TABLE public.beone_opname_detail (
    opname_detail_id integer NOT NULL,
    opname_header_id integer,
    item_id integer,
    qty_existing double precision,
    qty_opname double precision,
    qty_selisih double precision,
    status_opname integer,
    flag integer
);
 '   DROP TABLE public.beone_opname_detail;
       public         heap    postgres    false         �            1259    17465 (   beone_opname_detail_opname_detail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_opname_detail_opname_detail_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ?   DROP SEQUENCE public.beone_opname_detail_opname_detail_id_seq;
       public          postgres    false    248         t           0    0 (   beone_opname_detail_opname_detail_id_seq    SEQUENCE OWNED BY     u   ALTER SEQUENCE public.beone_opname_detail_opname_detail_id_seq OWNED BY public.beone_opname_detail.opname_detail_id;
          public          postgres    false    249         �            1259    17467    beone_opname_header    TABLE     �  CREATE TABLE public.beone_opname_header (
    opname_header_id integer NOT NULL,
    document_no character varying(50),
    opname_date date,
    total_item_opname integer,
    total_item_opname_match integer,
    total_item_opname_plus integer,
    total_item_opname_min integer,
    update_by integer,
    flag integer,
    keterangan character varying(100),
    update_date date
);
 '   DROP TABLE public.beone_opname_header;
       public         heap    postgres    false         �            1259    17470 (   beone_opname_header_opname_header_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_opname_header_opname_header_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ?   DROP SEQUENCE public.beone_opname_header_opname_header_id_seq;
       public          postgres    false    250         u           0    0 (   beone_opname_header_opname_header_id_seq    SEQUENCE OWNED BY     u   ALTER SEQUENCE public.beone_opname_header_opname_header_id_seq OWNED BY public.beone_opname_header.opname_header_id;
          public          postgres    false    251         �            1259    17472    beone_peleburan    TABLE     !  CREATE TABLE public.beone_peleburan (
    peleburan_id integer NOT NULL,
    peleburan_no character varying(20),
    peleburan_date date,
    keterangan character varying(100),
    item_id integer,
    qty_peleburan bigint,
    update_by integer,
    update_date date,
    flag integer
);
 #   DROP TABLE public.beone_peleburan;
       public         heap    postgres    false         �            1259    17475     beone_peleburan_peleburan_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_peleburan_peleburan_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 7   DROP SEQUENCE public.beone_peleburan_peleburan_id_seq;
       public          postgres    false    252         v           0    0     beone_peleburan_peleburan_id_seq    SEQUENCE OWNED BY     e   ALTER SEQUENCE public.beone_peleburan_peleburan_id_seq OWNED BY public.beone_peleburan.peleburan_id;
          public          postgres    false    253         �            1259    17477    beone_pemakaian_bahan_detail    TABLE     �   CREATE TABLE public.beone_pemakaian_bahan_detail (
    pemakaian_detail_id bigint NOT NULL,
    pemakaian_header_id integer,
    item_id integer,
    qty double precision,
    unit_price double precision
);
 0   DROP TABLE public.beone_pemakaian_bahan_detail;
       public         heap    postgres    false         �            1259    17480 4   beone_pemakaian_bahan_detail_pemakaian_detail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_pemakaian_bahan_detail_pemakaian_detail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 K   DROP SEQUENCE public.beone_pemakaian_bahan_detail_pemakaian_detail_id_seq;
       public          postgres    false    254         w           0    0 4   beone_pemakaian_bahan_detail_pemakaian_detail_id_seq    SEQUENCE OWNED BY     �   ALTER SEQUENCE public.beone_pemakaian_bahan_detail_pemakaian_detail_id_seq OWNED BY public.beone_pemakaian_bahan_detail.pemakaian_detail_id;
          public          postgres    false    255                     1259    17482    beone_pm_detail    TABLE     �   CREATE TABLE public.beone_pm_detail (
    pm_detail_id integer NOT NULL,
    item_id integer,
    qty double precision,
    satuan_id integer,
    pm_header_id integer
);
 #   DROP TABLE public.beone_pm_detail;
       public         heap    postgres    false                    1259    17485     beone_pm_detail_pm_detail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_pm_detail_pm_detail_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 7   DROP SEQUENCE public.beone_pm_detail_pm_detail_id_seq;
       public          postgres    false    256         x           0    0     beone_pm_detail_pm_detail_id_seq    SEQUENCE OWNED BY     e   ALTER SEQUENCE public.beone_pm_detail_pm_detail_id_seq OWNED BY public.beone_pm_detail.pm_detail_id;
          public          postgres    false    257                    1259    17487    beone_pm_header    TABLE     �   CREATE TABLE public.beone_pm_header (
    pm_header_id bigint NOT NULL,
    no_pm character varying(50),
    tgl_pm date,
    customer_id integer,
    qty double precision,
    keterangan_artikel character varying,
    status integer
);
 #   DROP TABLE public.beone_pm_header;
       public         heap    postgres    false                    1259    17493     beone_pm_header_pm_header_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_pm_header_pm_header_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 7   DROP SEQUENCE public.beone_pm_header_pm_header_id_seq;
       public          postgres    false    258         y           0    0     beone_pm_header_pm_header_id_seq    SEQUENCE OWNED BY     e   ALTER SEQUENCE public.beone_pm_header_pm_header_id_seq OWNED BY public.beone_pm_header.pm_header_id;
          public          postgres    false    259                    1259    17495    beone_po_import_detail    TABLE     �   CREATE TABLE public.beone_po_import_detail (
    purchase_detail_id bigint NOT NULL,
    purchase_header_id integer,
    item_id integer,
    qty double precision,
    price double precision,
    amount double precision,
    flag integer
);
 *   DROP TABLE public.beone_po_import_detail;
       public         heap    postgres    false                    1259    17498 -   beone_po_import_detail_purchase_detail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_po_import_detail_purchase_detail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 D   DROP SEQUENCE public.beone_po_import_detail_purchase_detail_id_seq;
       public          postgres    false    260         z           0    0 -   beone_po_import_detail_purchase_detail_id_seq    SEQUENCE OWNED BY        ALTER SEQUENCE public.beone_po_import_detail_purchase_detail_id_seq OWNED BY public.beone_po_import_detail.purchase_detail_id;
          public          postgres    false    261                    1259    17500    beone_po_import_header    TABLE     3  CREATE TABLE public.beone_po_import_header (
    purchase_header_id bigint NOT NULL,
    purchase_no character varying(50),
    trans_date date,
    supplier_id integer,
    keterangan character varying(250),
    grandtotal double precision,
    flag integer,
    update_by integer,
    update_date date
);
 *   DROP TABLE public.beone_po_import_header;
       public         heap    postgres    false                    1259    17503 -   beone_po_import_header_purchase_header_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_po_import_header_purchase_header_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 D   DROP SEQUENCE public.beone_po_import_header_purchase_header_id_seq;
       public          postgres    false    262         {           0    0 -   beone_po_import_header_purchase_header_id_seq    SEQUENCE OWNED BY        ALTER SEQUENCE public.beone_po_import_header_purchase_header_id_seq OWNED BY public.beone_po_import_header.purchase_header_id;
          public          postgres    false    263                    1259    17505    beone_produksi_detail    TABLE     �   CREATE TABLE public.beone_produksi_detail (
    produksi_detail_id bigint NOT NULL,
    produksi_header_id integer,
    pemakaian_header_id integer
);
 )   DROP TABLE public.beone_produksi_detail;
       public         heap    postgres    false         	           1259    17508 ,   beone_produksi_detail_produksi_detail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_produksi_detail_produksi_detail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 C   DROP SEQUENCE public.beone_produksi_detail_produksi_detail_id_seq;
       public          postgres    false    264         |           0    0 ,   beone_produksi_detail_produksi_detail_id_seq    SEQUENCE OWNED BY     }   ALTER SEQUENCE public.beone_produksi_detail_produksi_detail_id_seq OWNED BY public.beone_produksi_detail.produksi_detail_id;
          public          postgres    false    265         
           1259    17510    beone_produksi_header    TABLE     x  CREATE TABLE public.beone_produksi_header (
    produksi_header_id bigint NOT NULL,
    nomor_produksi character varying(20),
    tgl_produksi date,
    tipe_produksi integer,
    item_produksi integer,
    qty_hasil_produksi double precision,
    keterangan character varying(250),
    flag integer,
    updated_by integer,
    udpated_date date,
    pm_header_id integer
);
 )   DROP TABLE public.beone_produksi_header;
       public         heap    postgres    false                    1259    17513 ,   beone_produksi_header_produksi_header_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_produksi_header_produksi_header_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 C   DROP SEQUENCE public.beone_produksi_header_produksi_header_id_seq;
       public          postgres    false    266         }           0    0 ,   beone_produksi_header_produksi_header_id_seq    SEQUENCE OWNED BY     }   ALTER SEQUENCE public.beone_produksi_header_produksi_header_id_seq OWNED BY public.beone_produksi_header.produksi_header_id;
          public          postgres    false    267                    1259    17515    beone_purchase_detail    TABLE     �   CREATE TABLE public.beone_purchase_detail (
    purchase_detail_id bigint NOT NULL,
    purchase_header_id integer,
    item_id integer,
    qty double precision,
    price double precision,
    amount double precision,
    flag integer
);
 )   DROP TABLE public.beone_purchase_detail;
       public         heap    postgres    false                    1259    17518 ,   beone_purchase_detail_purchase_detail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_purchase_detail_purchase_detail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 C   DROP SEQUENCE public.beone_purchase_detail_purchase_detail_id_seq;
       public          postgres    false    268         ~           0    0 ,   beone_purchase_detail_purchase_detail_id_seq    SEQUENCE OWNED BY     }   ALTER SEQUENCE public.beone_purchase_detail_purchase_detail_id_seq OWNED BY public.beone_purchase_detail.purchase_detail_id;
          public          postgres    false    269                    1259    17520    beone_purchase_header    TABLE     �  CREATE TABLE public.beone_purchase_header (
    purchase_header_id bigint NOT NULL,
    purchase_no character varying(50),
    trans_date date,
    supplier_id integer,
    keterangan character varying(200),
    update_by integer,
    update_date date,
    flag integer,
    ppn integer,
    subtotal double precision,
    ppn_value double precision,
    grandtotal double precision,
    realisasi integer DEFAULT 0 NOT NULL
);
 )   DROP TABLE public.beone_purchase_header;
       public         heap    postgres    false                    1259    17524 ,   beone_purchase_header_purchase_header_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_purchase_header_purchase_header_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 C   DROP SEQUENCE public.beone_purchase_header_purchase_header_id_seq;
       public          postgres    false    270                    0    0 ,   beone_purchase_header_purchase_header_id_seq    SEQUENCE OWNED BY     }   ALTER SEQUENCE public.beone_purchase_header_purchase_header_id_seq OWNED BY public.beone_purchase_header.purchase_header_id;
          public          postgres    false    271                    1259    17526    beone_received_import    TABLE     �  CREATE TABLE public.beone_received_import (
    received_id bigint NOT NULL,
    nomor_aju character varying(50),
    nomor_dokumen_pabean character varying(10),
    tanggal_received date,
    status_received integer,
    flag integer,
    update_by integer,
    update_date date,
    nomor_received character varying(50),
    tpb_header_id integer,
    kurs double precision,
    po_import_header_id integer
);
 )   DROP TABLE public.beone_received_import;
       public         heap    postgres    false                    1259    17529 %   beone_received_import_received_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_received_import_received_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 <   DROP SEQUENCE public.beone_received_import_received_id_seq;
       public          postgres    false    272         �           0    0 %   beone_received_import_received_id_seq    SEQUENCE OWNED BY     o   ALTER SEQUENCE public.beone_received_import_received_id_seq OWNED BY public.beone_received_import.received_id;
          public          postgres    false    273                    1259    17531 
   beone_role    TABLE     �  CREATE TABLE public.beone_role (
    role_id integer NOT NULL,
    nama_role character varying(50),
    keterangan character varying(200),
    master_menu integer,
    pembelian_menu integer,
    penjualan_menu integer,
    inventory_menu integer,
    produksi_menu integer,
    asset_menu integer,
    jurnal_umum_menu integer,
    kasbank_menu integer,
    laporan_inventory integer,
    laporan_keuangan integer,
    konfigurasi integer,
    import_menu integer,
    eksport_menu integer
);
    DROP TABLE public.beone_role;
       public         heap    postgres    false                    1259    17534    beone_role_role_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_role_role_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.beone_role_role_id_seq;
       public          postgres    false    274         �           0    0    beone_role_role_id_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE public.beone_role_role_id_seq OWNED BY public.beone_role.role_id;
          public          postgres    false    275                    1259    17536    beone_role_user    TABLE     �  CREATE TABLE public.beone_role_user (
    role_id integer NOT NULL,
    nama_role character varying(50),
    keterangan character varying(200),
    master_menu integer,
    user_add integer,
    user_edit integer,
    user_delete integer,
    role_add integer,
    role_edit integer,
    role_delete integer,
    customer_add integer,
    customer_edit integer,
    customer_delete integer,
    supplier_add integer,
    supplier_edit integer,
    supplier_delete integer,
    item_add integer,
    item_edit integer,
    item_delete integer,
    jenis_add integer,
    jenis_edit integer,
    jenis_delete integer,
    satuan_add integer,
    satuan_edit integer,
    satuan_delete integer,
    gudang_add integer,
    gudang_edit integer,
    gudang_delete integer,
    master_import integer,
    po_add integer,
    po_edit integer,
    po_delete integer,
    tracing integer,
    terima_barang integer,
    master_pembelian integer,
    pembelian_add integer,
    pembelian_edit integer,
    pembelian_delete integer,
    kredit_note_add integer,
    kredit_note_edit integer,
    kredit_note_delete integer,
    master_eksport integer,
    eksport_add integer,
    eksport_edit integer,
    eksport_delete integer,
    kirim_barang integer,
    menu_penjualan integer,
    penjualan_add integer,
    penjualan_edit integer,
    penjualan_delete integer,
    debit_note_add integer,
    debit_note_edit integer,
    debit_note_delete integer,
    menu_inventory integer,
    pindah_gudang integer,
    stockopname_add integer,
    stockopname_edit integer,
    stockopname_delete integer,
    stockopname_opname integer,
    adjustment_add integer,
    adjustment_edit integer,
    adjustment_delete integer,
    pemusnahan_add integer,
    pemusnahan_edit integer,
    pemusnahan_delete integer,
    recal_inventory integer,
    menu_produksi integer,
    produksi_add integer,
    produksi_edit integer,
    produksi_delete integer,
    menu_asset integer,
    menu_jurnal_umum integer,
    jurnal_umum_add integer,
    jurnal_umum_edit integer,
    jurnal_umum_delete integer,
    menu_kas_bank integer,
    kas_bank_add integer,
    kas_bank_edit integer,
    kas_bank_delete integer,
    menu_laporan_inventory integer,
    menu_laporan_keuangan integer,
    menu_konfigurasi integer
);
 #   DROP TABLE public.beone_role_user;
       public         heap    postgres    false                    1259    17539    beone_role_user_role_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_role_user_role_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 2   DROP SEQUENCE public.beone_role_user_role_id_seq;
       public          postgres    false    276         �           0    0    beone_role_user_role_id_seq    SEQUENCE OWNED BY     [   ALTER SEQUENCE public.beone_role_user_role_id_seq OWNED BY public.beone_role_user.role_id;
          public          postgres    false    277                    1259    17541    beone_sales_detail    TABLE     �   CREATE TABLE public.beone_sales_detail (
    sales_detail_id bigint NOT NULL,
    sales_header_id integer,
    item_id integer,
    qty double precision,
    price double precision,
    amount double precision,
    flag integer
);
 &   DROP TABLE public.beone_sales_detail;
       public         heap    postgres    false                    1259    17544 &   beone_sales_detail_sales_detail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_sales_detail_sales_detail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 =   DROP SEQUENCE public.beone_sales_detail_sales_detail_id_seq;
       public          postgres    false    278         �           0    0 &   beone_sales_detail_sales_detail_id_seq    SEQUENCE OWNED BY     q   ALTER SEQUENCE public.beone_sales_detail_sales_detail_id_seq OWNED BY public.beone_sales_detail.sales_detail_id;
          public          postgres    false    279                    1259    17546    beone_sales_header    TABLE     �  CREATE TABLE public.beone_sales_header (
    sales_header_id bigint NOT NULL,
    sales_no character varying(50),
    trans_date date,
    customer_id integer,
    keterangan character varying(200),
    ppn double precision,
    subtotal double precision,
    ppn_value double precision,
    grandtotal double precision,
    update_by integer,
    update_date date,
    flag integer,
    biaya double precision,
    status integer,
    realisasi integer DEFAULT 0 NOT NULL
);
 &   DROP TABLE public.beone_sales_header;
       public         heap    postgres    false                    1259    17550 &   beone_sales_header_sales_header_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_sales_header_sales_header_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 =   DROP SEQUENCE public.beone_sales_header_sales_header_id_seq;
       public          postgres    false    280         �           0    0 &   beone_sales_header_sales_header_id_seq    SEQUENCE OWNED BY     q   ALTER SEQUENCE public.beone_sales_header_sales_header_id_seq OWNED BY public.beone_sales_header.sales_header_id;
          public          postgres    false    281                    1259    17552    beone_satuan_item    TABLE     �   CREATE TABLE public.beone_satuan_item (
    satuan_id integer NOT NULL,
    satuan_code character varying(5),
    keterangan character varying(100),
    flag integer
);
 %   DROP TABLE public.beone_satuan_item;
       public         heap    postgres    false                    1259    17555    beone_satuan_item_satuan_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_satuan_item_satuan_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 6   DROP SEQUENCE public.beone_satuan_item_satuan_id_seq;
       public          postgres    false    282         �           0    0    beone_satuan_item_satuan_id_seq    SEQUENCE OWNED BY     c   ALTER SEQUENCE public.beone_satuan_item_satuan_id_seq OWNED BY public.beone_satuan_item.satuan_id;
          public          postgres    false    283                    1259    17557    beone_subkon_in_detail    TABLE     �   CREATE TABLE public.beone_subkon_in_detail (
    subkon_in_detail_id bigint NOT NULL,
    subkon_in_header_id integer,
    item_id integer,
    qty double precision,
    flag integer
);
 *   DROP TABLE public.beone_subkon_in_detail;
       public         heap    postgres    false                    1259    17560 .   beone_subkon_in_detail_subkon_in_detail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_subkon_in_detail_subkon_in_detail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 E   DROP SEQUENCE public.beone_subkon_in_detail_subkon_in_detail_id_seq;
       public          postgres    false    284         �           0    0 .   beone_subkon_in_detail_subkon_in_detail_id_seq    SEQUENCE OWNED BY     �   ALTER SEQUENCE public.beone_subkon_in_detail_subkon_in_detail_id_seq OWNED BY public.beone_subkon_in_detail.subkon_in_detail_id;
          public          postgres    false    285                    1259    17562    beone_subkon_in_header    TABLE     ~  CREATE TABLE public.beone_subkon_in_header (
    subkon_in_id bigint NOT NULL,
    no_subkon_in character varying(50),
    no_subkon_out character varying(50),
    trans_date date,
    keterangan character varying(250),
    nomor_aju character varying(50),
    biaya_subkon double precision,
    supplier_id integer,
    flag integer,
    update_by integer,
    update_date date
);
 *   DROP TABLE public.beone_subkon_in_header;
       public         heap    postgres    false                    1259    17565 '   beone_subkon_in_header_subkon_in_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_subkon_in_header_subkon_in_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 >   DROP SEQUENCE public.beone_subkon_in_header_subkon_in_id_seq;
       public          postgres    false    286         �           0    0 '   beone_subkon_in_header_subkon_in_id_seq    SEQUENCE OWNED BY     s   ALTER SEQUENCE public.beone_subkon_in_header_subkon_in_id_seq OWNED BY public.beone_subkon_in_header.subkon_in_id;
          public          postgres    false    287                     1259    17567    beone_subkon_out_detail    TABLE     �   CREATE TABLE public.beone_subkon_out_detail (
    subkon_out_detail_id bigint NOT NULL,
    subkon_out_header_id integer,
    item_id integer,
    qty double precision,
    unit_price double precision,
    flag integer
);
 +   DROP TABLE public.beone_subkon_out_detail;
       public         heap    postgres    false         !           1259    17570 0   beone_subkon_out_detail_subkon_out_detail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_subkon_out_detail_subkon_out_detail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 G   DROP SEQUENCE public.beone_subkon_out_detail_subkon_out_detail_id_seq;
       public          postgres    false    288         �           0    0 0   beone_subkon_out_detail_subkon_out_detail_id_seq    SEQUENCE OWNED BY     �   ALTER SEQUENCE public.beone_subkon_out_detail_subkon_out_detail_id_seq OWNED BY public.beone_subkon_out_detail.subkon_out_detail_id;
          public          postgres    false    289         "           1259    17572    beone_subkon_out_header    TABLE     5  CREATE TABLE public.beone_subkon_out_header (
    subkon_out_id bigint NOT NULL,
    no_subkon_out character varying(50),
    trans_date date,
    supplier_id integer,
    keterangan character varying(250),
    nomor_aju character varying(50),
    flag integer,
    update_by integer,
    update_date date
);
 +   DROP TABLE public.beone_subkon_out_header;
       public         heap    postgres    false         #           1259    17575 )   beone_subkon_out_header_subkon_out_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_subkon_out_header_subkon_out_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 @   DROP SEQUENCE public.beone_subkon_out_header_subkon_out_id_seq;
       public          postgres    false    290         �           0    0 )   beone_subkon_out_header_subkon_out_id_seq    SEQUENCE OWNED BY     w   ALTER SEQUENCE public.beone_subkon_out_header_subkon_out_id_seq OWNED BY public.beone_subkon_out_header.subkon_out_id;
          public          postgres    false    291         $           1259    17577    beone_tipe_coa    TABLE     i   CREATE TABLE public.beone_tipe_coa (
    tipe_coa_id integer NOT NULL,
    nama character varying(50)
);
 "   DROP TABLE public.beone_tipe_coa;
       public         heap    postgres    false         %           1259    17580    beone_tipe_coa_tipe_coa_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_tipe_coa_tipe_coa_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 5   DROP SEQUENCE public.beone_tipe_coa_tipe_coa_id_seq;
       public          postgres    false    292         �           0    0    beone_tipe_coa_tipe_coa_id_seq    SEQUENCE OWNED BY     a   ALTER SEQUENCE public.beone_tipe_coa_tipe_coa_id_seq OWNED BY public.beone_tipe_coa.tipe_coa_id;
          public          postgres    false    293         &           1259    17582    beone_tipe_coa_transaksi    TABLE     �   CREATE TABLE public.beone_tipe_coa_transaksi (
    tipe_coa_trans_id integer NOT NULL,
    nama character varying(50),
    flag integer
);
 ,   DROP TABLE public.beone_tipe_coa_transaksi;
       public         heap    postgres    false         '           1259    17585 .   beone_tipe_coa_transaksi_tipe_coa_trans_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_tipe_coa_transaksi_tipe_coa_trans_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 E   DROP SEQUENCE public.beone_tipe_coa_transaksi_tipe_coa_trans_id_seq;
       public          postgres    false    294         �           0    0 .   beone_tipe_coa_transaksi_tipe_coa_trans_id_seq    SEQUENCE OWNED BY     �   ALTER SEQUENCE public.beone_tipe_coa_transaksi_tipe_coa_trans_id_seq OWNED BY public.beone_tipe_coa_transaksi.tipe_coa_trans_id;
          public          postgres    false    295         (           1259    17587    beone_transfer_stock    TABLE       CREATE TABLE public.beone_transfer_stock (
    transfer_stock_id bigint NOT NULL,
    transfer_no character varying(50),
    transfer_date date,
    coa_kode_biaya integer,
    keterangan character varying(200),
    update_by integer,
    update_date date,
    flag integer
);
 (   DROP TABLE public.beone_transfer_stock;
       public         heap    postgres    false         )           1259    17590    beone_transfer_stock_detail    TABLE     u  CREATE TABLE public.beone_transfer_stock_detail (
    transfer_stock_detail_id bigint NOT NULL,
    transfer_stock_header_id integer,
    tipe_transfer_stock character varying(10),
    item_id integer,
    qty double precision,
    gudang_id integer,
    biaya double precision,
    update_by integer,
    update_date date,
    flag integer,
    persen_produksi integer
);
 /   DROP TABLE public.beone_transfer_stock_detail;
       public         heap    postgres    false         *           1259    17593 8   beone_transfer_stock_detail_transfer_stock_detail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_transfer_stock_detail_transfer_stock_detail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 O   DROP SEQUENCE public.beone_transfer_stock_detail_transfer_stock_detail_id_seq;
       public          postgres    false    297         �           0    0 8   beone_transfer_stock_detail_transfer_stock_detail_id_seq    SEQUENCE OWNED BY     �   ALTER SEQUENCE public.beone_transfer_stock_detail_transfer_stock_detail_id_seq OWNED BY public.beone_transfer_stock_detail.transfer_stock_detail_id;
          public          postgres    false    298         +           1259    17595 *   beone_transfer_stock_transfer_stock_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_transfer_stock_transfer_stock_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 A   DROP SEQUENCE public.beone_transfer_stock_transfer_stock_id_seq;
       public          postgres    false    296         �           0    0 *   beone_transfer_stock_transfer_stock_id_seq    SEQUENCE OWNED BY     y   ALTER SEQUENCE public.beone_transfer_stock_transfer_stock_id_seq OWNED BY public.beone_transfer_stock.transfer_stock_id;
          public          postgres    false    299         ,           1259    17597 
   beone_user    TABLE     �   CREATE TABLE public.beone_user (
    user_id integer NOT NULL,
    username character varying(50),
    password character varying(50),
    nama character varying(100),
    role_id integer,
    update_by integer,
    update_date date,
    flag integer
);
    DROP TABLE public.beone_user;
       public         heap    postgres    false         -           1259    17600    beone_user_user_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_user_user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.beone_user_user_id_seq;
       public          postgres    false    300         �           0    0    beone_user_user_id_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE public.beone_user_user_id_seq OWNED BY public.beone_user.user_id;
          public          postgres    false    301         .           1259    17602    beone_voucher_detail    TABLE     B  CREATE TABLE public.beone_voucher_detail (
    voucher_detail_id bigint NOT NULL,
    voucher_header_id integer,
    coa_id_lawan integer,
    coa_no_lawan character varying(20),
    jumlah_valas double precision,
    kurs double precision,
    jumlah_idr double precision,
    keterangan_detail character varying(200)
);
 (   DROP TABLE public.beone_voucher_detail;
       public         heap    postgres    false         /           1259    17605 *   beone_voucher_detail_voucher_detail_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_voucher_detail_voucher_detail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 A   DROP SEQUENCE public.beone_voucher_detail_voucher_detail_id_seq;
       public          postgres    false    302         �           0    0 *   beone_voucher_detail_voucher_detail_id_seq    SEQUENCE OWNED BY     y   ALTER SEQUENCE public.beone_voucher_detail_voucher_detail_id_seq OWNED BY public.beone_voucher_detail.voucher_detail_id;
          public          postgres    false    303         0           1259    17607    beone_voucher_header    TABLE     ;  CREATE TABLE public.beone_voucher_header (
    voucher_header_id bigint NOT NULL,
    voucher_number character varying(20),
    voucher_date date,
    tipe integer,
    keterangan character varying(200),
    coa_id_cash_bank integer,
    coa_no character varying(20),
    update_by integer,
    update_date date
);
 (   DROP TABLE public.beone_voucher_header;
       public         heap    postgres    false         1           1259    17610 *   beone_voucher_header_voucher_header_id_seq    SEQUENCE     �   CREATE SEQUENCE public.beone_voucher_header_voucher_header_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 A   DROP SEQUENCE public.beone_voucher_header_voucher_header_id_seq;
       public          postgres    false    304         �           0    0 *   beone_voucher_header_voucher_header_id_seq    SEQUENCE OWNED BY     y   ALTER SEQUENCE public.beone_voucher_header_voucher_header_id_seq OWNED BY public.beone_voucher_header.voucher_header_id;
          public          postgres    false    305         �           2604    17612    beone_adjustment adjustment_id    DEFAULT     �   ALTER TABLE ONLY public.beone_adjustment ALTER COLUMN adjustment_id SET DEFAULT nextval('public.beone_adjustment_adjustment_id_seq'::regclass);
 M   ALTER TABLE public.beone_adjustment ALTER COLUMN adjustment_id DROP DEFAULT;
       public          postgres    false    203    202         �           2604    17613    beone_coa coa_id    DEFAULT     t   ALTER TABLE ONLY public.beone_coa ALTER COLUMN coa_id SET DEFAULT nextval('public.beone_coa_coa_id_seq'::regclass);
 ?   ALTER TABLE public.beone_coa ALTER COLUMN coa_id DROP DEFAULT;
       public          postgres    false    205    204         �           2604    17614    beone_coa_jurnal coa_jurnal_id    DEFAULT     �   ALTER TABLE ONLY public.beone_coa_jurnal ALTER COLUMN coa_jurnal_id SET DEFAULT nextval('public.beone_coa_jurnal_coa_jurnal_id_seq'::regclass);
 M   ALTER TABLE public.beone_coa_jurnal ALTER COLUMN coa_jurnal_id DROP DEFAULT;
       public          postgres    false    207    206         �           2604    17615    beone_country country_id    DEFAULT     �   ALTER TABLE ONLY public.beone_country ALTER COLUMN country_id SET DEFAULT nextval('public.beone_country_country_id_seq'::regclass);
 G   ALTER TABLE public.beone_country ALTER COLUMN country_id DROP DEFAULT;
       public          postgres    false    209    208         �           2604    17616    beone_custsup custsup_id    DEFAULT     �   ALTER TABLE ONLY public.beone_custsup ALTER COLUMN custsup_id SET DEFAULT nextval('public.beone_custsup_custsup_id_seq'::regclass);
 G   ALTER TABLE public.beone_custsup ALTER COLUMN custsup_id DROP DEFAULT;
       public          postgres    false    211    210         �           2604    17617 $   beone_export_detail export_detail_id    DEFAULT     �   ALTER TABLE ONLY public.beone_export_detail ALTER COLUMN export_detail_id SET DEFAULT nextval('public.beone_export_detail_export_detail_id_seq'::regclass);
 S   ALTER TABLE public.beone_export_detail ALTER COLUMN export_detail_id DROP DEFAULT;
       public          postgres    false    213    212         �           2604    17618 $   beone_export_header export_header_id    DEFAULT     �   ALTER TABLE ONLY public.beone_export_header ALTER COLUMN export_header_id SET DEFAULT nextval('public.beone_export_header_export_header_id_seq'::regclass);
 S   ALTER TABLE public.beone_export_header ALTER COLUMN export_header_id DROP DEFAULT;
       public          postgres    false    215    214         �           2604    17619    beone_gl gl_id    DEFAULT     p   ALTER TABLE ONLY public.beone_gl ALTER COLUMN gl_id SET DEFAULT nextval('public.beone_gl_gl_id_seq'::regclass);
 =   ALTER TABLE public.beone_gl ALTER COLUMN gl_id DROP DEFAULT;
       public          postgres    false    217    216         �           2604    17620    beone_gudang gudang_id    DEFAULT     �   ALTER TABLE ONLY public.beone_gudang ALTER COLUMN gudang_id SET DEFAULT nextval('public.beone_gudang_gudang_id_seq'::regclass);
 E   ALTER TABLE public.beone_gudang ALTER COLUMN gudang_id DROP DEFAULT;
       public          postgres    false    221    218         �           2604    17621 $   beone_gudang_detail gudang_detail_id    DEFAULT     �   ALTER TABLE ONLY public.beone_gudang_detail ALTER COLUMN gudang_detail_id SET DEFAULT nextval('public.beone_gudang_detail_gudang_detail_id_seq'::regclass);
 S   ALTER TABLE public.beone_gudang_detail ALTER COLUMN gudang_detail_id DROP DEFAULT;
       public          postgres    false    220    219         �           2604    17622 &   beone_hutang_piutang hutang_piutang_id    DEFAULT     �   ALTER TABLE ONLY public.beone_hutang_piutang ALTER COLUMN hutang_piutang_id SET DEFAULT nextval('public.beone_hutang_piutang_hutang_piutang_id_seq'::regclass);
 U   ALTER TABLE public.beone_hutang_piutang ALTER COLUMN hutang_piutang_id DROP DEFAULT;
       public          postgres    false    225    224         �           2604    17623 $   beone_import_detail import_detail_id    DEFAULT     �   ALTER TABLE ONLY public.beone_import_detail ALTER COLUMN import_detail_id SET DEFAULT nextval('public.beone_import_detail_import_detail_id_seq'::regclass);
 S   ALTER TABLE public.beone_import_detail ALTER COLUMN import_detail_id DROP DEFAULT;
       public          postgres    false    227    226         �           2604    17624 $   beone_import_header import_header_id    DEFAULT     �   ALTER TABLE ONLY public.beone_import_header ALTER COLUMN import_header_id SET DEFAULT nextval('public.beone_import_header_import_header_id_seq'::regclass);
 S   ALTER TABLE public.beone_import_header ALTER COLUMN import_header_id DROP DEFAULT;
       public          postgres    false    229    228         �           2604    17625     beone_inventory intvent_trans_id    DEFAULT     �   ALTER TABLE ONLY public.beone_inventory ALTER COLUMN intvent_trans_id SET DEFAULT nextval('public.beone_inventory_intvent_trans_id_seq'::regclass);
 O   ALTER TABLE public.beone_inventory ALTER COLUMN intvent_trans_id DROP DEFAULT;
       public          postgres    false    231    230         �           2604    17626    beone_item item_id    DEFAULT     x   ALTER TABLE ONLY public.beone_item ALTER COLUMN item_id SET DEFAULT nextval('public.beone_item_item_id_seq'::regclass);
 A   ALTER TABLE public.beone_item ALTER COLUMN item_id DROP DEFAULT;
       public          postgres    false    234    232         �           2604    17627    beone_item_type item_type_id    DEFAULT     �   ALTER TABLE ONLY public.beone_item_type ALTER COLUMN item_type_id SET DEFAULT nextval('public.beone_item_type_item_type_id_seq'::regclass);
 K   ALTER TABLE public.beone_item_type ALTER COLUMN item_type_id DROP DEFAULT;
       public          postgres    false    236    235         �           2604    17628    beone_kode_trans kode_trans_id    DEFAULT     �   ALTER TABLE ONLY public.beone_kode_trans ALTER COLUMN kode_trans_id SET DEFAULT nextval('public.beone_kode_trans_kode_trans_id_seq'::regclass);
 M   ALTER TABLE public.beone_kode_trans ALTER COLUMN kode_trans_id DROP DEFAULT;
       public          postgres    false    238    237         �           2604    17629    beone_komposisi komposisi_id    DEFAULT     �   ALTER TABLE ONLY public.beone_komposisi ALTER COLUMN komposisi_id SET DEFAULT nextval('public.beone_komposisi_komposisi_id_seq'::regclass);
 K   ALTER TABLE public.beone_komposisi ALTER COLUMN komposisi_id DROP DEFAULT;
       public          postgres    false    240    239         �           2604    17630 2   beone_konversi_stok_detail konversi_stok_detail_id    DEFAULT     �   ALTER TABLE ONLY public.beone_konversi_stok_detail ALTER COLUMN konversi_stok_detail_id SET DEFAULT nextval('public.beone_konversi_stok_detail_konversi_stok_detail_id_seq'::regclass);
 a   ALTER TABLE public.beone_konversi_stok_detail ALTER COLUMN konversi_stok_detail_id DROP DEFAULT;
       public          postgres    false    243    242         �           2604    17631 2   beone_konversi_stok_header konversi_stok_header_id    DEFAULT     �   ALTER TABLE ONLY public.beone_konversi_stok_header ALTER COLUMN konversi_stok_header_id SET DEFAULT nextval('public.beone_konversi_stok_header_konversi_stok_header_id_seq'::regclass);
 a   ALTER TABLE public.beone_konversi_stok_header ALTER COLUMN konversi_stok_header_id DROP DEFAULT;
       public          postgres    false    245    244         �           2604    17632    beone_log log_id    DEFAULT     t   ALTER TABLE ONLY public.beone_log ALTER COLUMN log_id SET DEFAULT nextval('public.beone_log_log_id_seq'::regclass);
 ?   ALTER TABLE public.beone_log ALTER COLUMN log_id DROP DEFAULT;
       public          postgres    false    247    246         �           2604    17633 $   beone_opname_detail opname_detail_id    DEFAULT     �   ALTER TABLE ONLY public.beone_opname_detail ALTER COLUMN opname_detail_id SET DEFAULT nextval('public.beone_opname_detail_opname_detail_id_seq'::regclass);
 S   ALTER TABLE public.beone_opname_detail ALTER COLUMN opname_detail_id DROP DEFAULT;
       public          postgres    false    249    248         �           2604    17634 $   beone_opname_header opname_header_id    DEFAULT     �   ALTER TABLE ONLY public.beone_opname_header ALTER COLUMN opname_header_id SET DEFAULT nextval('public.beone_opname_header_opname_header_id_seq'::regclass);
 S   ALTER TABLE public.beone_opname_header ALTER COLUMN opname_header_id DROP DEFAULT;
       public          postgres    false    251    250         �           2604    17635    beone_peleburan peleburan_id    DEFAULT     �   ALTER TABLE ONLY public.beone_peleburan ALTER COLUMN peleburan_id SET DEFAULT nextval('public.beone_peleburan_peleburan_id_seq'::regclass);
 K   ALTER TABLE public.beone_peleburan ALTER COLUMN peleburan_id DROP DEFAULT;
       public          postgres    false    253    252         �           2604    17636 0   beone_pemakaian_bahan_detail pemakaian_detail_id    DEFAULT     �   ALTER TABLE ONLY public.beone_pemakaian_bahan_detail ALTER COLUMN pemakaian_detail_id SET DEFAULT nextval('public.beone_pemakaian_bahan_detail_pemakaian_detail_id_seq'::regclass);
 _   ALTER TABLE public.beone_pemakaian_bahan_detail ALTER COLUMN pemakaian_detail_id DROP DEFAULT;
       public          postgres    false    255    254         �           2604    17637 0   beone_pemakaian_bahan_header pemakaian_header_id    DEFAULT     �   ALTER TABLE ONLY public.beone_pemakaian_bahan_header ALTER COLUMN pemakaian_header_id SET DEFAULT nextval('public.beone_header_pemakaian_bahan_pemakaian_header_id_seq'::regclass);
 _   ALTER TABLE public.beone_pemakaian_bahan_header ALTER COLUMN pemakaian_header_id DROP DEFAULT;
       public          postgres    false    223    222         �           2604    17638    beone_pm_detail pm_detail_id    DEFAULT     �   ALTER TABLE ONLY public.beone_pm_detail ALTER COLUMN pm_detail_id SET DEFAULT nextval('public.beone_pm_detail_pm_detail_id_seq'::regclass);
 K   ALTER TABLE public.beone_pm_detail ALTER COLUMN pm_detail_id DROP DEFAULT;
       public          postgres    false    257    256         �           2604    17639    beone_pm_header pm_header_id    DEFAULT     �   ALTER TABLE ONLY public.beone_pm_header ALTER COLUMN pm_header_id SET DEFAULT nextval('public.beone_pm_header_pm_header_id_seq'::regclass);
 K   ALTER TABLE public.beone_pm_header ALTER COLUMN pm_header_id DROP DEFAULT;
       public          postgres    false    259    258         �           2604    17640 )   beone_po_import_detail purchase_detail_id    DEFAULT     �   ALTER TABLE ONLY public.beone_po_import_detail ALTER COLUMN purchase_detail_id SET DEFAULT nextval('public.beone_po_import_detail_purchase_detail_id_seq'::regclass);
 X   ALTER TABLE public.beone_po_import_detail ALTER COLUMN purchase_detail_id DROP DEFAULT;
       public          postgres    false    261    260         �           2604    17641 )   beone_po_import_header purchase_header_id    DEFAULT     �   ALTER TABLE ONLY public.beone_po_import_header ALTER COLUMN purchase_header_id SET DEFAULT nextval('public.beone_po_import_header_purchase_header_id_seq'::regclass);
 X   ALTER TABLE public.beone_po_import_header ALTER COLUMN purchase_header_id DROP DEFAULT;
       public          postgres    false    263    262         �           2604    17642 (   beone_produksi_detail produksi_detail_id    DEFAULT     �   ALTER TABLE ONLY public.beone_produksi_detail ALTER COLUMN produksi_detail_id SET DEFAULT nextval('public.beone_produksi_detail_produksi_detail_id_seq'::regclass);
 W   ALTER TABLE public.beone_produksi_detail ALTER COLUMN produksi_detail_id DROP DEFAULT;
       public          postgres    false    265    264         �           2604    17643 (   beone_produksi_header produksi_header_id    DEFAULT     �   ALTER TABLE ONLY public.beone_produksi_header ALTER COLUMN produksi_header_id SET DEFAULT nextval('public.beone_produksi_header_produksi_header_id_seq'::regclass);
 W   ALTER TABLE public.beone_produksi_header ALTER COLUMN produksi_header_id DROP DEFAULT;
       public          postgres    false    267    266         �           2604    17644 (   beone_purchase_detail purchase_detail_id    DEFAULT     �   ALTER TABLE ONLY public.beone_purchase_detail ALTER COLUMN purchase_detail_id SET DEFAULT nextval('public.beone_purchase_detail_purchase_detail_id_seq'::regclass);
 W   ALTER TABLE public.beone_purchase_detail ALTER COLUMN purchase_detail_id DROP DEFAULT;
       public          postgres    false    269    268         �           2604    17645 (   beone_purchase_header purchase_header_id    DEFAULT     �   ALTER TABLE ONLY public.beone_purchase_header ALTER COLUMN purchase_header_id SET DEFAULT nextval('public.beone_purchase_header_purchase_header_id_seq'::regclass);
 W   ALTER TABLE public.beone_purchase_header ALTER COLUMN purchase_header_id DROP DEFAULT;
       public          postgres    false    271    270         �           2604    17646 !   beone_received_import received_id    DEFAULT     �   ALTER TABLE ONLY public.beone_received_import ALTER COLUMN received_id SET DEFAULT nextval('public.beone_received_import_received_id_seq'::regclass);
 P   ALTER TABLE public.beone_received_import ALTER COLUMN received_id DROP DEFAULT;
       public          postgres    false    273    272         �           2604    17647    beone_role role_id    DEFAULT     x   ALTER TABLE ONLY public.beone_role ALTER COLUMN role_id SET DEFAULT nextval('public.beone_role_role_id_seq'::regclass);
 A   ALTER TABLE public.beone_role ALTER COLUMN role_id DROP DEFAULT;
       public          postgres    false    275    274         �           2604    17648    beone_role_user role_id    DEFAULT     �   ALTER TABLE ONLY public.beone_role_user ALTER COLUMN role_id SET DEFAULT nextval('public.beone_role_user_role_id_seq'::regclass);
 F   ALTER TABLE public.beone_role_user ALTER COLUMN role_id DROP DEFAULT;
       public          postgres    false    277    276         �           2604    17649 "   beone_sales_detail sales_detail_id    DEFAULT     �   ALTER TABLE ONLY public.beone_sales_detail ALTER COLUMN sales_detail_id SET DEFAULT nextval('public.beone_sales_detail_sales_detail_id_seq'::regclass);
 Q   ALTER TABLE public.beone_sales_detail ALTER COLUMN sales_detail_id DROP DEFAULT;
       public          postgres    false    279    278         �           2604    17650 "   beone_sales_header sales_header_id    DEFAULT     �   ALTER TABLE ONLY public.beone_sales_header ALTER COLUMN sales_header_id SET DEFAULT nextval('public.beone_sales_header_sales_header_id_seq'::regclass);
 Q   ALTER TABLE public.beone_sales_header ALTER COLUMN sales_header_id DROP DEFAULT;
       public          postgres    false    281    280         �           2604    17651    beone_satuan_item satuan_id    DEFAULT     �   ALTER TABLE ONLY public.beone_satuan_item ALTER COLUMN satuan_id SET DEFAULT nextval('public.beone_satuan_item_satuan_id_seq'::regclass);
 J   ALTER TABLE public.beone_satuan_item ALTER COLUMN satuan_id DROP DEFAULT;
       public          postgres    false    283    282         �           2604    17652 *   beone_subkon_in_detail subkon_in_detail_id    DEFAULT     �   ALTER TABLE ONLY public.beone_subkon_in_detail ALTER COLUMN subkon_in_detail_id SET DEFAULT nextval('public.beone_subkon_in_detail_subkon_in_detail_id_seq'::regclass);
 Y   ALTER TABLE public.beone_subkon_in_detail ALTER COLUMN subkon_in_detail_id DROP DEFAULT;
       public          postgres    false    285    284         �           2604    17653 #   beone_subkon_in_header subkon_in_id    DEFAULT     �   ALTER TABLE ONLY public.beone_subkon_in_header ALTER COLUMN subkon_in_id SET DEFAULT nextval('public.beone_subkon_in_header_subkon_in_id_seq'::regclass);
 R   ALTER TABLE public.beone_subkon_in_header ALTER COLUMN subkon_in_id DROP DEFAULT;
       public          postgres    false    287    286         �           2604    17654 ,   beone_subkon_out_detail subkon_out_detail_id    DEFAULT     �   ALTER TABLE ONLY public.beone_subkon_out_detail ALTER COLUMN subkon_out_detail_id SET DEFAULT nextval('public.beone_subkon_out_detail_subkon_out_detail_id_seq'::regclass);
 [   ALTER TABLE public.beone_subkon_out_detail ALTER COLUMN subkon_out_detail_id DROP DEFAULT;
       public          postgres    false    289    288         �           2604    17655 %   beone_subkon_out_header subkon_out_id    DEFAULT     �   ALTER TABLE ONLY public.beone_subkon_out_header ALTER COLUMN subkon_out_id SET DEFAULT nextval('public.beone_subkon_out_header_subkon_out_id_seq'::regclass);
 T   ALTER TABLE public.beone_subkon_out_header ALTER COLUMN subkon_out_id DROP DEFAULT;
       public          postgres    false    291    290         �           2604    17656    beone_tipe_coa tipe_coa_id    DEFAULT     �   ALTER TABLE ONLY public.beone_tipe_coa ALTER COLUMN tipe_coa_id SET DEFAULT nextval('public.beone_tipe_coa_tipe_coa_id_seq'::regclass);
 I   ALTER TABLE public.beone_tipe_coa ALTER COLUMN tipe_coa_id DROP DEFAULT;
       public          postgres    false    293    292         �           2604    17657 *   beone_tipe_coa_transaksi tipe_coa_trans_id    DEFAULT     �   ALTER TABLE ONLY public.beone_tipe_coa_transaksi ALTER COLUMN tipe_coa_trans_id SET DEFAULT nextval('public.beone_tipe_coa_transaksi_tipe_coa_trans_id_seq'::regclass);
 Y   ALTER TABLE public.beone_tipe_coa_transaksi ALTER COLUMN tipe_coa_trans_id DROP DEFAULT;
       public          postgres    false    295    294         �           2604    17658 &   beone_transfer_stock transfer_stock_id    DEFAULT     �   ALTER TABLE ONLY public.beone_transfer_stock ALTER COLUMN transfer_stock_id SET DEFAULT nextval('public.beone_transfer_stock_transfer_stock_id_seq'::regclass);
 U   ALTER TABLE public.beone_transfer_stock ALTER COLUMN transfer_stock_id DROP DEFAULT;
       public          postgres    false    299    296                     2604    17659 4   beone_transfer_stock_detail transfer_stock_detail_id    DEFAULT     �   ALTER TABLE ONLY public.beone_transfer_stock_detail ALTER COLUMN transfer_stock_detail_id SET DEFAULT nextval('public.beone_transfer_stock_detail_transfer_stock_detail_id_seq'::regclass);
 c   ALTER TABLE public.beone_transfer_stock_detail ALTER COLUMN transfer_stock_detail_id DROP DEFAULT;
       public          postgres    false    298    297                    2604    17660    beone_user user_id    DEFAULT     x   ALTER TABLE ONLY public.beone_user ALTER COLUMN user_id SET DEFAULT nextval('public.beone_user_user_id_seq'::regclass);
 A   ALTER TABLE public.beone_user ALTER COLUMN user_id DROP DEFAULT;
       public          postgres    false    301    300                    2604    17661 &   beone_voucher_detail voucher_detail_id    DEFAULT     �   ALTER TABLE ONLY public.beone_voucher_detail ALTER COLUMN voucher_detail_id SET DEFAULT nextval('public.beone_voucher_detail_voucher_detail_id_seq'::regclass);
 U   ALTER TABLE public.beone_voucher_detail ALTER COLUMN voucher_detail_id DROP DEFAULT;
       public          postgres    false    303    302                    2604    17662 &   beone_voucher_header voucher_header_id    DEFAULT     �   ALTER TABLE ONLY public.beone_voucher_header ALTER COLUMN voucher_header_id SET DEFAULT nextval('public.beone_voucher_header_voucher_header_id_seq'::regclass);
 U   ALTER TABLE public.beone_voucher_header ALTER COLUMN voucher_header_id DROP DEFAULT;
       public          postgres    false    305    304         �          0    17325    beone_adjustment 
   TABLE DATA           �   COPY public.beone_adjustment (adjustment_id, adjustment_no, adjustment_date, keterangan, item_id, qty_adjustment, posisi_qty, update_by, update_date, flag) FROM stdin;
    public          postgres    false    202       3312.dat �          0    17330 	   beone_coa 
   TABLE DATA           �   COPY public.beone_coa (coa_id, nama, nomor, tipe_akun, debet_valas, debet_idr, kredit_valas, kredit_idr, dk, tipe_transaksi) FROM stdin;
    public          postgres    false    204       3314.dat �          0    17338    beone_coa_jurnal 
   TABLE DATA           q   COPY public.beone_coa_jurnal (coa_jurnal_id, nama_coa_jurnal, keterangan_coa_jurnal, coa_id, coa_no) FROM stdin;
    public          postgres    false    206       3316.dat �          0    17346    beone_country 
   TABLE DATA           M   COPY public.beone_country (country_id, nama, country_code, flag) FROM stdin;
    public          postgres    false    208       3318.dat �          0    17351    beone_custsup 
   TABLE DATA           4  COPY public.beone_custsup (custsup_id, nama, alamat, tipe_custsup, negara, saldo_hutang_idr, saldo_hutang_valas, saldo_piutang_idr, saldo_piutang_valas, flag, pelunasan_hutang_idr, pelunasan_hutang_valas, pelunasan_piutang_idr, pelunasan_piutang_valas, status_lunas_piutang, status_lunas_hutang) FROM stdin;
    public          postgres    false    210       3320.dat �          0    17356    beone_export_detail 
   TABLE DATA           �   COPY public.beone_export_detail (export_detail_id, export_header_id, item_id, qty, satuan_qty, price, pack_qty, satuan_pack, origin_country, doc, volume, netto, brutto, flag, sales_header_id, gudang_id) FROM stdin;
    public          postgres    false    212       3322.dat �          0    17362    beone_export_header 
   TABLE DATA           �  COPY public.beone_export_header (export_header_id, jenis_bc, car_no, bc_no, bc_date, kontrak_no, kontrak_date, jenis_export, invoice_no, invoice_date, surat_jalan_no, surat_jalan_date, receiver_id, country_id, price_type, amount_value, valas_value, insurance_type, insurance_value, freight, flag, status, delivery_date, update_by, update_date, delivery_no, vessel, port_loading, port_destination, container, no_container, no_seal) FROM stdin;
    public          postgres    false    214       3324.dat �          0    17370    beone_gl 
   TABLE DATA           �   COPY public.beone_gl (gl_id, gl_date, coa_id, coa_no, coa_id_lawan, coa_no_lawan, keterangan, debet, kredit, pasangan_no, gl_number, update_by, update_date) FROM stdin;
    public          postgres    false    216       3326.dat            0    17375    beone_gudang 
   TABLE DATA           I   COPY public.beone_gudang (gudang_id, nama, keterangan, flag) FROM stdin;
    public          postgres    false    218       3328.dat           0    17378    beone_gudang_detail 
   TABLE DATA           �   COPY public.beone_gudang_detail (gudang_detail_id, gudang_id, trans_date, item_id, qty_in, qty_out, nomor_transaksi, update_by, update_date, flag, keterangan, kode_tracing) FROM stdin;
    public          postgres    false    219       3329.dat           0    17390    beone_hutang_piutang 
   TABLE DATA           �   COPY public.beone_hutang_piutang (hutang_piutang_id, custsup_id, trans_date, nomor, keterangan, valas_trans, idr_trans, valas_pelunasan, idr_pelunasan, tipe_trans, update_by, update_date, flag, status_lunas) FROM stdin;
    public          postgres    false    224       3334.dat           0    17395    beone_import_detail 
   TABLE DATA           !  COPY public.beone_import_detail (import_detail_id, item_id, qty, satuan_qty, price, pack_qty, satuan_pack, origin_country, volume, netto, brutto, hscode, tbm, ppnn, tpbm, cukai, sat_cukai, cukai_value, bea_masuk, sat_bea_masuk, flag, item_type_id, import_header_id, gudang_id) FROM stdin;
    public          postgres    false    226       3336.dat 
          0    17401    beone_import_header 
   TABLE DATA           ]  COPY public.beone_import_header (import_header_id, jenis_bc, car_no, bc_no, bc_date, invoice_no, invoice_date, kontrak_no, kontrak_date, purpose_id, supplier_id, price_type, "from", amount_value, valas_value, value_added, discount, insurance_type, insurace_value, freight, flag, update_by, update_date, status, receive_date, receive_no) FROM stdin;
    public          postgres    false    228       3338.dat           0    17406    beone_inventory 
   TABLE DATA           �   COPY public.beone_inventory (intvent_trans_id, intvent_trans_no, item_id, trans_date, keterangan, qty_in, value_in, qty_out, value_out, sa_qty, sa_unit_price, sa_amount, flag, update_by, update_date, gudang_id) FROM stdin;
    public          postgres    false    230       3340.dat           0    17412 
   beone_item 
   TABLE DATA           �   COPY public.beone_item (item_id, nama, item_code, saldo_qty, saldo_idr, keterangan, flag, item_type_id, hscode, satuan_id, item_category_id) FROM stdin;
    public          postgres    false    232       3342.dat           0    17416    beone_item_category 
   TABLE DATA           W   COPY public.beone_item_category (item_category_id, nama, keterangan, flag) FROM stdin;
    public          postgres    false    233       3343.dat           0    17421    beone_item_type 
   TABLE DATA           O   COPY public.beone_item_type (item_type_id, nama, keterangan, flag) FROM stdin;
    public          postgres    false    235       3345.dat           0    17426    beone_kode_trans 
   TABLE DATA           `   COPY public.beone_kode_trans (kode_trans_id, nama, coa_id, kode_bank, in_out, flag) FROM stdin;
    public          postgres    false    237       3347.dat           0    17434    beone_komposisi 
   TABLE DATA           h   COPY public.beone_komposisi (komposisi_id, item_jadi_id, item_baku_id, qty_item_baku, flag) FROM stdin;
    public          postgres    false    239       3349.dat           0    17439    beone_konfigurasi_perusahaan 
   TABLE DATA           �   COPY public.beone_konfigurasi_perusahaan (nama_perusahaan, alamat_perusahaan, kota_perusahaan, provinsi_perusahaan, kode_pos_perusahaan, logo_perusahaan) FROM stdin;
    public          postgres    false    241       3351.dat           0    17445    beone_konversi_stok_detail 
   TABLE DATA           �   COPY public.beone_konversi_stok_detail (konversi_stok_detail_id, konversi_stok_header_id, item_id, qty, qty_real, satuan_qty, gudang_id, qty_allocation) FROM stdin;
    public          postgres    false    242       3352.dat           0    17451    beone_konversi_stok_header 
   TABLE DATA           �   COPY public.beone_konversi_stok_header (konversi_stok_header_id, item_id, konversi_stok_no, konversi_stok_date, flag, qty, satuan_qty, gudang_id, qty_real) FROM stdin;
    public          postgres    false    244       3354.dat           0    17457 	   beone_log 
   TABLE DATA           S   COPY public.beone_log (log_id, log_user, log_tipe, log_desc, log_time) FROM stdin;
    public          postgres    false    246       3356.dat           0    17462    beone_opname_detail 
   TABLE DATA           �   COPY public.beone_opname_detail (opname_detail_id, opname_header_id, item_id, qty_existing, qty_opname, qty_selisih, status_opname, flag) FROM stdin;
    public          postgres    false    248       3358.dat            0    17467    beone_opname_header 
   TABLE DATA           �   COPY public.beone_opname_header (opname_header_id, document_no, opname_date, total_item_opname, total_item_opname_match, total_item_opname_plus, total_item_opname_min, update_by, flag, keterangan, update_date) FROM stdin;
    public          postgres    false    250       3360.dat "          0    17472    beone_peleburan 
   TABLE DATA           �   COPY public.beone_peleburan (peleburan_id, peleburan_no, peleburan_date, keterangan, item_id, qty_peleburan, update_by, update_date, flag) FROM stdin;
    public          postgres    false    252       3362.dat $          0    17477    beone_pemakaian_bahan_detail 
   TABLE DATA           z   COPY public.beone_pemakaian_bahan_detail (pemakaian_detail_id, pemakaian_header_id, item_id, qty, unit_price) FROM stdin;
    public          postgres    false    254       3364.dat           0    17385    beone_pemakaian_bahan_header 
   TABLE DATA           �   COPY public.beone_pemakaian_bahan_header (pemakaian_header_id, nomor_pemakaian, keterangan, tgl_pemakaian, update_by, update_date, nomor_pm, amount_pemakaian, status) FROM stdin;
    public          postgres    false    222       3332.dat &          0    17482    beone_pm_detail 
   TABLE DATA           ^   COPY public.beone_pm_detail (pm_detail_id, item_id, qty, satuan_id, pm_header_id) FROM stdin;
    public          postgres    false    256       3366.dat (          0    17487    beone_pm_header 
   TABLE DATA           t   COPY public.beone_pm_header (pm_header_id, no_pm, tgl_pm, customer_id, qty, keterangan_artikel, status) FROM stdin;
    public          postgres    false    258       3368.dat *          0    17495    beone_po_import_detail 
   TABLE DATA           {   COPY public.beone_po_import_detail (purchase_detail_id, purchase_header_id, item_id, qty, price, amount, flag) FROM stdin;
    public          postgres    false    260       3370.dat ,          0    17500    beone_po_import_header 
   TABLE DATA           �   COPY public.beone_po_import_header (purchase_header_id, purchase_no, trans_date, supplier_id, keterangan, grandtotal, flag, update_by, update_date) FROM stdin;
    public          postgres    false    262       3372.dat .          0    17505    beone_produksi_detail 
   TABLE DATA           l   COPY public.beone_produksi_detail (produksi_detail_id, produksi_header_id, pemakaian_header_id) FROM stdin;
    public          postgres    false    264       3374.dat 0          0    17510    beone_produksi_header 
   TABLE DATA           �   COPY public.beone_produksi_header (produksi_header_id, nomor_produksi, tgl_produksi, tipe_produksi, item_produksi, qty_hasil_produksi, keterangan, flag, updated_by, udpated_date, pm_header_id) FROM stdin;
    public          postgres    false    266       3376.dat 2          0    17515    beone_purchase_detail 
   TABLE DATA           z   COPY public.beone_purchase_detail (purchase_detail_id, purchase_header_id, item_id, qty, price, amount, flag) FROM stdin;
    public          postgres    false    268       3378.dat 4          0    17520    beone_purchase_header 
   TABLE DATA           �   COPY public.beone_purchase_header (purchase_header_id, purchase_no, trans_date, supplier_id, keterangan, update_by, update_date, flag, ppn, subtotal, ppn_value, grandtotal, realisasi) FROM stdin;
    public          postgres    false    270       3380.dat 6          0    17526    beone_received_import 
   TABLE DATA           �   COPY public.beone_received_import (received_id, nomor_aju, nomor_dokumen_pabean, tanggal_received, status_received, flag, update_by, update_date, nomor_received, tpb_header_id, kurs, po_import_header_id) FROM stdin;
    public          postgres    false    272       3382.dat 8          0    17531 
   beone_role 
   TABLE DATA           	  COPY public.beone_role (role_id, nama_role, keterangan, master_menu, pembelian_menu, penjualan_menu, inventory_menu, produksi_menu, asset_menu, jurnal_umum_menu, kasbank_menu, laporan_inventory, laporan_keuangan, konfigurasi, import_menu, eksport_menu) FROM stdin;
    public          postgres    false    274       3384.dat :          0    17536    beone_role_user 
   TABLE DATA           �  COPY public.beone_role_user (role_id, nama_role, keterangan, master_menu, user_add, user_edit, user_delete, role_add, role_edit, role_delete, customer_add, customer_edit, customer_delete, supplier_add, supplier_edit, supplier_delete, item_add, item_edit, item_delete, jenis_add, jenis_edit, jenis_delete, satuan_add, satuan_edit, satuan_delete, gudang_add, gudang_edit, gudang_delete, master_import, po_add, po_edit, po_delete, tracing, terima_barang, master_pembelian, pembelian_add, pembelian_edit, pembelian_delete, kredit_note_add, kredit_note_edit, kredit_note_delete, master_eksport, eksport_add, eksport_edit, eksport_delete, kirim_barang, menu_penjualan, penjualan_add, penjualan_edit, penjualan_delete, debit_note_add, debit_note_edit, debit_note_delete, menu_inventory, pindah_gudang, stockopname_add, stockopname_edit, stockopname_delete, stockopname_opname, adjustment_add, adjustment_edit, adjustment_delete, pemusnahan_add, pemusnahan_edit, pemusnahan_delete, recal_inventory, menu_produksi, produksi_add, produksi_edit, produksi_delete, menu_asset, menu_jurnal_umum, jurnal_umum_add, jurnal_umum_edit, jurnal_umum_delete, menu_kas_bank, kas_bank_add, kas_bank_edit, kas_bank_delete, menu_laporan_inventory, menu_laporan_keuangan, menu_konfigurasi) FROM stdin;
    public          postgres    false    276       3386.dat <          0    17541    beone_sales_detail 
   TABLE DATA           q   COPY public.beone_sales_detail (sales_detail_id, sales_header_id, item_id, qty, price, amount, flag) FROM stdin;
    public          postgres    false    278       3388.dat >          0    17546    beone_sales_header 
   TABLE DATA           �   COPY public.beone_sales_header (sales_header_id, sales_no, trans_date, customer_id, keterangan, ppn, subtotal, ppn_value, grandtotal, update_by, update_date, flag, biaya, status, realisasi) FROM stdin;
    public          postgres    false    280       3390.dat @          0    17552    beone_satuan_item 
   TABLE DATA           U   COPY public.beone_satuan_item (satuan_id, satuan_code, keterangan, flag) FROM stdin;
    public          postgres    false    282       3392.dat B          0    17557    beone_subkon_in_detail 
   TABLE DATA           n   COPY public.beone_subkon_in_detail (subkon_in_detail_id, subkon_in_header_id, item_id, qty, flag) FROM stdin;
    public          postgres    false    284       3394.dat D          0    17562    beone_subkon_in_header 
   TABLE DATA           �   COPY public.beone_subkon_in_header (subkon_in_id, no_subkon_in, no_subkon_out, trans_date, keterangan, nomor_aju, biaya_subkon, supplier_id, flag, update_by, update_date) FROM stdin;
    public          postgres    false    286       3396.dat F          0    17567    beone_subkon_out_detail 
   TABLE DATA           }   COPY public.beone_subkon_out_detail (subkon_out_detail_id, subkon_out_header_id, item_id, qty, unit_price, flag) FROM stdin;
    public          postgres    false    288       3398.dat H          0    17572    beone_subkon_out_header 
   TABLE DATA           �   COPY public.beone_subkon_out_header (subkon_out_id, no_subkon_out, trans_date, supplier_id, keterangan, nomor_aju, flag, update_by, update_date) FROM stdin;
    public          postgres    false    290       3400.dat J          0    17577    beone_tipe_coa 
   TABLE DATA           ;   COPY public.beone_tipe_coa (tipe_coa_id, nama) FROM stdin;
    public          postgres    false    292       3402.dat L          0    17582    beone_tipe_coa_transaksi 
   TABLE DATA           Q   COPY public.beone_tipe_coa_transaksi (tipe_coa_trans_id, nama, flag) FROM stdin;
    public          postgres    false    294       3404.dat N          0    17587    beone_transfer_stock 
   TABLE DATA           �   COPY public.beone_transfer_stock (transfer_stock_id, transfer_no, transfer_date, coa_kode_biaya, keterangan, update_by, update_date, flag) FROM stdin;
    public          postgres    false    296       3406.dat O          0    17590    beone_transfer_stock_detail 
   TABLE DATA           �   COPY public.beone_transfer_stock_detail (transfer_stock_detail_id, transfer_stock_header_id, tipe_transfer_stock, item_id, qty, gudang_id, biaya, update_by, update_date, flag, persen_produksi) FROM stdin;
    public          postgres    false    297       3407.dat R          0    17597 
   beone_user 
   TABLE DATA           n   COPY public.beone_user (user_id, username, password, nama, role_id, update_by, update_date, flag) FROM stdin;
    public          postgres    false    300       3410.dat T          0    17602    beone_voucher_detail 
   TABLE DATA           �   COPY public.beone_voucher_detail (voucher_detail_id, voucher_header_id, coa_id_lawan, coa_no_lawan, jumlah_valas, kurs, jumlah_idr, keterangan_detail) FROM stdin;
    public          postgres    false    302       3412.dat V          0    17607    beone_voucher_header 
   TABLE DATA           �   COPY public.beone_voucher_header (voucher_header_id, voucher_number, voucher_date, tipe, keterangan, coa_id_cash_bank, coa_no, update_by, update_date) FROM stdin;
    public          postgres    false    304       3414.dat �           0    0 "   beone_adjustment_adjustment_id_seq    SEQUENCE SET     Q   SELECT pg_catalog.setval('public.beone_adjustment_adjustment_id_seq', 10, true);
          public          postgres    false    203         �           0    0    beone_coa_coa_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.beone_coa_coa_id_seq', 1, false);
          public          postgres    false    205         �           0    0 "   beone_coa_jurnal_coa_jurnal_id_seq    SEQUENCE SET     Q   SELECT pg_catalog.setval('public.beone_coa_jurnal_coa_jurnal_id_seq', 12, true);
          public          postgres    false    207         �           0    0    beone_country_country_id_seq    SEQUENCE SET     J   SELECT pg_catalog.setval('public.beone_country_country_id_seq', 3, true);
          public          postgres    false    209         �           0    0    beone_custsup_custsup_id_seq    SEQUENCE SET     K   SELECT pg_catalog.setval('public.beone_custsup_custsup_id_seq', 34, true);
          public          postgres    false    211         �           0    0 (   beone_export_detail_export_detail_id_seq    SEQUENCE SET     W   SELECT pg_catalog.setval('public.beone_export_detail_export_detail_id_seq', 39, true);
          public          postgres    false    213         �           0    0 (   beone_export_header_export_header_id_seq    SEQUENCE SET     W   SELECT pg_catalog.setval('public.beone_export_header_export_header_id_seq', 34, true);
          public          postgres    false    215         �           0    0    beone_gl_gl_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.beone_gl_gl_id_seq', 2529, true);
          public          postgres    false    217         �           0    0 (   beone_gudang_detail_gudang_detail_id_seq    SEQUENCE SET     Y   SELECT pg_catalog.setval('public.beone_gudang_detail_gudang_detail_id_seq', 6228, true);
          public          postgres    false    220         �           0    0    beone_gudang_gudang_id_seq    SEQUENCE SET     H   SELECT pg_catalog.setval('public.beone_gudang_gudang_id_seq', 6, true);
          public          postgres    false    221         �           0    0 4   beone_header_pemakaian_bahan_pemakaian_header_id_seq    SEQUENCE SET     c   SELECT pg_catalog.setval('public.beone_header_pemakaian_bahan_pemakaian_header_id_seq', 35, true);
          public          postgres    false    223         �           0    0 *   beone_hutang_piutang_hutang_piutang_id_seq    SEQUENCE SET     Z   SELECT pg_catalog.setval('public.beone_hutang_piutang_hutang_piutang_id_seq', 437, true);
          public          postgres    false    225         �           0    0 (   beone_import_detail_import_detail_id_seq    SEQUENCE SET     W   SELECT pg_catalog.setval('public.beone_import_detail_import_detail_id_seq', 39, true);
          public          postgres    false    227         �           0    0 (   beone_import_header_import_header_id_seq    SEQUENCE SET     W   SELECT pg_catalog.setval('public.beone_import_header_import_header_id_seq', 31, true);
          public          postgres    false    229         �           0    0 $   beone_inventory_intvent_trans_id_seq    SEQUENCE SET     U   SELECT pg_catalog.setval('public.beone_inventory_intvent_trans_id_seq', 1235, true);
          public          postgres    false    231         �           0    0    beone_item_item_id_seq    SEQUENCE SET     G   SELECT pg_catalog.setval('public.beone_item_item_id_seq', 2107, true);
          public          postgres    false    234         �           0    0     beone_item_type_item_type_id_seq    SEQUENCE SET     O   SELECT pg_catalog.setval('public.beone_item_type_item_type_id_seq', 16, true);
          public          postgres    false    236         �           0    0 "   beone_kode_trans_kode_trans_id_seq    SEQUENCE SET     Q   SELECT pg_catalog.setval('public.beone_kode_trans_kode_trans_id_seq', 25, true);
          public          postgres    false    238         �           0    0     beone_komposisi_komposisi_id_seq    SEQUENCE SET     N   SELECT pg_catalog.setval('public.beone_komposisi_komposisi_id_seq', 6, true);
          public          postgres    false    240         �           0    0 6   beone_konversi_stok_detail_konversi_stok_detail_id_seq    SEQUENCE SET     d   SELECT pg_catalog.setval('public.beone_konversi_stok_detail_konversi_stok_detail_id_seq', 6, true);
          public          postgres    false    243         �           0    0 6   beone_konversi_stok_header_konversi_stok_header_id_seq    SEQUENCE SET     d   SELECT pg_catalog.setval('public.beone_konversi_stok_header_konversi_stok_header_id_seq', 3, true);
          public          postgres    false    245         �           0    0    beone_log_log_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.beone_log_log_id_seq', 413, true);
          public          postgres    false    247         �           0    0 (   beone_opname_detail_opname_detail_id_seq    SEQUENCE SET     Y   SELECT pg_catalog.setval('public.beone_opname_detail_opname_detail_id_seq', 6037, true);
          public          postgres    false    249         �           0    0 (   beone_opname_header_opname_header_id_seq    SEQUENCE SET     W   SELECT pg_catalog.setval('public.beone_opname_header_opname_header_id_seq', 13, true);
          public          postgres    false    251         �           0    0     beone_peleburan_peleburan_id_seq    SEQUENCE SET     N   SELECT pg_catalog.setval('public.beone_peleburan_peleburan_id_seq', 6, true);
          public          postgres    false    253         �           0    0 4   beone_pemakaian_bahan_detail_pemakaian_detail_id_seq    SEQUENCE SET     c   SELECT pg_catalog.setval('public.beone_pemakaian_bahan_detail_pemakaian_detail_id_seq', 52, true);
          public          postgres    false    255         �           0    0     beone_pm_detail_pm_detail_id_seq    SEQUENCE SET     O   SELECT pg_catalog.setval('public.beone_pm_detail_pm_detail_id_seq', 1, false);
          public          postgres    false    257         �           0    0     beone_pm_header_pm_header_id_seq    SEQUENCE SET     O   SELECT pg_catalog.setval('public.beone_pm_header_pm_header_id_seq', 13, true);
          public          postgres    false    259         �           0    0 -   beone_po_import_detail_purchase_detail_id_seq    SEQUENCE SET     ]   SELECT pg_catalog.setval('public.beone_po_import_detail_purchase_detail_id_seq', 113, true);
          public          postgres    false    261         �           0    0 -   beone_po_import_header_purchase_header_id_seq    SEQUENCE SET     ]   SELECT pg_catalog.setval('public.beone_po_import_header_purchase_header_id_seq', 108, true);
          public          postgres    false    263         �           0    0 ,   beone_produksi_detail_produksi_detail_id_seq    SEQUENCE SET     [   SELECT pg_catalog.setval('public.beone_produksi_detail_produksi_detail_id_seq', 48, true);
          public          postgres    false    265         �           0    0 ,   beone_produksi_header_produksi_header_id_seq    SEQUENCE SET     [   SELECT pg_catalog.setval('public.beone_produksi_header_produksi_header_id_seq', 46, true);
          public          postgres    false    267         �           0    0 ,   beone_purchase_detail_purchase_detail_id_seq    SEQUENCE SET     [   SELECT pg_catalog.setval('public.beone_purchase_detail_purchase_detail_id_seq', 96, true);
          public          postgres    false    269         �           0    0 ,   beone_purchase_header_purchase_header_id_seq    SEQUENCE SET     [   SELECT pg_catalog.setval('public.beone_purchase_header_purchase_header_id_seq', 65, true);
          public          postgres    false    271         �           0    0 %   beone_received_import_received_id_seq    SEQUENCE SET     U   SELECT pg_catalog.setval('public.beone_received_import_received_id_seq', 225, true);
          public          postgres    false    273         �           0    0    beone_role_role_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.beone_role_role_id_seq', 6, true);
          public          postgres    false    275         �           0    0    beone_role_user_role_id_seq    SEQUENCE SET     I   SELECT pg_catalog.setval('public.beone_role_user_role_id_seq', 3, true);
          public          postgres    false    277         �           0    0 &   beone_sales_detail_sales_detail_id_seq    SEQUENCE SET     U   SELECT pg_catalog.setval('public.beone_sales_detail_sales_detail_id_seq', 26, true);
          public          postgres    false    279         �           0    0 &   beone_sales_header_sales_header_id_seq    SEQUENCE SET     U   SELECT pg_catalog.setval('public.beone_sales_header_sales_header_id_seq', 20, true);
          public          postgres    false    281         �           0    0    beone_satuan_item_satuan_id_seq    SEQUENCE SET     M   SELECT pg_catalog.setval('public.beone_satuan_item_satuan_id_seq', 3, true);
          public          postgres    false    283         �           0    0 .   beone_subkon_in_detail_subkon_in_detail_id_seq    SEQUENCE SET     \   SELECT pg_catalog.setval('public.beone_subkon_in_detail_subkon_in_detail_id_seq', 8, true);
          public          postgres    false    285         �           0    0 '   beone_subkon_in_header_subkon_in_id_seq    SEQUENCE SET     U   SELECT pg_catalog.setval('public.beone_subkon_in_header_subkon_in_id_seq', 4, true);
          public          postgres    false    287         �           0    0 0   beone_subkon_out_detail_subkon_out_detail_id_seq    SEQUENCE SET     _   SELECT pg_catalog.setval('public.beone_subkon_out_detail_subkon_out_detail_id_seq', 37, true);
          public          postgres    false    289         �           0    0 )   beone_subkon_out_header_subkon_out_id_seq    SEQUENCE SET     X   SELECT pg_catalog.setval('public.beone_subkon_out_header_subkon_out_id_seq', 18, true);
          public          postgres    false    291         �           0    0    beone_tipe_coa_tipe_coa_id_seq    SEQUENCE SET     M   SELECT pg_catalog.setval('public.beone_tipe_coa_tipe_coa_id_seq', 1, false);
          public          postgres    false    293         �           0    0 .   beone_tipe_coa_transaksi_tipe_coa_trans_id_seq    SEQUENCE SET     ]   SELECT pg_catalog.setval('public.beone_tipe_coa_transaksi_tipe_coa_trans_id_seq', 20, true);
          public          postgres    false    295         �           0    0 8   beone_transfer_stock_detail_transfer_stock_detail_id_seq    SEQUENCE SET     h   SELECT pg_catalog.setval('public.beone_transfer_stock_detail_transfer_stock_detail_id_seq', 656, true);
          public          postgres    false    298         �           0    0 *   beone_transfer_stock_transfer_stock_id_seq    SEQUENCE SET     Z   SELECT pg_catalog.setval('public.beone_transfer_stock_transfer_stock_id_seq', 299, true);
          public          postgres    false    299         �           0    0    beone_user_user_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.beone_user_user_id_seq', 22, true);
          public          postgres    false    301         �           0    0 *   beone_voucher_detail_voucher_detail_id_seq    SEQUENCE SET     Z   SELECT pg_catalog.setval('public.beone_voucher_detail_voucher_detail_id_seq', 215, true);
          public          postgres    false    303         �           0    0 *   beone_voucher_header_voucher_header_id_seq    SEQUENCE SET     Z   SELECT pg_catalog.setval('public.beone_voucher_header_voucher_header_id_seq', 142, true);
          public          postgres    false    305                    2606    17664 &   beone_adjustment beone_adjustment_pkey 
   CONSTRAINT     o   ALTER TABLE ONLY public.beone_adjustment
    ADD CONSTRAINT beone_adjustment_pkey PRIMARY KEY (adjustment_id);
 P   ALTER TABLE ONLY public.beone_adjustment DROP CONSTRAINT beone_adjustment_pkey;
       public            postgres    false    202         	           2606    17666 &   beone_coa_jurnal beone_coa_jurnal_pkey 
   CONSTRAINT     o   ALTER TABLE ONLY public.beone_coa_jurnal
    ADD CONSTRAINT beone_coa_jurnal_pkey PRIMARY KEY (coa_jurnal_id);
 P   ALTER TABLE ONLY public.beone_coa_jurnal DROP CONSTRAINT beone_coa_jurnal_pkey;
       public            postgres    false    206                    2606    17668    beone_coa beone_coa_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.beone_coa
    ADD CONSTRAINT beone_coa_pkey PRIMARY KEY (coa_id);
 B   ALTER TABLE ONLY public.beone_coa DROP CONSTRAINT beone_coa_pkey;
       public            postgres    false    204                    2606    17670     beone_country beone_country_pkey 
   CONSTRAINT     f   ALTER TABLE ONLY public.beone_country
    ADD CONSTRAINT beone_country_pkey PRIMARY KEY (country_id);
 J   ALTER TABLE ONLY public.beone_country DROP CONSTRAINT beone_country_pkey;
       public            postgres    false    208                    2606    17672 ,   beone_export_detail beone_export_detail_pkey 
   CONSTRAINT     x   ALTER TABLE ONLY public.beone_export_detail
    ADD CONSTRAINT beone_export_detail_pkey PRIMARY KEY (export_detail_id);
 V   ALTER TABLE ONLY public.beone_export_detail DROP CONSTRAINT beone_export_detail_pkey;
       public            postgres    false    212                    2606    17674    beone_gl beone_gl_pkey 
   CONSTRAINT     W   ALTER TABLE ONLY public.beone_gl
    ADD CONSTRAINT beone_gl_pkey PRIMARY KEY (gl_id);
 @   ALTER TABLE ONLY public.beone_gl DROP CONSTRAINT beone_gl_pkey;
       public            postgres    false    216                    2606    17676 ,   beone_gudang_detail beone_gudang_detail_pkey 
   CONSTRAINT     x   ALTER TABLE ONLY public.beone_gudang_detail
    ADD CONSTRAINT beone_gudang_detail_pkey PRIMARY KEY (gudang_detail_id);
 V   ALTER TABLE ONLY public.beone_gudang_detail DROP CONSTRAINT beone_gudang_detail_pkey;
       public            postgres    false    219                    2606    17678    beone_gudang beone_gudang_pkey 
   CONSTRAINT     c   ALTER TABLE ONLY public.beone_gudang
    ADD CONSTRAINT beone_gudang_pkey PRIMARY KEY (gudang_id);
 H   ALTER TABLE ONLY public.beone_gudang DROP CONSTRAINT beone_gudang_pkey;
       public            postgres    false    218                    2606    17680 >   beone_pemakaian_bahan_header beone_header_pemakaian_bahan_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.beone_pemakaian_bahan_header
    ADD CONSTRAINT beone_header_pemakaian_bahan_pkey PRIMARY KEY (pemakaian_header_id);
 h   ALTER TABLE ONLY public.beone_pemakaian_bahan_header DROP CONSTRAINT beone_header_pemakaian_bahan_pkey;
       public            postgres    false    222                    2606    17682 .   beone_hutang_piutang beone_hutang_piutang_pkey 
   CONSTRAINT     {   ALTER TABLE ONLY public.beone_hutang_piutang
    ADD CONSTRAINT beone_hutang_piutang_pkey PRIMARY KEY (hutang_piutang_id);
 X   ALTER TABLE ONLY public.beone_hutang_piutang DROP CONSTRAINT beone_hutang_piutang_pkey;
       public            postgres    false    224                    2606    17684 ,   beone_import_detail beone_import_detail_pkey 
   CONSTRAINT     x   ALTER TABLE ONLY public.beone_import_detail
    ADD CONSTRAINT beone_import_detail_pkey PRIMARY KEY (import_detail_id);
 V   ALTER TABLE ONLY public.beone_import_detail DROP CONSTRAINT beone_import_detail_pkey;
       public            postgres    false    226                    2606    17686 ,   beone_import_header beone_import_header_pkey 
   CONSTRAINT     x   ALTER TABLE ONLY public.beone_import_header
    ADD CONSTRAINT beone_import_header_pkey PRIMARY KEY (import_header_id);
 V   ALTER TABLE ONLY public.beone_import_header DROP CONSTRAINT beone_import_header_pkey;
       public            postgres    false    228                    2606    17688 $   beone_inventory beone_inventory_pkey 
   CONSTRAINT     p   ALTER TABLE ONLY public.beone_inventory
    ADD CONSTRAINT beone_inventory_pkey PRIMARY KEY (intvent_trans_id);
 N   ALTER TABLE ONLY public.beone_inventory DROP CONSTRAINT beone_inventory_pkey;
       public            postgres    false    230         !           2606    17690 ,   beone_item_category beone_item_category_pkey 
   CONSTRAINT     x   ALTER TABLE ONLY public.beone_item_category
    ADD CONSTRAINT beone_item_category_pkey PRIMARY KEY (item_category_id);
 V   ALTER TABLE ONLY public.beone_item_category DROP CONSTRAINT beone_item_category_pkey;
       public            postgres    false    233                    2606    17692    beone_item beone_item_pkey 
   CONSTRAINT     ]   ALTER TABLE ONLY public.beone_item
    ADD CONSTRAINT beone_item_pkey PRIMARY KEY (item_id);
 D   ALTER TABLE ONLY public.beone_item DROP CONSTRAINT beone_item_pkey;
       public            postgres    false    232         #           2606    17694 $   beone_item_type beone_item_type_pkey 
   CONSTRAINT     l   ALTER TABLE ONLY public.beone_item_type
    ADD CONSTRAINT beone_item_type_pkey PRIMARY KEY (item_type_id);
 N   ALTER TABLE ONLY public.beone_item_type DROP CONSTRAINT beone_item_type_pkey;
       public            postgres    false    235         %           2606    17696 &   beone_kode_trans beone_kode_trans_pkey 
   CONSTRAINT     o   ALTER TABLE ONLY public.beone_kode_trans
    ADD CONSTRAINT beone_kode_trans_pkey PRIMARY KEY (kode_trans_id);
 P   ALTER TABLE ONLY public.beone_kode_trans DROP CONSTRAINT beone_kode_trans_pkey;
       public            postgres    false    237         '           2606    17698 $   beone_komposisi beone_komposisi_pkey 
   CONSTRAINT     l   ALTER TABLE ONLY public.beone_komposisi
    ADD CONSTRAINT beone_komposisi_pkey PRIMARY KEY (komposisi_id);
 N   ALTER TABLE ONLY public.beone_komposisi DROP CONSTRAINT beone_komposisi_pkey;
       public            postgres    false    239         )           2606    17700 :   beone_konversi_stok_detail beone_konversi_stok_detail_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.beone_konversi_stok_detail
    ADD CONSTRAINT beone_konversi_stok_detail_pkey PRIMARY KEY (konversi_stok_detail_id);
 d   ALTER TABLE ONLY public.beone_konversi_stok_detail DROP CONSTRAINT beone_konversi_stok_detail_pkey;
       public            postgres    false    242         +           2606    17702 :   beone_konversi_stok_header beone_konversi_stok_header_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.beone_konversi_stok_header
    ADD CONSTRAINT beone_konversi_stok_header_pkey PRIMARY KEY (konversi_stok_header_id);
 d   ALTER TABLE ONLY public.beone_konversi_stok_header DROP CONSTRAINT beone_konversi_stok_header_pkey;
       public            postgres    false    244         -           2606    17704    beone_log beone_log_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.beone_log
    ADD CONSTRAINT beone_log_pkey PRIMARY KEY (log_id);
 B   ALTER TABLE ONLY public.beone_log DROP CONSTRAINT beone_log_pkey;
       public            postgres    false    246         /           2606    17706 ,   beone_opname_detail beone_opname_detail_pkey 
   CONSTRAINT     x   ALTER TABLE ONLY public.beone_opname_detail
    ADD CONSTRAINT beone_opname_detail_pkey PRIMARY KEY (opname_detail_id);
 V   ALTER TABLE ONLY public.beone_opname_detail DROP CONSTRAINT beone_opname_detail_pkey;
       public            postgres    false    248         1           2606    17708 ,   beone_opname_header beone_opname_header_pkey 
   CONSTRAINT     x   ALTER TABLE ONLY public.beone_opname_header
    ADD CONSTRAINT beone_opname_header_pkey PRIMARY KEY (opname_header_id);
 V   ALTER TABLE ONLY public.beone_opname_header DROP CONSTRAINT beone_opname_header_pkey;
       public            postgres    false    250         3           2606    17710 $   beone_peleburan beone_peleburan_pkey 
   CONSTRAINT     l   ALTER TABLE ONLY public.beone_peleburan
    ADD CONSTRAINT beone_peleburan_pkey PRIMARY KEY (peleburan_id);
 N   ALTER TABLE ONLY public.beone_peleburan DROP CONSTRAINT beone_peleburan_pkey;
       public            postgres    false    252         5           2606    17712 >   beone_pemakaian_bahan_detail beone_pemakaian_bahan_detail_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.beone_pemakaian_bahan_detail
    ADD CONSTRAINT beone_pemakaian_bahan_detail_pkey PRIMARY KEY (pemakaian_detail_id);
 h   ALTER TABLE ONLY public.beone_pemakaian_bahan_detail DROP CONSTRAINT beone_pemakaian_bahan_detail_pkey;
       public            postgres    false    254         7           2606    17714 $   beone_pm_detail beone_pm_detail_pkey 
   CONSTRAINT     l   ALTER TABLE ONLY public.beone_pm_detail
    ADD CONSTRAINT beone_pm_detail_pkey PRIMARY KEY (pm_detail_id);
 N   ALTER TABLE ONLY public.beone_pm_detail DROP CONSTRAINT beone_pm_detail_pkey;
       public            postgres    false    256         9           2606    17716 $   beone_pm_header beone_pm_header_pkey 
   CONSTRAINT     l   ALTER TABLE ONLY public.beone_pm_header
    ADD CONSTRAINT beone_pm_header_pkey PRIMARY KEY (pm_header_id);
 N   ALTER TABLE ONLY public.beone_pm_header DROP CONSTRAINT beone_pm_header_pkey;
       public            postgres    false    258         ;           2606    17718 2   beone_po_import_detail beone_po_import_detail_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.beone_po_import_detail
    ADD CONSTRAINT beone_po_import_detail_pkey PRIMARY KEY (purchase_detail_id);
 \   ALTER TABLE ONLY public.beone_po_import_detail DROP CONSTRAINT beone_po_import_detail_pkey;
       public            postgres    false    260         =           2606    17720 2   beone_po_import_header beone_po_import_header_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.beone_po_import_header
    ADD CONSTRAINT beone_po_import_header_pkey PRIMARY KEY (purchase_header_id);
 \   ALTER TABLE ONLY public.beone_po_import_header DROP CONSTRAINT beone_po_import_header_pkey;
       public            postgres    false    262         ?           2606    17722 0   beone_produksi_detail beone_produksi_detail_pkey 
   CONSTRAINT     ~   ALTER TABLE ONLY public.beone_produksi_detail
    ADD CONSTRAINT beone_produksi_detail_pkey PRIMARY KEY (produksi_detail_id);
 Z   ALTER TABLE ONLY public.beone_produksi_detail DROP CONSTRAINT beone_produksi_detail_pkey;
       public            postgres    false    264         A           2606    17724 0   beone_produksi_header beone_produksi_header_pkey 
   CONSTRAINT     ~   ALTER TABLE ONLY public.beone_produksi_header
    ADD CONSTRAINT beone_produksi_header_pkey PRIMARY KEY (produksi_header_id);
 Z   ALTER TABLE ONLY public.beone_produksi_header DROP CONSTRAINT beone_produksi_header_pkey;
       public            postgres    false    266         C           2606    17726 0   beone_purchase_detail beone_purchase_detail_pkey 
   CONSTRAINT     ~   ALTER TABLE ONLY public.beone_purchase_detail
    ADD CONSTRAINT beone_purchase_detail_pkey PRIMARY KEY (purchase_detail_id);
 Z   ALTER TABLE ONLY public.beone_purchase_detail DROP CONSTRAINT beone_purchase_detail_pkey;
       public            postgres    false    268         E           2606    17728 0   beone_purchase_header beone_purchase_header_pkey 
   CONSTRAINT     ~   ALTER TABLE ONLY public.beone_purchase_header
    ADD CONSTRAINT beone_purchase_header_pkey PRIMARY KEY (purchase_header_id);
 Z   ALTER TABLE ONLY public.beone_purchase_header DROP CONSTRAINT beone_purchase_header_pkey;
       public            postgres    false    270         G           2606    17730 0   beone_received_import beone_received_import_pkey 
   CONSTRAINT     w   ALTER TABLE ONLY public.beone_received_import
    ADD CONSTRAINT beone_received_import_pkey PRIMARY KEY (received_id);
 Z   ALTER TABLE ONLY public.beone_received_import DROP CONSTRAINT beone_received_import_pkey;
       public            postgres    false    272         I           2606    17732    beone_role beone_role_pkey 
   CONSTRAINT     ]   ALTER TABLE ONLY public.beone_role
    ADD CONSTRAINT beone_role_pkey PRIMARY KEY (role_id);
 D   ALTER TABLE ONLY public.beone_role DROP CONSTRAINT beone_role_pkey;
       public            postgres    false    274         K           2606    17734 $   beone_role_user beone_role_user_pkey 
   CONSTRAINT     g   ALTER TABLE ONLY public.beone_role_user
    ADD CONSTRAINT beone_role_user_pkey PRIMARY KEY (role_id);
 N   ALTER TABLE ONLY public.beone_role_user DROP CONSTRAINT beone_role_user_pkey;
       public            postgres    false    276         M           2606    17736 *   beone_sales_detail beone_sales_detail_pkey 
   CONSTRAINT     u   ALTER TABLE ONLY public.beone_sales_detail
    ADD CONSTRAINT beone_sales_detail_pkey PRIMARY KEY (sales_detail_id);
 T   ALTER TABLE ONLY public.beone_sales_detail DROP CONSTRAINT beone_sales_detail_pkey;
       public            postgres    false    278         O           2606    17738 *   beone_sales_header beone_sales_header_pkey 
   CONSTRAINT     u   ALTER TABLE ONLY public.beone_sales_header
    ADD CONSTRAINT beone_sales_header_pkey PRIMARY KEY (sales_header_id);
 T   ALTER TABLE ONLY public.beone_sales_header DROP CONSTRAINT beone_sales_header_pkey;
       public            postgres    false    280         Q           2606    17740 (   beone_satuan_item beone_satuan_item_pkey 
   CONSTRAINT     m   ALTER TABLE ONLY public.beone_satuan_item
    ADD CONSTRAINT beone_satuan_item_pkey PRIMARY KEY (satuan_id);
 R   ALTER TABLE ONLY public.beone_satuan_item DROP CONSTRAINT beone_satuan_item_pkey;
       public            postgres    false    282         S           2606    17742 2   beone_subkon_in_detail beone_subkon_in_detail_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.beone_subkon_in_detail
    ADD CONSTRAINT beone_subkon_in_detail_pkey PRIMARY KEY (subkon_in_detail_id);
 \   ALTER TABLE ONLY public.beone_subkon_in_detail DROP CONSTRAINT beone_subkon_in_detail_pkey;
       public            postgres    false    284         U           2606    17744 2   beone_subkon_in_header beone_subkon_in_header_pkey 
   CONSTRAINT     z   ALTER TABLE ONLY public.beone_subkon_in_header
    ADD CONSTRAINT beone_subkon_in_header_pkey PRIMARY KEY (subkon_in_id);
 \   ALTER TABLE ONLY public.beone_subkon_in_header DROP CONSTRAINT beone_subkon_in_header_pkey;
       public            postgres    false    286         W           2606    17746 4   beone_subkon_out_detail beone_subkon_out_detail_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.beone_subkon_out_detail
    ADD CONSTRAINT beone_subkon_out_detail_pkey PRIMARY KEY (subkon_out_detail_id);
 ^   ALTER TABLE ONLY public.beone_subkon_out_detail DROP CONSTRAINT beone_subkon_out_detail_pkey;
       public            postgres    false    288         Y           2606    17748 4   beone_subkon_out_header beone_subkon_out_header_pkey 
   CONSTRAINT     }   ALTER TABLE ONLY public.beone_subkon_out_header
    ADD CONSTRAINT beone_subkon_out_header_pkey PRIMARY KEY (subkon_out_id);
 ^   ALTER TABLE ONLY public.beone_subkon_out_header DROP CONSTRAINT beone_subkon_out_header_pkey;
       public            postgres    false    290         [           2606    17750 "   beone_tipe_coa beone_tipe_coa_pkey 
   CONSTRAINT     i   ALTER TABLE ONLY public.beone_tipe_coa
    ADD CONSTRAINT beone_tipe_coa_pkey PRIMARY KEY (tipe_coa_id);
 L   ALTER TABLE ONLY public.beone_tipe_coa DROP CONSTRAINT beone_tipe_coa_pkey;
       public            postgres    false    292         _           2606    17752 <   beone_transfer_stock_detail beone_transfer_stock_detail_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.beone_transfer_stock_detail
    ADD CONSTRAINT beone_transfer_stock_detail_pkey PRIMARY KEY (transfer_stock_detail_id);
 f   ALTER TABLE ONLY public.beone_transfer_stock_detail DROP CONSTRAINT beone_transfer_stock_detail_pkey;
       public            postgres    false    297         ]           2606    17754 .   beone_transfer_stock beone_transfer_stock_pkey 
   CONSTRAINT     {   ALTER TABLE ONLY public.beone_transfer_stock
    ADD CONSTRAINT beone_transfer_stock_pkey PRIMARY KEY (transfer_stock_id);
 X   ALTER TABLE ONLY public.beone_transfer_stock DROP CONSTRAINT beone_transfer_stock_pkey;
       public            postgres    false    296         a           2606    17756    beone_user beone_user_pkey 
   CONSTRAINT     ]   ALTER TABLE ONLY public.beone_user
    ADD CONSTRAINT beone_user_pkey PRIMARY KEY (user_id);
 D   ALTER TABLE ONLY public.beone_user DROP CONSTRAINT beone_user_pkey;
       public            postgres    false    300         c           2606    17758 .   beone_voucher_detail beone_voucher_detail_pkey 
   CONSTRAINT     {   ALTER TABLE ONLY public.beone_voucher_detail
    ADD CONSTRAINT beone_voucher_detail_pkey PRIMARY KEY (voucher_detail_id);
 X   ALTER TABLE ONLY public.beone_voucher_detail DROP CONSTRAINT beone_voucher_detail_pkey;
       public            postgres    false    302         e           2606    17760 .   beone_voucher_header beone_voucher_header_pkey 
   CONSTRAINT     {   ALTER TABLE ONLY public.beone_voucher_header
    ADD CONSTRAINT beone_voucher_header_pkey PRIMARY KEY (voucher_header_id);
 X   ALTER TABLE ONLY public.beone_voucher_header DROP CONSTRAINT beone_voucher_header_pkey;
       public            postgres    false    304         f           2620    24589 2   beone_export_detail trg_create_beone_export_detail    TRIGGER     �   CREATE TRIGGER trg_create_beone_export_detail BEFORE INSERT OR UPDATE ON public.beone_export_detail FOR EACH ROW EXECUTE FUNCTION public.trg_create_beone_export_detail();
 K   DROP TRIGGER trg_create_beone_export_detail ON public.beone_export_detail;
       public          postgres    false    324    212         h           2620    24591 2   beone_export_header trg_create_beone_export_header    TRIGGER     �   CREATE TRIGGER trg_create_beone_export_header BEFORE INSERT OR UPDATE ON public.beone_export_header FOR EACH ROW EXECUTE FUNCTION public.trg_create_beone_export_header();
 K   DROP TRIGGER trg_create_beone_export_header ON public.beone_export_header;
       public          postgres    false    326    214         i           2620    24587 2   beone_import_detail trg_create_beone_import_detail    TRIGGER     �   CREATE TRIGGER trg_create_beone_import_detail BEFORE INSERT OR UPDATE ON public.beone_import_detail FOR EACH ROW EXECUTE FUNCTION public.trg_create_beone_import_detail();
 K   DROP TRIGGER trg_create_beone_import_detail ON public.beone_import_detail;
       public          postgres    false    226    319         k           2620    24592 2   beone_import_header trg_create_beone_import_header    TRIGGER     �   CREATE TRIGGER trg_create_beone_import_header BEFORE INSERT OR UPDATE ON public.beone_import_header FOR EACH ROW EXECUTE FUNCTION public.trg_create_beone_import_header();
 K   DROP TRIGGER trg_create_beone_import_header ON public.beone_import_header;
       public          postgres    false    228    327         l           2620    24593 @   beone_konversi_stok_detail trg_create_beone_konversi_stok_detail    TRIGGER     �   CREATE TRIGGER trg_create_beone_konversi_stok_detail BEFORE INSERT OR UPDATE ON public.beone_konversi_stok_detail FOR EACH ROW EXECUTE FUNCTION public.trg_create_beone_konversi_stok_detail();
 Y   DROP TRIGGER trg_create_beone_konversi_stok_detail ON public.beone_konversi_stok_detail;
       public          postgres    false    242    328         n           2620    24595 @   beone_konversi_stok_header trg_create_beone_konversi_stok_header    TRIGGER     �   CREATE TRIGGER trg_create_beone_konversi_stok_header BEFORE INSERT OR UPDATE ON public.beone_konversi_stok_header FOR EACH ROW EXECUTE FUNCTION public.trg_create_beone_konversi_stok_header();
 Y   DROP TRIGGER trg_create_beone_konversi_stok_header ON public.beone_konversi_stok_header;
       public          postgres    false    244    330         p           2620    24599 B   beone_transfer_stock_detail trg_create_beone_transfer_stock_detail    TRIGGER     �   CREATE TRIGGER trg_create_beone_transfer_stock_detail BEFORE INSERT OR UPDATE ON public.beone_transfer_stock_detail FOR EACH ROW EXECUTE FUNCTION public.trg_create_beone_transfer_stock_detail();
 [   DROP TRIGGER trg_create_beone_transfer_stock_detail ON public.beone_transfer_stock_detail;
       public          postgres    false    320    297         g           2620    24590 2   beone_export_detail trg_delete_beone_export_detail    TRIGGER     �   CREATE TRIGGER trg_delete_beone_export_detail BEFORE DELETE ON public.beone_export_detail FOR EACH ROW EXECUTE FUNCTION public.trg_delete_beone_export_detail();
 K   DROP TRIGGER trg_delete_beone_export_detail ON public.beone_export_detail;
       public          postgres    false    325    212         j           2620    24588 2   beone_import_detail trg_delete_beone_import_detail    TRIGGER     �   CREATE TRIGGER trg_delete_beone_import_detail BEFORE DELETE ON public.beone_import_detail FOR EACH ROW EXECUTE FUNCTION public.trg_delete_beone_import_detail();
 K   DROP TRIGGER trg_delete_beone_import_detail ON public.beone_import_detail;
       public          postgres    false    323    226         m           2620    24594 @   beone_konversi_stok_detail trg_delete_beone_konversi_stok_detail    TRIGGER     �   CREATE TRIGGER trg_delete_beone_konversi_stok_detail BEFORE DELETE ON public.beone_konversi_stok_detail FOR EACH ROW EXECUTE FUNCTION public.trg_delete_beone_konversi_stok_detail();
 Y   DROP TRIGGER trg_delete_beone_konversi_stok_detail ON public.beone_konversi_stok_detail;
       public          postgres    false    329    242         o           2620    24596 @   beone_konversi_stok_header trg_delete_beone_konversi_stok_header    TRIGGER     �   CREATE TRIGGER trg_delete_beone_konversi_stok_header BEFORE DELETE ON public.beone_konversi_stok_header FOR EACH ROW EXECUTE FUNCTION public.trg_delete_beone_konversi_stok_header();
 Y   DROP TRIGGER trg_delete_beone_konversi_stok_header ON public.beone_konversi_stok_header;
       public          postgres    false    306    244         q           2620    24600 B   beone_transfer_stock_detail trg_delete_beone_transfer_stock_detail    TRIGGER     �   CREATE TRIGGER trg_delete_beone_transfer_stock_detail BEFORE DELETE ON public.beone_transfer_stock_detail FOR EACH ROW EXECUTE FUNCTION public.trg_delete_beone_transfer_stock_detail();
 [   DROP TRIGGER trg_delete_beone_transfer_stock_detail ON public.beone_transfer_stock_detail;
       public          postgres    false    331    297                                                                                                                                                                                                                                                                                                                                                                                                                                   3312.dat                                                                                            0000600 0004000 0002000 00000000005 13607356547 0014255 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        \.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           3314.dat                                                                                            0000600 0004000 0002000 00000137505 13607356547 0014277 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        1	CASH	10.01.01	1	0	220746014.42	0	0	D	1
2	RESONA  03050342 - 068   ( US $)	11.01.01	1	0	869788528	0	0	D	1
3	RESONA  03050300 - 063   ( US $)	11.01.02	1	0	0	0	0	D	1
4	BCA  - 769.000.9549   (IDR)	11.01.03	1	0	109268504.1	0	0	D	1
5	UFJ  000032 - 002   (US $)	11.01.04	1	0	0	0	0	D	1
6	UFJ  000032 - 003   (JP ¥)	11.01.05	1	0	0	0	0	D	1
7	UFJ  000035   (JP ¥)	11.01.06	1	0	0	0	0	D	1
8	RESONA  03000438005   (IDR)	11.01.07	1	0	307688826.66	0	0	D	1
9	RESONA  03050578010   (JP ¥)	11.01.08	1	0	2710924463	0	0	D	1
10	DANAMON   29452752   (IDR)	11.01.09	1	0	0	0	0	D	1
11	DANAMON   00299-70-530   (IDR)	11.01.10	1	0	0	0	0	D	1
12	BNI 33 20458750 (IDR)	11.01.11	1	0	63541829	0	0	D	1
13	BNI 033 2045875 (IDR)	11.01.12	1	0	0	0	0	D	1
14	BOT 3667-IDR-CUA-7100014347 	11.01.13	1	0	0	0	0	D	1
15	BOT 3667-JPY-CUA-7200236597	11.01.14	1	0	0	0	0	D	1
16	BOT 3667-USD-CUA-7300236609 (USD)	11.01.15	1	0	37016599	0	0	D	1
17	RESONA  03760001065   ( US$)	11.01.16	1	0	263789790	0	0	D	1
18	RESONA  03760006016   ( JPY)	11.01.17	1	0	23392851473	0	0	D	1
19	RESONA  03730001001   ( Rp)	11.01.18	1	0	0	0	0	D	1
20	TIME DEPOSIT - TEMPORARY	12.01.01	1	0	43917740000	0	0	D	19
21	TIME DEPOSIT - DANAMON  (IDR)	12.01.02	1	0	0	0	0	D	19
22	TIME DEPOSIT - BANK MEGA (IDR)	12.01.03	1	0	0	0	0	D	19
23	TIME DEPOSIT - BANK MEGA ($)	12.01.04	1	0	0	0	0	D	19
24	TIME DEPOSIT - RESONA (Employee Benefit)	12.01.05	1	0	16292036820.88	0	0	D	19
25	TIME DEPOSIT - RESONA	12.01.06	1	0	0	0	0	D	19
26	INVESTA - DANAMON  (IDR)	12.02.01	1	0	0	0	0	D	19
27	DEPOSIT - ON CALL	12.03.01	1	0	0	0	0	D	19
28	ACCOUNT RECEIVABLE - EXPORT	13.01.00	1	0	17439922929	0	0	D	11
29	ACCOUNT RECEIVABLE - LOCAL	13.02.00	1	0	1923323627.3	0	0	D	11
30		13.01.98	1	0	0	0	0	D	11
31	ACCOUNT RECEIVABLE - OTHER	13.01.99	1	0	0	0	0	D	11
32	ADVANCE PAYMENT	15.01.01	1	0	950910545.2	0	0	D	11
33	MATERIAL - MAIN MATERIAL	16.01.01	1	0	7690221729.5	0	0	D	6
34	MATERIAL - SUB MATERIAL	16.01.02	1	0	595207356	0	0	D	6
35	MATERIAL - PACKING MATERIAL	16.01.03	1	0	142453534.93	0	0	D	6
36	MATERIAL - PARTS MATERIAL	16.01.04	1	0	1443682659.34	0	0	D	6
37	SUPPLEMENT	16.02.01	1	0	73709247.74	0	0	D	6
38	SPARE PART - MACHINE	16.03.01	1	0	0	0	0	D	6
39	SPARE PART - CUTTER	16.03.02	1	0	0	0	0	D	6
40	TOOL	16.04.01	1	0	0	0	0	D	6
41	STATIONARY	16.99.01	1	0	0	0	0	D	6
42	MEDICINE	16.99.02	1	0	0	0	0	D	6
43	WORK IN PROCESS	17.01.01	1	0	3108111538.84	0	0	D	6
44	WORK IN PROCESS - M PROJECT	17.01.02	1	0	686209821.48	0	0	D	6
45	FINISH GOOD	18.01.01	1	0	0	0	0	D	6
46	PREPAID - INCOME / EXPENSE	19.01.01	1	0	1172187931.6	0	0	D	12
47	PREPAID - INSURANCE	19.01.02	1	0	24509715	0	0	D	12
48	PREPAID - PPH 25	19.01.03	1	0	605682638	0	0	D	12
49	PREPAID - RENT	19.01.04	1	0	0	0	0	D	12
50	PREPAID - NNA	19.01.05	1	0	0	0	0	D	12
51	PREPAID - DPKK	19.01.06	1	0	22659000	0	0	D	12
52	PREPAID - MEDICAL CLAIM	19.01.07	1	0	0	0	0	D	12
53	PREPAID - JAPANESE EXP	19.01.08	1	0	0	0	0	D	12
54	PREPAID - ASSEMBLY IN JAPAN/PPH 23 SUB KONTRAK	19.01.09	1	0	4215810	0	0	D	12
55	PREPAID - CROSS CODE	19.01.99	1	0	0	0	0	D	12
56	CAPITAL INVESTMENT	19.99.99	1	0	2970000000	0	0	D	7
57	LAND	20.01.01	1	0	1110286370	0	0	D	7
58	BUILDING - FACTORY	21.01.01	1	0	5895816000	0	0	D	7
59	BUILDING - OFFICE	21.01.02	1	0	0	0	0	D	7
60	BUILDING - FINNA	21.01.03	1	0	0	0	0	D	7
61	BUILDING - PURI MATAHARI	21.01.04	1	0	0	0	0	D	7
62	ACC DEP BUILDING - FACTORY	21.01.96	1	0	0	0	5642776244	K	0
63	ACC DEP BUILDING - OFFICE	21.01.97	1	0	0	0	0	K	0
64	ACC DEP BUILDING - FINNA	21.01.98	1	0	0	0	0	K	0
65	ACC DEP BUILDING - PURI MATAHARI	21.01.99	1	0	0	0	0	K	0
66	MACHINE	22.01.01	1	0	56307941774.91	0	0	D	7
67	ACC. DEP. MACHINE	22.01.99	1	0	0	0	33061602191.4	D	0
68	VEHICLE	23.01.01	1	0	2834369160.5	0	0	D	7
69	HEAVY EQUIPMENT	23.01.02	1	0	0	0	0	D	7
70	ACC DEP VEHICLE	23.01.98	1	0	0	0	2291792653	K	0
71	ACC DEP HEAVY EQUIPMENT	23.01.99	1	0	0	0	0	K	0
72	OFFICE EQUIPMENT	24.01.01	1	0	1160269699.8	0	0	D	7
73	FINNA EQUIPMENT	24.01.02	1	0	0	0	0	D	7
74	FACTORY EQUIPMENT	24.01.03	1	0	0	0	0	D	7
75	ACC DEP FACTORY EQUIPMENT	24.01.97	1	0	0	0	1055616855.8	K	0
76	ACC DEP OFFICE EQUIPMENT	24.01.98	1	0	0	0	0	K	0
77	ACC DEP FINNA EQUIPMENT	24.01.99	1	0	0	0	0	K	0
78	INSTALLATION	25.01.01	1	0	0	0	0	D	7
79	ACC. DEP. INSTALLATION	25.01.99	1	0	0	0	0	K	0
80	CONSTRUCTION IN PROGRESS - ASSETS	29.01.01	1	0	0	0	0	D	7
81	BANK LOAN - DAIWA (LONG TERM)	30.01.01	2	0	0	0	0	K	16
82	BANK LOAN - DAIWA (SHORT TERM)	30.01.02	2	0	0	0	0	K	16
83	BANK LOAN - RESONA / PLAFOND	30.01.03	2	0	0	0	0	K	16
84	ACCOUNT PAYABLE - IMPORT	31.01.00	2	0	0	0	875145057.1	K	8
85	ACCOUNT PAYABLE - LOCAL	31.02.00	2	0	0	0	1888015969	K	8
86	UNEARNED REVENUE - EXPORT	32.01.01	2	0	0	0	0	K	8
87	UNEARNED REVENUE - LOCAL	32.01.02	2	0	0	0	0	K	8
88	ACCRUED EXPENSE - MARKETING	33.01.01	2	0	0	0	0	K	10
89	ACCRUED EXPENSE - INSURANCE	33.01.02	2	0	0	0	0	K	10
90	ACCRUED EXPENSE - ISPM	33.01.03	2	0	0	0	0	K	10
91	ACCRUED EXPENSE - OTHER	33.01.99	2	0	0	0	0	K	10
92	TAX PAYABLE - PPH - 21	34.01.01	2	0	0	0	732431619	K	9
93	TAX PAYABLE - PPH - 25	34.01.02	2	0	0	0	55930900	K	9
94	TAX PAYABLE - PPH - 23	34.01.03	2	0	0	0	7518941	K	9
95	TAX PAYABLE - PPN KELUARAN	34.01.03	2	0	0	0	593982606.9	K	9
96	RESONA - LEASE	35.01.01	2	0	0	0	6382797563	K	15
97		36.01.01	2	0	0	0	2441283715	K	0
98	CAPITAL	40.01.01	3	0	0	0	9144000000	K	3
99	RETAINED EARNING	41.01.01	3	0	0	0	130988490171	K	4
100	PROFIT /LOSS (RETAINED)	42.01.01	3	0	0	0	8261465873	K	4
101	PROFIT /LOSS (CURRENT)	42.01.02	3	0	0	0	-1335187422	K	4
102	DEVIDEN	42.01.03	3	0	0	0	-7740579000	K	4
103	RESERVE fr EXC. RATE	43.01.01	3	0	0	0	0	K	4
104	SALES - EXPORT	50.01.01	4	0	0	0	0	K	0
105	SALES - LOCAL	50.01.02	4	0	0	0	0	K	0
106	SALES - OTHER	50.01.99	4	0	0	0	0	K	0
107	SALES DISCOUNT - DAIKEN	51.01.01	4	0	0	0	0	D	0
108	SALES DISCOUNT - FUKAI	51.01.11	4	0	0	0	0	D	0
109	SALES DISCOUNT - NASLUCK	51.01.21	4	0	0	0	0	D	0
110	SALES DISCOUNT - TOPPAN	51.01.31	4	0	0	0	0	D	0
111	SALES DISCOUNT - TENMA	51.01.41	4	0	0	0	0	D	0
112	SALES DISCOUNT - 	51.01.51	4	0	0	0	0	D	0
113	SALES DISCOUNT - 	51.01.81	4	0	0	0	0	D	0
114	SALES DISCOUNT - 	51.01.82	4	0	0	0	0	D	0
115	SALES DISCOUNT - LOCAL	51.01.99	4	0	0	0	0	D	0
116	SALES RETURN - EXPORT	52.01.01	4	0	0	0	0	D	0
117	SALES RETURN - LOCAL	52.01.02	4	0	0	0	0	D	0
118	C O G S	53.01.01	5	0	0	0	0	D	20
119	BANK INTEREST	59.01.01	7	0	0	0	0	K	0
120	EXC RATE	59.01.08	7	0	0	0	0	K	0
121	OTHER	59.01.09	7	0	0	0	0	K	0
122	MARKETING JP - SALARY	60.01.01	6	0	0	0	0	D	0
123	MARKETING JP - BONUS 	60.01.02	6	0	0	0	0	D	0
124	MARKETING JP - PPH 21	60.01.06	6	0	0	0	0	D	0
125	MARKETING JP - MEDICAL 	60.01.07	6	0	0	0	0	D	0
126	MARKETING JP - OVERSEAS BUSINESS TRIP	60.01.16	6	0	0	0	0	D	0
127	MARKETING JP - TRAVELL - OUT OF TOWN	60.01.17	6	0	0	0	0	D	0
128	MARKETING JP - TRAVELL & ACCO	60.01.18	6	0	0	0	0	D	0
129	MARKETING JP - LICENSES	60.01.24	6	0	0	0	0	D	0
130	MARKETING JP - SALES COMMISION	60.01.36	6	0	0	0	0	D	0
131	MARKETING JP - ENTERTAINMNET	60.01.54	6	0	0	0	0	D	0
132	MARKETING JP - INSURANCE	60.01.70	6	0	0	0	0	D	0
133	MARKETING ADM - LOCAL SALARY	60.02.01	6	0	0	0	0	D	0
134	MARKETING ADM - LOCAL BONUS	60.02.02	6	0	0	0	0	D	0
135	MARKETING ADM - OVERTIME	60.02.05	6	0	0	0	0	D	0
136	MARKETING ADM - PPH 21	60.02.06	6	0	0	0	0	D	0
137	MARKETING ADM - MEDICAL	60.02.07	6	0	0	0	0	D	0
138	MARKETING ADM - ASTEK & BP	60.02.08	6	0	0	0	0	D	0
139	MARKETING ADM - MEAL	60.02.09	6	0	0	0	0	D	0
140	MARKETING ADM - TRAVELLING OUT OF TOWN	60.02.17	6	0	0	0	0	D	0
141	MARKETING ADM - EXPENSE IN JAPAN	60.02.29	6	0	0	0	0	D	0
142	EXIM CHARGE - EXPORT FEE	60.02.30	6	0	0	0	0	D	0
143	EXIM CHARGE - IMPORT FEE	60.02.31	6	0	0	0	0	D	0
144	EXIM CHARGE - CARGO INSURANCE	60.02.32	6	0	0	0	0	D	0
145	EXIM CHARGE - SAMPLE FEE	60.02.33	6	0	0	0	0	D	0
146	EXIM CHARGE - CUSTOM EXP	60.02.34	6	0	0	0	0	D	0
147	EXIM CHARGE - M PROJECT EXP	60.02.35	6	0	0	0	0	D	0
148	EXIM CHARGE - SALES COMM	60.02.36	6	0	0	0	0	D	0
149	EXIM CHARGE - LOCAL EXPENSES	60.02.37	6	0	0	0	0	D	0
150	MARKETING ADM  - SALES PROMOTION	60.02.38	6	0	0	0	0	D	0
151	MARKETING ADM  - ELECTRICITY	60.02.39	6	0	0	0	0	D	0
152	MARKETING ADM - WATER & MAINT 	60.02.41	6	0	0	0	0	D	0
153	MARKETING ADM - STATIONARY	60.02.46	6	0	0	0	0	D	0
154	MARKETING ADM  - BUILDING DEP	60.02.90	6	0	0	0	0	D	0
155	MARKETING EXPENSE	60.02.99	6	0	0	0	0	D	0
156	FACTORY EXP JP - SALARY	70.01.01	6	0	0	0	0	D	0
157	FACTORY EXP JP - BONUS	70.01.02	6	0	0	0	0	D	0
158	FACTORY EXP JP - PPH 21	70.01.06	6	0	0	0	0	D	0
159	FACTORY EXP JP - MEDICAL	70.01.07	6	0	0	0	0	D	0
160	FACTORY EXP JP - EXPATRIATE SERVICE	70.01.14	6	0	0	0	0	D	0
161	FACTORY EXP JP - INSPECTION FEE	70.01.15	6	0	0	0	0	D	0
162	FACTORY EXP JP - OVERSEAS BUSINESS TRIP	70.01.16	6	0	0	0	0	D	0
163	FACTORY EXP JP - TRAVELLING - OUT OF TOWN	70.01.17	6	0	0	0	0	D	0
164	FACTORY EXP JP - TRAVELLING & ACCOMODATION	70.01.18	6	0	0	0	0	D	0
165	FACTORY EXP JP - JAPANESE LICENSES	70.01.24	6	0	0	0	0	D	0
166	FACTORY EXP JP - ENTERTAINMENT	70.01.54	6	0	0	0	0	D	0
167	FACTORY EXP JP - INSURANCE	70.01.70	6	0	0	0	0	D	0
168	FACTORY EXP JP	70.01.99	6	0	0	0	0	D	0
169	PRODUCTION - LOCAL SALARY	70.05.01	5	0	0	0	0	D	20
170	PRODUCTION - LOCAL BONUS	70.05.02	5	0	0	0	0	D	20
171	PRODUCTION - PKWT SALARY	70.05.03	5	0	0	0	0	D	20
172	PRODUCTION - PKWT BONUS	70.05.04	5	0	0	0	0	D	20
173	PRODUCTION - OVERTIME	70.05.05	5	0	0	0	0	D	20
174	PRODUCTION - PPH 21	70.05.06	5	0	0	0	0	D	20
175	PRODUCTION - MEDICAL	70.05.07	5	0	0	0	0	D	20
176	PRODUCTION - ASTEK & BP	70.05.08	5	0	0	0	0	D	20
177	PRODUCTION - MEAL	70.05.09	5	0	0	0	0	D	20
178	PROD. DAIKEN CLOUCK DOOR - LOCAL SALARY	70.10.01	5	0	0	0	0	D	20
179	PROD. DAIKEN CLOUCK DOOR - LOCAL BONUS	70.10.02	5	0	0	0	0	D	20
180	PROD. DAIKEN CLOUCK DOOR - PKWT SALARY	70.10.03	5	0	0	0	0	D	20
181	PROD. DAIKEN CLOUCK DOOR - PKWT BONUS	70.10.04	5	0	0	0	0	D	20
182	PROD. DAIKEN CLOUCK DOOR - OVERTIME	70.10.05	5	0	0	0	0	D	20
183	PROD. DAIKEN CLOUCK DOOR - PPH 21	70.10.06	5	0	0	0	0	D	20
184	PROD. DAIKEN CLOUCK DOOR - MEDICAL	70.10.07	5	0	0	0	0	D	20
185	PROD. DAIKEN CLOUCK DOOR - ASTEK & BP	70.10.08	5	0	0	0	0	D	20
186	PROD. DAIKEN CLOUCK DOOR - MEAL	70.10.09	5	0	0	0	0	D	20
187	PROD. DAIKEN CLOUCK DOOR - ELECTRICITY	70.10.39	5	0	0	0	0	D	20
188	PROD. DAIKEN CLOUCK DOOR - WATER & MAINT	70.10.41	5	0	0	0	0	D	20
189	PROD. DAIKEN CLOUCK DOOR - STATIONARY	70.10.46	5	0	0	0	0	D	20
190	PROD. DAIKEN CLOUCK DOOR - BUILDING INSURANCE	70.10.71	5	0	0	0	0	D	20
191	PROD. DAIKEN CLOUCK DOOR - BUILDING DEP	70.10.90	5	0	0	0	0	D	20
192	PROD. DAIKEN CLOUCK DOOR - MACHINE DEP	70.10.92	5	0	0	0	0	D	20
193	PROD. DAIKEN CLOUCK DOOR - OTHER EXP	70.10.99	5	0	0	0	0	D	20
194	PROD. DAIKEN MIRROW DOOR - LOCAL SALARY	70.11.01	5	0	0	0	0	D	20
195	PROD. DAIKEN MIRROW DOOR - LOCAL BONUS	70.11.02	5	0	0	0	0	D	20
196	PROD. DAIKEN MIRROW DOOR - PKWT SALARY	70.11.03	5	0	0	0	0	D	20
197	PROD. DAIKEN MIRROW DOOR - PKWT BONUS	70.11.04	5	0	0	0	0	D	20
198	PROD. DAIKEN MIRROW DOOR - OVERTIME	70.11.05	5	0	0	0	0	D	20
199	PROD. DAIKEN MIRROW DOOR - PPH 21	70.11.06	5	0	0	0	0	D	20
200	PROD. DAIKEN MIRROW DOOR - MEDICAL	70.11.07	5	0	0	0	0	D	20
201	PROD. DAIKEN MIRROW DOOR - ASTEK & BP	70.11.08	5	0	0	0	0	D	20
202	PROD. DAIKEN MIRROW DOOR - MEAL	70.11.09	5	0	0	0	0	D	20
203	PROD. DAIKEN MIRROW DOOR - ELECTRICITY	70.11.39	5	0	0	0	0	D	20
204	PROD. DAIKEN MIRROW DOOR - WATER & MAINT	70.11.41	5	0	0	0	0	D	20
205	PROD. DAIKEN MIRROW DOOR - STATIONARY	70.11.46	5	0	0	0	0	D	20
206	PROD. DAIKEN MIRROW DOOR - BUILDING INSURANCE	70.11.71	5	0	0	0	0	D	20
207	PROD. DAIKEN MIRROW DOOR - BUILDING DEP	70.11.90	5	0	0	0	0	D	20
208	PROD. DAIKEN MIRROW DOOR - MACHINE DEP	70.11.92	5	0	0	0	0	D	20
209	PROD. DAIKEN MIRROW DOOR - OTHER EXP	70.11.99	5	0	0	0	0	D	20
210	PROD. DAIKEN DAIWA FLOAT - LOCAL SALARY	70.12.01	5	0	0	0	0	D	20
211	PROD. DAIKEN DAIWA FLOAT - LOCAL BONUS	70.12.02	5	0	0	0	0	D	20
212	PROD. DAIKEN DAIWA FLOAT - PKWT SALARY	70.12.03	5	0	0	0	0	D	20
213	PROD. DAIKEN DAIWA FLOAT - PKWT BONUS	70.12.04	5	0	0	0	0	D	20
214	PROD. DAIKEN DAIWA FLOAT - OVERTIME	70.12.05	5	0	0	0	0	D	20
215	PROD. DAIKEN DAIWA FLOAT - PPH 21	70.12.06	5	0	0	0	0	D	20
216	PROD. DAIKEN DAIWA FLOAT - MEDICAL	70.12.07	5	0	0	0	0	D	20
217	PROD. DAIKEN DAIWA FLOAT - ASTEK & BP	70.12.08	5	0	0	0	0	D	20
218	PROD. DAIKEN DAIWA FLOAT - MEAL	70.12.09	5	0	0	0	0	D	20
219	PROD. DAIKEN DAIWA FLOAT - ELECTRICITY	70.12.39	5	0	0	0	0	D	20
220	PROD. DAIKEN DAIWA FLOAT - WATER & MAINT	70.12.41	5	0	0	0	0	D	20
221	PROD. DAIKEN DAIWA FLOAT - STATIONARY	70.12.46	5	0	0	0	0	D	20
222	PROD. DAIKEN DAIWA FLOAT - BUILDING INSURANCE	70.12.71	5	0	0	0	0	D	20
223	PROD. DAIKEN DAIWA FLOAT - BUILDING DEP	70.12.90	5	0	0	0	0	D	20
224	PROD. DAIKEN DAIWA FLOAT - MACHINE DEP	70.12.92	5	0	0	0	0	D	20
225	PROD. DAIKEN DAIWA FLOAT - OTHER EXP	70.12.99	5	0	0	0	0	D	20
226	PROD. DAIKEN KANTANA - LOCAL SALARY	70.13.01	5	0	0	0	0	D	20
227	PROD. DAIKEN KANTANA - LOCAL BONUS	70.13.02	5	0	0	0	0	D	20
228	PROD. DAIKEN KANTANA - PKWT SALARY	70.13.03	5	0	0	0	0	D	20
229	PROD. DAIKEN KANTANA - PKWT BONUS	70.13.04	5	0	0	0	0	D	20
230	PROD. DAIKEN KANTANA - OVERTIME	70.13.05	5	0	0	0	0	D	20
231	PROD. DAIKEN KANTANA - PPH 21	70.13.06	5	0	0	0	0	D	20
232	PROD. DAIKEN KANTANA - MEDICAL	70.13.07	5	0	0	0	0	D	20
233	PROD. DAIKEN KANTANA - ASTEK & BP	70.13.08	5	0	0	0	0	D	20
234	PROD. DAIKEN KANTANA - MEAL	70.13.09	5	0	0	0	0	D	20
235	PROD. DAIKEN KANTANA - ELECTRICITY	70.13.39	5	0	0	0	0	D	20
236	PROD. DAIKEN KANTANA - WATER & MAINT	70.13.41	5	0	0	0	0	D	20
237	PROD. DAIKEN KANTANA - STATIONARY	70.13.46	5	0	0	0	0	D	20
238	PROD. DAIKEN KANTANA - BUILDING INSURANCE	70.13.71	5	0	0	0	0	D	20
239	PROD. DAIKEN KANTANA - BUILDING DEP	70.13.90	5	0	0	0	0	D	20
240	PROD. DAIKEN KANTANA - MACHINE DEP	70.13.92	5	0	0	0	0	D	20
241	PROD. DAIKEN KANTANA - EXPENSE	70.13.99	5	0	0	0	0	D	20
242	PROD. FUKAI TV RACK - LOCAL SALARY	70.14.01	5	0	0	0	0	D	20
243	PROD. FUKAI TV RACK - LOCAL BONUS	70.14.02	5	0	0	0	0	D	20
244	PROD. FUKAI TV RACK - PKWT SALARY	70.14.03	5	0	0	0	0	D	20
245	PROD. FUKAI TV RACK - PKWT BONUS	70.14.04	5	0	0	0	0	D	20
246	PROD. FUKAI TV RACK - OVERTIME	70.14.05	5	0	0	0	0	D	20
247	PROD. FUKAI TV RACK - PPH 21	70.14.06	5	0	0	0	0	D	20
248	PROD. FUKAI TV RACK - MEDICAL	70.14.07	5	0	0	0	0	D	20
249	PROD. FUKAI TV RACK - ASTEK & BP	70.14.08	5	0	0	0	0	D	20
250	PROD. FUKAI TV RACK - MEAL	70.14.09	5	0	0	0	0	D	20
251	PROD. FUKAI TV RACK - ELECTRICITY	70.14.39	5	0	0	0	0	D	20
252	PROD. FUKAI TV RACK - WATER & MAINT	70.14.41	5	0	0	0	0	D	20
253	PROD. FUKAI TV RACK - STATIONARY	70.14.46	5	0	0	0	0	D	20
254	PROD. FUKAI TV RACK - BUILDING INSURANCE	70.14.71	5	0	0	0	0	D	20
255	PROD. FUKAI TV RACK - BUILDING DEP	70.14.90	5	0	0	0	0	D	20
256	PROD. FUKAI TV RACK - MACHINE DEP	70.14.92	5	0	0	0	0	D	20
257	PROD. FUKAI TV RACK - EXPENSE	70.14.99	5	0	0	0	0	D	20
258	PROD. FUKAI KITCHEN - LOCAL SALARY	70.15.01	5	0	0	0	0	D	20
259	PROD. FUKAI KITCHEN - LOCAL BONUS	70.15.02	5	0	0	0	0	D	20
260	PROD. FUKAI KITCHEN - PKWT SALARY	70.15.03	5	0	0	0	0	D	20
261	PROD. FUKAI KITCHEN - PKWT BONUS	70.15.04	5	0	0	0	0	D	20
262	PROD. FUKAI KITCHEN - OVERTIME	70.15.05	5	0	0	0	0	D	20
263	PROD. FUKAI KITCHEN - PPH 21	70.15.06	5	0	0	0	0	D	20
264	PROD. FUKAI KITCHEN - MEDICAL	70.15.07	5	0	0	0	0	D	20
265	PROD. FUKAI KITCHEN - ASTEK & BP	70.15.08	5	0	0	0	0	D	20
266	PROD. FUKAI KITCHEN - MEAL	70.15.09	5	0	0	0	0	D	20
267	PROD. FUKAI KITCHEN - ELECTRICITY	70.15.39	5	0	0	0	0	D	20
268	PROD. FUKAI KITCHEN - WATER & MAINT	70.15.41	5	0	0	0	0	D	20
269	PROD. FUKAI KITCHEN - STATIONARY	70.15.46	5	0	0	0	0	D	20
270	PROD. FUKAI KITCHEN - BUILDING INSURANCE	70.15.71	5	0	0	0	0	D	20
271	PROD. FUKAI KITCHEN - BUILDING DEP	70.15.90	5	0	0	0	0	D	20
272	PROD. FUKAI KITCHEN - MACHINE DEP	70.15.92	5	0	0	0	0	D	20
273	PROD. FUKAI KITCHEN - EXPENSE	70.15.99	5	0	0	0	0	D	20
274	PROD. SEIKO SHOES DOOR/DAITO - LOCAL SALARY	70.16.01	5	0	0	0	0	D	20
275	PROD. SEIKO SHOES DOOR/DAITO - LOCAL BONUS	70.16.02	5	0	0	0	0	D	20
276	PROD. SEIKO SHOES DOOR/DAITO - PKWT SALARY	70.16.03	5	0	0	0	0	D	20
277	PROD. SEIKO SHOES DOOR/DAITO - PKWT BONUS	70.16.04	5	0	0	0	0	D	20
278	PROD. SEIKO SHOES DOOR/DAITO - OVERTIME	70.16.05	5	0	0	0	0	D	20
279	PROD. SEIKO SHOES DOOR/DAITO - PPH 21	70.16.06	5	0	0	0	0	D	20
280	PROD. SEIKO SHOES DOOR/DAITO - MEDICAL	70.16.07	5	0	0	0	0	D	20
281	PROD. SEIKO SHOES DOOR/DAITO - ASTEK & BP	70.16.08	5	0	0	0	0	D	20
282	PROD. SEIKO SHOES DOOR/DAITO - MEAL	70.16.09	5	0	0	0	0	D	20
283	PROD. SEIKO SHOES DOOR/DAITO - ELECTRICITY	70.16.39	5	0	0	0	0	D	20
284	PROD. SEIKO SHOES DOOR/DAITO - WATER & MAINT	70.16.41	5	0	0	0	0	D	20
285	PROD. SEIKO SHOES DOOR/DAITO - STATIONARY	70.16.46	5	0	0	0	0	D	20
286	PROD. SEIKO SHOES DOOR/DAITO - BUILDING INSURANCE	70.16.71	5	0	0	0	0	D	20
287	PROD. SEIKO SHOES DOOR/DAITO - BUILDING DEP	70.16.90	5	0	0	0	0	D	20
288	PROD. SEIKO SHOES DOOR/DAITO - MACHINE DEP	70.16.92	5	0	0	0	0	D	20
289	PROD. SEIKO SHOES DOOR/DAITO - EXPENSE	70.16.99	5	0	0	0	0	D	20
290	PROD. OREDO - LOCAL SALARY	70.17.01	5	0	0	0	0	D	20
291	PROD. OREDO - LOCAL BONUS	70.17.02	5	0	0	0	0	D	20
292	PROD. OREDO - PKWT SALARY	70.17.03	5	0	0	0	0	D	20
293	PROD. OREDO - PKWT BONUS	70.17.04	5	0	0	0	0	D	20
294	PROD. OREDO - OVERTIME	70.17.05	5	0	0	0	0	D	20
295	PROD. OREDO - PPH 21	70.17.06	5	0	0	0	0	D	20
296	PROD. OREDO - MEDICAL	70.17.07	5	0	0	0	0	D	20
297	PROD. OREDO - ASTEK & BP	70.17.08	5	0	0	0	0	D	20
298	PROD. OREDO - MEAL	70.17.09	5	0	0	0	0	D	20
299	PROD. OREDO - ELECTRICITY	70.17.39	5	0	0	0	0	D	20
300	PROD. OREDO - WATER & MAINT	70.17.41	5	0	0	0	0	D	20
301	PROD. OREDO - STATIONARY	70.17.46	5	0	0	0	0	D	20
302	PROD. OREDO - BUILDING INSURANCE	70.17.71	5	0	0	0	0	D	20
303	PROD. OREDO - BUILDING DEP	70.17.90	5	0	0	0	0	D	20
304	PROD. OREDO - MACHINE DEP	70.17.92	5	0	0	0	0	D	20
305	PROD. OREDO - EXPENSE	70.17.99	5	0	0	0	0	D	20
306	PROD. TORI SETSU SYUNO - LOCAL SALARY	70.18.01	5	0	0	0	0	D	20
307	PROD. TORI SETSU SYUNO - LOCAL BONUS	70.18.02	5	0	0	0	0	D	20
308	PROD. TORI SETSU SYUNO - PKWT SALARY	70.18.03	5	0	0	0	0	D	20
309	PROD. TORI SETSU SYUNO - PKWT BONUS	70.18.04	5	0	0	0	0	D	20
310	PROD. TORI SETSU SYUNO - OVERTIME	70.18.05	5	0	0	0	0	D	20
311	PROD. TORI SETSU SYUNO - PPH 21	70.18.06	5	0	0	0	0	D	20
312	PROD. TORI SETSU SYUNO - MEDICAL	70.18.07	5	0	0	0	0	D	20
313	PROD. TORI SETSU SYUNO - ASTEK & BP	70.18.08	5	0	0	0	0	D	20
314	PROD. TORI SETSU SYUNO - MEAL	70.18.09	5	0	0	0	0	D	20
315	PROD. TORI SETSU SYUNO - ELECTRICITY	70.18.39	5	0	0	0	0	D	20
316	PROD. TORI SETSU SYUNO - WATER & MAINT	70.18.41	5	0	0	0	0	D	20
317	PROD. TORI SETSU SYUNO - STATIONARY	70.18.46	5	0	0	0	0	D	20
318	PROD. TORI SETSU SYUNO - BUILDING INSURANCE	70.18.71	5	0	0	0	0	D	20
319	PROD. TORI SETSU SYUNO - BUILDING DEP	70.18.90	5	0	0	0	0	D	20
320	PROD. TORI SETSU SYUNO - MACHINE DEP	70.18.92	5	0	0	0	0	D	20
321	PROD. TORI SETSU SYUNO - EXPENSE	70.18.99	5	0	0	0	0	D	20
322	PROD. MAGUCHI / TOIRE SYUNO - LOCAL SALARY	70.19.01	5	0	0	0	0	D	20
323	PROD. MAGUCHI / TOIRE SYUNO - LOCAL BONUS	70.19.02	5	0	0	0	0	D	20
324	PROD. MAGUCHI / TOIRE SYUNO - PKWT SALARY	70.19.03	5	0	0	0	0	D	20
325	PROD. MAGUCHI / TOIRE SYUNO - PKWT BONUS	70.19.04	5	0	0	0	0	D	20
326	PROD. MAGUCHI / TOIRE SYUNO - OVERTIME	70.19.05	5	0	0	0	0	D	20
327	PROD. MAGUCHI / TOIRE SYUNO - PPH 21	70.19.06	5	0	0	0	0	D	20
328	PROD. MAGUCHI / TOIRE SYUNO - MEDICAL	70.19.07	5	0	0	0	0	D	20
329	PROD. MAGUCHI / TOIRE SYUNO - ASTEK & BP	70.19.08	5	0	0	0	0	D	20
330	PROD. MAGUCHI / TOIRE SYUNO - MEAL	70.19.09	5	0	0	0	0	D	20
331	PROD. MAGUCHI / TOIRE SYUNO - ELECTRICITY	70.19.39	5	0	0	0	0	D	20
332	PROD. MAGUCHI / TOIRE SYUNO - WATER & MAINT	70.19.41	5	0	0	0	0	D	20
333	PROD. MAGUCHI / TOIRE SYUNO - STATIONARY	70.19.46	5	0	0	0	0	D	20
334	PROD. MAGUCHI / TOIRE SYUNO - BUILDING INSURANCE	70.19.71	5	0	0	0	0	D	20
335	PROD. MAGUCHI / TOIRE SYUNO - BUILDING DEP	70.19.90	5	0	0	0	0	D	20
336	PROD. MAGUCHI / TOIRE SYUNO - MACHINE DEP	70.19.92	5	0	0	0	0	D	20
337	PROD. MAGUCHI / TOIRE SYUNO - EXPENSE	70.19.99	5	0	0	0	0	D	20
338	PROD. NASLUCK SHOES BOX - LOCAL SALARY	70.30.01	5	0	0	0	0	D	20
339	PROD. NASLUCK SHOES BOX - LOCAL BONUS	70.30.02	5	0	0	0	0	D	20
340	PROD. NASLUCK SHOES BOX - PKWT SALARY	70.30.03	5	0	0	0	0	D	20
341	PROD. NASLUCK SHOES BOX - PKWT BONUS	70.30.04	5	0	0	0	0	D	20
342	PROD. NASLUCK SHOES BOX - OVERTIME	70.30.05	5	0	0	0	0	D	20
343	PROD. NASLUCK SHOES BOX - PPH 21	70.30.06	5	0	0	0	0	D	20
344	PROD. NASLUCK SHOES BOX - MEDICAL	70.30.07	5	0	0	0	0	D	20
345	PROD. NASLUCK SHOES BOX - ASTEK & BP	70.30.08	5	0	0	0	0	D	20
346	PROD. NASLUCK SHOES BOX - MEAL	70.30.09	5	0	0	0	0	D	20
347	PROD. NASLUCK SHOES BOX - ELECTRICITY	70.30.39	5	0	0	0	0	D	20
348	PROD. NASLUCK SHOES BOX - WATER & MAINT	70.30.41	5	0	0	0	0	D	20
349	PROD. NASLUCK SHOES BOX - STATIONARY	70.30.46	5	0	0	0	0	D	20
350	PROD. NASLUCK SHOES BOX - BUILDING INSURANCE	70.30.71	5	0	0	0	0	D	20
351	PROD. NASLUCK SHOES BOX - BUILDING DEP	70.30.90	5	0	0	0	0	D	20
352	PROD. NASLUCK SHOES BOX - MACHINE DEP	70.30.92	5	0	0	0	0	D	20
353	PROD. NASLUCK SHOES BOX - EXPENSE	70.30.99	5	0	0	0	0	D	20
354	PROD. NASLUCK STORAGE - LOCAL SALARY	70.31.01	5	0	0	0	0	D	20
355	PROD. NASLUCK STORAGE - LOCAL BONUS	70.31.02	5	0	0	0	0	D	20
356	PROD. NASLUCK STORAGE - PKWT SALARY	70.31.03	5	0	0	0	0	D	20
357	PROD. NASLUCK STORAGE - PKWT BONUS	70.31.04	5	0	0	0	0	D	20
358	PROD. NASLUCK STORAGE - OVERTIME	70.31.05	5	0	0	0	0	D	20
359	PROD. NASLUCK STORAGE - PPH 21	70.31.06	5	0	0	0	0	D	20
360	PROD. NASLUCK STORAGE - MEDICAL	70.31.07	5	0	0	0	0	D	20
361	PROD. NASLUCK STORAGE - ASTEK & BP	70.31.08	5	0	0	0	0	D	20
362	PROD. NASLUCK STORAGE - MEAL	70.31.09	5	0	0	0	0	D	20
363	PROD. NASLUCK STORAGE - ELECTRICITY	70.31.39	5	0	0	0	0	D	20
364	PROD. NASLUCK STORAGE - WATER & MAINT	70.31.41	5	0	0	0	0	D	20
365	PROD. NASLUCK STORAGE - STATIONARY	70.31.46	5	0	0	0	0	D	20
366	PROD. NASLUCK STORAGE - BUILDING INSURANCE	70.31.71	5	0	0	0	0	D	20
367	PROD. NASLUCK STORAGE - BUILDING DEP	70.31.90	5	0	0	0	0	D	20
368	PROD. NASLUCK STORAGE - MACHINE DEP	70.31.92	5	0	0	0	0	D	20
369	PROD. NASLUCK STORAGE - OTHER EXP	70.31.99	5	0	0	0	0	D	20
370	PROD. NASLUCK SPICE - LOCAL SALARY	70.32.01	5	0	0	0	0	D	20
371	PROD. NASLUCK SPICE - LOCAL BONUS	70.32.02	5	0	0	0	0	D	20
372	PROD. NASLUCK SPICE - PKWT SALARY	70.32.03	5	0	0	0	0	D	20
373	PROD. NASLUCK SPICE - PKWT BONUS	70.32.04	5	0	0	0	0	D	20
374	PROD. NASLUCK SPICE - OVERTIME	70.32.05	5	0	0	0	0	D	20
375	PROD. NASLUCK SPICE - PPH 21	70.32.06	5	0	0	0	0	D	20
376	PROD. NASLUCK SPICE - MEDICAL	70.32.07	5	0	0	0	0	D	20
377	PROD. NASLUCK SPICE - ASTEK & BP	70.32.08	5	0	0	0	0	D	20
378	PROD. NASLUCK SPICE - MEAL	70.32.09	5	0	0	0	0	D	20
379	PROD. NASLUCK SPICE - ELECTRICITY	70.32.39	5	0	0	0	0	D	20
380	PROD. NASLUCK SPICE - WATER & MAINT	70.32.41	5	0	0	0	0	D	20
381	PROD. NASLUCK SPICE - STATIONARY	70.32.46	5	0	0	0	0	D	20
382	PROD. NASLUCK SPICE - BUILDING INSURANCE	70.32.71	5	0	0	0	0	D	20
383	PROD. NASLUCK SPICE - BUILDING DEP	70.32.90	5	0	0	0	0	D	20
384	PROD. NASLUCK SPICE - MACHINE DEP	70.32.92	5	0	0	0	0	D	20
385	PROD. NASLUCK SPICE - OTHER EXP	70.32.99	5	0	0	0	0	D	20
386	PROD. NASLUCK CLOUCK - LOCAL SALARY	70.33.01	5	0	0	0	0	D	20
387	PROD. NASLUCK CLOUCK - LOCAL BONUS	70.33.02	5	0	0	0	0	D	20
388	PROD. NASLUCK CLOUCK - PKWT SALARY	70.33.03	5	0	0	0	0	D	20
389	PROD. NASLUCK CLOUCK - PKWT BONUS	70.33.04	5	0	0	0	0	D	20
390	PROD. NASLUCK CLOUCK - OVERTIME	70.33.05	5	0	0	0	0	D	20
391	PROD. NASLUCK CLOUCK - PPH 21	70.33.06	5	0	0	0	0	D	20
392	PROD. NASLUCK CLOUCK - MEDICAL	70.33.07	5	0	0	0	0	D	20
393	PROD. NASLUCK CLOUCK - ASTEK & BP	70.33.08	5	0	0	0	0	D	20
394	PROD. NASLUCK CLOUCK - MEAL	70.33.09	5	0	0	0	0	D	20
395	PROD. NASLUCK CLOUCK - ELECTRICITY	70.33.39	5	0	0	0	0	D	20
396	PROD. NASLUCK CLOUCK - WATER & MAINT	70.33.41	5	0	0	0	0	D	20
397	PROD. NASLUCK CLOUCK - STATIONARY	70.33.46	5	0	0	0	0	D	20
398	PROD. NASLUCK CLOUCK - BUILDING INSURANCE	70.33.71	5	0	0	0	0	D	20
399	PROD. NASLUCK CLOUCK - BUILDING DEP	70.33.90	5	0	0	0	0	D	20
400	PROD. NASLUCK CLOUCK - MACHINE DEP	70.33.92	5	0	0	0	0	D	20
401	PROD. NASLUCK CLOUCK - OTHER EXP	70.33.99	5	0	0	0	0	D	20
402	PROD. NASLUCK DOOR - LOCAL SALARY	70.34.01	5	0	0	0	0	D	20
403	PROD. NASLUCK DOOR - LOCAL BONUS	70.34.02	5	0	0	0	0	D	20
404	PROD. NASLUCK DOOR - PKWT SALARY	70.34.03	5	0	0	0	0	D	20
405	PROD. NASLUCK DOOR - PKWT BONUS	70.34.04	5	0	0	0	0	D	20
406	PROD. NASLUCK DOOR - OVERTIME	70.34.05	5	0	0	0	0	D	20
407	PROD. NASLUCK DOOR - PPH 21	70.34.06	5	0	0	0	0	D	20
408	PROD. NASLUCK DOOR - MEDICAL	70.34.07	5	0	0	0	0	D	20
409	PROD. NASLUCK DOOR - ASTEK & BP	70.34.08	5	0	0	0	0	D	20
410	PROD. NASLUCK DOOR - MEAL	70.34.09	5	0	0	0	0	D	20
411	PROD. NASLUCK DOOR - ELECTRICITY	70.34.39	5	0	0	0	0	D	20
412	PROD. NASLUCK DOOR - WATER & MAINT	70.34.41	5	0	0	0	0	D	20
413	PROD. NASLUCK DOOR - STATIONARY	70.34.46	5	0	0	0	0	D	20
414	PROD. NASLUCK DOOR - BUILDING INSURANCE	70.34.71	5	0	0	0	0	D	20
415	PROD. NASLUCK DOOR - BUILDING DEP	70.34.90	5	0	0	0	0	D	20
416	PROD. NASLUCK DOOR - MACHINE DEP	70.34.92	5	0	0	0	0	D	20
417	PROD. NASLUCK DOOR - OTHER EXP	70.34.99	5	0	0	0	0	D	20
418	PROD. WOOD ONE - LOCAL SALARY	70.35.01	5	0	0	0	0	D	20
419	PROD. WOOD ONE - LOCAL BONUS	70.35.02	5	0	0	0	0	D	20
420	PROD. WOOD ONE - PKWT SALARY	70.35.03	5	0	0	0	0	D	20
421	PROD. WOOD ONE - PKWT BONUS	70.35.04	5	0	0	0	0	D	20
422	PROD. WOOD ONE - OVERTIME	70.35.05	5	0	0	0	0	D	20
423	PROD. WOOD ONE - PPH 21	70.35.06	5	0	0	0	0	D	20
424	PROD. WOOD ONE - MEDICAL	70.35.07	5	0	0	0	0	D	20
425	PROD. WOOD ONE - ASTEK & BP	70.35.08	5	0	0	0	0	D	20
426	PROD. WOOD ONE - MEAL	70.35.09	5	0	0	0	0	D	20
427	PROD. WOOD ONE - ELECTRICITY	70.35.39	5	0	0	0	0	D	20
428	PROD. WOOD ONE - WATER & MAINT	70.35.41	5	0	0	0	0	D	20
429	PROD. WOOD ONE - STATIONARY	70.35.46	5	0	0	0	0	D	20
430	PROD. WOOD ONE - BUILDING INSURANCE	70.35.71	5	0	0	0	0	D	20
431	PROD. WOOD ONE - BUILDING DEP	70.35.90	5	0	0	0	0	D	20
432	PROD. WOOD ONE - MACHINE DEP	70.35.92	5	0	0	0	0	D	20
433	PROD. WOOD ONE - OTHER EXP	70.35.99	5	0	0	0	0	D	20
434	PROD. KYUSHU JYUTAKU KONKYO - LOCAL SALARY	70.36.01	5	0	0	0	0	D	20
435	PROD. KYUSHU JYUTAKU KONKYO - LOCAL BONUS	70.36.02	5	0	0	0	0	D	20
436	PROD. KYUSHU JYUTAKU KONKYO - PKWT SALARY	70.36.03	5	0	0	0	0	D	20
437	PROD. KYUSHU JYUTAKU KONKYO - PKWT BONUS	70.36.04	5	0	0	0	0	D	20
438	PROD. KYUSHU JYUTAKU KONKYO - OVERTIME	70.36.05	5	0	0	0	0	D	20
439	PROD. KYUSHU JYUTAKU KONKYO - PPH 21	70.36.06	5	0	0	0	0	D	20
440	PROD. KYUSHU JYUTAKU KONKYO - MEDICAL	70.36.07	5	0	0	0	0	D	20
441	PROD. KYUSHU JYUTAKU KONKYO - ASTEK & BP	70.36.08	5	0	0	0	0	D	20
442	PROD. KYUSHU JYUTAKU KONKYO - MEAL	70.36.09	5	0	0	0	0	D	20
443	PROD. KYUSHU JYUTAKU KONKYO - ELECTRICITY	70.36.39	5	0	0	0	0	D	20
444	PROD. KYUSHU JYUTAKU KONKYO - WATER & MAINT	70.36.41	5	0	0	0	0	D	20
445	PROD. KYUSHU JYUTAKU KONKYO - STATIONARY	70.36.46	5	0	0	0	0	D	20
446	PROD. KYUSHU JYUTAKU KONKYO - BUILDING INS	70.36.71	5	0	0	0	0	D	20
447	PROD. KYUSHU JYUTAKU KONKYO - BUILDING DEP	70.36.90	5	0	0	0	0	D	20
448	PROD. KYUSHU JYUTAKU KONKYO - MACHINE DEP	70.36.92	5	0	0	0	0	D	20
449	PROD. KYUSHU JYUTAKU KONKYO - OTHERS EXP	70.36.99	5	0	0	0	0	D	20
450	PROD. PANTORY - LOCAL SALARY	70.37.01	5	0	0	0	0	D	20
451	PROD. PANTORY - LOCAL BONUS	70.37.02	5	0	0	0	0	D	20
452	PROD. PANTORY - PKWT SALARY	70.37.03	5	0	0	0	0	D	20
453	PROD. PANTORY - PKWT BONUS	70.37.04	5	0	0	0	0	D	20
454	PROD. PANTORY - OVERTIME	70.37.05	5	0	0	0	0	D	20
455	PROD. PANTORY - PPH 21	70.37.06	5	0	0	0	0	D	20
456	PROD. PANTORY - MEDICAL	70.37.07	5	0	0	0	0	D	20
457	PROD. PANTORY - ASTEK & BP	70.37.08	5	0	0	0	0	D	20
458	PROD. PANTORY - MEAL	70.37.09	5	0	0	0	0	D	20
459	PROD. PANTORY - ELECTRICITY	70.37.39	5	0	0	0	0	D	20
460	PROD. PANTORY - WATER & MAINT	70.37.41	5	0	0	0	0	D	20
461	PROD. PANTORY - STATIONARY	70.37.46	5	0	0	0	0	D	20
462	PROD. PANTORY - BUILDING INS	70.37.71	5	0	0	0	0	D	20
463	PROD. PANTORY - BUILDING DEP	70.37.90	5	0	0	0	0	D	20
464	PROD. PANTORY - MACHINE DEP	70.37.92	5	0	0	0	0	D	20
465	PROD. PANTORY - OTHERS EXP	70.37.99	5	0	0	0	0	D	20
466	PROD. WASHING MACHINE - LOCAL SALARY	70.38.01	5	0	0	0	0	D	20
467	PROD. WASHING MACHINE - LOCAL BONUS	70.38.02	5	0	0	0	0	D	20
468	PROD. WASHING MACHINE - PKWT SALARY	70.38.03	5	0	0	0	0	D	20
469	PROD. WASHING MACHINE - PKWT BONUS	70.38.04	5	0	0	0	0	D	20
470	PROD. WASHING MACHINE - OVERTIME	70.38.05	5	0	0	0	0	D	20
471	PROD. WASHING MACHINE - PPH 21	70.38.06	5	0	0	0	0	D	20
472	PROD. WASHING MACHINE - MEDICAL	70.38.07	5	0	0	0	0	D	20
473	PROD. WASHING MACHINE - ASTEK & BP	70.38.08	5	0	0	0	0	D	20
474	PROD. WASHING MACHINE - MEAL	70.38.09	5	0	0	0	0	D	20
475	PROD. WASHING MACHINE - ELECTRICITY	70.38.39	5	0	0	0	0	D	20
476	PROD. WASHING MACHINE - WATER & MAINT	70.38.41	5	0	0	0	0	D	20
477	PROD. WASHING MACHINE - STATIONARY	70.38.46	5	0	0	0	0	D	20
478	PROD. WASHING MACHINE - BUILDING INS	70.38.71	5	0	0	0	0	D	20
479	PROD. WASHING MACHINE - BUILDING DEP	70.38.90	5	0	0	0	0	D	20
480	PROD. WASHING MACHINE - MACHINE DEP	70.38.92	5	0	0	0	0	D	20
481	PROD. WASHING MACHINE - OTHERS EXP	70.38.99	5	0	0	0	0	D	20
482	PROD. W ~ 600 - LOCAL SALARY	70.39.01	5	0	0	0	0	D	20
483	PROD. W ~ 600 - LOCAL BONUS	70.39.02	5	0	0	0	0	D	20
484	PROD. W ~ 600 - PKWT SALARY	70.39.03	5	0	0	0	0	D	20
485	PROD. W ~ 600 - PKWT BONUS	70.39.04	5	0	0	0	0	D	20
486	PROD. W ~ 600 - OVERTIME	70.39.05	5	0	0	0	0	D	20
487	PROD. W ~ 600 - PPH 21	70.39.06	5	0	0	0	0	D	20
488	PROD. W ~ 600 - MEDICAL	70.39.07	5	0	0	0	0	D	20
489	PROD. W ~ 600 - ASTEK & BP	70.39.08	5	0	0	0	0	D	20
490	PROD. W ~ 600 - MEAL	70.39.09	5	0	0	0	0	D	20
491	PROD. W ~ 600 - ELECTRICITY	70.39.39	5	0	0	0	0	D	20
492	PROD. W ~ 600 - WATER & MAINT	70.39.41	5	0	0	0	0	D	20
493	PROD. W ~ 600 - STATIONARY	70.39.46	5	0	0	0	0	D	20
494	PROD. W ~ 600 - BUILDING INS	70.39.71	5	0	0	0	0	D	20
495	PROD. W ~ 600 - BUILDING DEP	70.39.90	5	0	0	0	0	D	20
496	PROD. W ~ 600 - MACHINE DEP	70.39.92	5	0	0	0	0	D	20
497	PROD. W ~ 600 - OTHERS EXP	70.39.99	5	0	0	0	0	D	20
498	PROD. TOPPAN TANAITA TV - LOCAL SALARY	70.50.01	5	0	0	0	0	D	20
499	PROD. TOPPAN TANAITA TV - LOCAL BONUS	70.50.02	5	0	0	0	0	D	20
500	PROD. TOPPAN TANAITA TV - PKWT SALARY	70.50.03	5	0	0	0	0	D	20
501	PROD. TOPPAN TANAITA TV - PKWT BONUS	70.50.04	5	0	0	0	0	D	20
502	PROD. TOPPAN TANAITA TV - OVERTIME	70.50.05	5	0	0	0	0	D	20
503	PROD. TOPPAN TANAITA TV - PPH 21	70.50.06	5	0	0	0	0	D	20
504	PROD. TOPPAN TANAITA TV - MEDICAL	70.50.07	5	0	0	0	0	D	20
505	PROD. TOPPAN TANAITA TV - ASTEK & BP	70.50.08	5	0	0	0	0	D	20
506	PROD. TOPPAN TANAITA TV - MEAL	70.50.09	5	0	0	0	0	D	20
507	PROD. TOPPAN TANAITA TV - ELECTRICITY	70.50.39	5	0	0	0	0	D	20
508	PROD. TOPPAN TANAITA TV - WATER & MAINT	70.50.41	5	0	0	0	0	D	20
509	PROD. TOPPAN TANAITA TV - STATIONARY	70.50.46	5	0	0	0	0	D	20
510	PROD. TOPPAN TANAITA TV - BUILDING INS	70.50.71	5	0	0	0	0	D	20
511	PROD. TOPPAN TANAITA TV - BUILDING DEP	70.50.90	5	0	0	0	0	D	20
512	PROD. TOPPAN TANAITA TV - MACHINE DEP	70.50.92	5	0	0	0	0	D	20
513	PROD. TOPPAN TANAITA TV - EXPENSE	70.50.99	5	0	0	0	0	D	20
514	PROD. XX - LOCAL SALARY	70.50.01	5	0	0	0	0	D	20
515	PROD. XX - LOCAL BONUS	70.50.02	5	0	0	0	0	D	20
516	PROD. XX - PKWT SALARY	70.50.03	5	0	0	0	0	D	20
517	PROD. XX - PKWT BONUS	70.50.04	5	0	0	0	0	D	20
518	PROD. XX - OVERTIME	70.50.05	5	0	0	0	0	D	20
519	PROD. XX - PPH 21	70.50.06	5	0	0	0	0	D	20
520	PROD. XX - MEDICAL	70.50.07	5	0	0	0	0	D	20
521	PROD. XX - ASTEK & BP	70.50.08	5	0	0	0	0	D	20
522	PROD. XX - MEAL	70.50.09	5	0	0	0	0	D	20
523	PROD. XX - ELECTRICITY	70.50.39	5	0	0	0	0	D	20
524	PROD. XX - WATER & MAINT	70.50.41	5	0	0	0	0	D	20
525	PROD. XX - STATIONARY	70.50.46	5	0	0	0	0	D	20
526	PROD. XX - BUILDING INS	70.50.71	5	0	0	0	0	D	20
527	PROD. XX - BUILDING DEP	70.50.90	5	0	0	0	0	D	20
528	PROD. XX - MACHINE DEP	70.50.92	5	0	0	0	0	D	20
529	PROD. XX - EXPENSE	70.50.99	5	0	0	0	0	D	20
530	PROD. TENMA - LOCAL SALARY	70.60.01	5	0	0	0	0	D	20
531	PROD. TENMA - LOCAL BONUS	70.60.02	5	0	0	0	0	D	20
532	PROD. TENMA - PKWT SALARY	70.60.03	5	0	0	0	0	D	20
533	PROD. TENMA - PKWT BONUS	70.60.04	5	0	0	0	0	D	20
534	PROD. TENMA - OVERTIME	70.60.05	5	0	0	0	0	D	20
535	PROD. TENMA - PPH 21	70.60.06	5	0	0	0	0	D	20
536	PROD. TENMA - MEDICINE	70.60.07	5	0	0	0	0	D	20
537	PROD. TENMA - ASTEK & BP	70.60.08	5	0	0	0	0	D	20
538	PROD. TENMA - MEAL	70.60.09	5	0	0	0	0	D	20
539	PROD. TENMA - ELECTRICITY	70.60.39	5	0	0	0	0	D	20
540	PROD. TENMA - WATER & MAINT	70.60.41	5	0	0	0	0	D	20
541	PROD. TENMA - STATIONARY	70.60.46	5	0	0	0	0	D	20
542	PROD. TENMA - BUILDING INSURANCE	70.60.71	5	0	0	0	0	D	20
543	PROD. TENMA - BUILDING DEP	70.60.90	5	0	0	0	0	D	20
544	PROD. TENMA - MACHINE DEP	70.60.92	5	0	0	0	0	D	20
545	PROD. TENMA - EXPENSE	70.60.99	5	0	0	0	0	D	20
546	PROD. MARUNI - LOCAL SALARY	70.70.01	5	0	0	0	0	D	20
547	PROD. MARUNI - LOCAL BONUS	70.70.02	5	0	0	0	0	D	20
548	PROD. MARUNI - PKWT SALARY	70.70.03	5	0	0	0	0	D	20
549	PROD. MARUNI - PKWT BONUS	70.70.04	5	0	0	0	0	D	20
550	PROD. MARUNI - OVERTIME	70.70.05	5	0	0	0	0	D	20
551	PROD. MARUNI - PPH 21	70.70.06	5	0	0	0	0	D	20
552	PROD. MARUNI - MEDICAL	70.70.07	5	0	0	0	0	D	20
553	PROD. MARUNI - ASTEK & BP	70.70.08	5	0	0	0	0	D	20
554	PROD. MARUNI - MEAL	70.70.09	5	0	0	0	0	D	20
555	PROD. MARUNI - ELECTRICITY	70.70.39	5	0	0	0	0	D	20
556	PROD. MARUNI - WATER & MAINT	70.70.41	5	0	0	0	0	D	20
557	PROD. MARUNI - STATIONARY	70.70.46	5	0	0	0	0	D	20
558	PROD. MARUNI - BUILDING INSURANCE	70.70.71	5	0	0	0	0	D	20
559	PROD. MARUNI - BUILDING DEP	70.70.90	5	0	0	0	0	D	20
560	PROD. MARUNI - MACHINE DEP	70.70.92	5	0	0	0	0	D	20
561	PROD. MARUNI - OTHER EXP	70.70.99	5	0	0	0	0	D	20
562	PROD. DAIKEN TAIWAN - LOCAL SALARY	70.71.01	5	0	0	0	0	D	20
563	PROD. DAIKEN TAIWAN - LOCAL BONUS	70.71.02	5	0	0	0	0	D	20
564	PROD. DAIKEN TAIWAN - PKWT SALARY	70.71.03	5	0	0	0	0	D	20
565	PROD. DAIKEN TAIWAN - PKWT BONUS	70.71.04	5	0	0	0	0	D	20
566	PROD. DAIKEN TAIWAN - OVERTIME	70.71.05	5	0	0	0	0	D	20
567	PROD. DAIKEN TAIWAN - PPH 21	70.71.06	5	0	0	0	0	D	20
568	PROD. DAIKEN TAIWAN - MEDICAL	70.71.07	5	0	0	0	0	D	20
569	PROD. DAIKEN TAIWAN - ASTEK & BP	70.71.08	5	0	0	0	0	D	20
570	PROD. DAIKEN TAIWAN - MEAL	70.71.09	5	0	0	0	0	D	20
571	PROD. DAIKEN TAIWAN - ELECTRICITY	70.71.39	5	0	0	0	0	D	20
572	PROD. DAIKEN TAIWAN - WATER & MAINT	70.71.41	5	0	0	0	0	D	20
573	PROD. DAIKEN TAIWAN - STATIONARY	70.71.46	5	0	0	0	0	D	20
574	PROD. DAIKEN TAIWAN - BUILDING INSURANCE	70.71.71	5	0	0	0	0	D	20
575	PROD. DAIKEN TAIWAN - BUILDING DEP	70.71.90	5	0	0	0	0	D	20
576	PROD. DAIKEN TAIWAN - MACHINE DEP	70.71.92	5	0	0	0	0	D	20
577	PROD. DAIKEN TAIWAN - OTHER EXP	70.71.99	5	0	0	0	0	D	20
578	PROD. DAIKEN SINGAPORE - LOCAL SALARY	70.72.01	5	0	0	0	0	D	20
579	PROD. DAIKEN SINGAPORE - LOCAL BONUS	70.72.02	5	0	0	0	0	D	20
580	PROD. DAIKEN SINGAPORE - PKWT SALARY	70.72.03	5	0	0	0	0	D	20
581	PROD. DAIKEN SINGAPORE - PKWT BONUS	70.72.04	5	0	0	0	0	D	20
582	PROD. DAIKEN SINGAPORE - OVERTIME	70.72.05	5	0	0	0	0	D	20
583	PROD. DAIKEN SINGAPORE - PPH 21	70.72.06	5	0	0	0	0	D	20
584	PROD. DAIKEN SINGAPORE - MEDICINE	70.72.07	5	0	0	0	0	D	20
585	PROD. DAIKEN SINGAPORE - ASTEK & BP	70.72.08	5	0	0	0	0	D	20
586	PROD. DAIKEN SINGAPORE - MEAL	70.72.09	5	0	0	0	0	D	20
587	PROD. DAIKEN SINGAPORE - ELECTRICITY	70.72.39	5	0	0	0	0	D	20
588	PROD. DAIKEN SINGAPORE - WATER & MAINT	70.72.41	5	0	0	0	0	D	20
589	PROD. DAIKEN SINGAPORE - STATIONARY	70.72.46	5	0	0	0	0	D	20
590	PROD. DAIKEN SINGAPORE - BUILDING INSURANCE	70.72.71	5	0	0	0	0	D	20
591	PROD. DAIKEN SINGAPORE - BUILDING DEP	70.72.90	5	0	0	0	0	D	20
592	PROD. DAIKEN SINGAPORE - MACHINE DEP	70.72.92	5	0	0	0	0	D	20
593	PROD. DAIKEN SINGAPORE - EXPENSE	70.72.99	5	0	0	0	0	D	20
594	PROD. CHITOSE - LOCAL SALARY	70.73.01	5	0	0	0	0	D	20
595	PROD. CHITOSE - LOCAL BONUS	70.73.02	5	0	0	0	0	D	20
596	PROD. CHITOSE - PKWT SALARY	70.73.03	5	0	0	0	0	D	20
597	PROD. CHITOSE - PKWT BONUS	70.73.04	5	0	0	0	0	D	20
598	PROD. CHITOSE - OVERTIME	70.73.05	5	0	0	0	0	D	20
599	PROD. CHITOSE - PPH 21	70.73.06	5	0	0	0	0	D	20
600	PROD. CHITOSE - MEDICINE	70.73.07	5	0	0	0	0	D	20
601	PROD. CHITOSE - ASTEK & BP	70.73.08	5	0	0	0	0	D	20
602	PROD. CHITOSE - MEAL	70.73.09	5	0	0	0	0	D	20
603	PROD. CHITOSE - ELECTRICITY	70.73.39	5	0	0	0	0	D	20
604	PROD. CHITOSE - WATER & MAINT	70.73.41	5	0	0	0	0	D	20
605	PROD. CHITOSE - STATIONARY	70.73.46	5	0	0	0	0	D	20
606	PROD. CHITOSE - BUILDING INSURANCE	70.73.71	5	0	0	0	0	D	20
607	PROD. CHITOSE - BUILDING DEP	70.73.90	5	0	0	0	0	D	20
608	PROD. CHITOSE - MACHINE DEP	70.73.92	5	0	0	0	0	D	20
609	PROD. CHITOSE - EXPENSE	70.73.99	5	0	0	0	0	D	20
610	PROD. LOCAL  D D I - LOCAL SALARY	70.90.01	5	0	0	0	0	D	20
611	PROD. LOCAL  D D I - LOCAL BONUS	70.90.02	5	0	0	0	0	D	20
612	PROD. LOCAL  D D I - PKWT SALARY	70.90.03	5	0	0	0	0	D	20
613	PROD. LOCAL  D D I - PKWT BONUS	70.90.04	5	0	0	0	0	D	20
614	PROD. LOCAL  D D I - OVERTIME	70.90.05	5	0	0	0	0	D	20
615	PROD. LOCAL  D D I - PPH 21	70.90.06	5	0	0	0	0	D	20
616	PROD. LOCAL  D D I - MEDICAL	70.90.07	5	0	0	0	0	D	20
617	PROD. LOCAL  D D I - ASTEK & BP	70.90.08	5	0	0	0	0	D	20
618	PROD. LOCAL  D D I - MEAL	70.90.09	5	0	0	0	0	D	20
619	PROD. LOCAL  D D I - ELECTRICITY	70.90.39	5	0	0	0	0	D	20
620	PROD. LOCAL  D D I - WATER & MAINT	70.90.41	5	0	0	0	0	D	20
621	PROD. LOCAL  D D I - STATIONARY	70.90.46	5	0	0	0	0	D	20
622	PROD. LOCAL  D D I - BUILDING INSURANCE	70.90.71	5	0	0	0	0	D	20
623	PROD. LOCAL  D D I - BUILDING DEP	70.90.90	5	0	0	0	0	D	20
624	PROD. LOCAL  D D I - MACHINE DEP	70.90.92	5	0	0	0	0	D	20
625	PROD. LOCAL  D D I - OTHER EXP	70.90.99	5	0	0	0	0	D	20
626	PROD. LOCAL SDI DOOR SETS - LOCAL SALARY	70.91.01	5	0	0	0	0	D	20
627	PROD. LOCAL SDI DOOR SETS - LOCAL BONUS	70.91.02	5	0	0	0	0	D	20
628	PROD. LOCAL SDI DOOR SETS - PKWT SALARY	70.91.03	5	0	0	0	0	D	20
629	PROD. LOCAL SDI DOOR SETS - PKWT BONUS	70.91.04	5	0	0	0	0	D	20
630	PROD. LOCAL SDI DOOR SETS - OVERTIME	70.91.05	5	0	0	0	0	D	20
631	PROD. LOCAL SDI DOOR SETS - PPH 21	70.91.06	5	0	0	0	0	D	20
632	PROD. LOCAL SDI DOOR SETS - MEDICAL	70.91.07	5	0	0	0	0	D	20
633	PROD. LOCAL SDI DOOR SETS - ASTEK & BP	70.91.08	5	0	0	0	0	D	20
634	PROD. LOCAL SDI DOOR SETS - MEAL	70.91.09	5	0	0	0	0	D	20
635	PROD. LOCAL SDI DOOR SETS - ELECTRICITY	70.91.39	5	0	0	0	0	D	20
636	PROD. LOCAL SDI DOOR SETS - WATER & MAINT	70.91.41	5	0	0	0	0	D	20
637	PROD. LOCAL SDI DOOR SETS - STATIONARY	70.91.46	5	0	0	0	0	D	20
638	PROD. LOCAL SDI DOOR SETS - BUILDING INSURANCE	70.91.71	5	0	0	0	0	D	20
639	PROD. LOCAL SDI DOOR SETS - BUILDING DEP	70.91.90	5	0	0	0	0	D	20
640	PROD. LOCAL SDI DOOR SETS - MACHINE DEP	70.91.92	5	0	0	0	0	D	20
641	PROD. LOCAL SDI DOOR SETS - OTHER EXP	70.91.99	5	0	0	0	0	D	20
642	PROD. LOCAL SIWI - LOCAL SALARY	70.92.01	5	0	0	0	0	D	20
643	PROD. LOCAL SIWI - LOCAL BONUS	70.92.02	5	0	0	0	0	D	20
644	PROD. LOCAL SIWI - PKWT SALARY	70.92.03	5	0	0	0	0	D	20
645	PROD. LOCAL SIWI - PKWT BONUS	70.92.04	5	0	0	0	0	D	20
646	PROD. LOCAL SIWI - OVERTIME	70.92.05	5	0	0	0	0	D	20
647	PROD. LOCAL SIWI - PPH 21	70.92.06	5	0	0	0	0	D	20
648	PROD. LOCAL SIWI - MEDICAL	70.92.07	5	0	0	0	0	D	20
649	PROD. LOCAL SIWI - ASTEK & BP	70.92.08	5	0	0	0	0	D	20
650	PROD. LOCAL SIWI - MEAL	70.92.09	5	0	0	0	0	D	20
651	PROD. LOCAL SIWI - ELECTRICITY	70.92.39	5	0	0	0	0	D	20
652	PROD. LOCAL SIWI - WATER & MAINT	70.92.41	5	0	0	0	0	D	20
653	PROD. LOCAL SIWI - STATIONARY	70.92.46	5	0	0	0	0	D	20
654	PROD. LOCAL SIWI - BUILDING INSURANCE	70.92.71	5	0	0	0	0	D	20
655	PROD. LOCAL SIWI - BUILDING DEP	70.92.90	5	0	0	0	0	D	20
656	PROD. LOCAL SIWI - MACHINE DEP	70.92.92	5	0	0	0	0	D	20
657	PROD. LOCAL SIWI - OTHER EXP	70.92.99	5	0	0	0	0	D	20
658	PROD. LOCAL INA - LOCAL SALARY	70.93.01	5	0	0	0	0	D	20
659	PROD. LOCAL INA - LOCAL BONUS	70.93.02	5	0	0	0	0	D	20
660	PROD. LOCAL INA - PKWT SALARY	70.93.03	5	0	0	0	0	D	20
661	PROD. LOCAL INA - PKWT BONUS	70.93.04	5	0	0	0	0	D	20
662	PROD. LOCAL INA - OVERTIME	70.93.05	5	0	0	0	0	D	20
663	PROD. LOCAL INA - PPH 21	70.93.06	5	0	0	0	0	D	20
664	PROD. LOCAL INA - MEDICAL	70.93.07	5	0	0	0	0	D	20
665	PROD. LOCAL INA - ASTEK & BP	70.93.08	5	0	0	0	0	D	20
666	PROD. LOCAL INA - MEAL	70.93.09	5	0	0	0	0	D	20
667	PROD. LOCAL INA - ELECTRICITY	70.93.39	5	0	0	0	0	D	20
668	PROD. LOCAL INA - WATER & MAINT	70.93.41	5	0	0	0	0	D	20
669	PROD. LOCAL INA - STATIONARY	70.93.46	5	0	0	0	0	D	20
670	PROD. LOCAL INA - BUILDING INSURANCE	70.93.71	5	0	0	0	0	D	20
671	PROD. LOCAL INA - BUILDING DEP	70.93.90	5	0	0	0	0	D	20
672	PROD. LOCAL INA - MACHINE DEP	70.93.92	5	0	0	0	0	D	20
673	PROD. LOCAL INA - OTHER EXP	70.93.99	5	0	0	0	0	D	20
674	PROD. SAMPLE & OTHERS - LOCAL SALARY	70.99.01	5	0	0	0	0	D	20
675	PROD. SAMPLE & OTHERS - LOCAL BONUS	70.99.02	5	0	0	0	0	D	20
676	PROD. SAMPLE & OTHERS - PKWT SALARY	70.99.03	5	0	0	0	0	D	20
677	PROD. SAMPLE & OTHERS - PKWT BONUS	70.99.04	5	0	0	0	0	D	20
678	PROD. SAMPLE & OTHERS - OVERTIME	70.99.05	5	0	0	0	0	D	20
679	PROD. SAMPLE & OTHERS - PPH 21	70.99.06	5	0	0	0	0	D	20
680	PROD. SAMPLE & OTHERS - MEDICAL	70.99.07	5	0	0	0	0	D	20
681	PROD. SAMPLE & OTHERS - ASTEK & BP	70.99.08	5	0	0	0	0	D	20
682	PROD. SAMPLE & OTHERS - MEAL	70.99.09	5	0	0	0	0	D	20
683	PROD. SAMPLE & OTHERS - ELECTRICITY	70.99.39	5	0	0	0	0	D	20
684	PROD. SAMPLE & OTHERS - WATER & MAINT	70.99.41	5	0	0	0	0	D	20
685	PROD. SAMPLE & OTHERS - STATIONARY	70.99.46	5	0	0	0	0	D	20
686	PROD. SAMPLE & OTHERS - BUILDING INSURANCE	70.99.71	5	0	0	0	0	D	20
687	PROD. SAMPLE & OTHERS - BUILDING DEP	70.99.90	5	0	0	0	0	D	20
688	PROD. SAMPLE & OTHERS - MACHINE DEP	70.99.92	5	0	0	0	0	D	20
689	PROD. SAMPLE & OTHERS - OTHER EXP	70.99.99	5	0	0	0	0	D	20
690	MAINTENANCE - LOCAL SALARY	80.90.01	6	0	0	0	0	D	0
691	MAINTENANCE - LOCAL BONUS	80.90.02	6	0	0	0	0	D	0
692	MAINTENANCE - PKWT SALARY	80.90.03	6	0	0	0	0	D	0
693	MAINTENANCE - PKWT BONUS	80.90.04	6	0	0	0	0	D	0
694	MAINTENANCE - OVERTIME	80.90.05	6	0	0	0	0	D	0
695	MAINTENANCE - PPH 21	80.90.06	6	0	0	0	0	D	0
696	MAINTENANCE - MEDICAL	80.90.07	6	0	0	0	0	D	0
697	MAINTENANCE - ASTEK & BP	80.90.08	6	0	0	0	0	D	0
698	MAINTENANCE - MEAL	80.90.09	6	0	0	0	0	D	0
699	MAINTENANCE - ELECTRICAL SUPPLIES	80.90.38	6	0	0	0	0	D	0
700	MAINTENANCE - ELECTRICITY	80.90.39	6	0	0	0	0	D	0
701	MAINTENANCE - WATER & MAINT	80.90.41	6	0	0	0	0	D	0
702	MAINTENANCE - STATIONARY	80.90.46	6	0	0	0	0	D	0
703	MAINTENANCE -  REPAIR PARTS	80.90.63	6	0	0	0	0	D	0
704	MAINTENANCE -  REPAIR	80.90.66	6	0	0	0	0	D	0
705	MAINTENANCE - WORKSHOP	80.90.67	6	0	0	0	0	D	0
706	MAINTENANCE -  RETYPING & REGRINDING	80.90.68	6	0	0	0	0	D	0
707	MAINTENANCE - OTHER EXP	80.90.99	6	0	0	0	0	D	0
708	WAREHOUSE - LOCAL SALARY	80.91.01	6	0	0	0	0	D	0
709	WAREHOUSE - LOCAL BONUS	80.91.02	6	0	0	0	0	D	0
710	WAREHOUSE - PKWT SALARY	80.91.03	6	0	0	0	0	D	0
711	WAREHOUSE - PKWT BONUS	80.91.04	6	0	0	0	0	D	0
712	WAREHOUSE - OVERTIME	80.91.05	6	0	0	0	0	D	0
713	WAREHOUSE - PPH 21	80.91.06	6	0	0	0	0	D	0
714	WAREHOUSE - MEDICAL	80.91.07	6	0	0	0	0	D	0
715	WAREHOUSE - ASTEK & BP	80.91.08	6	0	0	0	0	D	0
716	WAREHOUSE - MEAL	80.91.09	6	0	0	0	0	D	0
717	WAREHOUSE - OUT OF TOWN	80.91.17	6	0	0	0	0	D	0
718	WAREHOUSE - ELECTRICITY	80.91.39	6	0	0	0	0	D	0
719	WAREHOUSE - WATER & MAINT	80.91.41	6	0	0	0	0	D	0
720	WAREHOUSE - STATIONARY	80.91.46	6	0	0	0	0	D	0
721	WAREHOUSE - REPAIR	80.91.66	6	0	0	0	0	D	0
722	WAREHOUSE - HEAVY EQUIPMENT DEP.	80.91.93	6	0	0	0	0	D	0
723	WAREHOUSE - FACTORY EQUIPMENT DEP.	80.91.95	6	0	0	0	0	D	0
724	WAREHOUSE - FORKLIFT EXP	80.91.99	6	0	0	0	0	D	0
725	Q A - LOCAL SALARY	80.92.01	5	0	0	0	0	D	20
726	Q A - LOCAL BONUS	80.92.02	5	0	0	0	0	D	20
727	Q A - PKWT SALARY	80.92.03	5	0	0	0	0	D	20
728	Q A - PKWT BONUS	80.92.04	5	0	0	0	0	D	20
729	Q A - OVERTIME	80.92.05	5	0	0	0	0	D	20
730	Q A - PPH 21	80.92.06	5	0	0	0	0	D	20
731	Q A - MEDICAL	80.92.07	5	0	0	0	0	D	20
732	Q A - ASTEK & BP	80.92.08	5	0	0	0	0	D	20
733	Q A - MEAL	80.92.09	5	0	0	0	0	D	20
734	Q A - BUSINESS TRIP	80.92.16	5	0	0	0	0	D	20
735	Q A - ELECTRICITY	80.92.39	5	0	0	0	0	D	20
736	Q A - WATER & MAINT	80.92.41	5	0	0	0	0	D	20
737	Q A - STATIONARY	80.92.46	5	0	0	0	0	D	20
738	Q A - REPAIR	80.92.66	5	0	0	0	0	D	20
739	Q A - OTHER EXP	80.92.99	5	0	0	0	0	D	20
740	R & D - LOCAL SALARY	80.93.01	5	0	0	0	0	D	20
741	R & D - LOCAL BONUS	80.93.02	5	0	0	0	0	D	20
742	R & D - PKWT SALARY	80.93.03	5	0	0	0	0	D	20
743	R & D - PKWT BONUS	80.93.04	5	0	0	0	0	D	20
744	R & D - OVERTIME	80.93.05	5	0	0	0	0	D	20
745	R & D - PPH 21	80.93.06	5	0	0	0	0	D	20
746	R & D - MEDICAL	80.93.07	5	0	0	0	0	D	20
747	R & D - ASTEK & BP	80.93.08	5	0	0	0	0	D	20
748	R & D - MEAL	80.93.09	5	0	0	0	0	D	20
749	R & D - INSPECTION FEE	80.93.15	5	0	0	0	0	D	20
750	R & D - ELECTRICITY	80.93.39	5	0	0	0	0	D	20
751	R & D - WATER & MAINT	80.93.41	5	0	0	0	0	D	20
752	R & D - STATIONARY	80.93.46	5	0	0	0	0	D	20
753	R & D - OFFICE EQUIP MAINT	80.93.63	5	0	0	0	0	D	20
754	R & D - REPAIR	80.93.66	5	0	0	0	0	D	20
755	R & D - BUILDING DEP	80.93.90	5	0	0	0	0	D	20
756	R & D - MACHINE DEP	80.93.92	5	0	0	0	0	D	20
757	R & D - OTHER EXP	80.93.99	5	0	0	0	0	D	20
758	KAIZEN - LOCAL SALARY	80.94.01	5	0	0	0	0	D	20
759	KAIZEN - LOCAL BONUS	80.94.02	5	0	0	0	0	D	20
760	KAIZEN - PKWT SALARY	80.94.03	5	0	0	0	0	D	20
761	KAIZEN - PKWT BONUS	80.94.04	5	0	0	0	0	D	20
762	KAIZEN - OVERTIME	80.94.05	5	0	0	0	0	D	20
763	KAIZEN - PPH 21	80.94.06	5	0	0	0	0	D	20
764	KAIZEN - MEDICAL	80.94.07	5	0	0	0	0	D	20
765	KAIZEN - ASTEK & BP	80.94.08	5	0	0	0	0	D	20
766	KAIZEN - MEAL	80.94.09	5	0	0	0	0	D	20
767	KAIZEN - ELECTRICITY	80.94.39	5	0	0	0	0	D	20
768	KAIZEN - WATER & MAINT	80.94.41	5	0	0	0	0	D	20
769	KAIZEN - STATIONARY	80.94.46	5	0	0	0	0	D	20
770	KAIZEN - REPAIR	80.94.66	5	0	0	0	0	D	20
771	KAIZEN - BUILDING DEP	80.94.90	5	0	0	0	0	D	20
772	KAIZEN - OTHER EXP	80.94.99	5	0	0	0	0	D	20
773	P P I C - LOCAL SALARY	80.99.01	5	0	0	0	0	D	20
774	P P I C - LOCAL BONUS	80.99.02	5	0	0	0	0	D	20
775	P P I C - PKWT SALARY	80.99.03	5	0	0	0	0	D	20
776	P P I C - PKWT BONUS	80.99.04	5	0	0	0	0	D	20
777	P P I C - OVERTIME	80.99.05	5	0	0	0	0	D	20
778	P P I C - PPH 21	80.99.06	5	0	0	0	0	D	20
779	P P I C - MEDICAL	80.99.07	5	0	0	0	0	D	20
780	P P I C - ASTEK & BP	80.99.08	5	0	0	0	0	D	20
781	P P I C - MEAL	80.99.09	5	0	0	0	0	D	20
782	P P I C - OVERSEAS BUSINESS TRIP	80.99.16	5	0	0	0	0	D	20
783	P P I C - TRAINING & EDUCATION	80.99.27	5	0	0	0	0	D	20
784	P P I C - ELECTRICITY	80.99.39	5	0	0	0	0	D	20
785	P P I C - WATER & MAINT	80.99.41	5	0	0	0	0	D	20
786	P P I C - STATIONARY	80.99.46	5	0	0	0	0	D	20
787	P P I C - REPAIR	80.99.66	5	0	0	0	0	D	20
788	P P I C - INSURANCE	80.99.71	5	0	0	0	0	D	20
789	P P I C - BUILDING DEP.	80.99.90	5	0	0	0	0	D	20
790	P P I C - MACHINE DEP.	80.99.92	5	0	0	0	0	D	20
791	P P I C - OTHER EXP	80.99.99	5	0	0	0	0	D	20
792	GEN & ADM JP - SALARY	90.01.01	6	0	0	0	0	D	0
793	GEN & ADM JP - BONUS	90.01.02	6	0	0	0	0	D	0
794	GEN & ADM JP - PPH 21	90.01.06	6	0	0	0	0	D	0
795	GEN & ADM JP - MEDICAL	90.01.07	6	0	0	0	0	D	0
796	GEN & ADM JP - EXPATRIATE SERVICE	90.01.14	6	0	0	0	0	D	0
797	GEN & ADM JP - OVERSEAS BUSINESS TRIP	90.01.16	6	0	0	0	0	D	0
798	GEN & ADM JP - TRAVELLING / OUT of TOWN	90.01.17	6	0	0	0	0	D	0
799	GEN & ADM JP - TRAVELLING & ACCOMODATION	90.01.18	6	0	0	0	0	D	0
800	GEN & ADM JP - JAPANESE LICENSES	90.01.24	6	0	0	0	0	D	0
801	GEN & ADM JP - ENTERTAINMENT	90.01.54	6	0	0	0	0	D	0
802	GEN & ADM JP - JAPANESE INSURANCE	90.01.70	6	0	0	0	0	D	0
803	GEN & ADM JP - OTHER EXPENSE	90.01.99	6	0	0	0	0	D	0
804	OFFICE ADM - LOCAL SALARY	90.02.01	6	0	0	0	0	D	0
805	OFFICE ADM - LOCAL BONUS	90.02.02	6	0	0	0	0	D	0
806	OFFICE ADM - PKWT SALARY	90.02.03	6	0	0	0	0	D	0
807	OFFICE ADM - PKWT BONUS	90.02.04	6	0	0	0	0	D	0
808	OFFICE ADM - OVERTIME	90.02.05	6	0	0	0	0	D	0
809	OFFICE ADM - PPH 21	90.02.06	6	0	0	0	0	D	0
810	OFFICE ADM - MEDICAL	90.02.07	6	0	0	0	0	D	0
811	OFFICE ADM - ASTEK & BP	90.02.08	6	0	0	0	0	D	0
812	OFFICE ADM - MEAL	90.02.09	6	0	0	0	0	D	0
813	OFFICE ADM - OVERSEAS BUSINESS TRIP	90.02.16	6	0	0	0	0	D	0
814	OFFICE ADM - OUT OF TOWN	90.02.17	6	0	0	0	0	D	0
815	OFFICE ADM - TRANSPORT VEHICLE	90.02.19	6	0	0	0	0	D	0
816	OFFICE ADM - COMPANY LICENSES	90.02.23	6	0	0	0	0	D	0
817	OFFICE ADM - VISITOR LICENSES	90.02.24	6	0	0	0	0	D	0
818	OFFICE ADM - TRAINNING & EDUCATION	90.02.27	6	0	0	0	0	D	0
819	OFFICE ADM - RENTAL	90.02.30	6	0	0	0	0	D	0
820	OFFICE ADM - ELECTRICITY	90.02.39	6	0	0	0	0	D	0
821	OFFICE ADM - TELEPHONE	90.02.40	6	0	0	0	0	D	0
822	OFFICE ADM - WATER & MAINT  (Dharmala)	90.02.41	6	0	0	0	0	D	0
823	OFFICE ADM - UNIFORM & MEDICINE	90.02.45	6	0	0	0	0	D	0
824	OFFICE ADM - STATIONARY	90.02.46	6	0	0	0	0	D	0
825	OFFICE ADM - PHOTO COPY	90.02.47	6	0	0	0	0	D	0
826	OFFICE ADM - POSTAGE / COURIER	90.02.48	6	0	0	0	0	D	0
827	OFFICE ADM - PRINTING	90.02.49	6	0	0	0	0	D	0
828	OFFICE ADM - OUTSOURCHING	90.02.50	6	0	0	0	0	D	0
829	OFFICE ADM - OUTSOURCHING MANAGEMENT FEE	90.02.51	6	0	0	0	0	D	0
830	OFFICE ADM - DONATION	90.02.53	6	0	0	0	0	D	0
831	OFFICE ADM - PROFESIONAL FEE	90.02.55	6	0	0	0	0	D	0
832	OFFICE ADM - LAND & BUILDING TAX	90.02.59	6	0	0	0	0	D	0
833	OFFICE ADM - BUILDING MAINT.	90.02.62	6	0	0	0	0	D	0
834	OFFICE ADM - OFFICE EQUIPT MAINT.	90.02.63	6	0	0	0	0	D	0
835	OFFICE ADM - VEHICLE MAINT.	90.02.65	6	0	0	0	0	D	0
836	OFFICE ADM - REPAIR FEE	90.02.66	6	0	0	0	0	D	0
837	OFFICE ADM - BUILDING INSURANCE	90.02.71	6	0	0	0	0	D	0
838	OFFICE ADM - VEHICLE INSURANCE	90.02.72	6	0	0	0	0	D	0
839	OFFICE ADM - INTEREST BANK LOAN	90.02.75	6	0	0	0	0	D	0
840	OFFICE ADM - INTEREST LONG TERM	90.02.76	6	0	0	0	0	D	0
841	BANK EXP - TRANSFER FEE	90.02.80	6	0	0	0	0	D	0
842	BANK EXP - BANK CHARGE	90.02.81	6	0	0	0	0	D	0
843	OFFICE ADM - PPH 25	90.02.82	6	0	0	0	0	D	0
844	OFFICE ADM - PPN	90.02.83	6	0	0	0	0	D	0
845	OFFICE ADM - FINNA INSURANCE	90.02.84	6	0	0	0	0	D	0
846	OFFICE ADM - FINNA DEP.	90.02.85	6	0	0	0	0	D	0
847	OFFICE ADM - FINNA EXP / MEAL	90.02.86	6	0	0	0	0	D	0
848	OFFICE ADM - BUILDING DEP.	90.02.90	6	0	0	0	0	D	0
849	OFFICE ADM - PURI MATAHARI DEP.	90.02.91	6	0	0	0	0	D	0
850	OFFICE ADM - VEHICLE DEP.	90.02.94	6	0	0	0	0	D	0
851	OFFICE ADM - OFFICE EQUIPT DEP.	90.02.95	6	0	0	0	0	D	0
852	OFFICE ADM - OTHER	90.02.99	6	0	0	0	0	D	0
\.


                                                                                                                                                                                           3316.dat                                                                                            0000600 0004000 0002000 00000001355 13607356547 0014272 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        4	HARGA POKOK PENJUALAN	AKUN UNTUK HPP	61	500.01.01
11	HARGA POKOK PRODUKSI	AKUN UNTUK HARGA POKOK PRODUKSI	76	511.01.01
2	HUTANG PPN	AKUN UNTUK HUTANG PPN	23	117.02.01
3	HUTANG USAHA	AKUN UNTUK HUTANG USAHA	42	211.01.01
8	HUTANG USAHA IMPORT	AKUN UNTUK HUTANG USAHA IMPORT	43	211.02.01
9	PEMAKAIAN BAHAN BAKU	AKUN UNTUK PRODUKSI PEMAKAIAN BAHAN BAKU	63	510.02.01
12	PENJUALAN EXPORT	AKUN PENJUALAN EXPORT	59	411.01.01
7	PENJUALAN LOKAL	AKUN UNTUK PENJUALAN LOKAL	60	411.01.02
1	PERSEDIAAN BARANG	AKUN UNTUK PERSEDIAAN BARANG PADA COA	12	116.01.01
10	PERSEDIAAN BARANG DALAM PROSES	AKUN UNTUK PRODUKSI WIP	17	116.05.01
5	PERSEDIAAN BARANG JADI	AKUN UNTUK PERSEDIAAN BARANG JADI	18	116.06.01
6	PIUTANG USAHA	AKUN UNTUK PIUTANG USAHA	8	115.01.01
\.


                                                                                                                                                                                                                                                                                   3318.dat                                                                                            0000600 0004000 0002000 00000000072 13607356547 0014267 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        1	UNITED STATE	US	1
2	BRAZILIA	BZ	1
3	INDONESIA	ID	1
\.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                      3320.dat                                                                                            0000600 0004000 0002000 00000033023 13607356547 0014262 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        1	MARUFUJI KENZAI CO., LTD.	803-2 NISHINOYA KAZO CITY, SAITAMA, JAPAN 347-0117	1	2	0	720807089.3	0	0	1	0	0	0	0	0	0
2	TOPPAN PRINTING CO., LTD.	1-5-1 TAITO, TAITO-KU, TOKYO 110-8560, JAPAN	1	2	0	97487816	0	0	1	0	0	0	0	0	0
3	FUKUMOTO CO., LTD.	NO. 2-15, I-CHOME, NISHIKI NAKA-KU, NAGOYA JAPAN	1	2	0	51117400	0	0	1	0	0	0	0	0	0
4	DAIKEN CORPORATION	NO. 8 TEMASEK BOULEVARD, #43-01A, SUNTEC CITY TOWER 3, SINGAPORE 038988	1	1	0	-3804391.82	0	0	1	0	0	0	0	0	0
5	KOIKE IMATECS CORP.	KOIKE BUILDING 5F, 8-7, 1-CHOME, HIRANOMACH CHUO-KU, OSAKA, JAPAN	1	2	0	624630945	0	0	1	0	0	0	0	0	0
6	SUBUR TIASA PARTICLEBOARD SDN BHD (198523-K)	NO. 66-78 PUSAT SURIA PERMATA, JALAN UPPER LANANG, S.D.T. NO. 123, 96000 SIBU, SARAWAK, MALAYSIA	1	1	0	0	0	0	1	0	0	0	0	0	0
7	TOKYO KOHSAKUSHO CO., LTD.	1-1-14, TABATA-SHINMACHI, KITA-KU, TOKYO, 1140012 JAPAN	1	2	0	289924183	0	0	1	0	0	0	0	0	0
8	SOMPO INSURANCE INDONESIA, PT	MAYAPADA COMPLEX, 8TH FLOOR UNIT 01, JL. MAYJEN SUNGKONO NO. 178, SURABAYA 60225	1	2	0	0	0	0	1	0	0	0	0	0	0
9	FUKAI MUSEN KOGYO CO., LTD.	1719 NAGAOKA, SHINTOU-MURA, KITAGUNMA-GUN, GUNMA, JAPAN	1	2	0	0	0	0	1	0	0	0	0	0	0
10	BURKLE MACHINERY CO., LTD.	BUILDING 5 & 6, NO. 2715, LEDU ROAD (W), SONGJIANG DISTRICT, 201614 SHANGHAI	1	1	0	0	0	0	1	0	0	0	0	0	0
11	FOSHAN TIANZHIBAO PLASTICS CO., LTD.	NO. 15, 7TH ROAD AREA EAST, LIANHE INDUSTRIAL PARK, LUOCUN, NANHAI DISTRICT, FOSHAN CITY, PR CHINA 528226	1	1	0	2862913.4	0	0	1	0	0	0	0	0	0
12	LEUCO	LOT 4213, JALAN TTC 30, TAMAN TEKNOLOGI CHENG, 75250 MELAKA, MALAYSIA	1	1	0	163342816	0	0	1	0	0	0	0	0	0
13	LIANYUNGGANG		1	1	0	616234	0	0	1	0	0	0	0	0	0
14	AGUNG TEKNIK	JL. RAYA JEMURSARI 9, SURABAYA	1	0	0	0	0	0	1	0	0	0	0	0	0
15	AMANO INDONESIA, PT	JL. HR RASUNA SAID KAV. C-22, GEDUNG PPF HAJI USMAR NO ISMAIL LT. 3, KUNINGAN, JAKARTA SELATAN-12940	1	0	0	0	0	0	1	0	0	0	0	0	0
16	ANUGERAH BERSAMA SAPUTRA, PT	JL. TAMBAK DALAM BARU BLOK III NO. 27 RT.03, ASEMROWO, SURABAYA	1	0	0	0	0	0	1	0	0	0	0	0	0
17	ANUGERAH PRIMA GEMILANG, CV	JL. DUKUH KUPANG BARAT XXVIII/21, DUKUH KUPANG, SURABAYA	1	0	80927700	0	0	0	1	0	0	0	0	0	0
18	ANUGERAH UTAMA	SURABAYA	1	0	3900000	0	0	0	1	0	0	0	0	0	0
19	ARTAMAS JAYA	JL. RAYA SEMOLOWARU 80, SURABAYA	1	0	0	0	0	0	1	0	0	0	0	0	0
20	ASRI, CV (ASRI MOTOR)	JL. JENGGOLO NO. 52 RT.026 RW.006, SIDOARJO-61219	1	0	0	0	0	0	1	0	0	0	0	0	0
21	BERDIRI MATAHARI LOGISTIK, PT	JL. JEND. GATOT SUBROTO KAV. 74-75 GD. GRAHA MUSTIKA RATU LT. 4, JAKARTA SELATAN-12870	1	0	0	0	0	0	1	0	0	0	0	0	0
22	BERNANTA JAYA	SURABAYA	1	0	0	0	0	0	1	0	0	0	0	0	0
23	BERRY TAPE INDONESIA, PT	JL. RAYA WEDI KOMP. GDG SINAR GEDANGAN D/02, GEMURUNG, SIDOARJO	1	0	5536800	0	0	0	1	0	0	0	0	0	0
24	BINA UTAMA SAKTI, PT	JL. KARTINI GG. MASJID KEC. PUNGGING KAB. MOJOKERTO-61384	1	0	0	0	0	0	1	0	0	0	0	0	0
25	BINTANG BAROKAH PERDANA, PT	JL. RAYA MOJOSARI DS. MOJOTAMPING, KEC. BANGSAL, MOJOKERTO	1	0	0	0	0	0	1	0	0	0	0	0	0
26	BINTANG MAS GLASSOLUTIONS, PT	JL. YOS SUDARSO KM 1,5, BEDALI, LAWANG, MALANG	1	0	97167000	0	0	0	1	0	0	0	0	0	0
27	BIOTECH INDO GEMILANG, PT	JL. PESANGGRAHAN RAYA NO. 54 D/E, KEMBANGAN SELATAN, JAKARTA BARAT	1	0	7800000	0	0	0	1	0	0	0	0	0	0
28	BIROTIKA SEMESTA, PT (DHL)	MULIA BUSINESS PARK BLD F, JL. MT. HARYONO KAV. 58-60, PANCORAN, JAKARTA SELATAN	1	0	0	0	0	0	1	0	0	0	0	0	0
29	BRATACO, PT	JL. KELENTENG NO. 8, KEBON JERUK, BANDUNG	1	0	0	0	0	0	1	0	0	0	0	0	0
30	CAHAYA MAS MAKMUR, PT	JL. RAYA JETIS, JETIS, KAB. MOJOKERTO	1	0	0	0	0	0	1	0	0	0	0	0	0
31	CAHAYA TERANG	SURABAYA	1	0	375000	0	0	0	1	0	0	0	0	0	0
32	CALTESYS INDONESIA, PT	RUKAN PURI NIAGA II JL. PURI KENCANA BLOK J1/2-V, KEMBANGAN SELATAN, JAKARTA BARAT-11610	1	0	0	0	0	0	1	0	0	0	0	0	0
33	CASA TERRA MANDIRI INDONESIA, PT	JL. PEGANGSAAN DUA NO. 3, GADING NIAS RESIDENCES BLOK DAHLIA UNIT KM/GF/MD 18 RT.002 RW.003,PEGANGSAAN DUA, JAKARTA UTARA	1	0	0	0	0	0	1	0	0	0	0	0	0
34	CIPTA KARYA SENTOSA, CV	MIRAH DELIMA BLOK 12C/B NO. 29 RT.026 RW,006, GADUNG, GRESIK	1	0	0	0	0	0	1	0	0	0	0	0	0
35	CIPTA SEJAHTERA TEKNIK, CV	JL. RAYA JEMURSARI NO. 9 RT.003 RW.008, JEMUR WONOSARI, SURABAYA	1	0	253000	0	0	0	1	0	0	0	0	0	0
36	DELTA LISTRIK	SURABAYA	1	0	1625000	0	0	0	1	0	0	0	0	0	0
37	DENKO WAHANA SAKTI, PT	KOMP. DUTA MERLIN BLOK C. 1-3, JL. GAJAH MADA 3-5, PETOJO UTARA, JAKARTA PUSAT	1	0	0	0	0	0	1	0	0	0	0	0	0
38	DITOSA, PT	JL. BATU CEPER 34, KEBON KELAPA, JAKARTA PUSAT-10120	1	0	0	0	0	0	1	0	0	0	0	0	0
39	DWI MAJU, PT	ROA MALAKA UTARA NO. 14, RT.006/003, TAMBORA, JAKARTA BARAT-11230	1	0	7200000	0	0	0	1	0	0	0	0	0	0
40	EKADHARMA INTERNATIONAL, PT	Pergudangan Meiko Abadi Blok I C10 Gedangan	1	0	6179500	0	0	0	1	0	0	0	0	0	0
41	EKAMANT INDONESIA, PT	JL. PULOBUARAN IV BPSP BLOK W NO. 3 KIP PULOGADUNG, JAKARTA TIMUR	1	0	0	0	0	0	1	0	0	0	0	0	0
42	GAMMA MICROTECH COMPUTER	JL. KERTAJAYA 76, SURABAYA	1	0	0	0	0	0	1	0	0	0	0	0	0
43	GYUNG DO INDONESIA, PT	KAWASAN INDUSTRI NGORO KAV. H NO. 03/A, NGORO, MOJOKERTO	1	0	0	0	0	0	1	0	0	0	0	0	0
44	HAFELE INDOTAMA, PT	TAMAN TEKNO BLOK A NO. 3, SETU-SERPONG, TANGERANG SELATAN-15314	1	0	9300000	0	0	0	1	0	0	0	0	0	0
45	HAKAMATA UTAMA, PT	NGORO INDUSTRI PERSADA BLOK N-1, NGORO, MOJOKERTO	1	0	17493550	0	0	0	1	0	0	0	0	0	0
46	HASIL, TOKO	SURABAYA	1	0	61172	0	0	0	1	0	0	0	0	0	0
47	HEISEI STAINLESS STEEL INDUSTRY, PT	DS. BANJAR KEMANTREN, KEC. BUDURAN, SIDOARJO-61252	1	0	0	0	0	0	1	0	0	0	0	0	0
48	HENKEL ADHESIVE TECHNOLOGIES, PT	NISSI BINTARO CAMPUS LT.3 JL. TEGAL ROTAN RAYA NO. 78, RT.002/RW,008, SAWAH BARU, CIPUTAT 15413,TANGERANG SELATAN, BANTEN	1	0	64413468	0	0	0	1	0	0	0	0	0	0
49	HENKEL INDONESIEN, PT	NISSI BINTARO CAMPUS LT.3 JL. TEGAL ROTAN RAYA NO. 78, RT.002/RW,008, SAWAH BARU, CIPUTAT 15413,TANGERANG SELATAN, BANTEN	1	0	48250856	0	0	0	1	0	0	0	0	0	0
50	HERRY & CO, CV	JL. RUNGKUT INDUSTRI III/28, KALIRUNGKUT, SURABAYA	1	0	0	0	0	0	1	0	0	0	0	0	0
51	HURI COMPUTER	JL. RAYA NGORO 191, NGORO	1	0	2541750	0	0	0	1	0	0	0	0	0	0
52	INDOCHEMICAL CITRA KIMIA, PT	GRAHA INDCHEM LT.9, JL. PANTAI INDAH KAPUK BOULEVARD KAV. SSB/E RT.004 RW.003, JAKARTA UTARA-14470	1	0	9600000	0	0	0	1	0	0	0	0	0	0
53	INTIMIKRO NIAGATAMA	SURABAYA	1	0	1100000	0	0	0	1	0	0	0	0	0	0
54	IRON BIRD TRANSPORT, PT	JL. DARMO KALI 2-4-6, KEPUTRAN, SURABAYA	1	0	0	0	0	0	1	0	0	0	0	0	0
55	JATMIKO CIPTA KARYA, PT	JL. RADEN SALEH 32-32 I, BUBUTAN, SURABAYA	1	0	9346000	0	0	0	1	0	0	0	0	0	0
56	JAYA, TOKO	SURABAYA	1	0	1800000	0	0	0	1	0	0	0	0	0	0
57	KALI JATI	SURABAYA	1	0	7425000	0	0	0	1	0	0	0	0	0	0
58	KANEFUSA INDONESIA, PT	EJIP INDUSTRIAL PARK PLOT 8 D, CIKARANG SELATAN, BEKASI-17530	1	0	994400	0	0	0	1	0	0	0	0	0	0
59	KEMASAN CIPTATAMA SEMPURNA, PT	DESA RANDUPITU, RANDUPITU, KEC. GEMPOL, PASURUAN-67155	1	0	35537445	0	0	0	1	0	0	0	0	0	0
60	Kutai Timber Indonesia, PT		1	0	21344720	0	0	0	1	0	0	0	0	0	0
61	LEMINDO ABADI JAYA, PT	KP. CIKUDA RT.001 RW.005, WANAHERANG, GUNUNGPUTRI, BOGOR-16965	1	0	40680800	0	0	0	1	0	0	0	0	0	0
62	LIMALIMA DINAMIKA, PT	KOMPLEK SEMUT INDAH B/16, BONGKARAN, SURABAYA	1	0	2009700	0	0	0	1	0	0	0	0	0	0
63	MAKMUR MITRA ABADI, CV	DS. GADING RT. 002 RW.001, KAB. SIDOARJO	1	0	0	0	0	0	1	0	0	0	0	0	0
64	Matratama Manunggal Jaya, PT	JL. Raya Secang Semarang Ds. Kaliampo Pringsurat Temanggung	1	0	78221190	0	0	0	1	0	0	0	0	0	0
65	MAYATAMA MANUNGGAL SENTOSA, PT	JL. PERUSAHAAN NO. 35, BANJARARUM, MALANG	1	0	0	0	0	0	1	0	0	0	0	0	0
66	MEGA WAJA CORPORINDO, PT	PLUIT TIMUR BLOK L- BARAT NO. 4, JAKARTA	1	0	0	0	0	0	1	0	0	0	0	0	0
67	MEIWA INDONESIA, PT.	JALAN RAYA BOGOR KM.30, SUKAMAJU BARU, CIMANGGIS, DEPOK 16951	1	0	73106780	0	0	0	1	0	0	0	0	0	0
68	MENARA CIPTA INDONESIA, PT	BRATANG GEDE VI-E/64, NGAGEL REJO, SURABAYA	1	0	19647900	0	0	0	1	0	0	0	0	0	0
69	MENTARI LIMA	SURABAYA	1	0	1040000	0	0	0	1	0	0	0	0	0	0
70	MIRA MAKMUR	SURABAYA	1	0	2079682	0	0	0	1	0	0	0	0	0	0
71	Mitra Perkakas  Indonesia, PT	Jl. Semarang No. 118 B16	1	0	40761357	0	0	0	1	0	0	0	0	0	0
72	MOLINDO RAYA INDUSTRIAL, PT	JL. SUMBER WARAS NO. 255 RT.001 RW.008, KAB MALANG	1	0	0	0	0	0	1	0	0	0	0	0	0
73	MULTI ARTHAMAS GLASS INDUSTRY, PT	JL. KEDUNG BARUK TENGAH NO. 8, KEDUNG BARUK, SURABAYA	1	0	82730931	0	0	0	1	0	0	0	0	0	0
74	MUSTIKA BAHANA JAYA, PT	JL. RAYA LUMAJANG TEMPEH KM.7, DESA BESUK, RT.017/RW.003, LUMAJANG	1	0	133956000	0	0	0	1	0	0	0	0	0	0
75	NACHINDO TAPE INDUSTRY, PT	DESA BETRO, KEC. SEDATI, SIDOARJO	1	0	10116000	0	0	0	1	0	0	0	0	0	0
76	NITTO ALAM INDONESIA, PT	JL. MANIS II, KAWASAN INDUSTRI MANIS, KAB. TANGERANG, BANTEN-15810	1	0	0	0	0	0	1	0	0	0	0	0	0
77	PANDU SIWI SENTOSA, PT	JL. RAYA BEKASI TIMUR KM.18 NO. 30, JATINEGARA, JAKARTA TIMUR	1	0	0	0	0	0	1	0	0	0	0	0	0
78	POLYCHEMIE ASIA PACIFIC PERMAI, PT	JL. RAYA KEBAYORAN LAMA NO. 15 RT.002 RW.010, GROGOL SELATAN, JAKARTA SELATAN	1	0	18292313	0	0	0	1	0	0	0	0	0	0
79	POWER PACK INDUTRIAL SOLUTION, PT	JL. RAYA SUMENGKO KM. 30,6, KAB. GRESIK	1	0	3800000	0	0	0	1	0	0	0	0	0	0
80	PRATAMA SUMBER MILINDO	NGORO INDUSTRI PERSADA  NGORO, MOJOKERTO	1	0	731500	0	0	0	1	0	0	0	0	0	0
81	PRIMA, CV	PB SUDIRMAN 67, JAGALAN, MOJOKERTO	1	0	0	0	0	0	1	0	0	0	0	0	0
82	PRIMABOX ADIPERKASA, PT	JAKSA AGUNG SUPRAPTO, DUSUN KEMISIK, DESA SUMBER GEDANG, PANDAAN, PASURUAN-67156	1	0	80112925	0	0	0	1	0	0	0	0	0	0
83	RIMBA PARTIKEL INDONESIA, PT	DESA MOROREJO, KALIWUNGU, KENDAL	1	0	375287810	0	0	0	1	0	0	0	0	0	0
84	Ronadamar Sejahtera, PT	JL. Kedung baruk Tengah No.2	1	0	11746699	0	0	0	1	0	0	0	0	0	0
85	RUKUN SENTOSA ABADI, PT	DSN. GADUNGAN TIMUR RT.01/RW.03, DS GADUNGAN KEC PUNCU, PARE, KEDIRI	1	0	22761040	0	0	0	1	0	0	0	0	0	0
86	Saga Teknindo Sejati, PT	JL. Raya Serang Cibarusah,Kp Pagaulan RT.010/RW.02 Ds.Sukaresmi 	1	0	424000	0	0	0	1	0	0	0	0	0	0
87	SAMUDRA ARUNG NUSANTARA	SURABAYA	1	0	9419000	0	0	0	1	0	0	0	0	0	0
88	SARANA MANDIRITAMA SEJAHTERA, PT	JEMURSARI SELATAN BLOK IV NO. 2D, RT,001 RW.008, SURABAYA	1	0	0	0	0	0	1	0	0	0	0	0	0
89	SARANA MENTARI CEMERLANG, PT	JL. RADEN SALEH BLOK 41 KAV. 15, SURABAYA	1	0	8534064	0	0	0	1	0	0	0	0	0	0
90	SARI LISTRIK	Mangkuluhur City Tower One 8th Floor JL. Jend Gatot Subroto Kav1-3 	1	0	3565000	0	0	0	1	0	0	0	0	0	0
91	SECOM INDONESIA, PT	GEDUNG MANGKULUHUR CITY TOWER ONE LT. 8 JL. JEND. GATOT SUBROTO KAV 1-3 RT.003 RW.004, JAKARTA SELATAN-12930	1	0	0	0	0	0	1	0	0	0	0	0	0
92	SENTRAL KONEKSI INTERNASIONAL, PT	JL. MAYJEND SUNGKONO 142-143 DARMO GALERIA CENTER D-18, SURABAYA-60224	1	0	4688200	0	0	0	1	0	0	0	0	0	0
93	SINAR ABADI	SIDOARJO	1	0	0	0	0	0	1	0	0	0	0	0	0
94	Sinar Aneka Pack, CV	Griya Taman Cipta Karya Jl. Gading No.228 Bohar Taman	1	0	2121600	0	0	0	1	0	0	0	0	0	0
95	SINAR BACAN KHATULISTIWA, PT	JL. EMBONG MALANG NO. 71-E LT.2 RT.001 RW.008, SURABAYA	1	0	0	0	0	0	1	0	0	0	0	0	0
96	SINAR ERA BOX, PT	SIMOKERTO 11 RT.007 RW.006, SIMOKERTO, SURABAYA	1	0	67880761	0	0	0	1	0	0	0	0	0	0
97	SINAR MAKMUR, CV	MULYOSARI PRIMA MS-12-A/26 RT.005 RW.007, KALISARI, SURABAYA	1	0	0	0	0	0	1	0	0	0	0	0	0
98	SINAR SURYA	SURABAYA	1	0	120000	0	0	0	1	0	0	0	0	0	0
99	Somagede Indonesia, PT	Kawasan Industri Sirie JL. Lingkar Timur KM.5,5 Blok F25-26	1	0	1080000	0	0	0	1	0	0	0	0	0	0
100	SPARTA PRIMA, PT	TAMAN SARI VI/NO. 54 RT.014/007, TAMANSARI, JAKARTA BARAT	1	0	17706750	0	0	0	1	0	0	0	0	0	0
101	SUKSES WIJAYA PANEL, PT	MARGOMULYO 44 BLOK DD NO. 16-A, RT.001 RW.001, SURABAYA	1	0	0	0	0	0	1	0	0	0	0	0	0
102	SUMBER SELAMAT, PT	KRAMAT GANTUNG 75, SURABAYA	1	0	18394834	0	0	0	1	0	0	0	0	0	0
103	SURYA ALAM CEMERLANG, CV	PURI INDAH BLOK P-16, SUKO, SIDOARJO	1	0	0	0	0	0	1	0	0	0	0	0	0
104	SURYA CITRA UTAMA MANDIRI, PT	R.A. BASUNI NO. 142 RT.001 RW.006, SOOKO, KAB. MOJOKERTO	1	0	10880000	0	0	0	1	0	0	0	0	0	0
105	SURYA MANDIRI	SURABAYA	1	0	1800000	0	0	0	1	0	0	0	0	0	0
106	SURYA MASA MANDIRI	SURABAYA	1	0	25748231	0	0	0	1	0	0	0	0	0	0
107	SURYA PRATAMA, CV	DUSUN PANJER RT.004 RW.003, TUNGGALPAGER, KAB. MOJOKERTO	1	0	15019788	0	0	0	1	0	0	0	0	0	0
108	TITUS TEKFORM INDONESIA, PT	TAMAN TEKNO BSD SEKTOR XI BLOK L-2 NO. 07, KOTA TANGERANG SELATAN-15314	1	0	22490000	0	0	0	1	0	0	0	0	0	0
109	TNT SKYPAK INTERNATIONAL EXPRESS, PT	GD. SUMMITMAS I LT.21 JL. JEND. SUDIRMAN KAV.61-62, JAKARTA SELATAN	1	0	0	0	0	0	1	0	0	0	0	0	0
110	TOYOMATSU, PT	JL. RADEN SALEH 4-6 KAV. A-5, BUBUTAN, SURABAYA	1	0	4987950	0	0	0	1	0	0	0	0	0	0
111	TRUSTINDO PRIMA KARYA, PT	JL. KUSUMA BANGSA NO. 80, GED. DIKLAT APHI, SAMARIDA 75121	1	0	0	0	0	0	1	0	0	0	0	0	0
112	USTEGRA, PT	JL. MERPATI RAYA NO. 22 RT.004/03, SAWAH, TANGERANG SELATAN	1	0	0	0	0	0	1	0	0	0	0	0	0
113	WIRATAMA SUKSES SEJAHTERA ABADI, PT	JL. SEMERU NO. 95 RT.007 RW.002, BAMBE, GRESIK	1	0	0	0	0	0	1	0	0	0	0	0	0
114	YAN JIN INDONESIA, PT	EJIP INDUSTRIAL PARK PLOT 8H, CIBATU, CIKARANG SELATAN, BEKASI-17550	1	0	38022000	0	0	0	1	0	0	0	0	0	0
115	YUSEN LOGISTICS INDONESIA, PT	KAWASAN INDUSTRI MM2100 BLOK EE-4, BEKASI-17520	1	0	0	0	0	0	1	0	0	0	0	0	0
116	Z EXPRESS/MASTRA GOLDEN INTERNASIONAL, PT	JL. NGAGEL JAYA SELATAN II/12, SURABAYA-60284	1	0	0	0	0	0	1	0	0	0	0	0	0
117	MARUFUJI KENZAI CO., LTD.	803-2 NISHINOYA KAZO CITY, SAITAMA, JAPAN 347-0117	2	2	0	0	0	6320473591	1	0	0	0	0	0	0
118	KYUSHU MARUFUJI KENZAI CO., LTD.	NO. 2208-3, OH AZA HAJI, KEISEN-MACHI, KAHO GUN FUKUOKA 820-06	2	2	0	0	0	6971969334	1	0	0	0	0	0	0
119	TOPPAN PRINTING CO., LTD.	1-5-1 TAITO, TAITO-KU, TOKYO 110-8560, JAPAN	2	2	0	0	0	0	1	0	0	0	0	0	0
120	TENMA CORPORATION	1-63-6 AKABANE KITAKU, TOKYO, JAPAN 115-0045	2	2	0	0	0	2298087644	1	0	0	0	0	0	0
121	MARUNI WOOD INDUSTRY INC.	24 SHIRASAGO YUKI-CHO, SAIKI-KU, HIROSHIMA-SHI 738-0512, JAPAN	2	2	0	0	0	0	1	0	0	0	0	0	0
122	ITOKI CORPORATION	21-1, KITASHINJYUKU 2-CHOME, SHINJYUKU-KU, TOKYO, JAPAN 169-0074	2	2	0	0	0	0	1	0	0	0	0	0	0
123	KOIKE IMATECS	8.7, 1-CHOME, HIRANOMACHI CHIOKA-KU, OSAKA,JAPAN	2	2	0	0	0	13264700	1	0	0	0	0	0	0
125	SUSETA DAIKEN INDONESIA, PT	RUKO BANGUNAN BLOK F7/1-2, JL. MANGGA DUA RAYA, KEL. MANGGA DUA SELATAN, KEC. SAWAH BESAR, JAKARTA PUSAT, DKI JAKARTA RAYA	2	0	0	0	899240877	0	1	0	0	0	0	0	0
126	DAIKEN DHARMA INDONESIA, PT	JL. KALIANAK NO. 55 L, KEL. KALIANAK, KEC. ASEMROWO, SURABAYA, JAWA TIMUR	2	0	0	0	0	0	1	0	0	0	0	0	0
127	DHARMA SUMBER NUSANTARA, PT	GD. SAPTA MULIA CENTER LT. 3, JL. RAYA GELAM V KAVELING OR NO. 3B, KAWASAN INDUSTRI PULO GADUNG, KOTA ADM JAKARTA TIMUR, DKI JAKARTA	2	0	0	0	0	0	1	0	0	0	0	0	0
\.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             3322.dat                                                                                            0000600 0004000 0002000 00000000005 13607356547 0014256 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        \.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           3324.dat                                                                                            0000600 0004000 0002000 00000000005 13607356547 0014260 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        \.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           3326.dat                                                                                            0000600 0004000 0002000 00000000005 13607356547 0014262 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        \.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           3328.dat                                                                                            0000600 0004000 0002000 00000000431 13607356547 0014267 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        3	GUDANG BARANG DALAM PROSES	UNTUK STOCK WIP	1
2	GUDANG BARANG JADI	UNTUK STOCK BARANG JADI	1
1	GUDANG BAHAN BAKU	UNTUK STOCK BAHAN BAKU	1
4	GUDANG PENERIMAAN	UNTUK AWAL PENERIMAAN BARANG	1
5	GUDANG SUBKONTRAK	UNTUK PROSES SUBKON	1
6	GUDANG BARANG MODAL	GUDANG BARANG MODAL	1
\.


                                                                                                                                                                                                                                       3329.dat                                                                                            0000600 0004000 0002000 00000000005 13607356547 0014265 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        \.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           3334.dat                                                                                            0000600 0004000 0002000 00000000005 13607356547 0014261 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        \.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           3336.dat                                                                                            0000600 0004000 0002000 00000000005 13607356547 0014263 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        \.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           3338.dat                                                                                            0000600 0004000 0002000 00000000005 13607356547 0014265 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        \.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           3340.dat                                                                                            0000600 0004000 0002000 00000000005 13607356547 0014256 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        \.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           3342.dat                                                                                            0000600 0004000 0002000 00001601134 13607356547 0014273 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        1	  HARPLEX  9x1200x2400 , TRIARTA A                                           	 2H1-1            	7	84636320	  HARPLEX  9x1200x2400 , TRIARTA A                                           	1	1	 2H1-1            	27	1
2	  PB 9x1220x2440 S.E0, Rimba P                                               	 2P20-1           	1	75988569	  PB 9x1220x2440 S.E0, Rimba P                                               	1	1	 2P20-1           	27	1
3	  PB 9x1220x2440 S.E0, SUBUR TIASA                                           	 2P20-2           	122	288225038	  PB 9x1220x2440 S.E0, SUBUR TIASA                                           	1	1	 2P20-2           	27	1
4	  PB 9x1220x2740 S.E0, Rimba P                                               	 2P21-1           	381	0	  PB 9x1220x2740 S.E0, Rimba P                                               	1	1	 2P21-1           	27	1
5	  PB 9x1220x2440 E2A, Rimba P                                                	 2P22-1           	638	0	  PB 9x1220x2440 E2A, Rimba P                                                	1	1	 2P22-1           	27	1
6	  PB 12x1220x2440 E2A, Rimba P                                               	 2P25-1           	73	178852553	  PB 12x1220x2440 E2A, Rimba P                                               	1	1	 2P25-1           	27	1
7	  PB 12x1220x2740 E2, RIMBA PARTICEL I                                       	 2P48-1           	20	0	  PB 12x1220x2740 E2, RIMBA PARTICEL I                                       	1	1	 2P48-1           	27	1
8	  PB 12x1220x2740 S.E0, Rimba P                                              	 2P26-1           	5	120717	  PB 12x1220x2740 S.E0, Rimba P                                              	1	1	 2P26-1           	27	1
9	  PB 12x1220x2440 S.E0 A, Rimba P                                            	 2P23-1           	400	33314402	  PB 12x1220x2440 S.E0 A, Rimba P                                            	1	1	 2P23-1           	27	1
10	  PB 12x1220x2440 S.E0, SUBUR TIASA                                          	 2P23-2           	765	0	  PB 12x1220x2440 S.E0, SUBUR TIASA                                          	1	1	 2P23-2           	27	1
11	  PB 15x1220x2440 S.E0 A, Rimba P                                            	 2P28-1           	508	37588750	  PB 15x1220x2440 S.E0 A, Rimba P                                            	1	1	 2P28-1           	27	1
12	  PB 15x1220x2440 S.E0, SUBUR TIASA                                          	 2P28-2           	2171	0	  PB 15x1220x2440 S.E0, SUBUR TIASA                                          	1	1	 2P28-2           	27	1
13	  PB 15x1220x2440 S.E0 AE9 MP, PANEL PLUS Co.                                	 2P28-5           	1	0	  PB 15x1220x2440 S.E0 AE9 MP, PANEL PLUS Co.                                	1	1	 2P28-5           	27	1
14	  PB 15x1230x2450 S.E0 SIAMRISO, ITOCHU SINGAPURE                            	 2P29-1           	2	0	  PB 15x1230x2450 S.E0 SIAMRISO, ITOCHU SINGAPURE                            	1	1	 2P29-1           	27	1
15	  PB 15x1220x2440 E2 A, Rimba P                                              	 2P32-3           	1662	27418364	  PB 15x1220x2440 E2 A, Rimba P                                              	1	1	 2P32-3           	27	1
16	  PB 16x1220x2440 S.E0 A, Rimba P                                            	 2P34-1           	18	1587600	  PB 16x1220x2440 S.E0 A, Rimba P                                            	1	1	 2P34-1           	27	1
17	  PB 18x1220x1830 E2 A, Rimba P                                              	 2P61-1           	1	0	  PB 18x1220x1830 E2 A, Rimba P                                              	1	1	 2P61-1           	27	1
18	  PB 18x1220x2440 E2 A, Rimba P                                              	 2P36-1           	258	414930	  PB 18x1220x2440 E2 A, Rimba P                                              	1	1	 2P36-1           	27	1
19	  PB 18x1220x2740 S.E0 A, Rimba P                                            	 2P39-1           	72	666234	  PB 18x1220x2740 S.E0 A, Rimba P                                            	1	1	 2P39-1           	27	1
20	  PB 20x1220x2440 E2 A, Rimba P                                              	 2P40-1           	245	0	  PB 20x1220x2440 E2 A, Rimba P                                              	1	1	 2P40-1           	27	1
21	  PB 20x1220x2440 S.E0, Rimba P                                              	 2P51-1           	51	235387	  PB 20x1220x2440 S.E0, Rimba P                                              	1	1	 2P51-1           	27	1
22	  PB 25x1220x2440 S.E0, Rimba P                                              	 2P43-1           	89	118643	  PB 25x1220x2440 S.E0, Rimba P                                              	1	1	 2P43-1           	27	1
23	  PB 25x1220x2440 Non Standart NS, Rimba P                                   	 2P44-1           	203	8833896	  PB 25x1220x2440 Non Standart NS, Rimba P                                   	1	1	 2P44-1           	27	1
24	  PB 25x1220x2440 E2, Rimba P                                                	 2P45-1           	140	2008570	  PB 25x1220x2440 E2, Rimba P                                                	1	1	 2P45-1           	27	1
25	  PINE WOOD FJLB 15x500x2100MM, MUSTIKA BAHANA JAYA                          	 2P69-1           	8	2849301	  PINE WOOD FJLB 15x500x2100MM, MUSTIKA BAHANA JAYA                          	1	1	 2P69-1           	27	1
26	  PINE WOOD 24x1220x2440 FINGER JOINT, Mustika Bahana Jaya                   	 2P46-1           	43	1546726	  PINE WOOD 24x1220x2440 FINGER JOINT, Mustika Bahana Jaya                   	1	1	 2P46-1           	27	1
27	  RUBBER GRADE AB FINGER JOINT 15x80x2050MM, PT. ARIA A. A.                  	 2R15-1           	12	159335	  RUBBER GRADE AB FINGER JOINT 15x80x2050MM, PT. ARIA A. A.                  	1	1	 2R15-1           	27	1
28	  RUBBER GRADE AB FINGER JOINT 15x110x2050MM, PT. ARIA A. A.                 	 2R15-2           	14	483441	  RUBBER GRADE AB FINGER JOINT 15x110x2050MM, PT. ARIA A. A.                 	1	1	 2R15-2           	27	1
29	  RUBBERWOOD 17x1220x2430 GRADE:MT, Mustika BJ                               	 2R1-1            	2	306084	  RUBBERWOOD 17x1220x2430 GRADE:MT, Mustika BJ                               	1	1	 2R1-1            	27	1
30	  RUBBERWOOD FJLB 24x65x2100, Mustika BJ                                     	 2R14-5           	6	531357	  RUBBERWOOD FJLB 24x65x2100, Mustika BJ                                     	1	1	 2R14-5           	27	1
31	  RUBBERWOOD FJLB 24x560x737, Mustika BJ                                     	 2R14-3           	1	401628	  RUBBERWOOD FJLB 24x560x737, Mustika BJ                                     	1	1	 2R14-3           	27	1
32	  RUBBERWOOD FJLB 24x560x1120, Mustika BJ                                    	 2R14-8           	49	128597760	  RUBBERWOOD FJLB 24x560x1120, Mustika BJ                                    	1	1	 2R14-8           	27	1
33	  RUBBERWOOD FJLB 24x560x1560, Mustika BJ                                    	 2R14-9           	8	32197244	  RUBBERWOOD FJLB 24x560x1560, Mustika BJ                                    	1	1	 2R14-9           	27	1
34	  RUBBERWOOD FJLB 24x1180x2100, Mustika BJ                                   	 2R14-7           	4	0	  RUBBERWOOD FJLB 24x1180x2100, Mustika BJ                                   	1	1	 2R14-7           	27	1
35	  RUBBERWOOD FJLB 24x1180x2280, Mustika BJ                                   	 2R14-6           	2	0	  RUBBERWOOD FJLB 24x1180x2280, Mustika BJ                                   	1	1	 2R14-6           	27	1
36	  RUBBER WOOD GRADE A 25x220x570MM, PT. ARIA A. A.                           	 2R13-1           	4	0	  RUBBER WOOD GRADE A 25x220x570MM, PT. ARIA A. A.                           	1	1	 2R13-1           	27	1
37	  RUBBER WOOD GRADE A 25x380x570MM, PT. ARIA A. A.                           	 2R13-2           	7	0	  RUBBER WOOD GRADE A 25x380x570MM, PT. ARIA A. A.                           	1	1	 2R13-2           	27	1
38	  RUBBER WOOD GRADE A 25x565x570MM, PT. ARIA A. A.                           	 2R13-4           	3	0	  RUBBER WOOD GRADE A 25x565x570MM, PT. ARIA A. A.                           	1	1	 2R13-4           	27	1
39	  RUBBER WOOD GRADE A 25x580x570MM, PT. ARIA A. A.                           	 2R13-5           	4	0	  RUBBER WOOD GRADE A 25x580x570MM, PT. ARIA A. A.                           	1	1	 2R13-5           	27	1
40	  RUBBER WOOD GRADE A 25x745x570MM, PT. ARIA A. A.                           	 2R13-3           	3	0	  RUBBER WOOD GRADE A 25x745x570MM, PT. ARIA A. A.                           	1	1	 2R13-3           	27	1
41	  RUBBERWOOD 25x1220x2440, Mustika B.J                                       	 2R3-1            	192	0	  RUBBERWOOD 25x1220x2440, Mustika B.J                                       	1	1	 2R3-1            	27	1
42	  RUBBERWOOD AA 36x1220x2440, Mustika B.J                                    	 2R12-2           	26	0	  RUBBERWOOD AA 36x1220x2440, Mustika B.J                                    	1	1	 2R12-2           	27	1
43	  VENEER 0.5x50x2440,  Anugerah KA                                           	 2V1-1            	13	0	  VENEER 0.5x50x2440,  Anugerah KA                                           	1	1	 2V1-1            	27	1
44	  Barcore Sengon 11.7x1200x2250, Mustika BS                                  	 2B2-1            	81	92800	  Barcore Sengon 11.7x1200x2250, Mustika BS                                  	1	1	 2B2-1            	27	1
45	  Barcore Sengon 13x1260x2480, AMIWOOD I                                     	 2B8-1            	2	790800	  Barcore Sengon 13x1260x2480, AMIWOOD I                                     	1	1	 2B8-1            	27	1
46	  Barcore T2F3STAR 15x1260x2490, KTI                                         	 2B9-1            	1	83253251	  Barcore T2F3STAR 15x1260x2490, KTI                                         	1	1	 2B9-1            	27	1
47	  Barcore Sengon 16x1230x2050, Mustika BS                                    	 2B3-1            	76	0	  Barcore Sengon 16x1230x2050, Mustika BS                                    	1	1	 2B3-1            	27	1
48	  Barcore T2F3STAR 17.4x1260x2490, KTI                                       	 2B10-1           	6	0	  Barcore T2F3STAR 17.4x1260x2490, KTI                                       	1	1	 2B10-1           	27	1
49	  Barcore Sengon 22x920x1840, Nankai                                         	 2B4-1            	3	0	  Barcore Sengon 22x920x1840, Nankai                                         	1	1	 2B4-1            	27	1
50	  Barcore Sengon 22.3x945x1855, Mustika BS                                   	 2B5-1            	7	0	  Barcore Sengon 22.3x945x1855, Mustika BS                                   	1	1	 2B5-1            	27	1
51	  Barcore Sengon 38x930x2150,  Karunia RA                                    	 2B6-1            	1	0	  Barcore Sengon 38x930x2150,  Karunia RA                                    	1	1	 2B6-1            	27	1
52	  Barcore Sengon 41x1240x2480, Karunia RA                                    	 2B7-1            	2	0	  Barcore Sengon 41x1240x2480, Karunia RA                                    	1	1	 2B7-1            	27	1
53	  Barcore W/ FORMALIN CATCHER 10x920x1830MM, KTI                             	 2B11-1           	1	0	  Barcore W/ FORMALIN CATCHER 10x920x1830MM, KTI                             	1	1	 2B11-1           	27	1
54	  Barcore W/ FORMALIN CATCHER 15X920X1830MM, KTI                             	 2B11-2           	6	0	  Barcore W/ FORMALIN CATCHER 15X920X1830MM, KTI                             	1	1	 2B11-2           	27	1
55	  BLOCKBOARD 30.8x1220x2440MM, MATRATAMA M. J.                               	 2B12-1           	298	2660644	  BLOCKBOARD 30.8x1220x2440MM, MATRATAMA M. J.                               	1	1	 2B12-1           	27	1
56	  DaiLite FAL 6x1225x2440 NEW,  Daiken                                       	 2D2-1            	630	0	  DaiLite FAL 6x1225x2440 NEW,  Daiken                                       	1	1	 2D2-1            	27	1
57	  DaiLite Board (FA) 6x1225x2440 Alumunium,  Daiken                          	 2D3-1            	1	0	  DaiLite Board (FA) 6x1225x2440 Alumunium,  Daiken                          	1	1	 2D3-1            	27	1
58	  DaiLite FAL 9x1225x2440 NEW,  Daiken                                       	 2D4-1            	362	0	  DaiLite FAL 9x1225x2440 NEW,  Daiken                                       	1	1	 2D4-1            	27	1
59	  Dai Lite Board (FA) 9x1225x2440 Alumunium,  Daiken                         	 2D5-1            	1	0	  Dai Lite Board (FA) 9x1225x2440 Alumunium,  Daiken                         	1	1	 2D5-1            	27	1
60	  Falcata 9x12x1820 Wh Shelf, SGP                                            	 2F1-1            	14673	0	  Falcata 9x12x1820 Wh Shelf, SGP                                            	1	1	 2F1-1            	27	1
61	  Falcata 11.7x47x1855,  Mustika BS                                          	 2F2-1            	60	0	  Falcata 11.7x47x1855,  Mustika BS                                          	1	1	 2F2-1            	27	1
62	  Falcata 12x1200x2020 Taiwan Grade,  Mustika BS                             	 2F4-1            	17	0	  Falcata 12x1200x2020 Taiwan Grade,  Mustika BS                             	1	1	 2F4-1            	27	1
63	  Falcata 15x920x1820,  Nankai                                               	 2F5-1            	5	0	  Falcata 15x920x1820,  Nankai                                               	1	1	 2F5-1            	27	1
64	  FALCATA Barcore 15x915x1830, KTI                                           	 2F10-1           	21	0	  FALCATA Barcore 15x915x1830, KTI                                           	1	1	 2F10-1           	27	1
65	  Falcata 15x1220x2440,  Nankai                                              	 2F6-1            	1	0	  Falcata 15x1220x2440,  Nankai                                              	1	1	 2F6-1            	27	1
66	  Falcata 17.5x1220x2440,  Mustika BS                                        	 2F7-1            	2	0	  Falcata 17.5x1220x2440,  Mustika BS                                        	1	1	 2F7-1            	27	1
67	  Kayu Karet 18x150x2200,  RATINDO UTAMA                                     	 2K1-1            	8	0	  Kayu Karet 18x150x2200,  RATINDO UTAMA                                     	1	1	 2K1-1            	27	1
68	  Kayu MERBAU 545 15x150x2400,  ACHMADI PP                                   	 2K3-1            	12	0	  Kayu MERBAU 545 15x150x2400,  ACHMADI PP                                   	1	1	 2K3-1            	27	1
69	  Kayu Merbau 24x90x2400, Achmadi PP                                         	 2K4-1            	2	0	  Kayu Merbau 24x90x2400, Achmadi PP                                         	1	1	 2K4-1            	27	1
70	  Kayu Merbau 18x25x2440, Achmadi PP                                         	 2K19-1           	56	0	  Kayu Merbau 18x25x2440, Achmadi PP                                         	1	1	 2K19-1           	27	1
71	  Kayu APITON 15.5x19x2450,  Marubeni DJ Gresik                              	 2K5-1            	10	0	  Kayu APITON 15.5x19x2450,  Marubeni DJ Gresik                              	1	1	 2K5-1            	27	1
72	  Kayu Sengon Laut 25x70x1300, SURYA P                                       	 2K7-1            	0	0	  Kayu Sengon Laut 25x70x1300, SURYA P                                       	1	1	 2K7-1            	27	1
73	  Kayu MERANTI S4S 24x90x2400, Gresik                                        	 2K13-1           	4	0	  Kayu MERANTI S4S 24x90x2400, Gresik                                        	1	1	 2K13-1           	27	1
74	  LVB 18x1220x1220                                                           	 2L11-1           	1	0	  LVB 18x1220x1220                                                           	1	1	 2L11-1           	27	1
75	  LVB 18x1220x2440,  SGS                                                     	 2L12-1           	2	64302244	  LVB 18x1220x2440,  SGS                                                     	1	1	 2L12-1           	27	1
76	  LVB 30x300x2140,  SUMITOMO                                                 	 2L13-1           	4	20833762	  LVB 30x300x2140,  SUMITOMO                                                 	1	1	 2L13-1           	27	1
77	  LVB 30x920x2130 T2/0.3,  SMIP                                              	 2L14-1           	3	87596271	  LVB 30x920x2130 T2/0.3,  SMIP                                              	1	1	 2L14-1           	27	1
78	  LVB 30x920x2130 UTY,  SMIP                                                 	 2L15-1           	3	28302264	  LVB 30x920x2130 UTY,  SMIP                                                 	1	1	 2L15-1           	27	1
79	  LVB 30x1230x2100,  SGS                                                     	 2L16-1           	2	1092749	  LVB 30x1230x2100,  SGS                                                     	1	1	 2L16-1           	27	1
80	  LVB 48x920x2330,  SMIP                                                     	 2L17-1           	31	0	  LVB 48x920x2330,  SMIP                                                     	1	1	 2L17-1           	27	1
81	  LVB 55x28x2440, Anugerah                                                   	 2L18-1           	20	0	  LVB 55x28x2440, Anugerah                                                   	1	1	 2L18-1           	27	1
82	  LVS 15x1230x2250,  Nankai Indonesia                                        	 2L19-1           	48	0	  LVS 15x1230x2250,  Nankai Indonesia                                        	1	1	 2L19-1           	27	1
83	  LVL 18x1220x2440 T2/F4 Stars,  SGS                                         	 2L1-1            	2	0	  LVL 18x1220x2440 T2/F4 Stars,  SGS                                         	1	1	 2L1-1            	27	1
84	  LVL 22x39x2020 F4 Stars POPLAR,  Daiken                                    	 2L36-1           	5732	49899100	  LVL 22x39x2020 F4 Stars POPLAR,  Daiken                                    	1	1	 2L36-1           	27	1
85	  LVL 22x39x2046 F4 Stars POPLAR,  Daiken                                    	 2L45-1           	1864	0	  LVL 22x39x2046 F4 Stars POPLAR,  Daiken                                    	1	1	 2L45-1           	27	1
86	  LVL 22x39x2320 F4 Stars POPLAR,  Daiken                                    	 2L35-1           	6864	0	  LVL 22x39x2320 F4 Stars POPLAR,  Daiken                                    	1	1	 2L35-1           	27	1
87	  LVL 22x39x2331 F4 Stars POPLAR,  Daiken                                    	 2L43-1           	2242	70437477	  LVL 22x39x2331 F4 Stars POPLAR,  Daiken                                    	1	1	 2L43-1           	27	1
88	  LVL 22x770x2050 F4 Stars POPLAR,  Daiken                                   	 2L31-1           	5	214128	  LVL 22x770x2050 F4 Stars POPLAR,  Daiken                                   	1	1	 2L31-1           	27	1
89	  LVL 25x1200x2150 T2 MR,  SGS                                               	 2L2-1            	3	214128	  LVL 25x1200x2150 T2 MR,  SGS                                               	1	1	 2L2-1            	27	1
90	  LVL 25x1230x2440 FCO.3/T2,  SMIP                                           	 2L3-1            	21	192469137	  LVL 25x1230x2440 FCO.3/T2,  SMIP                                           	1	1	 2L3-1            	27	1
91	  LVL 25x1230x2440 FCO.3/T2 UTY,  SMIP                                       	 2L4-1            	19	52983	  LVL 25x1230x2440 FCO.3/T2 UTY,  SMIP                                       	1	1	 2L4-1            	27	1
92	  LVL 25x1220x1220,  SUMBER Graha                                            	 2L5-1            	1	105709317	  LVL 25x1220x1220,  SUMBER Graha                                            	1	1	 2L5-1            	27	1
93	  LVL 30x460x2420MM, KTI                                                     	 2L46-1           	229	43521565	  LVL 30x460x2420MM, KTI                                                     	1	1	 2L46-1           	27	1
94	  LVL 30x1220x2130 T2 T4STAR,  SUMBER Graha                                  	 2L25-1           	9	186195323	  LVL 30x1220x2130 T2 T4STAR,  SUMBER Graha                                  	1	1	 2L25-1           	27	1
95	  LVL 30x1230x2300,  SUMBER Graha                                            	 2L7-1            	1	47087677	  LVL 30x1230x2300,  SUMBER Graha                                            	1	1	 2L7-1            	27	1
96	  LVL 35x21x2050 POPLAR EUCALIPTUS,  DAIKEN Co.                              	 2L37-1           	6579	22082974	  LVL 35x21x2050 POPLAR EUCALIPTUS,  DAIKEN Co.                              	1	1	 2L37-1           	27	1
97	  LVL 35x21x2050 POPLAR,  NIPPON PAPER LUMBER                                	 2L37-2           	20	0	  LVL 35x21x2050 POPLAR,  NIPPON PAPER LUMBER                                	1	1	 2L37-2           	27	1
98	  LVL 35x21x2050 POPLAR,  LIANYUNGGANG T.W                                   	 2L37-3           	20	0	  LVL 35x21x2050 POPLAR,  LIANYUNGGANG T.W                                   	1	1	 2L37-3           	27	1
99	  LVL 39x20x2050 F4 Stars POPLAR,  Daiken                                    	 2L41-1           	18577	0	  LVL 39x20x2050 F4 Stars POPLAR,  Daiken                                    	1	1	 2L41-1           	27	1
100	  LVL 39x20x2050 POPLAR,  LIANYUNGGANG T.W                                   	 2L41-3           	6	102246715	  LVL 39x20x2050 POPLAR,  LIANYUNGGANG T.W                                   	1	1	 2L41-3           	27	1
101	  LVL 39x22x2020 F4 Stars POPLAR,  Daiken                                    	 2L32-1           	8994	895876	  LVL 39x22x2020 F4 Stars POPLAR,  Daiken                                    	1	1	 2L32-1           	27	1
102	  LVL 39x22x2046 F4 Stars POPLAR,  Daiken                                    	 2L44-1           	3793	45272500	  LVL 39x22x2046 F4 Stars POPLAR,  Daiken                                    	1	1	 2L44-1           	27	1
103	  LVL 39x22x2320 F4 Stars POPLAR,  Daiken                                    	 2L34-1           	14100	189815250	  LVL 39x22x2320 F4 Stars POPLAR,  Daiken                                    	1	1	 2L34-1           	27	1
104	  LVL 39x22x2331 F4 Stars POPLAR,  Daiken                                    	 2L42-1           	3579	0	  LVL 39x22x2331 F4 Stars POPLAR,  Daiken                                    	1	1	 2L42-1           	27	1
105	  LVL 39x770x2050 F4 Stars POPLAR,  Daiken                                   	 2L40-1           	57	0	  LVL 39x770x2050 F4 Stars POPLAR,  Daiken                                   	1	1	 2L40-1           	27	1
106	  LVL 39x1220x2440 T2 F4STAR,  SUMBER Graha                                  	 2L26-1           	6	0	  LVL 39x1220x2440 T2 F4STAR,  SUMBER Graha                                  	1	1	 2L26-1           	27	1
107	  LVL POPLAR 30x455x2430, MARUBENI LUMBER                                    	 2L20-1           	3	185666365	  LVL POPLAR 30x455x2430, MARUBENI LUMBER                                    	1	1	 2L20-1           	27	1
108	  LVL POPLAR 30x910x2050 (MARUBENI), DAIKEN                                  	 2L24-1           	325	17816905	  LVL POPLAR 30x910x2050 (MARUBENI), DAIKEN                                  	1	1	 2L24-1           	27	1
109	  MDF 2.5x1200x2050 SE0 DSK, DAIKEN Co                                       	 2M128-1          	2737	1206928	  MDF 2.5x1200x2050 SE0 DSK, DAIKEN Co                                       	1	1	 2M128-1          	27	1
110	  MDF 2.5x1230x2440 S.E0, TOPAN COSMO                                        	 2M6-2            	17	111194920	  MDF 2.5x1230x2440 S.E0, TOPAN COSMO                                        	1	1	 2M6-2            	27	1
111	  MDF 2.5x1220x2440 RUBBER E2, SUKSES PERKASA FORESTAMA                      	 2M149-1          	1393	0	  MDF 2.5x1220x2440 RUBBER E2, SUKSES PERKASA FORESTAMA                      	1	1	 2M149-1          	27	1
112	  MDF 2.5x1220x2440 E2 MLH, Indonesia Fibreboard                             	 2M7-3            	5324	0	  MDF 2.5x1220x2440 E2 MLH, Indonesia Fibreboard                             	1	1	 2M7-3            	27	1
113	  MDF 2.5x1245x2450, MARUFUJI KK                                             	 2M116-1          	12	0	  MDF 2.5x1245x2450, MARUFUJI KK                                             	1	1	 2M116-1          	27	1
114	  MDF 2.7x850x700 RUBBERWOOD,  ADVANCE FIBER                                 	 2M152-1          	18	0	  MDF 2.7x850x700 RUBBERWOOD,  ADVANCE FIBER                                 	1	1	 2M152-1          	27	1
115	  MDF 2.7x880x2040 S.E0, TOPAN COSMO                                         	 2M10-1           	18	0	  MDF 2.7x880x2040 S.E0, TOPAN COSMO                                         	1	1	 2M10-1           	27	1
116	  MDF 2.7x1220x2440 E2 MLH REG SND GR.A P900 SIT, IFI                        	 2M151-1          	4765	0	  MDF 2.7x1220x2440 E2 MLH REG SND GR.A P900 SIT, IFI                        	1	1	 2M151-1          	27	1
117	  MDF 2.7x1230x2440 S.E0, TOPAN COSMO                                        	 2M16-1           	317	0	  MDF 2.7x1230x2440 S.E0, TOPAN COSMO                                        	1	1	 2M16-1           	27	1
118	  MDF 3x1220x2440 E2 MLH, Indonesia Fibreboard                               	 2M74-1           	28	81098	  MDF 3x1220x2440 E2 MLH, Indonesia Fibreboard                               	1	1	 2M74-1           	27	1
119	  MDF 3x1230x2440  E0 (F*3),  Daiken Co                                      	 2M140-2          	2106	0	  MDF 3x1230x2440  E0 (F*3),  Daiken Co                                      	1	1	 2M140-2          	27	1
120	  MDF 3x1230x2440 S.E0, TOPAN COSMO                                          	 2M22-2           	772	20219158	  MDF 3x1230x2440 S.E0, TOPAN COSMO                                          	1	1	 2M22-2           	27	1
121	  MDF 4x1515x2130 S.E0, Daiken                                               	 2M23-1           	278	1469347	  MDF 4x1515x2130 S.E0, Daiken                                               	1	1	 2M23-1           	27	1
122	  MDF 4x1230x2440 S.E0, TOPAN COSMO                                          	 2M24-1           	617	515051	  MDF 4x1230x2440 S.E0, TOPAN COSMO                                          	1	1	 2M24-1           	27	1
123	  MDF 5.5x820x1650 S.E0, TOPAN COSMO                                         	 2M25-1           	31	36085587	  MDF 5.5x820x1650 S.E0, TOPAN COSMO                                         	1	1	 2M25-1           	27	1
124	  MDF 5.5x920x1830 S.E0, TOPAN COSMO                                         	 2M26-1           	411	107767338	  MDF 5.5x920x1830 S.E0, TOPAN COSMO                                         	1	1	 2M26-1           	27	1
125	  MDF 5.5x1230x1630 S.E0, TOPAN COSMO                                        	 2M27-1           	560	238431	  MDF 5.5x1230x1630 S.E0, TOPAN COSMO                                        	1	1	 2M27-1           	27	1
126	  MDF 5.5x1230x1830 S.E0,  TOPAN COSMO                                       	 2M28-1           	17	0	  MDF 5.5x1230x1830 S.E0,  TOPAN COSMO                                       	1	1	 2M28-1           	27	1
127	  MDF 6x1220x2440 E2 MLH , IFI                                               	 2M131-1          	1	0	  MDF 6x1220x2440 E2 MLH , IFI                                               	1	1	 2M131-1          	27	1
128	  MDF 7x1230x2130 S.E0, Daiken                                               	 2M29-1           	292	0	  MDF 7x1230x2130 S.E0, Daiken                                               	1	1	 2M29-1           	27	1
129	  MDF 9x1220x2440 E2 MLH, Indonesia Fibreboard                               	 2M30-2           	173	0	  MDF 9x1220x2440 E2 MLH, Indonesia Fibreboard                               	1	1	 2M30-2           	27	1
130	  MDF 9x1230x2130 S.E0, Daiken                                               	 2M32-1           	24	117324156	  MDF 9x1230x2130 S.E0, Daiken                                               	1	1	 2M32-1           	27	1
131	  MDF 12x1230x2130 S.E0, TOPAN COSMO                                         	 2M34-2           	4	33192400	  MDF 12x1230x2130 S.E0, TOPAN COSMO                                         	1	1	 2M34-2           	27	1
132	  MDF 15x1220x2440 E2 MLH, Indonesia Fibreboard I                            	 2M37-2           	180	904522	  MDF 15x1220x2440 E2 MLH, Indonesia Fibreboard I                            	1	1	 2M37-2           	27	1
133	  MDF 15x1220x2440 E2 MLH Grade A, DAIIKEN Co.                               	 2M37-3           	659	2279612	  MDF 15x1220x2440 E2 MLH Grade A, DAIIKEN Co.                               	1	1	 2M37-3           	27	1
134	  MDF 15x1230x2130 S.E0, TOPAN COSMO                                         	 2M38-1           	1	36436606	  MDF 15x1230x2130 S.E0, TOPAN COSMO                                         	1	1	 2M38-1           	27	1
135	  MDF 18x850x700 EUCALYPTUS,  ADVANCE FIBER                                  	 2M153-1          	4	0	  MDF 18x850x700 EUCALYPTUS,  ADVANCE FIBER                                  	1	1	 2M153-1          	27	1
136	  MDF 18x1225x2445 S.E0 BROWN,  AST. IND                                     	 2M40-1           	17	48317373	  MDF 18x1225x2445 S.E0 BROWN,  AST. IND                                     	1	1	 2M40-1           	27	1
137	  MDF 18x1225x2445 S.E0 WHITE,  AST. IND                                     	 2M41-1           	47	0	  MDF 18x1225x2445 S.E0 WHITE,  AST. IND                                     	1	1	 2M41-1           	27	1
138	  MDF 18x1220x1830 S.E0 Grade B,  Lamindo                                    	 2M42-1           	2	0	  MDF 18x1220x1830 S.E0 Grade B,  Lamindo                                    	1	1	 2M42-1           	27	1
139	  MDF 18x1220x1830 SE0 U TYPE RS30 SND F4 MLH, IFI                           	 2M76-2           	600	0	  MDF 18x1220x1830 SE0 U TYPE RS30 SND F4 MLH, IFI                           	1	1	 2M76-2           	27	1
140	  MDF 18x1230x2130 SE0, DAIKEN Co                                            	 2M43-3           	133	323368030	  MDF 18x1230x2130 SE0, DAIKEN Co                                            	1	1	 2M43-3           	27	1
141	  MDF 18x1225x2445 LITE,  AST. IND                                           	 2M46-2           	3	261714910	  MDF 18x1225x2445 LITE,  AST. IND                                           	1	1	 2M46-2           	27	1
142	  MDF 21x1230x2050 SE0 DSK, DAIKEN Co                                        	 2M129-1          	7	2905368	  MDF 21x1230x2050 SE0 DSK, DAIKEN Co                                        	1	1	 2M129-1          	27	1
143	  MDF 21x1230x2440 S.E0, DAIKEN Co.                                          	 2M48-2           	94	7138493	  MDF 21x1230x2440 S.E0, DAIKEN Co.                                          	1	1	 2M48-2           	27	1
144	  MDF 21x1220x2440 E2,  SUMITOMO                                             	 2M49-2           	161	78193214	  MDF 21x1220x2440 E2,  SUMITOMO                                             	1	1	 2M49-2           	27	1
145	  MDF 21x1220x2440 E2 MLH, Indonesia Fibreboard                              	 2M49-4           	163	51028451	  MDF 21x1220x2440 E2 MLH, Indonesia Fibreboard                              	1	1	 2M49-4           	27	1
146	  MDF 22x1220x2440 E2 EXP, Lamindo/DMG                                       	 2M50-1           	88	36308022	  MDF 22x1220x2440 E2 EXP, Lamindo/DMG                                       	1	1	 2M50-1           	27	1
147	  MDF 27x1220x2130 AE-N S.E0, Carter Holt Harvey Wood                        	 2M51-1           	67	143122106	  MDF 27x1220x2130 AE-N S.E0, Carter Holt Harvey Wood                        	1	1	 2M51-1           	27	1
148	  MDF 30x1220x2440 E2, SUMITOMO                                              	 2M52-1           	92	0	  MDF 30x1220x2440 E2, SUMITOMO                                              	1	1	 2M52-1           	27	1
149	  MDF SE0 2.5x1200x2020, DAIKEN NEWZEALAND                                   	 2M125-1          	8862	176954994	  MDF SE0 2.5x1200x2020, DAIKEN NEWZEALAND                                   	1	1	 2M125-1          	27	1
150	  MDF SE0 2.5x1200x2320, DAIKEN NEWZEALAND                                   	 2M127-1          	6264	0	  MDF SE0 2.5x1200x2320, DAIKEN NEWZEALAND                                   	1	1	 2M127-1          	27	1
151	  MDF SE0 2.7x1080x2440 FUKAI, DAIKEN NEWZEALAND                             	 2M58-1           	102	4778322	  MDF SE0 2.7x1080x2440 FUKAI, DAIKEN NEWZEALAND                             	1	1	 2M58-1           	27	1
152	  MDF SE0 2.7x1150x2120, DAIKEN NEWZEALAND                                   	 2M124-1          	181	1730949	  MDF SE0 2.7x1150x2120, DAIKEN NEWZEALAND                                   	1	1	 2M124-1          	27	1
153	  MDF SE0 2.7x1230x1830, DAIKEN NEWZEALAND                                   	 2M122-1          	2022	256937623	  MDF SE0 2.7x1230x1830, DAIKEN NEWZEALAND                                   	1	1	 2M122-1          	27	1
154	  MDF SE0 2.7x1200x2120, DAIKEN NEWZEALAND                                   	 2M123-1          	1258	0	  MDF SE0 2.7x1200x2120, DAIKEN NEWZEALAND                                   	1	1	 2M123-1          	27	1
155	  MDF SE0 2.7x1230x2130, DAIKEN NEWZEALAND                                   	 2M60-1           	858	243102779	  MDF SE0 2.7x1230x2130, DAIKEN NEWZEALAND                                   	1	1	 2M60-1           	27	1
156	  MDF SE0 2.7x1230x2440, DAIKEN NEWZEALAND                                   	 2M154-1          	2651	620750	  MDF SE0 2.7x1230x2440, DAIKEN NEWZEALAND                                   	1	1	 2M154-1          	27	1
157	  MDF SE0 3x1220x2440, DAIKEN NEWZEALAND                                     	 2M132-1          	3	0	  MDF SE0 3x1220x2440, DAIKEN NEWZEALAND                                     	1	1	 2M132-1          	27	1
158	  MDF SE0 3x1240x1780,  DAIKEN NEWZEALAND                                    	 2M126-1          	4590	101980	  MDF SE0 3x1240x1780,  DAIKEN NEWZEALAND                                    	1	1	 2M126-1          	27	1
159	  MDF SE0 5.2x920x2130 UF MD CARB P2, DAIKEN NEWZEALAND                      	 2M121-1          	4	0	  MDF SE0 5.2x920x2130 UF MD CARB P2, DAIKEN NEWZEALAND                      	1	1	 2M121-1          	27	1
160	  MDF E1 (F*2)12x1220x2440 Newzealand, PT.NITORI                             	 2M146-1          	39	0	  MDF E1 (F*2)12x1220x2440 Newzealand, PT.NITORI                             	1	1	 2M146-1          	27	1
161	  MDF E1 (F*2) 15x1220x2440 Newzealand, PT.NITORI                            	 2M147-1          	11	0	  MDF E1 (F*2) 15x1220x2440 Newzealand, PT.NITORI                            	1	1	 2M147-1          	27	1
162	  MDF SE0 15x1200x1750, DAIKEN NEWZEALAND                                    	 2M150-1          	1541	0	  MDF SE0 15x1200x1750, DAIKEN NEWZEALAND                                    	1	1	 2M150-1          	27	1
163	  MDF SE0 15x1230x2440, DAIKEN NEWZEALAND                                    	 2M133-1          	1	0	  MDF SE0 15x1230x2440, DAIKEN NEWZEALAND                                    	1	1	 2M133-1          	27	1
164	  MDF SE0 18x1230x1830, DAIKEN NEWZEALAND                                    	 2M119-1          	1189	0	  MDF SE0 18x1230x1830, DAIKEN NEWZEALAND                                    	1	1	 2M119-1          	27	1
165	  MDF SE0 18x1230x1850 FUKAI, DAIKEN NEWZEALAND                              	 2M67-1           	4	0	  MDF SE0 18x1230x1850 FUKAI, DAIKEN NEWZEALAND                              	1	1	 2M67-1           	27	1
166	  MDF SE0 18x1230x2130, DAIKEN NEWZEALAND                                    	 2M114-1          	0	0	  MDF SE0 18x1230x2130, DAIKEN NEWZEALAND                                    	1	1	 2M114-1          	27	1
167	  MDF SE0 18x1245x1200 FUKAI, DAIKEN NEWZEALAND                              	 2M65-1           	1	1345841	  MDF SE0 18x1245x1200 FUKAI, DAIKEN NEWZEALAND                              	1	1	 2M65-1           	27	1
168	  MDF SE0 18x1245x2430 FUKAI, DAIKEN NEWZEALAND                              	 2M109-1          	0	416531	  MDF SE0 18x1245x2430 FUKAI, DAIKEN NEWZEALAND                              	1	1	 2M109-1          	27	1
169	  MDF SE0 18x1245x2464 FUKAI, DAIKEN NEWZEALAND                              	 2M108-1          	1	77928000	  MDF SE0 18x1245x2464 FUKAI, DAIKEN NEWZEALAND                              	1	1	 2M108-1          	27	1
170	  MDF SE0 18x1245x2465 FUKAI, DAIKEN NEWZEALAND                              	 2M107-1          	5	0	  MDF SE0 18x1245x2465 FUKAI, DAIKEN NEWZEALAND                              	1	1	 2M107-1          	27	1
171	  MDF SE0 21x1230x1000 FUKAI, DAIKEN NEWZEALAND                              	 2M69-1           	58	0	  MDF SE0 21x1230x1000 FUKAI, DAIKEN NEWZEALAND                              	1	1	 2M69-1           	27	1
172	  MDF SE0 24x1000x1200 FUKAI, DAIKEN NEWZEALAND                              	 2M70-1           	72	0	  MDF SE0 24x1000x1200 FUKAI, DAIKEN NEWZEALAND                              	1	1	 2M70-1           	27	1
173	  MDF SE0 24x1230x1830 FUKAI, DAIKEN NEWZEALAND                              	 2M71-1           	20	0	  MDF SE0 24x1230x1830 FUKAI, DAIKEN NEWZEALAND                              	1	1	 2M71-1           	27	1
174	  MDF SE0 30x1230x1000 FUKAI, DAIKEN NEWZEALAND                              	 2M72-1           	59	0	  MDF SE0 30x1230x1000 FUKAI, DAIKEN NEWZEALAND                              	1	1	 2M72-1           	27	1
175	  PW 2.4x1220x2440 UTY, SMIP                                                 	 2P2-2            	15	26516388	  PW 2.4x1220x2440 UTY, SMIP                                                 	1	1	 2P2-2            	27	1
176	  PW 2.7x1220x2440 UTY, SMIP                                                 	 2P59-2           	12	849049	  PW 2.7x1220x2440 UTY, SMIP                                                 	1	1	 2P59-2           	27	1
177	  PW 2.7x1220x2440 T2 FE 5.0, KTI                                            	 2P54-4           	6	20663506	  PW 2.7x1220x2440 T2 FE 5.0, KTI                                            	1	1	 2P54-4           	27	1
178	  PW 2.7x1220x2440 OVL, KARUNIA R.A                                          	 2P54-6           	1146	0	  PW 2.7x1220x2440 OVL, KARUNIA R.A                                          	1	1	 2P54-6           	27	1
179	  PW 2.7x1220x2440 UTY BTR GRADE T2, SEJAHTERA USAHA BERSAMA                 	 2P54-2           	10	153132472	  PW 2.7x1220x2440 UTY BTR GRADE T2, SEJAHTERA USAHA BERSAMA                 	1	1	 2P54-2           	27	1
180	  PW 4.8x1220x2440 UTY BTR GRADE T2, SEJAHTERA USAHA BERSAMA                 	 2P62-1           	148	21449637	  PW 4.8x1220x2440 UTY BTR GRADE T2, SEJAHTERA USAHA BERSAMA                 	1	1	 2P62-1           	27	1
181	  PW 5.2x1230x2150 T2 FC0.3, MerantiKatingan/KTC                             	 2P3-1            	96	87578793	  PW 5.2x1230x2150 T2 FC0.3, MerantiKatingan/KTC                             	1	1	 2P3-1            	27	1
182	  PW 6x915x2440, Anugerah KA                                                 	 2P4-1            	7	9500196	  PW 6x915x2440, Anugerah KA                                                 	1	1	 2P4-1            	27	1
183	  PW 6x1220x2440, Anugerah KA                                                	 2P5-1            	1	12403834	  PW 6x1220x2440, Anugerah KA                                                	1	1	 2P5-1            	27	1
184	  PW 9x920x1830 G2, SMIP                                                     	 2P6-1            	157	0	  PW 9x920x1830 G2, SMIP                                                     	1	1	 2P6-1            	27	1
185	  PW 9x1230x2440 (F4*)JASOPG-2, KTI                                          	 2P9-4            	4	0	  PW 9x1230x2440 (F4*)JASOPG-2, KTI                                          	1	1	 2P9-4            	27	1
186	  PW 9x1220x2440 E-1 OVLBTR, KTI                                             	 2P9-5            	91	0	  PW 9x1220x2440 E-1 OVLBTR, KTI                                             	1	1	 2P9-5            	27	1
187	  PW 11.9x945x1840 FCO Ovl, SMIP                                             	 2P10-1           	9	0	  PW 11.9x945x1840 FCO Ovl, SMIP                                             	1	1	 2P10-1           	27	1
188	  PW 12x1220x2440 T2/0.3 OVL-BTR, SMIP                                       	 2P11-4           	445	9653537	  PW 12x1220x2440 T2/0.3 OVL-BTR, SMIP                                       	1	1	 2P11-4           	27	1
189	  PW 12x1220x2440 (UTY)T2/0.3 OVL-BTR, SMIP                                  	 2P11-5           	65	0	  PW 12x1220x2440 (UTY)T2/0.3 OVL-BTR, SMIP                                  	1	1	 2P11-5           	27	1
190	  PW 15x920x1830 T2/0.3 OVB, SMIP                                            	 2P12-2           	350	0	  PW 15x920x1830 T2/0.3 OVB, SMIP                                            	1	1	 2P12-2           	27	1
191	  PW 15x920x1830 UTY, SMIP                                                   	 2P12-3           	40	137990736	  PW 15x920x1830 UTY, SMIP                                                   	1	1	 2P12-3           	27	1
192	  PW 15x1230x2440 T2FE5.0, KTI                                               	 2P63-2           	40	0	  PW 15x1230x2440 T2FE5.0, KTI                                               	1	1	 2P63-2           	27	1
193	  PW 18x900x1800, FALCATA JAYA                                               	 2P14-1           	5	322693422	  PW 18x900x1800, FALCATA JAYA                                               	1	1	 2P14-1           	27	1
194	  PW 18x1220x2440 T2/FE03 OVL-BTR, SMIP                                      	 2P17-2           	214	0	  PW 18x1220x2440 T2/FE03 OVL-BTR, SMIP                                      	1	1	 2P17-2           	27	1
195	  PW 18x1220x2440 (UTY) T2/FE03 OVL-BTR, SMIP                                	 2P17-3           	25	0	  PW 18x1220x2440 (UTY) T2/FE03 OVL-BTR, SMIP                                	1	1	 2P17-3           	27	1
196	  PW 18x1220x2440 T2FE5.0, KTI                                               	 2P17-4           	183	0	  PW 18x1220x2440 T2FE5.0, KTI                                               	1	1	 2P17-4           	27	1
197	  PW 18x1230x2440 T2FE5.0, KTI                                               	 2P17-5           	25	0	  PW 18x1230x2440 T2FE5.0, KTI                                               	1	1	 2P17-5           	27	1
198	  PW / Block Board 18x1220x2440, karunia R.A                                 	 2P18-2           	5	0	  PW / Block Board 18x1220x2440, karunia R.A                                 	1	1	 2P18-2           	27	1
199	  PW 21x920x1830 FC0.3 G1, KTI                                               	 2P19-1           	325	0	  PW 21x920x1830 FC0.3 G1, KTI                                               	1	1	 2P19-1           	27	1
200	  MFC 15x1830x2440 WHITE MK630,  Daiken Co                                   	 2M138-1          	416	0	  MFC 15x1830x2440 WHITE MK630,  Daiken Co                                   	1	1	 2M138-1          	27	1
201	  MFC 15x2070x2800 S.E0 WHITE-II Kaindl,  ToppanCosmo                        	 2M73-1           	316	0	  MFC 15x2070x2800 S.E0 WHITE-II Kaindl,  ToppanCosmo                        	1	1	 2M73-1           	27	1
202	  MFC 20x1830x2440 WHITE MK630,  Daiken Co                                   	 2M139-1          	749	0	  MFC 20x1830x2440 WHITE MK630,  Daiken Co                                   	1	1	 2M139-1          	27	1
203	  GLUE AQUENCE PL 27M PLASTIC PAIL@20KG, HENKEL A.T.                         	 4G38-1           	4	78311	  GLUE AQUENCE PL 27M PLASTIC PAIL@20KG, HENKEL A.T.                         	1	1	 4G38-1           	1	1
204	  GLUE AQUENCE PL 27MIF @200KG DRUM, HENKEL A.T.                             	 4G37-1           	3	12661200	  GLUE AQUENCE PL 27MIF @200KG DRUM, HENKEL A.T.                             	1	1	 4G37-1           	1	1
205	  GLUE HE3162A (putih) Can1.4, Henkel Indonesien                             	 4G9-1            	18	2256067	  GLUE HE3162A (putih) Can1.4, Henkel Indonesien                             	1	1	 4G9-1            	27	1
206	  GLUE HE3162B (kuning) Can1.4, Henkel Indonesien                            	 4G10-1           	15	1876459	  GLUE HE3162B (kuning) Can1.4, Henkel Indonesien                            	1	1	 4G10-1           	27	1
207	  GLUE CH-67L Drum200, Lemindo                                               	 4G11-1           	4	11377817	  GLUE CH-67L Drum200, Lemindo                                               	1	1	 4G11-1           	27	1
208	  GLUE SH-20L Drum200, Lemindo                                               	 4G12-1           	4	14879526	  GLUE SH-20L Drum200, Lemindo                                               	1	1	 4G12-1           	27	1
209	  GLUE INWOOD 245 LV DRUM 200KG, PT. RONADAMAR S.                            	 4G34-1           	2	5036287	  GLUE INWOOD 245 LV DRUM 200KG, PT. RONADAMAR S.                            	1	1	 4G34-1           	1	1
210	  GLUE G602F Can2.5, Saitama                                                 	 4G14-1           	16	0	  GLUE G602F Can2.5, Saitama                                                 	1	1	 4G14-1           	27	1
211	  GLUE GRIP 3KG, Polichemi APP                                               	 4G21-1           	9	1396043	  GLUE GRIP 3KG, Polichemi APP                                               	1	1	 4G21-1           	27	1
212	  GLUE Lem Wraping 5275P Pail24, Sparta Prima                                	 4G15-1           	6	9916414	  GLUE Lem Wraping 5275P Pail24, Sparta Prima                                	1	1	 4G15-1           	27	1
213	  GLUE ELMELT W-1 Box15, MKK                                                 	 4G16-1           	1	0	  GLUE ELMELT W-1 Box15, MKK                                                 	1	1	 4G16-1           	27	1
214	  GLUE ELMELT W-10 Box15, MKK                                                	 4G17-1           	1	0	  GLUE ELMELT W-10 Box15, MKK                                                	1	1	 4G17-1           	27	1
215	  GLUE ELMELT W-39 Box15, MKK                                                	 4G18-1           	1	0	  GLUE ELMELT W-39 Box15, MKK                                                	1	1	 4G18-1           	27	1
216	  GLUE PRESTO PC 3603B Box20, Polichemi APP                                  	 4G19-1           	1	551690	  GLUE PRESTO PC 3603B Box20, Polichemi APP                                  	1	1	 4G19-1           	1	1
217	  GLUE PRESTO PC2324HV Drum200, Polichemi APP                                	 4G20-1           	4	10817849	  GLUE PRESTO PC2324HV Drum200, Polichemi APP                                	1	1	 4G20-1           	1	1
218	  GLUE DORUS MS680P Zak25, Henkel                                            	 4G2-1            	1	762779	  GLUE DORUS MS680P Zak25, Henkel                                            	1	1	 4G2-1            	1	1
219	  GLUE DORUS MD8101 Pail20, Henkel                                           	 4G4-1            	1	0	  GLUE DORUS MD8101 Pail20, Henkel                                           	1	1	 4G4-1            	1	1
220	  GLUE DORUS MD8102 Drum200, Henkel                                          	 4G5-1            	2	8830000	  GLUE DORUS MD8102 Drum200, Henkel                                          	1	1	 4G5-1            	1	1
221	  GLUE Dorus R15-205 Kg1, Henkel                                             	 4G6-1            	3	0	  GLUE Dorus R15-205 Kg1, Henkel                                             	1	1	 4G6-1            	1	1
222	  GLUE Dorus FD150/6LS Pail30, Henkel Indonesien                             	 4G8-1            	11	61946298	  GLUE Dorus FD150/6LS Pail30, Henkel Indonesien                             	1	1	 4G8-1            	1	1
223	  GLUE TECNOMELT Q239 ZAK 25, HENKEL AT                                      	 4G31-2           	1	1592067	  GLUE TECNOMELT Q239 ZAK 25, HENKEL AT                                      	1	1	 4G31-2           	1	1
224	  GLUE TECHNOMELT DORUS KS 351 @25KG , HENKEL INDONESIEN                     	 4G35-1           	5	6714932	  GLUE TECHNOMELT DORUS KS 351 @25KG , HENKEL INDONESIEN                     	1	1	 4G35-1           	1	1
225	  GLUE TECHNOMELT KS 3561 25K,  HENKEL INDONESIEN                            	 4G36-1           	5	6657390	  GLUE TECHNOMELT KS 3561 25K,  HENKEL INDONESIEN                            	1	1	 4G36-1           	1	1
226	  Hardener U - 5 PCan1Sparta Prima	 4H1-1	1	72516	  Hardener U - 5 PCan1Sparta Prima	1	1	 4H1-1	27	1
227	  Hardener U - 7 PCan1Sparta Prima	 4H2-1	5	526907	  Hardener U - 7 PCan1Sparta Prima	1	1	 4H2-1	27	1
228	  Stel Wool Roll30, Mikro                                                    	 4S1-1            	2	755914	  Stel Wool Roll30, Mikro                                                    	1	1	 4S1-1            	27	1
229	  ET CHITOSE MB008 (WHITE) W=22MM T=1MM, OUPU MATERIAL BANGUNAN              	 6ED80-1          	2900	8700000	  ET CHITOSE MB008 (WHITE) W=22MM T=1MM, OUPU MATERIAL BANGUNAN              	1	1	 6ED80-1          	5	1
230	  ET CHITOSE MB008 (WHITE) W=29MM T=1MM, OUPU MATERIAL BANGUNAN              	 6ED81-1          	2200	8030000	  ET CHITOSE MB008 (WHITE) W=29MM T=1MM, OUPU MATERIAL BANGUNAN              	1	1	 6ED81-1          	5	1
231	  ET CHITOSE MB41 (OAK) W=22MM T=1MM, OUPU MATERIAL BANGUNAN                 	 6ED82-1          	3800	11400000	  ET CHITOSE MB41 (OAK) W=22MM T=1MM, OUPU MATERIAL BANGUNAN                 	1	1	 6ED82-1          	5	1
232	  ET CHITOSE MB41 (OAK) W=29MM T=1MM, OUPU MATERIAL BANGUNAN                 	 6ED83-1          	3000	10950000	  ET CHITOSE MB41 (OAK) W=29MM T=1MM, OUPU MATERIAL BANGUNAN                 	1	1	 6ED83-1          	5	1
233	  ET Orifien  ITOKI  WA 4502 21mm, KOIKE I                                   	 6EI1-1           	306	0	  ET Orifien  ITOKI  WA 4502 21mm, KOIKE I                                   	1	1	 6EI1-1           	5	1
234	  ET Orifien  ITOKI  WA 4502 27mm, KOIKE I                                   	 6EI3-1           	200	775341	  ET Orifien  ITOKI  WA 4502 27mm, KOIKE I                                   	1	1	 6EI3-1           	5	1
235	  ET Orifien  ITOKI  WA 4404E 21mm, KOIKE I                                  	 6EI5-1           	256	0	  ET Orifien  ITOKI  WA 4404E 21mm, KOIKE I                                  	1	1	 6EI5-1           	5	1
236	  ET Orifien  ITOKI  WA 4404E 27mm, KOIKE I                                  	 6EI7-1           	200	775341	  ET Orifien  ITOKI  WA 4404E 27mm, KOIKE I                                  	1	1	 6EI7-1           	5	1
237	  ET SC40.2320-3012  ITOKI  23mm, KOIKE I                                    	 6EI11-1          	30	0	  ET SC40.2320-3012  ITOKI  23mm, KOIKE I                                    	1	1	 6EI11-1          	5	1
238	  EP MW43-3320-4306  ITOKI  2x33mm, KOIKE IMATEC                             	 6EI12-1          	229	7242081	  EP MW43-3320-4306  ITOKI  2x33mm, KOIKE IMATEC                             	1	1	 6EI12-1          	5	1
239	  EP MW43-3320-4311  ITOKI  2x33mm, KOIKE IMATEC                             	 6EI13-1          	244	7624071	  EP MW43-3320-4311  ITOKI  2x33mm, KOIKE IMATEC                             	1	1	 6EI13-1          	5	1
240	  ORIFIEN SHEET  ITOKI  2300-524 970mm, KOIKE IMATEC                         	 6OI1-1           	20	1395721	  ORIFIEN SHEET  ITOKI  2300-524 970mm, KOIKE IMATEC                         	1	1	 6OI1-1           	5	1
241	  ORIFIEN SHEET  ITOKI  2281-201 970mm, KOIKE IMATEC                         	 6OI2-1           	20	1395721	  ORIFIEN SHEET  ITOKI  2281-201 970mm, KOIKE IMATEC                         	1	1	 6OI2-1           	5	1
242	  PVC ORIFINE  ITOKI  WS6001E 970mm, KOIKE I                                 	 6PI1-1           	18	0	  PVC ORIFINE  ITOKI  WS6001E 970mm, KOIKE I                                 	1	1	 6PI1-1           	5	1
243	  PAPER Sheet  ITOKI  2281-121 970mm, KOIKE IMATEC                           	 6PI3-1           	500	10664083	  PAPER Sheet  ITOKI  2281-121 970mm, KOIKE IMATEC                           	1	1	 6PI3-1           	5	1
244	  Orifien Sheet  WOODONE  NE50112LS (DARK GREY)-1270mm, Kyushu Marufuji K    	 6OW2-1           	421	0	  Orifien Sheet  WOODONE  NE50112LS (DARK GREY)-1270mm, Kyushu Marufuji K    	1	1	 6OW2-1           	5	1
245	  Paper Sheet  WOODONE NAGOMI 1245mm, Kyushu Marufuji K                      	 6PW2-1           	1690	0	  Paper Sheet  WOODONE NAGOMI 1245mm, Kyushu Marufuji K                      	1	1	 6PW2-1           	5	1
246	  Paper Sheet  WOODONE WH 1245mm, Kyushu Marufuji K                          	 6PW3-1           	100	0	  Paper Sheet  WOODONE WH 1245mm, Kyushu Marufuji K                          	1	1	 6PW3-1           	5	1
247	  Paper Sheet  WOODONE PW (dainippon)1245mm, Kyushu Marufuji K               	 6PW4-1           	785	0	  Paper Sheet  WOODONE PW (dainippon)1245mm, Kyushu Marufuji K               	1	1	 6PW4-1           	5	1
248	  Orefin DAITO SELENA WALNUT  W-1250mm, Koike Imatec                         	 6OD8-1           	300	12685188	  Orefin DAITO SELENA WALNUT  W-1250mm, Koike Imatec                         	1	1	 6OD8-1           	5	1
249	  ET Orefin DAITO SELENA WALNUT 30VS 21mm, Koike Imatec                      	 6ED59-1          	5010	15053121	  ET Orefin DAITO SELENA WALNUT 30VS 21mm, Koike Imatec                      	1	1	 6ED59-1          	5	1
250	  Paper Sheet DAITO SELENA WALNUT 30VS 1270mm, Koike Imatec                  	 6PD61-1          	3400	78681523	  Paper Sheet DAITO SELENA WALNUT 30VS 1270mm, Koike Imatec                  	1	1	 6PD61-1          	5	1
251	  Paper Sheet FUKAI P0897TSU 1250mm, Topan Cosmo                             	 6PF1-1           	4893	0	  Paper Sheet FUKAI P0897TSU 1250mm, Topan Cosmo                             	1	1	 6PF1-1           	5	1
252	  PAPER Sheet FUKAI  P6434SPU 30x1270mm, Topan Cosmo                         	 6PF7-2           	5000	0	  PAPER Sheet FUKAI  P6434SPU 30x1270mm, Topan Cosmo                         	1	1	 6PF7-2           	5	1
253	  PVC FUKAI S502-PB (BLACK SANDEMBOSS) 0.12mmx1260mm, FOSHAN T               	 6PF26-1          	21820	90638370	  PVC FUKAI S502-PB (BLACK SANDEMBOSS) 0.12mmx1260mm, FOSHAN T               	1	1	 6PF26-1          	5	1
254	  PVC FUKAI H60120-003(Walnut) 0.15mmx1250mm, FOSHAN TP                      	 6PF28-1          	2871	34713979	  PVC FUKAI H60120-003(Walnut) 0.15mmx1250mm, FOSHAN TP                      	1	1	 6PF28-1          	5	1
255	  PVC FUKAI H60120-A(Walnut HI GLoss) 0.2mmx1250mm, FOSHAN TP                	 6PF29-1          	517	12870069	  PVC FUKAI H60120-A(Walnut HI GLoss) 0.2mmx1250mm, FOSHAN TP                	1	1	 6PF29-1          	5	1
256	  PVC FUKAI LF844-41 1270mm, Orchard                                         	 6PF5-1           	1863	0	  PVC FUKAI LF844-41 1270mm, Orchard                                         	1	1	 6PF5-1           	5	1
257	  PVC FUKAI SL-34B 0.12mmx1250mm, Meiwa                                      	 6PF9-1           	100	0	  PVC FUKAI SL-34B 0.12mmx1250mm, Meiwa                                      	1	1	 6PF9-1           	5	1
258	  PVC FUKAI SF50006-03T 1245mm, Orchard D M                                  	 6PF11-1          	69	1943349	  PVC FUKAI SF50006-03T 1245mm, Orchard D M                                  	1	1	 6PF11-1          	5	1
259	  PVC FUKAI SF50006-03T 0.2x1245, Topan Cosmo                                	 6PF13-1          	1230	34564406	  PVC FUKAI SF50006-03T 0.2x1245, Topan Cosmo                                	1	1	 6PF13-1          	5	1
260	  PVC FUKAI MRF3000N18 GY 42 ROSE 0.16x1250, MEIWA                           	 6PF24-1          	2940	56637810	  PVC FUKAI MRF3000N18 GY 42 ROSE 0.16x1250, MEIWA                           	1	1	 6PF24-1          	5	1
261	  PVC FUKAI MRF PRINT W-1 (CLEANER BOX), MEIWA                               	 6PF30-1          	50	2333300	  PVC FUKAI MRF PRINT W-1 (CLEANER BOX), MEIWA                               	1	1	 6PF30-1          	5	1
262	  PVC FUKAI MRF PRINT W-2 (CLEANER BOX), MEIWA                               	 6PF31-1          	50	2333300	  PVC FUKAI MRF PRINT W-2 (CLEANER BOX), MEIWA                               	1	1	 6PF31-1          	5	1
263	  PVC FUKAI MRF PRINT BR-1 (CLEANER BOX), MEIWA                              	 6PF32-1          	50	2333300	  PVC FUKAI MRF PRINT BR-1 (CLEANER BOX), MEIWA                              	1	1	 6PF32-1          	5	1
264	  PVC Sheet FUKAI TACO SHEET S878L 1250mm, SUMBER SELAMAT                    	 6PF27-1          	320	13151402	  PVC Sheet FUKAI TACO SHEET S878L 1250mm, SUMBER SELAMAT                    	1	1	 6PF27-1          	5	1
265	  PVC Sheet FUKAI LF2905-24 0.2mmx1270mm, TOPAN PRINTING                     	 6PF23-6          	550	7473318	  PVC Sheet FUKAI LF2905-24 0.2mmx1270mm, TOPAN PRINTING                     	1	1	 6PF23-6          	5	1
266	  PVC Sheet FUKAI LF2905-90 0.17mmx1270mm, TOPAN COSMO                       	 6PF22-1          	660	8966211	  PVC Sheet FUKAI LF2905-90 0.17mmx1270mm, TOPAN COSMO                       	1	1	 6PF22-1          	5	1
267	  PVC Sheet FUKAI BK-02 0.12x1250mm, Meiwa                                   	 6PF19-1          	4545	47617614	  PVC Sheet FUKAI BK-02 0.12x1250mm, Meiwa                                   	1	1	 6PF19-1          	5	1
268	  ET FUKAI MRF1062N W05S 0.52X1250mm, Meiwa                                  	 6EF20-1          	1025	52647075	  ET FUKAI MRF1062N W05S 0.52X1250mm, Meiwa                                  	1	1	 6EF20-1          	5	1
269	  ET FUKAI MRF3000N05 GY42ROSE 0.46X1250mm, Meiwa                            	 6EF19-1          	110	6125240	  ET FUKAI MRF3000N05 GY42ROSE 0.46X1250mm, Meiwa                            	1	1	 6EF19-1          	5	1
270	  ET FUKAI  MRF3000N BR149TRE (Maple) 0.46 x 1250mm, MEIWA                   	 6EF13-1          	505	28300631	  ET FUKAI  MRF3000N BR149TRE (Maple) 0.46 x 1250mm, MEIWA                   	1	1	 6EF13-1          	5	1
271	  ET FUKAI  MRF3000N BR150TRE (Oak) 0.46 x 1250mm, MEIWA                     	 6EF14-1          	145	8125963	  ET FUKAI  MRF3000N BR150TRE (Oak) 0.46 x 1250mm, MEIWA                     	1	1	 6EF14-1          	5	1
272	  ET FUKAI SF50006-03TPCTZ 0.3x1245mm, Topan Cosmo                           	 6EF2-1           	128	0	  ET FUKAI SF50006-03TPCTZ 0.3x1245mm, Topan Cosmo                           	1	1	 6EF2-1           	5	1
273	  ET FUKAI SF50006-03TPCTZ  0.5x1400mm, Topan Cosmo                          	 6EF3-1           	2907	213715140	  ET FUKAI SF50006-03TPCTZ  0.5x1400mm, Topan Cosmo                          	1	1	 6EF3-1           	5	1
274	  ET FUKAI PR04A 1250mm, Meiwa                                               	 6EF4-1           	1760	0	  ET FUKAI PR04A 1250mm, Meiwa                                               	1	1	 6EF4-1           	5	1
275	  ET FUKAI SL-34A 1250mm, Meiwa                                              	 6EF5-1           	1455	0	  ET FUKAI SL-34A 1250mm, Meiwa                                              	1	1	 6EF5-1           	5	1
276	  ET FUKAI SF10014-03PCT  0.45x1245mm, Topan Cosmo                           	 6EF8-1           	1517	101761983	  ET FUKAI SF10014-03PCT  0.45x1245mm, Topan Cosmo                           	1	1	 6EF8-1           	5	1
277	  ET FUKAI VF844-41 1270mm, Orchard                                          	 6EF11-1          	2062	0	  ET FUKAI VF844-41 1270mm, Orchard                                          	1	1	 6EF11-1          	5	1
278	  ET FUKAI BK02S 0.52x1250mm, Meiwa                                          	 6EF12-1          	1640	84701498	  ET FUKAI BK02S 0.52x1250mm, Meiwa                                          	1	1	 6EF12-1          	5	1
279	  ET FUKAI H60120-003(Walnut) 0.4mmx1250mm, FOSHAN TP                        	 6EF15-1          	379	12130369	  ET FUKAI H60120-003(Walnut) 0.4mmx1250mm, FOSHAN TP                        	1	1	 6EF15-1          	5	1
280	  ET FUKAI H60120-A(Walnut Hi Gloss) 0.4mmx1250mm, FOSHAN TP                 	 6EF16-1          	286	11391380	  ET FUKAI H60120-A(Walnut Hi Gloss) 0.4mmx1250mm, FOSHAN TP                 	1	1	 6EF16-1          	5	1
281	  ORIFIEN SHEET FUKAI TPU-022 1260mm, TOPAN COSMO                            	 6OF2-1           	230	40789285	  ORIFIEN SHEET FUKAI TPU-022 1260mm, TOPAN COSMO                            	1	1	 6OF2-1           	5	1
282	  ORIFIEN SHEET FUKAI TML-0025 1260mm, TOPAN PRINTING                        	 6OF4-1           	17	0	  ORIFIEN SHEET FUKAI TML-0025 1260mm, TOPAN PRINTING                        	1	1	 6OF4-1           	5	1
283	  ORIFIEN SHEET FUKAI TML-0027 1260mm, TOPAN PRINTING                        	 6OF5-1           	15	0	  ORIFIEN SHEET FUKAI TML-0027 1260mm, TOPAN PRINTING                        	1	1	 6OF5-1           	5	1
284	  ORIFIEN SHEET FUKAI KITCHEN WS 400I E-S 1260mm, KOIKE I                    	 6OF1-1           	600	30559647	  ORIFIEN SHEET FUKAI KITCHEN WS 400I E-S 1260mm, KOIKE I                    	1	1	 6OF1-1           	5	1
285	  PVC SHEET DAIKEN TD(45017-20) 1250mm, Daiken Co                            	 6PD8-1           	13	0	  PVC SHEET DAIKEN TD(45017-20) 1250mm, Daiken Co                            	1	1	 6PD8-1           	5	1
286	  PVC SHEET DAIKEN MRF1082N20 WH07 0.25x1250mm, MEIWA                        	 6PD84-1          	2510	54487279	  PVC SHEET DAIKEN MRF1082N20 WH07 0.25x1250mm, MEIWA                        	1	1	 6PD84-1          	5	1
287	  PVC SHEET DAIKEN WH07 1250mm, Meiwa                                        	 6PD9-1           	1100	14221783	  PVC SHEET DAIKEN WH07 1250mm, Meiwa                                        	1	1	 6PD9-1           	5	1
288	  PVC SHEET DAIKEN WH09S 1250mm, Meiwa                                       	 6PD11-1          	1200	0	  PVC SHEET DAIKEN WH09S 1250mm, Meiwa                                       	1	1	 6PD11-1          	5	1
289	  PVC DAIKEN DKC1082 N18 GR06 Green 1250mm, MEIWA                            	 6PD51-1          	630	0	  PVC DAIKEN DKC1082 N18 GR06 Green 1250mm, MEIWA                            	1	1	 6PD51-1          	5	1
290	  PVC DAIKEN DKC3004N 18 WW05 DK01 1250mm, MEIWA                             	 6PD48-1          	3003	57371239	  PVC DAIKEN DKC3004N 18 WW05 DK01 1250mm, MEIWA                             	1	1	 6PD48-1          	5	1
291	  PVC DAIKEN DKC3004N BR123 DK01(ML PVC)1250mm, MEIWA                        	 6PD44-1          	57	0	  PVC DAIKEN DKC3004N BR123 DK01(ML PVC)1250mm, MEIWA                        	1	1	 6PD44-1          	5	1
292	  PVC DAIKEN DKC3004N BR127 DK01 0.16x1250mm, MEIWA                          	 6PD67-1          	414	0	  PVC DAIKEN DKC3004N BR127 DK01 0.16x1250mm, MEIWA                          	1	1	 6PD67-1          	5	1
293	  PVC SHEET DAIKEN BR 129 DK01 1250mm, MEIWA                                 	 6PD68-1          	994	0	  PVC SHEET DAIKEN BR 129 DK01 1250mm, MEIWA                                 	1	1	 6PD68-1          	5	1
294	  PVC DAIKEN DKC2003N20 BLACK 1250mm, MEIWA                                  	 6PD45-1          	2754	0	  PVC DAIKEN DKC2003N20 BLACK 1250mm, MEIWA                                  	1	1	 6PD45-1          	5	1
295	  PVC DAIKEN DKC 3004N BR118 DK01(MT PVC) 1250mm, MEIWA                      	 6PD46-1          	1510	0	  PVC DAIKEN DKC 3004N BR118 DK01(MT PVC) 1250mm, MEIWA                      	1	1	 6PD46-1          	5	1
296	  PVC SHEET DAIKEN BR 117DK01 (TG PVC) 1250mm, MEIWA                         	 6PD13-1          	6490	101389411	  PVC SHEET DAIKEN BR 117DK01 (TG PVC) 1250mm, MEIWA                         	1	1	 6PD13-1          	5	1
297	  PVC Sheet DAIKEN DK010 ( WG / GY40 ) 0.16mmx1250mm, MEIWA                  	 6PD14-1          	1114	17769414	  PVC Sheet DAIKEN DK010 ( WG / GY40 ) 0.16mmx1250mm, MEIWA                  	1	1	 6PD14-1          	5	1
298	  PVC Sheet DAIKEN D06-KD 1250mm, Daiken                                     	 6PD16-1          	76	0	  PVC Sheet DAIKEN D06-KD 1250mm, Daiken                                     	1	1	 6PD16-1          	5	1
299	  PVC DAIKEN BD-PVC(11604-26) 1250mm, Daiken Co                              	 6PD69-1          	706	0	  PVC DAIKEN BD-PVC(11604-26) 1250mm, Daiken Co                              	1	1	 6PD69-1          	5	1
300	  Paper Sheet DAIKEN CE-001 1250mm, Koike Imatec                             	 6PD18-1          	17000	155766752	  Paper Sheet DAIKEN CE-001 1250mm, Koike Imatec                             	1	1	 6PD18-1          	5	1
301	  Paper Sheet DAIKEN S05P-LG 1260mm, Koike Imatec                            	 6PD27-1          	12720	135958802	  Paper Sheet DAIKEN S05P-LG 1260mm, Koike Imatec                            	1	1	 6PD27-1          	5	1
302	  Paper Sheet DAIKEN T09PMF 1250mm, Koike Imatec                             	 6PD36-1          	89	0	  Paper Sheet DAIKEN T09PMF 1250mm, Koike Imatec                             	1	1	 6PD36-1          	5	1
303	  Paper Sheet DAIKEN D11G-CL 1250mm, Koike Imatec                            	 6PD39-1          	100	0	  Paper Sheet DAIKEN D11G-CL 1250mm, Koike Imatec                            	1	1	 6PD39-1          	5	1
304	  Paper Sheet DAIKEN D18P MG (PS)1270mm, Koike Imatec                        	 6PD70-1          	2000	32231831	  Paper Sheet DAIKEN D18P MG (PS)1270mm, Koike Imatec                        	1	1	 6PD70-1          	5	1
305	  Paper Sheet DAIKEN D18P MG YOKO (PS)1270mm, Koike Imatec                   	 6PD77-1          	900	14361494	  Paper Sheet DAIKEN D18P MG YOKO (PS)1270mm, Koike Imatec                   	1	1	 6PD77-1          	5	1
306	  Paper Sheet DAIKEN D18P MJ (PS)1270mm, Koike Imatec                        	 6PD71-1          	2050	32327704	  Paper Sheet DAIKEN D18P MJ (PS)1270mm, Koike Imatec                        	1	1	 6PD71-1          	5	1
307	  Paper Sheet DAIKEN D18P MJ YOKO (PS)1270mm, Koike Imatec                   	 6PD78-1          	1430	23145190	  Paper Sheet DAIKEN D18P MJ YOKO (PS)1270mm, Koike Imatec                   	1	1	 6PD78-1          	5	1
308	  Paper Sheet DAIKEN D18P MT8 (PS)1270mm, Koike Imatec                       	 6PD72-1          	2080	34169204	  Paper Sheet DAIKEN D18P MT8 (PS)1270mm, Koike Imatec                       	1	1	 6PD72-1          	5	1
309	  Paper Sheet DAIKEN D18P MT8 YOKO (PS)1270mm, Koike Imatec                  	 6PD79-1          	1720	27323356	  Paper Sheet DAIKEN D18P MT8 YOKO (PS)1270mm, Koike Imatec                  	1	1	 6PD79-1          	5	1
310	  Paper Sheet DAIKEN D18P WH8 (PS)1270mm, Koike Imatec                       	 6PD73-1          	7040	114745233	  Paper Sheet DAIKEN D18P WH8 (PS)1270mm, Koike Imatec                       	1	1	 6PD73-1          	5	1
311	  Paper Sheet DAIKEN D18P WH8 YOKO (PS)1270mm, Koike Imatec                  	 6PD80-1          	4100	65703024	  Paper Sheet DAIKEN D18P WH8 YOKO (PS)1270mm, Koike Imatec                  	1	1	 6PD80-1          	5	1
312	  Paper Sheet DAIKEN S18P MK8 (PS)1270mm, Koike Imatec                       	 6PD74-1          	2000	33356588	  Paper Sheet DAIKEN S18P MK8 (PS)1270mm, Koike Imatec                       	1	1	 6PD74-1          	5	1
313	  Paper Sheet DAIKEN S18P MK8 YOKO (PS)1270mm, Koike Imatec                  	 6PD75-1          	1060	16957082	  Paper Sheet DAIKEN S18P MK8 YOKO (PS)1270mm, Koike Imatec                  	1	1	 6PD75-1          	5	1
314	  Paper Sheet DAIKEN T18P MA8 (PS)1270mm, Koike Imatec                       	 6PD76-1          	4900	76592150	  Paper Sheet DAIKEN T18P MA8 (PS)1270mm, Koike Imatec                       	1	1	 6PD76-1          	5	1
315	  Paper Sheet DAIKEN T18P MA8 YOKO (PS)1270mm, Koike Imatec                  	 6PD81-1          	1310	21119248	  Paper Sheet DAIKEN T18P MA8 YOKO (PS)1270mm, Koike Imatec                  	1	1	 6PD81-1          	5	1
316	  Paper Sheet DAIKEN T18P ML8 (PS)1270mm, Koike Imatec                       	 6PD82-1          	5200	84636983	  Paper Sheet DAIKEN T18P ML8 (PS)1270mm, Koike Imatec                       	1	1	 6PD82-1          	5	1
317	  Paper Sheet DAIKEN T18P ML8 YOKO (PS)1270mm, Koike Imatec                  	 6PD83-1          	2070	32104584	  Paper Sheet DAIKEN T18P ML8 YOKO (PS)1270mm, Koike Imatec                  	1	1	 6PD83-1          	5	1
318	  Paper Sheet DAIKEN D15P MW 1270mm, Koike Imatec                            	 6PD57-1          	3000	48800024	  Paper Sheet DAIKEN D15P MW 1270mm, Koike Imatec                            	1	1	 6PD57-1          	5	1
319	  Paper Sheet DAIKEN D15P MW-YOKO 1270mm, Koike Imatec                       	 6PD58-1          	2000	33483354	  Paper Sheet DAIKEN D15P MW-YOKO 1270mm, Koike Imatec                       	1	1	 6PD58-1          	5	1
320	  Paper Sheet DAIKEN SANDGREY (gure coated} 1250mm, Koike Imatec             	 6PD41-1          	2100	13901328	  Paper Sheet DAIKEN SANDGREY (gure coated} 1250mm, Koike Imatec             	1	1	 6PD41-1          	5	1
321	  Paper Sheet DAIKEN VS25 1250mm, Koike Imatec                               	 6PD42-1          	7390	57939213	  Paper Sheet DAIKEN VS25 1250mm, Koike Imatec                               	1	1	 6PD42-1          	5	1
322	  ORIFIEN SHEET DAIKEN D09MT-100 1270mm, Koike Imatec                        	 6OD4-1           	387	0	  ORIFIEN SHEET DAIKEN D09MT-100 1270mm, Koike Imatec                        	1	1	 6OD4-1           	5	1
323	  ORIFIEN SHEET DAIKEN D10MK-120 1270mm, Koike Imatec                        	 6OD7-1           	253	0	  ORIFIEN SHEET DAIKEN D10MK-120 1270mm, Koike Imatec                        	1	1	 6OD7-1           	5	1
324	  ORIFIEN SHEET DAIKEN T15-TH150 1260mm, Koike Imatec                        	 6OD9-1           	3405	99190589	  ORIFIEN SHEET DAIKEN T15-TH150 1260mm, Koike Imatec                        	1	1	 6OD9-1           	5	1
325	  EP DAIKEN SMAR 30KD 30mm, Daiken                                           	 6ED1-1           	50	0	  EP DAIKEN SMAR 30KD 30mm, Daiken                                           	1	1	 6ED1-1           	5	1
326	  EP DAIKEN BD 2x38mm, Daiken C0                                             	 6ED70-1          	840	0	  EP DAIKEN BD 2x38mm, Daiken C0                                             	1	1	 6ED70-1          	5	1
327	  ET DAIKEN BD 0.5x38mm, Daiken Co                                           	 6ED71-1          	320	0	  ET DAIKEN BD 0.5x38mm, Daiken Co                                           	1	1	 6ED71-1          	5	1
328	  ET DAIKEN SMAR (WHITE) 38mm, Daiken                                        	 6ED47-1          	100	0	  ET DAIKEN SMAR (WHITE) 38mm, Daiken                                        	1	1	 6ED47-1          	5	1
329	  ET DAIKEN SMAR (DARK) 38mm, Daiken                                         	 6ED44-1          	600	0	  ET DAIKEN SMAR (DARK) 38mm, Daiken                                         	1	1	 6ED44-1          	5	1
330	  ET DAIKEN DARK(D) 21mm, Daiken                                             	 6ED4-1           	300	0	  ET DAIKEN DARK(D) 21mm, Daiken                                             	1	1	 6ED4-1           	5	1
331	  ET DAIKEN D15 MW-100 18mm,  Koike Imatec                                   	 6ED67-1          	746	0	  ET DAIKEN D15 MW-100 18mm,  Koike Imatec                                   	1	1	 6ED67-1          	5	1
332	  ET DAIKEN D15 WH-100 18mm,  Koike Imatec                                   	 6ED68-1          	188	0	  ET DAIKEN D15 WH-100 18mm,  Koike Imatec                                   	1	1	 6ED68-1          	5	1
333	  ET DAIKEN D15 MA-100 18mm,  Koike Imatec                                   	 6ED61-1          	264	0	  ET DAIKEN D15 MA-100 18mm,  Koike Imatec                                   	1	1	 6ED61-1          	5	1
334	  ET DAIKEN D14 MT-100 18mm,  Koike Imatec                                   	 6ED60-1          	350	0	  ET DAIKEN D14 MT-100 18mm,  Koike Imatec                                   	1	1	 6ED60-1          	5	1
335	  ET DAIKEN D15 MT-100 18mm,  Koike Imatec                                   	 6ED73-1          	289	0	  ET DAIKEN D15 MT-100 18mm,  Koike Imatec                                   	1	1	 6ED73-1          	5	1
336	  ET DAIKEN D15 MK-100 18mm,  Koike Imatec                                   	 6ED75-1          	507	0	  ET DAIKEN D15 MK-100 18mm,  Koike Imatec                                   	1	1	 6ED75-1          	5	1
337	  ET DAIKEN D15 ML-100 18mm,  Koike Imatec                                   	 6ED62-1          	630	0	  ET DAIKEN D15 ML-100 18mm,  Koike Imatec                                   	1	1	 6ED62-1          	5	1
338	  ET DAIKEN TA4603DB 38mm, Koike Imatec                                      	 6ED19-1          	480	0	  ET DAIKEN TA4603DB 38mm, Koike Imatec                                      	1	1	 6ED19-1          	5	1
339	  ET DAIKEN TA4603DB 39mm, Daiken                                            	 6ED19-2          	400	0	  ET DAIKEN TA4603DB 39mm, Daiken                                            	1	1	 6ED19-2          	5	1
340	  ET DAIKEN TA4670 18mm, Koike Imatec                                        	 6ED20-2          	34000	65095826	  ET DAIKEN TA4670 18mm, Koike Imatec                                        	1	1	 6ED20-2          	5	1
341	  ET DAIKEN TA4670 24mm, Koike Imatec                                        	 6ED20-1          	40790	103331533	  ET DAIKEN TA4670 24mm, Koike Imatec                                        	1	1	 6ED20-1          	5	1
342	  ET DAIKEN NWAM 3350MW 0.5mmx38mm, Koike Imatec                             	 6ED23-1          	130	0	  ET DAIKEN NWAM 3350MW 0.5mmx38mm, Koike Imatec                             	1	1	 6ED23-1          	5	1
343	  ET DAIKEN NWAL 4122 SANDGREY 18mm, Koike Imatecs                           	 6ED24-1          	2490	5080183	  ET DAIKEN NWAL 4122 SANDGREY 18mm, Koike Imatecs                           	1	1	 6ED24-1          	5	1
344	  ET DAIKEN NWAM 3241WH 0.5mmx38mm, Koike Imatec                             	 6ED25-1          	1300	0	  ET DAIKEN NWAM 3241WH 0.5mmx38mm, Koike Imatec                             	1	1	 6ED25-1          	5	1
345	  ET DAIKEN NWAM 3349MT 0.5mmx38mm, Koike Imatec                             	 6ED26-1          	2550	0	  ET DAIKEN NWAM 3349MT 0.5mmx38mm, Koike Imatec                             	1	1	 6ED26-1          	5	1
346	  ET DAIKEN NWAM 3351ML 0.5mmx38mm, Koike Imatec                             	 6ED27-1          	1740	0	  ET DAIKEN NWAM 3351ML 0.5mmx38mm, Koike Imatec                             	1	1	 6ED27-1          	5	1
347	  ET DAIKEN NWAM 3352MF 0.5mmx38mm, Koike Imatec                             	 6ED28-1          	610	0	  ET DAIKEN NWAM 3352MF 0.5mmx38mm, Koike Imatec                             	1	1	 6ED28-1          	5	1
348	  EP DAIKEN NW40-3820-1759E ML 2mmx38mm, Koike Imatec                        	 6ED29-1          	820	0	  EP DAIKEN NW40-3820-1759E ML 2mmx38mm, Koike Imatec                        	1	1	 6ED29-1          	5	1
349	  EP DAIKEN NW40-3820-1188 WH 2mmx38mm, Koike Imatec                         	 6ED32-1          	35	0	  EP DAIKEN NW40-3820-1188 WH 2mmx38mm, Koike Imatec                         	1	1	 6ED32-1          	5	1
350	  EP DAIKEN SR3 (WUB 01-370 D)38 MT 38mm, Koike Imatec                       	 6ED33-1          	2250	0	  EP DAIKEN SR3 (WUB 01-370 D)38 MT 38mm, Koike Imatec                       	1	1	 6ED33-1          	5	1
351	  EP DAIKEN SR3 (MUB 02-650 D)38 MW 38mm, Koike Imatec                       	 6ED34-1          	4110	0	  EP DAIKEN SR3 (MUB 02-650 D)38 MW 38mm, Koike Imatec                       	1	1	 6ED34-1          	5	1
352	  ET DAIKEN TACO 508  22mm, SUMBER SELAMAT                                   	 6ED54-3          	75	0	  ET DAIKEN TACO 508  22mm, SUMBER SELAMAT                                   	1	1	 6ED54-3          	5	1
353	  ET DAIKEN TACO 508 2mmx34mm, SUMBER SELAMAT                                	 6ED54-2          	50	0	  ET DAIKEN TACO 508 2mmx34mm, SUMBER SELAMAT                                	1	1	 6ED54-2          	5	1
354	  ET DAIKEN TACO 002G 2mmx42mm, SUMBER SELAMAT                               	 6ED50-1          	60	0	  ET DAIKEN TACO 002G 2mmx42mm, SUMBER SELAMAT                               	1	1	 6ED50-1          	5	1
355	  ET DAIKEN TACO 512 2mmx42mm, SUMBER SELAMAT                                	 6ED53-1          	200	0	  ET DAIKEN TACO 512 2mmx42mm, SUMBER SELAMAT                                	1	1	 6ED53-1          	5	1
356	  EP DAIKEN TACO 903 1mmx42mm, PT. SUKSES WIJAYA PANEL                       	 6ED78-1          	322	1460328	  EP DAIKEN TACO 903 1mmx42mm, PT. SUKSES WIJAYA PANEL                       	1	1	 6ED78-1          	5	1
357	  EP DAIKEN TACO 903 1mmx42mm, SUMBER SELAMAT                                	 6ED78-2          	400	1814072	  EP DAIKEN TACO 903 1mmx42mm, SUMBER SELAMAT                                	1	1	 6ED78-2          	5	1
358	  EP DAIKEN TACO 508 2mmx42mm, SUMBER SELAMAT                                	 6ED54-1          	400	0	  EP DAIKEN TACO 508 2mmx42mm, SUMBER SELAMAT                                	1	1	 6ED54-1          	5	1
359	  EP DAIKEN TACO 504 2mmx42mm, SUMBER SELAMAT                                	 6ED69-1          	30	0	  EP DAIKEN TACO 504 2mmx42mm, SUMBER SELAMAT                                	1	1	 6ED69-1          	5	1
360	  EP DAIKEN TACO 106 2mmx42mm, SUMBER SELAMAT                                	 6ED72-1          	95	0	  EP DAIKEN TACO 106 2mmx42mm, SUMBER SELAMAT                                	1	1	 6ED72-1          	5	1
361	  ET DAIKEN TACO 805 2mmx42mm, SUMBER SELAMAT                                	 6ED63-1          	150	0	  ET DAIKEN TACO 805 2mmx42mm, SUMBER SELAMAT                                	1	1	 6ED63-1          	5	1
362	  ET DAIKEN TACO 003 2mmx42mm, SUMBER SELAMAT                                	 6ED55-1          	300	0	  ET DAIKEN TACO 003 2mmx42mm, SUMBER SELAMAT                                	1	1	 6ED55-1          	5	1
363	  EP DAIKEN TACO 270 2mmx42mm, SUMBER SELAMAT                                	 6ED74-1          	95	0	  EP DAIKEN TACO 270 2mmx42mm, SUMBER SELAMAT                                	1	1	 6ED74-1          	5	1
364	  ET DAIKEN TACO 300H 2mmx42mm, SUMBER SELAMAT                               	 6ED51-1          	1329	0	  ET DAIKEN TACO 300H 2mmx42mm, SUMBER SELAMAT                               	1	1	 6ED51-1          	5	1
365	  ET DAIKEN TACO 1206 2mmx55mm, SUMBER SELAMAT                               	 6ED52-2          	200	1909200	  ET DAIKEN TACO 1206 2mmx55mm, SUMBER SELAMAT                               	1	1	 6ED52-2          	5	1
366	  ET DAIKEN TACO 200AA 2mmx42mm, SUMBER SELAMAT                              	 6ED56-1          	200	0	  ET DAIKEN TACO 200AA 2mmx42mm, SUMBER SELAMAT                              	1	1	 6ED56-1          	5	1
367	  ET DAIKEN TACO 355 2mmx42mm, SUMBER SELAMAT                                	 6ED49-1          	525	0	  ET DAIKEN TACO 355 2mmx42mm, SUMBER SELAMAT                                	1	1	 6ED49-1          	5	1
368	  ET DAIKEN WH07 0.52x1250mm, Meiwa                                          	 6ED35-1          	1590	90488359	  ET DAIKEN WH07 0.52x1250mm, Meiwa                                          	1	1	 6ED35-1          	5	1
369	  ET DAIKEN DK010 ( WG/grey) 0.46mmx1250mm, Meiwa                            	 6ED36-1          	270	0	  ET DAIKEN DK010 ( WG/grey) 0.46mmx1250mm, Meiwa                            	1	1	 6ED36-1          	5	1
370	  ET DAIKEN BR 117DK01 (TG PVC) 0.46mmx1250mm, Meiwa                         	 6ED37-1          	345	0	  ET DAIKEN BR 117DK01 (TG PVC) 0.46mmx1250mm, Meiwa                         	1	1	 6ED37-1          	5	1
371	  ET DAIKEN BR 129 DK01 1250mm, MEIWA                                        	 6ED66-1          	80	0	  ET DAIKEN BR 129 DK01 1250mm, MEIWA                                        	1	1	 6ED66-1          	5	1
372	  EP DAIKEN EP.535 2mmx42mm, FIO HOME                                        	 6ED39-1          	40	0	  EP DAIKEN EP.535 2mmx42mm, FIO HOME                                        	1	1	 6ED39-1          	5	1
373	  EP DAIKEN EP.557 2mmx42mm, FIO HOME                                        	 6ED40-1          	90	0	  EP DAIKEN EP.557 2mmx42mm, FIO HOME                                        	1	1	 6ED40-1          	5	1
374	  ET DAIKEN DKC1082N GR06  0.42mmx1250mm, MEIWA                              	 6ED57-1          	455	0	  ET DAIKEN DKC1082N GR06  0.42mmx1250mm, MEIWA                              	1	1	 6ED57-1          	5	1
375	  ET DAIKEN DKC2003N BLACK  0.46mmx1250mm, MEIWA                             	 6ED42-1          	300	0	  ET DAIKEN DKC2003N BLACK  0.46mmx1250mm, MEIWA                             	1	1	 6ED42-1          	5	1
376	  ET DAIKEN DKC3004N  BR118 DK01  1250mm, MEIWA                              	 6ED45-1          	360	0	  ET DAIKEN DKC3004N  BR118 DK01  1250mm, MEIWA                              	1	1	 6ED45-1          	5	1
377	  ET DAIKEN DKC3004 N18 BR123  0.46mmx1250mm, MEIWA                          	 6ED43-1          	375	0	  ET DAIKEN DKC3004 N18 BR123  0.46mmx1250mm, MEIWA                          	1	1	 6ED43-1          	5	1
378	  ET DAIKEN DKC3004N BR127 DK01 0.46mmx1250mm, Meiwa                         	 6ED65-1          	390	0	  ET DAIKEN DKC3004N BR127 DK01 0.46mmx1250mm, Meiwa                         	1	1	 6ED65-1          	5	1
379	  Paper Sheet MKI-ORIGINAL P6175SC 1260mm White, Toppan Cosmo                	 6PM1-1           	5975	0	  Paper Sheet MKI-ORIGINAL P6175SC 1260mm White, Toppan Cosmo                	1	1	 6PM1-1           	5	1
380	  Paper Sheet MKI-ORIGINAL P5522TSU  50gr 1250mm, Toppan Cosmo               	 6PM2-1           	5600	0	  Paper Sheet MKI-ORIGINAL P5522TSU  50gr 1250mm, Toppan Cosmo               	1	1	 6PM2-1           	5	1
381	  Paper 50gr MKI-ORIGINAL P0774SC 1250mm White, Toppan Cosmo                 	 6PM3-1           	281	0	  Paper 50gr MKI-ORIGINAL P0774SC 1250mm White, Toppan Cosmo                 	1	1	 6PM3-1           	5	1
382	  Paper 50gr MKI-ORIGINAL P0897SC 1250mm Black, Toppan Cosmo                 	 6PM4-1           	490	0	  Paper 50gr MKI-ORIGINAL P0897SC 1250mm Black, Toppan Cosmo                 	1	1	 6PM4-1           	5	1
383	  Paper sheet  RAKUDA CE.002 1250mm, KOIKE Imatec                            	 6PR1-1           	1905	0	  Paper sheet  RAKUDA CE.002 1250mm, KOIKE Imatec                            	1	1	 6PR1-1           	5	1
384	  Paper 50gr NISSEN P0774CTSU White 1250mm, Toppan Cosmo                     	 6PN1-1           	4950	25275547	  Paper 50gr NISSEN P0774CTSU White 1250mm, Toppan Cosmo                     	1	1	 6PN1-1           	5	1
385	  ORIFINE SHEET ALKORCELL 0.3x1250 MG BLACK, TOPAN COSMO                     	 6O2-1            	3135	0	  ORIFINE SHEET ALKORCELL 0.3x1250 MG BLACK, TOPAN COSMO                     	1	1	 6O2-1            	5	1
386	  Paper Sheet NAS CE009 1250mm, Koike imatec                                 	 6PN2-1           	980	11898094	  Paper Sheet NAS CE009 1250mm, Koike imatec                                 	1	1	 6PN2-1           	5	1
387	  Paper Sheet NAS WHITE 1270mm, Toppan C                                     	 6PN3-1           	17100	0	  Paper Sheet NAS WHITE 1270mm, Toppan C                                     	1	1	 6PN3-1           	5	1
388	  Paper Sheet NAS Charcoal Gray CE01 1270mm, KOIKE IMATEC                    	 6PN4-2           	3920	37481006	  Paper Sheet NAS Charcoal Gray CE01 1270mm, KOIKE IMATEC                    	1	1	 6PN4-2           	5	1
389	  Paper NAS P5812TSU Brown 1250mm, Topan Cosmo                               	 6PN8-2           	15400	60525943	  Paper NAS P5812TSU Brown 1250mm, Topan Cosmo                               	1	1	 6PN8-2           	5	1
390	  Paper NAS GM 30G PM 1250mm, TOPAN C                                        	 6PN17-1          	3900	45318872	  Paper NAS GM 30G PM 1250mm, TOPAN C                                        	1	1	 6PN17-1          	5	1
391	  Paper NAS 30G New PM 970mm, TOPAN C                                        	 6PN17-4          	689	0	  Paper NAS 30G New PM 970mm, TOPAN C                                        	1	1	 6PN17-4          	5	1
392	  Paper NAS GM 30G SM 1250mm, Topan Cosmo                                    	 6PN19-1          	3600	41128474	  Paper NAS GM 30G SM 1250mm, Topan Cosmo                                    	1	1	 6PN19-1          	5	1
393	  Paper NAS GM 65G MB 1270mm, Topan Cosmo                                    	 6PN37-1          	475	0	  Paper NAS GM 65G MB 1270mm, Topan Cosmo                                    	1	1	 6PN37-1          	5	1
394	  Paper NAS GM 30G MB 1270mm, Topan Cosmo                                    	 6PN36-9          	4200	48815640	  Paper NAS GM 30G MB 1270mm, Topan Cosmo                                    	1	1	 6PN36-9          	5	1
395	  Paper Sheet NAS DB 30gr 1270mm, Toppan C                                   	 6PN38-1          	5250	61867513	  Paper Sheet NAS DB 30gr 1270mm, Toppan C                                   	1	1	 6PN38-1          	5	1
396	  Paper Sheet  NAS 30gr PW 1250mm, Topan Cosmo                               	 6PN23-1          	9380	84234215	  Paper Sheet  NAS 30gr PW 1250mm, Topan Cosmo                               	1	1	 6PN23-1          	5	1
397	  Paper Sheet NAS 65gr PW 1250mm, Topan Cosmo                                	 6PN24-1          	1400	23489289	  Paper Sheet NAS 65gr PW 1250mm, Topan Cosmo                                	1	1	 6PN24-1          	5	1
398	  Paper Sheet NAS DB 65gr 1270mm, TOPPAN Printing                            	 6PN35-1          	2100	36372059	  Paper Sheet NAS DB 65gr 1270mm, TOPPAN Printing                            	1	1	 6PN35-1          	5	1
399	  Paper NAS GM 65G PM 1250mm, Topan Cosmo                                    	 6PN27-1          	2940	51266793	  Paper NAS GM 65G PM 1250mm, Topan Cosmo                                    	1	1	 6PN27-1          	5	1
400	  Paper NAS GM 65G SM 1250mm, Topan Cosmo                                    	 6PN29-1          	2040	35053230	  Paper NAS GM 65G SM 1250mm, Topan Cosmo                                    	1	1	 6PN29-1          	5	1
401	  Paper Sheet NAS N600 White 1260mm, Topan Cosmo                             	 6PN33-1          	3270	27734093	  Paper Sheet NAS N600 White 1260mm, Topan Cosmo                             	1	1	 6PN33-1          	5	1
402	  Paper Sheet NAS N600 White 951mm, Topan Cosmo                              	 6PN33-2          	3970	37001387	  Paper Sheet NAS N600 White 951mm, Topan Cosmo                              	1	1	 6PN33-2          	5	1
403	  ET Orifien NAS SC40-1910-P0027 19mm, Koike Imatec                          	 6EN21-1          	0	0	  ET Orifien NAS SC40-1910-P0027 19mm, Koike Imatec                          	1	1	 6EN21-1          	5	1
404	  EP GREY NAS (No.19-154mm)/SC40.1810-B0166THOO 1mmx18mm, PANEFRI INDUSTRIAL 	 6EN22-1          	36000	105344280	  EP GREY NAS (No.19-154mm)/SC40.1810-B0166THOO 1mmx18mm, PANEFRI INDUSTRIAL 	1	1	 6EN22-1          	5	1
405	  ET NAS TA 4660 0.45x18mm, Topan Cosmo                                      	 6EN10-1          	31240	59307373	  ET NAS TA 4660 0.45x18mm, Topan Cosmo                                      	1	1	 6EN10-1          	5	1
406	  ET NAS TA 4660 21mm, Topan Cosmo                                           	 6EN10-2          	1950	4321276	  ET NAS TA 4660 21mm, Topan Cosmo                                           	1	1	 6EN10-2          	5	1
407	  ET NAS TA 4660 23mm, Topan Cosmo                                           	 6EN10-3          	3200	6984298	  ET NAS TA 4660 23mm, Topan Cosmo                                           	1	1	 6EN10-3          	5	1
408	  ET NAS TA 4685 18mm, TOPAN COSMO        (NG)                               	 6EN12-1          	7000	13960571	  ET NAS TA 4685 18mm, TOPAN COSMO        (NG)                               	1	1	 6EN12-1          	5	1
409	  ET NAS TA 4685 18mm, KOIKE IMATEC                                          	 6EN12-4          	15970	31850047	  ET NAS TA 4685 18mm, KOIKE IMATEC                                          	1	1	 6EN12-4          	5	1
410	  ET NAS TA 4685 24mm, TOPAN C                                               	 6EN12-2          	16040	41297773	  ET NAS TA 4685 24mm, TOPAN C                                               	1	1	 6EN12-2          	5	1
411	  ET NAS TA 4774 18mm, KOIKE IMATEC                                          	 6EN20-1          	600	1136269	  ET NAS TA 4774 18mm, KOIKE IMATEC                                          	1	1	 6EN20-1          	5	1
412	  ET NAS TA 4774 22mm, KOIKE IMATEC                                          	 6EN20-3          	800	1571614	  ET NAS TA 4774 22mm, KOIKE IMATEC                                          	1	1	 6EN20-3          	5	1
413	  ET NAS TA 4774 24mm, KOIKE IMATEC                                          	 6EN20-2          	1000	2538639	  ET NAS TA 4774 24mm, KOIKE IMATEC                                          	1	1	 6EN20-2          	5	1
414	  Pvc TENMA PROTECT FILM 4160 1000mm, NOVACEL                                	 6PT5-7           	29800	163931367	  Pvc TENMA PROTECT FILM 4160 1000mm, NOVACEL                                	1	1	 6PT5-7           	5	1
415	  Pvc TENMA PROTECT FILM 4334  500mm, NOVACEL                                	 6PT5-6           	109000	291750567	  Pvc TENMA PROTECT FILM 4334  500mm, NOVACEL                                	1	1	 6PT5-6           	5	1
416	  Pvc TENMA PROTECT FILM (SL 26) 1250mm, TOPAN PRINTING                      	 6PT5-4           	4000	71347369	  Pvc TENMA PROTECT FILM (SL 26) 1250mm, TOPAN PRINTING                      	1	1	 6PT5-4           	5	1
417	  Pvc TENMA SF10014-03T White ( 0.2 )1245mm, Topan Cosmo                     	 6PT7-3           	2920	88660398	  Pvc TENMA SF10014-03T White ( 0.2 )1245mm, Topan Cosmo                     	1	1	 6PT7-3           	5	1
418	  Pvc TENMA WF40316-27TL Black 1245mm, Topan Cosmo                           	 6PT8-1           	401	0	  Pvc TENMA WF40316-27TL Black 1245mm, Topan Cosmo                           	1	1	 6PT8-1           	5	1
419	  Pvc TENMA WF11604-27T ( 0.17mm) Linear Walnut (OAK) 1245mm, Topan Cosmo    	 6PT9-1           	60	0	  Pvc TENMA WF11604-27T ( 0.17mm) Linear Walnut (OAK) 1245mm, Topan Cosmo    	1	1	 6PT9-1           	5	1
420	  Pvc TENMA WF25048T (0.17 mm)Maple 1245mm, Topan Cosmo                      	 6PT6-1           	88	0	  Pvc TENMA WF25048T (0.17 mm)Maple 1245mm, Topan Cosmo                      	1	1	 6PT6-1           	5	1
421	  Pvc TENMA  WF25048-81T (No Protection Film) 1245mm, TOPAN COSMO            	 6PT16-3          	191	0	  Pvc TENMA  WF25048-81T (No Protection Film) 1245mm, TOPAN COSMO            	1	1	 6PT16-3          	5	1
422	  Pvc TENMA 7H40007-003 (MAPLE) 0.18mmx1260mm, FOSHAN T.P                    	 6PT17-1          	7512	100894802	  Pvc TENMA 7H40007-003 (MAPLE) 0.18mmx1260mm, FOSHAN T.P                    	1	1	 6PT17-1          	5	1
423	  HPL TH003AA TACO BLACK,  Sumber Selamat                                    	 6H36-1           	55	6125020	  HPL TH003AA TACO BLACK,  Sumber Selamat                                    	1	1	 6H36-1           	27	1
424	  HPL TH382H, PT. SUKSES WIJAYA PANEL                                        	 6H30-1           	37	5124500	  HPL TH382H, PT. SUKSES WIJAYA PANEL                                        	1	1	 6H30-1           	27	1
425	  HPL TH382H TACO GREY RECOMPOSED WOOD, SUMBER SELAMAT                       	 6H30-2           	87	12021834	  HPL TH382H TACO GREY RECOMPOSED WOOD, SUMBER SELAMAT                       	1	1	 6H30-2           	27	1
426	  HPL  TH325H TACO BROWN KRAFWOOD,  Sumber Selamat                           	 6H18-1           	1	0	  HPL  TH325H TACO BROWN KRAFWOOD,  Sumber Selamat                           	1	1	 6H18-1           	27	1
427	  HPL  SHG-1051 GLOSS WHEAT, PT. TAK PROD.                                   	 6H19-1           	2	0	  HPL  SHG-1051 GLOSS WHEAT, PT. TAK PROD.                                   	1	1	 6H19-1           	27	1
428	  HPL  SHG-1150 HIGH GLOSS TAUPEI, PT. TAK PROD.                             	 6H20-1           	3	0	  HPL  SHG-1150 HIGH GLOSS TAUPEI, PT. TAK PROD.                             	1	1	 6H20-1           	27	1
429	  HPL  SHG-1154 HIGH GLOSS BLUEBERRY, PT. TAK PROD.                          	 6H21-1           	3	0	  HPL  SHG-1154 HIGH GLOSS BLUEBERRY, PT. TAK PROD.                          	1	1	 6H21-1           	27	1
430	  HPL  SHG-1155 HIGH GLOSS FROST, PT. TAK PROD.                              	 6H22-1           	3	0	  HPL  SHG-1155 HIGH GLOSS FROST, PT. TAK PROD.                              	1	1	 6H22-1           	27	1
431	  HPL  SHG-1159 GLOSS CELADON, PT. TAK PROD.                                 	 6H23-1           	2	0	  HPL  SHG-1159 GLOSS CELADON, PT. TAK PROD.                                 	1	1	 6H23-1           	27	1
432	  HPL  TACO WHITE VIR (TH355H),  Sumber Selamat                              	 6H25-1           	3	0	  HPL  TACO WHITE VIR (TH355H),  Sumber Selamat                              	1	1	 6H25-1           	27	1
433	  HPL AICA MB041DN74 MB SMOKED ENGLISH OAK,  Sumber Selamat                  	 6H34-1           	23	3868186	  HPL AICA MB041DN74 MB SMOKED ENGLISH OAK,  Sumber Selamat                  	1	1	 6H34-1           	27	1
434	  HPL AICA MB0008DN74 MB WHITE CEDAR,  Sumber Selamat                        	 6H35-1           	11	1850002	  HPL AICA MB0008DN74 MB WHITE CEDAR,  Sumber Selamat                        	1	1	 6H35-1           	27	1
435	  HPL ABK 06 SS  1220x2440,  Marga Bharata                                   	 6H6-1            	1	0	  HPL ABK 06 SS  1220x2440,  Marga Bharata                                   	1	1	 6H6-1            	27	1
436	  HPL /ABT:300C55 0.8x1220x2440,  Marga Bharata                              	 6H1-1            	19	0	  HPL /ABT:300C55 0.8x1220x2440,  Marga Bharata                              	1	1	 6H1-1            	27	1
437	  HPL /AS-13004 CN74 0.8x1220x2440,  Marga Bharata                           	 6H11-1           	33	0	  HPL /AS-13004 CN74 0.8x1220x2440,  Marga Bharata                           	1	1	 6H11-1           	27	1
438	  HPL /AS-13012 CN74 0.8x1220x2440,  Marga Bharata                           	 6H12-1           	3	0	  HPL /AS-13012 CN74 0.8x1220x2440,  Marga Bharata                           	1	1	 6H12-1           	27	1
439	  HPL /AS-14024 CS99 0.8x1220x2440,  Marga Bharata                           	 6H4-1            	14	0	  HPL /AS-14024 CS99 0.8x1220x2440,  Marga Bharata                           	1	1	 6H4-1            	27	1
440	  HPL /AS-14015 CS98 (AICA LIGHT BERRY TEAK) 0.8x1220x2440, Sumber Selamat   	 6H13-1           	11	0	  HPL /AS-14015 CS98 (AICA LIGHT BERRY TEAK) 0.8x1220x2440, Sumber Selamat   	1	1	 6H13-1           	27	1
441	  HPL AS14014CS62 AICA MEDIUM RETRO TEAK/STRAIGHT GRAIN,  Sumber Selamat     	 6H33-1           	1	298000	  HPL AS14014CS62 AICA MEDIUM RETRO TEAK/STRAIGHT GRAIN,  Sumber Selamat     	1	1	 6H33-1           	27	1
442	  HPL /TJ-10 C174 0.8x1220x2440,  Marga Bharata                              	 6H5-1            	43	0	  HPL /TJ-10 C174 0.8x1220x2440,  Marga Bharata                              	1	1	 6H5-1            	27	1
443	  OTHERSGUMMED TAPE (fuji)W =19mm x 550mm TANPA LUBANG, PARTIWA A            	 6O53-3           	11	695018	  OTHERSGUMMED TAPE (fuji)W =19mm x 550mm TANPA LUBANG, PARTIWA A            	1	1	 6O53-3           	27	1
444	  OTHERSPaperP 5780 SPU1250mmOrchard	 6O88-1	185	0	  OTHERSPaperP 5780 SPU1250mmOrchard	1	1	 6O88-1	27	1
445	  OTHERSPIPA BESI PVC	 6O140-1	7	0	  OTHERSPIPA BESI PVC	1	1	 6O140-1	27	1
446	  OTHERS Orifien Sheet  EB.CONCER WALNUT (TY) 1240mm, MARUFUJI KK            	 6O174-1          	28	0	  OTHERS Orifien Sheet  EB.CONCER WALNUT (TY) 1240mm, MARUFUJI KK            	1	1	 6O174-1          	5	1
447	  OTHERS ORIFIN SHEET ANTIC WHITE 1260mm, MARUFUJI KENZAI                    	 6O180-1          	29	0	  OTHERS ORIFIN SHEET ANTIC WHITE 1260mm, MARUFUJI KENZAI                    	1	1	 6O180-1          	5	1
448	  OTHERS PVC SHEET P85085 0.20x1250mm, HUI KONG/FOSHAN                       	 6O181-1          	40	0	  OTHERS PVC SHEET P85085 0.20x1250mm, HUI KONG/FOSHAN                       	1	1	 6O181-1          	5	1
449	  OTHERS PVC SHEET S502-A 0.20x1250mm, HUI KONG/FOSHAN                       	 6O181-4          	46	0	  OTHERS PVC SHEET S502-A 0.20x1250mm, HUI KONG/FOSHAN                       	1	1	 6O181-4          	5	1
450	  OTHERS PVC SHEET S46651-A 0.20x1250mm, HUI KONG/FOSHAN                     	 6O181-5          	46	0	  OTHERS PVC SHEET S46651-A 0.20x1250mm, HUI KONG/FOSHAN                     	1	1	 6O181-5          	5	1
451	  Orefin DAITO SELENA WALNUT  W-420mm, Koike Imatec                          	 6OD8-2           	115	0	  Orefin DAITO SELENA WALNUT  W-420mm, Koike Imatec                          	1	1	 6OD8-2           	5	1
452	  Paper Sheet DAITO SELENA WALNUT 30VS 530mm, Koike Imatec                   	 6PD61-10         	60	605303	  Paper Sheet DAITO SELENA WALNUT 30VS 530mm, Koike Imatec                   	1	1	 6PD61-10         	5	1
453	  Paper Sheet DAITO SELENA WALNUT 30VS 450mm, Koike Imatec                   	 6PD61-2          	0	0	  Paper Sheet DAITO SELENA WALNUT 30VS 450mm, Koike Imatec                   	1	1	 6PD61-2          	5	1
454	  Paper Sheet DAITO SELENA WALNUT 30VS 300mm, Koike Imatec                   	 6PD61-4          	350	2001216	  Paper Sheet DAITO SELENA WALNUT 30VS 300mm, Koike Imatec                   	1	1	 6PD61-4          	5	1
455	  Paper Sheet DAITO SELENA WALNUT 30VS 115mm, Koike Imatec                   	 6PD61-5          	0	0	  Paper Sheet DAITO SELENA WALNUT 30VS 115mm, Koike Imatec                   	1	1	 6PD61-5          	5	1
456	  Paper Sheet FUKAI P0897TSU 980mm, Orchard Taiwan                           	 6PF1-6           	122	0	  Paper Sheet FUKAI P0897TSU 980mm, Orchard Taiwan                           	1	1	 6PF1-6           	5	1
457	  Paper Sheet FUKAI P0897TSU 970mm, Orchard Taiwan                           	 6PF1-3           	238	0	  Paper Sheet FUKAI P0897TSU 970mm, Orchard Taiwan                           	1	1	 6PF1-3           	5	1
458	  Paper Sheet FUKAI P0897TSU 870mm, Orchard Taiwan                           	 6PF1-5           	436	0	  Paper Sheet FUKAI P0897TSU 870mm, Orchard Taiwan                           	1	1	 6PF1-5           	5	1
459	  Paper Sheet FUKAI P0897TSU 410mm, Orchard Taiwan                           	 6PF1-4           	720	0	  Paper Sheet FUKAI P0897TSU 410mm, Orchard Taiwan                           	1	1	 6PF1-4           	5	1
460	  PVC FUKAI S502-PB (BLACK SANDEMBOSS) 0.12mmx475mm, FOSHAN T                	 6PF26-3          	390	586995	  PVC FUKAI S502-PB (BLACK SANDEMBOSS) 0.12mmx475mm, FOSHAN T                	1	1	 6PF26-3          	5	1
461	  PVC FUKAI H60120-003(Walnut) 0.15mmx630mm, FOSHAN TP                       	 6PF28-2          	0	0	  PVC FUKAI H60120-003(Walnut) 0.15mmx630mm, FOSHAN TP                       	1	1	 6PF28-2          	5	1
462	  PVC FUKAI H60120-003(Walnut) 0.15mmx550mm, FOSHAN TP                       	 6PF28-3          	200	1089882	  PVC FUKAI H60120-003(Walnut) 0.15mmx550mm, FOSHAN TP                       	1	1	 6PF28-3          	5	1
463	  PVC FUKAI H60120-003(Walnut) 0.15mmx130mm, FOSHAN TP                       	 6PF28-4          	0	0	  PVC FUKAI H60120-003(Walnut) 0.15mmx130mm, FOSHAN TP                       	1	1	 6PF28-4          	5	1
464	  PVC FUKAI H60120-003(Walnut) 0.15mmx87mm, FOSHAN TP                        	 6PF28-5          	400	346232	  PVC FUKAI H60120-003(Walnut) 0.15mmx87mm, FOSHAN TP                        	1	1	 6PF28-5          	5	1
465	  PVC FUKAI H60120-003(Walnut) 0.15mmx50mm, FOSHAN TP                        	 6PF28-6          	450	461253	  PVC FUKAI H60120-003(Walnut) 0.15mmx50mm, FOSHAN TP                        	1	1	 6PF28-6          	5	1
466	  PVC FUKAI H60120-A(Walnut HI GLoss) 0.2mmx550mm, FOSHAN TP                 	 6PF29-3          	100	1106950	  PVC FUKAI H60120-A(Walnut HI GLoss) 0.2mmx550mm, FOSHAN TP                 	1	1	 6PF29-3          	5	1
467	  PVC FUKAI H60120-A(Walnut HI GLoss) 0.2mmx415mm, FOSHAN TP                 	 6PF29-2          	25	207448	  PVC FUKAI H60120-A(Walnut HI GLoss) 0.2mmx415mm, FOSHAN TP                 	1	1	 6PF29-2          	5	1
468	  PVC FUKAI H60120-A(Walnut HI GLoss) 0.2mmx130mm, FOSHAN TP                 	 6PF29-4          	670	1845678	  PVC FUKAI H60120-A(Walnut HI GLoss) 0.2mmx130mm, FOSHAN TP                 	1	1	 6PF29-4          	5	1
469	  PVC FUKAI LF844-41 90mm, Orchard                                           	 6PF5-3           	1280	0	  PVC FUKAI LF844-41 90mm, Orchard                                           	1	1	 6PF5-3           	5	1
470	  PVC FUKAI SL-34A 1100mm, Meiwa                                             	 6PF10-2          	52	0	  PVC FUKAI SL-34A 1100mm, Meiwa                                             	1	1	 6PF10-2          	5	1
471	  PVC FUKAI SL-34A 1000mm,  Meiwa                                            	 6PF10-3          	25	0	  PVC FUKAI SL-34A 1000mm,  Meiwa                                            	1	1	 6PF10-3          	5	1
472	  PVC FUKAI SL-34A 950mm, Meiwa                                              	 6PF10-4          	148	0	  PVC FUKAI SL-34A 950mm, Meiwa                                              	1	1	 6PF10-4          	5	1
473	  PVC FUKAI SL-34A 142mm, Meiwa                                              	 6PF10-5          	145	0	  PVC FUKAI SL-34A 142mm, Meiwa                                              	1	1	 6PF10-5          	5	1
474	  PVC FUKAI SF50006-03T 0.2x340mm, Topan Cosmo                               	 6PF13-15         	0	0	  PVC FUKAI SF50006-03T 0.2x340mm, Topan Cosmo                               	1	1	 6PF13-15         	5	1
475	  PVC FUKAI SF50006-03T 0.2x240mm, Topan Cosmo                               	 6PF13-16         	55	316406	  PVC FUKAI SF50006-03T 0.2x240mm, Topan Cosmo                               	1	1	 6PF13-16         	5	1
476	  PVC FUKAI SF50006-03T 0.2x160mm, Topan Cosmo                               	 6PF13-17         	200	732806	  PVC FUKAI SF50006-03T 0.2x160mm, Topan Cosmo                               	1	1	 6PF13-17         	5	1
477	  PVC FUKAI SF50006-03T 0.2x120mm, Topan Cosmo                               	 6PF13-25         	910	2564099	  PVC FUKAI SF50006-03T 0.2x120mm, Topan Cosmo                               	1	1	 6PF13-25         	5	1
478	  PVC FUKAI SF50006-03T 0.2x55mm, Topan Cosmo                                	 6PF13-6          	150	167488	  PVC FUKAI SF50006-03T 0.2x55mm, Topan Cosmo                                	1	1	 6PF13-6          	5	1
479	  PVC FUKAI MRF3000N18 GY 42 ROSE 0.16x570, MEIWA                            	 6PF24-2          	150	1312135	  PVC FUKAI MRF3000N18 GY 42 ROSE 0.16x570, MEIWA                            	1	1	 6PF24-2          	5	1
480	  PVC FUKAI MRF3000N18 GY 42 ROSE 0.16x470, MEIWA                            	 6PF24-3          	340	2954715	  PVC FUKAI MRF3000N18 GY 42 ROSE 0.16x470, MEIWA                            	1	1	 6PF24-3          	5	1
481	  PVC FUKAI MRF3000N18 GY 42 ROSE 0.16x370, MEIWA                            	 6PF24-4          	500	3210760	  PVC FUKAI MRF3000N18 GY 42 ROSE 0.16x370, MEIWA                            	1	1	 6PF24-4          	5	1
482	  PVC FUKAI MRF3000N18 GY 42 ROSE 0.16x80, MEIWA                             	 6PF24-9          	150	189560	  PVC FUKAI MRF3000N18 GY 42 ROSE 0.16x80, MEIWA                             	1	1	 6PF24-9          	5	1
483	  PVC FUKAI MRF3000N18 GY 42 ROSE 0.16x53, MEIWA                             	 6PF24-10         	150	148358	  PVC FUKAI MRF3000N18 GY 42 ROSE 0.16x53, MEIWA                             	1	1	 6PF24-10         	5	1
484	  PVC Sheet FUKAI LF2905-90 0.17mmx460mm, TOPAN COSMO                        	 6PF22-3          	0	0	  PVC Sheet FUKAI LF2905-90 0.17mmx460mm, TOPAN COSMO                        	1	1	 6PF22-3          	5	1
485	  PVC Sheet FUKAI LF2905-90 0.17mmx320mm, TOPAN COSMO                        	 6PF22-12         	1025	3601741	  PVC Sheet FUKAI LF2905-90 0.17mmx320mm, TOPAN COSMO                        	1	1	 6PF22-12         	5	1
486	  PVC Sheet FUKAI LF2905-90 0.17mmx100mm, TOPAN COSMO                        	 6PF22-5          	320	368647	  PVC Sheet FUKAI LF2905-90 0.17mmx100mm, TOPAN COSMO                        	1	1	 6PF22-5          	5	1
487	  PVC Sheet FUKAI BK-02 0.12x750mm, Meiwa                                    	 6PF19-9          	200	1278185	  PVC Sheet FUKAI BK-02 0.12x750mm, Meiwa                                    	1	1	 6PF19-9          	5	1
488	  PVC Sheet FUKAI BK-02 0.12x475mm, Meiwa                                    	 6PF19-3          	250	1181564	  PVC Sheet FUKAI BK-02 0.12x475mm, Meiwa                                    	1	1	 6PF19-3          	5	1
489	  PVC Sheet FUKAI BK-02 0.12x370mm, Meiwa                                    	 6PF19-5          	210	783164	  PVC Sheet FUKAI BK-02 0.12x370mm, Meiwa                                    	1	1	 6PF19-5          	5	1
490	  ET FUKAI MRF3000N05 GY42ROSE 0.46x25mm, Meiwa                              	 6EF19-2          	480	556840	  ET FUKAI MRF3000N05 GY42ROSE 0.46x25mm, Meiwa                              	1	1	 6EF19-2          	5	1
491	  ET FUKAI  MRF3000N BR149TRE (Maple) 0.46 x 480mm, MEIWA                    	 6EF13-8          	100	2164330	  ET FUKAI  MRF3000N BR149TRE (Maple) 0.46 x 480mm, MEIWA                    	1	1	 6EF13-8          	5	1
492	  ET FUKAI  MRF3000N BR149TRE (Maple) 0.46 x 36mm, MEIWA                     	 6EF13-6          	720	1206601	  ET FUKAI  MRF3000N BR149TRE (Maple) 0.46 x 36mm, MEIWA                     	1	1	 6EF13-6          	5	1
493	  ET FUKAI  MRF3000N BR149TRE (Maple) 0.46 x 27mm, MEIWA                     	 6EF13-5          	1271	1584006	  ET FUKAI  MRF3000N BR149TRE (Maple) 0.46 x 27mm, MEIWA                     	1	1	 6EF13-5          	5	1
494	  ET FUKAI  MRF3000N BR149TRE (Maple) 0.46 x 24mm, MEIWA                     	 6EF13-2          	1738	1960028	  ET FUKAI  MRF3000N BR149TRE (Maple) 0.46 x 24mm, MEIWA                     	1	1	 6EF13-2          	5	1
495	  ET FUKAI  MRF3000N BR149TRE (Maple) 0.46 x 21mm, MEIWA                     	 6EF13-3          	6652	6681899	  ET FUKAI  MRF3000N BR149TRE (Maple) 0.46 x 21mm, MEIWA                     	1	1	 6EF13-3          	5	1
496	  ET FUKAI  MRF3000N BR149TRE (Maple) 0.46 x 18mm, MEIWA                     	 6EF13-4          	4035	3566310	  ET FUKAI  MRF3000N BR149TRE (Maple) 0.46 x 18mm, MEIWA                     	1	1	 6EF13-4          	5	1
497	  ET FUKAI  MRF3000N BR150TRE (Oak) 0.46 x 500mm, MEIWA                      	 6EF14-7          	70	1571757	  ET FUKAI  MRF3000N BR150TRE (Oak) 0.46 x 500mm, MEIWA                      	1	1	 6EF14-7          	5	1
498	  ET FUKAI  MRF3000N BR150TRE (Oak) 0.46 x 490mm, MEIWA                      	 6EF14-9          	70	1546305	  ET FUKAI  MRF3000N BR150TRE (Oak) 0.46 x 490mm, MEIWA                      	1	1	 6EF14-9          	5	1
499	  ET FUKAI  MRF3000N BR150TRE (Oak) 0.46 x 37mm, MEIWA                       	 6EF14-5          	99	128527	  ET FUKAI  MRF3000N BR150TRE (Oak) 0.46 x 37mm, MEIWA                       	1	1	 6EF14-5          	5	1
500	  ET FUKAI  MRF3000N BR150TRE (Oak) 0.46 x 30mm, MEIWA                       	 6EF14-4          	488	318311	  ET FUKAI  MRF3000N BR150TRE (Oak) 0.46 x 30mm, MEIWA                       	1	1	 6EF14-4          	5	1
501	  ET FUKAI  MRF3000N BR150TRE (Oak) 0.46 x 27mm, MEIWA                       	 6EF14-2          	382	240842	  ET FUKAI  MRF3000N BR150TRE (Oak) 0.46 x 27mm, MEIWA                       	1	1	 6EF14-2          	5	1
502	  ET FUKAI  MRF3000N BR150TRE (Oak) 0.46 x 21mm, MEIWA                       	 6EF14-3          	2694	2008806	  ET FUKAI  MRF3000N BR150TRE (Oak) 0.46 x 21mm, MEIWA                       	1	1	 6EF14-3          	5	1
503	  ET FUKAI  MRF3000N BR150TRE (Oak) 0.46 x 17mm, MEIWA                       	 6EF14-6          	420	335806	  ET FUKAI  MRF3000N BR150TRE (Oak) 0.46 x 17mm, MEIWA                       	1	1	 6EF14-6          	5	1
504	  ET FUKAI SF50006-03TPCTZ  0.5x670mm, Topan Cosmo                           	 6EF3-29          	100	3540553	  ET FUKAI SF50006-03TPCTZ  0.5x670mm, Topan Cosmo                           	1	1	 6EF3-29          	5	1
505	  ET FUKAI SF50006-03TPCTZ  0.5x35mm, Topan Cosmo                            	 6EF3-4           	100	186033	  ET FUKAI SF50006-03TPCTZ  0.5x35mm, Topan Cosmo                            	1	1	 6EF3-4           	5	1
506	  ET FUKAI SF50006-03TPCTZ  0.5x30mm, Topan Cosmo                            	 6EF3-19          	700	1244554	  ET FUKAI SF50006-03TPCTZ  0.5x30mm, Topan Cosmo                            	1	1	 6EF3-19          	5	1
507	  ET FUKAI SF50006-03TPCTZ  0.5x27mm, Topan Cosmo                            	 6EF3-26          	700	1123277	  ET FUKAI SF50006-03TPCTZ  0.5x27mm, Topan Cosmo                            	1	1	 6EF3-26          	5	1
508	  ET FUKAI SF50006-03TPCTZ  0.5x25mm, Topan Cosmo                            	 6EF3-5           	1200	1689619	  ET FUKAI SF50006-03TPCTZ  0.5x25mm, Topan Cosmo                            	1	1	 6EF3-5           	5	1
509	  ET FUKAI SF50006-03TPCTZ  0.5x21mm, Topan Cosmo                            	 6EF3-23          	400	477931	  ET FUKAI SF50006-03TPCTZ  0.5x21mm, Topan Cosmo                            	1	1	 6EF3-23          	5	1
510	  ET FUKAI PR04A 31mm, Meiwa                                                 	 6EF4-4           	1102	0	  ET FUKAI PR04A 31mm, Meiwa                                                 	1	1	 6EF4-4           	5	1
511	  ET FUKAI SL-34A 650mm, Meiwa                                               	 6EF5-2           	100	0	  ET FUKAI SL-34A 650mm, Meiwa                                               	1	1	 6EF5-2           	5	1
512	  ET FUKAI SL-34A 400mm, Meiwa                                               	 6EF5-3           	430	0	  ET FUKAI SL-34A 400mm, Meiwa                                               	1	1	 6EF5-3           	5	1
513	  ET FUKAI SL-34A 37mm, Meiwa                                                	 6EF5-4           	453	0	  ET FUKAI SL-34A 37mm, Meiwa                                                	1	1	 6EF5-4           	5	1
514	  ET FUKAI SL-34A 25mm, Meiwa                                                	 6EF5-5           	300	0	  ET FUKAI SL-34A 25mm, Meiwa                                                	1	1	 6EF5-5           	5	1
515	  ET FUKAI SL-34A 21mm, Meiwa                                                	 6EF5-6           	145	0	  ET FUKAI SL-34A 21mm, Meiwa                                                	1	1	 6EF5-6           	5	1
516	  ET FUKAI SL-34A 16mm, Meiwa                                                	 6EF5-7           	340	0	  ET FUKAI SL-34A 16mm, Meiwa                                                	1	1	 6EF5-7           	5	1
517	  ET White FUKAI W05S 0.52x760mm, Meiwa                                      	 6EF7-21          	88	2664572	  ET White FUKAI W05S 0.52x760mm, Meiwa                                      	1	1	 6EF7-21          	5	1
518	  ET White FUKAI W05S 0.52x350mm, Meiwa                                      	 6EF7-13          	62	780658	  ET White FUKAI W05S 0.52x350mm, Meiwa                                      	1	1	 6EF7-13          	5	1
519	  ET White FUKAI W05S 0.52x31mm, Meiwa                                       	 6EF7-5           	200	233997	  ET White FUKAI W05S 0.52x31mm, Meiwa                                       	1	1	 6EF7-5           	5	1
520	  ET White FUKAI W05S 0.52x30mm, Meiwa                                       	 6EF7-6           	240	267180	  ET White FUKAI W05S 0.52x30mm, Meiwa                                       	1	1	 6EF7-6           	5	1
521	  ET White FUKAI W05S 0.52x17mm, Meiwa                                       	 6EF7-9           	1536	1067937	  ET White FUKAI W05S 0.52x17mm, Meiwa                                       	1	1	 6EF7-9           	5	1
522	  ET FUKAI SF10014-03PCT  0.45x800mm, Topan Cosmo                            	 6EF8-33          	0	0	  ET FUKAI SF10014-03PCT  0.45x800mm, Topan Cosmo                            	1	1	 6EF8-33          	5	1
523	  ET FUKAI SF10014-03PCT  0.45x440mm, Topan Cosmo                            	 6EF8-34          	110	2622796	  ET FUKAI SF10014-03PCT  0.45x440mm, Topan Cosmo                            	1	1	 6EF8-34          	5	1
524	  ET FUKAI SF10014-03PCT  0.45x28mm, Topan Cosmo                             	 6EF8-10          	880	1417332	  ET FUKAI SF10014-03PCT  0.45x28mm, Topan Cosmo                             	1	1	 6EF8-10          	5	1
525	  ET FUKAI SF10014-03PCT  0.45x25mm, Topan Cosmo                             	 6EF8-20          	330	466477	  ET FUKAI SF10014-03PCT  0.45x25mm, Topan Cosmo                             	1	1	 6EF8-20          	5	1
526	  ET FUKAI SF10014-03PCT  0.45x17mm, Topan Cosmo                             	 6EF8-22          	1529	1364766	  ET FUKAI SF10014-03PCT  0.45x17mm, Topan Cosmo                             	1	1	 6EF8-22          	5	1
527	  ET FUKAI (0.45) WF11604-27PCL 37mm, Topan Cosmo                            	 6EF9-10          	72	0	  ET FUKAI (0.45) WF11604-27PCL 37mm, Topan Cosmo                            	1	1	 6EF9-10          	5	1
528	  ET FUKAI (0.45) WF11604-27PCL 35mm, Topan Cosmo                            	 6EF9-15          	13	0	  ET FUKAI (0.45) WF11604-27PCL 35mm, Topan Cosmo                            	1	1	 6EF9-15          	5	1
529	  ET FUKAI (0.45) WF11604-27PCL 29mm, Topan Cosmo                            	 6EF9-17          	14	0	  ET FUKAI (0.45) WF11604-27PCL 29mm, Topan Cosmo                            	1	1	 6EF9-17          	5	1
530	  ET FUKAI (0.45) WF11604-27PCL 21mm, Topan Cosmo                            	 6EF9-6           	815	752893	  ET FUKAI (0.45) WF11604-27PCL 21mm, Topan Cosmo                            	1	1	 6EF9-6           	5	1
531	  ET FUKAI (0.45) WF11604-27PCL 17mm, Topan Cosmo                            	 6EF9-8           	469	345265	  ET FUKAI (0.45) WF11604-27PCL 17mm, Topan Cosmo                            	1	1	 6EF9-8           	5	1
532	  ET FUKAI VF844-41 740mm, Orchard                                           	 6EF11-7          	100	0	  ET FUKAI VF844-41 740mm, Orchard                                           	1	1	 6EF11-7          	5	1
533	  ET FUKAI VF844-41 340mm,  Orchard                                          	 6EF11-20         	100	0	  ET FUKAI VF844-41 340mm,  Orchard                                          	1	1	 6EF11-20         	5	1
534	  ET FUKAI VF844-41 17mm, Orchard                                            	 6EF11-11         	400	0	  ET FUKAI VF844-41 17mm, Orchard                                            	1	1	 6EF11-11         	5	1
535	  ET FUKAI BK02S 0.52x760mm, Meiwa                                           	 6EF12-20         	0	0	  ET FUKAI BK02S 0.52x760mm, Meiwa                                           	1	1	 6EF12-20         	5	1
536	  ET FUKAI BK02S 0.52x740mm, Meiwa                                           	 6EF12-42         	0	0	  ET FUKAI BK02S 0.52x740mm, Meiwa                                           	1	1	 6EF12-42         	5	1
537	  ET FUKAI BK02S 0.52x250mm, Meiwa                                           	 6EF12-30         	75	776939	  ET FUKAI BK02S 0.52x250mm, Meiwa                                           	1	1	 6EF12-30         	5	1
538	  ET FUKAI BK02S 0.52x37mm, Meiwa                                            	 6EF12-3          	640	1029225	  ET FUKAI BK02S 0.52x37mm, Meiwa                                            	1	1	 6EF12-3          	5	1
539	  ET FUKAI BK02S 0.52x33mm, Meiwa                                            	 6EF12-4          	280	410073	  ET FUKAI BK02S 0.52x33mm, Meiwa                                            	1	1	 6EF12-4          	5	1
540	  ET FUKAI BK02S 0.52x30mm, Meiwa                                            	 6EF12-25         	1260	1641281	  ET FUKAI BK02S 0.52x30mm, Meiwa                                            	1	1	 6EF12-25         	5	1
541	  ET FUKAI BK02S 0.52x27mm, Meiwa                                            	 6EF12-11         	1460	1539303	  ET FUKAI BK02S 0.52x27mm, Meiwa                                            	1	1	 6EF12-11         	5	1
542	  ET FUKAI BK02S 0.52x25mm, Meiwa                                            	 6EF12-6          	1000	1647381	  ET FUKAI BK02S 0.52x25mm, Meiwa                                            	1	1	 6EF12-6          	5	1
543	  ET FUKAI BK02S 0.52x21mm, Meiwa                                            	 6EF12-7          	2205	2047981	  ET FUKAI BK02S 0.52x21mm, Meiwa                                            	1	1	 6EF12-7          	5	1
544	  ET FUKAI BK02S 0.52x19mm, Meiwa                                            	 6EF12-8          	120	100321	  ET FUKAI BK02S 0.52x19mm, Meiwa                                            	1	1	 6EF12-8          	5	1
545	  ET FUKAI BK02S 0.52x17mm,  Meiwa                                           	 6EF12-9          	2600	1930690	  ET FUKAI BK02S 0.52x17mm,  Meiwa                                           	1	1	 6EF12-9          	5	1
546	  ET FUKAI H60120-003(Walnut) 0.4mmx30mm, FOSHAN TP                          	 6EF15-4          	300	237790	  ET FUKAI H60120-003(Walnut) 0.4mmx30mm, FOSHAN TP                          	1	1	 6EF15-4          	5	1
547	  ET FUKAI H60120-003(Walnut) 0.4mmx27mm, FOSHAN TP                          	 6EF15-5          	200	143133	  ET FUKAI H60120-003(Walnut) 0.4mmx27mm, FOSHAN TP                          	1	1	 6EF15-5          	5	1
548	  ET FUKAI H60120-003(Walnut) 0.4mmx24mm, FOSHAN TP                          	 6EF15-7          	0	0	  ET FUKAI H60120-003(Walnut) 0.4mmx24mm, FOSHAN TP                          	1	1	 6EF15-7          	5	1
549	  ET FUKAI H60120-A(Walnut Hi Gloss) 0.4mmx640mm, FOSHAN TP                  	 6EF16-9          	100	2057434	  ET FUKAI H60120-A(Walnut Hi Gloss) 0.4mmx640mm, FOSHAN TP                  	1	1	 6EF16-9          	5	1
550	  ET FUKAI H60120-A(Walnut Hi Gloss) 0.4mmx46mm, FOSHAN TP                   	 6EF16-4          	0	0	  ET FUKAI H60120-A(Walnut Hi Gloss) 0.4mmx46mm, FOSHAN TP                   	1	1	 6EF16-4          	5	1
551	  ET FUKAI H60120-A(Walnut Hi Gloss) 0.4mmx30mm, FOSHAN TP                   	 6EF16-5          	100	100906	  ET FUKAI H60120-A(Walnut Hi Gloss) 0.4mmx30mm, FOSHAN TP                   	1	1	 6EF16-5          	5	1
552	  ET FUKAI H60120-A(Walnut Hi Gloss) 0.4mmx27mm, FOSHAN TP                   	 6EF16-7          	108	76913	  ET FUKAI H60120-A(Walnut Hi Gloss) 0.4mmx27mm, FOSHAN TP                   	1	1	 6EF16-7          	5	1
553	  ET FUKAI H60120-A(Walnut Hi Gloss) 0.4mmx21mm, FOSHAN TP                   	 6EF16-8          	108	97733	  ET FUKAI H60120-A(Walnut Hi Gloss) 0.4mmx21mm, FOSHAN TP                   	1	1	 6EF16-8          	5	1
554	  PVC SHEET DAIKEN TH(15505-20) 645mm, Daiken Co                             	 6PD6-16          	80	0	  PVC SHEET DAIKEN TH(15505-20) 645mm, Daiken Co                             	1	1	 6PD6-16          	5	1
555	  PVC SHEET DAIKEN TH(15505-20) 290mm, Daiken Co                             	 6PD6-19          	260	0	  PVC SHEET DAIKEN TH(15505-20) 290mm, Daiken Co                             	1	1	 6PD6-19          	5	1
556	  PVC SHEET DAIKEN TH(15505-20) 150mm, Daiken Co                             	 6PD6-21          	141	0	  PVC SHEET DAIKEN TH(15505-20) 150mm, Daiken Co                             	1	1	 6PD6-21          	5	1
557	  PVC SHEET DAIKEN TH(15505-20) 145mm, Daiken Co                             	 6PD6-20          	480	0	  PVC SHEET DAIKEN TH(15505-20) 145mm, Daiken Co                             	1	1	 6PD6-20          	5	1
558	  PVC SHEET DAIKEN TH(15505-20) 120mm, Daiken Co                             	 6PD6-7           	705	0	  PVC SHEET DAIKEN TH(15505-20) 120mm, Daiken Co                             	1	1	 6PD6-7           	5	1
559	  PVC SHEET DAIKEN TH(15505-20) 73mm, Daiken Co                              	 6PD6-15          	290	0	  PVC SHEET DAIKEN TH(15505-20) 73mm, Daiken Co                              	1	1	 6PD6-15          	5	1
560	  PVC SHEET DAIKEN TH(15505-20) 65mm, Daiken Co                              	 6PD6-11          	50	0	  PVC SHEET DAIKEN TH(15505-20) 65mm, Daiken Co                              	1	1	 6PD6-11          	5	1
561	  PVC SHEET DAIKEN TH(15505-20) 50mm, Daiken Co                              	 6PD6-3           	290	0	  PVC SHEET DAIKEN TH(15505-20) 50mm, Daiken Co                              	1	1	 6PD6-3           	5	1
562	  PVC SHEET DAIKEN TD(45017-20) 178mm, Daiken Co                             	 6PD8-46          	190	0	  PVC SHEET DAIKEN TD(45017-20) 178mm, Daiken Co                             	1	1	 6PD8-46          	5	1
563	  PVC SHEET DAIKEN TD(45017-20) 168mm, Daiken Co                             	 6PD8-48          	85	0	  PVC SHEET DAIKEN TD(45017-20) 168mm, Daiken Co                             	1	1	 6PD8-48          	5	1
564	  PVC SHEET DAIKEN TD(45017-20) 135mm, Daiken Co                             	 6PD8-36          	50	0	  PVC SHEET DAIKEN TD(45017-20) 135mm, Daiken Co                             	1	1	 6PD8-36          	5	1
565	  PVC SHEET DAIKEN TD(45017-20) 65mm, Daiken Co                              	 6PD8-19          	240	0	  PVC SHEET DAIKEN TD(45017-20) 65mm, Daiken Co                              	1	1	 6PD8-19          	5	1
566	  PVC SHEET DAIKEN TD(45017-20) 50mm, Daiken Co                              	 6PD8-9           	100	0	  PVC SHEET DAIKEN TD(45017-20) 50mm, Daiken Co                              	1	1	 6PD8-9           	5	1
567	  PVC SHEET DAIKEN TD(45017-20) 35mm, Daiken Co                              	 6PD8-33          	197	0	  PVC SHEET DAIKEN TD(45017-20) 35mm, Daiken Co                              	1	1	 6PD8-33          	5	1
568	  PVC SHEET DAIKEN WH07 820mm, Meiwa                                         	 6PD9-16          	60	0	  PVC SHEET DAIKEN WH07 820mm, Meiwa                                         	1	1	 6PD9-16          	5	1
569	  PVC SHEET DAIKEN WH07 231mm, Meiwa                                         	 6PD9-28          	250	0	  PVC SHEET DAIKEN WH07 231mm, Meiwa                                         	1	1	 6PD9-28          	5	1
570	  PVC SHEET DAIKEN WH07 230mm, Meiwa                                         	 6PD9-35          	250	0	  PVC SHEET DAIKEN WH07 230mm, Meiwa                                         	1	1	 6PD9-35          	5	1
571	  PVC SHEET DAIKEN WH07 190mm, Meiwa                                         	 6PD9-33          	150	0	  PVC SHEET DAIKEN WH07 190mm, Meiwa                                         	1	1	 6PD9-33          	5	1
572	  PVC SHEET DAIKEN WH07 160mm, Meiwa                                         	 6PD9-10          	400	0	  PVC SHEET DAIKEN WH07 160mm, Meiwa                                         	1	1	 6PD9-10          	5	1
573	  PVC SHEET DAIKEN WH07 56mm, Meiwa                                          	 6PD9-8           	65	0	  PVC SHEET DAIKEN WH07 56mm, Meiwa                                          	1	1	 6PD9-8           	5	1
574	  PVC SHEET DAIKEN WH07 50mm, Meiwa                                          	 6PD9-15          	500	0	  PVC SHEET DAIKEN WH07 50mm, Meiwa                                          	1	1	 6PD9-15          	5	1
575	  PVC SHEET DAIKEN WH09S 860mm, Meiwa                                        	 6PD11-18         	40	0	  PVC SHEET DAIKEN WH09S 860mm, Meiwa                                        	1	1	 6PD11-18         	5	1
576	  PVC SHEET DAIKEN WH09S 580mm, Meiwa                                        	 6PD11-17         	320	0	  PVC SHEET DAIKEN WH09S 580mm, Meiwa                                        	1	1	 6PD11-17         	5	1
577	  PVC SHEET DAIKEN WH09S 520mm, Meiwa                                        	 6PD11-14         	330	0	  PVC SHEET DAIKEN WH09S 520mm, Meiwa                                        	1	1	 6PD11-14         	5	1
578	  PVC SHEET DAIKEN WH09S 215mm, Meiwa                                        	 6PD11-4          	65	0	  PVC SHEET DAIKEN WH09S 215mm, Meiwa                                        	1	1	 6PD11-4          	5	1
579	  PVC SHEET DAIKEN WH09S 120mm, Meiwa                                        	 6PD11-6          	420	0	  PVC SHEET DAIKEN WH09S 120mm, Meiwa                                        	1	1	 6PD11-6          	5	1
580	  PVC SHEET DAIKEN WH09S 65mm, Meiwa                                         	 6PD11-3          	500	0	  PVC SHEET DAIKEN WH09S 65mm, Meiwa                                         	1	1	 6PD11-3          	5	1
581	  PVC SHEET DAIKEN WH09S 56mm, Meiwa                                         	 6PD11-8          	150	0	  PVC SHEET DAIKEN WH09S 56mm, Meiwa                                         	1	1	 6PD11-8          	5	1
582	  PVC SHEET DAIKEN WH09S 50mm, Meiwa                                         	 6PD11-7          	182	0	  PVC SHEET DAIKEN WH09S 50mm, Meiwa                                         	1	1	 6PD11-7          	5	1
583	  PVC DAIKEN DKC1082 N18 GR06 Green 330mm, MEIWA                             	 6PD51-2          	250	0	  PVC DAIKEN DKC1082 N18 GR06 Green 330mm, MEIWA                             	1	1	 6PD51-2          	5	1
584	  PVC DAIKEN DKC1082 N18 GR06 Green 247mm, MEIWA                             	 6PD51-3          	45	0	  PVC DAIKEN DKC1082 N18 GR06 Green 247mm, MEIWA                             	1	1	 6PD51-3          	5	1
585	  PVC DAIKEN DKC3004N 18 WW05 DK01 570mm, MEIWA                              	 6PD48-35         	60	573139	  PVC DAIKEN DKC3004N 18 WW05 DK01 570mm, MEIWA                              	1	1	 6PD48-35         	5	1
586	  PVC DAIKEN DKC3004N 18 WW05 DK01 540mm, MEIWA                              	 6PD48-27         	250	1961084	  PVC DAIKEN DKC3004N 18 WW05 DK01 540mm, MEIWA                              	1	1	 6PD48-27         	5	1
587	  PVC DAIKEN DKC3004N 18 WW05 DK01 470mm, MEIWA                              	 6PD48-34         	300	2206824	  PVC DAIKEN DKC3004N 18 WW05 DK01 470mm, MEIWA                              	1	1	 6PD48-34         	5	1
588	  PVC DAIKEN DKC3004N 18 WW05 DK01 370mm, MEIWA                              	 6PD48-33         	200	1273643	  PVC DAIKEN DKC3004N 18 WW05 DK01 370mm, MEIWA                              	1	1	 6PD48-33         	5	1
589	  PVC DAIKEN DKC3004N 18 WW05 DK01 335mm, MEIWA                              	 6PD48-21         	144	0	  PVC DAIKEN DKC3004N 18 WW05 DK01 335mm, MEIWA                              	1	1	 6PD48-21         	5	1
590	  PVC DAIKEN DKC3004N 18 WW05 DK01 200mm, MEIWA                              	 6PD48-24         	181	0	  PVC DAIKEN DKC3004N 18 WW05 DK01 200mm, MEIWA                              	1	1	 6PD48-24         	5	1
591	  PVC DAIKEN DKC3004N 18 WW05 DK01 145mm, MEIWA                              	 6PD48-19         	100	0	  PVC DAIKEN DKC3004N 18 WW05 DK01 145mm, MEIWA                              	1	1	 6PD48-19         	5	1
592	  PVC DAIKEN DKC3004N 18 WW05 DK01 140mm, MEIWA                              	 6PD48-14         	120	0	  PVC DAIKEN DKC3004N 18 WW05 DK01 140mm, MEIWA                              	1	1	 6PD48-14         	5	1
593	  PVC DAIKEN DKC3004N 18 WW05 DK01 120mm, MEIWA                              	 6PD48-3          	400	792100	  PVC DAIKEN DKC3004N 18 WW05 DK01 120mm, MEIWA                              	1	1	 6PD48-3          	5	1
594	  PVC DAIKEN DKC3004N 18 WW05 DK01 90mm, MEIWA                               	 6PD48-36         	1720	2509032	  PVC DAIKEN DKC3004N 18 WW05 DK01 90mm, MEIWA                               	1	1	 6PD48-36         	5	1
595	  PVC DAIKEN DKC3004N 18 WW05 DK01 65mm, MEIWA                               	 6PD48-10         	250	0	  PVC DAIKEN DKC3004N 18 WW05 DK01 65mm, MEIWA                               	1	1	 6PD48-10         	5	1
596	  PVC DAIKEN DKC3004N 18 WW05 DK01 56mm, MEIWA                               	 6PD48-5          	522	458411	  PVC DAIKEN DKC3004N 18 WW05 DK01 56mm, MEIWA                               	1	1	 6PD48-5          	5	1
597	  PVC DAIKEN DKC3004N 18 WW05 DK01 50mm, MEIWA                               	 6PD48-8          	288	0	  PVC DAIKEN DKC3004N 18 WW05 DK01 50mm, MEIWA                               	1	1	 6PD48-8          	5	1
598	  PVC DAIKEN DKC3004N BR123 DK01(ML PVC) 740mm, MEIWA                        	 6PD44-29         	50	0	  PVC DAIKEN DKC3004N BR123 DK01(ML PVC) 740mm, MEIWA                        	1	1	 6PD44-29         	5	1
599	  PVC DAIKEN DKC3004N BR123 DK01(ML PVC) 50mm, MEIWA                         	 6PD44-8          	144	0	  PVC DAIKEN DKC3004N BR123 DK01(ML PVC) 50mm, MEIWA                         	1	1	 6PD44-8          	5	1
600	  PVC DAIKEN DKC3004N BR127 DK01 0.16x750mm, MEIWA                           	 6PD67-7          	70	0	  PVC DAIKEN DKC3004N BR127 DK01 0.16x750mm, MEIWA                           	1	1	 6PD67-7          	5	1
601	  PVC DAIKEN DKC3004N BR127 DK01 0.16x275mm, MEIWA                           	 6PD67-12         	48	0	  PVC DAIKEN DKC3004N BR127 DK01 0.16x275mm, MEIWA                           	1	1	 6PD67-12         	5	1
602	  PVC DAIKEN DKC3004N BR127 DK01 0.16x65mm, MEIWA                            	 6PD67-14         	252	284344	  PVC DAIKEN DKC3004N BR127 DK01 0.16x65mm, MEIWA                            	1	1	 6PD67-14         	5	1
603	  PVC DAIKEN DKC3004N BR127 DK01 0.16x56mm, MEIWA                            	 6PD67-4          	392	384179	  PVC DAIKEN DKC3004N BR127 DK01 0.16x56mm, MEIWA                            	1	1	 6PD67-4          	5	1
604	  PVC DAIKEN DKC3004N BR127 DK01 0.16x50mm, MEIWA                            	 6PD67-5          	21	0	  PVC DAIKEN DKC3004N BR127 DK01 0.16x50mm, MEIWA                            	1	1	 6PD67-5          	5	1
605	  PVC SHEET DAIKEN BR 129 DK01 255mm, MEIWA                                  	 6PD68-2          	191	0	  PVC SHEET DAIKEN BR 129 DK01 255mm, MEIWA                                  	1	1	 6PD68-2          	5	1
606	  PVC SHEET DAIKEN BR 129 DK01 200mm, MEIWA                                  	 6PD68-8          	250	812553	  PVC SHEET DAIKEN BR 129 DK01 200mm, MEIWA                                  	1	1	 6PD68-8          	5	1
607	  PVC DAIKEN DKC2003N20 BLACK 600mm, MEIWA                                   	 6PD45-11         	70	651502	  PVC DAIKEN DKC2003N20 BLACK 600mm, MEIWA                                   	1	1	 6PD45-11         	5	1
608	  PVC DAIKEN DKC2003N20 BLACK 180mm, MEIWA                                   	 6PD45-10         	150	404060	  PVC DAIKEN DKC2003N20 BLACK 180mm, MEIWA                                   	1	1	 6PD45-10         	5	1
609	  PVC DAIKEN DKC2003N20 BLACK 70mm, MEIWA                                    	 6PD45-9          	300	323248	  PVC DAIKEN DKC2003N20 BLACK 70mm, MEIWA                                    	1	1	 6PD45-9          	5	1
610	  PVC DAIKEN DKC2003N20 BLACK 65mm, MEIWA                                    	 6PD45-2          	150	145748	  PVC DAIKEN DKC2003N20 BLACK 65mm, MEIWA                                    	1	1	 6PD45-2          	5	1
611	  PVC DAIKEN DKC2003N20 BLACK 50mm, MEIWA                                    	 6PD45-3          	150	112851	  PVC DAIKEN DKC2003N20 BLACK 50mm, MEIWA                                    	1	1	 6PD45-3          	5	1
612	  PVC DAIKEN DKC 3004N BR118 DK01(MT PVC) 230mm, MEIWA                       	 6PD46-6          	90	327253	  PVC DAIKEN DKC 3004N BR118 DK01(MT PVC) 230mm, MEIWA                       	1	1	 6PD46-6          	5	1
613	  PVC DAIKEN DKC 3004N BR118 DK01(MT PVC) 180mm, MEIWA                       	 6PD46-19         	128	0	  PVC DAIKEN DKC 3004N BR118 DK01(MT PVC) 180mm, MEIWA                       	1	1	 6PD46-19         	5	1
614	  PVC DAIKEN DKC 3004N BR118 DK01(MT PVC) 65mm, MEIWA                        	 6PD46-8          	74	0	  PVC DAIKEN DKC 3004N BR118 DK01(MT PVC) 65mm, MEIWA                        	1	1	 6PD46-8          	5	1
615	  PVC DAIKEN DKC 3004N BR118 DK01(MT PVC) 56mm, MEIWA                        	 6PD46-5          	275	227132	  PVC DAIKEN DKC 3004N BR118 DK01(MT PVC) 56mm, MEIWA                        	1	1	 6PD46-5          	5	1
616	  PVC DAIKEN DKC 3004N BR118 DK01(MT PVC) 50mm, MEIWA                        	 6PD46-9          	400	323105	  PVC DAIKEN DKC 3004N BR118 DK01(MT PVC) 50mm, MEIWA                        	1	1	 6PD46-9          	5	1
617	  PVC SHEET DAIKEN BR 119DK01(MF PVC) 300mm, MEIWA                           	 6PD12-31         	170	626480	  PVC SHEET DAIKEN BR 119DK01(MF PVC) 300mm, MEIWA                           	1	1	 6PD12-31         	5	1
618	  PVC SHEET DAIKEN BR 117DK01 (TG PVC) 800mm, MEIWA                          	 6PD13-18         	80	0	  PVC SHEET DAIKEN BR 117DK01 (TG PVC) 800mm, MEIWA                          	1	1	 6PD13-18         	5	1
619	  PVC SHEET DAIKEN BR 117DK01 (TG PVC) 410mm, MEIWA                          	 6PD13-30         	90	0	  PVC SHEET DAIKEN BR 117DK01 (TG PVC) 410mm, MEIWA                          	1	1	 6PD13-30         	5	1
620	  PVC SHEET DAIKEN BR 117DK01 (TG PVC) 320mm, MEIWA                          	 6PD13-57         	1040	5022785	  PVC SHEET DAIKEN BR 117DK01 (TG PVC) 320mm, MEIWA                          	1	1	 6PD13-57         	5	1
621	  PVC SHEET DAIKEN BR 117DK01 (TG PVC) 250mm, MEIWA                          	 6PD13-33         	65	0	  PVC SHEET DAIKEN BR 117DK01 (TG PVC) 250mm, MEIWA                          	1	1	 6PD13-33         	5	1
622	  PVC SHEET DAIKEN BR 117DK01 (TG PVC) 140mm, MEIWA                          	 6PD13-13         	90	0	  PVC SHEET DAIKEN BR 117DK01 (TG PVC) 140mm, MEIWA                          	1	1	 6PD13-13         	5	1
623	  PVC SHEET DAIKEN BR 117DK01 (TG PVC) 65mm, MEIWA                           	 6PD13-7          	250	232138	  PVC SHEET DAIKEN BR 117DK01 (TG PVC) 65mm, MEIWA                           	1	1	 6PD13-7          	5	1
624	  PVC SHEET DAIKEN BR 117DK01 (TG PVC) 56mm, MEIWA                           	 6PD13-4          	104	0	  PVC SHEET DAIKEN BR 117DK01 (TG PVC) 56mm, MEIWA                           	1	1	 6PD13-4          	5	1
625	  PVC SHEET DAIKEN BR 117DK01 (TG PVC) 50mm, MEIWA                           	 6PD13-6          	542	0	  PVC SHEET DAIKEN BR 117DK01 (TG PVC) 50mm, MEIWA                           	1	1	 6PD13-6          	5	1
626	  PVC Sheet DAIKEN DK010 ( WG / GY40 ) 0.16mmx150mm, MEIWA                   	 6PD14-26         	133	0	  PVC Sheet DAIKEN DK010 ( WG / GY40 ) 0.16mmx150mm, MEIWA                   	1	1	 6PD14-26         	5	1
627	  PVC Sheet DAIKEN DK010 ( WG / GY40 ) 0.16mmx120mm, MEIWA                   	 6PD14-3          	98	0	  PVC Sheet DAIKEN DK010 ( WG / GY40 ) 0.16mmx120mm, MEIWA                   	1	1	 6PD14-3          	5	1
628	  PVC Sheet DAIKEN DK010 ( WG / GY40 ) 0.16mmx70mm, MEIWA                    	 6PD14-11         	250	273902	  PVC Sheet DAIKEN DK010 ( WG / GY40 ) 0.16mmx70mm, MEIWA                    	1	1	 6PD14-11         	5	1
629	  PVC Sheet DAIKEN DK010 ( WG / GY40 ) 0.16mmx65mm, MEIWA                    	 6PD14-13         	411	415073	  PVC Sheet DAIKEN DK010 ( WG / GY40 ) 0.16mmx65mm, MEIWA                    	1	1	 6PD14-13         	5	1
630	  PVC Sheet DAIKEN DK010 ( WG / GY40 ) 0.16mmx50mm, MEIWA                    	 6PD14-7          	282	299076	  PVC Sheet DAIKEN DK010 ( WG / GY40 ) 0.16mmx50mm, MEIWA                    	1	1	 6PD14-7          	5	1
631	  PVC Sheet DAIKEN DK010 ( WG / GY40 ) 0.16mmx35mm, MEIWA                    	 6PD14-34         	873	470998	  PVC Sheet DAIKEN DK010 ( WG / GY40 ) 0.16mmx35mm, MEIWA                    	1	1	 6PD14-34         	5	1
632	  PVC Sheet DAIKEN GMA9797 440mm, Kyushu Marufuji K                          	 6PD15-9          	50	0	  PVC Sheet DAIKEN GMA9797 440mm, Kyushu Marufuji K                          	1	1	 6PD15-9          	5	1
633	  PVC Sheet DAIKEN GMA9797 190mm, Kyushu Marufuji K                          	 6PD15-11         	50	0	  PVC Sheet DAIKEN GMA9797 190mm, Kyushu Marufuji K                          	1	1	 6PD15-11         	5	1
634	  PVC Sheet DAIKEN D06-KD 120mm, Daiken                                      	 6PD16-3          	150	0	  PVC Sheet DAIKEN D06-KD 120mm, Daiken                                      	1	1	 6PD16-3          	5	1
635	  PVC DAIKEN BD-PVC(11604-26) 145mm, Daiken Co                               	 6PD69-3          	210	0	  PVC DAIKEN BD-PVC(11604-26) 145mm, Daiken Co                               	1	1	 6PD69-3          	5	1
636	  PVC DAIKEN BD-PVC(11604-26) 120mm, Daiken Co                               	 6PD69-5          	300	0	  PVC DAIKEN BD-PVC(11604-26) 120mm, Daiken Co                               	1	1	 6PD69-5          	5	1
637	  PVC DAIKEN BD-PVC(11604-26) 56mm, Daiken Co                                	 6PD69-4          	730	0	  PVC DAIKEN BD-PVC(11604-26) 56mm, Daiken Co                                	1	1	 6PD69-4          	5	1
638	  Paper Sheet DAIKEN PS 970mm, Koike Imatec                                  	 6PD19-2          	500	3621806	  Paper Sheet DAIKEN PS 970mm, Koike Imatec                                  	1	1	 6PD19-2          	5	1
639	  Paper Sheet DAIKEN PS 625mm, Koike Imatec                                  	 6PD19-3          	288	863472	  Paper Sheet DAIKEN PS 625mm, Koike Imatec                                  	1	1	 6PD19-3          	5	1
640	  Paper Sheet DAIKEN VS-25 450mm, MARUFIJI KK                                	 6PD42-3          	44	0	  Paper Sheet DAIKEN VS-25 450mm, MARUFIJI KK                                	1	1	 6PD42-3          	5	1
641	  Paper Sheet DAIKEN VS-25 240mm, Koike Imatec                               	 6PD42-5          	0	0	  Paper Sheet DAIKEN VS-25 240mm, Koike Imatec                               	1	1	 6PD42-5          	5	1
642	  ET DAIKEN WH07 0.52x680mm, Meiwa                                           	 6ED35-48         	100	3103834	  ET DAIKEN WH07 0.52x680mm, Meiwa                                           	1	1	 6ED35-48         	5	1
643	  ET DAIKEN WH07 0.52x40mm, Meiwa                                            	 6ED35-3          	190	297073	  ET DAIKEN WH07 0.52x40mm, Meiwa                                            	1	1	 6ED35-3          	5	1
644	  ET DAIKEN WH07 0.52x36mm, Meiwa                                            	 6ED35-5          	210	354571	  ET DAIKEN WH07 0.52x36mm, Meiwa                                            	1	1	 6ED35-5          	5	1
645	  ET DAIKEN WH07 0.52x25mm, Meiwa                                            	 6ED35-12         	0	0	  ET DAIKEN WH07 0.52x25mm, Meiwa                                            	1	1	 6ED35-12         	5	1
646	  ET DAIKEN WH07 0.52x21mm, Meiwa                                            	 6ED35-8          	2000	2069806	  ET DAIKEN WH07 0.52x21mm, Meiwa                                            	1	1	 6ED35-8          	5	1
647	  ET DAIKEN WH07 0.52x18mm, Meiwa                                            	 6ED35-40         	46	0	  ET DAIKEN WH07 0.52x18mm, Meiwa                                            	1	1	 6ED35-40         	5	1
648	  ET DAIKEN WH07 0.52x17mm, Meiwa                                            	 6ED35-39         	800	663076	  ET DAIKEN WH07 0.52x17mm, Meiwa                                            	1	1	 6ED35-39         	5	1
649	  ET DAIKEN DK010 ( WG/grey) 0.46mmx670mm, Meiwa                             	 6ED36-25         	50	0	  ET DAIKEN DK010 ( WG/grey) 0.46mmx670mm, Meiwa                             	1	1	 6ED36-25         	5	1
650	  ET DAIKEN DK010 ( WG/grey) 0.46mmx40mm, Meiwa                              	 6ED36-8          	300	136495	  ET DAIKEN DK010 ( WG/grey) 0.46mmx40mm, Meiwa                              	1	1	 6ED36-8          	5	1
651	  ET DAIKEN DK010 ( WG/grey) 0.46mmx36mm, Meiwa                              	 6ED36-24         	700	1265253	  ET DAIKEN DK010 ( WG/grey) 0.46mmx36mm, Meiwa                              	1	1	 6ED36-24         	5	1
652	  ET DAIKEN BR 117DK01 (TG PVC) 0.46mmx40mm, Meiwa                           	 6ED37-8          	770	0	  ET DAIKEN BR 117DK01 (TG PVC) 0.46mmx40mm, Meiwa                           	1	1	 6ED37-8          	5	1
653	  ET DAIKEN BR 117DK01 (TG PVC) 0.46mmx25mm, Meiwa                           	 6ED37-41         	71	0	  ET DAIKEN BR 117DK01 (TG PVC) 0.46mmx25mm, Meiwa                           	1	1	 6ED37-41         	5	1
654	  ET DAIKEN BR 117DK01 (TG PVC) 0.46mmx21mm, Meiwa                           	 6ED37-12         	218	0	  ET DAIKEN BR 117DK01 (TG PVC) 0.46mmx21mm, Meiwa                           	1	1	 6ED37-12         	5	1
655	  ET DAIKEN BR 117DK01 (TG PVC) 0.46mmx17mm, Meiwa                           	 6ED37-25         	1050	700132	  ET DAIKEN BR 117DK01 (TG PVC) 0.46mmx17mm, Meiwa                           	1	1	 6ED37-25         	5	1
656	  ET DAIKEN BR 117DK01 (TG PVC) 0.46mmx14mm, Meiwa                           	 6ED37-38         	37	0	  ET DAIKEN BR 117DK01 (TG PVC) 0.46mmx14mm, Meiwa                           	1	1	 6ED37-38         	5	1
657	  ET DAIKEN BR 119DK01 (MF PVC) 0.46mmx810mm, Meiwa                          	 6ED38-19         	40	1452437	  ET DAIKEN BR 119DK01 (MF PVC) 0.46mmx810mm, Meiwa                          	1	1	 6ED38-19         	5	1
658	  ET DAIKEN BR 119DK01 (MF PVC) 0.46mmx52mm, Meiwa                           	 6ED38-13         	80	193181	  ET DAIKEN BR 119DK01 (MF PVC) 0.46mmx52mm, Meiwa                           	1	1	 6ED38-13         	5	1
659	  ET DAIKEN BR 129 DK01 320mm, MEIWA                                         	 6ED66-9          	90	1313444	  ET DAIKEN BR 129 DK01 320mm, MEIWA                                         	1	1	 6ED66-9          	5	1
660	  ET DAIKEN BR 129 DK01 35mm, MEIWA                                          	 6ED66-8          	90	150897	  ET DAIKEN BR 129 DK01 35mm, MEIWA                                          	1	1	 6ED66-8          	5	1
661	  ET DAIKEN BR 129 DK01 25mm, MEIWA                                          	 6ED66-7          	90	110133	  ET DAIKEN BR 129 DK01 25mm, MEIWA                                          	1	1	 6ED66-7          	5	1
662	  ET DAIKEN DKC1082N GR06  0.42mmx530mm, MEIWA                               	 6ED57-2          	100	0	  ET DAIKEN DKC1082N GR06  0.42mmx530mm, MEIWA                               	1	1	 6ED57-2          	5	1
663	  ET DAIKEN DKC1082N GR06  0.42mmx300mm, MEIWA                               	 6ED57-3          	100	0	  ET DAIKEN DKC1082N GR06  0.42mmx300mm, MEIWA                               	1	1	 6ED57-3          	5	1
664	  ET DAIKEN DKC1082N GR06  0.42mmx45mm, MEIWA                                	 6ED57-4          	100	0	  ET DAIKEN DKC1082N GR06  0.42mmx45mm, MEIWA                                	1	1	 6ED57-4          	5	1
665	  ET DAIKEN DKC2003N BLACK  0.46mmx600mm, MEIWA                              	 6ED42-13         	100	2703982	  ET DAIKEN DKC2003N BLACK  0.46mmx600mm, MEIWA                              	1	1	 6ED42-13         	5	1
666	  ET DAIKEN DKC2003N BLACK 0.46mmx43mm, MEIWA                                	 6ED42-10         	410	803113	  ET DAIKEN DKC2003N BLACK 0.46mmx43mm, MEIWA                                	1	1	 6ED42-10         	5	1
667	  ET DAIKEN DKC2003N BLACK  0.46mmx40mm, MEIWA                               	 6ED42-2          	70	0	  ET DAIKEN DKC2003N BLACK  0.46mmx40mm, MEIWA                               	1	1	 6ED42-2          	5	1
668	  ET DAIKEN DKC2003N BLACK  0.46mmx37mm, MEIWA                               	 6ED42-9          	574	970888	  ET DAIKEN DKC2003N BLACK  0.46mmx37mm, MEIWA                               	1	1	 6ED42-9          	5	1
669	  ET DAIKEN DKC2003N BLACK  0.46mmx25mm, MEIWA                               	 6ED42-5          	1116	1296996	  ET DAIKEN DKC2003N BLACK  0.46mmx25mm, MEIWA                               	1	1	 6ED42-5          	5	1
670	  ET DAIKEN DKC2003N BLACK  0.46mmx21mm, MEIWA                               	 6ED42-3          	844	830146	  ET DAIKEN DKC2003N BLACK  0.46mmx21mm, MEIWA                               	1	1	 6ED42-3          	5	1
671	  ET DAIKEN DKC3004N  BR118 DK01  400mm, MEIWA                               	 6ED45-10         	100	1764990	  ET DAIKEN DKC3004N  BR118 DK01  400mm, MEIWA                               	1	1	 6ED45-10         	5	1
672	  ET DAIKEN DKC3004N  BR118 DK01  45mm, MEIWA                                	 6ED45-5          	100	213973	  ET DAIKEN DKC3004N  BR118 DK01  45mm, MEIWA                                	1	1	 6ED45-5          	5	1
673	  ET DAIKEN DKC3004N  BR118 DK01  17mm, MEIWA                                	 6ED45-8          	100	30179	  ET DAIKEN DKC3004N  BR118 DK01  17mm, MEIWA                                	1	1	 6ED45-8          	5	1
674	  ET DAIKEN DKC3004 N18 BR123  0.46mmx270mm, MEIWA                           	 6ED43-20         	105	552096	  ET DAIKEN DKC3004 N18 BR123  0.46mmx270mm, MEIWA                           	1	1	 6ED43-20         	5	1
675	  ET DAIKEN DKC3004 N18 BR123  0.46mmx55mm, MEIWA                            	 6ED43-11         	105	471284	  ET DAIKEN DKC3004 N18 BR123  0.46mmx55mm, MEIWA                            	1	1	 6ED43-11         	5	1
676	  ET DAIKEN DKC3004 N18 BR123  0.46mmx40mm, MEIWA                            	 6ED43-3          	435	687545	  ET DAIKEN DKC3004 N18 BR123  0.46mmx40mm, MEIWA                            	1	1	 6ED43-3          	5	1
677	  ET DAIKEN DKC3004 N18 BR123  0.46mmx21mm, MEIWA                            	 6ED43-6          	390	386467	  ET DAIKEN DKC3004 N18 BR123  0.46mmx21mm, MEIWA                            	1	1	 6ED43-6          	5	1
678	  ET DAIKEN DKC3004N BR127 DK01 0.46mmx40mm, Meiwa                           	 6ED65-4          	250	513764	  ET DAIKEN DKC3004N BR127 DK01 0.46mmx40mm, Meiwa                           	1	1	 6ED65-4          	5	1
679	  ET DAIKEN DKC3004N  WW05 DK01  330mm, MEIWA                                	 6ED48-23         	100	1521410	  ET DAIKEN DKC3004N  WW05 DK01  330mm, MEIWA                                	1	1	 6ED48-23         	5	1
680	  ET DAIKEN DKC3004N  WW05 DK01  40mm, MEIWA                                 	 6ED48-11         	300	515337	  ET DAIKEN DKC3004N  WW05 DK01  40mm, MEIWA                                 	1	1	 6ED48-11         	5	1
681	  ET DAIKEN DKC3004N  WW05 DK01  25mm, MEIWA                                 	 6ED48-5          	81	0	  ET DAIKEN DKC3004N  WW05 DK01  25mm, MEIWA                                 	1	1	 6ED48-5          	5	1
682	  ORIFINE SHEET ALKORCELL 0.3x350 MG BLACK, TOPAN COSMO                      	 6O2-8            	100	0	  ORIFINE SHEET ALKORCELL 0.3x350 MG BLACK, TOPAN COSMO                      	1	1	 6O2-8            	5	1
683	  ORIFINE SHEET ALKORCELL 0.3x330 MG BLACK, TOPAN COSMO                      	 6O2-5            	183	0	  ORIFINE SHEET ALKORCELL 0.3x330 MG BLACK, TOPAN COSMO                      	1	1	 6O2-5            	5	1
684	  ORIFINE SHEET ALKORCELL 0.3x120 MG BLACK, TOPAN COSMO                      	 6O2-7            	600	0	  ORIFINE SHEET ALKORCELL 0.3x120 MG BLACK, TOPAN COSMO                      	1	1	 6O2-7            	5	1
685	  Paper NAS P5812TSU Brown 810mm, Topan Cosmo                                	 6PN8-9           	504	0	  Paper NAS P5812TSU Brown 810mm, Topan Cosmo                                	1	1	 6PN8-9           	5	1
686	  Paper NAS P5812TSU Brown 450mm, Topan Cosmo                                	 6PN8-8           	719	0	  Paper NAS P5812TSU Brown 450mm, Topan Cosmo                                	1	1	 6PN8-8           	5	1
687	  Paper NAS GM 30G PM 390mm, Topan Cosmo                                     	 6PN17-17         	400	1454852	  Paper NAS GM 30G PM 390mm, Topan Cosmo                                     	1	1	 6PN17-17         	5	1
688	  Paper NAS GM 30G PM 150mm, Topan Cosmo                                     	 6PN17-9          	400	458500	  Paper NAS GM 30G PM 150mm, Topan Cosmo                                     	1	1	 6PN17-9          	5	1
689	  Paper NAS GM 30G PM 130mm, Topan Cosmo                                     	 6PN17-6          	1000	1173321	  Paper NAS GM 30G PM 130mm, Topan Cosmo                                     	1	1	 6PN17-6          	5	1
690	  Paper NAS GM 30G PM 70mm, Topan Cosmo                                      	 6PN17-11         	0	0	  Paper NAS GM 30G PM 70mm, Topan Cosmo                                      	1	1	 6PN17-11         	5	1
691	  Paper NAS GM 30G SM 470mm, Topan Cosmo                                     	 6PN19-13         	200	861413	  Paper NAS GM 30G SM 470mm, Topan Cosmo                                     	1	1	 6PN19-13         	5	1
692	  Paper NAS GM 30G SM 150mm, Topan Cosmo                                     	 6PN19-7          	200	276474	  Paper NAS GM 30G SM 150mm, Topan Cosmo                                     	1	1	 6PN19-7          	5	1
693	  Paper NAS GM 30G SM 130mm, Topan Cosmo                                     	 6PN19-5          	500	610435	  Paper NAS GM 30G SM 130mm, Topan Cosmo                                     	1	1	 6PN19-5          	5	1
694	  Paper NAS GM 30G SM 70mm, Topan Cosmo                                      	 6PN19-8          	400	270877	  Paper NAS GM 30G SM 70mm, Topan Cosmo                                      	1	1	 6PN19-8          	5	1
695	  Paper NAS GM 30G MB 670mm, Topan Cosmo                                     	 6PN36-14         	0	0	  Paper NAS GM 30G MB 670mm, Topan Cosmo                                     	1	1	 6PN36-14         	5	1
696	  Paper NAS GM 30G MB 150mm, Topan Cosmo                                     	 6PN36-6          	250	354795	  Paper NAS GM 30G MB 150mm, Topan Cosmo                                     	1	1	 6PN36-6          	5	1
697	  Paper NAS GM 30G MB 130mm, Topan Cosmo                                     	 6PN36-4          	360	444735	  Paper NAS GM 30G MB 130mm, Topan Cosmo                                     	1	1	 6PN36-4          	5	1
698	  Paper NAS GM 30G MB 70mm, Topan Cosmo                                      	 6PN36-7          	500	346084	  Paper NAS GM 30G MB 70mm, Topan Cosmo                                      	1	1	 6PN36-7          	5	1
699	  Paper NAS GM 30G MB 50mm, Topan Cosmo                                      	 6PN36-5          	360	0	  Paper NAS GM 30G MB 50mm, Topan Cosmo                                      	1	1	 6PN36-5          	5	1
700	  Paper Sheet NAS DB 30gr 1120mm, Toppan C                                   	 6PN22-9          	0	0	  Paper Sheet NAS DB 30gr 1120mm, Toppan C                                   	1	1	 6PN22-9          	5	1
701	  Paper Sheet NAS DB 30gr 480mm, Toppan C                                    	 6PN22-2          	267	0	  Paper Sheet NAS DB 30gr 480mm, Toppan C                                    	1	1	 6PN22-2          	5	1
702	  Paper Sheet NAS DB 30gr 470mm, Toppan C                                    	 6PN22-12         	300	1332803	  Paper Sheet NAS DB 30gr 470mm, Toppan C                                    	1	1	 6PN22-12         	5	1
703	  Paper Sheet NAS DB 30gr 150mm, Toppan C                                    	 6PN22-3          	500	1158395	  Paper Sheet NAS DB 30gr 150mm, Toppan C                                    	1	1	 6PN22-3          	5	1
704	  Paper Sheet NAS DB 30gr 130mm, Toppan C                                    	 6PN22-5          	950	1199640	  Paper Sheet NAS DB 30gr 130mm, Toppan C                                    	1	1	 6PN22-5          	5	1
705	  Paper Sheet NAS DB 30gr 70mm, Toppan C                                     	 6PN22-6          	500	622211	  Paper Sheet NAS DB 30gr 70mm, Toppan C                                     	1	1	 6PN22-6          	5	1
706	  Paper Sheet NAS DB 30gr 50mm, Toppan C                                     	 6PN22-8          	0	0	  Paper Sheet NAS DB 30gr 50mm, Toppan C                                     	1	1	 6PN22-8          	5	1
707	  Paper Sheet NAS 65gr PW 520mm, Topan Cosmo                                 	 6PN24-6          	0	0	  Paper Sheet NAS 65gr PW 520mm, Topan Cosmo                                 	1	1	 6PN24-6          	5	1
708	  Paper Sheet NAS 65gr PW 105mm, Topan Cosmo                                 	 6PN24-5          	800	1061475	  Paper Sheet NAS 65gr PW 105mm, Topan Cosmo                                 	1	1	 6PN24-5          	5	1
709	  Paper Sheet NAS 65gr PW 65mm, Topan Cosmo                                  	 6PN24-4          	600	538045	  Paper Sheet NAS 65gr PW 65mm, Topan Cosmo                                  	1	1	 6PN24-4          	5	1
710	  Paper Sheet NAS DB 65gr 410mm, TOPPAN Printing                             	 6PN35-4          	1100	6402499	  Paper Sheet NAS DB 65gr 410mm, TOPPAN Printing                             	1	1	 6PN35-4          	5	1
711	  Paper Sheet NAS DB 65gr 350mm, TOPPAN Printing                             	 6PN35-3          	1057	6223430	  Paper Sheet NAS DB 65gr 350mm, TOPPAN Printing                             	1	1	 6PN35-3          	5	1
712	  Paper NAS GM 65G PM 890mm, A&S TRADE                                       	 6PN27-11         	250	3248354	  Paper NAS GM 65G PM 890mm, A&S TRADE                                       	1	1	 6PN27-11         	5	1
713	  Paper NAS GM 65G PM 410mm, A&S TRADE                                       	 6PN27-9          	550	3209354	  Paper NAS GM 65G PM 410mm, A&S TRADE                                       	1	1	 6PN27-9          	5	1
714	  Paper NAS GM 65G SM 410mm, A&S TRADE                                       	 6PN29-8          	550	3159580	  Paper NAS GM 65G SM 410mm, A&S TRADE                                       	1	1	 6PN29-8          	5	1
715	  Paper NAS GM 65G SM 350mm, A&S TRADE                                       	 6PN29-12         	250	1437788	  Paper NAS GM 65G SM 350mm, A&S TRADE                                       	1	1	 6PN29-12         	5	1
716	  Pvc TENMA SF10014-03T White ( 0.2 )110mm, Topan Cosmo                      	 6PT7-8           	140	404060	  Pvc TENMA SF10014-03T White ( 0.2 )110mm, Topan Cosmo                      	1	1	 6PT7-8           	5	1
717	  Pvc TENMA WF11604-27T PCL(0.45mm) Linear Walnut (OAK) 475mm, Topan Cosmo   	 6PT9-4           	190	444096	  Pvc TENMA WF11604-27T PCL(0.45mm) Linear Walnut (OAK) 475mm, Topan Cosmo   	1	1	 6PT9-4           	5	1
718	  ET TENMA WF25048-07 PCL 520mm, Topan Cosmo                                 	 6ET1-14          	130	2798954	  ET TENMA WF25048-07 PCL 520mm, Topan Cosmo                                 	1	1	 6ET1-14          	5	1
719	  ET TENMA WF25048-07 PCL 27mm, Topan Cosmo                                  	 6ET1-12          	260	303224	  ET TENMA WF25048-07 PCL 27mm, Topan Cosmo                                  	1	1	 6ET1-12          	5	1
720	  ET TENMA WF25048-07 PCL 30mm, Topan Cosmo                                  	 6ET1-4           	82	0	  ET TENMA WF25048-07 PCL 30mm, Topan Cosmo                                  	1	1	 6ET1-4           	5	1
721	  SOLAR INDUSTRI, SINAR BACAN KHATULISTIWA                                   	 5S8-1            	3360	27600006	  SOLAR INDUSTRI, SINAR BACAN KHATULISTIWA                                   	1	11	 5S8-1            	33	1
722	  CeLLuLae Sanding Sealer Clear CLS-610-JYN Pail20, Gyungdo Ind              	 5C9-1            	1	715150	  CeLLuLae Sanding Sealer Clear CLS-610-JYN Pail20, Gyungdo Ind              	1	11	 5C9-1            	33	1
723	  CeLLuLae Solid Black Side Pail20, Gyungdo Ind                              	 5C3-1            	4	3241790	  CeLLuLae Solid Black Side Pail20, Gyungdo Ind                              	1	11	 5C3-1            	33	1
724	  CeLLuLae Solid Black METALIC CLC-610-01-BM-MFJ (5LT)GALON, Gyungdo Ind     	 5C7-1            	1	249823	  CeLLuLae Solid Black METALIC CLC-610-01-BM-MFJ (5LT)GALON, Gyungdo Ind     	1	11	 5C7-1            	33	1
725	  CeLLuLae Solid Black CLC-610-01MFJ 5L, Gyungdo Ind                         	 5C14-1           	1	249823	  CeLLuLae Solid Black CLC-610-01MFJ 5L, Gyungdo Ind                         	1	11	 5C14-1           	33	1
726	  CeLLuLac  SOLID Brown CLC-610-BROWN-MFJ (5LTR), Gyungdo Ind                	 5C15-3           	1	448465	  CeLLuLac  SOLID Brown CLC-610-BROWN-MFJ (5LTR), Gyungdo Ind                	1	11	 5C15-3           	33	1
727	  Lacquer Thiner Pail20, Gyungdo Ind                                         	 5L1-1            	5	2542091	  Lacquer Thiner Pail20, Gyungdo Ind                                         	1	11	 5L1-1            	1	1
728	  Lacquer Thiner CLT 031P2 Pail20, Gyungdo Ind                               	 5L2-1            	0	0	  Lacquer Thiner CLT 031P2 Pail20, Gyungdo Ind                               	1	11	 5L2-1            	1	1
729	  Platop 500N Clear Can5, Gajah tunggal                                      	 5P1-1            	2	0	  Platop 500N Clear Can5, Gajah tunggal                                      	1	11	 5P1-1            	33	1
730	  Splesh Coklat Can2.5, KansaiPaint                                          	 5S1-1            	2	432380	  Splesh Coklat Can2.5, KansaiPaint                                          	1	11	 5S1-1            	33	1
731	  Splesh Cream Yellow Can2.5, Gajah Tunggal                                  	 5S2-1            	1	370734	  Splesh Cream Yellow Can2.5, Gajah Tunggal                                  	1	11	 5S2-1            	33	1
732	  Splesh Cream Red Can2.5, Gajah Tunggal                                     	 5S3-1            	4	1101045	  Splesh Cream Red Can2.5, Gajah Tunggal                                     	1	11	 5S3-1            	33	1
733	  GAKUBUCHI L=2400MM, PT. DDI                                                	 7G140-1          	15	600000	  GAKUBUCHI L=2400MM, PT. DDI                                                	1	11	 7G140-1          	27	1
734	  MESIAWASE L=2400MM, PT. DDI                                                	 7M11-1           	3	120000	  MESIAWASE L=2400MM, PT. DDI                                                	1	11	 7M11-1           	27	1
735	  LOUVER L=2400MM, PT. DDI                                                   	 7L10-1           	14	896000	  LOUVER L=2400MM, PT. DDI                                                   	1	11	 7L10-1           	27	1
736	  LOUVER W/ CAP, PT. DDI                                                     	 7L10-2           	24	11760	  LOUVER W/ CAP, PT. DDI                                                     	1	11	 7L10-2           	27	1
737	  Hinge Miror DAITO L=266.5, YAN JIN INDONESIA                               	 7H154-1          	7492	19926510	  Hinge Miror DAITO L=266.5, YAN JIN INDONESIA                               	1	11	 7H154-1          	27	1
738	  Hinge (71M258E)  MAGUCHI SYUNO, Marufuji KK                                	 7H165-1          	70	551259	  Hinge (71M258E)  MAGUCHI SYUNO, Marufuji KK                                	1	11	 7H165-1          	27	1
739	  WASHER (174E613) MAGUCHI SYUNO, Marufuji KK                                	 7W4-1            	70	180727	  WASHER (174E613) MAGUCHI SYUNO, Marufuji KK                                	1	11	 7W4-1            	27	1
740	  JYUSI ANNGLE  MAGUCHI SYUNO, KYUSHU MARUFUJI                               	 7J3-1            	1268	65889201	  JYUSI ANNGLE  MAGUCHI SYUNO, KYUSHU MARUFUJI                               	1	11	 7J3-1            	27	1
741	  SHELF RECEIVING MEMBER  MAGUCHI SYUNO, KYUSHU MARUFUJI                     	 7S74-1           	1678	42639961	  SHELF RECEIVING MEMBER  MAGUCHI SYUNO, KYUSHU MARUFUJI                     	1	11	 7S74-1           	27	1
742	  LABEL MAGUCHI SYUNO, Marufuji KK                                           	 7L9-1            	632	0	  LABEL MAGUCHI SYUNO, Marufuji KK                                           	1	11	 7L9-1            	27	1
743	  Dowel 8,3x31 DAIKEN SHOES, Sinar Abadi                                     	 7D3-1            	98998	2563975	  Dowel 8,3x31 DAIKEN SHOES, Sinar Abadi                                     	1	11	 7D3-1            	27	1
744	  Screw DAIKEN SHOES 2.6x15, PT SAGATEKNINDO                                 	 7S3-1            	11700	617576	  Screw DAIKEN SHOES 2.6x15, PT SAGATEKNINDO                                 	1	11	 7S3-1            	27	1
745	  Screw VIS WH DAIKEN SHOES 4x50, Marufuji KK                                	 7S4-1            	15642	12029041	  Screw VIS WH DAIKEN SHOES 4x50, Marufuji KK                                	1	11	 7S4-1            	27	1
746	  Screw VIS DAIKEN SHOES 3.3x25, Marufuji KK                                 	 7S6-1            	0	0	  Screw VIS DAIKEN SHOES 3.3x25, Marufuji KK                                 	1	11	 7S6-1            	27	1
747	  Screw DAIKEN AFTP 3.5x12, Marufuji KK                                      	 7S73-1           	6148	779370	  Screw DAIKEN AFTP 3.5x12, Marufuji KK                                      	1	11	 7S73-1           	27	1
748	  BASE CUP DAIKEN CLOUK,  Marufuji KK                                        	 7B7-1            	140	0	  BASE CUP DAIKEN CLOUK,  Marufuji KK                                        	1	11	 7B7-1            	27	1
749	  DLR2  DAIKEN CLOUK  RUNNER ASSY, Marufuji KK                               	 7D24-1           	24947	100798656	  DLR2  DAIKEN CLOUK  RUNNER ASSY, Marufuji KK                               	1	11	 7D24-1           	27	1
750	  DLR2  DAIKEN CLOUK  BASE CUP ASSY, Marufuji KK                             	 7D25-1           	25056	126691664	  DLR2  DAIKEN CLOUK  BASE CUP ASSY, Marufuji KK                             	1	11	 7D25-1           	27	1
751	  Screw Hinge DAIKEN CLOUK VIS 3.1x22,  Marufuji KK                          	 7S1-1            	151904	18406811	  Screw Hinge DAIKEN CLOUK VIS 3.1x22,  Marufuji KK                          	1	11	 7S1-1            	27	1
752	  Screw  DAIKEN CLOUK  8x21, Marufuji KK                                     	 7S60-1           	275	4005	  Screw  DAIKEN CLOUK  8x21, Marufuji KK                                     	1	11	 7S60-1           	27	1
753	  Screw  DAIKEN CLOUK  vis3.1x10,  Marufuji KK                               	 7S61-2           	131852	34422626	  Screw  DAIKEN CLOUK  vis3.1x10,  Marufuji KK                               	1	11	 7S61-2           	27	1
754	  Screw  DAIKEN CLOUK  3.5x20Ni DLR1,  Marufuji KK                           	 7S65-1           	66852	11199549	  Screw  DAIKEN CLOUK  3.5x20Ni DLR1,  Marufuji KK                           	1	11	 7S65-1           	27	1
755	  PIPOTTO DAIKEN CLOUK,  Marufuji KK                                         	 7P34-1           	50	0	  PIPOTTO DAIKEN CLOUK,  Marufuji KK                                         	1	11	 7P34-1           	27	1
756	  Chouban Screw STORAGE BOX 3.5x12, Marufuji KK                              	 7C6-1            	66986	16832020	  Chouban Screw STORAGE BOX 3.5x12, Marufuji KK                              	1	11	 7C6-1            	27	1
757	  Cap STORAGE BOX Ivory PM,  Marufuji KK                                     	 7C7-1            	3516	0	  Cap STORAGE BOX Ivory PM,  Marufuji KK                                     	1	11	 7C7-1            	27	1
758	  Cap STORAGE BOX BBJYU SM/MC, Marufuji KK                                   	 7C8-1            	2000	0	  Cap STORAGE BOX BBJYU SM/MC, Marufuji KK                                   	1	11	 7C8-1            	27	1
759	  Cap STORAGE BOX Black BO, Marufuji KK                                      	 7C10-1           	6240	0	  Cap STORAGE BOX Black BO, Marufuji KK                                      	1	11	 7C10-1           	27	1
760	  Cap STORAGE BOX White PW, Marufuji KK                                      	 7C11-1           	65710	16953290	  Cap STORAGE BOX White PW, Marufuji KK                                      	1	11	 7C11-1           	27	1
761	  C.PL Metalla Mini STORAGE BOX A.S 37/2mm W 600, HAFELE                     	 7C12-1           	5063	3131590	  C.PL Metalla Mini STORAGE BOX A.S 37/2mm W 600, HAFELE                     	1	11	 7C12-1           	27	1
762	  Drywall STORAGE BOX M6x1 3/8" White, Harapan Jaya                          	 7D9-1            	4800	586709	  Drywall STORAGE BOX M6x1 3/8" White, Harapan Jaya                          	1	11	 7D9-1            	27	1
763	  Hinge Mirror STORAGE BOX NEW L=387mm W625, Yan Jin Indonesia               	 7H157-1          	8113	26696798	  Hinge Mirror STORAGE BOX NEW L=387mm W625, Yan Jin Indonesia               	1	11	 7H157-1          	27	1
764	  Metalla Mini AFull STORAGE BOX OVER L.95 W600, HAFELE                      	 7M4-1            	4821	11962609	  Metalla Mini AFull STORAGE BOX OVER L.95 W600, HAFELE                      	1	11	 7M4-1            	27	1
765	  Screw (Hinge) STORAGE BOX VIS3.5x13, Atom                                  	 7S19-1           	2900	450913	  Screw (Hinge) STORAGE BOX VIS3.5x13, Atom                                  	1	11	 7S19-1           	27	1
766	  Screw  STORAGE BOX  4x10 Kuromate, Marufuji KK                             	 7S68-1           	15460	3874714	  Screw  STORAGE BOX  4x10 Kuromate, Marufuji KK                             	1	11	 7S68-1           	27	1
767	  Screw Vis STORAGE BOX 2.6x12, Marufuji KK                                  	 7S41-1           	22818	3817271	  Screw Vis STORAGE BOX 2.6x12, Marufuji KK                                  	1	11	 7S41-1           	27	1
768	  Taping Screw STORAGE BOX 3.5x16, Marufuji KK                               	 7T2-1            	30800	13897463	  Taping Screw STORAGE BOX 3.5x16, Marufuji KK                               	1	11	 7T2-1            	27	1
769	  Washer Set Jyurakon STORAGE BOX, Marufuji KK                               	 7W2-1            	55936	18447784	  Washer Set Jyurakon STORAGE BOX, Marufuji KK                               	1	11	 7W2-1            	27	1
770	  Hinge WOOD ONE (311.05.110), Marufuji KK                                   	 7H119-1          	6908	45904802	  Hinge WOOD ONE (311.05.110), Marufuji KK                                   	1	11	 7H119-1          	27	1
771	  Hinge Plate WOOD ONE (311.10.546), Marufuji KK                             	 7H120-1          	6898	35524475	  Hinge Plate WOOD ONE (311.10.546), Marufuji KK                             	1	11	 7H120-1          	27	1
772	  MAGNET Latch NF WOOD ONE, Marufuji KK                                      	 7M9-1            	5715	255171273	  MAGNET Latch NF WOOD ONE, Marufuji KK                                      	1	11	 7M9-1            	27	1
773	  Magnet NF PLATE WOOD ONE Type 245.11.631, Marufuji KK                      	 7M10-2           	5667	17847922	  Magnet NF PLATE WOOD ONE Type 245.11.631, Marufuji KK                      	1	11	 7M10-2           	27	1
774	  Screw  WOOD ONE  4.1x28 , Marufuji KK                                      	 7S53-1           	1987	973151	  Screw  WOOD ONE  4.1x28 , Marufuji KK                                      	1	11	 7S53-1           	27	1
775	  Screw  WOOD ONE  3.5x13 NI, Marufuji KK                                    	 7S55-1           	14252	6651909	  Screw  WOOD ONE  3.5x13 NI, Marufuji KK                                    	1	11	 7S55-1           	27	1
776	  Intruction Sheet  WOOD ONE 6 Pages, CRESTEC                                	 7I11-1           	0	0	  Intruction Sheet  WOOD ONE 6 Pages, CRESTEC                                	1	11	 7I11-1           	27	1
777	  Iron Pin MARUNI / WOODONE 5x7NI, Marufuji KK                               	 7I14-1           	48690	23116309	  Iron Pin MARUNI / WOODONE 5x7NI, Marufuji KK                               	1	11	 7I14-1           	27	1
778	  AF SCREW DAIWA RAKUDA 4x65, Marufuji KK                                    	 7A5-1            	5412	0	  AF SCREW DAIWA RAKUDA 4x65, Marufuji KK                                    	1	11	 7A5-1            	27	1
779	  Dampor DI-4000 DAIWA RAKUDA, Marufuji KENZAI                               	 7D22-1           	244	0	  Dampor DI-4000 DAIWA RAKUDA, Marufuji KENZAI                               	1	11	 7D22-1           	27	1
780	  Dampor DAIWA RAKUDA 970A 1602, Topan Cosmo                                 	 7D16-1           	500	0	  Dampor DAIWA RAKUDA 970A 1602, Topan Cosmo                                 	1	11	 7D16-1           	27	1
781	  Socket screw DAIWA RAKUDA 2.7x13, Marufuji KK                              	 7S39-1           	19856	0	  Socket screw DAIWA RAKUDA 2.7x13, Marufuji KK                              	1	11	 7S39-1           	27	1
782	  Screw DAIWA RAKUDA AFTP 4x50, Marufuji KK                                  	 7S57-1           	5845	0	  Screw DAIWA RAKUDA AFTP 4x50, Marufuji KK                                  	1	11	 7S57-1           	27	1
783	  Baut tanpa kepala+ring NISSEN, NH Karya T                                  	 7B3-1            	103	0	  Baut tanpa kepala+ring NISSEN, NH Karya T                                  	1	11	 7B3-1            	27	1
784	  Cas Minifix NISSEN 12/12.Z1 BRIGHT, HAFELE                                 	 7C13-1           	106	0	  Cas Minifix NISSEN 12/12.Z1 BRIGHT, HAFELE                                 	1	11	 7C13-1           	27	1
785	  EURO THREAD DOWEL NISSEN 5x11mm, TITUSINDO                                 	 7E1-1            	3000	0	  EURO THREAD DOWEL NISSEN 5x11mm, TITUSINDO                                 	1	11	 7E1-1            	27	1
786	  Iron Pin Nikel NISSEN 5mmx19.5, Ginta Inti Pratama                         	 7I6-1            	2063	0	  Iron Pin Nikel NISSEN 5mmx19.5, Ginta Inti Pratama                         	1	11	 7I6-1            	27	1
787	  JB CAP WHITE NISSEN, A&S TRADE                                             	 7J2-1            	2000	0	  JB CAP WHITE NISSEN, A&S TRADE                                             	1	11	 7J2-1            	27	1
788	  Minifix Bolt NISSEN B24/5 VEL GALV, HAFELE                                 	 7M6-1            	85	0	  Minifix Bolt NISSEN B24/5 VEL GALV, HAFELE                                 	1	11	 7M6-1            	27	1
789	  Onime Nut B TYPE NISSEN M8x20.5, A&S TRADE                                 	 7O2-1            	952	0	  Onime Nut B TYPE NISSEN M8x20.5, A&S TRADE                                 	1	11	 7O2-1            	27	1
790	  SIDE ENTRY NISSEN 15/16(ZP) WHITE,  TITUSINDO                              	 7S35-1           	1779	0	  SIDE ENTRY NISSEN 15/16(ZP) WHITE,  TITUSINDO                              	1	11	 7S35-1           	27	1
791	  SYSTEM 15 NISSEN CAM NT06381WA,  A&S TRADE                                 	 7S36-1           	1000	0	  SYSTEM 15 NISSEN CAM NT06381WA,  A&S TRADE                                 	1	11	 7S36-1           	27	1
792	  Sara screw NISSEN 4.5x38UNI,  Marufuji K KK                                	 7S37-1           	216	0	  Sara screw NISSEN 4.5x38UNI,  Marufuji K KK                                	1	11	 7S37-1           	27	1
793	  Honey Camb Paper OTHER 15x400x800, Marufuji Kenzai Saitama                 	 7H19-1           	4	0	  Honey Camb Paper OTHER 15x400x800, Marufuji Kenzai Saitama                 	1	11	 7H19-1           	27	1
794	  BASIC SLIDE ON PLATE DAIKEN LOKAL, TITUS                                   	 7D28-2           	1927	0	  BASIC SLIDE ON PLATE DAIKEN LOKAL, TITUS                                   	1	11	 7D28-2           	27	1
795	  B-TYPE 110 HINGE (45/9.5)-0MM 248.0G54.050 DAIKEN LOKAL, TITUS             	 7D28-6           	1927	9078332	  B-TYPE 110 HINGE (45/9.5)-0MM 248.0G54.050 DAIKEN LOKAL, TITUS             	1	11	 7D28-6           	27	1
796	  GLISSANDO TL2 45N DAIKEN LOKAL, TITUS                                      	 7G133-1          	981	7198869	  GLISSANDO TL2 45N DAIKEN LOKAL, TITUS                                      	1	11	 7G133-1          	27	1
797	  HINGE MIRROR L=817.5MM DRILL WR-MBR-DJ-01-1A, YAN JIN IND.                 	 7H166-1          	0	0	  HINGE MIRROR L=817.5MM DRILL WR-MBR-DJ-01-1A, YAN JIN IND.                 	1	11	 7H166-1          	27	1
798	  HINGE MIRROR L=787.5MM DRILL (WR-MBR-DJ-01-2A), YAN JIN IND.               	 7H167-1          	0	0	  HINGE MIRROR L=787.5MM DRILL (WR-MBR-DJ-01-2A), YAN JIN IND.               	1	11	 7H167-1          	27	1
799	  HINGE MIRROR L=613.5MM DRILL (WR-MBR-DJ-01-3A), YAN JIN IND.               	 7H168-1          	0	0	  HINGE MIRROR L=613.5MM DRILL (WR-MBR-DJ-01-3A), YAN JIN IND.               	1	11	 7H168-1          	27	1
800	  HINGE MIRROR L=695MM DRILL (WR-MBR-DJ-01-4A), YAN JIN IND.                 	 7H169-1          	0	0	  HINGE MIRROR L=695MM DRILL (WR-MBR-DJ-01-4A), YAN JIN IND.                 	1	11	 7H169-1          	27	1
801	  HINGE MIRROR L=647.5MM DRILL (WR-MBR-DJ-01-5A), YAN JIN IND.               	 7H170-1          	0	0	  HINGE MIRROR L=647.5MM DRILL (WR-MBR-DJ-01-5A), YAN JIN IND.               	1	11	 7H170-1          	27	1
802	  HINGE MIRROR L=676MM DRILL (WR-MBR-DJ-01-6A), YAN JIN IND.                 	 7H171-1          	0	0	  HINGE MIRROR L=676MM DRILL (WR-MBR-DJ-01-6A), YAN JIN IND.                 	1	11	 7H171-1          	27	1
803	  HINGE MIRROR L=609.5MM DRILL (WR-MBR-DJ-01-7A), YAN JIN IND.               	 7H172-1          	0	0	  HINGE MIRROR L=609.5MM DRILL (WR-MBR-DJ-01-7A), YAN JIN IND.               	1	11	 7H172-1          	27	1
804	  HINGE MIRROR L=579.5MM DRILL (WR-MBR-DJ-01-8A), YAN JIN IND.               	 7H173-1          	0	0	  HINGE MIRROR L=579.5MM DRILL (WR-MBR-DJ-01-8A), YAN JIN IND.               	1	11	 7H173-1          	27	1
805	  HINGE MIRROR L=817.5MM GROOVE (WR-MBR-DJ-01-1B), YAN JIN IND.              	 7H174-1          	0	0	  HINGE MIRROR L=817.5MM GROOVE (WR-MBR-DJ-01-1B), YAN JIN IND.              	1	11	 7H174-1          	27	1
806	  HINGE MIRROR L=787.5MM GROOVE (WR-MBR-DJ-01-2B), YAN JIN IND.              	 7H175-1          	0	0	  HINGE MIRROR L=787.5MM GROOVE (WR-MBR-DJ-01-2B), YAN JIN IND.              	1	11	 7H175-1          	27	1
807	  HINGE MIRROR L=613.5MM GROOVE (WR-MBR-DJ-01-3B), YAN JIN IND.              	 7H176-1          	0	0	  HINGE MIRROR L=613.5MM GROOVE (WR-MBR-DJ-01-3B), YAN JIN IND.              	1	11	 7H176-1          	27	1
808	  HINGE MIRROR L=695MM GROOVE (WR-MBR-DJ-01-4B), YAN JIN IND.                	 7H177-1          	0	0	  HINGE MIRROR L=695MM GROOVE (WR-MBR-DJ-01-4B), YAN JIN IND.                	1	11	 7H177-1          	27	1
809	  HINGE MIRROR L=647.5MM GROOVE (WR-MBR-DJ-01-5B), YAN JIN IND.              	 7H178-1          	0	0	  HINGE MIRROR L=647.5MM GROOVE (WR-MBR-DJ-01-5B), YAN JIN IND.              	1	11	 7H178-1          	27	1
810	  HINGE MIRROR L=676MM GROOVE (WR-MBR-DJ-01-5B), YAN JIN IND.                	 7H179-1          	0	0	  HINGE MIRROR L=676MM GROOVE (WR-MBR-DJ-01-5B), YAN JIN IND.                	1	11	 7H179-1          	27	1
811	  HINGE MIRROR L=609.5MM GROOVE (WR-MBR-DJ-01-7B), YAN JIN IND.              	 7H180-1          	0	0	  HINGE MIRROR L=609.5MM GROOVE (WR-MBR-DJ-01-7B), YAN JIN IND.              	1	11	 7H180-1          	27	1
812	  HINGE MIRROR L=579.5MM GROOVE (WR-MBR-DJ-01-8B), YAN JIN IND.              	 7H181-1          	0	0	  HINGE MIRROR L=579.5MM GROOVE (WR-MBR-DJ-01-8B), YAN JIN IND.              	1	11	 7H181-1          	27	1
813	  Hold Cap DAIKEN LOKAL 13 DARK, MARUFUJI KK                                 	 7H143-1          	269	0	  Hold Cap DAIKEN LOKAL 13 DARK, MARUFUJI KK                                 	1	11	 7H143-1          	27	1
814	  Hold Cap DAIKEN LOKAL 13 WHITE, MARUFUJI KK                                	 7H144-1          	490	0	  Hold Cap DAIKEN LOKAL 13 WHITE, MARUFUJI KK                                	1	11	 7H144-1          	27	1
815	  LOCK NUT E26 M6X13 PELANGI, PANCAMAS ELITE                                 	 7L11-1           	15000	5100000	  LOCK NUT E26 M6X13 PELANGI, PANCAMAS ELITE                                 	1	11	 7L11-1           	27	1
816	  Dowel 10x100MM LOKAL, Sinar Abadi                                          	 7D30-1           	20	0	  Dowel 10x100MM LOKAL, Sinar Abadi                                          	1	11	 7D30-1           	27	1
817	  TOATARI WHITE D.JAKARTA  8.7x8x2400, ALVINY I                              	 7T5-1            	150	0	  TOATARI WHITE D.JAKARTA  8.7x8x2400, ALVINY I                              	1	11	 7T5-1            	27	1
818	  T.NUT STEEL BRIGHT M10 DAIKEN LOKAL, HAFELE I                              	 7T7-1            	1294	0	  T.NUT STEEL BRIGHT M10 DAIKEN LOKAL, HAFELE I                              	1	11	 7T7-1            	27	1
819	  ADJUST SCREW GALV M10x70mm DAIKEN LOKAL, HAFELE I                          	 7A9-1            	1446	0	  ADJUST SCREW GALV M10x70mm DAIKEN LOKAL, HAFELE I                          	1	11	 7A9-1            	27	1
820	  Adjusting Screw DAIKEN LOKAL N1 PL M10x20mm, HAFELE I                      	 7A8-1            	1543	0	  Adjusting Screw DAIKEN LOKAL N1 PL M10x20mm, HAFELE I                      	1	11	 7A8-1            	27	1
821	  Runing Roller DAIKEN LOKAL PL Brown (404.22.106),  HAFELE                  	 7R8-1            	11	0	  Runing Roller DAIKEN LOKAL PL Brown (404.22.106),  HAFELE                  	1	11	 7R8-1            	27	1
822	  OPK MANUAL IN LINE SLIDING MECH ANISM 1800MM DOOR FITTING, AKU             	 7O4-1            	0	0	  OPK MANUAL IN LINE SLIDING MECH ANISM 1800MM DOOR FITTING, AKU             	1	11	 7O4-1            	27	1
823	  OPK MANUAL IN LINE SLIDING VERTIKAL DOOR FRAME 3000MM DOOR FITTING, AKU    	 7O5-1            	0	0	  OPK MANUAL IN LINE SLIDING VERTIKAL DOOR FRAME 3000MM DOOR FITTING, AKU    	1	11	 7O5-1            	27	1
824	  Running GUIDE RAIL BROWN (404.14.133), HAFELE                              	 7R10-1           	127	4102834	  Running GUIDE RAIL BROWN (404.14.133), HAFELE                              	1	11	 7R10-1           	27	1
825	  Cap For Tanaita SHOES BOX AIN KO29-IPO,  PT AST                            	 7C5-1            	12660	0	  Cap For Tanaita SHOES BOX AIN KO29-IPO,  PT AST                            	1	11	 7C5-1            	27	1
826	  DABO RL SHOES BOX RM8x25,  Marufuji KK                                     	 7D5-1            	75000	7871794	  DABO RL SHOES BOX RM8x25,  Marufuji KK                                     	1	11	 7D5-1            	27	1
827	  DAISU CORE no.80E SHOES BOX  15x1000x250,  Marufuji KK                     	 7D29-1           	1056	4660525	  DAISU CORE no.80E SHOES BOX  15x1000x250,  Marufuji KK                     	1	11	 7D29-1           	27	1
828	  Hinge Mirror SHOES BOX 700 L-264.5, YAN JIN IND                            	 7H160-2          	9392	35399730	  Hinge Mirror SHOES BOX 700 L-264.5, YAN JIN IND                            	1	11	 7H160-2          	27	1
829	  Hinge Mirror SHOES BOX 525 L-320.5, YAN JIN IND                            	 7H161-2          	6530	25208085	  Hinge Mirror SHOES BOX 525 L-320.5, YAN JIN IND                            	1	11	 7H161-2          	27	1
830	  Iron Pin SHOES BOX Ø5x8x17,  A&S Trade                                     	 7I2-1            	1200	0	  Iron Pin SHOES BOX Ø5x8x17,  A&S Trade                                     	1	11	 7I2-1            	27	1
831	  Paper Core SHOES BOX 12x411x2000 W525, PT TRI W                            	 7P27-1           	2602	12474934	  Paper Core SHOES BOX 12x411x2000 W525, PT TRI W                            	1	11	 7P27-1           	27	1
832	  Screw  RSF #8x65-40 MC3 SHOES BOX 4.2x65, Marufuji KK                      	 7S13-2           	3700	1107338	  Screw  RSF #8x65-40 MC3 SHOES BOX 4.2x65, Marufuji KK                      	1	11	 7S13-2           	27	1
833	  Screw  RSF #8x65-40 MC3 SHOES BOX 4.2x65,  PT.Saga Teknindo                	 7S13-1           	12002	3556950	  Screw  RSF #8x65-40 MC3 SHOES BOX 4.2x65,  PT.Saga Teknindo                	1	11	 7S13-1           	27	1
834	  Screw For Wall 3.8X51 SHOES BOX TSAF 3.5x51MC3, PT.Saga Teknindo           	 7S14-1           	48951	9502838	  Screw For Wall 3.8X51 SHOES BOX TSAF 3.5x51MC3, PT.Saga Teknindo           	1	11	 7S14-1           	27	1
835	  Tana Pin Baru SHOES BOX,  Marufuji KK                                      	 7T1-1            	9950	4666497	  Tana Pin Baru SHOES BOX,  Marufuji KK                                      	1	11	 7T1-1            	27	1
836	  Dabo Hatome UNICLOSE 8mm NI8x6,  ATOM                                      	 7D12-1           	3808	0	  Dabo Hatome UNICLOSE 8mm NI8x6,  ATOM                                      	1	11	 7D12-1           	27	1
837	  System Sharf UNICLOSE 7SCam 18Gray,  A&S Trade                             	 7S26-1           	692	0	  System Sharf UNICLOSE 7SCam 18Gray,  A&S Trade                             	1	11	 7S26-1           	27	1
838	  System Sharf UNICLOSE 7SCam 15Gray,  A&S Trade                             	 7S27-1           	1450	0	  System Sharf UNICLOSE 7SCam 15Gray,  A&S Trade                             	1	11	 7S27-1           	27	1
839	  System Sharf UNICLOSE 7DCam 18Gray,  A&S Trade                             	 7S28-1           	413	0	  System Sharf UNICLOSE 7DCam 18Gray,  A&S Trade                             	1	11	 7S28-1           	27	1
840	  System Sharf UNICLOSE 7SCam 18White,  Marufuji KK                          	 7S29-1           	1600	0	  System Sharf UNICLOSE 7SCam 18White,  Marufuji KK                          	1	11	 7S29-1           	27	1
841	  System Sharf UNICLOSE 7SCam 15White,  Marufuji KK                          	 7S30-1           	2469	0	  System Sharf UNICLOSE 7SCam 15White,  Marufuji KK                          	1	11	 7S30-1           	27	1
842	  System Sharf UNICLOSE 7DCam 18White,  Marufuji KK                          	 7S31-1           	4106	0	  System Sharf UNICLOSE 7DCam 18White,  Marufuji KK                          	1	11	 7S31-1           	27	1
843	  Screw Taping JF4x5/8 UNICLOSE 3x16,  AJBS                                  	 7S32-1           	3500	157333	  Screw Taping JF4x5/8 UNICLOSE 3x16,  AJBS                                  	1	11	 7S32-1           	27	1
844	  Adjuster  ITOKI M6x26, TOKYO K                                             	 8A26-1           	500	3080330	  Adjuster  ITOKI M6x26, TOKYO K                                             	1	11	 8A26-1           	27	1
845	  C-PL METALLAMAT-A  ITOKI NI PL S4MM 316.51.504, HAFELE                     	 8C28-1           	16	46128	  C-PL METALLAMAT-A  ITOKI NI PL S4MM 316.51.504, HAFELE                     	1	11	 8C28-1           	27	1
846	  Cylinder Lock  ITOKI (A614S-26-CL-33B), MARUFUJI KK                        	 8C29-1           	100	13303484	  Cylinder Lock  ITOKI (A614S-26-CL-33B), MARUFUJI KK                        	1	11	 8C29-1           	27	1
847	  HINGE Tyoban Blum  ITOKI , KOIKE I                                         	 8H27-1           	1000	7653048	  HINGE Tyoban Blum  ITOKI , KOIKE I                                         	1	11	 8H27-1           	27	1
848	  Mur Nanas ITOKI E-28 M06X20, HASIL FASTINDO                                	 8M15-1           	730	458041	  Mur Nanas ITOKI E-28 M06X20, HASIL FASTINDO                                	1	11	 8M15-1           	27	1
849	  METALLAMAT-A  ITOKI 110 FOL 48/6 S 316.30.500, HAFELE                      	 8M16-1           	12	97404	  METALLAMAT-A  ITOKI 110 FOL 48/6 S 316.30.500, HAFELE                      	1	11	 8M16-1           	27	1
850	  METAL PLATE  ITOKI , ITOKI                                                 	 8M17-1           	4	0	  METAL PLATE  ITOKI , ITOKI                                                 	1	11	 8M17-1           	27	1
851	  METAL HOOK OF Door  ITOKI 1500/1200W White, ITOKI                          	 8M18-1           	4	0	  METAL HOOK OF Door  ITOKI 1500/1200W White, ITOKI                          	1	11	 8M18-1           	27	1
852	  METAL HOOK OF Door  ITOKI 1500/1200W Black, ITOKI                          	 8M19-1           	4	0	  METAL HOOK OF Door  ITOKI 1500/1200W Black, ITOKI                          	1	11	 8M19-1           	27	1
853	  METAL HOOK OF Drawer  ITOKI For1500 White, ITOKI                           	 8M20-1           	5	0	  METAL HOOK OF Drawer  ITOKI For1500 White, ITOKI                           	1	11	 8M20-1           	27	1
854	  METAL HOOK OF Drawer  ITOKI For1500 Black, ITOKI                           	 8M21-1           	4	0	  METAL HOOK OF Drawer  ITOKI For1500 Black, ITOKI                           	1	11	 8M21-1           	27	1
855	  METAL HOOK OF Drawer  ITOKI For1200 White, ITOKI                           	 8M22-1           	3	0	  METAL HOOK OF Drawer  ITOKI For1200 White, ITOKI                           	1	11	 8M22-1           	27	1
856	  METAL HOOK OF Drawer  ITOKI For1200 Black, ITOKI                           	 8M23-1           	5	0	  METAL HOOK OF Drawer  ITOKI For1200 Black, ITOKI                           	1	11	 8M23-1           	27	1
857	  PLUG  ITOKI, Marufuji KK                                                   	 8P159-1          	1	223939	  PLUG  ITOKI, Marufuji KK                                                   	1	11	 8P159-1          	27	1
858	  Screw ITOKI M06x10, MARUFUJI KK                                            	 8S72-1           	144	672574	  Screw ITOKI M06x10, MARUFUJI KK                                            	1	11	 8S72-1           	27	1
859	  Slide Rail  ITOKI (3618-250), MARUFUJI KK                                  	 8S73-1           	10	988386	  Slide Rail  ITOKI (3618-250), MARUFUJI KK                                  	1	11	 8S73-1           	27	1
860	  Washer Zagane Blum  ITOKI, KOIKE I                                         	 8W9-1            	1000	2509196	  Washer Zagane Blum  ITOKI, KOIKE I                                         	1	11	 8W9-1            	27	1
861	  Needle FELTO , MARUFUJI KK                                                 	 8N3-1            	1020	28174985	  Needle FELTO , MARUFUJI KK                                                 	1	11	 8N3-1            	27	1
862	  CAP White F.KULKAS (KD-772-3W), Marufuji KK                                	 8C26-1           	9000	25161122	  CAP White F.KULKAS (KD-772-3W), Marufuji KK                                	1	11	 8C26-1           	27	1
863	  CAP White Free F.KULKAS Samping, Marufuji KK                               	 8C26-2           	880	567543	  CAP White Free F.KULKAS Samping, Marufuji KK                               	1	11	 8C26-2           	27	1
864	  DRYWALL F.KULKAS BUGLE M.6X1-1/4 (4x30) ABU, AJBS                          	 8D9-1            	1000	56783	  DRYWALL F.KULKAS BUGLE M.6X1-1/4 (4x30) ABU, AJBS                          	1	11	 8D9-1            	27	1
865	  JB A Connection Bolt  F.KULKAS M6x26, Marufuji KK                          	 8J4-1            	1893	1902442	  JB A Connection Bolt  F.KULKAS M6x26, Marufuji KK                          	1	11	 8J4-1            	27	1
866	  Onime Nut F.Kulkas M8xL13, Marufuji KK                                     	 8O16-1           	2152	0	  Onime Nut F.Kulkas M8xL13, Marufuji KK                                     	1	11	 8O16-1           	27	1
867	  Onime Nut F.Kulkas M6x10, Marufuji KK                                      	 8O15-1           	13060	0	  Onime Nut F.Kulkas M6x10, Marufuji KK                                      	1	11	 8O15-1           	27	1
868	  Rail-L Plastik F.KULKAS , TOKYO K                                          	 8R3-1            	1197	4438221	  Rail-L Plastik F.KULKAS , TOKYO K                                          	1	11	 8R3-1            	27	1
869	  Rail-R Plastik F.KULKAS , TOKYO K                                          	 8R4-1            	1228	4553217	  Rail-R Plastik F.KULKAS , TOKYO K                                          	1	11	 8R4-1            	27	1
870	  Screw Handle F.KULKAS  4x20 Sankakurometa, MARUFUJI KK                     	 8S62-1           	2374	0	  Screw Handle F.KULKAS  4x20 Sankakurometa, MARUFUJI KK                     	1	11	 8S62-1           	27	1
871	  Screw F.KULKAS TP 3.5x14 Sara, MARUFUJI KK                                 	 8S63-1           	19598	0	  Screw F.KULKAS TP 3.5x14 Sara, MARUFUJI KK                                 	1	11	 8S63-1           	27	1
872	  Shelf Board Pin F.KULKAS SP5025-02(2pcs/set), TOKYO K                      	 8S64-1           	4532	0	  Shelf Board Pin F.KULKAS SP5025-02(2pcs/set), TOKYO K                      	1	11	 8S64-1           	27	1
873	  Screw F.KULKAS 4x45 Silver, TOKYO K                                        	 8S65-1           	3448	0	  Screw F.KULKAS 4x45 Silver, TOKYO K                                        	1	11	 8S65-1           	27	1
874	  Screw F.KULKAS  3x8, MARUFUJI KK                                           	 8S67-1           	2040	0	  Screw F.KULKAS  3x8, MARUFUJI KK                                           	1	11	 8S67-1           	27	1
875	  Screw F.KULKAS  3.5x16 NI, MARUFUJI KK                                     	 8S68-1           	7824	0	  Screw F.KULKAS  3.5x16 NI, MARUFUJI KK                                     	1	11	 8S68-1           	27	1
876	  Assy Screw FUKAI SCW5550, A&S T+Tokyo K                                    	 8A3-1            	46200	26969868	  Assy Screw FUKAI SCW5550, A&S T+Tokyo K                                    	1	11	 8A3-1            	27	1
877	  Bush CB FUKAI HH140P, A&S Trade                                            	 8B1-1            	2527	5196092	  Bush CB FUKAI HH140P, A&S Trade                                            	1	11	 8B1-1            	27	1
878	  Belt  FUKAI  10 x 400 Black, TOKYO  KOHSAKUSHO                             	 8B4-1            	8150	70763805	  Belt  FUKAI  10 x 400 Black, TOKYO  KOHSAKUSHO                             	1	11	 8B4-1            	27	1
879	  Catch Plate FUKAI HD180-01N, A&S T+Tokyo K                                 	 8C8-1            	3880	7632449	  Catch Plate FUKAI HD180-01N, A&S T+Tokyo K                                 	1	11	 8C8-1            	27	1
880	  Dowel FUKAI Ø6x25, Sinar Abadi                                             	 8D2-1            	55700	1169624	  Dowel FUKAI Ø6x25, Sinar Abadi                                             	1	11	 8D2-1            	27	1
881	  Dowel FUKAI Ø6x30, Sinar Abadi                                             	 8D3-1            	6500	142987	  Dowel FUKAI Ø6x30, Sinar Abadi                                             	1	11	 8D3-1            	27	1
882	  Dowel FUKAI Ø8x25, Sinar Abadi                                             	 8D4-1            	20000	480051	  Dowel FUKAI Ø8x25, Sinar Abadi                                             	1	11	 8D4-1            	27	1
883	  DRYWALL FUKAI  M.6X1-1/4 (6x30) HITAM, HASIL FASTINDO                      	 8D9-2            	2646	198354	  DRYWALL FUKAI  M.6X1-1/4 (6x30) HITAM, HASIL FASTINDO                      	1	11	 8D9-2            	27	1
884	  FEET FUKAI PAD 20, TOKYO K                                                 	 8F4-1            	26800	69448290	  FEET FUKAI PAD 20, TOKYO K                                                 	1	11	 8F4-1            	27	1
885	  Hinge Holder FUKAI , MARUFUJI KK                                           	 8H16-2           	324	0	  Hinge Holder FUKAI , MARUFUJI KK                                           	1	11	 8H16-2           	27	1
886	  HIKIDASI stopper FUKAI, Marufuji KK                                        	 8H3-1            	101	0	  HIKIDASI stopper FUKAI, Marufuji KK                                        	1	11	 8H3-1            	27	1
887	  HOOK Black FUKAI, TOKYO K                                                  	 8H28-1           	6950	58487960	  HOOK Black FUKAI, TOKYO K                                                  	1	11	 8H28-1           	27	1
888	  HOOK Screw 5x15 Black, TOKYO K                                             	 8H29-1           	5950	1539802	  HOOK Screw 5x15 Black, TOKYO K                                             	1	11	 8H29-1           	27	1
889	  Minifix 15 Bolt FUKAI GALFS24/M6 FMB, Hafele Indotama                      	 8M5-1            	640	274045	  Minifix 15 Bolt FUKAI GALFS24/M6 FMB, Hafele Indotama                      	1	11	 8M5-1            	27	1
890	  MINIFIX  FUKAI  NG 15/16 NICK 212, HAFELE I                                	 8M10-1           	715	420794	  MINIFIX  FUKAI  NG 15/16 NICK 212, HAFELE I                                	1	11	 8M10-1           	27	1
891	  Mur Nanas FUKAI E-22 M06X15, HASIL FASTINDO                                	 8M24-1           	114	101161	  Mur Nanas FUKAI E-22 M06X15, HASIL FASTINDO                                	1	11	 8M24-1           	27	1
892	  Onime Nut FUKAI M6xL15.5, Saitama                                          	 8O2-1            	13786	0	  Onime Nut FUKAI M6xL15.5, Saitama                                          	1	11	 8O2-1            	27	1
893	  Onime Nut FUKAI M6x13 NT066135, A&S Trade                                  	 8O1-1            	252	190659	  Onime Nut FUKAI M6x13 NT066135, A&S Trade                                  	1	11	 8O1-1            	27	1
894	  PLATE  FUKAI  , TOKYO K                                                    	 8PL1-1           	6876	3575985	  PLATE  FUKAI  , TOKYO K                                                    	1	11	 8PL1-1           	27	1
895	  PLATE Screw FUKAI M2.8x12 , TOKYO K                                        	 8PL2-1           	84	30036	  PLATE Screw FUKAI M2.8x12 , TOKYO K                                        	1	11	 8PL2-1           	27	1
896	  Poly Bag FUKAI 5x10, Multi Niaga                                           	 8P13-1           	40	0	  Poly Bag FUKAI 5x10, Multi Niaga                                           	1	11	 8P13-1           	27	1
897	  Slime Screw  FUKAI  3.3 x 25, MARUFUJI KK                                  	 8S75-1           	7000	1734523	  Slime Screw  FUKAI  3.3 x 25, MARUFUJI KK                                  	1	11	 8S75-1           	27	1
898	  Screw Tap JP FUKAI 8x1 1/2 FMB, Harapan Jaya                               	 8S20-1           	3750	595291	  Screw Tap JP FUKAI 8x1 1/2 FMB, Harapan Jaya                               	1	11	 8S20-1           	27	1
899	  Screw Tap JP FUKAI M8x1/2 (4x1/2) (4x12), AJBS                             	 8S20-2           	2457	182469	  Screw Tap JP FUKAI M8x1/2 (4x1/2) (4x12), AJBS                             	1	11	 8S20-2           	27	1
900	  Screw Tap JP FUKAI M8x1/2 (4x12), HASIL  FASTINDO                          	 8S20-3           	1484	119800	  Screw Tap JP FUKAI M8x1/2 (4x12), HASIL  FASTINDO                          	1	11	 8S20-3           	27	1
901	  Screw Tap JP FUKAI 8x1 1/4 (4x30), AJBS                                    	 8S21-2           	3340	287476	  Screw Tap JP FUKAI 8x1 1/4 (4x30), AJBS                                    	1	11	 8S21-2           	27	1
902	  Screw TAP PH 08x1 1/4 +PUTIH FULL (4x30),TOKO HASIL                        	 8S21-3           	800	98909	  Screw TAP PH 08x1 1/4 +PUTIH FULL (4x30),TOKO HASIL                        	1	11	 8S21-3           	27	1
903	  Screw Taping FUKAI 4x20 / 8x3/4 Kuning, AJBS                               	 8S5-2            	3340	323839	  Screw Taping FUKAI 4x20 / 8x3/4 Kuning, AJBS                               	1	11	 8S5-2            	27	1
904	  Screw Taping FUKAI 4x20 / 8x3/4 Kuning, HASIL FASTINDO                     	 8S5-3            	3200	281454	  Screw Taping FUKAI 4x20 / 8x3/4 Kuning, HASIL FASTINDO                     	1	11	 8S5-3            	27	1
905	  Screw Handle FUKAI M4x25 FS890/1170W kuning, AJBS                          	 8S23-1           	2102	210491	  Screw Handle FUKAI M4x25 FS890/1170W kuning, AJBS                          	1	11	 8S23-1           	27	1
906	  Screw Handle FUKAI M4x25 FS890/1170W kuning, Toko Hasil                    	 8S23-2           	1600	110727	  Screw Handle FUKAI M4x25 FS890/1170W kuning, Toko Hasil                    	1	11	 8S23-2           	27	1
907	  Screw Handle FUKAI M4x20 FS1000/1170, A&S Trade+Marufuji                   	 8S15-1           	4568	1534323	  Screw Handle FUKAI M4x20 FS1000/1170, A&S Trade+Marufuji                   	1	11	 8S15-1           	27	1
908	  Screw FUKAI  4x45 Silver, Tokyo M                                          	 8S60-1           	25980	12933659	  Screw FUKAI  4x45 Silver, Tokyo M                                          	1	11	 8S60-1           	27	1
909	  Screw  FUKAI  5x20,  TOKYO KOHSAKUSHO                                      	 8S76-1           	6000	0	  Screw  FUKAI  5x20,  TOKYO KOHSAKUSHO                                      	1	11	 8S76-1           	27	1
910	  Screw FUKAI 4x45 BLACK F600SB, Tokyo Koshakusho                            	 8S53-1           	9691	4820395	  Screw FUKAI 4x45 BLACK F600SB, Tokyo Koshakusho                            	1	11	 8S53-1           	27	1
911	  Screw FUKAI TP PAI 3x12 FS1000/FS1170, Marufuji KK                         	 8S18-1           	44326	0	  Screw FUKAI TP PAI 3x12 FS1000/FS1170, Marufuji KK                         	1	11	 8S18-1           	27	1
912	  SCREW FUKAI TSAF 3X12 MC3, SAGA TEKNINDO                                   	 8S70-1           	43160	2710883	  SCREW FUKAI TSAF 3X12 MC3, SAGA TEKNINDO                                   	1	11	 8S70-1           	27	1
913	  Screw FUKAI TSAT Htm 8x5/8" / 4x16 BBC, AJBS                               	 8S4-1            	5800	415931	  Screw FUKAI TSAT Htm 8x5/8" / 4x16 BBC, AJBS                               	1	11	 8S4-1            	27	1
914	  Screw FUKAI 4x18 FS1000/1170, Harapan Jaya                                 	 8S16-1           	711	0	  Screw FUKAI 4x18 FS1000/1170, Harapan Jaya                                 	1	11	 8S16-1           	27	1
915	  Screw & Protector FUKAI HG130M8P-03&04(N), TOKYO K                         	 8S48-1           	2345	1515219	  Screw & Protector FUKAI HG130M8P-03&04(N), TOKYO K                         	1	11	 8S48-1           	27	1
916	  Screw Magnet FUKAI MRSP3116Z Silver, A&S Trade+Tokyo                       	 8S3-1            	19435	2780875	  Screw Magnet FUKAI MRSP3116Z Silver, A&S Trade+Tokyo                       	1	11	 8S3-1            	27	1
917	  Socket Nylon FUKAI  Insert 10/11mm /MG, TITUSINDO S                        	 8S19-2           	37110	9090780	  Socket Nylon FUKAI  Insert 10/11mm /MG, TITUSINDO S                        	1	11	 8S19-2           	27	1
918	  Shelf Board Pin FUKAI SP5025-02(2pcs/set), Tokyo K                         	 8S2-1            	23704	7002471	  Shelf Board Pin FUKAI SP5025-02(2pcs/set), Tokyo K                         	1	11	 8S2-1            	27	1
919	  Shelf Dowel FUKAI TDH0623470 Ø5, Saitama                                   	 8S6-1            	3097	3918593	  Shelf Dowel FUKAI TDH0623470 Ø5, Saitama                                   	1	11	 8S6-1            	27	1
920	  System 5 Shaft FUKAI MT05746 M6x24, Marufuji KK                            	 8S7-1            	1042	931554	  System 5 Shaft FUKAI MT05746 M6x24, Marufuji KK                            	1	11	 8S7-1            	27	1
921	  Sistem 5 Shaft / COMBI P-DOWEL 24XM6, PT. TITUS TEKFORM IND.               	 8S7-4            	35199	9678677	  Sistem 5 Shaft / COMBI P-DOWEL 24XM6, PT. TITUS TEKFORM IND.               	1	11	 8S7-4            	27	1
922	  System Cam FUKAI MT05644 NIL11.9, Marufuji KK                              	 8S8-1            	29	8943	  System Cam FUKAI MT05644 NIL11.9, Marufuji KK                              	1	11	 8S8-1            	27	1
923	  System Cam FUKAI MT05644 NIL11.9, Marufuji KK                              	 8S8-2            	1412	435451	  System Cam FUKAI MT05644 NIL11.9, Marufuji KK                              	1	11	 8S8-2            	27	1
924	  System Cam FUKAI 3000.15x15/16BD/HEX.2N, TITUSINDO S                       	 8S8-3            	37256	12105969	  System Cam FUKAI 3000.15x15/16BD/HEX.2N, TITUSINDO S                       	1	11	 8S8-3            	27	1
925	  System Cam FUKAI 12X12/13 BD, TITUS TEKFORM                                	 8S69-1           	2165	650603	  System Cam FUKAI 12X12/13 BD, TITUS TEKFORM                                	1	11	 8S69-1           	27	1
926	  Support Belt FUKAI SABE110-01, TOKYO K                                     	 8S1-1            	2527	0	  Support Belt FUKAI SABE110-01, TOKYO K                                     	1	11	 8S1-1            	27	1
927	  WASHER FUKAI, FUKAI MUSEN K                                                	 8W6-1            	953	0	  WASHER FUKAI, FUKAI MUSEN K                                                	1	11	 8W6-1            	27	1
928	  WOOD SCREW PF/RS M 3.1X22 PUTIH (5X7/8"), MEGA WAJA                        	 8W8-1            	38620	1825248	  WOOD SCREW PF/RS M 3.1X22 PUTIH (5X7/8"), MEGA WAJA                        	1	11	 8W8-1            	27	1
929	  Wood Screw FUKAI MRSP3832 MSWR3.8x32 BBC, Sagateknindo                     	 8W2-1            	14086	2607019	  Wood Screw FUKAI MRSP3832 MSWR3.8x32 BBC, Sagateknindo                     	1	11	 8W2-1            	27	1
930	  WL( S )( Ring ) FUKAI FS1000/1170, A&S Trade                               	 8W3-1            	3000	81956	  WL( S )( Ring ) FUKAI FS1000/1170, A&S Trade                               	1	11	 8W3-1            	27	1
931	  Intruction MARUNI Manual book, CRESTEC                                     	 8I20-2           	12	0	  Intruction MARUNI Manual book, CRESTEC                                     	1	11	 8I20-2           	27	1
932	  MINIFIX Housing 15/12 ZI.BL MARUNI,  HAFELE INDOTAMA                       	 8M13-1           	255	248300	  MINIFIX Housing 15/12 ZI.BL MARUNI,  HAFELE INDOTAMA                       	1	11	 8M13-1           	27	1
933	  MINIFIX BL Tise B24/5 YEI GALV MARUNI,  HAFELE INDOTAMA                    	 8M14-1           	353	161767	  MINIFIX BL Tise B24/5 YEI GALV MARUNI,  HAFELE INDOTAMA                    	1	11	 8M14-1           	27	1
934	  Ornament MARUNI L=1149, Tokyo K                                            	 8O8-1            	100	0	  Ornament MARUNI L=1149, Tokyo K                                            	1	11	 8O8-1            	27	1
935	  Screw MARUNI 2.6x12(UCP) , Marufuji KK                                     	 8S71-1           	2008	0	  Screw MARUNI 2.6x12(UCP) , Marufuji KK                                     	1	11	 8S71-1           	27	1
936	  Onime Nut SONY D.M5x13, Topan Cosmo                                        	 8O10-1           	16970	0	  Onime Nut SONY D.M5x13, Topan Cosmo                                        	1	11	 8O10-1           	27	1
937	  Onime Nut SONY M5E Type, Perfect P                                         	 8O9-1            	9198	0	  Onime Nut SONY M5E Type, Perfect P                                         	1	11	 8O9-1            	27	1
938	  Adjuster UNICHARM M8xL16, Marufuji Kenzai KK                               	 8A18-1           	140	0	  Adjuster UNICHARM M8xL16, Marufuji Kenzai KK                               	1	11	 8A18-1           	27	1
939	  OnimeNut UNICHARM MUx10(M8xL15.5), Marufuji KK                             	 8O11-1           	138	0	  OnimeNut UNICHARM MUx10(M8xL15.5), Marufuji KK                             	1	11	 8O11-1           	27	1
940	  Draw Runner Self UNICHARM CLWH 450m/m set, Hafele I                        	 8D8-1            	336	0	  Draw Runner Self UNICHARM CLWH 450m/m set, Hafele I                        	1	11	 8D8-1            	27	1
941	  Cas Minifix PANASONIC 12/12Zi BRIGHT, Hafele                               	 8C15-1           	118	0	  Cas Minifix PANASONIC 12/12Zi BRIGHT, Hafele                               	1	11	 8C15-1           	27	1
942	  JOINT PIN KRPG14 PANASONIC Screw VB35, TITUS INDO S                        	 8J3-2            	7594	0	  JOINT PIN KRPG14 PANASONIC Screw VB35, TITUS INDO S                        	1	11	 8J3-2            	27	1
943	  Minifi Bltise PANASONIC B24/5 Yellow, Hafele                               	 8M7-1            	13586	0	  Minifi Bltise PANASONIC B24/5 Yellow, Hafele                               	1	11	 8M7-1            	27	1
944	  SIDE ENTRY VB-35 PANASONIC LTBrown, Titus Indo S                           	 8S42-1           	5239	0	  SIDE ENTRY VB-35 PANASONIC LTBrown, Titus Indo S                           	1	11	 8S42-1           	27	1
945	  SIDE ENTRY VB-35 PANASONIC White, Titus Indo S                             	 8S43-1           	3800	0	  SIDE ENTRY VB-35 PANASONIC White, Titus Indo S                             	1	11	 8S43-1           	27	1
946	  CAP D.KANTANA Cover White, Marufuji KK                                     	 8C17-1           	12938	3807586	  CAP D.KANTANA Cover White, Marufuji KK                                     	1	11	 8C17-1           	27	1
947	  Craft Tape D.KANTANA , MARUFUJI KK                                         	 8C23-1           	255	478966	  Craft Tape D.KANTANA , MARUFUJI KK                                         	1	11	 8C23-1           	27	1
948	  Load Bearing Seal D.KANTANA to 15kg, Marufuji KK                           	 8L4-1            	3355	0	  Load Bearing Seal D.KANTANA to 15kg, Marufuji KK                           	1	11	 8L4-1            	27	1
949	  Load Bearing Seal D.KANTANA to 20kg, Marufuji KK                           	 8L5-1            	11770	0	  Load Bearing Seal D.KANTANA to 20kg, Marufuji KK                           	1	11	 8L5-1            	27	1
950	  Load Bearing Seal D.KANTANA to 30kg, Marufuji KK                           	 8L6-1            	4370	0	  Load Bearing Seal D.KANTANA to 30kg, Marufuji KK                           	1	11	 8L6-1            	27	1
951	  Screw D.KANTANA 3.5x30 Yunikuro, Marufuji KK                               	 8S49-1           	80829	19282099	  Screw D.KANTANA 3.5x30 Yunikuro, Marufuji KK                               	1	11	 8S49-1           	27	1
952	  Screw D.KANTANA 3.8x51 Yunikuro, Marufuji KK                               	 8S50-1           	7947	2402409	  Screw D.KANTANA 3.8x51 Yunikuro, Marufuji KK                               	1	11	 8S50-1           	27	1
953	  Screw D.KANTANA 3.5x16 Yunikuro, Marufuji KK                               	 8S51-1           	120854	17526219	  Screw D.KANTANA 3.5x16 Yunikuro, Marufuji KK                               	1	11	 8S51-1           	27	1
954	  Screw D.KANTANA 3.5x12 Yunikuro, Marufuji KK                               	 8S52-1           	201636	32946846	  Screw D.KANTANA 3.5x12 Yunikuro, Marufuji KK                               	1	11	 8S52-1           	27	1
955	  Washer D.KANTANA set cap washer 19x22x32cm, Marufuji KK                    	 8W5-1            	12906	4717152	  Washer D.KANTANA set cap washer 19x22x32cm, Marufuji KK                    	1	11	 8W5-1            	27	1
956	  Onime Nut TOTO TS/SLIM J SERIES M4x L11.5                                  	 7O1-1            	4999	0	  Onime Nut TOTO TS/SLIM J SERIES M4x L11.5                                  	1	11	 7O1-1            	27	1
957	  Dowel TOTO KITCHEN 8.1x31, Sinar Abadi                                     	 7D14-1           	95000	2469965	  Dowel TOTO KITCHEN 8.1x31, Sinar Abadi                                     	1	11	 7D14-1           	27	1
958	  Dowel 12x1820MM U/ JIG NAS CLOUCK, SUNJAYA COATING P.                      	 7D31-1           	5	0	  Dowel 12x1820MM U/ JIG NAS CLOUCK, SUNJAYA COATING P.                      	1	11	 7D31-1           	27	1
959	  Joint Bracket TENMA (white),  Tokyo Kuhsakusho                             	 7J1-3            	113213	88593920	  Joint Bracket TENMA (white),  Tokyo Kuhsakusho                             	1	11	 7J1-3            	27	1
960	  Screw TENMA TSAB 3x12 MC NEW FINISHING, NITTO ALAM                         	 7S11-7           	44164	2472925	  Screw TENMA TSAB 3x12 MC NEW FINISHING, NITTO ALAM                         	1	11	 7S11-7           	27	1
961	  Screw TENMA AT3x12 MC, Marufuji KK                                         	 7S11-6           	2200	372879	  Screw TENMA AT3x12 MC, Marufuji KK                                         	1	11	 7S11-6           	27	1
962	  Sticker MEBEL TENMA  ARTPAPER (68Pcs/Lbr), CRESTEC                         	 7S58-2           	17887	419762	  Sticker MEBEL TENMA  ARTPAPER (68Pcs/Lbr), CRESTEC                         	1	11	 7S58-2           	27	1
963	  Paper Core/NKN TENMA 18x329x270,  Marufuji K Saitama                       	 7P24-1           	2025	18126118	  Paper Core/NKN TENMA 18x329x270,  Marufuji K Saitama                       	1	11	 7P24-1           	27	1
964	  Paper Core/NKN TENMA 18x329x270,  TOKYO K                                  	 7P24-2           	452	4045929	  Paper Core/NKN TENMA 18x329x270,  TOKYO K                                  	1	11	 7P24-2           	27	1
965	  Paper Core/NKN TENMA 18x329x470,  Marufuji K Saitama                       	 7P25-1           	3240	51533170	  Paper Core/NKN TENMA 18x329x470,  Marufuji K Saitama                       	1	11	 7P25-1           	27	1
966	  Paper Core/NKN TENMA 18x329x470,  TOKYO K                                  	 7P25-2           	275	4373957	  Paper Core/NKN TENMA 18x329x470,  TOKYO K                                  	1	11	 7P25-2           	27	1
967	  Paper Core/NKN TENMA 18x329x670,  Marufuji K Saitama                       	 7P26-2           	0	0	  Paper Core/NKN TENMA 18x329x670,  Marufuji K Saitama                       	1	11	 7P26-2           	27	1
968	  Paper Core/NKN TENMA 18x329x670,  TOKYO K                                  	 7P26-3           	5940	120155818	  Paper Core/NKN TENMA 18x329x670,  TOKYO K                                  	1	11	 7P26-3           	27	1
969	  Paper Core/NKN TENMA 18x329x570,  TOKYO K                                  	 7P35-2           	247	4356861	  Paper Core/NKN TENMA 18x329x570,  TOKYO K                                  	1	11	 7P35-2           	27	1
970	  Paper Core/Corrugated Board 18x329x270 (A+A), Tokyo K                      	 7P37-1           	3	0	  Paper Core/Corrugated Board 18x329x270 (A+A), Tokyo K                      	1	11	 7P37-1           	27	1
971	  Paper Core/Corrugated Board 18x329x470 (A+A), Tokyo K                      	 7P38-1           	3	0	  Paper Core/Corrugated Board 18x329x470 (A+A), Tokyo K                      	1	11	 7P38-1           	27	1
972	  Paper Core/Corrugated Board 18x329x570 (A+A), Tokyo K                      	 7P39-1           	3	0	  Paper Core/Corrugated Board 18x329x570 (A+A), Tokyo K                      	1	11	 7P39-1           	27	1
973	  Paper Core/Corrugated Board 18x329x270 (A+B), Tokyo K                      	 7P37-2           	3	0	  Paper Core/Corrugated Board 18x329x270 (A+B), Tokyo K                      	1	11	 7P37-2           	27	1
974	  Paper Core/Corrugated Board 18x329x470 (A+B), Tokyo K                      	 7P38-2           	3	0	  Paper Core/Corrugated Board 18x329x470 (A+B), Tokyo K                      	1	11	 7P38-2           	27	1
975	  Paper Core/Corrugated Board 18x329x570 (A+B), Tokyo K                      	 7P39-2           	3	0	  Paper Core/Corrugated Board 18x329x570 (A+B), Tokyo K                      	1	11	 7P39-2           	27	1
976	  Honeycomb 30 x 560 x 943, RSA                                              	 7H112-2          	5	0	  Honeycomb 30 x 560 x 943, RSA                                              	1	11	 7H112-2          	27	1
977	  Honeycomb 30 x 560 x 1014, RSA                                             	 7H112-3          	5	0	  Honeycomb 30 x 560 x 1014, RSA                                             	1	11	 7H112-3          	27	1
978	  Honeycomb 30 x 587 x 358, RSA                                              	 7H136-1          	108	0	  Honeycomb 30 x 587 x 358, RSA                                              	1	11	 7H136-1          	27	1
979	  Honeycomb 30 x 587 x 493, RSA                                              	 7H137-1          	108	0	  Honeycomb 30 x 587 x 493, RSA                                              	1	11	 7H137-1          	27	1
980	  Honeycomb 30 x 587 x 1045, RSA                                             	 7H138-1          	108	0	  Honeycomb 30 x 587 x 1045, RSA                                             	1	11	 7H138-1          	27	1
981	  Honeycomb 30 x 593 x 404, RSA                                              	 7H147-1          	114	0	  Honeycomb 30 x 593 x 404, RSA                                              	1	11	 7H147-1          	27	1
982	  Honeycomb 30 x 593 x 704, RSA                                              	 7H148-1          	114	0	  Honeycomb 30 x 593 x 704, RSA                                              	1	11	 7H148-1          	27	1
983	  Honeycomb 30 x 638 x 404, RSA                                              	 7H145-1          	262	0	  Honeycomb 30 x 638 x 404, RSA                                              	1	11	 7H145-1          	27	1
984	  Honeycomb 30 x 638 x 704, RSA                                              	 7H146-1          	132	0	  Honeycomb 30 x 638 x 704, RSA                                              	1	11	 7H146-1          	27	1
985	  Honeycomb 30 x 660 x 943, RSA                                              	 7H112-4          	2	0	  Honeycomb 30 x 660 x 943, RSA                                              	1	11	 7H112-4          	27	1
986	  Honeycomb 30 x 660 x 1014, RSA                                             	 7H112-5          	2	0	  Honeycomb 30 x 660 x 1014, RSA                                             	1	11	 7H112-5          	27	1
987	  Honeycomb Axxor 150x1300x22x18.3, ALPHA UTAMA MANDIRI                      	 7H149-4          	2338	21151133	  Honeycomb Axxor 150x1300x22x18.3, ALPHA UTAMA MANDIRI                      	1	11	 7H149-4          	27	1
988	  Honeycomb Axxor 150X1300X22X21.3MM, ALPHA UTAMA MANDIRI                    	 7H164-1          	1702	16412263	  Honeycomb Axxor 150X1300X22X21.3MM, ALPHA UTAMA MANDIRI                    	1	11	 7H164-1          	27	1
989	  Honeycomb Axxor 150X1250X22X21.3MM, ALPHA UTAMA MANDIRI                    	 7H164-2          	2078	20364400	  Honeycomb Axxor 150X1250X22X21.3MM, ALPHA UTAMA MANDIRI                    	1	11	 7H164-2          	27	1
990	  Honeycomb 34 x 966 x 1208, RSA                                             	 7H124-1          	38	0	  Honeycomb 34 x 966 x 1208, RSA                                             	1	11	 7H124-1          	27	1
991	  CARTON DAITO DTD W650-01, SINAR ERA BOX                                    	 3DT1-2           	109	1413627	  CARTON DAITO DTD W650-01, SINAR ERA BOX                                    	1	14	 3DT1-2           	27	1
992	  CARTON DAITO DTD W650-02, SINAR ERA BOX                                    	 3DT2-2           	188	200189	  CARTON DAITO DTD W650-02, SINAR ERA BOX                                    	1	14	 3DT2-2           	27	1
993	  CARTON DAITO DTD W650-03, SINAR ERA BOX                                    	 3DT3-2           	173	618310	  CARTON DAITO DTD W650-03, SINAR ERA BOX                                    	1	14	 3DT3-2           	27	1
994	  CARTON DAITO DTD W800.2.16-01, CMB                                         	 3DT4-1           	49	708285	  CARTON DAITO DTD W800.2.16-01, CMB                                         	1	14	 3DT4-1           	27	1
995	  CARTON DAITO DTD W800.2.16-02, CMB                                         	 3DT5-1           	116	123864	  CARTON DAITO DTD W800.2.16-02, CMB                                         	1	14	 3DT5-1           	27	1
996	  CARTON DAITO DTD W800.2.17-01, SINAR ERA BOX                               	 3DT6-3           	93	1286376	  CARTON DAITO DTD W800.2.17-01, SINAR ERA BOX                               	1	14	 3DT6-3           	27	1
997	  CARTON DAITO DTD W800.2.17-02, SINAR ERA BOX                               	 3DT7-2           	75	74100	  CARTON DAITO DTD W800.2.17-02, SINAR ERA BOX                               	1	14	 3DT7-2           	27	1
998	  CARTON BOX PK DAI TSS-01 (TORISETSU SYUNO), SINAR ERA BOX                  	 3DS38-2          	1700	4945592	  CARTON BOX PK DAI TSS-01 (TORISETSU SYUNO), SINAR ERA BOX                  	1	14	 3DS38-2          	1	1
999	  CARTON BOX MK-MC900-NAS-01 (MAGUCHI SYUNO), CMB                            	 3DM1-1           	29	187369	  CARTON BOX MK-MC900-NAS-01 (MAGUCHI SYUNO), CMB                            	1	14	 3DM1-1           	27	1
1000	  CARTON Lyr MK-MC900-NAS-02 (MAGUCHI SYUNO), CMB                            	 3DM1-2           	62	42480	  CARTON Lyr MK-MC900-NAS-02 (MAGUCHI SYUNO), CMB                            	1	14	 3DM1-2           	27	1
1001	  CARTON Lyr MK-MC900-NAS-03 (MAGUCHI SYUNO), CMB                            	 3DM1-3           	32	4005	  CARTON Lyr MK-MC900-NAS-03 (MAGUCHI SYUNO), CMB                            	1	14	 3DM1-3           	27	1
1002	  CARTON D.SHOES MK Flort Shijisan/FS-F, RSA                                 	 3DS10-4          	1360	2162394	  CARTON D.SHOES MK Flort Shijisan/FS-F, RSA                                 	1	14	 3DS10-4          	27	1
1003	  CARTON D.SHOES MK Flort Shijisan/FS-F Layer 2, RSA                         	 3DS11-2          	2655	217707	  CARTON D.SHOES MK Flort Shijisan/FS-F Layer 2, RSA                         	1	14	 3DS11-2          	27	1
1004	  CARTON D.SHOES Ly SINGLE FACE W=1600, MITRA A                              	 3DS37-1          	100	1000000	  CARTON D.SHOES Ly SINGLE FACE W=1600, MITRA A                              	1	14	 3DS37-1          	1	1
1005	  CARTON DAIKEN DOOR ALL, Packing I                                          	 3DD1-2           	239	539269	  CARTON DAIKEN DOOR ALL, Packing I                                          	1	14	 3DD1-2           	27	1
1006	  CARTON D.CLOUK FEJ200-12 LAYER 1, Mitra Abadi                              	 3DC1-1           	828	389574	  CARTON D.CLOUK FEJ200-12 LAYER 1, Mitra Abadi                              	1	14	 3DC1-1           	27	1
1007	  CARTON D.CLOUK FEJ200-12 LAYER 2, Mitra Abadi                              	 3DC2-1           	785	271800	  CARTON D.CLOUK FEJ200-12 LAYER 2, Mitra Abadi                              	1	14	 3DC2-1           	27	1
1008	  CARTON D.CLOUK FEJ200-12 LAYER 3, Mitra Abadi                              	 3DC3-1           	2325	6019146	  CARTON D.CLOUK FEJ200-12 LAYER 3, Mitra Abadi                              	1	14	 3DC3-1           	27	1
1009	  CARTON D.CLOUK FEJ200-22 LAYER 1, Mitra Abadi                              	 3DC4-1           	537	341844	  CARTON D.CLOUK FEJ200-22 LAYER 1, Mitra Abadi                              	1	14	 3DC4-1           	27	1
1010	  CARTON D.CLOUK FEJ200-22 LAYER 2, Mitra Abadi                              	 3DC5-1           	1319	321121	  CARTON D.CLOUK FEJ200-22 LAYER 2, Mitra Abadi                              	1	14	 3DC5-1           	27	1
1011	  CARTON D.CLOUK FEJ200-22 LAYER 3, Mitra Abadi                              	 3DC6-1           	1660	4882680	  CARTON D.CLOUK FEJ200-22 LAYER 3, Mitra Abadi                              	1	14	 3DC6-1           	27	1
1012	  CARTON D.CLOUK FEJ200-32 LAYER 1, Mitra Abadi                              	 3DC7-1           	388	704747	  CARTON D.CLOUK FEJ200-32 LAYER 1, Mitra Abadi                              	1	14	 3DC7-1           	27	1
1013	  CARTON D.CLOUK FEJ200-32 LAYER 2, Mitra Abadi                              	 3DC8-1           	1408	383723	  CARTON D.CLOUK FEJ200-32 LAYER 2, Mitra Abadi                              	1	14	 3DC8-1           	27	1
1014	  CARTON D.CLOUK FEJ200-32 LAYER 3, Mitra Abadi                              	 3DC9-1           	2045	7146194	  CARTON D.CLOUK FEJ200-32 LAYER 3, Mitra Abadi                              	1	14	 3DC9-1           	27	1
1015	  CARTON D.CLOUK FFB 312/302-11 LAYER 1, Mitra Abadi                         	 3DC10-1          	625	648294	  CARTON D.CLOUK FFB 312/302-11 LAYER 1, Mitra Abadi                         	1	14	 3DC10-1          	27	1
1016	  CARTON D.CLOUK FFB 312/302-11 LAYER 2, Mitra Abadi                         	 3DC11-1          	589	242725	  CARTON D.CLOUK FFB 312/302-11 LAYER 2, Mitra Abadi                         	1	14	 3DC11-1          	27	1
1017	  CARTON D.CLOUK FFB 312/302-11 LAYER 3, Mitra Abadi                         	 3DC12-1          	3176	8053078	  CARTON D.CLOUK FFB 312/302-11 LAYER 3, Mitra Abadi                         	1	14	 3DC12-1          	27	1
1018	  CARTON D.CLOUK FFB 312/302-15 LAYER 1, Mitra Abadi                         	 3DC19-1          	115	169379	  CARTON D.CLOUK FFB 312/302-15 LAYER 1, Mitra Abadi                         	1	14	 3DC19-1          	27	1
1019	  CARTON D.CLOUK FFB 312/302-15 LAYER 2, Mitra Abadi                         	 3DC20-1          	137	110460	  CARTON D.CLOUK FFB 312/302-15 LAYER 2, Mitra Abadi                         	1	14	 3DC20-1          	27	1
1020	  CARTON D.CLOUK FFB 312/302-21 LAYER 1, Mitra Abadi                         	 3DC25-1          	592	512460	  CARTON D.CLOUK FFB 312/302-21 LAYER 1, Mitra Abadi                         	1	14	 3DC25-1          	27	1
1021	  CARTON D.CLOUK FFB 312/302-21 LAYER 2, Mitra Abadi                         	 3DC26-1          	574	268650	  CARTON D.CLOUK FFB 312/302-21 LAYER 2, Mitra Abadi                         	1	14	 3DC26-1          	27	1
1022	  CARTON D.CLOUK FFB 312/302-25 LAYER 1, Mitra Abadi                         	 3DC34-1          	135	166279	  CARTON D.CLOUK FFB 312/302-25 LAYER 1, Mitra Abadi                         	1	14	 3DC34-1          	27	1
1023	  CARTON D.CLOUK FFB 312/302-25 LAYER 2, Mitra Abadi                         	 3DC35-1          	158	99878	  CARTON D.CLOUK FFB 312/302-25 LAYER 2, Mitra Abadi                         	1	14	 3DC35-1          	27	1
1024	  CARTON D.CLOUK FFB 312/302-32 LAYER 1, Mitra Abadi                         	 3DC43-1          	955	1025973	  CARTON D.CLOUK FFB 312/302-32 LAYER 1, Mitra Abadi                         	1	14	 3DC43-1          	27	1
1025	  CARTON D.CLOUK FFB 312/302-32 LAYER 2, Mitra Abadi                         	 3DC44-1          	1212	531288	  CARTON D.CLOUK FFB 312/302-32 LAYER 2, Mitra Abadi                         	1	14	 3DC44-1          	27	1
1026	  CARTON D.CLOUK FFB 312/302-32 LAYER 3, Mitra Abadi                         	 3DC45-1          	1148	4308500	  CARTON D.CLOUK FFB 312/302-32 LAYER 3, Mitra Abadi                         	1	14	 3DC45-1          	27	1
1027	  CARTON D.CLOUK FFB 312/302-43 LAYER 1, Mitra Abadi                         	 3DC55-1          	300	236643	  CARTON D.CLOUK FFB 312/302-43 LAYER 1, Mitra Abadi                         	1	14	 3DC55-1          	27	1
1028	  CARTON D.CLOUK FFB 312/302-43 LAYER 2, Mitra Abadi                         	 3DC56-1          	368	132600	  CARTON D.CLOUK FFB 312/302-43 LAYER 2, Mitra Abadi                         	1	14	 3DC56-1          	27	1
1029	  CARTON D.CLOUK FFB 312/302-43 LAYER 3, Mitra Abadi                         	 3DC57-1          	225	743143	  CARTON D.CLOUK FFB 312/302-43 LAYER 3, Mitra Abadi                         	1	14	 3DC57-1          	27	1
1030	  CARTON D.CLOUK FFB 312/302-46 LAYER 1, Mitra Abadi                         	 3DC61-1          	158	248381	  CARTON D.CLOUK FFB 312/302-46 LAYER 1, Mitra Abadi                         	1	14	 3DC61-1          	27	1
1031	  CARTON D.CLOUK FFB 312/302-46 LAYER 2, Mitra Abadi                         	 3DC62-1          	123	114569	  CARTON D.CLOUK FFB 312/302-46 LAYER 2, Mitra Abadi                         	1	14	 3DC62-1          	27	1
1032	  CARTON DK KTFAG1-111-01, SINAR ERA BOX                                     	 3DK1-3           	1720	12521600	  CARTON DK KTFAG1-111-01, SINAR ERA BOX                                     	1	14	 3DK1-3           	27	1
1033	  CARTON DK KTFAG1-211-01, SINAR ERA BOX                                     	 3DK3-3           	403	4275016	  CARTON DK KTFAG1-211-01, SINAR ERA BOX                                     	1	14	 3DK3-3           	27	1
1034	  CARTON DK KTFAG4-111-01, SINAR ERA BOX                                     	 3DK5-3           	168	436558	  CARTON DK KTFAG4-111-01, SINAR ERA BOX                                     	1	14	 3DK5-3           	27	1
1035	  CARTON DK KTFAG4-112-01, SINAR ERA BOX                                     	 3DK6-3           	485	1462760	  CARTON DK KTFAG4-112-01, SINAR ERA BOX                                     	1	14	 3DK6-3           	27	1
1036	  CARTON DK KTFAG4-113-02, MITRA ABADI                                       	 3DK34-2          	1422	298619	  CARTON DK KTFAG4-113-02, MITRA ABADI                                       	1	14	 3DK34-2          	27	1
1037	  CARTON DK KTFAG4-211-01, SINAR ERA BOX                                     	 3DK7-3           	171	604554	  CARTON DK KTFAG4-211-01, SINAR ERA BOX                                     	1	14	 3DK7-3           	27	1
1038	  CARTON DK KTFAG4-212-01, SINAR ERA BOX                                     	 3DK8-3           	664	2589600	  CARTON DK KTFAG4-212-01, SINAR ERA BOX                                     	1	14	 3DK8-3           	27	1
1039	  CARTON DK KTFAG4-311-01, SINAR ERA BOX                                     	 3DK9-3           	159	660984	  CARTON DK KTFAG4-311-01, SINAR ERA BOX                                     	1	14	 3DK9-3           	27	1
1040	  CARTON DK KTFAG4-312-01, SINAR ERA BOX                                     	 3DK10-3          	424	1940223	  CARTON DK KTFAG4-312-01, SINAR ERA BOX                                     	1	14	 3DK10-3          	27	1
1041	  CARTON DK KTFAG5-111-01, SINAR ERA BOX                                     	 3DK11-3          	99	308196	  CARTON DK KTFAG5-111-01, SINAR ERA BOX                                     	1	14	 3DK11-3          	27	1
1042	  CARTON DK KTFAG5-112-01, SINAR ERA BOX                                     	 3DK12-3          	235	928720	  CARTON DK KTFAG5-112-01, SINAR ERA BOX                                     	1	14	 3DK12-3          	27	1
1043	  CARTON DK KTFAG5-211-01, SINAR ERA BOX                                     	 3DK14-3          	272	1103223	  CARTON DK KTFAG5-211-01, SINAR ERA BOX                                     	1	14	 3DK14-3          	27	1
1044	  CARTON DK KTFAG5-212-01, PT. SINAR ERA BOX                                 	 3DK15-3          	1372	6420963	  CARTON DK KTFAG5-212-01, PT. SINAR ERA BOX                                 	1	14	 3DK15-3          	27	1
1045	  CARTON DK KTFAG5-212-03, Mitra Abadi                                       	 3DK16-1          	2360	377598	  CARTON DK KTFAG5-212-03, Mitra Abadi                                       	1	14	 3DK16-1          	27	1
1046	  CARTON DK KTFAG5-221-01, SINAR ERA BOX                                     	 3DK17-3          	222	1269814	  CARTON DK KTFAG5-221-01, SINAR ERA BOX                                     	1	14	 3DK17-3          	27	1
1047	  CARTON DK KTFAG5-222-01, Mitra Abadi                                       	 3DK18-1          	272	1778561	  CARTON DK KTFAG5-222-01, Mitra Abadi                                       	1	14	 3DK18-1          	27	1
1048	  CARTON DK KTFAG5-222-03, Mitra Abadi                                       	 3DK19-1          	793	190319	  CARTON DK KTFAG5-222-03, Mitra Abadi                                       	1	14	 3DK19-1          	27	1
1049	  CARTON DK KTFAG5-311-01, SINAR ERA BOX                                     	 3DK20-3          	169	746533	  CARTON DK KTFAG5-311-01, SINAR ERA BOX                                     	1	14	 3DK20-3          	27	1
1050	  CARTON DK KTFAG5-312-01, SINAR ERA BOX                                     	 3DK21-3          	245	1273991	  CARTON DK KTFAG5-312-01, SINAR ERA BOX                                     	1	14	 3DK21-3          	27	1
1051	  CARTON DK KTFAG5-321-01, SINAR ERA BOX                                     	 3DK23-3          	220	1395542	  CARTON DK KTFAG5-321-01, SINAR ERA BOX                                     	1	14	 3DK23-3          	27	1
1052	  CARTON DK KTFAG5-322-01, SINAR ERA BOX                                     	 3DK24-3          	261	1967940	  CARTON DK KTFAG5-322-01, SINAR ERA BOX                                     	1	14	 3DK24-3          	27	1
1053	  CARTON MK F600SW, RSA                                                      	 3F229-4          	79	586600	  CARTON MK F600SW, RSA                                                      	1	14	 3F229-4          	27	1
1054	  CARTON MK F600SB, RSA                                                      	 3F229-3          	76	565423	  CARTON MK F600SB, RSA                                                      	1	14	 3F229-3          	27	1
1055	  CARTON MK F600SB-02, APG                                                   	 3F230-3          	71	85198	  CARTON MK F600SB-02, APG                                                   	1	14	 3F230-3          	27	1
1056	  CARTON MK F600SB-03, APG                                                   	 3F231-3          	75	22470	  CARTON MK F600SB-03, APG                                                   	1	14	 3F231-3          	27	1
1057	  CARTON MK F600SB-04, APG                                                   	 3F232-3          	70	69980	  CARTON MK F600SB-04, APG                                                   	1	14	 3F232-3          	27	1
1058	  CARTON MK F600SB-05, APG                                                   	 3F233-3          	45	94441	  CARTON MK F600SB-05, APG                                                   	1	14	 3F233-3          	27	1
1059	  CARTON MK F600SB-10, MITRA ABADI                                           	 3F234-3          	54	33481	  CARTON MK F600SB-10, MITRA ABADI                                           	1	14	 3F234-3          	27	1
1060	  CARTON MK F800SW, Prima Box                                                	 3F235-1          	79	788183	  CARTON MK F800SW, Prima Box                                                	1	14	 3F235-1          	27	1
1061	  CARTON MK F800SB, Prima Box                                                	 3F5-1            	314	3231627	  CARTON MK F800SB, Prima Box                                                	1	14	 3F5-1            	27	1
1062	  CARTON MK F800SB-02, ANUGERAH PRIMA GEMILANG                               	 3F6-3            	95	152000	  CARTON MK F800SB-02, ANUGERAH PRIMA GEMILANG                               	1	14	 3F6-3            	27	1
1063	  CARTON MK F800SB-03, ANUGERAH PRIMA GEMILANG                               	 3F7-4            	156	39011	  CARTON MK F800SB-03, ANUGERAH PRIMA GEMILANG                               	1	14	 3F7-4            	27	1
1064	  CARTON MK F800SB-04, ANUGERAH PRIMA GEMILANG                               	 3F8-3            	265	39750	  CARTON MK F800SB-04, ANUGERAH PRIMA GEMILANG                               	1	14	 3F8-3            	27	1
1065	  CARTON MK F800SB-06, ANUGERAH PRIMA GEMILANG                               	 3F10-3           	269	645600	  CARTON MK F800SB-06, ANUGERAH PRIMA GEMILANG                               	1	14	 3F10-3           	27	1
1066	  CARTON MK F800SB-12, MITRA ABADI                                           	 3F12-4           	364	163072	  CARTON MK F800SB-12, MITRA ABADI                                           	1	14	 3F12-4           	27	1
1067	  CARTON MK F1000SB, RSA                                                     	 3F13-3           	197	1740831	  CARTON MK F1000SB, RSA                                                     	1	14	 3F13-3           	27	1
1068	  CARTON MK F1000SW, RSA                                                     	 3F236-3          	67	584899	  CARTON MK F1000SW, RSA                                                     	1	14	 3F236-3          	27	1
1069	  CARTON MK F1000SB-02, ANUGERAH PRIMA GEMILANG                              	 3F14-4           	75	150000	  CARTON MK F1000SB-02, ANUGERAH PRIMA GEMILANG                              	1	14	 3F14-4           	27	1
1070	  CARTON MK F1000SB-03, ANUGERAH PRIMA GEMILANG                              	 3F15-4           	147	36752	  CARTON MK F1000SB-03, ANUGERAH PRIMA GEMILANG                              	1	14	 3F15-4           	27	1
1071	  CARTON MK F1000SB-04, ANUGERAH PRIMA GEMILANG                              	 3F16-5           	255	38250	  CARTON MK F1000SB-04, ANUGERAH PRIMA GEMILANG                              	1	14	 3F16-5           	27	1
1072	  CARTON MK F1000SB-05, ANUGERAH PRIMA GEMILANG                              	 3F17-4           	197	98503	  CARTON MK F1000SB-05, ANUGERAH PRIMA GEMILANG                              	1	14	 3F17-4           	27	1
1073	  CARTON MK F1000SB-07, ANUGERAH PRIMA GEMILANG                              	 3F19-4           	284	852000	  CARTON MK F1000SB-07, ANUGERAH PRIMA GEMILANG                              	1	14	 3F19-4           	27	1
1074	  CARTON MK F1000SB-13, MITRA ABADI                                          	 3F21-4           	387	184986	  CARTON MK F1000SB-13, MITRA ABADI                                          	1	14	 3F21-4           	27	1
1075	  CARTON CB F1000MW, Prima Box                                               	 3F34-1           	231	4593548	  CARTON CB F1000MW, Prima Box                                               	1	14	 3F34-1           	27	1
1076	  CARTON CB F1000MB, Prima Box                                               	 3F35-1           	419	8388826	  CARTON CB F1000MB, Prima Box                                               	1	14	 3F35-1           	27	1
1077	  CARTON MK F1000MB-02, ANUGERAH PRIMA GEMILANG                              	 3F36-6           	471	1224600	  CARTON MK F1000MB-02, ANUGERAH PRIMA GEMILANG                              	1	14	 3F36-6           	27	1
1078	  CARTON MK F1000MB-03, ANUGERAH PRIMA GEMILANG                              	 3F37-5           	281	70250	  CARTON MK F1000MB-03, ANUGERAH PRIMA GEMILANG                              	1	14	 3F37-5           	27	1
1079	  CARTON MK F1000MB-04, ANUGERAH PRIMA GEMILANG                              	 3F38-5           	260	221000	  CARTON MK F1000MB-04, ANUGERAH PRIMA GEMILANG                              	1	14	 3F38-5           	27	1
1080	  CARTON MK F1000MB-05, ANUGERAH PRIMA GEMILANG                              	 3F39-5           	321	160500	  CARTON MK F1000MB-05, ANUGERAH PRIMA GEMILANG                              	1	14	 3F39-5           	27	1
1081	  CARTON MK F1000MB-06, ANUGERAH PRIMA GEMILANG                              	 3F40-6           	676	811200	  CARTON MK F1000MB-06, ANUGERAH PRIMA GEMILANG                              	1	14	 3F40-6           	27	1
1082	  CARTON MK F1000MB-07, ANUGERAH PRIMA GEMILANG                              	 3F41-5           	261	730800	  CARTON MK F1000MB-07, ANUGERAH PRIMA GEMILANG                              	1	14	 3F41-5           	27	1
1083	  CARTON CB FA800B, RSA                                                      	 3F42-3           	77	907057	  CARTON CB FA800B, RSA                                                      	1	14	 3F42-3           	27	1
1084	  CARTON MK FA800B-02, RSA                                                   	 3F43-5           	43	86129	  CARTON MK FA800B-02, RSA                                                   	1	14	 3F43-5           	27	1
1085	  CARTON MK FA800B-03, RSA                                                   	 3F44-5           	62	102297	  CARTON MK FA800B-03, RSA                                                   	1	14	 3F44-5           	27	1
1086	  CARTON MK FA800B-04, RSA                                                   	 3F45-5           	54	79537	  CARTON MK FA800B-04, RSA                                                   	1	14	 3F45-5           	27	1
1087	  CARTON MK FA800B-05, RSA                                                   	 3F46-4           	55	27170	  CARTON MK FA800B-05, RSA                                                   	1	14	 3F46-4           	27	1
1088	  CARTON MK FA800B-13, RSA                                                   	 3F47-5           	51	54109	  CARTON MK FA800B-13, RSA                                                   	1	14	 3F47-5           	27	1
1089	  CARTON MK FA800B-14, RSA                                                   	 3F48-4           	56	40765	  CARTON MK FA800B-14, RSA                                                   	1	14	 3F48-4           	27	1
1090	  CARTON MK FA800B-19, RSA                                                   	 3F49-4           	59	15339	  CARTON MK FA800B-19, RSA                                                   	1	14	 3F49-4           	27	1
1091	  CARTON MK FA800B-20, RSA                                                   	 3F50-4           	53	78065	  CARTON MK FA800B-20, RSA                                                   	1	14	 3F50-4           	27	1
1092	  CARTON PK FL1300 B1-01, RSA                                                	 3F273-3          	81	1635322	  CARTON PK FL1300 B1-01, RSA                                                	1	14	 3F273-3          	27	1
1093	  CARTON PK FL1300 B1-02, RSA                                                	 3F274-4          	50	29917	  CARTON PK FL1300 B1-02, RSA                                                	1	14	 3F274-4          	27	1
1094	  CARTON PK FL1300 B1-03, RSA                                                	 3F275-4          	30	53785	  CARTON PK FL1300 B1-03, RSA                                                	1	14	 3F275-4          	27	1
1095	  CARTON PK FL1300 B1-04, RSA                                                	 3F276-4          	30	74757	  CARTON PK FL1300 B1-04, RSA                                                	1	14	 3F276-4          	27	1
1096	  CARTON PK FL1300 B2-01, RSA                                                	 3F277-3          	21	215914	  CARTON PK FL1300 B2-01, RSA                                                	1	14	 3F277-3          	27	1
1097	  CARTON PK FL1300 B2-02, RSA                                                	 3F278-4          	40	111474	  CARTON PK FL1300 B2-02, RSA                                                	1	14	 3F278-4          	27	1
1098	  CARTON PK FL1300 B2-03, RSA                                                	 3F279-4          	54	75121	  CARTON PK FL1300 B2-03, RSA                                                	1	14	 3F279-4          	27	1
1099	  CARTON PK FL1300 B2-05, RSA                                                	 3F281-4          	50	89893	  CARTON PK FL1300 B2-05, RSA                                                	1	14	 3F281-4          	27	1
1100	  CARTON PK FL1300 B2-06, RSA                                                	 3F282-4          	30	55829	  CARTON PK FL1300 B2-06, RSA                                                	1	14	 3F282-4          	27	1
1101	  CARTON PK FL1600 B1-01, RSA                                                	 3F283-2          	73	1486578	  CARTON PK FL1600 B1-01, RSA                                                	1	14	 3F283-2          	27	1
1102	  CARTON PK FL1600 B1-02, CMB                                                	 3F284-2          	39	22754	  CARTON PK FL1600 B1-02, CMB                                                	1	14	 3F284-2          	27	1
1103	  CARTON PK FL1600 B1-02, APG                                                	 3F284-3          	90	52509	  CARTON PK FL1600 B1-02, APG                                                	1	14	 3F284-3          	27	1
1104	  CARTON PK FL1600 B1-03, CMB                                                	 3F285-2          	60	139654	  CARTON PK FL1600 B1-03, CMB                                                	1	14	 3F285-2          	27	1
1105	  CARTON PK FL1600 B1-03, APG                                                	 3F285-3          	0	0	  CARTON PK FL1600 B1-03, APG                                                	1	14	 3F285-3          	27	1
1106	  CARTON PK FL1600 B1-04, CMB                                                	 3F286-2          	39	111747	  CARTON PK FL1600 B1-04, CMB                                                	1	14	 3F286-2          	27	1
1107	  CARTON PK FL1600 B1-04, APG                                                	 3F286-3          	0	0	  CARTON PK FL1600 B1-04, APG                                                	1	14	 3F286-3          	27	1
1108	  CARTON PK FL1600 B2-01, RSA                                                	 3F287-2          	84	1093326	  CARTON PK FL1600 B2-01, RSA                                                	1	14	 3F287-2          	27	1
1109	  CARTON PK FL1600 B2-02, CMB                                                	 3F288-2          	40	163326	  CARTON PK FL1600 B2-02, CMB                                                	1	14	 3F288-2          	27	1
1110	  CARTON PK FL1600 B2-02, APG                                                	 3F288-3          	0	0	  CARTON PK FL1600 B2-02, APG                                                	1	14	 3F288-3          	27	1
1111	  CARTON PK FL1600 B2-03, CMB                                                	 3F289-2          	40	62028	  CARTON PK FL1600 B2-03, CMB                                                	1	14	 3F289-2          	27	1
1112	  CARTON PK FL1600 B2-03, APG                                                	 3F289-3          	22	34116	  CARTON PK FL1600 B2-03, APG                                                	1	14	 3F289-3          	27	1
1113	  CARTON PK FL1600 B2-04, CMB                                                	 3F290-2          	40	46747	  CARTON PK FL1600 B2-04, CMB                                                	1	14	 3F290-2          	27	1
1114	  CARTON PK FL1600 B2-04, APG                                                	 3F290-3          	0	0	  CARTON PK FL1600 B2-04, APG                                                	1	14	 3F290-3          	27	1
1115	  CARTON PK FL1600 B2-05, CMB                                                	 3F291-2          	40	35964	  CARTON PK FL1600 B2-05, CMB                                                	1	14	 3F291-2          	27	1
1116	  CARTON PK FL1600 B2-05, APG                                                	 3F291-3          	0	0	  CARTON PK FL1600 B2-05, APG                                                	1	14	 3F291-3          	27	1
1117	  CARTON PK FL1600 B2-06, CMB                                                	 3F292-2          	43	133462	  CARTON PK FL1600 B2-06, CMB                                                	1	14	 3F292-2          	27	1
1118	  CARTON PK FL1600 B2-06, APG                                                	 3F292-3          	0	0	  CARTON PK FL1600 B2-06, APG                                                	1	14	 3F292-3          	27	1
1119	  CARTON PK FL1600 B2-07, CMB                                                	 3F293-2          	40	4001	  CARTON PK FL1600 B2-07, CMB                                                	1	14	 3F293-2          	27	1
1120	  CARTON PK FL1600 B2-07, APG                                                	 3F293-3          	0	0	  CARTON PK FL1600 B2-07, APG                                                	1	14	 3F293-3          	27	1
1121	  CARTON PK FR1200P-01, PT. PRIMABOX                                         	 3F342-3          	540	9677430	  CARTON PK FR1200P-01, PT. PRIMABOX                                         	1	14	 3F342-3          	27	1
1122	  CARTON PK FR1200AG-P-01, PT. PRIMABOX                                      	 3F342-4          	187	3351258	  CARTON PK FR1200AG-P-01, PT. PRIMABOX                                      	1	14	 3F342-4          	27	1
1123	  CARTON PK FR1200P-02, ANUGERAH P. G.                                       	 3F343-2          	86	232200	  CARTON PK FR1200P-02, ANUGERAH P. G.                                       	1	14	 3F343-2          	27	1
1124	  CARTON PK FR1200P-03, ANUGERAH P. G.                                       	 3F344-2          	384	211200	  CARTON PK FR1200P-03, ANUGERAH P. G.                                       	1	14	 3F344-2          	27	1
1125	  CARTON PK FR1200P-04, ANUGERAH P. G.                                       	 3F345-2          	357	1749299	  CARTON PK FR1200P-04, ANUGERAH P. G.                                       	1	14	 3F345-2          	27	1
1126	  CARTON PK FR1200P-05, ANUGERAH P. G.                                       	 3F346-2          	824	700400	  CARTON PK FR1200P-05, ANUGERAH P. G.                                       	1	14	 3F346-2          	27	1
1127	  CARTON PK FR1200P-06, ANUGERAH P. G.                                       	 3F347-2          	370	684500	  CARTON PK FR1200P-06, ANUGERAH P. G.                                       	1	14	 3F347-2          	27	1
1128	  CARTON PK FR1200P-07, ANUGERAH P. G.                                       	 3F348-2          	375	412499	  CARTON PK FR1200P-07, ANUGERAH P. G.                                       	1	14	 3F348-2          	27	1
1129	  CARTON PK FR1200P-08, ANUGERAH P. G.                                       	 3F349-2          	369	424349	  CARTON PK FR1200P-08, ANUGERAH P. G.                                       	1	14	 3F349-2          	27	1
1130	  CARTON PK FR1200P-09, ANUGERAH P. G.                                       	 3F350-2          	427	234849	  CARTON PK FR1200P-09, ANUGERAH P. G.                                       	1	14	 3F350-2          	27	1
1131	  CARTON PK FR1400P-01, PT. PRIMABOX                                         	 3F351-3          	200	3389337	  CARTON PK FR1400P-01, PT. PRIMABOX                                         	1	14	 3F351-3          	27	1
1132	  CARTON PK FR1400AG-P-01, PT. PRIMABOX                                      	 3F351-4          	153	2592842	  CARTON PK FR1400AG-P-01, PT. PRIMABOX                                      	1	14	 3F351-4          	27	1
1133	  CARTON PK FR1400P-02, APG                                                  	 3F352-2          	58	156584	  CARTON PK FR1400P-02, APG                                                  	1	14	 3F352-2          	27	1
1134	  CARTON PK FR1400P-03, APG                                                  	 3F353-2          	174	113016	  CARTON PK FR1400P-03, APG                                                  	1	14	 3F353-2          	27	1
1135	  CARTON PK FR1400P-04, APG                                                  	 3F354-2          	166	746823	  CARTON PK FR1400P-04, APG                                                  	1	14	 3F354-2          	27	1
1136	  CARTON PK FR1400P-05, APG                                                  	 3F355-2          	296	251600	  CARTON PK FR1400P-05, APG                                                  	1	14	 3F355-2          	27	1
1137	  CARTON PK FR1400P-06, APG                                                  	 3F356-2          	143	264474	  CARTON PK FR1400P-06, APG                                                  	1	14	 3F356-2          	27	1
1138	  CARTON PK FR1400P-07, APG                                                  	 3F357-2          	171	239279	  CARTON PK FR1400P-07, APG                                                  	1	14	 3F357-2          	27	1
1139	  CARTON PK FR1400P-08, APG                                                  	 3F358-2          	166	190801	  CARTON PK FR1400P-08, APG                                                  	1	14	 3F358-2          	27	1
1140	  CARTON PK FR1400P-09, APG                                                  	 3F359-2          	187	149532	  CARTON PK FR1400P-09, APG                                                  	1	14	 3F359-2          	27	1
1141	  CARTON PK FR1400P-10, APG                                                  	 3F360-2          	199	119331	  CARTON PK FR1400P-10, APG                                                  	1	14	 3F360-2          	27	1
1142	  CARTON PK FR1600P-01, RSA                                                  	 3F361-1          	193	3668847	  CARTON PK FR1600P-01, RSA                                                  	1	14	 3F361-1          	27	1
1143	  CARTON PK FR1600AG-P-01, RSA                                               	 3F361-2          	69	1311661	  CARTON PK FR1600AG-P-01, RSA                                               	1	14	 3F361-2          	27	1
1144	  CARTON PK FR1600P-02, APG                                                  	 3F362-2          	88	272652	  CARTON PK FR1600P-02, APG                                                  	1	14	 3F362-2          	27	1
1145	  CARTON PK FR1600P-03, APG                                                  	 3F363-2          	132	69051	  CARTON PK FR1600P-03, APG                                                  	1	14	 3F363-2          	27	1
1146	  CARTON PK FR1600P-04, APG                                                  	 3F364-3          	187	914951	  CARTON PK FR1600P-04, APG                                                  	1	14	 3F364-3          	27	1
1147	  CARTON PK FR1600P-05, APG                                                  	 3F365-2          	260	234120	  CARTON PK FR1600P-05, APG                                                  	1	14	 3F365-2          	27	1
1148	  CARTON PK FR1600P-06, APG                                                  	 3F366-2          	180	235172	  CARTON PK FR1600P-06, APG                                                  	1	14	 3F366-2          	27	1
1149	  CARTON PK FR1600P-07, APG                                                  	 3F367-2          	246	308163	  CARTON PK FR1600P-07, APG                                                  	1	14	 3F367-2          	27	1
1150	  CARTON PK FR1600P-08, APG                                                  	 3F368-2          	193	174701	  CARTON PK FR1600P-08, APG                                                  	1	14	 3F368-2          	27	1
1151	  CARTON PK FR1600P-09, APG                                                  	 3F369-2          	188	114774	  CARTON PK FR1600P-09, APG                                                  	1	14	 3F369-2          	27	1
1152	  CARTON PK FR1600P-10, APG                                                  	 3F370-2          	207	115335	  CARTON PK FR1600P-10, APG                                                  	1	14	 3F370-2          	27	1
1153	  CARTON CB FSV1000B, RSA                                                    	 3F128-3          	97	1283955	  CARTON CB FSV1000B, RSA                                                    	1	14	 3F128-3          	27	1
1154	  CARTON MK FSV1000B-02, ANUGERAH P. G.                                      	 3F129-3          	77	142448	  CARTON MK FSV1000B-02, ANUGERAH P. G.                                      	1	14	 3F129-3          	27	1
1155	  CARTON MK FSV1000B-03, ANUGERAH P. G.                                      	 3F130-3          	76	53152	  CARTON MK FSV1000B-03, ANUGERAH P. G.                                      	1	14	 3F130-3          	27	1
1156	  CARTON MK FSV1000B-04, ANUGERAH P. G.                                      	 3F131-3          	55	140205	  CARTON MK FSV1000B-04, ANUGERAH P. G.                                      	1	14	 3F131-3          	27	1
1157	  CARTON MK FSV1000B-05, ANUGERAH P. G.                                      	 3F132-3          	82	311482	  CARTON MK FSV1000B-05, ANUGERAH P. G.                                      	1	14	 3F132-3          	27	1
1158	  CARTON CB FSV1000B-HK, RSA                                                 	 3F133-3          	93	923288	  CARTON CB FSV1000B-HK, RSA                                                 	1	14	 3F133-3          	27	1
1159	  CARTON MK FSV1000B HK-02, ANUGERAH P. G.                                   	 3F134-3          	51	107106	  CARTON MK FSV1000B HK-02, ANUGERAH P. G.                                   	1	14	 3F134-3          	27	1
1160	  CARTON MK FSV1000B HK-03, ANUGERAH P. G.                                   	 3F135-3          	72	108000	  CARTON MK FSV1000B HK-03, ANUGERAH P. G.                                   	1	14	 3F135-3          	27	1
1161	  CARTON MK FSV1000B HK-04, ANUGERAH P. G.                                   	 3F136-3          	76	212786	  CARTON MK FSV1000B HK-04, ANUGERAH P. G.                                   	1	14	 3F136-3          	27	1
1162	  CARTON MK FSV1000B HK-05, ANUGERAH P. G.                                   	 3F137-3          	74	81267	  CARTON MK FSV1000B HK-05, ANUGERAH P. G.                                   	1	14	 3F137-3          	27	1
1163	  CARTON PK FG1150 -01, RSA                                                  	 3F304-2          	84	1340834	  CARTON PK FG1150 -01, RSA                                                  	1	14	 3F304-2          	27	1
1164	  CARTON PK FG1150 -02, RSA                                                  	 3F305-3          	50	107884	  CARTON PK FG1150 -02, RSA                                                  	1	14	 3F305-3          	27	1
1165	  CARTON PK FG1150 -03, RSA                                                  	 3F306-3          	50	20538	  CARTON PK FG1150 -03, RSA                                                  	1	14	 3F306-3          	27	1
1166	  CARTON PK FG1150 -04, RSA                                                  	 3F307-3          	40	138668	  CARTON PK FG1150 -04, RSA                                                  	1	14	 3F307-3          	27	1
1167	  CARTON PK FG1150 -05, RSA                                                  	 3F308-3          	42	69020	  CARTON PK FG1150 -05, RSA                                                  	1	14	 3F308-3          	27	1
1168	  CARTON PK FG1150 -06, RSA                                                  	 3F309-3          	40	22638	  CARTON PK FG1150 -06, RSA                                                  	1	14	 3F309-3          	27	1
1169	  CARTON PK FG1150 -07, RSA                                                  	 3F310-3          	40	8239	  CARTON PK FG1150 -07, RSA                                                  	1	14	 3F310-3          	27	1
1170	  CARTON PK FG1150 -08, RSA                                                  	 3F311-3          	40	36987	  CARTON PK FG1150 -08, RSA                                                  	1	14	 3F311-3          	27	1
1171	  CARTON PK FG1150 -09, RSA                                                  	 3F312-3          	40	16438	  CARTON PK FG1150 -09, RSA                                                  	1	14	 3F312-3          	27	1
1172	  CARTON PK FG1300 -01, RSA                                                  	 3F313-2          	66	945523	  CARTON PK FG1300 -01, RSA                                                  	1	14	 3F313-2          	27	1
1173	  CARTON PK FG1300 -02, RSA                                                  	 3F314-3          	40	92717	  CARTON PK FG1300 -02, RSA                                                  	1	14	 3F314-3          	27	1
1174	  CARTON PK FG1300 -03, RSA                                                  	 3F315-3          	48	21153	  CARTON PK FG1300 -03, RSA                                                  	1	14	 3F315-3          	27	1
1175	  CARTON PK FG1300 -04, RSA                                                  	 3F316-3          	47	181430	  CARTON PK FG1300 -04, RSA                                                  	1	14	 3F316-3          	27	1
1176	  CARTON PK FG1300 -05, RSA                                                  	 3F317-3          	43	15170	  CARTON PK FG1300 -05, RSA                                                  	1	14	 3F317-3          	27	1
1177	  CARTON PK FG1300 -06, RSA                                                  	 3F318-3          	48	10553	  CARTON PK FG1300 -06, RSA                                                  	1	14	 3F318-3          	27	1
1178	  CARTON PK FG1300 -07, RSA                                                  	 3F319-3          	48	11899	  CARTON PK FG1300 -07, RSA                                                  	1	14	 3F319-3          	27	1
1179	  CARTON PK FG1300 -08, RSA                                                  	 3F320-3          	48	47628	  CARTON PK FG1300 -08, RSA                                                  	1	14	 3F320-3          	27	1
1180	  CARTON PK FG1300 -09, RSA                                                  	 3F321-3          	46	15223	  CARTON PK FG1300 -09, RSA                                                  	1	14	 3F321-3          	27	1
1181	  CARTON PK FG1300 -10, RSA                                                  	 3F322-3          	46	3633	  CARTON PK FG1300 -10, RSA                                                  	1	14	 3F322-3          	27	1
1182	  CARTON PK FG1500 -01, RSA                                                  	 3F323-2          	77	1273400	  CARTON PK FG1500 -01, RSA                                                  	1	14	 3F323-2          	27	1
1183	  CARTON PK FG1500 -02, RSA                                                  	 3F324-3          	38	96438	  CARTON PK FG1500 -02, RSA                                                  	1	14	 3F324-3          	27	1
1184	  CARTON PK FG1500 -03, RSA                                                  	 3F325-3          	69	30418	  CARTON PK FG1500 -03, RSA                                                  	1	14	 3F325-3          	27	1
1185	  CARTON PK FG1500 -04, RSA                                                  	 3F326-3          	58	268745	  CARTON PK FG1500 -04, RSA                                                  	1	14	 3F326-3          	27	1
1186	  CARTON PK FG1500 -05, RSA                                                  	 3F327-3          	66	29096	  CARTON PK FG1500 -05, RSA                                                  	1	14	 3F327-3          	27	1
1187	  CARTON PK FG1500 -06, RSA                                                  	 3F328-3          	69	7588	  CARTON PK FG1500 -06, RSA                                                  	1	14	 3F328-3          	27	1
1188	  CARTON PK FG1500 -07, RSA                                                  	 3F329-3          	48	29184	  CARTON PK FG1500 -07, RSA                                                  	1	14	 3F329-3          	27	1
1189	  CARTON PK FG1500 -08, RSA                                                  	 3F330-3          	59	58573	  CARTON PK FG1500 -08, RSA                                                  	1	14	 3F330-3          	27	1
1190	  CARTON PK FG1500 -09, RSA                                                  	 3F331-3          	58	19197	  CARTON PK FG1500 -09, RSA                                                  	1	14	 3F331-3          	27	1
1191	  CARTON PK FG1500 -10, RSA                                                  	 3F332-3          	69	19037	  CARTON PK FG1500 -10, RSA                                                  	1	14	 3F332-3          	27	1
1192	  CARTON PK FG1700 -01, RSA                                                  	 3F333-2          	36	700870	  CARTON PK FG1700 -01, RSA                                                  	1	14	 3F333-2          	27	1
1193	  CARTON PK FG1700 -02, RSA                                                  	 3F334-2          	45	109652	  CARTON PK FG1700 -02, RSA                                                  	1	14	 3F334-2          	27	1
1194	  CARTON PK FG1700 -03, RSA                                                  	 3F335-2          	40	26744	  CARTON PK FG1700 -03, RSA                                                  	1	14	 3F335-2          	27	1
1195	  CARTON PK FG1700 -04, RSA                                                  	 3F336-2          	30	157423	  CARTON PK FG1700 -04, RSA                                                  	1	14	 3F336-2          	27	1
1196	  CARTON PK FG1700 -05, RSA                                                  	 3F337-2          	20	17731	  CARTON PK FG1700 -05, RSA                                                  	1	14	 3F337-2          	27	1
1197	  CARTON PK FG1700 -06, RSA                                                  	 3F338-2          	30	6641	  CARTON PK FG1700 -06, RSA                                                  	1	14	 3F338-2          	27	1
1198	  CARTON PK FG1700 -07, RSA                                                  	 3F339-2          	50	38810	  CARTON PK FG1700 -07, RSA                                                  	1	14	 3F339-2          	27	1
1199	  CARTON PK FG1700 -08, RSA                                                  	 3F340-2          	31	39461	  CARTON PK FG1700 -08, RSA                                                  	1	14	 3F340-2          	27	1
1200	  CARTON PK FG1700 -09, RSA                                                  	 3F341-2          	45	21211	  CARTON PK FG1700 -09, RSA                                                  	1	14	 3F341-2          	27	1
1201	  CARTON CB FM1270B1, Prima Box                                              	 3F159-1          	213	3588198	  CARTON CB FM1270B1, Prima Box                                              	1	14	 3F159-1          	27	1
1202	  CARTON MK FM1270B1-02, APG                                                 	 3F160-4          	54	150900	  CARTON MK FM1270B1-02, APG                                                 	1	14	 3F160-4          	27	1
1203	  CARTON MK FM1270B1-03, APG                                                 	 3F161-4          	68	53371	  CARTON MK FM1270B1-03, APG                                                 	1	14	 3F161-4          	27	1
1204	  CARTON MK FM1270B1-04, APG                                                 	 3F162-4          	69	175612	  CARTON MK FM1270B1-04, APG                                                 	1	14	 3F162-4          	27	1
1205	  CARTON MK FM1270B1-18, APG                                                 	 3F163-4          	34	8029	  CARTON MK FM1270B1-18, APG                                                 	1	14	 3F163-4          	27	1
1206	  CARTON MK FM1270B1-21, APG                                                 	 3F164-4          	31	7783	  CARTON MK FM1270B1-21, APG                                                 	1	14	 3F164-4          	27	1
1207	  CARTON CB FM1270B2 W, Prima Box                                            	 3F165-1          	8	0	  CARTON CB FM1270B2 W, Prima Box                                            	1	14	 3F165-1          	27	1
1208	  CARTON CB FM1270B2, Prima Box                                              	 3F166-1          	246	2261850	  CARTON CB FM1270B2, Prima Box                                              	1	14	 3F166-1          	27	1
1209	  CARTON MK FM1270B2-02, APG                                                 	 3F167-4          	35	132822	  CARTON MK FM1270B2-02, APG                                                 	1	14	 3F167-4          	27	1
1210	  CARTON MK FM1270B2-03, APG                                                 	 3F168-4          	63	103936	  CARTON MK FM1270B2-03, APG                                                 	1	14	 3F168-4          	27	1
1211	  CARTON MK FM1270B2-04, APG                                                 	 3F169-4          	104	125975	  CARTON MK FM1270B2-04, APG                                                 	1	14	 3F169-4          	27	1
1212	  CARTON MK FM1270B2-05, APG                                                 	 3F170-4          	79	72432	  CARTON MK FM1270B2-05, APG                                                 	1	14	 3F170-4          	27	1
1213	  CARTON MK FM1270B2-14, MITRA ABADI                                         	 3F171-4          	127	19042	  CARTON MK FM1270B2-14, MITRA ABADI                                         	1	14	 3F171-4          	27	1
1214	  CARTON CB FM1470B1 W, Prima Box                                            	 3F172-1          	15	279384	  CARTON CB FM1470B1 W, Prima Box                                            	1	14	 3F172-1          	27	1
1215	  CARTON CB FM1470B1, RSA                                                    	 3F173-3          	40	745024	  CARTON CB FM1470B1, RSA                                                    	1	14	 3F173-3          	27	1
1216	  CARTON MK FM1470B1-02, APG                                                 	 3F174-4          	56	149955	  CARTON MK FM1470B1-02, APG                                                 	1	14	 3F174-4          	27	1
1217	  CARTON MK FM1470B1-03, APG                                                 	 3F175-4          	52	45740	  CARTON MK FM1470B1-03, APG                                                 	1	14	 3F175-4          	27	1
1218	  CARTON CB FM1470B2 W, Prima Box                                            	 3F176-1          	30	0	  CARTON CB FM1470B2 W, Prima Box                                            	1	14	 3F176-1          	27	1
1219	  CARTON CB FM1470B2, RSA                                                    	 3F177-3          	35	396489	  CARTON CB FM1470B2, RSA                                                    	1	14	 3F177-3          	27	1
1220	  CARTON CB FM1470B2-02, APG                                                 	 3F178-4          	44	162002	  CARTON CB FM1470B2-02, APG                                                 	1	14	 3F178-4          	27	1
1221	  CARTON MK FM1470B2-03, APG                                                 	 3F179-4          	50	74294	  CARTON MK FM1470B2-03, APG                                                 	1	14	 3F179-4          	27	1
1222	  CARTON MK FM1470B2-04, APG                                                 	 3F180-4          	48	84318	  CARTON MK FM1470B2-04, APG                                                 	1	14	 3F180-4          	27	1
1223	  CARTON MK FM1470B2-05, APG                                                 	 3F181-4          	70	62207	  CARTON MK FM1470B2-05, APG                                                 	1	14	 3F181-4          	27	1
1224	  CARTON MK FM1470B2-06, APG                                                 	 3F182-4          	42	116339	  CARTON MK FM1470B2-06, APG                                                 	1	14	 3F182-4          	27	1
1225	  CARTON MK FM1470B2-017, APG                                                	 3F183-4          	60	16168	  CARTON MK FM1470B2-017, APG                                                	1	14	 3F183-4          	27	1
1226	  CARTON MK FM1470B2-20, APG                                                 	 3F184-4          	60	14748	  CARTON MK FM1470B2-20, APG                                                 	1	14	 3F184-4          	27	1
1227	  CARTON CB FM1600B1, RSA                                                    	 3F186-3          	62	1202599	  CARTON CB FM1600B1, RSA                                                    	1	14	 3F186-3          	27	1
1228	  CARTON MK FM1600B1-02, RSA                                                 	 3F187-4          	40	100854	  CARTON MK FM1600B1-02, RSA                                                 	1	14	 3F187-4          	27	1
1229	  CARTON MK FM1600B1-03, RSA                                                 	 3F188-4          	70	54028	  CARTON MK FM1600B1-03, RSA                                                 	1	14	 3F188-4          	27	1
1230	  CARTON MK FM1600B1-04, RSA                                                 	 3F189-4          	68	48188	  CARTON MK FM1600B1-04, RSA                                                 	1	14	 3F189-4          	27	1
1231	  CARTON CB FM1600B2, RSA                                                    	 3F191-3          	80	840966	  CARTON CB FM1600B2, RSA                                                    	1	14	 3F191-3          	27	1
1232	  CARTON MK FM1600B2-02, RSA                                                 	 3F192-4          	40	185682	  CARTON MK FM1600B2-02, RSA                                                 	1	14	 3F192-4          	27	1
1233	  CARTON MK FM1600B2-03, RSA                                                 	 3F193-4          	40	72634	  CARTON MK FM1600B2-03, RSA                                                 	1	14	 3F193-4          	27	1
1234	  CARTON MK FM1600B2-04, RSA                                                 	 3F194-4          	50	111908	  CARTON MK FM1600B2-04, RSA                                                 	1	14	 3F194-4          	27	1
1235	  CARTON MK FM1600B2-05, RSA                                                 	 3F195-4          	50	48155	  CARTON MK FM1600B2-05, RSA                                                 	1	14	 3F195-4          	27	1
1236	  CARTON MK FM1600B2-06, RSA                                                 	 3F196-4          	50	197462	  CARTON MK FM1600B2-06, RSA                                                 	1	14	 3F196-4          	27	1
1237	  CARTON MK FM1600B2-17, RSA                                                 	 3F197-4          	90	23021	  CARTON MK FM1600B2-17, RSA                                                 	1	14	 3F197-4          	27	1
1238	  CARTON MK FM1600B2-21, RSA                                                 	 3F198-4          	80	18391	  CARTON MK FM1600B2-21, RSA                                                 	1	14	 3F198-4          	27	1
1239	  CARTON BOX PK-YFCS290B-P1-01, PRIMABOX                                     	 3F433-1          	16	171200	  CARTON BOX PK-YFCS290B-P1-01, PRIMABOX                                     	1	14	 3F433-1          	27	1
1240	  CARTON LAYER PK-YFCS290B-P1-02, APG                                        	 3F434-1          	3230	3068500	  CARTON LAYER PK-YFCS290B-P1-02, APG                                        	1	14	 3F434-1          	27	1
1241	  CARTON LAYER PK-YFCS290B-P1-02T, APG                                       	 3F435-1          	135	30375	  CARTON LAYER PK-YFCS290B-P1-02T, APG                                       	1	14	 3F435-1          	27	1
1242	  CARTON LAYER PK-YFCS290B-P1-03, APG                                        	 3F436-1          	3095	3559250	  CARTON LAYER PK-YFCS290B-P1-03, APG                                        	1	14	 3F436-1          	27	1
1243	  CARTON BOX PK-YFO1180-P-01, RSA                                            	 3F371-1          	350	5325250	  CARTON BOX PK-YFO1180-P-01, RSA                                            	1	14	 3F371-1          	27	1
1244	  CARTON LY PK-YFO1180-P-02, APG                                             	 3F372-2          	625	1036875	  CARTON LY PK-YFO1180-P-02, APG                                             	1	14	 3F372-2          	27	1
1245	  CARTON LY PK-YFO1180-P-03, APG                                             	 3F373-2          	326	149308	  CARTON LY PK-YFO1180-P-03, APG                                             	1	14	 3F373-2          	27	1
1246	  CARTON LY PK-YFO1180-P-04, APG                                             	 3F374-2          	349	119707	  CARTON LY PK-YFO1180-P-04, APG                                             	1	14	 3F374-2          	27	1
1247	  CARTON LY PK-YFO1180-P-05, APG                                             	 3F375-2          	333	251415	  CARTON LY PK-YFO1180-P-05, APG                                             	1	14	 3F375-2          	27	1
1248	  CARTON LY PK-YFO1180-P-06, APG                                             	 3F376-2          	344	440664	  CARTON LY PK-YFO1180-P-06, APG                                             	1	14	 3F376-2          	27	1
1249	  CARTON BOX PK-YFW1600G-P1-01, PRIMABOX                                     	 3F398-1          	57	1368000	  CARTON BOX PK-YFW1600G-P1-01, PRIMABOX                                     	1	14	 3F398-1          	27	1
1250	  CARTON LAYER PK-YFW1600G-P1-02, APG                                        	 3F399-1          	193	723750	  CARTON LAYER PK-YFW1600G-P1-02, APG                                        	1	14	 3F399-1          	27	1
1251	  CARTON LAYER PK-YFW1600G-P1-03, APG                                        	 3F400-1          	122	61000	  CARTON LAYER PK-YFW1600G-P1-03, APG                                        	1	14	 3F400-1          	27	1
1252	  CARTON LAYER PK-YFW1600G-P1-04, APG                                        	 3F401-1          	125	637500	  CARTON LAYER PK-YFW1600G-P1-04, APG                                        	1	14	 3F401-1          	27	1
1253	  CARTON BOX PK-YFW1600G-P2-01, PRIMABOX                                     	 3F402-1          	52	1508000	  CARTON BOX PK-YFW1600G-P2-01, PRIMABOX                                     	1	14	 3F402-1          	27	1
1254	  CARTON LAYER PK-YFW1600G-P2-02, APG                                        	 3F403-1          	63	289800	  CARTON LAYER PK-YFW1600G-P2-02, APG                                        	1	14	 3F403-1          	27	1
1255	  CARTON LAYER PK-YFW1600G-P2-03, APG                                        	 3F404-1          	126	296100	  CARTON LAYER PK-YFW1600G-P2-03, APG                                        	1	14	 3F404-1          	27	1
1256	  CARTON LAYER PK-YFW1600G-P2-04, APG                                        	 3F405-1          	126	214200	  CARTON LAYER PK-YFW1600G-P2-04, APG                                        	1	14	 3F405-1          	27	1
1257	  CARTON LAYER PK-YFW1600G-P2-05, APG                                        	 3F406-1          	182	54600	  CARTON LAYER PK-YFW1600G-P2-05, APG                                        	1	14	 3F406-1          	27	1
1258	  CARTON LAYER PK-YFW1600G-P2-06, APG                                        	 3F407-1          	138	924600	  CARTON LAYER PK-YFW1600G-P2-06, APG                                        	1	14	 3F407-1          	27	1
1259	  CARTON LAYER PK-YFW1600G-P2-07, APG                                        	 3F408-1          	96	105600	  CARTON LAYER PK-YFW1600G-P2-07, APG                                        	1	14	 3F408-1          	27	1
1260	  CARTON BOX PK-YFX1600HG-P1-01, PRIMABOX                                    	 3F409-1          	57	1311000	  CARTON BOX PK-YFX1600HG-P1-01, PRIMABOX                                    	1	14	 3F409-1          	27	1
1261	  CARTON LAYER PK-YFX1600HG-P1-02, APG                                       	 3F410-1          	60	180000	  CARTON LAYER PK-YFX1600HG-P1-02, APG                                       	1	14	 3F410-1          	27	1
1262	  CARTON LAYER PK-YFX1600HG-P1-03, APG                                       	 3F411-1          	150	75000	  CARTON LAYER PK-YFX1600HG-P1-03, APG                                       	1	14	 3F411-1          	27	1
1263	  CARTON LAYER PK-YFX1600HG-P1-04, APG                                       	 3F412-1          	144	237600	  CARTON LAYER PK-YFX1600HG-P1-04, APG                                       	1	14	 3F412-1          	27	1
1264	  CARTON CARTON PK-YFX1600HG-P2-01, PRIMABOX                                 	 3F413-1          	59	1191800	  CARTON CARTON PK-YFX1600HG-P2-01, PRIMABOX                                 	1	14	 3F413-1          	27	1
1265	  CARTON LAYER PK-YFX1600HG-P2-02, APG                                       	 3F414-1          	70	91000	  CARTON LAYER PK-YFX1600HG-P2-02, APG                                       	1	14	 3F414-1          	27	1
1266	  CARTON BOX PK-YFY1600B-P1-01, PRIMABOX                                     	 3F415-1          	59	1410100	  CARTON BOX PK-YFY1600B-P1-01, PRIMABOX                                     	1	14	 3F415-1          	27	1
1267	  CARTON LAYER PK-YFY1600B-P1-02, APG                                        	 3F416-1          	65	243750	  CARTON LAYER PK-YFY1600B-P1-02, APG                                        	1	14	 3F416-1          	27	1
1268	  CARTON LAYER PK-YFY1600B-P1-03, APG                                        	 3F417-1          	142	71000	  CARTON LAYER PK-YFY1600B-P1-03, APG                                        	1	14	 3F417-1          	27	1
1269	  CARTON LAYER PK-YFY1600B-P1-04, APG                                        	 3F418-1          	137	648900	  CARTON LAYER PK-YFY1600B-P1-04, APG                                        	1	14	 3F418-1          	27	1
1270	  CARTON LAYER PK-YFY1600B-P1-05, APG                                        	 3F419-1          	278	69500	  CARTON LAYER PK-YFY1600B-P1-05, APG                                        	1	14	 3F419-1          	27	1
1271	  CARTON LAYER PK-YFY1600B-P1-06, APG                                        	 3F420-1          	204	412800	  CARTON LAYER PK-YFY1600B-P1-06, APG                                        	1	14	 3F420-1          	27	1
1272	  CARTON LAYER PK-YFY1600B-P1-07, APG                                        	 3F421-1          	122	115900	  CARTON LAYER PK-YFY1600B-P1-07, APG                                        	1	14	 3F421-1          	27	1
1273	  CARTON LAYER PK-YFY1600B-P1-08, APG                                        	 3F422-1          	122	146400	  CARTON LAYER PK-YFY1600B-P1-08, APG                                        	1	14	 3F422-1          	27	1
1274	  CARTON BOX PK-YFY1600B-P2-01, PRIMABOX                                     	 3F423-1          	56	1008000	  CARTON BOX PK-YFY1600B-P2-01, PRIMABOX                                     	1	14	 3F423-1          	27	1
1275	  CARTON LAYER PK-YFY1600B-P2-02, APG                                        	 3F424-1          	226	542400	  CARTON LAYER PK-YFY1600B-P2-02, APG                                        	1	14	 3F424-1          	27	1
1276	  CARTON LAYER PK-YFY1600B-P2-03, APG                                        	 3F425-1          	124	217000	  CARTON LAYER PK-YFY1600B-P2-03, APG                                        	1	14	 3F425-1          	27	1
1277	  CARTON BOX PK-YFZ1200GT-P1-01, PRIMABOX                                    	 3F426-1          	84	1974000	  CARTON BOX PK-YFZ1200GT-P1-01, PRIMABOX                                    	1	14	 3F426-1          	27	1
1278	  CARTON LAYER PK-YFZ1200GT-P1-02, PRIMABOX                                  	 3F427-1          	85	850000	  CARTON LAYER PK-YFZ1200GT-P1-02, PRIMABOX                                  	1	14	 3F427-1          	27	1
1279	  CARTON LAYER PK-YFZ1200GT-P1-03, PRIMABOX                                  	 3F428-1          	40	103000	  CARTON LAYER PK-YFZ1200GT-P1-03, PRIMABOX                                  	1	14	 3F428-1          	27	1
1280	  CARTON LAYER PK-YFZ1200GT-P1-03, APG                                       	 3F428-2          	100	632700	  CARTON LAYER PK-YFZ1200GT-P1-03, APG                                       	1	14	 3F428-2          	27	1
1281	  CARTON LAYER PK-YFZ1200GT-P1-04, PRIMABOX                                  	 3F429-1          	85	174000	  CARTON LAYER PK-YFZ1200GT-P1-04, PRIMABOX                                  	1	14	 3F429-1          	27	1
1282	  CARTON LAYER PK-YFZ1200GT-P1-04, APG                                       	 3F429-2          	134	408000	  CARTON LAYER PK-YFZ1200GT-P1-04, APG                                       	1	14	 3F429-2          	27	1
1283	  CARTON LAYER PK-YFZ1200GT-P1-05, PRIMABOX                                  	 3F430-1          	50	375000	  CARTON LAYER PK-YFZ1200GT-P1-05, PRIMABOX                                  	1	14	 3F430-1          	27	1
1284	  CARTON LAYER PK-YFZ1200GT-P1-05, APG                                       	 3F430-2          	74	0	  CARTON LAYER PK-YFZ1200GT-P1-05, APG                                       	1	14	 3F430-2          	27	1
1285	  CARTON LAYER PK-YFZ1200GT-P1-06, PRIMABOX                                  	 3F431-1          	85	0	  CARTON LAYER PK-YFZ1200GT-P1-06, PRIMABOX                                  	1	14	 3F431-1          	27	1
1286	  CARTON LAYER PK-YFZ1200GT-P1-07, PRIMABOX                                  	 3F432-1          	50	0	  CARTON LAYER PK-YFZ1200GT-P1-07, PRIMABOX                                  	1	14	 3F432-1          	27	1
1287	  CARTON DK FZ1300 B1-01, RSA                                                	 3F261-3          	54	873816	  CARTON DK FZ1300 B1-01, RSA                                                	1	14	 3F261-3          	27	1
1288	  CARTON DK FZ1300 B1-02, RSA                                                	 3F262-2          	68	31340	  CARTON DK FZ1300 B1-02, RSA                                                	1	14	 3F262-2          	27	1
1289	  CARTON DK FZ1300 B1-03, RSA                                                	 3F263-2          	55	98449	  CARTON DK FZ1300 B1-03, RSA                                                	1	14	 3F263-2          	27	1
1290	  CARTON DK FZ1300 B1-04, RSA                                                	 3F264-2          	60	70008	  CARTON DK FZ1300 B1-04, RSA                                                	1	14	 3F264-2          	27	1
1291	  CARTON DK FZ1300 B1-09, RSA                                                	 3F265-4          	56	58743	  CARTON DK FZ1300 B1-09, RSA                                                	1	14	 3F265-4          	27	1
1292	  CARTON DK FZ1300 B2-01, RSA                                                	 3F267-3          	52	487547	  CARTON DK FZ1300 B2-01, RSA                                                	1	14	 3F267-3          	27	1
1293	  CARTON DK FZ1300 B2-02, RSA                                                	 3F268-2          	60	129598	  CARTON DK FZ1300 B2-02, RSA                                                	1	14	 3F268-2          	27	1
1294	  CARTON DK FZ1300 B2-03, RSA                                                	 3F269-2          	46	57545	  CARTON DK FZ1300 B2-03, RSA                                                	1	14	 3F269-2          	27	1
1295	  CARTON PK FZ1300 B2-04, RSA                                                	 3F270-2          	58	43957	  CARTON PK FZ1300 B2-04, RSA                                                	1	14	 3F270-2          	27	1
1296	  CARTON PK FZ1300 B2-05, RSA                                                	 3F271-2          	66	165093	  CARTON PK FZ1300 B2-05, RSA                                                	1	14	 3F271-2          	27	1
1297	  CARTON PK FZ1300 B2-06, RSA                                                	 3F272-2          	63	26203	  CARTON PK FZ1300 B2-06, RSA                                                	1	14	 3F272-2          	27	1
1298	  CARTON DK FZ1600 B1-01, RSA                                                	 3F237-2          	78	1541208	  CARTON DK FZ1600 B1-01, RSA                                                	1	14	 3F237-2          	27	1
1299	  CARTON DK FZ1600 B1-02, RSA                                                	 3F238-3          	70	36296	  CARTON DK FZ1600 B1-02, RSA                                                	1	14	 3F238-3          	27	1
1300	  CARTON DK FZ1600 B1-03, RSA                                                	 3F239-3          	50	97893	  CARTON DK FZ1600 B1-03, RSA                                                	1	14	 3F239-3          	27	1
1301	  CARTON DK FZ1600 B1-04, RSA                                                	 3F240-3          	70	73841	  CARTON DK FZ1600 B1-04, RSA                                                	1	14	 3F240-3          	27	1
1302	  CARTON DK FZ1600 B1-09, RSA                                                	 3F241-2          	60	171561	  CARTON DK FZ1600 B1-09, RSA                                                	1	14	 3F241-2          	27	1
1303	  CARTON DK FZ1600 B2-01, RSA                                                	 3F242-2          	89	877681	  CARTON DK FZ1600 B2-01, RSA                                                	1	14	 3F242-2          	27	1
1304	  CARTON DK FZ1600 B2-02, RSA                                                	 3F243-4          	60	179902	  CARTON DK FZ1600 B2-02, RSA                                                	1	14	 3F243-4          	27	1
1305	  CARTON DK FZ1600 B2-03, RSA                                                	 3F244-3          	60	73583	  CARTON DK FZ1600 B2-03, RSA                                                	1	14	 3F244-3          	27	1
1306	  CARTON DK FZ1600 B2-04, RSA                                                	 3F245-3          	80	57187	  CARTON DK FZ1600 B2-04, RSA                                                	1	14	 3F245-3          	27	1
1307	  CARTON DK FZ1600 B2-05, RSA                                                	 3F246-3          	80	62214	  CARTON DK FZ1600 B2-05, RSA                                                	1	14	 3F246-3          	27	1
1308	  CARTON DK FZ1600 B2-06, RSA                                                	 3F247-3          	80	205656	  CARTON DK FZ1600 B2-06, RSA                                                	1	14	 3F247-3          	27	1
1309	  CARTON DK FZ1600 B2-07, RSA                                                	 3F248-3          	80	34103	  CARTON DK FZ1600 B2-07, RSA                                                	1	14	 3F248-3          	27	1
1310	  CARTON DK FZ1800 B1-01, RSA                                                	 3F249-2          	59	1357668	  CARTON DK FZ1800 B1-01, RSA                                                	1	14	 3F249-2          	27	1
1311	  CARTON DK FZ1800 B1-02, RSA                                                	 3F250-3          	56	31011	  CARTON DK FZ1800 B1-02, RSA                                                	1	14	 3F250-3          	27	1
1312	  CARTON DK FZ1800 B1-03, RSA                                                	 3F251-3          	32	73442	  CARTON DK FZ1800 B1-03, RSA                                                	1	14	 3F251-3          	27	1
1313	  CARTON DK FZ1800 B1-04, RSA                                                	 3F252-3          	57	238603	  CARTON DK FZ1800 B1-04, RSA                                                	1	14	 3F252-3          	27	1
1314	  CARTON DK FZ1800 B1-09, Mitra A                                            	 3F253-2          	54	161540	  CARTON DK FZ1800 B1-09, Mitra A                                            	1	14	 3F253-2          	27	1
1315	  CARTON DK FZ1800 B2-01, RSA                                                	 3F254-2          	50	680335	  CARTON DK FZ1800 B2-01, RSA                                                	1	14	 3F254-2          	27	1
1316	  CARTON DK FZ1800 B2-02, RSA                                                	 3F255-4          	58	186298	  CARTON DK FZ1800 B2-02, RSA                                                	1	14	 3F255-4          	27	1
1317	  CARTON DK FZ1800 B2-03, RSA                                                	 3F256-3          	58	79826	  CARTON DK FZ1800 B2-03, RSA                                                	1	14	 3F256-3          	27	1
1318	  CARTON DK FZ1800 B2-04, RSA                                                	 3F257-3          	52	50021	  CARTON DK FZ1800 B2-04, RSA                                                	1	14	 3F257-3          	27	1
1319	  CARTON DK FZ1800 B2-05, RSA                                                	 3F258-3          	61	61450	  CARTON DK FZ1800 B2-05, RSA                                                	1	14	 3F258-3          	27	1
1320	  CARTON DK FZ1800 B2-06, RSA                                                	 3F259-3          	54	148319	  CARTON DK FZ1800 B2-06, RSA                                                	1	14	 3F259-3          	27	1
1321	  CARTON DK FZ1800 B2-07, RSA                                                	 3F260-3          	57	26093	  CARTON DK FZ1800 B2-07, RSA                                                	1	14	 3F260-3          	27	1
1322	  CARTON MK FMD 1500SB1-02, APG                                              	 3F200-3          	71	200750	  CARTON MK FMD 1500SB1-02, APG                                              	1	14	 3F200-3          	27	1
1323	  CARTON MK FMD 1500SB1-03, APG                                              	 3F201-3          	113	77784	  CARTON MK FMD 1500SB1-03, APG                                              	1	14	 3F201-3          	27	1
1324	  CARTON MK FMD 1500SB1-04, APG                                              	 3F202-3          	115	767629	  CARTON MK FMD 1500SB1-04, APG                                              	1	14	 3F202-3          	27	1
1325	  CARTON MK FMD 1500SB1-05, APG                                              	 3F203-3          	105	965822	  CARTON MK FMD 1500SB1-05, APG                                              	1	14	 3F203-3          	27	1
1326	  CARTON MK FMD 1500SB1-06, APG                                              	 3F204-3          	124	61214	  CARTON MK FMD 1500SB1-06, APG                                              	1	14	 3F204-3          	27	1
1327	  CARTON MK FMD 1500SB2-01, RSA                                              	 3F205-2          	10	144088	  CARTON MK FMD 1500SB2-01, RSA                                              	1	14	 3F205-2          	27	1
1328	  CARTON MK FMD 1500SB2-02, APG                                              	 3F206-3          	46	169442	  CARTON MK FMD 1500SB2-02, APG                                              	1	14	 3F206-3          	27	1
1329	  CARTON MK FMD 1500SB2-03, APG                                              	 3F207-3          	44	85251	  CARTON MK FMD 1500SB2-03, APG                                              	1	14	 3F207-3          	27	1
1330	  CARTON MK FMD 1500SB2-04, APG                                              	 3F208-3          	105	270428	  CARTON MK FMD 1500SB2-04, APG                                              	1	14	 3F208-3          	27	1
1331	  CARTON MK FMD 1500SB2-05, APG                                              	 3F209-3          	42	76827	  CARTON MK FMD 1500SB2-05, APG                                              	1	14	 3F209-3          	27	1
1332	  CARTON MK FMD 1500SB2-06, APG                                              	 3F210-3          	107	158906	  CARTON MK FMD 1500SB2-06, APG                                              	1	14	 3F210-3          	27	1
1333	  CARTON MK FMD 1500SB2-07, APG                                              	 3F211-3          	113	149645	  CARTON MK FMD 1500SB2-07, APG                                              	1	14	 3F211-3          	27	1
1334	  CARTON MK FMD 1500SB2-08, APG                                              	 3F212-3          	40	9858	  CARTON MK FMD 1500SB2-08, APG                                              	1	14	 3F212-3          	27	1
1335	  CARTON MK FMD 1500SB2-16, MITRA ABADI                                      	 3F213-3          	88	101602	  CARTON MK FMD 1500SB2-16, MITRA ABADI                                      	1	14	 3F213-3          	27	1
1336	  CARTON MK FMD 1700SB1-01, PT. RSA                                          	 3F214-2          	77	1671803	  CARTON MK FMD 1700SB1-01, PT. RSA                                          	1	14	 3F214-2          	27	1
1337	  CARTON MK FMD 1700SB1-02, RSA                                              	 3F215-3          	43	142525	  CARTON MK FMD 1700SB1-02, RSA                                              	1	14	 3F215-3          	27	1
1338	  CARTON MK FMD 1700SB1-03, RSA                                              	 3F216-3          	81	82422	  CARTON MK FMD 1700SB1-03, RSA                                              	1	14	 3F216-3          	27	1
1339	  CARTON MK FMD 1700SB1-04, RSA                                              	 3F217-3          	84	466083	  CARTON MK FMD 1700SB1-04, RSA                                              	1	14	 3F217-3          	27	1
1340	  CARTON MK FMD 1700SB1-05, RSA                                              	 3F218-3          	84	1039840	  CARTON MK FMD 1700SB1-05, RSA                                              	1	14	 3F218-3          	27	1
1341	  CARTON MK FMD 1700SB1-06, RSA                                              	 3F219-3          	40	37718	  CARTON MK FMD 1700SB1-06, RSA                                              	1	14	 3F219-3          	27	1
1342	  CARTON MK FMD 1700SB2-01, RSA                                              	 3F220-2          	85	1173104	  CARTON MK FMD 1700SB2-01, RSA                                              	1	14	 3F220-2          	27	1
1343	  CARTON MK FMD 1700SB2-02, RSA                                              	 3F221-3          	42	173278	  CARTON MK FMD 1700SB2-02, RSA                                              	1	14	 3F221-3          	27	1
1344	  CARTON MK FMD 1700SB2-03, RSA                                              	 3F222-3          	50	112196	  CARTON MK FMD 1700SB2-03, RSA                                              	1	14	 3F222-3          	27	1
1345	  CARTON MK FMD 1700SB2-04, RSA                                              	 3F223-3          	95	297242	  CARTON MK FMD 1700SB2-04, RSA                                              	1	14	 3F223-3          	27	1
1346	  CARTON MK FMD 1700SB2-05, RSA                                              	 3F224-3          	40	72572	  CARTON MK FMD 1700SB2-05, RSA                                              	1	14	 3F224-3          	27	1
1347	  CARTON MK FMD 1700SB2-06, RSA                                              	 3F225-3          	30	54384	  CARTON MK FMD 1700SB2-06, RSA                                              	1	14	 3F225-3          	27	1
1348	  CARTON MK FMD 1700SB2-07, RSA                                              	 3F226-3          	28	41711	  CARTON MK FMD 1700SB2-07, RSA                                              	1	14	 3F226-3          	27	1
1349	  CARTON MK FMD 1700SB2-08, RSA                                              	 3F227-3          	33	5986	  CARTON MK FMD 1700SB2-08, RSA                                              	1	14	 3F227-3          	27	1
1350	  CARTON MK FMD 1700SB2-15, RSA                                              	 3F228-3          	40	31926	  CARTON MK FMD 1700SB2-15, RSA                                              	1	14	 3F228-3          	27	1
1351	  CARTON ITOKI PK-W1200H-P1-01, RSA                                          	 3I1-1            	52	1216800	  CARTON ITOKI PK-W1200H-P1-01, RSA                                          	1	14	 3I1-1            	27	1
1352	  CARTON ITOKI PK-W1200H-P1-02, RSA                                          	 3I1-2            	20	134000	  CARTON ITOKI PK-W1200H-P1-02, RSA                                          	1	14	 3I1-2            	27	1
1353	  CARTON ITOKI PK-W1200H-P1-03, RSA                                          	 3I1-3            	60	39000	  CARTON ITOKI PK-W1200H-P1-03, RSA                                          	1	14	 3I1-3            	27	1
1354	  CARTON ITOKI PK-W1200H-P1-04, RSA                                          	 3I1-4            	10	10000	  CARTON ITOKI PK-W1200H-P1-04, RSA                                          	1	14	 3I1-4            	27	1
1355	  CARTON ITOKI PK-W1200H-P1-05, RSA                                          	 3I1-5            	60	129000	  CARTON ITOKI PK-W1200H-P1-05, RSA                                          	1	14	 3I1-5            	27	1
1356	  CARTON ITOKI PK-W1200H-P2-01, RSA                                          	 3I1-6            	53	842700	  CARTON ITOKI PK-W1200H-P2-01, RSA                                          	1	14	 3I1-6            	27	1
1357	  CARTON ITOKI PK-W1200H-P2-02, RSA                                          	 3I1-7            	20	91000	  CARTON ITOKI PK-W1200H-P2-02, RSA                                          	1	14	 3I1-7            	27	1
1358	  CARTON ITOKI PK-W1200H-P2-03, RSA                                          	 3I1-8            	60	39000	  CARTON ITOKI PK-W1200H-P2-03, RSA                                          	1	14	 3I1-8            	27	1
1359	  CARTON ITOKI PK-W1200H-P2-04, RSA                                          	 3I1-9            	60	249000	  CARTON ITOKI PK-W1200H-P2-04, RSA                                          	1	14	 3I1-9            	27	1
1360	  CARTON ITOKI PK-W1200H-P2-05, RSA                                          	 3I1-10           	60	99000	  CARTON ITOKI PK-W1200H-P2-05, RSA                                          	1	14	 3I1-10           	27	1
1361	  CARTON ITOKI PK-W1200H-P2-06, RSA                                          	 3I1-11           	10	10000	  CARTON ITOKI PK-W1200H-P2-06, RSA                                          	1	14	 3I1-11           	27	1
1362	  CARTON ITOKI PK-W1200LOW-P1-01, RSA                                        	 3I2-1            	52	1060800	  CARTON ITOKI PK-W1200LOW-P1-01, RSA                                        	1	14	 3I2-1            	27	1
1363	  CARTON ITOKI PK-W1200LOW-P1-02, RSA                                        	 3I2-2            	20	79000	  CARTON ITOKI PK-W1200LOW-P1-02, RSA                                        	1	14	 3I2-2            	27	1
1364	  CARTON ITOKI PK-W1200LOW-P1-03, RSA                                        	 3I2-3            	60	39000	  CARTON ITOKI PK-W1200LOW-P1-03, RSA                                        	1	14	 3I2-3            	27	1
1365	  CARTON ITOKI PK-W1200LOW-P1-04, RSA                                        	 3I2-4            	20	28000	  CARTON ITOKI PK-W1200LOW-P1-04, RSA                                        	1	14	 3I2-4            	27	1
1366	  CARTON ITOKI PK-W1200LOW-P1-05, RSA                                        	 3I2-5            	10	10000	  CARTON ITOKI PK-W1200LOW-P1-05, RSA                                        	1	14	 3I2-5            	27	1
1367	  CARTON ITOKI PK-W1200LOW-P2-01, RSA                                        	 3I2-6            	53	572400	  CARTON ITOKI PK-W1200LOW-P2-01, RSA                                        	1	14	 3I2-6            	27	1
1368	  CARTON ITOKI PK-W1200LOW-P2-02, RSA                                        	 3I2-7            	20	44000	  CARTON ITOKI PK-W1200LOW-P2-02, RSA                                        	1	14	 3I2-7            	27	1
1369	  CARTON ITOKI PK-W1200LOW-P2-03, RSA                                        	 3I2-8            	60	33000	  CARTON ITOKI PK-W1200LOW-P2-03, RSA                                        	1	14	 3I2-8            	27	1
1370	  CARTON ITOKI PK-W1200LOW-P2-04, RSA                                        	 3I2-9            	60	21000	  CARTON ITOKI PK-W1200LOW-P2-04, RSA                                        	1	14	 3I2-9            	27	1
1371	  CARTON ITOKI PK-W1200LOW-P2-05, RSA                                        	 3I2-10           	60	99000	  CARTON ITOKI PK-W1200LOW-P2-05, RSA                                        	1	14	 3I2-10           	27	1
1372	  CARTON ITOKI PK-W1200LOW-P2-06, RSA                                        	 3I2-11           	60	93000	  CARTON ITOKI PK-W1200LOW-P2-06, RSA                                        	1	14	 3I2-11           	27	1
1373	  CARTON ITOKI PK-W1200LOW-P2-07, RSA                                        	 3I2-12           	60	63000	  CARTON ITOKI PK-W1200LOW-P2-07, RSA                                        	1	14	 3I2-12           	27	1
1374	  CARTON ITOKI PK-W1200LOW-P2-08, RSA                                        	 3I2-13           	10	10000	  CARTON ITOKI PK-W1200LOW-P2-08, RSA                                        	1	14	 3I2-13           	27	1
1375	  CARTON ITOKI PK-W1500-P1-01, RSA                                           	 3I3-1            	51	1173000	  CARTON ITOKI PK-W1500-P1-01, RSA                                           	1	14	 3I3-1            	27	1
1376	  CARTON ITOKI PK-W1500-P1-02, RSA                                           	 3I3-2            	20	84000	  CARTON ITOKI PK-W1500-P1-02, RSA                                           	1	14	 3I3-2            	27	1
1377	  CARTON ITOKI PK-W1500-P1-03, RSA                                           	 3I3-3            	60	39000	  CARTON ITOKI PK-W1500-P1-03, RSA                                           	1	14	 3I3-3            	27	1
1378	  CARTON ITOKI PK-W1500-P1-04, RSA                                           	 3I3-4            	60	9000	  CARTON ITOKI PK-W1500-P1-04, RSA                                           	1	14	 3I3-4            	27	1
1379	  CARTON ITOKI PK-W1500-P1-05, RSA                                           	 3I3-5            	20	23000	  CARTON ITOKI PK-W1500-P1-05, RSA                                           	1	14	 3I3-5            	27	1
1380	  CARTON ITOKI PK-W1500-P2-01, RSA                                           	 3I3-7            	53	731400	  CARTON ITOKI PK-W1500-P2-01, RSA                                           	1	14	 3I3-7            	27	1
1381	  CARTON ITOKI PK-W1500-P2-02, RSA                                           	 3I3-8            	20	52000	  CARTON ITOKI PK-W1500-P2-02, RSA                                           	1	14	 3I3-8            	27	1
1382	  CARTON ITOKI PK-W1500-P2-03, RSA                                           	 3I3-9            	60	33000	  CARTON ITOKI PK-W1500-P2-03, RSA                                           	1	14	 3I3-9            	27	1
1383	  CARTON ITOKI PK-W1500-P2-04, RSA                                           	 3I3-10           	60	36000	  CARTON ITOKI PK-W1500-P2-04, RSA                                           	1	14	 3I3-10           	27	1
1384	  CARTON ITOKI PK-W1500-P2-05, RSA                                           	 3I3-11           	60	150000	  CARTON ITOKI PK-W1500-P2-05, RSA                                           	1	14	 3I3-11           	27	1
1385	  CARTON ITOKI PK-W1500-P2-06, RSA                                           	 3I3-12           	60	84000	  CARTON ITOKI PK-W1500-P2-06, RSA                                           	1	14	 3I3-12           	27	1
1386	  CARTON ITOKI PK-W1500-P2-07, RSA                                           	 3I3-13           	60	102000	  CARTON ITOKI PK-W1500-P2-07, RSA                                           	1	14	 3I3-13           	27	1
1387	  CARTON ITOKI PAPER CRAFT PK-W1500-P1-19, MMA                               	 3I3-15           	29	75400	  CARTON ITOKI PAPER CRAFT PK-W1500-P1-19, MMA                               	1	14	 3I3-15           	27	1
1388	  CARTON F.KC W TEN/TAN, MITRA  ABADI                                        	 3FK1-2 (No.32)   	28	678762	  CARTON F.KC W TEN/TAN, MITRA  ABADI                                        	1	14	 3FK1-2 (No.32)   	27	1
1389	  CARTON F.KC W TEN/TAN-02, Mitra Abadi                                      	 3FK2-2           	50	132892	  CARTON F.KC W TEN/TAN-02, Mitra Abadi                                      	1	14	 3FK2-2           	27	1
1390	  CARTON F.KC W TEN/TAN-03, Mitra Abadi                                      	 3FK3-2           	67	60442	  CARTON F.KC W TEN/TAN-03, Mitra Abadi                                      	1	14	 3FK3-2           	27	1
1391	  CARTON F.KC W TEN, Mitra Abadi                                             	 3FK4-3 (No.43)   	18	222263	  CARTON F.KC W TEN, Mitra Abadi                                             	1	14	 3FK4-3 (No.43)   	27	1
1392	  CARTON F.KC W TEN-02, MITRA ABADI                                          	 3FK5-2           	98	59071	  CARTON F.KC W TEN-02, MITRA ABADI                                          	1	14	 3FK5-2           	27	1
1393	  CARTON F.KC W TEN-03, MITRA ABADI                                          	 3FK6-2           	23	7314	  CARTON F.KC W TEN-03, MITRA ABADI                                          	1	14	 3FK6-2           	27	1
1394	  CARTON F.KC WNKG, MITRA ABADI                                              	 3FK7-2 (24,25)   	36	956600	  CARTON F.KC WNKG, MITRA ABADI                                              	1	14	 3FK7-2 (24,25)   	27	1
1395	  CARTON F.KC WNKG-02, MITRA ABADI                                           	 3FK8-2           	74	88970	  CARTON F.KC WNKG-02, MITRA ABADI                                           	1	14	 3FK8-2           	27	1
1396	  CARTON F.KC WNKG-03, MITRA Abadi                                           	 3FK9-2           	76	38094	  CARTON F.KC WNKG-03, MITRA Abadi                                           	1	14	 3FK9-2           	27	1
1397	  CARTON F.KC WGWT, MITRA ABADI                                              	 3FK10-2 (41,42)  	57	1579764	  CARTON F.KC WGWT, MITRA ABADI                                              	1	14	 3FK10-2 (41,42)  	27	1
1398	  CARTON F.KC WGWT-02, Mitra Abadi                                           	 3FK11-2          	78	62474	  CARTON F.KC WGWT-02, Mitra Abadi                                           	1	14	 3FK11-2          	27	1
1399	  CARTON F.KC WGWT-03, Mitra Abadi                                           	 3FK12-2          	88	39646	  CARTON F.KC WGWT-03, Mitra Abadi                                           	1	14	 3FK12-2          	27	1
1400	  CARTON F.KC WGWT 3D, MITRA ABADI                                           	 3FK13-3 (22,23)  	29	1150370	  CARTON F.KC WGWT 3D, MITRA ABADI                                           	1	14	 3FK13-3 (22,23)  	27	1
1401	  CARTON F.KC WGWT 3D-02, Mitra Abadi                                        	 3FK14-3          	60	56990	  CARTON F.KC WGWT 3D-02, Mitra Abadi                                        	1	14	 3FK14-3          	27	1
1402	  CARTON F.KC WGWT 3D-03, Mitra Abadi                                        	 3FK15-3          	60	9104	  CARTON F.KC WGWT 3D-03, Mitra Abadi                                        	1	14	 3FK15-3          	27	1
1403	  CARTON F.KC WKD, Mitra Abadi                                               	 3FK16-3 (No.28)  	43	344954	  CARTON F.KC WKD, Mitra Abadi                                               	1	14	 3FK16-3 (No.28)  	27	1
1404	  CARTON F.KC WKD-02, MITRA ABADI                                            	 3FK17-2          	42	33493	  CARTON F.KC WKD-02, MITRA ABADI                                            	1	14	 3FK17-2          	27	1
1405	  CARTON F.KC WKD-03, MITRA ABADI                                            	 3FK18-2          	34	20402	  CARTON F.KC WKD-03, MITRA ABADI                                            	1	14	 3FK18-2          	27	1
1406	  CARTON F.KC WK D27, Mitra Abadi                                            	 3FK19-2 (No.27)  	47	596669	  CARTON F.KC WK D27, Mitra Abadi                                            	1	14	 3FK19-2 (No.27)  	27	1
1407	  CARTON F.KC WK D27-02, Mitra Abadi                                         	 3FK20-2          	47	38792	  CARTON F.KC WK D27-02, Mitra Abadi                                         	1	14	 3FK20-2          	27	1
1408	  CARTON F.KC WK D27-03, Mitra Abadi                                         	 3FK21-2          	33	16664	  CARTON F.KC WK D27-03, Mitra Abadi                                         	1	14	 3FK21-2          	27	1
1409	  CARTON F.KC WK D27-04, Mitra Abadi                                         	 3FK22-2          	48	34463	  CARTON F.KC WK D27-04, Mitra Abadi                                         	1	14	 3FK22-2          	27	1
1410	  CARTON F.KC WK D27-05, Mitra Abadi                                         	 3FK23-2          	31	15826	  CARTON F.KC WK D27-05, Mitra Abadi                                         	1	14	 3FK23-2          	27	1
1411	  CARTON F.KC WSOK, Mitra Abadi                                              	 3FK24-3 (No.33)  	66	726549	  CARTON F.KC WSOK, Mitra Abadi                                              	1	14	 3FK24-3 (No.33)  	27	1
1412	  CARTON F.KC WSOK-02, MITRA ABADI                                           	 3FK25-2          	72	50417	  CARTON F.KC WSOK-02, MITRA ABADI                                           	1	14	 3FK25-2          	27	1
1413	  CARTON F.KC WSOK-03, MITRA ABADI                                           	 3FK26-2          	82	49360	  CARTON F.KC WSOK-03, MITRA ABADI                                           	1	14	 3FK26-2          	27	1
1414	  CARTON F.KC WSHK, MITRA ABADI                                              	 3FK27-2 (No.26)  	31	704699	  CARTON F.KC WSHK, MITRA ABADI                                              	1	14	 3FK27-2 (No.26)  	27	1
1415	  CARTON F.KC WSHK-02, Mitra Abadi                                           	 3FK28-2          	98	127235	  CARTON F.KC WSHK-02, Mitra Abadi                                           	1	14	 3FK28-2          	27	1
1416	  CARTON F.KC WSHK-03, Mitra Abadi                                           	 3FK29-2          	84	50303	  CARTON F.KC WSHK-03, Mitra Abadi                                           	1	14	 3FK29-2          	27	1
1417	  CARTON F.KC R2 DRL, MITRA ABADI                                            	 3FK30-3 (No.44)  	21	168794	  CARTON F.KC R2 DRL, MITRA ABADI                                            	1	14	 3FK30-3 (No.44)  	27	1
1418	  CARTON F.KC R2 DRL-02, MITRA ABADI                                         	 3FK31-3          	56	98174	  CARTON F.KC R2 DRL-02, MITRA ABADI                                         	1	14	 3FK31-3          	27	1
1419	  CARTON F.KC R2 DRL-03, MITRA ABADI                                         	 3FK32-3          	50	84802	  CARTON F.KC R2 DRL-03, MITRA ABADI                                         	1	14	 3FK32-3          	27	1
1420	  CARTON F.KC R3DRL, MITRA ABADI                                             	 3FK33-3 (No.36)  	21	243334	  CARTON F.KC R3DRL, MITRA ABADI                                             	1	14	 3FK33-3 (No.36)  	27	1
1421	  CARTON F.KC R3DRL-02, MITRA ABADI                                          	 3FK34-3          	30	65704	  CARTON F.KC R3DRL-02, MITRA ABADI                                          	1	14	 3FK34-3          	27	1
1422	  CARTON F.KC R3DRL-03, Mitra Abadi                                          	 3FK35-3          	41	41693	  CARTON F.KC R3DRL-03, Mitra Abadi                                          	1	14	 3FK35-3          	27	1
1423	  CARTON F.KC RKDB 31, MITRA ABADI                                           	 3FK36-3 (No.31)  	21	199562	  CARTON F.KC RKDB 31, MITRA ABADI                                           	1	14	 3FK36-3 (No.31)  	27	1
1424	  CARTON F.KC R-KDB 31-02, MITRA ABADI                                       	 3FK37-3          	31	24890	  CARTON F.KC R-KDB 31-02, MITRA ABADI                                       	1	14	 3FK37-3          	27	1
1425	  CARTON F.KC R-KDB 31-03, MITRA ABADI                                       	 3FK38-2          	43	30392	  CARTON F.KC R-KDB 31-03, MITRA ABADI                                       	1	14	 3FK38-2          	27	1
1426	  CARTON F.KC R2 DTRT, MAKMUR M.A.                                           	 3FK39-3 (No.45)  	70	610920	  CARTON F.KC R2 DTRT, MAKMUR M.A.                                           	1	14	 3FK39-3 (No.45)  	27	1
1427	  CARTON F.KC R2 DTRT-02, MITRA ABADI                                        	 3FK40-2          	106	196936	  CARTON F.KC R2 DTRT-02, MITRA ABADI                                        	1	14	 3FK40-2          	27	1
1428	  CARTON F.KC R3 DTRT, MITRA ABADI                                           	 3FK41-3 (No.37)  	35	304301	  CARTON F.KC R3 DTRT, MITRA ABADI                                           	1	14	 3FK41-3 (No.37)  	27	1
1429	  CARTON F.KC R3 DTRT-02, MITRA ABADI                                        	 3FK42-3          	62	107498	  CARTON F.KC R3 DTRT-02, MITRA ABADI                                        	1	14	 3FK42-3          	27	1
1430	  CARTON F.KC SCNKGW, MITRA ABADI                                            	 3FK43-3 (5,18)   	42	1471754	  CARTON F.KC SCNKGW, MITRA ABADI                                            	1	14	 3FK43-3 (5,18)   	27	1
1431	  CARTON F.KC SCNKGW-02, Mitra Abadi                                         	 3FK44-3          	96	52477	  CARTON F.KC SCNKGW-02, Mitra Abadi                                         	1	14	 3FK44-3          	27	1
1432	  CARTON F.KC SCNKGW-03, Mitra Abadi                                         	 3FK45-3          	128	16496	  CARTON F.KC SCNKGW-03, Mitra Abadi                                         	1	14	 3FK45-3          	27	1
1433	  CARTON F.KC SCKDC 09, MITRA ABADI                                          	 3FK46-3 (No.9)   	101	1754978	  CARTON F.KC SCKDC 09, MITRA ABADI                                          	1	14	 3FK46-3 (No.9)   	27	1
1434	  CARTON F.KC SCKDC 09-02, Mitra Abadi                                       	 3FK47-3          	126	107665	  CARTON F.KC SCKDC 09-02, Mitra Abadi                                       	1	14	 3FK47-3          	27	1
1435	  CARTON F.KC SCKDC 09-03, Mitra Abadi                                       	 3FK48-3          	161	81236	  CARTON F.KC SCKDC 09-03, Mitra Abadi                                       	1	14	 3FK48-3          	27	1
1436	  CARTON F.KC SCKDC 09-04, Mitra Abadi                                       	 3FK49-3          	103	13232	  CARTON F.KC SCKDC 09-04, Mitra Abadi                                       	1	14	 3FK49-3          	27	1
1437	  CARTON F.KC SCKD7, MITRA ABADI                                             	 3FK50-2(No.7)    	83	1299662	  CARTON F.KC SCKD7, MITRA ABADI                                             	1	14	 3FK50-2(No.7)    	27	1
1438	  CARTON F.KC SCKD7-02, Mitra Abadi                                          	 3FK51-2          	124	172718	  CARTON F.KC SCKD7-02, Mitra Abadi                                          	1	14	 3FK51-2          	27	1
1439	  CARTON F.KC SCKD7-03, Mitra Abadi                                          	 3FK52-2          	61	57563	  CARTON F.KC SCKD7-03, Mitra Abadi                                          	1	14	 3FK52-2          	27	1
1440	  CARTON F.KC SCKD7-04, Mitra Abadi                                          	 3FK53-2          	64	76424	  CARTON F.KC SCKD7-04, Mitra Abadi                                          	1	14	 3FK53-2          	27	1
1441	  CARTON F.KC SCKD7-05, Mitra Abadi                                          	 3FK54-2          	66	52428	  CARTON F.KC SCKD7-05, Mitra Abadi                                          	1	14	 3FK54-2          	27	1
1442	  CARTON F.KC SCKD8, MITRA ABADI                                             	 3FK55-3 (No.8)   	77	1275097	  CARTON F.KC SCKD8, MITRA ABADI                                             	1	14	 3FK55-3 (No.8)   	27	1
1443	  CARTON F.KC SCKD8-02, Mitra Abadi                                          	 3FK56-3          	103	83160	  CARTON F.KC SCKD8-02, Mitra Abadi                                          	1	14	 3FK56-3          	27	1
1444	  CARTON F.KC SCKD8-03, Mitra Abadi                                          	 3FK57-3          	114	61052	  CARTON F.KC SCKD8-03, Mitra Abadi                                          	1	14	 3FK57-3          	27	1
1445	  CARTON F.KC SCKD8-04, Mitra Abadi                                          	 3FK58-3          	69	27651	  CARTON F.KC SCKD8-04, Mitra Abadi                                          	1	14	 3FK58-3          	27	1
1446	  CARTON F.KC SCKD8-05, Mitra Abadi                                          	 3FK59-3          	82	21416	  CARTON F.KC SCKD8-05, Mitra Abadi                                          	1	14	 3FK59-3          	27	1
1447	  CARTON F.KC SCKD10, MITRA ABADI                                            	 3FK60-3 (No.10)  	81	161237	  CARTON F.KC SCKD10, MITRA ABADI                                            	1	14	 3FK60-3 (No.10)  	27	1
1448	  CARTON F.KC SCKD10-02, Mitra Abadi                                         	 3FK61-3          	79	52534	  CARTON F.KC SCKD10-02, Mitra Abadi                                         	1	14	 3FK61-3          	27	1
1449	  CARTON F.KC SCKD10-03, Mitra Abadi                                         	 3FK62-3          	86	56320	  CARTON F.KC SCKD10-03, Mitra Abadi                                         	1	14	 3FK62-3          	27	1
1450	  CARTON F.KC SCKD11, MITRA ABADI                                            	 3FK63-3 (No.11)  	67	846125	  CARTON F.KC SCKD11, MITRA ABADI                                            	1	14	 3FK63-3 (No.11)  	27	1
1451	  CARTON F.KC SCKD11-02, Mitra Abadi                                         	 3FK64-3          	76	50565	  CARTON F.KC SCKD11-02, Mitra Abadi                                         	1	14	 3FK64-3          	27	1
1452	  CARTON F.KC SCKD11-03, Mitra Abadi                                         	 3FK65-3          	96	61081	  CARTON F.KC SCKD11-03, Mitra Abadi                                         	1	14	 3FK65-3          	27	1
1453	  CARTON F.KC SCKD11-04, Mitra Abadi                                         	 3FK66-3          	107	13768	  CARTON F.KC SCKD11-04, Mitra Abadi                                         	1	14	 3FK66-3          	27	1
1454	  CARTON F.KC SCGWT 15, MITRA ABADI                                          	 3FK67-3 (15,16)  	27	41015	  CARTON F.KC SCGWT 15, MITRA ABADI                                          	1	14	 3FK67-3 (15,16)  	27	1
1455	  CARTON F.KC SCGWT 15-02, Mitra Abadi                                       	 3FK68-3          	199	82083	  CARTON F.KC SCGWT 15-02, Mitra Abadi                                       	1	14	 3FK68-3          	27	1
1456	  CARTON F.KC SCGWT 15-03, Mitra Abadi                                       	 3FK69-3          	64	6392	  CARTON F.KC SCGWT 15-03, Mitra Abadi                                       	1	14	 3FK69-3          	27	1
1457	  CARTON F.KC SCGWT20, MITRA A                                               	 3FK70-2 (19,20)  	47	1400196	  CARTON F.KC SCGWT20, MITRA A                                               	1	14	 3FK70-2 (19,20)  	27	1
1458	  CARTON F.KC SCGWT20-02, Mitra Abadi                                        	 3FK71-2          	60	71640	  CARTON F.KC SCGWT20-02, Mitra Abadi                                        	1	14	 3FK71-2          	27	1
1459	  CARTON F.KC SCGWT20-03, Mitra Abadi                                        	 3FK72-2          	60	29880	  CARTON F.KC SCGWT20-03, Mitra Abadi                                        	1	14	 3FK72-2          	27	1
1460	  CARTON F.KC SCMG SHK, MITRA ABADI                                          	 3FK73-3 (No.6)   	33	926649	  CARTON F.KC SCMG SHK, MITRA ABADI                                          	1	14	 3FK73-3 (No.6)   	27	1
1461	  CARTON F.KC SCMG SHK-02, Mitra Abadi                                       	 3FK74-3          	76	33942	  CARTON F.KC SCMG SHK-02, Mitra Abadi                                       	1	14	 3FK74-3          	27	1
1462	  CARTON F.KC SCMG SHK-03, Mitra Abadi                                       	 3FK75-3          	132	17017	  CARTON F.KC SCMG SHK-03, Mitra Abadi                                       	1	14	 3FK75-3          	27	1
1463	  CARTON F.KC SE 3D, MITRA ABADI                                             	 3FK76-2 (No.34)  	36	1076524	  CARTON F.KC SE 3D, MITRA ABADI                                             	1	14	 3FK76-2 (No.34)  	27	1
1464	  CARTON F.KC 2SEI, MITRA ABADI                                              	 3FK77-2 (No.35)  	40	1689711	  CARTON F.KC 2SEI, MITRA ABADI                                              	1	14	 3FK77-2 (No.35)  	27	1
1465	  CARTON F.KC KHS, MITRA ABADI                                               	 3FK78-3 (No.30)  	43	429605	  CARTON F.KC KHS, MITRA ABADI                                               	1	14	 3FK78-3 (No.30)  	27	1
1466	  CARTON F.KC KHS-02, MITRA ABADI                                            	 3FK79-2          	104	83485	  CARTON F.KC KHS-02, MITRA ABADI                                            	1	14	 3FK79-2          	27	1
1467	  CARTON F.KC KHS-03, MITRA ABADI                                            	 3FK80-2          	121	84695	  CARTON F.KC KHS-03, MITRA ABADI                                            	1	14	 3FK80-2          	27	1
1468	  CARTON F.KC KDS 29, MITRA ABADI                                            	 3FK81-3 (No.29)  	55	675665	  CARTON F.KC KDS 29, MITRA ABADI                                            	1	14	 3FK81-3 (No.29)  	27	1
1469	  CARTON F.KC KDS 29-02, MITRA ABADI                                         	 3FK82-3          	30	26566	  CARTON F.KC KDS 29-02, MITRA ABADI                                         	1	14	 3FK82-3          	27	1
1470	  CARTON F.KC KDS 29-03, MITRA ABADI                                         	 3FK83-2          	53	30755	  CARTON F.KC KDS 29-03, MITRA ABADI                                         	1	14	 3FK83-2          	27	1
1471	  CARTON TENMA 350 Layer 1, Mitra Abadi                                      	 3T2-1            	319	0	  CARTON TENMA 350 Layer 1, Mitra Abadi                                      	1	14	 3T2-1            	27	1
1472	  CARTON TENMA 550 (NEW PRINTING), Mitra Abadi                               	 3T5-4            	66	0	  CARTON TENMA 550 (NEW PRINTING), Mitra Abadi                               	1	14	 3T5-4            	27	1
1473	  CARTON TENMA 550 Layer 1, MITRA A                                          	 3T6-3            	873	0	  CARTON TENMA 550 Layer 1, MITRA A                                          	1	14	 3T6-3            	27	1
1474	  CARTON TENMA 409-650 Layer 1, RSA                                          	 3T10-2           	44	0	  CARTON TENMA 409-650 Layer 1, RSA                                          	1	14	 3T10-2           	27	1
1475	  CARTON TENMA 750 (NEW PRINTING), Mitra Abadi                               	 3T7-4            	28	0	  CARTON TENMA 750 (NEW PRINTING), Mitra Abadi                               	1	14	 3T7-4            	27	1
1476	  CARTON TENMA 750 Layer 1, Mitra Abadi                                      	 3T8-2            	224	0	  CARTON TENMA 750 Layer 1, Mitra Abadi                                      	1	14	 3T8-2            	27	1
1477	  CARTON MARUNI CB NW800, RSA                                                	 3M1-2            	71	802371	  CARTON MARUNI CB NW800, RSA                                                	1	14	 3M1-2            	27	1
1478	  CARTON MARUNI CB NW800-02, RSA                                             	 3M2-3            	45	75025	  CARTON MARUNI CB NW800-02, RSA                                             	1	14	 3M2-3            	27	1
1479	  CARTON MARUNI CB NW800-03, RSA                                             	 3M3-3            	85	35471	  CARTON MARUNI CB NW800-03, RSA                                             	1	14	 3M3-3            	27	1
1480	  CARTON MARUNI CB NW800-04, RSA                                             	 3M4-3            	50	32917	  CARTON MARUNI CB NW800-04, RSA                                             	1	14	 3M4-3            	27	1
1481	  CARTON MARUNI CB NW800-05, RSA                                             	 3M5-3            	45	38963	  CARTON MARUNI CB NW800-05, RSA                                             	1	14	 3M5-3            	27	1
1482	  CARTON MARUNI CB NW800-18, MAKMUR M. A.                                    	 3M7-3            	94	74550	  CARTON MARUNI CB NW800-18, MAKMUR M. A.                                    	1	14	 3M7-3            	27	1
1483	  CARTON MARUNI MA1000CD NEW, Packing I                                      	 3M22-3           	59	0	  CARTON MARUNI MA1000CD NEW, Packing I                                      	1	14	 3M22-3           	27	1
1484	  CARTON MARUNI MA1000CD Layer 19, Packing I                                 	 3M28-2           	20	26174	  CARTON MARUNI MA1000CD Layer 19, Packing I                                 	1	14	 3M28-2           	27	1
1485	  CARTON MARUNI MSO W1000 -01, PRIMA BOX                                     	 3M54-3           	40	505754	  CARTON MARUNI MSO W1000 -01, PRIMA BOX                                     	1	14	 3M54-3           	27	1
1486	  CARTON MARUNI MSO W1000 -02, RSA                                           	 3M55-1           	40	57069	  CARTON MARUNI MSO W1000 -02, RSA                                           	1	14	 3M55-1           	27	1
1487	  CARTON MARUNI MSO W1000 -03, RSA                                           	 3M56-1           	50	15447	  CARTON MARUNI MSO W1000 -03, RSA                                           	1	14	 3M56-1           	27	1
1488	  CARTON MARUNI CB W1300, SINAR ERA BOX                                      	 3M38-3           	10	239200	  CARTON MARUNI CB W1300, SINAR ERA BOX                                      	1	14	 3M38-3           	27	1
1489	  CARTON MARUNI CB W1300-02, RSA                                             	 3M39-3           	82	177378	  CARTON MARUNI CB W1300-02, RSA                                             	1	14	 3M39-3           	27	1
1490	  CARTON MARUNI CB W1300-03, RSA                                             	 3M40-3           	66	25498	  CARTON MARUNI CB W1300-03, RSA                                             	1	14	 3M40-3           	27	1
1491	  CARTON MARUNI CB W1300-04, RSA                                             	 3M41-2           	82	80243	  CARTON MARUNI CB W1300-04, RSA                                             	1	14	 3M41-2           	27	1
1492	  CARTON MARUNI CB W1300-05, RSA                                             	 3M42-3           	82	72641	  CARTON MARUNI CB W1300-05, RSA                                             	1	14	 3M42-3           	27	1
1493	  CARTON MARUNI CB W1300-06, RSA                                             	 3M43-2           	76	177026	  CARTON MARUNI CB W1300-06, RSA                                             	1	14	 3M43-2           	27	1
1494	  CARTON MARUNI CB W1300-07, RSA                                             	 3M44-3           	82	48991	  CARTON MARUNI CB W1300-07, RSA                                             	1	14	 3M44-3           	27	1
1495	  CARTON MARUNI CB W1300-20, CV. MAKMUR M. A.                                	 3M45-3           	164	164000	  CARTON MARUNI CB W1300-20, CV. MAKMUR M. A.                                	1	14	 3M45-3           	27	1
1496	  CARTON MARUNI CB MA1500 CW-01, PRIMA BOX                                   	 3M46-1           	142	3602354	  CARTON MARUNI CB MA1500 CW-01, PRIMA BOX                                   	1	14	 3M46-1           	27	1
1497	  CARTON MARUNI CB MA1500 CW-02, CMB                                         	 3M47-2           	50	139025	  CARTON MARUNI CB MA1500 CW-02, CMB                                         	1	14	 3M47-2           	27	1
1498	  CARTON MARUNI CB MA1500 CW-03, CMB                                         	 3M48-2           	56	22313	  CARTON MARUNI CB MA1500 CW-03, CMB                                         	1	14	 3M48-2           	27	1
1499	  CARTON MARUNI CB MA1500 CW-04, CMB                                         	 3M49-2           	60	55782	  CARTON MARUNI CB MA1500 CW-04, CMB                                         	1	14	 3M49-2           	27	1
1500	  CARTON MARUNI CB MA1500 CW-05, PRIMA BOX                                   	 3M50-1           	290	2688964	  CARTON MARUNI CB MA1500 CW-05, PRIMA BOX                                   	1	14	 3M50-1           	27	1
1501	  CARTON MARUNI CB MA1500 CW-06, CMB                                         	 3M51-2           	50	53636	  CARTON MARUNI CB MA1500 CW-06, CMB                                         	1	14	 3M51-2           	27	1
1502	  CARTON MARUNI CB MA1500 CW-07, CMB                                         	 3M52-2           	40	68511	  CARTON MARUNI CB MA1500 CW-07, CMB                                         	1	14	 3M52-2           	27	1
1503	  CARTON MARUNI CB MA1500 CW-25, CV. MAKMUR M. A.                            	 3M53-3           	114	133530	  CARTON MARUNI CB MA1500 CW-25, CV. MAKMUR M. A.                            	1	14	 3M53-3           	27	1
1504	  CARTON  BOX D-NS-W525-01, CMB                                              	 3N23-1           	313	5163557	  CARTON  BOX D-NS-W525-01, CMB                                              	1	14	 3N23-1           	27	1
1505	  CARTON BOX D-NS-W525-01-N, ANUGERAH PRIMA G.                               	 3N23-3           	533	8792895	  CARTON BOX D-NS-W525-01-N, ANUGERAH PRIMA G.                               	1	14	 3N23-3           	27	1
1506	  CARTON  LY D-NS-W525-02-N, ANUGERAH PRIMA G.                               	 3N24-3           	1144	2287999	  CARTON  LY D-NS-W525-02-N, ANUGERAH PRIMA G.                               	1	14	 3N24-3           	27	1
1507	  CARTON  LY D-NS-W525-03, CMB                                               	 3N25-1           	201	1267675	  CARTON  LY D-NS-W525-03, CMB                                               	1	14	 3N25-1           	27	1
1508	  CARTON  BOX D-NS-W700-01, CMB                                              	 3N26-1           	763	9612803	  CARTON  BOX D-NS-W700-01, CMB                                              	1	14	 3N26-1           	27	1
1509	  CARTON BOX D-NS-W700-01-N, ANUGERAH PRIMA G.                               	 3N26-3           	616	7760795	  CARTON BOX D-NS-W700-01-N, ANUGERAH PRIMA G.                               	1	14	 3N26-3           	27	1
1510	  CARTON LY D-NS-W700-02-N, ANUGERAH PRIMA G.                                	 3N27-3           	633	1107750	  CARTON LY D-NS-W700-02-N, ANUGERAH PRIMA G.                                	1	14	 3N27-3           	27	1
1511	  CARTON LY D-NS-W700-03, CMB                                                	 3N28-1           	405	1798602	  CARTON LY D-NS-W700-03, CMB                                                	1	14	 3N28-1           	27	1
1512	  CARTON BOX PK-NAS-TANAITA-01, ANUGERAH P. G.                               	 3N29-2           	40	293070	  CARTON BOX PK-NAS-TANAITA-01, ANUGERAH P. G.                               	1	14	 3N29-2           	27	1
1513	  CARTON  W700 Daiwa 1, CMB                                                  	 3N17-2           	46	0	  CARTON  W700 Daiwa 1, CMB                                                  	1	14	 3N17-2           	27	1
1514	  CARTON  W700 Daiwa 2, CMB                                                  	 3N18-2           	31	0	  CARTON  W700 Daiwa 2, CMB                                                  	1	14	 3N18-2           	27	1
1515	  CARTON STORAGE MK SB625N-01, SEB                                           	 3ST16-3          	1581	12015600	  CARTON STORAGE MK SB625N-01, SEB                                           	1	14	 3ST16-3          	27	1
1516	  CARTON STORAGE MK SB625N-02, ANUGERAH P. G.                                	 3ST17-3          	1574	2833435	  CARTON STORAGE MK SB625N-02, ANUGERAH P. G.                                	1	14	 3ST17-3          	27	1
1517	  CARTON STORAGE MK SB625N-03, ANUGERAH P. G.                                	 3ST18-3          	3127	3752400	  CARTON STORAGE MK SB625N-03, ANUGERAH P. G.                                	1	14	 3ST18-3          	27	1
1518	  CARTON STORAGE MK SB625N-04, ANUREGAH P. G.                                	 3ST19-3          	1630	3097000	  CARTON STORAGE MK SB625N-04, ANUREGAH P. G.                                	1	14	 3ST19-3          	27	1
1519	  CARTON STORAGE MK SB625N-05, ANUGERAH P. G.                                	 3ST20-3          	4774	1193500	  CARTON STORAGE MK SB625N-05, ANUGERAH P. G.                                	1	14	 3ST20-3          	27	1
1520	  CARTON STORAGE MK SB625N-06, ANUGERAH P. G.                                	 3ST21-3          	3137	3450700	  CARTON STORAGE MK SB625N-06, ANUGERAH P. G.                                	1	14	 3ST21-3          	27	1
1521	  CARTON STORAGE MK SB625N-07, ANUGERAH P. G.                                	 3ST22-3          	3061	661775	  CARTON STORAGE MK SB625N-07, ANUGERAH P. G.                                	1	14	 3ST22-3          	27	1
1522	  CARTON CB SPICE BOX W405, KSI                                              	 3SP1-1           	18	0	  CARTON CB SPICE BOX W405, KSI                                              	1	14	 3SP1-1           	27	1
1523	  CARTON CB SPICE BOX  MK-W405, Prima Box                                    	 3SP1-2           	24	0	  CARTON CB SPICE BOX  MK-W405, Prima Box                                    	1	14	 3SP1-2           	27	1
1524	  CARTON CB SPICE BOX  W200, Prima Box                                       	 3SP2-1           	92	0	  CARTON CB SPICE BOX  W200, Prima Box                                       	1	14	 3SP2-1           	27	1
1525	  CARTON  NAS CLOUK NSCL-01, Rukun Sentosa Abadi                             	 3N21-1           	157	648196	  CARTON  NAS CLOUK NSCL-01, Rukun Sentosa Abadi                             	1	14	 3N21-1           	27	1
1526	  CARTON  NAS CLOUK NSCL-02 New, Rukun Sentosa Abadi                         	 3N22-3           	118	123309	  CARTON  NAS CLOUK NSCL-02 New, Rukun Sentosa Abadi                         	1	14	 3N22-3           	27	1
1527	  CARTON  WOOD ONE  MK WOD-FL-01, CV MITRA A                                 	 3W1-2            	134	823894	  CARTON  WOOD ONE  MK WOD-FL-01, CV MITRA A                                 	1	14	 3W1-2            	27	1
1528	  CARTON  WOOD ONE  MK WOD-FL-02, SEB                                        	 3W2-2            	0	0	  CARTON  WOOD ONE  MK WOD-FL-02, SEB                                        	1	14	 3W2-2            	27	1
1529	  CARTON  WOOD ONE  MK WOD-FL-02, APG                                        	 3W2-4            	101	598817	  CARTON  WOOD ONE  MK WOD-FL-02, APG                                        	1	14	 3W2-4            	27	1
1530	  CARTON  WOOD ONE  MK WOD-FL-03, CV MITRA A                                 	 3W3-2            	83	123953	  CARTON  WOOD ONE  MK WOD-FL-03, CV MITRA A                                 	1	14	 3W3-2            	27	1
1531	  CARTON  WOOD ONE  MK WOD-FL-04, CV MITRA A                                 	 3W4-2            	106	21751	  CARTON  WOOD ONE  MK WOD-FL-04, CV MITRA A                                 	1	14	 3W4-2            	27	1
1532	  CARTON  WOOD ONE  MK WOD-TALL-01, CV MITRA A                               	 3W5-2            	67	534361	  CARTON  WOOD ONE  MK WOD-TALL-01, CV MITRA A                               	1	14	 3W5-2            	27	1
1533	  CARTON  WOOD ONE  MK WOD-TALL-02, APG                                      	 3W6-3            	163	1406765	  CARTON  WOOD ONE  MK WOD-TALL-02, APG                                      	1	14	 3W6-3            	27	1
1534	  CARTON  WOOD ONE  MK WOD-TALL-03, CV MITRA A                               	 3W7-2            	142	88015	  CARTON  WOOD ONE  MK WOD-TALL-03, CV MITRA A                               	1	14	 3W7-2            	27	1
1535	  CARTON  WOOD ONE  MK WOD-TALL-04, CV MITRA A                               	 3W8-2            	64	32282	  CARTON  WOOD ONE  MK WOD-TALL-04, CV MITRA A                               	1	14	 3W8-2            	27	1
1536	  CARTON  WOOD ONE  MK WOD-MAG-01, CV MITRA A                                	 3W9-2            	425	1963489	  CARTON  WOOD ONE  MK WOD-MAG-01, CV MITRA A                                	1	14	 3W9-2            	27	1
1537	  CARTON  WOOD ONE  MK WOD-MAG-02, SEB                                       	 3W10-4           	0	0	  CARTON  WOOD ONE  MK WOD-MAG-02, SEB                                       	1	14	 3W10-4           	27	1
1538	  CARTON  WOOD ONE  MK WOD-MAG-02, APG                                       	 3W10-5           	175	607948	  CARTON  WOOD ONE  MK WOD-MAG-02, APG                                       	1	14	 3W10-5           	27	1
1539	  CARTON  WOOD ONE  MK WOD-TLA-01, CV MITRA A                                	 3W11-2           	794	3862016	  CARTON  WOOD ONE  MK WOD-TLA-01, CV MITRA A                                	1	14	 3W11-2           	27	1
1540	  CARTON  WOOD ONE  MK WOD-TLA-02, SEB                                       	 3W12-4           	394	1761968	  CARTON  WOOD ONE  MK WOD-TLA-02, SEB                                       	1	14	 3W12-4           	27	1
1541	  CARTON  WOOD ONE  MK WOD-TLA-03, CV MITRA A                                	 3W13-2           	477	343440	  CARTON  WOOD ONE  MK WOD-TLA-03, CV MITRA A                                	1	14	 3W13-2           	27	1
1542	  CARTON  WOOD ONE  MK WOD-TLA-04, CV MITRA A                                	 3W14-2           	408	122400	  CARTON  WOOD ONE  MK WOD-TLA-04, CV MITRA A                                	1	14	 3W14-2           	27	1
1543	  CARTON  WOOD ONE  MK WOD-TLB-01, PT. SINAR ERA BOX                         	 3W15-4           	499	1816360	  CARTON  WOOD ONE  MK WOD-TLB-01, PT. SINAR ERA BOX                         	1	14	 3W15-4           	27	1
1544	  CARTON  WOOD ONE  MK WOD-TLB-02, PT. SINAR ERA BOX                         	 3W16-4           	501	1641276	  CARTON  WOOD ONE  MK WOD-TLB-02, PT. SINAR ERA BOX                         	1	14	 3W16-4           	27	1
1545	  CARTON  WOOD ONE  MK WOD-TLB-03, PT. SINAR ERA BOX                         	 3W17-3           	506	315744	  CARTON  WOOD ONE  MK WOD-TLB-03, PT. SINAR ERA BOX                         	1	14	 3W17-3           	27	1
1546	  CARTON  WOOD ONE  MK WOD-TLB-04, PT. SINAR ERA BOX                         	 3W18-4           	66	15444	  CARTON  WOOD ONE  MK WOD-TLB-04, PT. SINAR ERA BOX                         	1	14	 3W18-4           	27	1
1547	  CARTON Handy Wrap  17MICx50Cmx300M, DWI MAJU                               	 3C1-8            	63	3464270	  CARTON Handy Wrap  17MICx50Cmx300M, DWI MAJU                               	1	14	 3C1-8            	27	1
1548	  CARTON Handy Wrap 10MICx50Cmx300M, DWI MAJU                                	 3C1-9            	140	5931975	  CARTON Handy Wrap 10MICx50Cmx300M, DWI MAJU                                	1	14	 3C1-9            	27	1
1549	  CARTON FoamSheet 0.5mmx1200x800m, PT.KCS                                   	 3C5-1            	12	6960000	  CARTON FoamSheet 0.5mmx1200x800m, PT.KCS                                   	1	14	 3C5-1            	5	1
1550	  CARTON Foam Sheet 1mmx1200x500m, PT KCS                                    	 3C4-1            	4	2272500	  CARTON Foam Sheet 1mmx1200x500m, PT KCS                                    	1	14	 3C4-1            	27	1
1551	  CARTON PP Band(BLUE) W=15mm /Strapping Band, PT. WIRATAMA SSA              	 3C6-3            	25	2128812	  CARTON PP Band(BLUE) W=15mm /Strapping Band, PT. WIRATAMA SSA              	1	14	 3C6-3            	27	1
1552	  CARTON PP Band(PUTIH) W=15mm /Strapping Band, Tondira S                    	 3C7-1            	3	235141	  CARTON PP Band(PUTIH) W=15mm /Strapping Band, Tondira S                    	1	14	 3C7-1            	27	1
1553	  CARTON PP Band (MERAH) W=15mm /Strapping Band, Tondira S                   	 3C56-1           	1	218407	  CARTON PP Band (MERAH) W=15mm /Strapping Band, Tondira S                   	1	14	 3C56-1           	27	1
1554	  CARTON PET Band W=15.5mm, Power Pack                                       	 3C8-2            	13	4939972	  CARTON PET Band W=15.5mm, Power Pack                                       	1	14	 3C8-2            	27	1
1555	  CARTON HD-Nat 0.02x600x1000(m), SekawanIntiplast                           	 3C9-1            	41	21116224	  CARTON HD-Nat 0.02x600x1000(m), SekawanIntiplast                           	1	14	 3C9-1            	27	1
1556	  CARTON Plastik Bubble Polycell 1250mmx50M, KCS                             	 3C12-2           	1	104269	  CARTON Plastik Bubble Polycell 1250mmx50M, KCS                             	1	14	 3C12-2           	27	1
1557	  CARTON Plastik  0.03x600x250(m), CAHAYA MAS MAKMUR                         	 3C13-2           	16	3139716	  CARTON Plastik  0.03x600x250(m), CAHAYA MAS MAKMUR                         	1	14	 3C13-2           	27	1
1558	  CARTON Plastik  0.03x1000x250(m), MAS MAKMUR                               	 3C15-2           	2	683136	  CARTON Plastik  0.03x1000x250(m), MAS MAKMUR                               	1	14	 3C15-2           	27	1
1559	  CARTON Plastik 1500mmx250m, Gracia Plastik                                 	 3C14-1           	3	1017515	  CARTON Plastik 1500mmx250m, Gracia Plastik                                 	1	14	 3C14-1           	27	1
1560	  CARTON Tape (Clear) Bening 48mm, NTI                                       	 3C16-1           	910	6506468	  CARTON Tape (Clear) Bening 48mm, NTI                                       	1	14	 3C16-1           	27	1
1561	  CARTON Tape (Clear) Bening 18mm, NTI                                       	 3C17-1           	222	577186	  CARTON Tape (Clear) Bening 18mm, NTI                                       	1	14	 3C17-1           	27	1
1562	  CARTON Tape (Paper) Brown 48mmx50Yx48R, Berry Tape Indonesia               	 3C18-2           	212	993867	  CARTON Tape (Paper) Brown 48mmx50Yx48R, Berry Tape Indonesia               	1	14	 3C18-2           	27	1
1563	  CARTON Tape (Paper) White 18mm, NTI                                        	 3C20-2           	4060	5684000	  CARTON Tape (Paper) White 18mm, NTI                                        	1	14	 3C20-2           	27	1
1564	  CARTON Tape Cloth (Black) Wide 46mm, NTI                                   	 3C21-1           	106	983684	  CARTON Tape Cloth (Black) Wide 46mm, NTI                                   	1	14	 3C21-1           	27	1
1565	  CARTON Tape Cloth (Putih) NC 46mmx72R, NTI                                 	 3C21-2           	27	258169	  CARTON Tape Cloth (Putih) NC 46mmx72R, NTI                                 	1	14	 3C21-2           	27	1
1566	  CARTON Tape Cloth NC 46mmx72R KUNING, NTI                                  	 3C22-2           	72	668379	  CARTON Tape Cloth NC 46mmx72R KUNING, NTI                                  	1	14	 3C22-2           	27	1
1567	  KARTON ODNER ACC, PT. RSA                                                  	 3C57-1           	78	993772	  KARTON ODNER ACC, PT. RSA                                                  	1	14	 3C57-1           	27	1
1568	  Layer packing 4x1000x2000, Prima Box                                       	 3C59-1           	80	761297	  Layer packing 4x1000x2000, Prima Box                                       	1	14	 3C59-1           	27	1
1569	  Layer packing 7x1000x2000, Prima Box                                       	 3C34-2           	30	488854	  Layer packing 7x1000x2000, Prima Box                                       	1	14	 3C34-2           	27	1
1570	  CARTON Kertas Kraf  W=1600mm, Mitra Abadi                                  	 3C39-3           	0	0	  CARTON Kertas Kraf  W=1600mm, Mitra Abadi                                  	1	14	 3C39-3           	27	1
1571	  DOUBLE TAPE 48mm, BERRY TAPE                                               	 3C40-1           	71	482764	  DOUBLE TAPE 48mm, BERRY TAPE                                               	1	14	 3C40-1           	27	1
1572	  STEROFOAM F1000M 10x95x443, MENARA CI                                      	 3SF22-1          	400	115600	  STEROFOAM F1000M 10x95x443, MENARA CI                                      	1	14	 3SF22-1          	27	1
1573	  STEROFOAM F1000M 20x20x830, MENARA CI                                      	 3SF23-1          	200	46200	  STEROFOAM F1000M 20x20x830, MENARA CI                                      	1	14	 3SF23-1          	27	1
1574	  STEROFOAM F1000M 30x42x80, MENARA CI                                       	 3SF24-1          	200	14800	  STEROFOAM F1000M 30x42x80, MENARA CI                                       	1	14	 3SF24-1          	27	1
1575	  STEROFOAM F1000M 42x68x80, MENARA CI                                       	 3SF25-1          	400	63200	  STEROFOAM F1000M 42x68x80, MENARA CI                                       	1	14	 3SF25-1          	27	1
1576	  STEROFOAM F1000M 12x50x340, MENARA CI                                      	 3SF26-1          	400	56800	  STEROFOAM F1000M 12x50x340, MENARA CI                                      	1	14	 3SF26-1          	27	1
1577	  STEROFOAM F1000M 12x90x230, MENARA CI                                      	 3SF27-1          	200	34600	  STEROFOAM F1000M 12x90x230, MENARA CI                                      	1	14	 3SF27-1          	27	1
1578	  STEROFOAM F1000M 12x100x350  , MENARA CI                                   	 3SF28-1          	200	57800	  STEROFOAM F1000M 12x100x350  , MENARA CI                                   	1	14	 3SF28-1          	27	1
1579	  STEROFOAM F1000M 15x150x350, MENARA CI                                     	 3SF29-1          	200	108200	  STEROFOAM F1000M 15x150x350, MENARA CI                                     	1	14	 3SF29-1          	27	1
1580	  STEROFOAM F1000M 10x10x430, MENARA CI                                      	 3SF30-1          	400	12800	  STEROFOAM F1000M 10x10x430, MENARA CI                                      	1	14	 3SF30-1          	27	1
1581	  Sterofoam F800SB 10x75x75, MENARA  CIPTA                                   	 3SF31-1          	0	0	  Sterofoam F800SB 10x75x75, MENARA  CIPTA                                   	1	14	 3SF31-1          	27	1
1582	  Sterofoam F800SB 10x80x228, MENARA  CIPTA                                  	 3SF32-1          	0	0	  Sterofoam F800SB 10x80x228, MENARA  CIPTA                                  	1	14	 3SF32-1          	27	1
1583	  Sterofoam F800SB 15x60x480, MENARA  CIPTA                                  	 3SF33-1          	0	0	  Sterofoam F800SB 15x60x480, MENARA  CIPTA                                  	1	14	 3SF33-1          	27	1
1584	  Sterofoam F800SB 10x70x70, MENARA  CIPTA                                   	 3SF34-1          	0	0	  Sterofoam F800SB 10x70x70, MENARA  CIPTA                                   	1	14	 3SF34-1          	27	1
1585	  Sterofoam F800SB 10x55x600, MENARA  CIPTA                                  	 3SF35-1          	0	0	  Sterofoam F800SB 10x55x600, MENARA  CIPTA                                  	1	14	 3SF35-1          	27	1
1586	  Sterofoam F1000SB 10x55x800, MENARA  CIPTA                                 	 3SF36-1          	0	0	  Sterofoam F1000SB 10x55x800, MENARA  CIPTA                                 	1	14	 3SF36-1          	27	1
1587	  Sterofoam F1000SB 25x65x65, MENARA  CIPTA                                  	 3SF37-1          	0	0	  Sterofoam F1000SB 25x65x65, MENARA  CIPTA                                  	1	14	 3SF37-1          	27	1
1588	  Sterofoam F1000SB 15x85x85, MENARA  CIPTA                                  	 3SF38-1          	0	0	  Sterofoam F1000SB 15x85x85, MENARA  CIPTA                                  	1	14	 3SF38-1          	27	1
1589	  Sterofoam F1000SB 12x22x300, MENARA  CIPTA                                 	 3SF39-1          	0	0	  Sterofoam F1000SB 12x22x300, MENARA  CIPTA                                 	1	14	 3SF39-1          	27	1
1590	  SteroFoam FA800B 15x20x500, Menara Cipta Ind                               	 3SF40-1          	0	0	  SteroFoam FA800B 15x20x500, Menara Cipta Ind                               	1	14	 3SF40-1          	27	1
1591	  SteroFoam FA800B 30x76x250, Menara Cipta Ind                               	 3SF41-1          	0	0	  SteroFoam FA800B 30x76x250, Menara Cipta Ind                               	1	14	 3SF41-1          	27	1
1592	  SteroFoam FA800B 30x30x270, Menara Cipta Ind                               	 3SF42-1          	0	0	  SteroFoam FA800B 30x30x270, Menara Cipta Ind                               	1	14	 3SF42-1          	27	1
1593	  SteroFoam FA800B 12x331x340, Menara Cipta Ind                              	 3SF43-1          	0	0	  SteroFoam FA800B 12x331x340, Menara Cipta Ind                              	1	14	 3SF43-1          	27	1
1594	  SteroFoam FA800B 12x76x800, Menara Cipta Ind                               	 3SF44-1          	0	0	  SteroFoam FA800B 12x76x800, Menara Cipta Ind                               	1	14	 3SF44-1          	27	1
1595	  SteroFoam FA800B 30x70x70, Menara Cipta Ind                                	 3SF45-1          	0	0	  SteroFoam FA800B 30x70x70, Menara Cipta Ind                                	1	14	 3SF45-1          	27	1
1596	  STEROFOAM FG1500 20x100x400, MENARA CIPTA                                  	 3SF278-1         	0	0	  STEROFOAM FG1500 20x100x400, MENARA CIPTA                                  	1	14	 3SF278-1         	27	1
1597	  STEROFOAM FG1500 8x17x245, MENARA CIPTA                                    	 3SF279-1         	0	0	  STEROFOAM FG1500 8x17x245, MENARA CIPTA                                    	1	14	 3SF279-1         	27	1
1598	  STEROFOAM FG1500 34x43x290, MENARA CIPTA                                   	 3SF280-1         	0	0	  STEROFOAM FG1500 34x43x290, MENARA CIPTA                                   	1	14	 3SF280-1         	27	1
1599	  STEROFOAM FG1500 10x120x165, MENARA CIPTA                                  	 3SF281-2         	0	0	  STEROFOAM FG1500 10x120x165, MENARA CIPTA                                  	1	14	 3SF281-2         	27	1
1600	  STEROFOAM FG1500 40x95x125, MENARA CIPTA                                   	 3SF282-2         	0	0	  STEROFOAM FG1500 40x95x125, MENARA CIPTA                                   	1	14	 3SF282-2         	27	1
1601	  STEROFOAM FG1500 10x65x425, MENARA CIPTA                                   	 3SF283-2         	0	0	  STEROFOAM FG1500 10x65x425, MENARA CIPTA                                   	1	14	 3SF283-2         	27	1
1602	  STEROFOAM FG1500 10x50x140, MENARA CIPTA                                   	 3SF284-1         	0	0	  STEROFOAM FG1500 10x50x140, MENARA CIPTA                                   	1	14	 3SF284-1         	27	1
1603	  STEROFOAM FG1500 30x110x235, MENARA CIPTA                                  	 3SF285-1         	0	0	  STEROFOAM FG1500 30x110x235, MENARA CIPTA                                  	1	14	 3SF285-1         	27	1
1604	  STEROFOAM FG1500 10x355x400, MENARA CIPTA                                  	 3SF286-1         	0	0	  STEROFOAM FG1500 10x355x400, MENARA CIPTA                                  	1	14	 3SF286-1         	27	1
1605	  STEROFOAM FG1500 40x95x50, MENARA CIPTA                                    	 3SF297-2         	0	0	  STEROFOAM FG1500 40x95x50, MENARA CIPTA                                    	1	14	 3SF297-2         	27	1
1606	  STEROFOAM FG1500 15x50x120, MENARA CIPTA                                   	 3SF298-2         	0	0	  STEROFOAM FG1500 15x50x120, MENARA CIPTA                                   	1	14	 3SF298-2         	27	1
1607	  STEROFOAM FL1600 10x69x450, MENARA CIPTA                                   	 3SF234-1         	0	0	  STEROFOAM FL1600 10x69x450, MENARA CIPTA                                   	1	14	 3SF234-1         	27	1
1608	  STEROFOAM FL1600 15x87x160, MENARA CIPTA                                   	 3SF235-1         	0	0	  STEROFOAM FL1600 15x87x160, MENARA CIPTA                                   	1	14	 3SF235-1         	27	1
1609	  STEROFOAM FL1600 15x130x380, MENARA CIPTA                                  	 3SF236-1         	0	0	  STEROFOAM FL1600 15x130x380, MENARA CIPTA                                  	1	14	 3SF236-1         	27	1
1610	  STEROFOAM FL1600 30x150x300, MENARA CIPTA                                  	 3SF237-1         	0	0	  STEROFOAM FL1600 30x150x300, MENARA CIPTA                                  	1	14	 3SF237-1         	27	1
1611	  STEROFOAM FL1600 40x270x270, MENARA CIPTA                                  	 3SF238-1         	0	0	  STEROFOAM FL1600 40x270x270, MENARA CIPTA                                  	1	14	 3SF238-1         	27	1
1612	  STEROFOAM FL1600 32x32x230, MENARA CIPTA                                   	 3SF239-1         	0	0	  STEROFOAM FL1600 32x32x230, MENARA CIPTA                                   	1	14	 3SF239-1         	27	1
1613	  STEROFOAM FL1600 50x98x310, MENARA CIPTA                                   	 3SF240-1         	0	0	  STEROFOAM FL1600 50x98x310, MENARA CIPTA                                   	1	14	 3SF240-1         	27	1
1614	  STEROFOAM FL1600 58x118x330, MENARA CIPTA                                  	 3SF241-1         	0	0	  STEROFOAM FL1600 58x118x330, MENARA CIPTA                                  	1	14	 3SF241-1         	27	1
1615	  STEROFOAM FL1600 30x190x300, MENARA CIPTA                                  	 3SF242-1         	0	0	  STEROFOAM FL1600 30x190x300, MENARA CIPTA                                  	1	14	 3SF242-1         	27	1
1616	  STEROFOAM FL1600 15x18x220, MENARA CIPTA                                   	 3SF243-1         	0	0	  STEROFOAM FL1600 15x18x220, MENARA CIPTA                                   	1	14	 3SF243-1         	27	1
1617	  STEROFOAM FZ1300 65x100x160, MENARA CIPTA                                  	 3SF207-1         	0	0	  STEROFOAM FZ1300 65x100x160, MENARA CIPTA                                  	1	14	 3SF207-1         	27	1
1618	  STEROFOAM FZ1300 15x100x350, MENARA CIPTA                                  	 3SF208-1         	0	0	  STEROFOAM FZ1300 15x100x350, MENARA CIPTA                                  	1	14	 3SF208-1         	27	1
1619	  STEROFOAM FZ1300 35x105x150, MENARA CIPTA                                  	 3SF209-1         	0	0	  STEROFOAM FZ1300 35x105x150, MENARA CIPTA                                  	1	14	 3SF209-1         	27	1
1620	  STEROFOAM FZ1300 65x100x144, MENARA CIPTA                                  	 3SF210-1         	0	0	  STEROFOAM FZ1300 65x100x144, MENARA CIPTA                                  	1	14	 3SF210-1         	27	1
1621	  STEROFOAM FZ1300 65x144x220, MENARA CIPTA                                  	 3SF211-1         	0	0	  STEROFOAM FZ1300 65x144x220, MENARA CIPTA                                  	1	14	 3SF211-1         	27	1
1622	  STEROFOAM FZ1300 20x290x320, MENARA CIPTA                                  	 3SF212-1         	0	0	  STEROFOAM FZ1300 20x290x320, MENARA CIPTA                                  	1	14	 3SF212-1         	27	1
1623	  STEROFOAM FZ1300 20x80x400, MENARA CIPTA                                   	 3SF213-1         	0	0	  STEROFOAM FZ1300 20x80x400, MENARA CIPTA                                   	1	14	 3SF213-1         	27	1
1624	  STEROFOAM FZ1300 20x120x195, MENARA CIPTA                                  	 3SF214-1         	0	0	  STEROFOAM FZ1300 20x120x195, MENARA CIPTA                                  	1	14	 3SF214-1         	27	1
1625	  STEROFOAM FZ1300 10x92x480, MENARA CIPTA                                   	 3SF215-2         	0	0	  STEROFOAM FZ1300 10x92x480, MENARA CIPTA                                   	1	14	 3SF215-2         	27	1
1626	  STEROFOAM FZ1300 10x24x600, MENARA CIPTA                                   	 3SF216-1         	0	0	  STEROFOAM FZ1300 10x24x600, MENARA CIPTA                                   	1	14	 3SF216-1         	27	1
1627	  STEROFOAM FZ1300 24x35x600, MENARA CIPTA                                   	 3SF217-1         	0	0	  STEROFOAM FZ1300 24x35x600, MENARA CIPTA                                   	1	14	 3SF217-1         	27	1
1628	  STEROFOAM FZ1300 20x130x400, MENARA CIPTA                                  	 3SF218-1         	0	0	  STEROFOAM FZ1300 20x130x400, MENARA CIPTA                                  	1	14	 3SF218-1         	27	1
1629	  STEROFOAM FZ1300 15x100x400, MENARA CIPTA                                  	 3SF219-1         	0	0	  STEROFOAM FZ1300 15x100x400, MENARA CIPTA                                  	1	14	 3SF219-1         	27	1
1630	  STEROFOAM FZ1300 35x35x260, MENARA CIPTA                                   	 3SF220-1         	0	0	  STEROFOAM FZ1300 35x35x260, MENARA CIPTA                                   	1	14	 3SF220-1         	27	1
1631	  STEROFOAM FZ1300 30x55x150, MENARA CIPTA                                   	 3SF221-1         	0	0	  STEROFOAM FZ1300 30x55x150, MENARA CIPTA                                   	1	14	 3SF221-1         	27	1
1632	  STEROFOAM FZ1300 30x105x150, MENARA CIPTA                                  	 3SF222-1         	0	0	  STEROFOAM FZ1300 30x105x150, MENARA CIPTA                                  	1	14	 3SF222-1         	27	1
1633	  STEROFOAM FZ1300 15x105x150, MENARA CIPTA                                  	 3SF223-1         	0	0	  STEROFOAM FZ1300 15x105x150, MENARA CIPTA                                  	1	14	 3SF223-1         	27	1
1634	  STEROFOAM FR1200 10x102x400, MENARA CIPTA                                  	 3SF301-1         	640	195200	  STEROFOAM FR1200 10x102x400, MENARA CIPTA                                  	1	14	 3SF301-1         	27	1
1635	  STEROFOAM FR1200 15x90x250, MENARA CIPTA                                   	 3SF302-1         	640	168320	  STEROFOAM FR1200 15x90x250, MENARA CIPTA                                   	1	14	 3SF302-1         	27	1
1636	  STEROFOAM FR1200 13x15x200, MENARA CIPTA                                   	 3SF303-1         	640	20480	  STEROFOAM FR1200 13x15x200, MENARA CIPTA                                   	1	14	 3SF303-1         	27	1
1637	  STEROFOAM FR1200 35x80x180, MENARA CIPTA                                   	 3SF304-1         	640	240640	  STEROFOAM FR1200 35x80x180, MENARA CIPTA                                   	1	14	 3SF304-1         	27	1
1638	  STEROFOAM FR1200 35x140x300, MENARA CIPTA                                  	 3SF305-1         	320	369600	  STEROFOAM FR1200 35x140x300, MENARA CIPTA                                  	1	14	 3SF305-1         	27	1
1639	  STEROFOAM FR1200 22x275x300, MENARA CIPTA                                  	 3SF307-1         	320	432960	  STEROFOAM FR1200 22x275x300, MENARA CIPTA                                  	1	14	 3SF307-1         	27	1
1640	  STEROFOAM FR1200 35x105x300, MENARA CIPTA                                  	 3SF308-1         	320	263040	  STEROFOAM FR1200 35x105x300, MENARA CIPTA                                  	1	14	 3SF308-1         	27	1
1641	  STYROFOAM YFCS290B 10X36X305, KCS                                          	 3SF336-1         	0	0	  STYROFOAM YFCS290B 10X36X305, KCS                                          	1	14	 3SF336-1         	27	1
1642	  STYROFOAM YFCS290B 18X208X300, KCS                                         	 3SF336-2         	3050	3400750	  STYROFOAM YFCS290B 18X208X300, KCS                                         	1	14	 3SF336-2         	27	1
1643	  STYROFOAM YFCS290B 18X120X300, KCS                                         	 3SF336-3         	3050	3546750	  STYROFOAM YFCS290B 18X120X300, KCS                                         	1	14	 3SF336-3         	27	1
1644	  STYROFOAM YFCS290B 10X36X315, KCS                                          	 3SF336-4         	6100	1342000	  STYROFOAM YFCS290B 10X36X315, KCS                                          	1	14	 3SF336-4         	27	1
1645	  STYROFOAM YFO1180 : 10X93X400, MENARA CIPTA                                	 3SF328-1         	600	162000	  STYROFOAM YFO1180 : 10X93X400, MENARA CIPTA                                	1	14	 3SF328-1         	27	1
1646	  STYROFOAM YFO1180 : 20x18x190, MENARA CIPTA                                	 3SF328-2         	600	33000	  STYROFOAM YFO1180 : 20x18x190, MENARA CIPTA                                	1	14	 3SF328-2         	27	1
1647	  STYROFOAM YFO1180 : 15x87x350, MENARA CIPTA                                	 3SF328-3         	300	133500	  STYROFOAM YFO1180 : 15x87x350, MENARA CIPTA                                	1	14	 3SF328-3         	27	1
1648	  STYROFOAM YFO1180 : 18x50x350, MENARA CIPTA                                	 3SF328-5         	300	73500	  STYROFOAM YFO1180 : 18x50x350, MENARA CIPTA                                	1	14	 3SF328-5         	27	1
1649	  STYROFOAM YFO1180 : 15x15x200, MENARA CIPTA                                	 3SF328-6         	300	18000	  STYROFOAM YFO1180 : 15x15x200, MENARA CIPTA                                	1	14	 3SF328-6         	27	1
1650	  STYROFOAM YFO1180 : 15x60x300, MENARA CIPTA                                	 3SF328-7         	300	76500	  STYROFOAM YFO1180 : 15x60x300, MENARA CIPTA                                	1	14	 3SF328-7         	27	1
1651	  STYROFOAM YFO1180 : 45x45x580, MENARA CIPTA                                	 3SF328-8         	300	304500	  STYROFOAM YFO1180 : 45x45x580, MENARA CIPTA                                	1	14	 3SF328-8         	27	1
1652	  STYROFOAM YFO1180 : 15x50x140, MENARA CIPTA                                	 3SF328-10        	300	30000	  STYROFOAM YFO1180 : 15x50x140, MENARA CIPTA                                	1	14	 3SF328-10        	27	1
1653	  STYROFOAM YFO1180 : 12x150x535, MENARA CIPTA                               	 3SF328-11        	300	256500	  STYROFOAM YFO1180 : 12x150x535, MENARA CIPTA                               	1	14	 3SF328-11        	27	1
1654	  STYROFOAM YFO1180 : 20x45x300, MENARA CIPTA                                	 3SF328-12        	300	94500	  STYROFOAM YFO1180 : 20x45x300, MENARA CIPTA                                	1	14	 3SF328-12        	27	1
1655	  STYROFOAM YFO1180 : 15x33x300, MENARA CIPTA                                	 3SF328-13        	300	63000	  STYROFOAM YFO1180 : 15x33x300, MENARA CIPTA                                	1	14	 3SF328-13        	27	1
1656	  STYROFOAM YFX1600HG 10X114X400, KCS                                        	 3SF334-1         	0	0	  STYROFOAM YFX1600HG 10X114X400, KCS                                        	1	14	 3SF334-1         	27	1
1657	  STYROFOAM YFX1600HG 30X77X1000, KCS                                        	 3SF334-2         	0	0	  STYROFOAM YFX1600HG 30X77X1000, KCS                                        	1	14	 3SF334-2         	27	1
1658	  STYROFOAM YFX1600HG 77X80X300, KCS                                         	 3SF334-3         	0	0	  STYROFOAM YFX1600HG 77X80X300, KCS                                         	1	14	 3SF334-3         	27	1
1659	  STYROFOAM YFX1600HG 52X138X195, KCS                                        	 3SF334-4         	0	0	  STYROFOAM YFX1600HG 52X138X195, KCS                                        	1	14	 3SF334-4         	27	1
1660	  STYROFOAM YFX1600HG 15X52X100, KCS                                         	 3SF334-5         	0	0	  STYROFOAM YFX1600HG 15X52X100, KCS                                         	1	14	 3SF334-5         	27	1
1661	  STYROFOAM YFX1600HG 24X150X400, KCS                                        	 3SF334-6         	0	0	  STYROFOAM YFX1600HG 24X150X400, KCS                                        	1	14	 3SF334-6         	27	1
1662	  STYROFOAM YFX1600HG 10X133X300, KCS                                        	 3SF334-7         	0	0	  STYROFOAM YFX1600HG 10X133X300, KCS                                        	1	14	 3SF334-7         	27	1
1663	  STYROFOAM YFX1600HG 30X130X138, KCS                                        	 3SF334-8         	0	0	  STYROFOAM YFX1600HG 30X130X138, KCS                                        	1	14	 3SF334-8         	27	1
1664	  STYROFOAM YFX1600HG 30X125X150, KCS                                        	 3SF334-9         	0	0	  STYROFOAM YFX1600HG 30X125X150, KCS                                        	1	14	 3SF334-9         	27	1
1665	  STYROFOAM YFX1600HG 15X30X150, KCS                                         	 3SF334-10        	0	0	  STYROFOAM YFX1600HG 15X30X150, KCS                                         	1	14	 3SF334-10        	27	1
1666	  STYROFOAM YFX1600HG 47X180X400, KCS                                        	 3SF334-11        	0	0	  STYROFOAM YFX1600HG 47X180X400, KCS                                        	1	14	 3SF334-11        	27	1
1667	  STYROFOAM YFX1600HG2 10X40X400, KCS                                        	 3SF334-12        	0	0	  STYROFOAM YFX1600HG2 10X40X400, KCS                                        	1	14	 3SF334-12        	27	1
1668	  STYROFOAM YFX1600HG2 18X250X350, KCS                                       	 3SF334-13        	0	0	  STYROFOAM YFX1600HG2 18X250X350, KCS                                       	1	14	 3SF334-13        	27	1
1669	  STYROFOAM ITOKI W1200H-P1 10X272X462, KCS                                  	 3SI7-1           	20	24900	  STYROFOAM ITOKI W1200H-P1 10X272X462, KCS                                  	1	14	 3SI7-1           	27	1
1670	  STYROFOAM ITOKI W1200H-P1 37X58X1168, KCS                                  	 3SI7-2           	10	32300	  STYROFOAM ITOKI W1200H-P1 37X58X1168, KCS                                  	1	14	 3SI7-2           	27	1
1671	  STYROFOAM ITOKI W1200H-P1 37X140X445, KCS                                  	 3SI7-3           	10	29700	  STYROFOAM ITOKI W1200H-P1 37X140X445, KCS                                  	1	14	 3SI7-3           	27	1
1672	  STYROFOAM ITOKI W1200H-P1 76X110X460, KCS                                  	 3SI7-4           	20	99000	  STYROFOAM ITOKI W1200H-P1 76X110X460, KCS                                  	1	14	 3SI7-4           	27	1
1673	  STYROFOAM ITOKI W1200H-P1 117X148X160, KCS                                 	 3SI7-5           	10	35700	  STYROFOAM ITOKI W1200H-P1 117X148X160, KCS                                 	1	14	 3SI7-5           	27	1
1674	  STYROFOAM ITOKI W1200H-P1 52X105X350, KCS                                  	 3SI7-6           	10	24600	  STYROFOAM ITOKI W1200H-P1 52X105X350, KCS                                  	1	14	 3SI7-6           	27	1
1675	  STYROFOAM ITOKI W1200H-P1 10X335X480, KCS                                  	 3SI7-7           	10	15950	  STYROFOAM ITOKI W1200H-P1 10X335X480, KCS                                  	1	14	 3SI7-7           	27	1
1676	  STYROFOAM ITOKI W1200H-P1 20X320X370, KCS                                  	 3SI7-8           	10	23450	  STYROFOAM ITOKI W1200H-P1 20X320X370, KCS                                  	1	14	 3SI7-8           	27	1
1677	  STYROFOAM ITOKI W1200H-P1 20X155X290, KCS                                  	 3SI7-9           	10	8900	  STYROFOAM ITOKI W1200H-P1 20X155X290, KCS                                  	1	14	 3SI7-9           	27	1
1678	  STYROFOAM ITOKI W1200H-P1 60X60X450, KCS                                   	 3SI7-10          	30	62550	  STYROFOAM ITOKI W1200H-P1 60X60X450, KCS                                   	1	14	 3SI7-10          	27	1
1679	  STYROFOAM ITOKI W1200H-P1 20X405X445, KCS                                  	 3SI7-11          	10	35700	  STYROFOAM ITOKI W1200H-P1 20X405X445, KCS                                  	1	14	 3SI7-11          	27	1
1680	  STYROFOAM ITOKI W1200H-P1 45X50X440, KCS                                   	 3SI7-12          	10	12750	  STYROFOAM ITOKI W1200H-P1 45X50X440, KCS                                   	1	14	 3SI7-12          	27	1
1681	  STYROFOAM ITOKI W1200H-P1 40X65X400, KCS                                   	 3SI7-13          	10	13400	  STYROFOAM ITOKI W1200H-P1 40X65X400, KCS                                   	1	14	 3SI7-13          	27	1
1682	  STYROFOAM ITOKI W1200H-P1 15X58X440, MCI                                   	 3SI7-14          	20	8200	  STYROFOAM ITOKI W1200H-P1 15X58X440, MCI                                   	1	14	 3SI7-14          	27	1
1683	  STYROFOAM ITOKI W1200H-P1 15X35X300, MCI                                   	 3SI7-15          	10	3011	  STYROFOAM ITOKI W1200H-P1 15X35X300, MCI                                   	1	14	 3SI7-15          	27	1
1684	  STYROFOAM ITOKI W1200H-P1 20X30X350, MCI                                   	 3SI7-16          	10	1650	  STYROFOAM ITOKI W1200H-P1 20X30X350, MCI                                   	1	14	 3SI7-16          	27	1
1685	  STYROFOAM ITOKI W1200H-P1 30X35X350, MCI                                   	 3SI7-17          	10	3250	  STYROFOAM ITOKI W1200H-P1 30X35X350, MCI                                   	1	14	 3SI7-17          	27	1
1686	  STYROFOAM ITOKI W1200H-P2 15X91X192, MCI                                   	 3SI8-6           	20	6200	  STYROFOAM ITOKI W1200H-P2 15X91X192, MCI                                   	1	14	 3SI8-6           	27	1
1687	  STYROFOAM ITOKI W1200H-P2 15X91X218, MCI                                   	 3SI8-7           	20	6800	  STYROFOAM ITOKI W1200H-P2 15X91X218, MCI                                   	1	14	 3SI8-7           	27	1
1688	  STYROFOAM ITOKI W1200H-P2 15X54X350, MCI                                   	 3SI8-8           	10	3250	  STYROFOAM ITOKI W1200H-P2 15X54X350, MCI                                   	1	14	 3SI8-8           	27	1
1689	  STYROFOAM ITOKI W1200H-P2 10X332X463, KCS                                  	 3SI8-1           	20	30500	  STYROFOAM ITOKI W1200H-P2 10X332X463, KCS                                  	1	14	 3SI8-1           	27	1
1690	  STYROFOAM ITOKI W1200H-P2 50X73X510, KCS                                   	 3SI8-2           	10	24000	  STYROFOAM ITOKI W1200H-P2 50X73X510, KCS                                   	1	14	 3SI8-2           	27	1
1691	  STYROFOAM ITOKI W1200H-P2 35X112X518, KCS                                  	 3SI8-3           	10	26150	  STYROFOAM ITOKI W1200H-P2 35X112X518, KCS                                  	1	14	 3SI8-3           	27	1
1692	  STYROFOAM ITOKI W1200H-P2 24X216X311, KCS                                  	 3SI8-4           	20	32000	  STYROFOAM ITOKI W1200H-P2 24X216X311, KCS                                  	1	14	 3SI8-4           	27	1
1693	  STYROFOAM ITOKI W1200H-P2 21X331X548, KCS                                  	 3SI8-5           	10	37750	  STYROFOAM ITOKI W1200H-P2 21X331X548, KCS                                  	1	14	 3SI8-5           	27	1
1694	  STYROFOAM ITOKI W1200LOW-P1 10X159X465, KCS                                	 3SI9-1           	20	14700	  STYROFOAM ITOKI W1200LOW-P1 10X159X465, KCS                                	1	14	 3SI9-1           	27	1
1695	  STYROFOAM ITOKI W1200LOW-P1 37X58X1168, KCS                                	 3SI9-2           	10	32300	  STYROFOAM ITOKI W1200LOW-P1 37X58X1168, KCS                                	1	14	 3SI9-2           	27	1
1696	  STYROFOAM ITOKI W1200LOW-P1 64X74X335, KCS                                 	 3SI9-3           	10	20450	  STYROFOAM ITOKI W1200LOW-P1 64X74X335, KCS                                 	1	14	 3SI9-3           	27	1
1697	  STYROFOAM ITOKI W1200LOW-P1 46X72X460, KCS                                 	 3SI9-4           	10	19650	  STYROFOAM ITOKI W1200LOW-P1 46X72X460, KCS                                 	1	14	 3SI9-4           	27	1
1698	  STYROFOAM ITOKI W1200LOW-P1 32X130X400, KCS                                	 3SI9-5           	10	21450	  STYROFOAM ITOKI W1200LOW-P1 32X130X400, KCS                                	1	14	 3SI9-5           	27	1
1699	  STYROFOAM ITOKI W1200LOW-P1 74X130X423, KCS                                	 3SI9-6           	10	52400	  STYROFOAM ITOKI W1200LOW-P1 74X130X423, KCS                                	1	14	 3SI9-6           	27	1
1700	  STYROFOAM ITOKI W1200LOW-P1 30X280X355, KCS                                	 3SI9-7           	10	29550	  STYROFOAM ITOKI W1200LOW-P1 30X280X355, KCS                                	1	14	 3SI9-7           	27	1
1701	  STYROFOAM ITOKI W1200LOW-P1 10X370X355, KCS                                	 3SI9-8           	10	13000	  STYROFOAM ITOKI W1200LOW-P1 10X370X355, KCS                                	1	14	 3SI9-8           	27	1
1702	  STYROFOAM ITOKI W1200LOW-P1 18X64X150, MCI                                 	 3SI9-9           	10	1650	  STYROFOAM ITOKI W1200LOW-P1 18X64X150, MCI                                 	1	14	 3SI9-9           	27	1
1703	  STYROFOAM ITOKI W1200LOW-P1 32X50X400, MCI                                 	 3SI9-10          	20	10400	  STYROFOAM ITOKI W1200LOW-P1 32X50X400, MCI                                 	1	14	 3SI9-10          	27	1
1704	  STYROFOAM ITOKI W1200LOW-P1 15X90X150, MCI                                 	 3SI9-11          	10	2100	  STYROFOAM ITOKI W1200LOW-P1 15X90X150, MCI                                 	1	14	 3SI9-11          	27	1
1705	  STYROFOAM ITOKI W1200LOW-P1 10X74X120, MCI                                 	 3SI9-12          	10	920	  STYROFOAM ITOKI W1200LOW-P1 10X74X120, MCI                                 	1	14	 3SI9-12          	27	1
1706	  STYROFOAM ITOKI W1200LOW-P1 22X72X460, MCI                                 	 3SI9-13          	10	8500	  STYROFOAM ITOKI W1200LOW-P1 22X72X460, MCI                                 	1	14	 3SI9-13          	27	1
1707	  STYROFOAM ITOKI W1200LOW-P1 15X58X440, MCI                                 	 3SI1-2           	20	8200	  STYROFOAM ITOKI W1200LOW-P1 15X58X440, MCI                                 	1	14	 3SI1-2           	27	1
1708	  STYROFOAM ITOKI W1200LOW-P1 15X64X172, MCI                                 	 3SI1-10          	10	1950	  STYROFOAM ITOKI W1200LOW-P1 15X64X172, MCI                                 	1	14	 3SI1-10          	27	1
1709	  STYROFOAM ITOKI W1200LOW-P2 15X36X198, MCI                                 	 3SI2-2           	20	2500	  STYROFOAM ITOKI W1200LOW-P2 15X36X198, MCI                                 	1	14	 3SI2-2           	27	1
1710	  STYROFOAM ITOKI W1200LOW-P2 15X36X175, MCI                                 	 3SI2-3           	20	2200	  STYROFOAM ITOKI W1200LOW-P2 15X36X175, MCI                                 	1	14	 3SI2-3           	27	1
1711	  STYROFOAM ITOKI W1200LOW-P2 18X45X510, MCI                                 	 3SI2-4           	10	3950	  STYROFOAM ITOKI W1200LOW-P2 18X45X510, MCI                                 	1	14	 3SI2-4           	27	1
1712	  STYROFOAM ITOKI W1200LOW-P2 10X171X403, KCS                                	 3SI10-1          	20	13700	  STYROFOAM ITOKI W1200LOW-P2 10X171X403, KCS                                	1	14	 3SI10-1          	27	1
1713	  STYROFOAM ITOKI W1200LOW-P2 18X240X315, KCS                                	 3SI10-2          	10	13500	  STYROFOAM ITOKI W1200LOW-P2 18X240X315, KCS                                	1	14	 3SI10-2          	27	1
1714	  STYROFOAM ITOKI W1200LOW-P2 19X248X325, KCS                                	 3SI10-3          	10	15200	  STYROFOAM ITOKI W1200LOW-P2 19X248X325, KCS                                	1	14	 3SI10-3          	27	1
1715	  STYROFOAM ITOKI W1200LOW-P2 48X131X128, KCS                                	 3SI10-4          	10	10400	  STYROFOAM ITOKI W1200LOW-P2 48X131X128, KCS                                	1	14	 3SI10-4          	27	1
1716	  STYROFOAM ITOKI W1200LOW-P2 23X75X490, KCS                                 	 3SI10-5          	10	10900	  STYROFOAM ITOKI W1200LOW-P2 23X75X490, KCS                                 	1	14	 3SI10-5          	27	1
1717	  STYROFOAM ITOKI W1200LOW-P2 22X95X518, KCS                                 	 3SI10-6          	10	13950	  STYROFOAM ITOKI W1200LOW-P2 22X95X518, KCS                                 	1	14	 3SI10-6          	27	1
1718	  STYROFOAM ITOKI W1200LOW-P2 14X40X290, MCI                                 	 3SI2-5           	10	1800	  STYROFOAM ITOKI W1200LOW-P2 14X40X290, MCI                                 	1	14	 3SI2-5           	27	1
1719	  STYROFOAM ITOKI W1200LOW-P2 14X48X518, MCI                                 	 3SI2-12          	10	4000	  STYROFOAM ITOKI W1200LOW-P2 14X48X518, MCI                                 	1	14	 3SI2-12          	27	1
1720	  STYROFOAM ITOKI W1200LOW-P2 19X88X335, MCI                                 	 3SI2-13          	20	9500	  STYROFOAM ITOKI W1200LOW-P2 19X88X335, MCI                                 	1	14	 3SI2-13          	27	1
1721	  STYROFOAM ITOKI W1500-P1 10X144X462, KCS                                   	 3SI11-1          	20	17200	  STYROFOAM ITOKI W1500-P1 10X144X462, KCS                                   	1	14	 3SI11-1          	27	1
1722	  STYROFOAM ITOKI W1500-P1 37X58X1468, KCS                                   	 3SI11-2          	10	40550	  STYROFOAM ITOKI W1500-P1 37X58X1468, KCS                                   	1	14	 3SI11-2          	27	1
1723	  STYROFOAM ITOKI W1500-P1 56X97X340, KCS                                    	 3SI11-3          	10	23800	  STYROFOAM ITOKI W1500-P1 56X97X340, KCS                                    	1	14	 3SI11-3          	27	1
1724	  STYROFOAM ITOKI W1500-P1 27X56X835, KCS                                    	 3SI11-4          	10	16250	  STYROFOAM ITOKI W1500-P1 27X56X835, KCS                                    	1	14	 3SI11-4          	27	1
1725	  STYROFOAM ITOKI W1500-P1 42X58X335, KCS                                    	 3SI11-5          	10	10500	  STYROFOAM ITOKI W1500-P1 42X58X335, KCS                                    	1	14	 3SI11-5          	27	1
1726	  STYROFOAM ITOKI W1500-P1 15X180X405, KCS                                   	 3SI11-6          	10	10850	  STYROFOAM ITOKI W1500-P1 15X180X405, KCS                                   	1	14	 3SI11-6          	27	1
1727	  STYROFOAM ITOKI W1500-P1 34X355X428, KCS                                   	 3SI11-7          	10	51150	  STYROFOAM ITOKI W1500-P1 34X355X428, KCS                                   	1	14	 3SI11-7          	27	1
1728	  STYROFOAM ITOKI W1500-P1 32X355X398, KCS                                   	 3SI11-8          	10	44800	  STYROFOAM ITOKI W1500-P1 32X355X398, KCS                                   	1	14	 3SI11-8          	27	1
1729	  STYROFOAM ITOKI W1500-P1 15X30X160, MCI                                    	 3SI11-9          	20	1642	  STYROFOAM ITOKI W1500-P1 15X30X160, MCI                                    	1	14	 3SI11-9          	27	1
1730	  STYROFOAM ITOKI W1500-P1 17X23X330, MCI                                    	 3SI11-10         	10	1550	  STYROFOAM ITOKI W1500-P1 17X23X330, MCI                                    	1	14	 3SI11-10         	27	1
1731	  STYROFOAM ITOKI W1500-P1 22X58X335, MCI                                    	 3SI11-11         	10	4750	  STYROFOAM ITOKI W1500-P1 22X58X335, MCI                                    	1	14	 3SI11-11         	27	1
1732	  STYROFOAM ITOKI W1500-P1 15X58X440, MCI                                    	 3SI5-2           	20	8200	  STYROFOAM ITOKI W1500-P1 15X58X440, MCI                                    	1	14	 3SI5-2           	27	1
1733	  STYROFOAM ITOKI W1500-P2 15X36X198, MCI                                    	 3SI6-2           	20	2500	  STYROFOAM ITOKI W1500-P2 15X36X198, MCI                                    	1	14	 3SI6-2           	27	1
1734	  STYROFOAM ITOKI W1500-P2 10X151X403, MCI                                   	 3SI12-7          	20	10100	  STYROFOAM ITOKI W1500-P2 10X151X403, MCI                                   	1	14	 3SI12-7          	27	1
1735	  STYROFOAM ITOKI W1500-P2 15X36X175, MCI                                    	 3SI6-3           	20	2200	  STYROFOAM ITOKI W1500-P2 15X36X175, MCI                                    	1	14	 3SI6-3           	27	1
1736	  STYROFOAM ITOKI W1500-P2 14X22X290, MCI                                    	 3SI6-5           	20	2700	  STYROFOAM ITOKI W1500-P2 14X22X290, MCI                                    	1	14	 3SI6-5           	27	1
1737	  STYROFOAM ITOKI W1500-P2 19X88X343, MCI                                    	 3SI12-8          	20	9700	  STYROFOAM ITOKI W1500-P2 19X88X343, MCI                                    	1	14	 3SI12-8          	27	1
1738	  STYROFOAM ITOKI W1500-P2 18X45X810, KCS                                    	 3SI12-1          	10	8450	  STYROFOAM ITOKI W1500-P2 18X45X810, KCS                                    	1	14	 3SI12-1          	27	1
1739	  STYROFOAM ITOKI W1500-P2 18X243X314, KCS                                   	 3SI12-2          	10	13600	  STYROFOAM ITOKI W1500-P2 18X243X314, KCS                                   	1	14	 3SI12-2          	27	1
1740	  STYROFOAM ITOKI W1500-P2 48X111X428, KCS                                   	 3SI12-3          	10	29350	  STYROFOAM ITOKI W1500-P2 48X111X428, KCS                                   	1	14	 3SI12-3          	27	1
1741	  STYROFOAM ITOKI W1500-P2 14X48X818, KCS                                    	 3SI12-4          	10	7100	  STYROFOAM ITOKI W1500-P2 14X48X818, KCS                                    	1	14	 3SI12-4          	27	1
1742	  STYROFOAM ITOKI W1500-P2 23X75X818, KCS                                    	 3SI12-5          	10	18200	  STYROFOAM ITOKI W1500-P2 23X75X818, KCS                                    	1	14	 3SI12-5          	27	1
1743	  STYROFOAM ITOKI W1500-P2 22X95X818, KCS                                    	 3SI12-6          	10	22000	  STYROFOAM ITOKI W1500-P2 22X95X818, KCS                                    	1	14	 3SI12-6          	27	1
1744	  STYROFOAM DK-WK-AV-01 48X100X125 ANANTARA VILLA, KCS                       	 3S175-1          	0	0	  STYROFOAM DK-WK-AV-01 48X100X125 ANANTARA VILLA, KCS                       	1	14	 3S175-1          	27	1
1745	  STYROFOAM DK-WK-AV-02 48X66X100 ANANTARA VILLA, KCS                        	 3S175-2          	0	0	  STYROFOAM DK-WK-AV-02 48X66X100 ANANTARA VILLA, KCS                        	1	14	 3S175-2          	27	1
1746	  STYROFOAM DK-WK-AV-03 26X26X150 ANANTARA VILLA, KCS                        	 3S175-3          	0	0	  STYROFOAM DK-WK-AV-03 26X26X150 ANANTARA VILLA, KCS                        	1	14	 3S175-3          	27	1
1747	  STYROFOAM DK-WK-AV-04 26X12X150 ANANTARA VILLA, KCS                        	 3S175-4          	0	0	  STYROFOAM DK-WK-AV-04 26X12X150 ANANTARA VILLA, KCS                        	1	14	 3S175-4          	27	1
1748	  STYROFOAM DK-MK-AV-01 18X50X100 ANANTARA VILLA, KCS                        	 3S175-5          	0	0	  STYROFOAM DK-MK-AV-01 18X50X100 ANANTARA VILLA, KCS                        	1	14	 3S175-5          	27	1
1749	  STYROFOAM DK-MK-AV-02 15X100X150 ANANTARA VILLA, KCS                       	 3S175-6          	0	0	  STYROFOAM DK-MK-AV-02 15X100X150 ANANTARA VILLA, KCS                       	1	14	 3S175-6          	27	1
1750	  STYROFOAM DK-WK-AH-01 48X100X150 ANANTARA HOTEL, KCS                       	 3S176-1          	0	0	  STYROFOAM DK-WK-AH-01 48X100X150 ANANTARA HOTEL, KCS                       	1	14	 3S176-1          	27	1
1751	  STYROFOAM DK-WK-AH-02 53X66X100 ANANTARA HOTEL, KCS                        	 3S176-2          	0	0	  STYROFOAM DK-WK-AH-02 53X66X100 ANANTARA HOTEL, KCS                        	1	14	 3S176-2          	27	1
1752	  STYROFOAM DK-WK-AH-03 26X40X150 ANANTARA HOTEL, KCS                        	 3S176-3          	0	0	  STYROFOAM DK-WK-AH-03 26X40X150 ANANTARA HOTEL, KCS                        	1	14	 3S176-3          	27	1
1753	  STYROFOAM DK-WK-AH-04 12X40X150 ANANTARA HOTEL, KCS                        	 3S176-4          	0	0	  STYROFOAM DK-WK-AH-04 12X40X150 ANANTARA HOTEL, KCS                        	1	14	 3S176-4          	27	1
1754	  STYROFOAM DK-MK-AH-01 18X50X100 ANANTARA HOTEL, KCS                        	 3S176-5          	0	0	  STYROFOAM DK-MK-AH-01 18X50X100 ANANTARA HOTEL, KCS                        	1	14	 3S176-5          	27	1
1755	  Sterofoam MARUNI 800 New 18x22x220, Menara Cipta I                         	 3SM6-1           	110	12183	  Sterofoam MARUNI 800 New 18x22x220, Menara Cipta I                         	1	14	 3SM6-1           	27	1
1756	  STEROFOAM FUKAI KITCHEN 14x66x800, MENARA C                                	 3SFK3-1          	14	11397	  STEROFOAM FUKAI KITCHEN 14x66x800, MENARA C                                	1	14	 3SFK3-1          	27	1
1757	  STEROFOAM FUKAI KITCHEN 15x40x460, MENARA C                                	 3SFK4-1          	25	5026	  STEROFOAM FUKAI KITCHEN 15x40x460, MENARA C                                	1	14	 3SFK4-1          	27	1
1758	  STEROFOAM FUKAI KITCHEN 18x25x1280, MENARA C                               	 3SFK5-1          	29	11969	  STEROFOAM FUKAI KITCHEN 18x25x1280, MENARA C                               	1	14	 3SFK5-1          	27	1
1759	  STEROFOAM FUKAI KITCHEN 18x40x135, MENARA C                                	 3SFK7-1          	0	0	  STEROFOAM FUKAI KITCHEN 18x40x135, MENARA C                                	1	14	 3SFK7-1          	27	1
1760	  STEROFOAM FUKAI KITCHEN 19x105x145, MENARA C                               	 3SFK8-1          	106	22174	  STEROFOAM FUKAI KITCHEN 19x105x145, MENARA C                               	1	14	 3SFK8-1          	27	1
1761	  STEROFOAM FUKAI KITCHEN 20x80x400, MENARA C                                	 3SFK9-1          	34	15523	  STEROFOAM FUKAI KITCHEN 20x80x400, MENARA C                                	1	14	 3SFK9-1          	27	1
1762	  STEROFOAM FUKAI KITCHEN 20x20x100, MENARA C                                	 3SFK10-1         	0	0	  STEROFOAM FUKAI KITCHEN 20x20x100, MENARA C                                	1	14	 3SFK10-1         	27	1
1763	  STEROFOAM FUKAI KITCHEN 20x72x530, KCS                                     	 3SFK11-2         	78	71143	  STEROFOAM FUKAI KITCHEN 20x72x530, KCS                                     	1	14	 3SFK11-2         	27	1
1764	  STEROFOAM FUKAI KITCHEN 20x92x530, KCS                                     	 3SFK12-2         	78	90541	  STEROFOAM FUKAI KITCHEN 20x92x530, KCS                                     	1	14	 3SFK12-2         	27	1
1765	  STEROFOAM FUKAI KITCHEN 20x96x530, KCS                                     	 3SFK13-2         	78	94531	  STEROFOAM FUKAI KITCHEN 20x96x530, KCS                                     	1	14	 3SFK13-2         	27	1
1766	  STEROFOAM FUKAI KITCHEN 20x112x314, KCS                                    	 3SFK14-2         	78	65273	  STEROFOAM FUKAI KITCHEN 20x112x314, KCS                                    	1	14	 3SFK14-2         	27	1
1767	  STEROFOAM FUKAI KITCHEN 20x30x525, KCS                                     	 3SFK15-2         	68	26451	  STEROFOAM FUKAI KITCHEN 20x30x525, KCS                                     	1	14	 3SFK15-2         	27	1
1768	  STEROFOAM FUKAI KITCHEN 20x144x450, MENARA C                               	 3SFK16-1         	29	27112	  STEROFOAM FUKAI KITCHEN 20x144x450, MENARA C                               	1	14	 3SFK16-1         	27	1
1769	  STEROFOAM FUKAI KITCHEN 20x124x860, MENARA C                               	 3SFK18-1         	47	73488	  STEROFOAM FUKAI KITCHEN 20x124x860, MENARA C                               	1	14	 3SFK18-1         	27	1
1770	  STEROFOAM FUKAI KITCHEN 21x40x555, KCS                                     	 3SFK19-2         	88	48353	  STEROFOAM FUKAI KITCHEN 21x40x555, KCS                                     	1	14	 3SFK19-2         	27	1
1771	  STEROFOAM FUKAI KITCHEN 21x79x580, MENARA C                                	 3SFK21-1         	20	13903	  STEROFOAM FUKAI KITCHEN 21x79x580, MENARA C                                	1	14	 3SFK21-1         	27	1
1772	  STEROFOAM FUKAI KITCHEN 21x50x100, MENARA C                                	 3SFK22-1         	30	2268	  STEROFOAM FUKAI KITCHEN 21x50x100, MENARA C                                	1	14	 3SFK22-1         	27	1
1773	  STEROFOAM FUKAI KITCHEN 21x58x240, MENARA C                                	 3SFK23-1         	30	6314	  STEROFOAM FUKAI KITCHEN 21x58x240, MENARA C                                	1	14	 3SFK23-1         	27	1
1774	  STEROFOAM FUKAI KITCHEN 21x37x490, MENARA C                                	 3SFK24-1         	30	8521	  STEROFOAM FUKAI KITCHEN 21x37x490, MENARA C                                	1	14	 3SFK24-1         	27	1
1775	  STEROFOAM FUKAI KITCHEN 21x30x100, MENARA C                                	 3SFK25-1         	30	1594	  STEROFOAM FUKAI KITCHEN 21x30x100, MENARA C                                	1	14	 3SFK25-1         	27	1
1776	  STEROFOAM FUKAI KITCHEN 24x85x1700, MENARA C                               	 3SFK27-1         	57	137084	  STEROFOAM FUKAI KITCHEN 24x85x1700, MENARA C                               	1	14	 3SFK27-1         	27	1
1777	  STEROFOAM FUKAI KITCHEN 20x60x610, MENARA C                                	 3SFK32-2         	48	26690	  STEROFOAM FUKAI KITCHEN 20x60x610, MENARA C                                	1	14	 3SFK32-2         	27	1
1778	  STEROFOAM FUKAI KITCHEN 40x22x445, MENARA C                                	 3SFK38-1         	19	6118	  STEROFOAM FUKAI KITCHEN 40x22x445, MENARA C                                	1	14	 3SFK38-1         	27	1
1779	  STEROFOAM FUKAI KITCHEN 40x143x845, MENARA C                               	 3SFK34-1         	84	272536	  STEROFOAM FUKAI KITCHEN 40x143x845, MENARA C                               	1	14	 3SFK34-1         	27	1
1780	  Paper Craft 230 x 1000, SURYA CITRA UTAMA MANDIRI                          	 8P174-1          	500	0	  Paper Craft 230 x 1000, SURYA CITRA UTAMA MANDIRI                          	1	14	 8P174-1          	27	1
1781	  Paper Craft 250 x 1000, SURYA CITRA                                        	 8P32-1           	2500	0	  Paper Craft 250 x 1000, SURYA CITRA                                        	1	14	 8P32-1           	27	1
1782	  Paper Craft  260 x 1000, Surya Citra                                       	 8P92-1           	1000	0	  Paper Craft  260 x 1000, Surya Citra                                       	1	14	 8P92-1           	27	1
1783	  Paper Craft 280 x 800, SURYA CITRA UTAMA MANDIRI                           	 8P167-1          	500	0	  Paper Craft 280 x 800, SURYA CITRA UTAMA MANDIRI                           	1	14	 8P167-1          	27	1
1784	  Paper Craft 290 x 750, SURYA CITRA UTAMA MANDIRI                           	 8P176-1          	0	0	  Paper Craft 290 x 750, SURYA CITRA UTAMA MANDIRI                           	1	14	 8P176-1          	27	1
1785	  Paper Craft 299 x 400, SURYA CITRA UTAMA MANDIRI                           	 8P172-1          	0	0	  Paper Craft 299 x 400, SURYA CITRA UTAMA MANDIRI                           	1	14	 8P172-1          	27	1
1786	  Paper Craft 299 x 568, SURYA CITRA UTAMA MANDIRI                           	 8P173-1          	0	0	  Paper Craft 299 x 568, SURYA CITRA UTAMA MANDIRI                           	1	14	 8P173-1          	27	1
1787	  Paper Craft 300 x 300, SURYA CITRA UTAMA MANDIRI                           	 8P160-1          	500	35000	  Paper Craft 300 x 300, SURYA CITRA UTAMA MANDIRI                           	1	14	 8P160-1          	27	1
1788	  Paper Craft 300 x 363, SURYA CITRA UTAMA MANDIRI                           	 8P161-1          	500	52500	  Paper Craft 300 x 363, SURYA CITRA UTAMA MANDIRI                           	1	14	 8P161-1          	27	1
1789	  Paper Craft 300 x 450, SURYA CITRA                                         	 8P1-2            	1000	98740	  Paper Craft 300 x 450, SURYA CITRA                                         	1	14	 8P1-2            	27	1
1790	  Paper Craft  300 x 750, Surya Citra                                        	 8P113-1          	13000	2210000	  Paper Craft  300 x 750, Surya Citra                                        	1	14	 8P113-1          	27	1
1791	  Paper Craft  300 x 870, Surya Citra                                        	 8P152-1          	1000	210000	  Paper Craft  300 x 870, Surya Citra                                        	1	14	 8P152-1          	27	1
1792	  Paper Craft 300 x 1000, Surya Citra                                        	 8P69-1           	1500	315000	  Paper Craft 300 x 1000, Surya Citra                                        	1	14	 8P69-1           	27	1
1793	  Paper Craft 310 x 350, SURYA CITRA UTAMA MANDIRI                           	 8P164-1          	250	26250	  Paper Craft 310 x 350, SURYA CITRA UTAMA MANDIRI                           	1	14	 8P164-1          	27	1
1794	  Paper Craft  310 x 370, Surya Citra                                        	 8P141-1          	250	26250	  Paper Craft  310 x 370, Surya Citra                                        	1	14	 8P141-1          	27	1
1795	  Paper Craft 310 x 830, Surya P&S                                           	 8P64-1           	1500	315000	  Paper Craft 310 x 830, Surya P&S                                           	1	14	 8P64-1           	27	1
1796	  Paper Craft 320 x 380, Surya Citra                                         	 8P144-1          	1000	105000	  Paper Craft 320 x 380, Surya Citra                                         	1	14	 8P144-1          	27	1
1797	  Paper Craft  320 x 650, Surya Citra                                        	 8P145-1          	2250	315000	  Paper Craft  320 x 650, Surya Citra                                        	1	14	 8P145-1          	27	1
1798	  Paper Craft  320 x 750, Surya Citra                                        	 8P146-1          	1000	210000	  Paper Craft  320 x 750, Surya Citra                                        	1	14	 8P146-1          	27	1
1799	  Paper Craft  320 x 850, Surya Citra                                        	 8P148-1          	500	105000	  Paper Craft  320 x 850, Surya Citra                                        	1	14	 8P148-1          	27	1
1800	  Paper Craft  330 x 350, Surya Citra                                        	 8P115-3          	500	70000	  Paper Craft  330 x 350, Surya Citra                                        	1	14	 8P115-3          	27	1
1801	  Paper Craft  330 x 450, Surya Citra                                        	 8P115-1          	750	105000	  Paper Craft  330 x 450, Surya Citra                                        	1	14	 8P115-1          	27	1
1802	  Paper Craft 350 x 600, Surya citra                                         	 8P3-3            	500	210000	  Paper Craft 350 x 600, Surya citra                                         	1	14	 8P3-3            	27	1
1803	  Paper Craft  350 x 950, Surya Citra                                        	 8P140-1          	500	85000	  Paper Craft  350 x 950, Surya Citra                                        	1	14	 8P140-1          	27	1
1804	  Paper Craft 358 x 620, SURYA CITRA UTAMA MANDIRI                           	 8P165-1          	0	0	  Paper Craft 358 x 620, SURYA CITRA UTAMA MANDIRI                           	1	14	 8P165-1          	27	1
1805	  Paper Craft 370 x 470, Surya Citra                                         	 8P34-2           	0	0	  Paper Craft 370 x 470, Surya Citra                                         	1	14	 8P34-2           	27	1
1806	  Paper Craft  370 x 525, Surya Citra                                        	 8P4-2            	1000	190000	  Paper Craft  370 x 525, Surya Citra                                        	1	14	 8P4-2            	27	1
1807	  Paper Craft 380 x 580, Surya citra                                         	 8P119-1          	1000	170000	  Paper Craft 380 x 580, Surya citra                                         	1	14	 8P119-1          	27	1
1808	  Paper Craft  380 x 1000, Surya Citra                                       	 8P82-1           	500	210000	  Paper Craft  380 x 1000, Surya Citra                                       	1	14	 8P82-1           	27	1
1809	  Paper Craft  390 x 600, Surya Citra                                        	 8P90-1           	500	105000	  Paper Craft  390 x 600, Surya Citra                                        	1	14	 8P90-1           	27	1
1810	  Paper Craft  390 x 650, Surya Citra                                        	 8P147-1          	2000	420000	  Paper Craft  390 x 650, Surya Citra                                        	1	14	 8P147-1          	27	1
1811	  Paper Craft  390 x 680, Surya Citra                                        	 8P91-1           	250	85000	  Paper Craft  390 x 680, Surya Citra                                        	1	14	 8P91-1           	27	1
1812	  Paper Craft  390 x 750, Surya Citra                                        	 8P149-1          	250	105000	  Paper Craft  390 x 750, Surya Citra                                        	1	14	 8P149-1          	27	1
1813	  Paper Craft  390 x 850, Surya Citra                                        	 8P150-1          	500	170000	  Paper Craft  390 x 850, Surya Citra                                        	1	14	 8P150-1          	27	1
1814	  Paper Craft 390 x 900, SURYA CITRA UTAMA MANDIRI                           	 8P169-1          	1000	420000	  Paper Craft 390 x 900, SURYA CITRA UTAMA MANDIRI                           	1	14	 8P169-1          	27	1
1815	  Paper Craft  400 x 450, Surya Citra                                        	 8P158-1          	2000	413643	  Paper Craft  400 x 450, Surya Citra                                        	1	14	 8P158-1          	27	1
1816	  Paper Craft  400 x 500, Surya Citra                                        	 8P35-1           	500	105000	  Paper Craft  400 x 500, Surya Citra                                        	1	14	 8P35-1           	27	1
1817	  Paper Craft 400 x 620, SURYA CITRA UTAMA MANDIRI                           	 8P170-1          	0	0	  Paper Craft 400 x 620, SURYA CITRA UTAMA MANDIRI                           	1	14	 8P170-1          	27	1
1818	  Paper Craft 400 x 800, Surya Citra                                         	 8P65-1           	500	170000	  Paper Craft 400 x 800, Surya Citra                                         	1	14	 8P65-1           	27	1
1819	  Paper Craft 400 x 810, Surya Citra                                         	 8P71-1           	500	190000	  Paper Craft 400 x 810, Surya Citra                                         	1	14	 8P71-1           	27	1
1820	  Paper CD 400 x 900, SURYA CITRA                                            	 8P151-1          	6000	2519826	  Paper CD 400 x 900, SURYA CITRA                                            	1	14	 8P151-1          	27	1
1821	  Paper CD 400 x 1000, SURYA CITRA                                           	 8P31-1           	500	210000	  Paper CD 400 x 1000, SURYA CITRA                                           	1	14	 8P31-1           	27	1
1822	  Paper Craft  420 x 475, Surya Citra                                        	 8P117-1          	500	93889	  Paper Craft  420 x 475, Surya Citra                                        	1	14	 8P117-1          	27	1
1823	  Paper Craft  420 x 480, SURYA CITRA                                        	 8P133-1          	1000	210000	  Paper Craft  420 x 480, SURYA CITRA                                        	1	14	 8P133-1          	27	1
1824	  Paper Craft 420 x 650, Surya Citra                                         	 8P129-1          	2500	0	  Paper Craft 420 x 650, Surya Citra                                         	1	14	 8P129-1          	27	1
1825	  Paper Craft 420 x 900, SURYA CITRA UTAMA MANDIRI                           	 8P175-1          	500	210000	  Paper Craft 420 x 900, SURYA CITRA UTAMA MANDIRI                           	1	14	 8P175-1          	27	1
1826	  Paper Craft 440 x 610, Surya Citra                                         	 8P95-3           	500	0	  Paper Craft 440 x 610, Surya Citra                                         	1	14	 8P95-3           	27	1
1827	  Paper Craft 450 x 600, Surya Citra                                         	 8P81-1           	1500	380000	  Paper Craft 450 x 600, Surya Citra                                         	1	14	 8P81-1           	27	1
1828	  Paper Craft  450 x 750, Surya Citra                                        	 8P139-1          	1000	400000	  Paper Craft  450 x 750, Surya Citra                                        	1	14	 8P139-1          	27	1
1829	  Paper Craft  450 x 830, SURYA CITRA                                        	 8P7-2            	500	210000	  Paper Craft  450 x 830, SURYA CITRA                                        	1	14	 8P7-2            	27	1
1830	  Paper Craft  450 x 850, Surya Citra                                        	 8P143-1          	0	0	  Paper Craft  450 x 850, Surya Citra                                        	1	14	 8P143-1          	27	1
1831	  Paper Craft  FK 450 x 860, Surya P&S                                       	 8P37-1           	500	205264	  Paper Craft  FK 450 x 860, Surya P&S                                       	1	14	 8P37-1           	27	1
1832	  Paper Craft 450 x 910, Surya Citra                                         	 8P74-1           	500	210000	  Paper Craft 450 x 910, Surya Citra                                         	1	14	 8P74-1           	27	1
1833	  Paper Craft  450 x 1000, SURYA CITRA                                       	 8P8-2            	1000	420000	  Paper Craft  450 x 1000, SURYA CITRA                                       	1	14	 8P8-2            	27	1
1834	  Paper Craft  460 x 630, Surya Citra                                        	 8P84-1           	1000	839586	  Paper Craft  460 x 630, Surya Citra                                        	1	14	 8P84-1           	27	1
1835	  Paper Craft  FK 465 x 600, Surya Citra                                     	 8P60-1           	1500	380000	  Paper Craft  FK 465 x 600, Surya Citra                                     	1	14	 8P60-1           	27	1
1836	  Paper Craft 465 x 800, SURYA CITRA                                         	 8P111-1          	1000	357185	  Paper Craft 465 x 800, SURYA CITRA                                         	1	14	 8P111-1          	27	1
1837	  Paper Craft 465 x 1000, SURYA CITRA                                        	 8P112-1          	0	0	  Paper Craft 465 x 1000, SURYA CITRA                                        	1	14	 8P112-1          	27	1
1838	  Paper Craft  FK 500 x 580, Surya P&S                                       	 8P38-1           	500	105000	  Paper Craft  FK 500 x 580, Surya P&S                                       	1	14	 8P38-1           	27	1
1839	  Paper Craft  500 x 810, SURYA CITRA                                        	 8P10-2           	500	190000	  Paper Craft  500 x 810, SURYA CITRA                                        	1	14	 8P10-2           	27	1
1840	  Paper Craft  FK 500 x 850, Surya P&S                                       	 8P42-1           	500	210000	  Paper Craft  FK 500 x 850, Surya P&S                                       	1	14	 8P42-1           	27	1
1841	  Paper Craft  FK 580 x 650, Surya P&S                                       	 8P39-1           	500	209948	  Paper Craft  FK 580 x 650, Surya P&S                                       	1	14	 8P39-1           	27	1
1842	  Paper Craft  FK 580 x 850, Surya P&S                                       	 8P47-1           	500	210000	  Paper Craft  FK 580 x 850, Surya P&S                                       	1	14	 8P47-1           	27	1
1843	  Paper Craft 610 x 860, Surya Citra                                         	 8P106-2          	250	100994	  Paper Craft 610 x 860, Surya Citra                                         	1	14	 8P106-2          	27	1
1844	  Paper Craft  FK 650 x 860, Surya Citra                                     	 8P51-2           	1000	403974	  Paper Craft  FK 650 x 860, Surya Citra                                     	1	14	 8P51-2           	27	1
1845	  Paper Craft  FK 650 x 1000, Surya P&S                                      	 8P52-1           	1250	523938	  Paper Craft  FK 650 x 1000, Surya P&S                                      	1	14	 8P52-1           	27	1
1846	  Adjuster  F.KULKAS M8x80, TOKYO K                                          	 8A24-1           	2576	0	  Adjuster  F.KULKAS M8x80, TOKYO K                                          	1	1	 8A24-1           	27	1
1847	  Adjuster F.KULKAS (set), TOKYO K                                           	 8A22-1           	1134	0	  Adjuster F.KULKAS (set), TOKYO K                                           	1	1	 8A22-1           	27	1
1848	  Adjuster Foot  F.KULKAS, TOKYO K                                           	 8A23-1           	3650	0	  Adjuster Foot  F.KULKAS, TOKYO K                                           	1	1	 8A23-1           	27	1
1849	  Earth Qudke Mat F.Kulkas, TOKYO K                                          	 8E1-1            	1383	25041263	  Earth Qudke Mat F.Kulkas, TOKYO K                                          	1	1	 8E1-1            	27	1
1850	  Earth Quake Lacht Set F.Kulkas, MARUFUJI K K                               	 8E2-1            	1966	30877459	  Earth Quake Lacht Set F.Kulkas, MARUFUJI K K                               	1	1	 8E2-1            	27	1
1851	  HINGE SET A  F.KULKAS, TOKYO K                                             	 8H23-1           	328	0	  HINGE SET A  F.KULKAS, TOKYO K                                             	1	1	 8H23-1           	27	1
1852	  Handle F.KULKAS 513C-64, TOKYO K                                           	 8H22-1           	835	5841488	  Handle F.KULKAS 513C-64, TOKYO K                                           	1	1	 8H22-1           	27	1
1853	  Nylon Collor F.Kulkas Ø19, TOKYO K                                         	 8N1-1            	3650	4436791	  Nylon Collor F.Kulkas Ø19, TOKYO K                                         	1	1	 8N1-1            	27	1
1854	  Non Slip F.Kulkas Sheet White 2x200x100, TOKYO K                           	 8N2-1            	3655	24746049	  Non Slip F.Kulkas Sheet White 2x200x100, TOKYO K                           	1	1	 8N2-1            	27	1
1855	  Caster Roller FUKAI , Tokyo K                                              	 8C30-1           	3140	22670674	  Caster Roller FUKAI , Tokyo K                                              	1	1	 8C30-1           	27	1
1856	  Caster Tray FUKAI CTR110-01, TOKYO K                                       	 8C25-1           	5252	4145036	  Caster Tray FUKAI CTR110-01, TOKYO K                                       	1	1	 8C25-1           	27	1
1857	  Caster Tray FUKAI CTR100(3pcs/set), Tokyo K                                	 8C6-1            	7636	3982285	  Caster Tray FUKAI CTR100(3pcs/set), Tokyo K                                	1	1	 8C6-1            	27	1
1858	  Caster Assy FUKAI CA213-01, TOKYO K                                        	 8C24-1           	6272	37355033	  Caster Assy FUKAI CA213-01, TOKYO K                                        	1	1	 8C24-1           	27	1
1859	  Caster Assy FUKAI CA222-01, Tokyo M                                        	 8C1-1            	95	0	  Caster Assy FUKAI CA222-01, Tokyo M                                        	1	1	 8C1-1            	27	1
1860	  Caster Assy FUKAI CA231-01(5pcs/set), Tokyo K                              	 8C2-1            	21794	85655590	  Caster Assy FUKAI CA231-01(5pcs/set), Tokyo K                              	1	1	 8C2-1            	27	1
1861	  Caster Assy FUKAI CA206-015, Tokyo K                                       	 8C27-1           	39	226380	  Caster Assy FUKAI CA206-015, Tokyo K                                       	1	1	 8C27-1           	27	1
1862	  Caster Assy FUKAI CA160-01B( FS890+FS1170 ), A&S T+Tokyo K                 	 8C3-1            	4	18451	  Caster Assy FUKAI CA160-01B( FS890+FS1170 ), A&S T+Tokyo K                 	1	1	 8C3-1            	27	1
1863	  Caster Holder FUKAI CH161-01, Tokyo M                                      	 8C5-1            	8451	0	  Caster Holder FUKAI CH161-01, Tokyo M                                      	1	1	 8C5-1            	27	1
1864	  Caster Holder FUKAI CH150-03(4pcs/set), Tokyo K                            	 8C4-1            	14954	9167054	  Caster Holder FUKAI CH150-03(4pcs/set), Tokyo K                            	1	1	 8C4-1            	27	1
1865	  Draw Run Self FUKAI 300Crem CL, Hafele Indotama                            	 8D1-1            	20	0	  Draw Run Self FUKAI 300Crem CL, Hafele Indotama                            	1	1	 8D1-1            	27	1
1866	  Draw Run Self FUKAI 300Crem CR, Hafele Indotama                            	 8D1-2            	20	0	  Draw Run Self FUKAI 300Crem CR, Hafele Indotama                            	1	1	 8D1-2            	27	1
1867	  Draw Run Self FUKAI 300Crem DR, Hafele Indotama                            	 8D1-3            	20	0	  Draw Run Self FUKAI 300Crem DR, Hafele Indotama                            	1	1	 8D1-3            	27	1
1868	  Draw Run Self FUKAI 300Crem DL, Hafele Indotama                            	 8D1-4            	20	0	  Draw Run Self FUKAI 300Crem DL, Hafele Indotama                            	1	1	 8D1-4            	27	1
1869	  FELT FUKAI, TOKYO K                                                        	 8F1-1            	3500	4611889	  FELT FUKAI, TOKYO K                                                        	1	1	 8F1-1            	27	1
1870	  FELT FUKAI (13x50x1), TOKYO K                                              	 8F3-1            	38098	50201069	  FELT FUKAI (13x50x1), TOKYO K                                              	1	1	 8F3-1            	27	1
1871	  Glass HINGE FUKAI Assy AL185-01(R), TOKYO K                                	 8G3-1            	411	20981693	  Glass HINGE FUKAI Assy AL185-01(R), TOKYO K                                	1	1	 8G3-1            	27	1
1872	  Glass HINGE FUKAI Assy AL185-01(L), TOKYO K                                	 8G3-2            	411	20981693	  Glass HINGE FUKAI Assy AL185-01(L), TOKYO K                                	1	1	 8G3-2            	27	1
1873	  HINGE SET A FUKAI , TOKYO K                                                	 8H15-1           	1405	9440566	  HINGE SET A FUKAI , TOKYO K                                                	1	1	 8H15-1           	27	1
1874	  Hinge Set FUKAI HSet 120W-01N, A&S Trade                                   	 8H2-1            	2938	42999280	  Hinge Set FUKAI HSet 120W-01N, A&S Trade                                   	1	1	 8H2-1            	27	1
1875	  Hinge Set FUKAI HSet 120W-01B (No Plate), TOKYO K                          	 8H26-1           	4003	49269986	  Hinge Set FUKAI HSet 120W-01B (No Plate), TOKYO K                          	1	1	 8H26-1           	27	1
1876	  Hinge Bracket FUKAI ATH0069(Z) Black, NH Teknik                            	 8H4-1            	123	0	  Hinge Bracket FUKAI ATH0069(Z) Black, NH Teknik                            	1	1	 8H4-1            	27	1
1877	  Hinge Bracket FUKAI F830/F1000 "S", PT.Koko                                	 8H5-1            	3340	4942623	  Hinge Bracket FUKAI F830/F1000 "S", PT.Koko                                	1	1	 8H5-1            	27	1
1878	  Handle FUKAI  513C-64, TOKYO KOHSAKOSHO                                    	 8H19-1           	2312	16622511	  Handle FUKAI  513C-64, TOKYO KOHSAKOSHO                                    	1	1	 8H19-1           	27	1
1879	  Handle FUKAI FL L=335, TOKYO KOHSAKOSHO                                    	 8H18-1           	642	2210689	  Handle FUKAI FL L=335, TOKYO KOHSAKOSHO                                    	1	1	 8H18-1           	27	1
1880	  Handle FUKAI  ETC-100, MARUFUJI KK                                         	 8H14-1           	444	22594184	  Handle FUKAI  ETC-100, MARUFUJI KK                                         	1	1	 8H14-1           	27	1
1881	  Handle FUKAI FMD Chrpol Black 107-93.231, MARUFUJI KK                      	 8H8-2            	550	70077996	  Handle FUKAI FMD Chrpol Black 107-93.231, MARUFUJI KK                      	1	1	 8H8-2            	27	1
1882	  Handle Silver FUKAI FSV890/1000,  TOKYO KOHSAKUSHO                         	 8H7-3            	2366	37031238	  Handle Silver FUKAI FSV890/1000,  TOKYO KOHSAKUSHO                         	1	1	 8H7-3            	27	1
1883	  Handle Black FUKAI HHun34.1026-64 FS890/1170W, Makmur                      	 8H8-1            	447	28159317	  Handle Black FUKAI HHun34.1026-64 FS890/1170W, Makmur                      	1	1	 8H8-1            	27	1
1884	  L Bracket FUKAI S093-90L, A&S Trade+Tokyo                                  	 8L1-1            	6095	9299811	  L Bracket FUKAI S093-90L, A&S Trade+Tokyo                                  	1	1	 8L1-1            	27	1
1885	  Low Caster FUKAI, Tokyo M                                                  	 8L7-1            	799	9145052	  Low Caster FUKAI, Tokyo M                                                  	1	1	 8L7-1            	27	1
1886	  Magnet FUKAI TK270W-01, A&S Trade+Tokyo                                    	 8M1-1            	3674	21372810	  Magnet FUKAI TK270W-01, A&S Trade+Tokyo                                    	1	1	 8M1-1            	27	1
1887	  Magnet FUKAI TK270S-01N, A&S Trade+Tokyo                                   	 8M3-1            	3274	13610116	  Magnet FUKAI TK270S-01N, A&S Trade+Tokyo                                   	1	1	 8M3-1            	27	1
1888	  Magnet Base FUKAI TK270W-01, A&S Trade                                     	 8M2-1            	2281	4027821	  Magnet Base FUKAI TK270W-01, A&S Trade                                     	1	1	 8M2-1            	27	1
1889	  Magnet Base FUKAI TK270SP-08, A&S Trade+Tokyo                              	 8M4-1            	3304	5224282	  Magnet Base FUKAI TK270SP-08, A&S Trade+Tokyo                              	1	1	 8M4-1            	27	1
1890	  Magnet  FUKAI  CATCH 834S, TOKYO K                                         	 8M11-1           	3650	14190132	  Magnet  FUKAI  CATCH 834S, TOKYO K                                         	1	1	 8M11-1           	27	1
1891	  Magnet  FUKAI  CATCH Screw M3x11, TOKYO K                                  	 8M12-2           	7189	2796392	  Magnet  FUKAI  CATCH Screw M3x11, TOKYO K                                  	1	1	 8M12-2           	27	1
1892	  Magnet  FUKAI  CATCH Screw M3x10, TOKYO K                                  	 8M12-3           	6884	1791412	  Magnet  FUKAI  CATCH Screw M3x10, TOKYO K                                  	1	1	 8M12-3           	27	1
1893	  O Namento Mouru FUKAI L819 F830DX, A&S Trade                               	 8O3-1            	268	0	  O Namento Mouru FUKAI L819 F830DX, A&S Trade                               	1	1	 8O3-1            	27	1
1894	  O Namento Mouru FUKAI L989 F1000DX, A&S Trade                              	 8O4-1            	435	0	  O Namento Mouru FUKAI L989 F1000DX, A&S Trade                              	1	1	 8O4-1            	27	1
1895	  O Namento Mouru FUKAI L=1600 FMB, A&S Trade                                	 8O7-1            	190	3187034	  O Namento Mouru FUKAI L=1600 FMB, A&S Trade                                	1	1	 8O7-1            	27	1
1896	  O Namento Mouru FUKAI L=1507 FMD1500, TOKYO K                              	 8O12-1           	185	3132341	  O Namento Mouru FUKAI L=1507 FMD1500, TOKYO K                              	1	1	 8O12-1           	27	1
1897	  O Namento Mouru FUKAI L=1707 FMD1700, TOKYO K                              	 8O13-1           	1178	22176930	  O Namento Mouru FUKAI L=1707 FMD1700, TOKYO K                              	1	1	 8O13-1           	27	1
1898	  PIN Sliding door FUKAI , TOKYO K                                           	 8P177-1          	3140	1648776	  PIN Sliding door FUKAI , TOKYO K                                           	1	1	 8P177-1          	27	1
1899	  Rubber Cushion FUKAI Clear banbon SJ1020C Ø10mm(288/lbr), Atom/Andalira    	 8R1-1            	6372	9415147	  Rubber Cushion FUKAI Clear banbon SJ1020C Ø10mm(288/lbr), Atom/Andalira    	1	1	 8R1-1            	27	1
1900	  REL LACI FUKAI ELCO 30Cm  BLACK, Sumber Selamat                            	 8R5-1            	870	8589667	  REL LACI FUKAI ELCO 30Cm  BLACK, Sumber Selamat                            	1	1	 8R5-1            	34	1
1901	  REL LACI / Draw Run Self  FUKAI ELCO 30Cm  BEIGE, Sumber Selamat           	 8R2-1            	1759	16862626	  REL LACI / Draw Run Self  FUKAI ELCO 30Cm  BEIGE, Sumber Selamat           	1	1	 8R2-1            	34	1
1902	  Rail FUKAI Sliding Door Atas CF12-6 L:1200 , TOKYO K                       	 8R6-1            	1570	22670674	  Rail FUKAI Sliding Door Atas CF12-6 L:1200 , TOKYO K                       	1	1	 8R6-1            	27	1
1903	  Rail FUKAI Sliding Door Bawah FU12-055 L:1200 , TOKYO K                    	 8R7-1            	1570	16487763	  Rail FUKAI Sliding Door Bawah FU12-055 L:1200 , TOKYO K                    	1	1	 8R7-1            	27	1
1904	  Support Pipe FUKAI 20x20x810, NH karya teknik                              	 8S10-1           	8	0	  Support Pipe FUKAI 20x20x810, NH karya teknik                              	1	1	 8S10-1           	27	1
1905	  Slide Rail  FUKAI  L:250, TOKYO K                                          	 8S74-1           	616	39938220	  Slide Rail  FUKAI  L:250, TOKYO K                                          	1	1	 8S74-1           	27	1
1906	  Slide Rail Plastik  FUKAI, TOKYO K                                         	 8S66-1           	2325	0	  Slide Rail Plastik  FUKAI, TOKYO K                                         	1	1	 8S66-1           	27	1
1907	  Support Pipe FUKAI 15x15x806, Triyuda                                      	 8S11-1           	37	0	  Support Pipe FUKAI 15x15x806, Triyuda                                      	1	1	 8S11-1           	27	1
1908	  Support Pipe FUKAI 1.4x15x706x15, Sunjaya                                  	 8S12-1           	29	0	  Support Pipe FUKAI 1.4x15x706x15, Sunjaya                                  	1	1	 8S12-1           	27	1
1909	  Support Pipe FUKAI F830PB 15x15x536, Sunjaya                               	 8S13-1           	1	0	  Support Pipe FUKAI F830PB 15x15x536, Sunjaya                               	1	1	 8S13-1           	27	1
1910	  Support Pipe FUKAI 15x15x890, NH karya teknik                              	 8S9-1            	10	153614	  Support Pipe FUKAI 15x15x890, NH karya teknik                              	1	1	 8S9-1            	27	1
1911	  Slide Rail FUKAI KS12-W-300 FS1000/1170 DR, A&S Trade                      	 8S17-1           	780	0	  Slide Rail FUKAI KS12-W-300 FS1000/1170 DR, A&S Trade                      	1	1	 8S17-1           	27	1
1912	  Slide Rail FUKAI KS12-W-300 FS1000/1170 DL, A&S Trade                      	 8S17-2           	725	0	  Slide Rail FUKAI KS12-W-300 FS1000/1170 DL, A&S Trade                      	1	1	 8S17-2           	27	1
1913	  Slide Rail FUKAI KS12-W-300 FS1000/1170 CL, A&S Trade                      	 8S17-3           	790	0	  Slide Rail FUKAI KS12-W-300 FS1000/1170 CL, A&S Trade                      	1	1	 8S17-3           	27	1
1914	  Slide Rail FUKAI KS12-W-300 FS1000/1170 CR, A&S Trade                      	 8S17-4           	796	0	  Slide Rail FUKAI KS12-W-300 FS1000/1170 CR, A&S Trade                      	1	1	 8S17-4           	27	1
1915	  Tana PIN A FUKAI  7x20, Tokyo K                                            	 8T3-1            	6209	8119817	  Tana PIN A FUKAI  7x20, Tokyo K                                            	1	1	 8T3-1            	27	1
1916	  Tana PIN B FUKAI  5x17, Tokyo K                                            	 8T4-1            	6180	6869710	  Tana PIN B FUKAI  5x17, Tokyo K                                            	1	1	 8T4-1            	27	1
1917	  Tana PIN HOLDER FUKAI  HH160, Tokyo K                                      	 8T5-1            	7204	15173834	  Tana PIN HOLDER FUKAI  HH160, Tokyo K                                      	1	1	 8T5-1            	27	1
1918	  Tsumami Handle FUKAI KT301M B-CR KNOB, Marufuji KK                         	 8T1-2            	94	1414733	  Tsumami Handle FUKAI KT301M B-CR KNOB, Marufuji KK                         	1	1	 8T1-2            	27	1
1919	  Tsumami Handle FUKAI 19x19mm KCH.001.0145, TOKYO KK                        	 8T1-3            	549	8262640	  Tsumami Handle FUKAI 19x19mm KCH.001.0145, TOKYO KK                        	1	1	 8T1-3            	27	1
1920	  Hinge DAIKEN CLOUK CENTER 2(PB)B Black,  Marufuji KK                       	 7H1-2            	27655	255249621	  Hinge DAIKEN CLOUK CENTER 2(PB)B Black,  Marufuji KK                       	1	1	 7H1-2            	27	1
1921	  Guide Roller Set (Pipot) DAIKEN CLOUK DKR-0.5,  Marufuji KK                	 7G1-1            	2851	67889855	  Guide Roller Set (Pipot) DAIKEN CLOUK DKR-0.5,  Marufuji KK                	1	1	 7G1-1            	27	1
1922	  Key DAIKEN DOOR LF51-2,  Koike Imates                                      	 7K2-1            	928	36403323	  Key DAIKEN DOOR LF51-2,  Koike Imates                                      	1	1	 7K2-1            	27	1
1923	  Plastic Decorative Pipe DAIKEN DOOR MS-55x2400mm, Koike Imates             	 7P8-2            	1995	38501589	  Plastic Decorative Pipe DAIKEN DOOR MS-55x2400mm, Koike Imates             	1	1	 7P8-2            	27	1
1924	  CYOUBAN NUT No.1 DAIKEN LOKAL,  Marufuji KK                                	 7C2-1            	9870	0	  CYOUBAN NUT No.1 DAIKEN LOKAL,  Marufuji KK                                	1	1	 7C2-1            	27	1
1925	  CYOUBAN NUT No.2 DAIKEN LOKAL, Marufuji KK                                 	 7C3-1            	10037	0	  CYOUBAN NUT No.2 DAIKEN LOKAL, Marufuji KK                                 	1	1	 7C3-1            	27	1
1926	  Draw Run Self NISSEN CLWH 350MM CR, HAFELE                                 	 7D15-1           	35	0	  Draw Run Self NISSEN CLWH 350MM CR, HAFELE                                 	1	1	 7D15-1           	27	1
1927	  Draw Run Self NISSEN CLWH 350MM DR, HAFELE                                 	 7D15-2           	35	0	  Draw Run Self NISSEN CLWH 350MM DR, HAFELE                                 	1	1	 7D15-2           	27	1
1928	  Draw Run Self NISSEN CLWH 350MM CL, HAFELE                                 	 7D15-3           	35	0	  Draw Run Self NISSEN CLWH 350MM CL, HAFELE                                 	1	1	 7D15-3           	27	1
1929	  Draw Run Self NISSEN CLWH 350MM DL, HAFELE                                 	 7D15-4           	35	0	  Draw Run Self NISSEN CLWH 350MM DL, HAFELE                                 	1	1	 7D15-4           	27	1
1930	  Adjuster D.KANTANA , Koike Imatec                                          	 8A19-1           	3807	154912394	  Adjuster D.KANTANA , Koike Imatec                                          	1	1	 8A19-1           	27	1
1931	  L Bracket SIKU  D.KANTANA , Koike Imatec                                   	 8L3-2            	11960	61163876	  L Bracket SIKU  D.KANTANA , Koike Imatec                                   	1	1	 8L3-2            	27	1
1932	  L Bracket  D.KANTANA , Koike Imatec                                        	 8L3-1            	17921	77519032	  L Bracket  D.KANTANA , Koike Imatec                                        	1	1	 8L3-1            	27	1
1933	  Prevention D.KANTANA Cross Piece L220, Koike Imatec                        	 8P93-1           	25274	133140754	  Prevention D.KANTANA Cross Piece L220, Koike Imatec                        	1	1	 8P93-1           	27	1
1934	  Prevention D.KANTANA Cross Piece L370, Koike Imatec                        	 8P94-1           	8559	61843190	  Prevention D.KANTANA Cross Piece L370, Koike Imatec                        	1	1	 8P94-1           	27	1
1935	  R verbinder NISSEN KST WE155(Rear panel conector), HAFELE                  	 7R4-1            	4000	0	  R verbinder NISSEN KST WE155(Rear panel conector), HAFELE                  	1	1	 7R4-1            	27	1
1936	  Foot  Ø43/42.5 x 250mm SHOES BOX CB, KlampayanSunjaya                      	 7F3-1            	1	0	  Foot  Ø43/42.5 x 250mm SHOES BOX CB, KlampayanSunjaya                      	1	1	 7F3-1            	27	1
1937	  Foot  Ø42.5 x 250mm SHOES BOX MC, Meranti  merah Parta Wood                	 7F4-1            	293	0	  Foot  Ø42.5 x 250mm SHOES BOX MC, Meranti  merah Parta Wood                	1	1	 7F4-1            	27	1
1938	  Foot  Ø42.5 x 250mm SHOES BOX CB, Sunjaya                                  	 7F5-1            	33	0	  Foot  Ø42.5 x 250mm SHOES BOX CB, Sunjaya                                  	1	1	 7F5-1            	27	1
1939	  Foot  Ø42.5 x 250mm SHOES BOX SM, Meranti  merah Parta Wood                	 7F6-1            	30	0	  Foot  Ø42.5 x 250mm SHOES BOX SM, Meranti  merah Parta Wood                	1	1	 7F6-1            	27	1
1940	  Foot  Ø42.5 x 250mm SHOES BOX SM, Sunjaya                                  	 7F6-2            	140	0	  Foot  Ø42.5 x 250mm SHOES BOX SM, Sunjaya                                  	1	1	 7F6-2            	27	1
1941	  Foot  Ø42.5 x 250mm SHOES BOX PM, Sunjaya                                  	 7F7-2            	160	0	  Foot  Ø42.5 x 250mm SHOES BOX PM, Sunjaya                                  	1	1	 7F7-2            	27	1
1942	  Foot Ø 42.5 x 250mm SHOES BOX PW, Sunjaya                                  	 7F9-1            	13	361437	  Foot Ø 42.5 x 250mm SHOES BOX PW, Sunjaya                                  	1	1	 7F9-1            	27	1
1943	  Handle SHOES BOX 0422S Silver,  Atom                                       	 7H3-1            	464	0	  Handle SHOES BOX 0422S Silver,  Atom                                       	1	1	 7H3-1            	27	1
1944	  Handle SHOES BOX 0422B Black,  Saitama                                     	 7H4-1            	2518	0	  Handle SHOES BOX 0422B Black,  Saitama                                     	1	1	 7H4-1            	27	1
1945	  Hinge Set SHOES BOX ER100-18-GER BK, Marufuji KK                           	 7H2-1            	26	0	  Hinge Set SHOES BOX ER100-18-GER BK, Marufuji KK                           	1	1	 7H2-1            	27	1
1946	  Hinge Bkt SHOES BOX ERBK30, Atom+Marufuji KK                               	 7H2-2            	5585	0	  Hinge Bkt SHOES BOX ERBK30, Atom+Marufuji KK                               	1	1	 7H2-2            	27	1
1947	  STAY STANDART STORAGE BOX  (LEI.GCOD-K276N), Koike I                       	 7S69-1           	4169	378688273	  STAY STANDART STORAGE BOX  (LEI.GCOD-K276N), Koike I                       	1	1	 7S69-1           	27	1
1948	  Clear Banbon SHOES BOX MSB 2-10 HITAM,  Marufuji KK                        	 7C4-1            	13621	4195118	  Clear Banbon SHOES BOX MSB 2-10 HITAM,  Marufuji KK                        	1	1	 7C4-1            	27	1
1949	  Handle White Set SHOES BOX 08896P,  Marufuji KK                            	 7H6-1            	125	0	  Handle White Set SHOES BOX 08896P,  Marufuji KK                            	1	1	 7H6-1            	27	1
1950	  Plat U SHOES BOX 1x9x12x635, NH Karya T                                    	 7P11-1           	4300	0	  Plat U SHOES BOX 1x9x12x635, NH Karya T                                    	1	1	 7P11-1           	27	1
1951	  Hinge Set STORAGE BOX U200-U7 H2, UWW                                      	 7H7-1            	6293	0	  Hinge Set STORAGE BOX U200-U7 H2, UWW                                      	1	1	 7H7-1            	27	1
1952	  Hinge (Gwt ) STORAGE BOX, A&S Trade                                        	 7H7-2            	6000	0	  Hinge (Gwt ) STORAGE BOX, A&S Trade                                        	1	1	 7H7-2            	27	1
1953	  Mini Hinge SD+Washer STORAGE BOX, Marufuji KK                              	 7M2-1            	2106	0	  Mini Hinge SD+Washer STORAGE BOX, Marufuji KK                              	1	1	 7M2-1            	27	1
1954	  Rubber Cushion STORAGE BOX, HAFELE                                         	 7R3-1            	7000	1804893	  Rubber Cushion STORAGE BOX, HAFELE                                         	1	1	 7R3-1            	27	1
1955	  Earthquake Resistant  DAIWA RAKUDA TAISHI LATCH, Marufuji KK               	 7E2-1            	256	0	  Earthquake Resistant  DAIWA RAKUDA TAISHI LATCH, Marufuji KK               	1	1	 7E2-1            	27	1
1956	  Earthquake Resistant Latch Holder DAIWA RAKUDA, Marufuji KK                	 7E3-1            	256	0	  Earthquake Resistant Latch Holder DAIWA RAKUDA, Marufuji KK                	1	1	 7E3-1            	27	1
1957	  PIPA SS304 DAIWA RAKUDA 1"x1mmx6m, Tk Langgeng                             	 7P18-1           	3	363582	  PIPA SS304 DAIWA RAKUDA 1"x1mmx6m, Tk Langgeng                             	1	1	 7P18-1           	27	1
1958	  Stainless SUS Socket DAIWA RAKUDA white, Marufuji KK                       	 7S38-1           	210	2554325	  Stainless SUS Socket DAIWA RAKUDA white, Marufuji KK                       	1	1	 7S38-1           	27	1
1959	  Stainless SUS Socket 19mm DAIWA RAKUDA, Marufuji KK                        	 7S56-1           	1487	0	  Stainless SUS Socket 19mm DAIWA RAKUDA, Marufuji KK                        	1	1	 7S56-1           	27	1
1960	  PIPA SS304TH DAIKEN LOKAL 1x10mmx10mmx6m, LANGGENG                         	 7P33-1           	11	0	  PIPA SS304TH DAIKEN LOKAL 1x10mmx10mmx6m, LANGGENG                         	1	1	 7P33-1           	27	1
1961	  PIPA ORNAMETAL  TORISETSU SYUNO TUBES TP304 9.53x0.8x372mm, HEISEI SSI     	 7P41-1           	1904	10912206	  PIPA ORNAMETAL  TORISETSU SYUNO TUBES TP304 9.53x0.8x372mm, HEISEI SSI     	1	1	 7P41-1           	27	1
1962	  PIPA ORNAMETAL  WOOD ONE TUBES TP304 9.53x0.8x542mm, HEISEI SSI            	 7P32-2           	827	6777033	  PIPA ORNAMETAL  WOOD ONE TUBES TP304 9.53x0.8x542mm, HEISEI SSI            	1	1	 7P32-2           	27	1
1963	  Door Mole OTHER 9x15x2100, Kyushu Marufuji                                 	 7D21-1           	405	0	  Door Mole OTHER 9x15x2100, Kyushu Marufuji                                 	1	1	 7D21-1           	27	1
1964	  Door Mole OTHER 13x15x2100,  Kyushu Marufuji                               	 7D20-1           	792	0	  Door Mole OTHER 13x15x2100,  Kyushu Marufuji                               	1	1	 7D20-1           	27	1
1965	  PVC H Profile Mild Br/112 KENOS 5x15x1850, SeIta Connector WolfPla         	 7P19-1           	98	0	  PVC H Profile Mild Br/112 KENOS 5x15x1850, SeIta Connector WolfPla         	1	1	 7P19-1           	27	1
1966	  PVC H Profile White/109 KENOS 5x20x1865, SeIta Connector Scan WolfPlast    	 7P20-1           	303	0	  PVC H Profile White/109 KENOS 5x20x1865, SeIta Connector Scan WolfPlast    	1	1	 7P20-1           	27	1
1967	  PVC H Profile Dark Br/117 KENOS 6x19x1840,  SeIta Connector Scan Wolf Pl   	 7P21-1           	3	0	  PVC H Profile Dark Br/117 KENOS 6x19x1840,  SeIta Connector Scan Wolf Pl   	1	1	 7P21-1           	27	1
1968	  PVC H Profile White/109 KENOS 6x19x1840, SeIta Connector (not use)Sc       	 7P22-1           	35	0	  PVC H Profile White/109 KENOS 6x19x1840, SeIta Connector (not use)Sc       	1	1	 7P22-1           	27	1
1969	  PVC H Profile Dark Br/117 KENOS 6x20x1800(default colour), ScanWolfP       	 7P23-1           	70	0	  PVC H Profile Dark Br/117 KENOS 6x20x1800(default colour), ScanWolfP       	1	1	 7P23-1           	27	1
1970	  Plastik Cusion MAETA Putih, Topan Cosmo                                    	 7P13-1           	6670	0	  Plastik Cusion MAETA Putih, Topan Cosmo                                    	1	1	 7P13-1           	27	1
1971	  Glass DGFL F600SB/SW 4x163x281, MAG                                        	 7G76-2           	30	157289	  Glass DGFL F600SB/SW 4x163x281, MAG                                        	1	1	 7G76-2           	27	1
1972	  Glass DGFL F800SB/SW DOOR 4x272x379, PT MAG                                	 7G45-2           	172	2029428	  Glass DGFL F800SB/SW DOOR 4x272x379, PT MAG                                	1	1	 7G45-2           	27	1
1973	  Glass DGFL F1000SB/SW DOOR 4x272x479, MAG                                  	 7G46-2           	208	3101488	  Glass DGFL F1000SB/SW DOOR 4x272x479, MAG                                  	1	1	 7G46-2           	27	1
1974	  GLASS DGFL 4x338x439 Door F1000MB/MW, PT MAG                               	 7G7-2            	273	4636359	  GLASS DGFL 4x338x439 Door F1000MB/MW, PT MAG                               	1	1	 7G7-2            	27	1
1975	  GLASS DGFL 4x325x347.5 Door FSV1000B,  PT BMG                              	 7G17-1           	1	12944	  GLASS DGFL 4x325x347.5 Door FSV1000B,  PT BMG                              	1	1	 7G17-1           	27	1
1976	  GLASS DGFL 4x325x347.5 Door FSV1000B,  PT MAG                              	 7G17-2           	58	750744	  GLASS DGFL 4x325x347.5 Door FSV1000B,  PT MAG                              	1	1	 7G17-2           	27	1
1977	  GLASS DGFL 4x272x334 FA800B,  PT MAG                                       	 7G18-3           	83	862938	  GLASS DGFL 4x272x334 FA800B,  PT MAG                                       	1	1	 7G18-3           	27	1
1978	  GLASS DGFL 4x286x439 Door FM1270B,  PT BMG                                 	 7G2-2            	45	941840	  GLASS DGFL 4x286x439 Door FM1270B,  PT BMG                                 	1	1	 7G2-2            	27	1
1979	  GLASS DGFL 4x286x449 Door FM1470B,  PT BMG                                 	 7G3-2            	41	905195	  GLASS DGFL 4x286x449 Door FM1470B,  PT BMG                                 	1	1	 7G3-2            	27	1
1980	  GLASS DGFL 4x286x504 Door FM1600B,  PT BMG                                 	 7G4-2            	47	1148338	  GLASS DGFL 4x286x504 Door FM1600B,  PT BMG                                 	1	1	 7G4-2            	27	1
1981	  GLASS DGFL FMD1500SB 4x286x460.5, PT BMG                                   	 7G60-1           	38	5127259	  GLASS DGFL FMD1500SB 4x286x460.5, PT BMG                                   	1	1	 7G60-1           	27	1
1982	  GLASS FL FMD1500SB  5x420x1507, TOKYO KOSHAKUSHO                           	 7G58-4           	265	82468175	  GLASS FL FMD1500SB  5x420x1507, TOKYO KOSHAKUSHO                           	1	1	 7G58-4           	27	1
1983	  GLASS DGFL FMD1700SB 4x286x560, PT BMG                                     	 7G61-1           	35	952659	  GLASS DGFL FMD1700SB 4x286x560, PT BMG                                     	1	1	 7G61-1           	27	1
1984	  GLASS  FMD1700SB 5x420x1707, TOKYO KUSHAKUSHO                              	 7G59-4           	127	51973944	  GLASS  FMD1700SB 5x420x1707, TOKYO KUSHAKUSHO                              	1	1	 7G59-4           	27	1
1985	  GLASS DGFL Door FL1600/1300/1150 4x205x457,  PT MAG                        	 7G106-2          	77	822920	  GLASS DGFL Door FL1600/1300/1150 4x205x457,  PT MAG                        	1	1	 7G106-2          	27	1
1986	  GLASS DGFL Door FZ1300/1600/1800 PB/SW 4x138x471,  PT MAG                  	 7G91-2           	66	490974	  GLASS DGFL Door FZ1300/1600/1800 PB/SW 4x138x471,  PT MAG                  	1	1	 7G91-2           	27	1
1987	  GLASS DGFL Door FG1150/1300/1500/1700 4x139x455, PT MAG                    	 7G113-2          	101	731139	  GLASS DGFL Door FG1150/1300/1500/1700 4x139x455, PT MAG                    	1	1	 7G113-2          	27	1
1988	  GLASS DGFL Door FR1200/1400/1500 4x270x298, PT BMG                         	 7G119-1          	1086	9773991	  GLASS DGFL Door FR1200/1400/1500 4x270x298, PT BMG                         	1	1	 7G119-1          	27	1
1989	  GLASS DGFL Door FR1200/1400/1500 4x270x298, PT MAG                         	 7G119-2          	32	288000	  GLASS DGFL Door FR1200/1400/1500 4x270x298, PT MAG                         	1	1	 7G119-2          	27	1
1990	  GLASS DGFL Door FR1200/1400/1500 4x270x298, PT MAYATAMA                    	 7G119-3          	4	36000	  GLASS DGFL Door FR1200/1400/1500 4x270x298, PT MAYATAMA                    	1	1	 7G119-3          	27	1
1991	  GLASS DGFL YFW1600G 5X90X795MM, BMG                                        	 7G141-1          	80	1192000	  GLASS DGFL YFW1600G 5X90X795MM, BMG                                        	1	1	 7G141-1          	27	1
1992	  GLASS MIRROR 3x321x1684 NAS SHOES W-525, PT.MAGI                           	 7G126-3          	591	66530050	  GLASS MIRROR 3x321x1684 NAS SHOES W-525, PT.MAGI                           	1	1	 7G126-3          	27	1
1993	  GLASS MIRROR 3x265x1684 NAS SHOES W-700, PT. BMG                           	 7G127-4          	854	79869269	  GLASS MIRROR 3x265x1684 NAS SHOES W-700, PT. BMG                           	1	1	 7G127-4          	27	1
1994	  GLASS MIRROR 3x265x1684 NAS SHOES W-700, PT. MAYATAMA                      	 7G127-5          	3	280571	  GLASS MIRROR 3x265x1684 NAS SHOES W-700, PT. MAYATAMA                      	1	1	 7G127-5          	27	1
1995	  GLASS MIRROR Storage W-625 New 3x387x291, PT MAG                           	 7G122-1          	2194	0	  GLASS MIRROR Storage W-625 New 3x387x291, PT MAG                           	1	1	 7G122-1          	27	1
1996	  GLASS MIRROR Storage W-625 New 3x387x291, PT MAYATAMA                      	 7G122-4          	3	55869	  GLASS MIRROR Storage W-625 New 3x387x291, PT MAYATAMA                      	1	1	 7G122-4          	27	1
1997	  GLASS MIRROR D.Shoes 3x1702x362 370H6, PT BMG                              	 7G22-1           	0	0	  GLASS MIRROR D.Shoes 3x1702x362 370H6, PT BMG                              	1	1	 7G22-1           	27	1
1998	  GLASS FL MARUNI DOOR W800-DG 4x291.5x298, BMG                              	 7G70-1           	3	30868	  GLASS FL MARUNI DOOR W800-DG 4x291.5x298, BMG                              	1	1	 7G70-1           	27	1
1999	  GLASS DGFL MARUNI DOOR MA800 4x299x292, PT.MAG                             	 7G70-2           	110	1131826	  GLASS DGFL MARUNI DOOR MA800 4x299x292, PT.MAG                             	1	1	 7G70-2           	27	1
2000	  GLASS FL MARUNI W1000CD 4x284x398 Door, PT MAG                             	 7G29-3           	46	553097	  GLASS FL MARUNI W1000CD 4x284x398 Door, PT MAG                             	1	1	 7G29-3           	27	1
2001	  GLASS FL MARUNI W1300 4x284x548 DOOR, PT MAG                               	 7G71-2           	43	766748	  GLASS FL MARUNI W1300 4x284x548 DOOR, PT MAG                               	1	1	 7G71-2           	27	1
2002	  GLASS FL MARUNI W1300-DG 4x283.5x548 DOOR, BMG                             	 7G71-1           	9	160482	  GLASS FL MARUNI W1300-DG 4x283.5x548 DOOR, BMG                             	1	1	 7G71-1           	27	1
2003	  GLASS DGFL MARUNI W1500 TYPE C 4x320x426, BMG                              	 7G80-3           	27	604302	  GLASS DGFL MARUNI W1500 TYPE C 4x320x426, BMG                              	1	1	 7G80-3           	27	1
2004	  Glass Miror  DAITO  3x267x1639, MAG                                        	 7G114-1          	143	9823767	  Glass Miror  DAITO  3x267x1639, MAG                                        	1	1	 7G114-1          	27	1
2005	  Glass Miror  DAITO  3x267x1642, MAG                                        	 7G115-1          	82	6184845	  Glass Miror  DAITO  3x267x1642, MAG                                        	1	1	 7G115-1          	27	1
2006	  Glass Miror  DAITO  3x267x1642, BMG                                        	 7G115-2          	25	1885623	  Glass Miror  DAITO  3x267x1642, BMG                                        	1	1	 7G115-2          	27	1
2007	  Glass Miror  DAITO  3x267x1762, MAG                                        	 7G116-1          	90	6994426	  Glass Miror  DAITO  3x267x1762, MAG                                        	1	1	 7G116-1          	27	1
2008	  Abbrasive 700x2000 #80, Eka Maint I                                        	 1A86-1           	18	1630948	  Abbrasive 700x2000 #80, Eka Maint I                                        	1	1	 1A86-1           	27	1
2009	  Abbrasive 700x2000 #150, Eka Maint I                                       	 1A87-1           	13	1090199	  Abbrasive 700x2000 #150, Eka Maint I                                       	1	1	 1A87-1           	27	1
2010	  Abbrasive 700x2000 #180, Eka Maint I                                       	 1A85-1           	10	838345	  Abbrasive 700x2000 #180, Eka Maint I                                       	1	1	 1A85-1           	27	1
2011	  Abbrasive 180x4000 #240, Deer                                              	 1A10-1           	18	1750115	  Abbrasive 180x4000 #240, Deer                                              	1	1	 1A10-1           	27	1
2012	  Abbrasive 180x4000 #400, Deer                                              	 1A11-1           	10	795533	  Abbrasive 180x4000 #400, Deer                                              	1	1	 1A11-1           	27	1
2013	  Abbrasive 180x4000 #100 Tx300, Ekamant I                                   	 1A8-2            	30	1429501	  Abbrasive 180x4000 #100 Tx300, Ekamant I                                   	1	1	 1A8-2            	27	1
2014	  Abbrasive 180x4000 #180 Tx300, Ekamant I                                   	 1A79-1           	11	520997	  Abbrasive 180x4000 #180 Tx300, Ekamant I                                   	1	1	 1A79-1           	27	1
2015	  Abbrasive W=100mm #120 Carpenter, Eka Maint I                              	 1A66-1           	540	2248718	  Abbrasive W=100mm #120 Carpenter, Eka Maint I                              	1	1	 1A66-1           	27	1
2016	  Abbrasive W=100mm #150 Carpenter, Eka Maint I                              	 1A67-1           	585	2432797	  Abbrasive W=100mm #150 Carpenter, Eka Maint I                              	1	1	 1A67-1           	5	1
2017	  Abbrasive W=100mm #180 Carpenter, Eka Maint I                              	 1A65-1           	360	1500404	  Abbrasive W=100mm #180 Carpenter, Eka Maint I                              	1	1	 1A65-1           	27	1
2018	  Abbrasive W=100mm #240 Carpenter, Eka Maint I                              	 1A51-1           	405	1685037	  Abbrasive W=100mm #240 Carpenter, Eka Maint I                              	1	1	 1A51-1           	27	1
2019	  Abbrasive W=100mm #320 Carpenter, Eka Maint I                              	 1A52-1           	540	2246143	  Abbrasive W=100mm #320 Carpenter, Eka Maint I                              	1	1	 1A52-1           	27	1
2020	  Abbrasive W=125mm #80 Carpenter, Eka Maint I                               	 1A68-1           	405	2103078	  Abbrasive W=125mm #80 Carpenter, Eka Maint I                               	1	1	 1A68-1           	27	1
2021	  Abbrasive W=125mm #120 Carpenter, Eka Maint I                              	 1A69-1           	360	1870657	  Abbrasive W=125mm #120 Carpenter, Eka Maint I                              	1	1	 1A69-1           	27	1
2022	  Abbrasive W=125mm #150 Carpenter, Eka Maint I                              	 1A70-1           	495	2588128	  Abbrasive W=125mm #150 Carpenter, Eka Maint I                              	1	1	 1A70-1           	27	1
2023	  Abbrasive W=125mm #180 Carpenter, Eka Maint I                              	 1A71-1           	540	2828929	  Abbrasive W=125mm #180 Carpenter, Eka Maint I                              	1	1	 1A71-1           	27	1
2024	  Abbrasive W=125mm #240 Carpenter, Eka Maint I                              	 1A55-1           	225	1196803	  Abbrasive W=125mm #240 Carpenter, Eka Maint I                              	1	1	 1A55-1           	27	1
2025	  Abbrasive W=125mm #320 Carpenter, Eka Maint I                              	 1A56-1           	360	1872485	  Abbrasive W=125mm #320 Carpenter, Eka Maint I                              	1	1	 1A56-1           	27	1
2026	  Abbrasive 1350x2515 #100, Deer                                             	 1A15-1           	8	2451391	  Abbrasive 1350x2515 #100, Deer                                             	1	1	 1A15-1           	27	1
2027	  Abbrasive 1350x2515 #320, Deer                                             	 1A19-1           	23	6663196	  Abbrasive 1350x2515 #320, Deer                                             	1	1	 1A19-1           	27	1
2028	  Abbrasive 1350x2515 #600, Deer                                             	 1A21-1           	10	2974595	  Abbrasive 1350x2515 #600, Deer                                             	1	1	 1A21-1           	27	1
2029	  Abbrasive 1350x2515 #80, EKA MANT                                          	 1A77-1           	10	1853279	  Abbrasive 1350x2515 #80, EKA MANT                                          	1	1	 1A77-1           	27	1
2030	  Abbrasive 1350x2515 #150, EKA MANT                                         	 1A22-1           	18	3483160	  Abbrasive 1350x2515 #150, EKA MANT                                         	1	1	 1A22-1           	27	1
2031	  Abbrasive 1350x2515 #180, EKA MANT                                         	 1A76-1           	15	2815427	  Abbrasive 1350x2515 #180, EKA MANT                                         	1	1	 1A76-1           	27	1
2032	  Abbrasive 1350x2515 #240, EKA MANT                                         	 1A23-1           	15	2921961	  Abbrasive 1350x2515 #240, EKA MANT                                         	1	1	 1A23-1           	27	1
2033	  Abbrasive 1350x2515 #400, Deer                                             	 1A20-1           	14	4220243	  Abbrasive 1350x2515 #400, Deer                                             	1	1	 1A20-1           	27	1
2034	  Abbrasive 150x628 #100, Deer                                               	 1A1-1            	33	506326	  Abbrasive 150x628 #100, Deer                                               	1	1	 1A1-1            	27	1
2035	  Abbrasive 150x635 #150 TJ500, EKA MANT                                     	 1A80-1           	20	162768	  Abbrasive 150x635 #150 TJ500, EKA MANT                                     	1	1	 1A80-1           	5	1
2036	  Abbrasive 150x635 #180 TJ500, EKA MANT                                     	 1A5-1            	27	229777	  Abbrasive 150x635 #180 TJ500, EKA MANT                                     	1	1	 1A5-1            	5	1
2037	  Abbrasive 150x635 #240 TJ500, EKA MANT                                     	 1A81-1           	20	174783	  Abbrasive 150x635 #240 TJ500, EKA MANT                                     	1	1	 1A81-1           	27	1
2038	  Abbrasive 200x2020 #150, Deer                                              	 1A24-1           	19	866905	  Abbrasive 200x2020 #150, Deer                                              	1	1	 1A24-1           	27	1
2039	  Abbrasive 200x2020 #180, Deer                                              	 1A25-1           	38	1739674	  Abbrasive 200x2020 #180, Deer                                              	1	1	 1A25-1           	27	1
2040	  Abbrasive 200x2050 #180 TJ 500, EKA MANT                                   	 1A26-1           	20	1502673	  Abbrasive 200x2050 #180 TJ 500, EKA MANT                                   	1	1	 1A26-1           	27	1
2041	  Abbrasive 200x2020 #240, Deer                                              	 1A27-1           	34	666091	  Abbrasive 200x2020 #240, Deer                                              	1	1	 1A27-1           	27	1
2042	  Abbrasive 200x2150  #120 TJ 500, EKA MANT                                  	 1A82-1           	10	234712	  Abbrasive 200x2150  #120 TJ 500, EKA MANT                                  	1	1	 1A82-1           	27	1
2043	  Abbrasive 200x2150  #150 TJ 500, EKA MANT                                  	 1A83-1           	10	247728	  Abbrasive 200x2150  #150 TJ 500, EKA MANT                                  	1	1	 1A83-1           	27	1
2044	  Abbrasive 200x2150  #180 TJ 500, EKA MANT                                  	 1A84-1           	20	469281	  Abbrasive 200x2150  #180 TJ 500, EKA MANT                                  	1	1	 1A84-1           	27	1
2045	  Abbrasive 400x2000 #80, Norton                                             	 1A28-1           	1	101980	  Abbrasive 400x2000 #80, Norton                                             	1	1	 1A28-1           	27	1
2046	  Abbrasive 400x2000 #180, Norton                                            	 1A29-1           	17	1962515	  Abbrasive 400x2000 #180, Norton                                            	1	1	 1A29-1           	27	1
2047	  Abbrasive 400x2000 #240, Norton                                            	 1A30-1           	13	760061	  Abbrasive 400x2000 #240, Norton                                            	1	1	 1A30-1           	27	1
2048	  Abbrasive 450x2000 #180, Norton                                            	 1A31-1           	10	1136659	  Abbrasive 450x2000 #180, Norton                                            	1	1	 1A31-1           	27	1
2049	  Abbrasive 1310x2615 #80, Norton                                            	 1A32-1           	5	1820295	  Abbrasive 1310x2615 #80, Norton                                            	1	1	 1A32-1           	27	1
2050	  Abbrasive 1310x2615 #80 (Eka 1000), EKAMANT                                	 1A33-2           	10	1944779	  Abbrasive 1310x2615 #80 (Eka 1000), EKAMANT                                	1	1	 1A33-2           	27	1
2051	  Abbrasive 1310x2615 #80 Norton, Micro Abadi                                	 1A33-1           	1	364059	  Abbrasive 1310x2615 #80 Norton, Micro Abadi                                	1	1	 1A33-1           	27	1
2052	  Abbrasive 1310x2615 #100, Norton                                           	 1A34-1           	8	2605291	  Abbrasive 1310x2615 #100, Norton                                           	1	1	 1A34-1           	27	1
2053	  Abbrasive 1310x2615 #120, Norton                                           	 1A35-1           	27	8814510	  Abbrasive 1310x2615 #120, Norton                                           	1	1	 1A35-1           	27	1
2054	  Abbrasive 1310x2615 #150, Norton                                           	 1A36-1           	3	873770	  Abbrasive 1310x2615 #150, Norton                                           	1	1	 1A36-1           	27	1
2055	  Abbrasive 1310x2615 #150 (EKA 1000), EKAMANT                               	 1A36-2           	8	1312672	  Abbrasive 1310x2615 #150 (EKA 1000), EKAMANT                               	1	1	 1A36-2           	27	1
2056	  Abbrasive 1310x2615 #180, Norton                                           	 1A37-1           	11	3588428	  Abbrasive 1310x2615 #180, Norton                                           	1	1	 1A37-1           	27	1
2057	  Abbrasive 1310x2615 #180, Deer                                             	 1A12-1           	1	326221	  Abbrasive 1310x2615 #180, Deer                                             	1	1	 1A12-1           	27	1
2058	  Abbrasive 1310x2615 #240 (EKA 1000), EKAMANT                               	 1A13-2           	7	1398726	  Abbrasive 1310x2615 #240 (EKA 1000), EKAMANT                               	1	1	 1A13-2           	27	1
2059	  Abbrasive 1310x2615 #240 Norton, Micro Abadi                               	 1A38-1           	1	199818	  Abbrasive 1310x2615 #240 Norton, Micro Abadi                               	1	1	 1A38-1           	27	1
2060	  Abbrasive 1310x2615 #320, Norton                                           	 1A39-1           	5	1603080	  Abbrasive 1310x2615 #320, Norton                                           	1	1	 1A39-1           	27	1
2061	  Abbrasive 3M 1310x2615 #180, C C I                                         	 1A40-1           	1	326251	  Abbrasive 3M 1310x2615 #180, C C I                                         	1	1	 1A40-1           	27	1
2062	  Abbrasive 230x280 #80, Sankyo                                              	 1A44-1           	100	245869	  Abbrasive 230x280 #80, Sankyo                                              	1	1	 1A44-1           	27	1
2063	  Abbrasive 230x280 #150, Sankyo                                             	 1A45-1           	63	153900	  Abbrasive 230x280 #150, Sankyo                                             	1	1	 1A45-1           	27	1
2064	  Abbrasive 230x280 #240 KSCW,  EKA MANT                                     	 1A46-1           	300	495468	  Abbrasive 230x280 #240 KSCW,  EKA MANT                                     	1	1	 1A46-1           	27	1
2065	  Abbrasive 230x280 #320 KSCW,  EKA MANT                                     	 1A47-1           	200	335000	  Abbrasive 230x280 #320 KSCW,  EKA MANT                                     	1	1	 1A47-1           	27	1
2066	  Abbrasive 230x280 #600 KSCW,  EKA MANT                                     	 1A48-1           	100	161528	  Abbrasive 230x280 #600 KSCW,  EKA MANT                                     	1	1	 1A48-1           	27	1
2067	  Abbrasive 230x280 #1000 KSCW,  EKA MANT                                    	 1A49-1           	200	315333	  Abbrasive 230x280 #1000 KSCW,  EKA MANT                                    	1	1	 1A49-1           	27	1
2068	  Abbrasive 152x7000 JA165 #320, Deer                                        	 1A6-1            	17	2369435	  Abbrasive 152x7000 JA165 #320, Deer                                        	1	1	 1A6-1            	27	1
2069	  Abbrasive 152x7000 JA 346 #600, Deer                                       	 1A7-1            	39	5415974	  Abbrasive 152x7000 JA 346 #600, Deer                                       	1	1	 1A7-1            	27	1
2070	  Abbrasive 85x2500 #120  Tx300, Ekamant I                                   	 1A61-3           	20	298573	  Abbrasive 85x2500 #120  Tx300, Ekamant I                                   	1	1	 1A61-3           	27	1
2071	  Abbrasive 85x2500 #150  Tx300, Ekamant I                                   	 1A61-2           	40	600000	  Abbrasive 85x2500 #150  Tx300, Ekamant I                                   	1	1	 1A61-2           	27	1
2072	  Abbrasive 85x2500 #180, Deer                                               	 1A62-1           	30	945285	  Abbrasive 85x2500 #180, Deer                                               	1	1	 1A62-1           	27	1
2073	  Abbrasive 85x2500 #240, Deer                                               	 1A63-1           	30	959159	  Abbrasive 85x2500 #240, Deer                                               	1	1	 1A63-1           	27	1
2074	  GrafitW = 200 mmKarunia	 1A64-1	45	3969369	  GrafitW = 200 mmKarunia	1	1	 1A64-1	27	1
2075	  Spirtus Bakar  BOTOL 1LT, BRATACO                                          	 5S7-2            	10	245455	  Spirtus Bakar  BOTOL 1LT, BRATACO                                          	1	1	 5S7-2            	33	1
2076	  METHANOL  drum 200, BRATACO                                                	 5M1-1            	2	5454545	  METHANOL  drum 200, BRATACO                                                	1	1	 5M1-1            	1	1
2077	  Alkohol 96% Drum200L/ETHANOL PRIMA, Molindo                                	 5A1-2            	3	20699999	  Alkohol 96% Drum200L/ETHANOL PRIMA, Molindo                                	1	1	 5A1-2            	1	1
2078	  Aqua Proof Transparan Can4, Pelangi H                                      	 5A3-1            	2	457267	  Aqua Proof Transparan Can4, Pelangi H                                      	1	1	 5A3-1            	27	1
2079	  Aqua Proof  Transparan Can4, Megah Baru                                    	 5A4-1            	1	228562	  Aqua Proof  Transparan Can4, Megah Baru                                    	1	1	 5A4-1            	27	1
2080	  Aceton Drum160, Indocemical                                                	 5A5-1            	3	7199668	  Aceton Drum160, Indocemical                                                	1	1	 5A5-1            	1	1
2081	  Spirtus Bakar  PaIL20, BRATACO                                             	 5S7-1            	1	409091	  Spirtus Bakar  PaIL20, BRATACO                                             	1	1	 5S7-1            	33	1
2082	  Thinner  Wash Bensin /Wipping Drum200, Brataco                             	 5T1-1            	4	10181818	  Thinner  Wash Bensin /Wipping Drum200, Brataco                             	1	1	 5T1-1            	33	1
2083	  Thinner A Special Pail 20, Sarana Indah J                                  	 5T2-1            	5	1183267	  Thinner A Special Pail 20, Sarana Indah J                                  	1	1	 5T2-1            	33	1
2084	  P-Pen HORY NA2634,  Toppan                                                 	 7P10-1           	1	0	  P-Pen HORY NA2634,  Toppan                                                 	1	1	 7P10-1           	27	1
2085	  Intruction Sheet D.KANTANA , Crestec                                       	 8I36-1           	1173	2709119	  Intruction Sheet D.KANTANA , Crestec                                       	1	1	 8I36-1           	27	1
2086	  Intruction Sheet  WOOD ONE CRS.8025-1 6 Pages, CRESTEC                     	 7I11-2           	4109	3803054	  Intruction Sheet  WOOD ONE CRS.8025-1 6 Pages, CRESTEC                     	1	1	 7I11-2           	27	1
2087	  Intruction Sheet  WOOD ONE 2 Pages, CRESTEC                                	 7I12-1           	4353	2243593	  Intruction Sheet  WOOD ONE 2 Pages, CRESTEC                                	1	1	 7I12-1           	27	1
2088	  Instruction Sheet FUKAI Door Glass JIH0079-001(NON FMD), Crestec           	 8I1-1            	801	144178	  Instruction Sheet FUKAI Door Glass JIH0079-001(NON FMD), Crestec           	1	1	 8I1-1            	27	1
2089	  L YS (21mm) FUKAI , TOKYO K                                                	 8L13-1           	3920	0	  L YS (21mm) FUKAI , TOKYO K                                                	1	1	 8L13-1           	27	1
2090	  L YS (36mm) FUKAI , TOKYO K                                                	 8L14-1           	1700	0	  L YS (36mm) FUKAI , TOKYO K                                                	1	1	 8L14-1           	27	1
2091	  HOLE SEAL F.Kulkas Polos (Putih) 12mm, PT.Aneka Rupa Tera                  	 8H20-1           	23640	683540	  HOLE SEAL F.Kulkas Polos (Putih) 12mm, PT.Aneka Rupa Tera                  	1	1	 8H20-1           	27	1
2092	  HOLE SEAL F.Kulkas Coklat (OAK) 12mm, PT.Aneka Rupa Tera                   	 8H21-1           	32016	875773	  HOLE SEAL F.Kulkas Coklat (OAK) 12mm, PT.Aneka Rupa Tera                   	1	1	 8H21-1           	27	1
2093	  HOLE SEAL FUKAI MAPLE (P4685C) 12mm, PT.Aneka Rupa Tera                    	 8H24-1           	21520	1522979	  HOLE SEAL FUKAI MAPLE (P4685C) 12mm, PT.Aneka Rupa Tera                    	1	1	 8H24-1           	27	1
2094	  HOLE SEAL FUKAI BLACK 12mm, PT.Aneka Rupa Tera                             	 8H25-1           	13840	445681	  HOLE SEAL FUKAI BLACK 12mm, PT.Aneka Rupa Tera                             	1	1	 8H25-1           	27	1
2095	  Label HIJAU F.kitchen, FUKAI MUSEN                                         	 8L8-1            	7452	3663925	  Label HIJAU F.kitchen, FUKAI MUSEN                                         	1	1	 8L8-1            	27	1
2096	  Label PERHATIAN F.kitchen, FUKAI MUSEN                                     	 8L9-1            	866	0	  Label PERHATIAN F.kitchen, FUKAI MUSEN                                     	1	1	 8L9-1            	27	1
2097	  Label 10kg F.kitchen, FUKAI MUSEN                                          	 8L10-1           	4524	2064628	  Label 10kg F.kitchen, FUKAI MUSEN                                          	1	1	 8L10-1           	27	1
2098	  Label 25kg F.kitchen, FUKAI MUSEN                                          	 8L11-1           	1000	0	  Label 25kg F.kitchen, FUKAI MUSEN                                          	1	1	 8L11-1           	27	1
2099	  Label 1500W F.kitchen, FUKAI MUSEN                                         	 8L12-1           	1000	0	  Label 1500W F.kitchen, FUKAI MUSEN                                         	1	1	 8L12-1           	27	1
2100	  Wooden Bond FUKAI, MARUFUJI KK                                             	 8W7-1            	2283	8605686	  Wooden Bond FUKAI, MARUFUJI KK                                             	1	1	 8W7-1            	27	1
2101	  DAIKEN SHOES Inspection Vote Seal, Marufuji KK                             	 7D2-1            	45600	0	  DAIKEN SHOES Inspection Vote Seal, Marufuji KK                             	1	1	 7D2-1            	27	1
2102	  Warning Seal / Red Long Seal NEW DAIKEN CLOUK,  Marufuji KK                	 7W1-2            	14060	33105	  Warning Seal / Red Long Seal NEW DAIKEN CLOUK,  Marufuji KK                	1	1	 7W1-2            	27	1
2103	  Stiker Hole NISSEN PM 16mm, Propen I                                       	 7S34-1           	5100	0	  Stiker Hole NISSEN PM 16mm, Propen I                                       	1	1	 7S34-1           	27	1
2104	  Botol Lem NISSEN, Karya baja plastik                                       	 7B2-1            	530	0	  Botol Lem NISSEN, Karya baja plastik                                       	1	1	 7B2-1            	27	1
2105	  P Pen GEOgray MOHAWK TOTO KITCHEN FIL STICK                                	 7P16-1           	10	0	  P Pen GEOgray MOHAWK TOTO KITCHEN FIL STICK                                	1	1	 7P16-1           	27	1
2106	  Double Tape Sekisui UNICLOSE #535(15x10), MEGAH PITA I                     	 7D13-2           	1084	28309462	  Double Tape Sekisui UNICLOSE #535(15x10), MEGAH PITA I                     	1	1	 7D13-2           	27	1
2107	WIP 01	WIP01	101	1000	WIP 01	1	2	WIP01	27	1
2108	WIP 02	WIP02	102	2000	WIP 02	1	2	WIP02	27	1
2109	WIP 03	WIP03	103	3000	WIP 03	1	2	WIP03	27	1
2110	WIP 04	WIP04	104	4000	WIP 04	1	2	WIP04	27	1
2111	WIP 05	WIP05	105	5000	WIP 05	1	2	WIP05	27	1
2112	WIP 06	WIP06	106	6000	WIP 06	1	2	WIP06	27	1
2113	WIP 07	WIP07	107	7000	WIP 07	1	2	WIP07	27	1
2114	WIP 08	WIP08	108	8000	WIP 08	1	2	WIP08	27	1
2115	WIP 09	WIP09	109	9000	WIP 09	1	2	WIP09	27	1
2116	WIP 10	WIP10	110	10000	WIP 10	1	2	WIP10	27	1
2117	WIP 11	WIP11	111	11000	WIP 11	1	2	WIP11	27	1
2118	WIP 12	WIP12	112	12000	WIP 12	1	2	WIP12	27	1
2119	WIP 13	WIP13	113	13000	WIP 13	1	2	WIP13	27	1
2120	WIP 14	WIP14	114	14000	WIP 14	1	2	WIP14	27	1
2121	WIP 15	WIP15	115	15000	WIP 15	1	2	WIP15	27	1
2122	WIP 16	WIP16	116	16000	WIP 16	1	2	WIP16	27	1
2123	WIP 17	WIP17	117	17000	WIP 17	1	2	WIP17	27	1
2124	WIP 18	WIP18	118	18000	WIP 18	1	2	WIP18	27	1
2125	WIP 19	WIP19	119	19000	WIP 19	1	2	WIP19	27	1
2126	WIP 20	WIP20	120	20000	WIP 20	1	2	WIP20	27	1
2127	WIP 21	WIP21	121	21000	WIP 21	1	2	WIP21	27	1
2128	WIP 22	WIP22	122	22000	WIP 22	1	2	WIP22	27	1
2129	WIP 23	WIP23	123	23000	WIP 23	1	2	WIP23	27	1
2130	WIP 24	WIP24	124	24000	WIP 24	1	2	WIP24	27	1
2131	WIP 25	WIP25	125	25000	WIP 25	1	2	WIP25	27	1
2132	WIP 26	WIP26	126	26000	WIP 26	1	2	WIP26	27	1
2133	WIP 27	WIP27	127	27000	WIP 27	1	2	WIP27	27	1
2134	WIP 28	WIP28	128	28000	WIP 28	1	2	WIP28	27	1
2135	WIP 29	WIP29	129	29000	WIP 29	1	2	WIP29	27	1
2136	WIP 30	WIP30	130	30000	WIP 30	1	2	WIP30	27	1
2137	WIP 31	WIP31	131	31000	WIP 31	1	2	WIP31	27	1
2138	WIP 32	WIP32	132	32000	WIP 32	1	2	WIP32	27	1
2139	WIP 33	WIP33	133	33000	WIP 33	1	2	WIP33	27	1
2140	WIP 34	WIP34	134	34000	WIP 34	1	2	WIP34	27	1
2141	WIP 35	WIP35	135	35000	WIP 35	1	2	WIP35	27	1
2142	WIP 36	WIP36	136	36000	WIP 36	1	2	WIP36	27	1
2143	WIP 37	WIP37	137	37000	WIP 37	1	2	WIP37	27	1
2144	BARANGJADI 01	BJ01	11	1100	BJ01	1	3	BJ01	27	1
2145	BARANGJADI 02	BJ02	12	1200	BJ02	1	3	BJ02	27	1
2146	BARANGJADI 03	BJ03	13	1300	BJ03	1	3	BJ03	27	1
2147	BARANGJADI 04	BJ04	14	1400	BJ04	1	3	BJ04	27	1
2148	BARANGJADI 05	BJ05	15	1500	BJ05	1	3	BJ05	27	1
2149	BARANGJADI 06	BJ06	16	1600	BJ06	1	3	BJ06	27	1
2150	BARANGJADI 07	BJ07	17	1700	BJ07	1	3	BJ07	27	1
2151	BARANGJADI 08	BJ08	18	1800	BJ08	1	3	BJ08	27	1
2152	BARANGJADI 09	BJ09	19	1900	BJ09	1	3	BJ09	27	1
2153	BARANGJADI 10	BJ10	20	2000	BJ10	1	3	BJ10	27	1
2154	BARANGJADI 11	BJ11	21	2100	BJ11	1	3	BJ11	27	1
2155	BARANGJADI 12	BJ12	22	2200	BJ12	1	3	BJ12	27	1
2156	BARANGJADI 13	BJ13	23	2300	BJ13	1	3	BJ13	27	1
2157	BARANGJADI 14	BJ14	24	2400	BJ14	1	3	BJ14	27	1
2158	BARANGJADI 15	BJ15	25	2500	BJ15	1	3	BJ15	27	1
2159	BARANGJADI 16	BJ16	26	2600	BJ16	1	3	BJ16	27	1
2160	BARANGJADI 17	BJ17	27	2700	BJ17	1	3	BJ17	27	1
2161	BARANGJADI 18	BJ18	28	2800	BJ18	1	3	BJ18	27	1
2162	BARANGJADI 19	BJ19	29	2900	BJ19	1	3	BJ19	27	1
2163	BARANGJADI 20	BJ20	30	3000	BJ20	1	3	BJ20	27	1
\.


                                                                                                                                                                                                                                                                                                                                                                                                                                    3343.dat                                                                                            0000600 0004000 0002000 00000000023 13607356547 0014261 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        1	UMUM	UMUM	1
\.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             3345.dat                                                                                            0000600 0004000 0002000 00000000523 13607356547 0014270 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        1	BAHAN BAKU		1
4	PERALATAN PABRIK		1
5	PERALATAN PERKANTORAN		1
6	PERALATAN KONTRUKSI		1
7	BARANG REIMPORT		1
8	BARANG CONTOH / TEST		1
9	LAINNYA		1
10	LEBIH DARI 1 JENIS BARANG 1 sd 6		1
2	BARANG SETENGAH JADI		1
11	BAHAN PENOLONG		1
3	BARANG JADI		1
12	MESIN / SPARE PART		1
14	PACKING	PACKING	1
16	SCRAPT / WASTE	SCRAPT / WASTE	1
\.


                                                                                                                                                                             3347.dat                                                                                            0000600 0004000 0002000 00000000554 13607356547 0014276 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        14	KAS BESAR IN	1	BKM	1	1
15	KAS BESAR OUT	1	BKK	0	1
16	KAS KECIL IN	2	BKm	1	1
17	KAS KECIL OUT	2	BKke	0	1
18	BANK BCA IN	3	BBM/BCA	1	1
19	BANK BCA OUT	3	BBK/BCA	0	1
20	BCA USD IN	4	BBM/BCAUSD	1	1
21	BCA USD OUT	4	BBK/BCAUSD	0	1
22	BANK CTBC IN	5	BBM/CTBC	1	1
23	BANK CTBC OUT	5	BBK/CTBC	0	1
24	CTBC USD IN	6	BBM/CTBCUSD	1	1
25	CTBC USD OUT	6	BKK/CTBCUSD	0	1
\.


                                                                                                                                                    3349.dat                                                                                            0000600 0004000 0002000 00000000005 13607356547 0014267 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        \.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           3351.dat                                                                                            0000600 0004000 0002000 00000000113 13607356547 0014260 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        PT. MARUFUJI KENZAI INDONESIA	MOJOKERTO	MOJOKERTO	Jawa Timur	61385	SS
\.


                                                                                                                                                                                                                                                                                                                                                                                                                                                     3352.dat                                                                                            0000600 0004000 0002000 00000000005 13607356547 0014261 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        \.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           3354.dat                                                                                            0000600 0004000 0002000 00000000005 13607356547 0014263 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        \.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           3356.dat                                                                                            0000600 0004000 0002000 00000000005 13607356547 0014265 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        \.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           3358.dat                                                                                            0000600 0004000 0002000 00000000005 13607356547 0014267 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        \.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           3360.dat                                                                                            0000600 0004000 0002000 00000000005 13607356547 0014260 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        \.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           3362.dat                                                                                            0000600 0004000 0002000 00000000005 13607356547 0014262 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        \.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           3364.dat                                                                                            0000600 0004000 0002000 00000000074 13607356547 0014272 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        51	35	5	45	145103.32205882
52	35	59	50	134550.35318182
\.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                    3332.dat                                                                                            0000600 0004000 0002000 00000000121 13607356547 0014256 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        35	PAK-19-08-0001	PRODUKSI 001	2019-08-16	2	2019-11-30	13	13257167.151738	1
\.


                                                                                                                                                                                                                                                                                                                                                                                                                                               3366.dat                                                                                            0000600 0004000 0002000 00000000005 13607356547 0014266 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        \.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           3368.dat                                                                                            0000600 0004000 0002000 00000000071 13607356547 0014273 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        13	NPM-19-08-0001	2019-08-15	202	1000	PRODUKSI001	1
\.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                       3370.dat                                                                                            0000600 0004000 0002000 00000000005 13607356547 0014261 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        \.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           3372.dat                                                                                            0000600 0004000 0002000 00000000005 13607356547 0014263 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        \.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           3374.dat                                                                                            0000600 0004000 0002000 00000000641 13607356547 0014273 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        1	1	19
2	2	20
3	2	19
4	3	22
5	4	22
6	5	22
7	6	22
8	7	22
9	8	22
10	9	22
11	10	22
12	11	22
13	12	22
14	13	22
15	14	22
16	15	22
17	16	22
18	17	22
19	18	23
20	19	23
21	20	25
22	21	25
23	22	25
24	23	0
25	24	25
26	25	25
27	26	25
28	27	26
29	28	26
30	29	26
31	30	26
32	31	26
33	32	26
34	33	27
35	34	27
36	35	26
37	36	26
38	37	26
39	38	26
40	39	26
41	40	28
42	41	28
43	42	29
44	43	29
45	44	28
46	45	30
47	45	31
48	46	33
\.


                                                                                               3376.dat                                                                                            0000600 0004000 0002000 00000006314 13607356547 0014300 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        1	\N	2019-11-26	1	4962	23	PRODUKSI COPPER	1	2	2019-11-26	\N
2	PRD-19-11-0001	2019-11-26	1	4962	232	ALUMINIUM SCRAP WIP	1	2	2019-11-26	\N
3	PRD-19-11-0002	2019-11-26	1	5117	100	PRODUKSI BRONZE	1	2	2019-11-26	\N
4	PRD-19-11-0003	2019-11-26	1	4964	10	PRODUKSI COPPER	1	2	2019-11-26	\N
5	PRD-19-11-0004	2019-11-26	1	4962	20	ALUMINIUM SCRAP	1	2	2019-11-26	\N
6	PRD-19-11-0005	2019-11-26	1	4962	12	PRODUKSI COPPER	1	2	2019-11-26	\N
7	PRD-19-11-0006	2019-11-26	1	5117	23	ALUMINIUM SCRAP	1	2	2019-11-26	\N
8	PRD-19-11-0007	2019-11-26	1	4999	15	PRODUKSI 1	1	2	2019-11-26	\N
9	PRD-19-11-0008	2019-11-26	1	4987	16	ROLLSS	1	2	2019-11-26	\N
10	PRD-19-11-0009	2019-11-26	1	5110	11	PRODUKSI BRONZE	1	2	2019-11-26	\N
11	PRD-19-11-0010	2019-11-26	1	5064	11	ROLLSS ok	1	2	2019-11-26	\N
12	PRD-19-11-0011	2019-11-26	1	5052	9	ALUMINIUM SCRAP	1	2	2019-11-26	\N
13	PRD-19-11-0012	2019-11-26	1	4989	8	PRODUKSI COPPER	1	2	2019-11-26	\N
14	PRD-19-11-0013	2019-11-26	1	4984	5	PRODUKSI COPPER	1	2	2019-11-26	\N
15	PRD-19-11-0014	2019-11-26	1	4963	2	PRODUKSI BRONZE	1	2	2019-11-26	\N
16	PRD-19-11-0015	2019-11-26	1	5026	7	PRODUKSI COPPER	1	2	2019-11-26	\N
17	PRD-19-11-0016	2019-11-26	1	4963	8	PRODUKSI COPPER	1	2	2019-11-26	\N
18	PRD-19-11-0017	2019-11-26	1	4962	200	PRODUKSI WIP	1	2	2019-11-26	\N
19	PRD-19-01-0001	2019-01-30	1	4962	9000	ok	1	2	2019-11-27	\N
20	PRD-19-04-0001	2019-04-02	1	4962	10	PRODUKSI 1	1	2	2019-11-27	\N
21	PRD-19-05-0001	2019-05-01	1	4965	8	ROLLSS	1	2	2019-11-27	\N
22	PRD-19-04-0002	2019-04-09	1	4965	5	PRODUKSI COPPER	1	2	2019-11-27	\N
23	PRD-19-11-0018	2019-11-27	1	5117	10	PRODUKSI BRONZE	1	2	2019-11-27	\N
24	PRD-19-05-0002	2019-05-31	1	4993	5	ALUMINIUM SCRAP	1	2	2019-11-27	\N
25	PRD-19-11-0019	2019-11-27	1	5117	10	PRODUKSI BRONZE	1	2	2019-11-27	\N
26	PRD-19-11-0020	2019-11-27	1	5117	10	PRODUKSI BRONZE	1	2	2019-11-27	\N
27	PRD-19-05-0003	2019-05-02	1	4962	150	PRODUKSI BRONZE	1	2	2019-11-27	\N
28	PRD-19-06-0001	2019-06-05	1	5039	50	PRODUKSI 1	1	2	2019-11-27	\N
29	PRD-19-04-0003	2019-04-04	1	4972	12	PRODUKSI BRONZE	1	2	2019-11-27	\N
30	PRD-19-11-0021	2019-11-27	1	5117	10	PRODUKSI BRONZE	1	2	2019-11-27	\N
31	PRD-19-11-0022	2019-11-27	1	4962	10	PRODUKSI COPPER	1	2	2019-11-27	\N
32	PRD-19-11-0023	2019-11-27	1	4962	10	PRODUKSI COPPER	1	2	2019-11-27	\N
33	PRD-19-11-0024	2019-11-27	1	3041	105	ALUMINIUM SCRAP	1	2	2019-11-27	\N
34	PRD-19-11-0025	2019-11-27	1	2928	34	PRODUKSI BRONZE	1	2	2019-11-27	\N
35	PRD-19-11-0026	2019-11-27	1	4963	3	PRODUKSI BRONZE	1	2	2019-11-27	\N
36	PRD-19-11-0027	2019-11-27	1	4962	105	PRODUKSI BRONZE	1	2	2019-11-27	\N
37	PRD-19-11-0028	2019-11-27	1	4963	1	PRODUKSI BRONZE	1	2	2019-11-27	\N
38	PRD-19-11-0029	2019-11-27	1	4973	99	PRODUKSI COPPER	1	2	2019-11-27	\N
39	PRD-19-11-0030	2019-11-27	1	4965	88	PRODUKSI COPPER	1	2	2019-11-27	\N
40	PRD-19-11-0031	2019-11-27	1	4962	20	PRODUKSI BRONZE	1	2	2019-11-27	\N
41	PRD-19-11-0032	2019-11-27	1	4962	10	PRODUKSI 1	1	2	2019-11-27	\N
42	PRD-19-11-0033	2019-11-27	1	3041	10	PRODUKSI COPPER	1	2	2019-11-27	\N
43	PRD-19-11-0034	2019-11-27	1	2930	3	PRODUKSI BRONZE	1	2	2019-11-27	\N
44	PRD-19-11-0035	2019-11-27	1	4962	5	PRODUKSI BRONZE	1	2	2019-11-27	\N
45	PRD-19-01-0002	2019-01-10	1	4962	200	PRODUKSI WIP	1	2	2019-11-28	\N
46	PRD-19-01-0003	2019-01-17	1	2929	120	PRODUKSI BJ	1	2	2019-11-28	\N
\.


                                                                                                                                                                                                                                                                                                                    3378.dat                                                                                            0000600 0004000 0002000 00000000005 13607356547 0014271 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        \.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           3380.dat                                                                                            0000600 0004000 0002000 00000000005 13607356547 0014262 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        \.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           3382.dat                                                                                            0000600 0004000 0002000 00000000005 13607356547 0014264 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        \.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           3384.dat                                                                                            0000600 0004000 0002000 00000000365 13607356547 0014277 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        1	PENJUALAN	PENJUALAN	1	1	1	1	1	1	1	1	1	1	1	\N	\N
4	PEMBELIAN	PEMBELIAN	0	1	0	1	0	0	0	0	1	0	0	\N	\N
5	EXIM	EXIM	1	1	1	1	1	1	1	1	1	1	1	\N	\N
3	SUPER ADMIN	SUPER ADMIN	1	1	1	1	1	1	1	1	1	1	1	1	1
6	BEACUKAI	BEACUKAI	0	0	0	0	0	0	0	0	1	0	0	\N	\N
\.


                                                                                                                                                                                                                                                                           3386.dat                                                                                            0000600 0004000 0002000 00000000547 13607356547 0014303 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        1	TARINDO	TARINDO	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1
2	BEACUKAI	BEACUKAI	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1	1
\.


                                                                                                                                                         3388.dat                                                                                            0000600 0004000 0002000 00000000062 13607356547 0014275 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        25	19	2144	5	5	25	1
26	20	2146	10	100	1000	1
\.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                              3390.dat                                                                                            0000600 0004000 0002000 00000000232 13607356547 0014265 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        19	SO/20/01/0001	2020-01-14	203	so001	0	25	0	25	2	2020-01-14	1	\N	\N	1
20	SO/20/01/0002	2020-01-14	123	qwdeqw	1	1000	100	1100	2	2020-01-14	1	\N	\N	1
\.


                                                                                                                                                                                                                                                                                                                                                                      3392.dat                                                                                            0000600 0004000 0002000 00000000654 13607356547 0014277 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        2	PCS	PCS	1
3	ROLL	ROLLS	0
4	YRD	YARD	1
1	KG	KILOGRAM	1
5	MTR	METER	1
6	FTK	FTK	1
7	LBR	LBR	1
8	NTD	NTD	1
9	PAIR	PAIR	1
10	PLT	PLT	1
11	PRS	PRS	1
12	PSG	PSG	1
13	RIM	RIM	1
14	ROLL	ROLL	1
15	SF	SF	1
16	SHT	SHT	1
17	TRD	TRD	1
18	USD	USD	1
19	BTL	BOTOL	1
20	EMB	EMB	1
21	JK	JK	1
22	JRG	JRG	1
23	KK	KK	1
24	KLG	KLG	1
25	SAG	SAG	1
26	SET	SET	1
27	PCE	PCE	1
28	CTN	CTN	1
29	LMBR	LEMBAR	1
30	PAK	PAK	1
31	UNIT	UNIT	1
32	BOX	BOX	1
\.


                                                                                    3394.dat                                                                                            0000600 0004000 0002000 00000000005 13607356547 0014267 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        \.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           3396.dat                                                                                            0000600 0004000 0002000 00000000005 13607356547 0014271 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        \.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           3398.dat                                                                                            0000600 0004000 0002000 00000000005 13607356547 0014273 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        \.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           3400.dat                                                                                            0000600 0004000 0002000 00000000005 13607356547 0014253 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        \.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           3402.dat                                                                                            0000600 0004000 0002000 00000000150 13607356547 0014256 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        1	AKTIVA
2	KEWAJIBAN
3	MODAL
4	PENJUALAN
5	HPP
6	BIAYA ADM DAN UMUM
7	BIAYA DAN PENDAPATAN LAINNYA
\.


                                                                                                                                                                                                                                                                                                                                                                                                                        3404.dat                                                                                            0000600 0004000 0002000 00000000564 13607356547 0014271 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        1	KAS BANK	1
2	PENYUSUTAN	1
3	MODAL SAHAM / DISETOR	1
4	LABA DITAHAN / LR TAHUN LALU	1
5	HPP	1
6	PERSEDIAAN	1
7	ASSET	1
8	HUTANG	1
9	HUTANG PAJAK	1
10	BIAYA SETOR / HUTANG BIAYA	1
11	PIUTANG	1
12	UANG MUKA	1
13	UANG MUKA PAJAK	1
14	UANG MUKA PENJUALAN	1
15	HUTANG LAINNYA	1
16	HUTANG BANK	1
17	PIUTANG LAINNYA	1
18	SURAT BERHARGA	1
19	INVESTASI	1
20	BEBAN PRODUKSI	1
\.


                                                                                                                                            3406.dat                                                                                            0000600 0004000 0002000 00000000005 13607356547 0014261 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        \.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           3407.dat                                                                                            0000600 0004000 0002000 00000000005 13607356547 0014262 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        \.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           3410.dat                                                                                            0000600 0004000 0002000 00000001137 13607356547 0014263 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        1	admin	admin	admin beone	1	1	2019-07-07	1
3	hakim1	d799846e52c3b0a617137b1aa68d3981	hakim1	1	1	2019-07-15	0
4	nur	ed1e56ef963bb91c45a14a50c2f3cd95	nur bella kurnia citra	1	1	2019-07-15	0
19	jonathan	97bdc896ca0ce9549f8bd92f3288e7ee	Jonahtan beta	1	2	2019-08-28	1
2	lukman	aee943e4df51eb1680fb3df6b02c7aa8	lukman hakim	1	2	2019-10-05	1
18	sdfdsf	d41d8cd98f00b204e9800998ecf8427e	sdfsdf	1	1	2019-07-23	0
20	tarindo	88e13b0bb462de6fb76473bc1ec2390f	tarindo	1	2	2019-10-05	1
21	yoser	99c1c2c83c24d867aa1787cb980c0cc5	yoser	1	2	2019-10-05	0
22	gojek	ce8cc43a5d7b700e15ef0c51b292276c	gojek	1	2	2019-10-05	0
\.


                                                                                                                                                                                                                                                                                                                                                                                                                                 3412.dat                                                                                            0000600 0004000 0002000 00000000005 13607356547 0014256 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        \.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           3414.dat                                                                                            0000600 0004000 0002000 00000000005 13607356547 0014260 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        \.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           restore.sql                                                                                         0000600 0004000 0002000 00000403544 13607356547 0015416 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        --
-- NOTE:
--
-- File paths need to be edited. Search for $$PATH$$ and
-- replace it with the path to the directory containing
-- the extracted data files.
--
--
-- PostgreSQL database dump
--

-- Dumped from database version 12.1
-- Dumped by pg_dump version 12.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

DROP DATABASE beone_marufuji;
--
-- Name: beone_marufuji; Type: DATABASE; Schema: -; Owner: postgres
--

CREATE DATABASE beone_marufuji WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'English_Indonesia.1252' LC_CTYPE = 'English_Indonesia.1252';


ALTER DATABASE beone_marufuji OWNER TO postgres;

\connect beone_marufuji

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: get_stok(integer, integer, date, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.get_stok(p_item_id integer, p_gudang_id integer, p_trans_date date, p_intvent_trans_no character varying) RETURNS record
    LANGUAGE plpgsql
    AS $$
DECLARE
    res RECORD;
BEGIN
    select sum(x.stok_akhir) as stok, avg(hpp) hpp
    into res
    from (
             select 'saldo_awal' as type, coalesce(sum(it.saldo_qty), 0) stok_akhir, coalesce(sum(it.saldo_idr), 0) hpp
             from beone_item it
             where it.item_id = p_item_id
               and p_gudang_id = 1 --PATEN 1
             union
             select 'stok' as type, coalesce(sum(i.qty_in) - sum(i.qty_out), 0) stok_akhir, coalesce(avg(sa_unit_price),0) hpp
             from beone_inventory i
             where i.item_id = p_item_id
               and i.gudang_id = p_gudang_id
--                and i.trans_date <= p_trans_date
             union
             select 'transaksi' as type, coalesce(sum(i.qty_out) - sum(i.qty_in), 0) stok_akhir, coalesce(avg(sa_unit_price),0) hpp
             from beone_inventory i
             where i.item_id = p_item_id
--                and i.gudang_id = p_gudang_id
               and i.intvent_trans_no = p_intvent_trans_no) x;
    RETURN res;
END;
$$;


ALTER FUNCTION public.get_stok(p_item_id integer, p_gudang_id integer, p_trans_date date, p_intvent_trans_no character varying) OWNER TO postgres;

--
-- Name: set_stok(integer, integer, date, character varying, character varying, double precision, double precision, double precision); Type: PROCEDURE; Schema: public; Owner: postgres
--

CREATE PROCEDURE public.set_stok(p_item_id integer, p_gudang_id integer, p_trans_date date, p_intvent_trans_no character varying, p_keterangan character varying, p_qty_in double precision, p_qty_out double precision, price double precision)
    LANGUAGE plpgsql
    AS $$
DECLARE
    v_stok_akhir int;
    v_is_update  int;
    v_hpp float;
BEGIN
    --     SELECT get_stok(p_item_id, p_gudang_id, p_trans_date, p_intvent_trans_no) INTO v_stok_akhir;
    SELECT stok_akhir, hpp
    into v_stok_akhir, v_hpp
    FROM get_stok(p_item_id, p_gudang_id, p_trans_date, p_intvent_trans_no) AS (stok_akhir float, hpp float);

    IF (v_stok_akhir < p_qty_out) THEN
        RAISE EXCEPTION 'STOK TIDAK MENCUKUPI UNTUK ITEM (%) GUDANG (%) -> STOK SAAT INI (%), QTY INPUT (%)!', p_item_id, p_gudang_id, v_stok_akhir, p_qty_out;
    END IF;

    IF (p_qty_in > 0) AND (price > 0) THEN
        v_hpp = price;
    END IF;

    SELECT count(*)
    FROM beone_inventory i
    where i.intvent_trans_no = p_intvent_trans_no
--       and i.keterangan = p_keterangan
    INTO v_is_update;

    IF (v_is_update > 0) THEN
        IF (p_qty_out = 0 AND p_qty_in = 0) THEN
            delete from beone_inventory where intvent_trans_no = p_intvent_trans_no;
        ELSE
            update beone_inventory
            set qty_in    = p_qty_in,
                qty_out   = p_qty_out,
                gudang_id = p_gudang_id,
                sa_unit_price = v_hpp
            where intvent_trans_no = p_intvent_trans_no
              and item_id = p_item_id;
        END IF;
    ELSE
        INSERT INTO beone_inventory (intvent_trans_no, item_id, gudang_id, trans_date, keterangan, qty_in, qty_out, sa_unit_price)
        SELECT p_intvent_trans_no, p_item_id, p_gudang_id, p_trans_date, p_keterangan, p_qty_in, p_qty_out, v_hpp;
    END IF;
END
$$;


ALTER PROCEDURE public.set_stok(p_item_id integer, p_gudang_id integer, p_trans_date date, p_intvent_trans_no character varying, p_keterangan character varying, p_qty_in double precision, p_qty_out double precision, price double precision) OWNER TO postgres;

--
-- Name: trg_create_beone_export_detail(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.trg_create_beone_export_detail() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
    v_trans_id   int;
    v_trans_date date;
    v_trans_no   varchar(200);
    v_keterangan varchar(200);
BEGIN
    select export_header_id, delivery_no, delivery_date, 'EXPORT' as keterangan
    into v_trans_id, v_trans_no, v_trans_date, v_keterangan
    from beone_export_header
    where export_header_id = NEW.export_header_id;

    if (coalesce(v_trans_no, '') != '') then
        call set_stok(NEW.item_id, NEW.gudang_id, v_trans_date, v_trans_no, v_keterangan, 0, NEW.qty, 0);
    end if;

    RETURN NEW;
END;
$$;


ALTER FUNCTION public.trg_create_beone_export_detail() OWNER TO postgres;

--
-- Name: trg_create_beone_export_header(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.trg_create_beone_export_header() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
    update beone_export_detail set qty = qty where export_header_id = NEW.export_header_id;

    RETURN NEW;
END;
$$;


ALTER FUNCTION public.trg_create_beone_export_header() OWNER TO postgres;

--
-- Name: trg_create_beone_import_detail(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.trg_create_beone_import_detail() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
    v_trans_id   int;
    v_trans_date date;
    v_trans_no   varchar(200);
    v_keterangan varchar(200);
BEGIN
    select import_header_id, receive_no, receive_date, 'IMPORT' as keterangan
    into v_trans_id, v_trans_no, v_trans_date, v_keterangan
    from beone_import_header
    where import_header_id = NEW.import_header_id;

    if (coalesce(v_trans_no, '') != '') then
        call set_stok(NEW.item_id, NEW.gudang_id, v_trans_date, v_trans_no, v_keterangan, NEW.qty, 0, NEW.price);
    end if;

    RETURN NEW;
END;
$$;


ALTER FUNCTION public.trg_create_beone_import_detail() OWNER TO postgres;

--
-- Name: trg_create_beone_import_header(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.trg_create_beone_import_header() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
    update beone_import_detail set qty = qty where import_header_id = NEW.import_header_id;

    RETURN NEW;
END;
$$;


ALTER FUNCTION public.trg_create_beone_import_header() OWNER TO postgres;

--
-- Name: trg_create_beone_konversi_stok_detail(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.trg_create_beone_konversi_stok_detail() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
    v_trans_id   int;
    v_trans_date date;
    v_trans_no   varchar(200);
    v_keterangan varchar(200);
BEGIN
    select konversi_stok_header_id, konversi_stok_no, konversi_stok_date, 'KONVERSI STOK' as keterangan
    into v_trans_id, v_trans_no, v_trans_date, v_keterangan
    from beone_konversi_stok_header
    where konversi_stok_header_id = NEW.konversi_stok_header_id;

    if (coalesce(v_trans_no, '') != '') then
        call set_stok(NEW.item_id, NEW.gudang_id, v_trans_date, v_trans_no, v_keterangan, 0, NEW.qty_real, 0);
    end if;

    RETURN NEW;
END;
$$;


ALTER FUNCTION public.trg_create_beone_konversi_stok_detail() OWNER TO postgres;

--
-- Name: trg_create_beone_konversi_stok_header(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.trg_create_beone_konversi_stok_header() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
    v_trans_id   int;
    v_trans_date date;
    v_trans_no   varchar(200);
    v_keterangan varchar(200);
BEGIN
    v_trans_id = NEW.konversi_stok_header_id;
    v_trans_no = NEW.konversi_stok_no;
    v_trans_date = NEW.konversi_stok_date;
    v_keterangan = 'KONVERSI STOK IN';

    if (coalesce(v_trans_no, '') != '') then
        call set_stok(NEW.item_id, NEW.gudang_id, v_trans_date, v_trans_no, v_keterangan, NEW.qty_real, 0, 0);
    end if;

    RETURN NEW;
END;
$$;


ALTER FUNCTION public.trg_create_beone_konversi_stok_header() OWNER TO postgres;

--
-- Name: trg_create_beone_transfer_stock_detail(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.trg_create_beone_transfer_stock_detail() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
    v_trans_id   int;
    v_trans_date date;
    v_trans_no   varchar(200);
    v_keterangan varchar(200);
BEGIN
    select transfer_stock_id, transfer_no, transfer_date, 'TRANSFER STOCK' as keterangan
    into v_trans_id, v_trans_no, v_trans_date, v_keterangan
    from beone_transfer_stock
    where transfer_stock_id = NEW.transfer_stock_header_id;

    if (coalesce(v_trans_no, '') != '') then
        if (NEW.tipe_transfer_stock = 'BB') then
            call set_stok(NEW.item_id, NEW.gudang_id, v_trans_date, v_trans_no, v_keterangan, 0, NEW.qty, 0);
        elsif (NEW.tipe_transfer_stock = 'WIP') then
            call set_stok(NEW.item_id, NEW.gudang_id, v_trans_date, v_trans_no, v_keterangan, NEW.qty, 0, 0);
        end if;
    end if;

    RETURN NEW;
END;
$$;


ALTER FUNCTION public.trg_create_beone_transfer_stock_detail() OWNER TO postgres;

--
-- Name: trg_delete_beone_export_detail(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.trg_delete_beone_export_detail() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
    v_trans_id   int;
    v_trans_date date;
    v_trans_no   varchar(200);
    v_keterangan varchar(200);
BEGIN
    select export_header_id, delivery_no, delivery_date, 'EXPORT' as keterangan
    into v_trans_id, v_trans_no, v_trans_date, v_keterangan
    from beone_export_header
    where export_header_id = OLD.export_header_id;

    if (coalesce(v_trans_no, '') != '') then
        call set_stok(OLD.item_id, OLD.gudang_id, v_trans_date, v_trans_no, v_keterangan, 0, 0, 0);
    end if;

    RETURN OLD;
END;
$$;


ALTER FUNCTION public.trg_delete_beone_export_detail() OWNER TO postgres;

--
-- Name: trg_delete_beone_import_detail(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.trg_delete_beone_import_detail() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
    v_trans_id   int;
    v_trans_date date;
    v_trans_no   varchar(200);
    v_keterangan varchar(200);
BEGIN
    select import_header_id, receive_no, receive_date, 'IMPORT' as keterangan
    into v_trans_id, v_trans_no, v_trans_date, v_keterangan
    from beone_import_header
    where import_header_id = OLD.import_header_id;

    if (coalesce(v_trans_no, '') != '') then
        call set_stok(OLD.item_id, OLD.gudang_id, v_trans_date, v_trans_no, v_keterangan, 0, 0, 0);
    end if;

    RETURN OLD;
END;
$$;


ALTER FUNCTION public.trg_delete_beone_import_detail() OWNER TO postgres;

--
-- Name: trg_delete_beone_konversi_stok_detail(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.trg_delete_beone_konversi_stok_detail() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
    v_trans_id   int;
    v_trans_date date;
    v_trans_no   varchar(200);
    v_keterangan varchar(200);
BEGIN
    select konversi_stok_header_id, konversi_stok_no, konversi_stok_date, 'KONVERSI STOK' as keterangan
    into v_trans_id, v_trans_no, v_trans_date, v_keterangan
    from beone_konversi_stok_header
    where konversi_stok_header_id = OLD.konversi_stok_header_id;

    if (coalesce(v_trans_no, '') != '') then
        call set_stok(OLD.item_id, OLD.gudang_id, v_trans_date, v_trans_no, v_keterangan, 0, 0, 0);
    end if;

    RETURN OLD;
END;
$$;


ALTER FUNCTION public.trg_delete_beone_konversi_stok_detail() OWNER TO postgres;

--
-- Name: trg_delete_beone_konversi_stok_header(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.trg_delete_beone_konversi_stok_header() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
    v_trans_id   int;
    v_trans_date date;
    v_trans_no   varchar(200);
    v_keterangan varchar(200);
BEGIN
    v_trans_id = OLD.konversi_stok_header_id;
    v_trans_no = OLD.konversi_stok_no;
    v_trans_date = OLD.konversi_stok_date;
    v_keterangan = 'KONVERSI STOK IN';

    if (coalesce(v_trans_no, '') != '') then
        call set_stok(OLD.item_id, OLD.gudang_id, v_trans_date, v_trans_no, v_keterangan, 0, 0, 0);
    end if;

    RETURN OLD;
END;
$$;


ALTER FUNCTION public.trg_delete_beone_konversi_stok_header() OWNER TO postgres;

--
-- Name: trg_delete_beone_transfer_stock_detail(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.trg_delete_beone_transfer_stock_detail() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
    v_trans_id   int;
    v_trans_date date;
    v_trans_no   varchar(200);
    v_keterangan varchar(200);
BEGIN
    select transfer_stock_id, transfer_no, transfer_date, 'TRANSFER STOCK' as keterangan
    into v_trans_id, v_trans_no, v_trans_date, v_keterangan
    from beone_transfer_stock
    where transfer_stock_id = OLD.transfer_stock_header_id;

    if (coalesce(v_trans_no, '') != '') then
        call set_stok(OLD.item_id, OLD.gudang_id, v_trans_date, v_trans_no, v_keterangan, 0, 0, 0);
    end if;

    RETURN OLD;
END;
$$;


ALTER FUNCTION public.trg_delete_beone_transfer_stock_detail() OWNER TO postgres;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: beone_adjustment; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.beone_adjustment (
    adjustment_id bigint NOT NULL,
    adjustment_no character varying(20),
    adjustment_date date,
    keterangan character varying(100),
    item_id integer,
    qty_adjustment bigint,
    posisi_qty integer,
    update_by integer,
    update_date date,
    flag integer
);


ALTER TABLE public.beone_adjustment OWNER TO postgres;

--
-- Name: beone_adjustment_adjustment_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.beone_adjustment_adjustment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.beone_adjustment_adjustment_id_seq OWNER TO postgres;

--
-- Name: beone_adjustment_adjustment_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.beone_adjustment_adjustment_id_seq OWNED BY public.beone_adjustment.adjustment_id;


--
-- Name: beone_coa; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.beone_coa (
    coa_id integer NOT NULL,
    nama character varying(50),
    nomor character varying(50),
    tipe_akun integer,
    debet_valas double precision,
    debet_idr double precision,
    kredit_valas double precision,
    kredit_idr double precision,
    dk character varying,
    tipe_transaksi integer
);


ALTER TABLE public.beone_coa OWNER TO postgres;

--
-- Name: beone_coa_coa_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.beone_coa_coa_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.beone_coa_coa_id_seq OWNER TO postgres;

--
-- Name: beone_coa_coa_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.beone_coa_coa_id_seq OWNED BY public.beone_coa.coa_id;


--
-- Name: beone_coa_jurnal; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.beone_coa_jurnal (
    coa_jurnal_id integer NOT NULL,
    nama_coa_jurnal character varying(100),
    keterangan_coa_jurnal character varying(1000),
    coa_id integer,
    coa_no character varying(50)
);


ALTER TABLE public.beone_coa_jurnal OWNER TO postgres;

--
-- Name: beone_coa_jurnal_coa_jurnal_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.beone_coa_jurnal_coa_jurnal_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.beone_coa_jurnal_coa_jurnal_id_seq OWNER TO postgres;

--
-- Name: beone_coa_jurnal_coa_jurnal_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.beone_coa_jurnal_coa_jurnal_id_seq OWNED BY public.beone_coa_jurnal.coa_jurnal_id;


--
-- Name: beone_country; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.beone_country (
    country_id integer NOT NULL,
    nama character varying(50),
    country_code character varying(10),
    flag integer
);


ALTER TABLE public.beone_country OWNER TO postgres;

--
-- Name: beone_country_country_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.beone_country_country_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.beone_country_country_id_seq OWNER TO postgres;

--
-- Name: beone_country_country_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.beone_country_country_id_seq OWNED BY public.beone_country.country_id;


--
-- Name: beone_custsup; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.beone_custsup (
    custsup_id integer NOT NULL,
    nama character varying(100),
    alamat character varying(200),
    tipe_custsup integer,
    negara integer,
    saldo_hutang_idr double precision,
    saldo_hutang_valas double precision,
    saldo_piutang_idr double precision,
    saldo_piutang_valas double precision,
    flag integer,
    pelunasan_hutang_idr double precision,
    pelunasan_hutang_valas double precision,
    pelunasan_piutang_idr double precision,
    pelunasan_piutang_valas double precision,
    status_lunas_piutang integer,
    status_lunas_hutang integer
);


ALTER TABLE public.beone_custsup OWNER TO postgres;

--
-- Name: beone_custsup_custsup_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.beone_custsup_custsup_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.beone_custsup_custsup_id_seq OWNER TO postgres;

--
-- Name: beone_custsup_custsup_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.beone_custsup_custsup_id_seq OWNED BY public.beone_custsup.custsup_id;


--
-- Name: beone_export_detail; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.beone_export_detail (
    export_detail_id integer NOT NULL,
    export_header_id integer,
    item_id integer,
    qty double precision,
    satuan_qty integer,
    price double precision,
    pack_qty double precision,
    satuan_pack integer,
    origin_country integer,
    doc character varying(50),
    volume double precision,
    netto double precision,
    brutto double precision,
    flag integer,
    sales_header_id integer,
    gudang_id integer DEFAULT 1 NOT NULL
);


ALTER TABLE public.beone_export_detail OWNER TO postgres;

--
-- Name: beone_export_detail_export_detail_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.beone_export_detail_export_detail_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.beone_export_detail_export_detail_id_seq OWNER TO postgres;

--
-- Name: beone_export_detail_export_detail_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.beone_export_detail_export_detail_id_seq OWNED BY public.beone_export_detail.export_detail_id;


--
-- Name: beone_export_header; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.beone_export_header (
    export_header_id integer NOT NULL,
    jenis_bc integer,
    car_no character varying(50),
    bc_no character varying(50),
    bc_date date,
    kontrak_no character varying(50),
    kontrak_date date,
    jenis_export integer,
    invoice_no character varying(50),
    invoice_date date,
    surat_jalan_no character varying(50),
    surat_jalan_date date,
    receiver_id integer,
    country_id integer,
    price_type character varying(10),
    amount_value double precision,
    valas_value double precision,
    insurance_type character varying(10),
    insurance_value double precision,
    freight double precision,
    flag integer,
    status integer,
    delivery_date date,
    update_by integer,
    update_date date,
    delivery_no character varying(50),
    vessel character varying(100),
    port_loading character varying(100),
    port_destination character varying(100),
    container character varying(10),
    no_container character varying(25),
    no_seal character varying(25)
);


ALTER TABLE public.beone_export_header OWNER TO postgres;

--
-- Name: beone_export_header_export_header_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.beone_export_header_export_header_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.beone_export_header_export_header_id_seq OWNER TO postgres;

--
-- Name: beone_export_header_export_header_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.beone_export_header_export_header_id_seq OWNED BY public.beone_export_header.export_header_id;


--
-- Name: beone_gl; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.beone_gl (
    gl_id bigint NOT NULL,
    gl_date date,
    coa_id integer,
    coa_no character varying(20),
    coa_id_lawan integer,
    coa_no_lawan character varying(20),
    keterangan character varying(100),
    debet double precision,
    kredit double precision,
    pasangan_no character varying(50),
    gl_number character varying(50),
    update_by integer,
    update_date date
);


ALTER TABLE public.beone_gl OWNER TO postgres;

--
-- Name: beone_gl_gl_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.beone_gl_gl_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.beone_gl_gl_id_seq OWNER TO postgres;

--
-- Name: beone_gl_gl_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.beone_gl_gl_id_seq OWNED BY public.beone_gl.gl_id;


--
-- Name: beone_gudang; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.beone_gudang (
    gudang_id integer NOT NULL,
    nama character varying(100),
    keterangan character varying(200),
    flag integer
);


ALTER TABLE public.beone_gudang OWNER TO postgres;

--
-- Name: beone_gudang_detail; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.beone_gudang_detail (
    gudang_detail_id bigint NOT NULL,
    gudang_id integer,
    trans_date date,
    item_id integer,
    qty_in double precision,
    qty_out double precision,
    nomor_transaksi character varying(50),
    update_by integer,
    update_date date,
    flag integer,
    keterangan character varying(100),
    kode_tracing character varying(50)
);


ALTER TABLE public.beone_gudang_detail OWNER TO postgres;

--
-- Name: beone_gudang_detail_gudang_detail_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.beone_gudang_detail_gudang_detail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.beone_gudang_detail_gudang_detail_id_seq OWNER TO postgres;

--
-- Name: beone_gudang_detail_gudang_detail_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.beone_gudang_detail_gudang_detail_id_seq OWNED BY public.beone_gudang_detail.gudang_detail_id;


--
-- Name: beone_gudang_gudang_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.beone_gudang_gudang_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.beone_gudang_gudang_id_seq OWNER TO postgres;

--
-- Name: beone_gudang_gudang_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.beone_gudang_gudang_id_seq OWNED BY public.beone_gudang.gudang_id;


--
-- Name: beone_pemakaian_bahan_header; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.beone_pemakaian_bahan_header (
    pemakaian_header_id bigint NOT NULL,
    nomor_pemakaian character varying(50),
    keterangan character varying(250),
    tgl_pemakaian date,
    update_by integer,
    update_date date,
    nomor_pm character varying(50),
    amount_pemakaian double precision,
    status integer
);


ALTER TABLE public.beone_pemakaian_bahan_header OWNER TO postgres;

--
-- Name: beone_header_pemakaian_bahan_pemakaian_header_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.beone_header_pemakaian_bahan_pemakaian_header_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.beone_header_pemakaian_bahan_pemakaian_header_id_seq OWNER TO postgres;

--
-- Name: beone_header_pemakaian_bahan_pemakaian_header_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.beone_header_pemakaian_bahan_pemakaian_header_id_seq OWNED BY public.beone_pemakaian_bahan_header.pemakaian_header_id;


--
-- Name: beone_hutang_piutang; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.beone_hutang_piutang (
    hutang_piutang_id bigint NOT NULL,
    custsup_id integer,
    trans_date date,
    nomor character varying(50),
    keterangan character varying(100),
    valas_trans double precision,
    idr_trans double precision,
    valas_pelunasan double precision,
    idr_pelunasan double precision,
    tipe_trans integer,
    update_by integer,
    update_date date,
    flag integer,
    status_lunas integer
);


ALTER TABLE public.beone_hutang_piutang OWNER TO postgres;

--
-- Name: beone_hutang_piutang_hutang_piutang_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.beone_hutang_piutang_hutang_piutang_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.beone_hutang_piutang_hutang_piutang_id_seq OWNER TO postgres;

--
-- Name: beone_hutang_piutang_hutang_piutang_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.beone_hutang_piutang_hutang_piutang_id_seq OWNED BY public.beone_hutang_piutang.hutang_piutang_id;


--
-- Name: beone_import_detail; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.beone_import_detail (
    import_detail_id integer NOT NULL,
    item_id integer,
    qty double precision,
    satuan_qty integer,
    price double precision,
    pack_qty double precision,
    satuan_pack integer,
    origin_country integer,
    volume double precision,
    netto double precision,
    brutto double precision,
    hscode character varying(50),
    tbm double precision,
    ppnn double precision,
    tpbm double precision,
    cukai integer,
    sat_cukai integer,
    cukai_value double precision,
    bea_masuk character varying(100),
    sat_bea_masuk integer,
    flag integer,
    item_type_id integer,
    import_header_id integer,
    gudang_id integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.beone_import_detail OWNER TO postgres;

--
-- Name: beone_import_detail_import_detail_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.beone_import_detail_import_detail_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.beone_import_detail_import_detail_id_seq OWNER TO postgres;

--
-- Name: beone_import_detail_import_detail_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.beone_import_detail_import_detail_id_seq OWNED BY public.beone_import_detail.import_detail_id;


--
-- Name: beone_import_header; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.beone_import_header (
    import_header_id integer NOT NULL,
    jenis_bc integer,
    car_no character varying(50),
    bc_no character varying(50),
    bc_date date,
    invoice_no character varying(50),
    invoice_date date,
    kontrak_no character varying(50),
    kontrak_date date,
    purpose_id character varying(100),
    supplier_id integer,
    price_type character varying(10),
    "from" character varying(10),
    amount_value double precision,
    valas_value double precision,
    value_added double precision,
    discount double precision,
    insurance_type character varying(10),
    insurace_value double precision,
    freight double precision,
    flag integer,
    update_by integer,
    update_date date,
    status integer,
    receive_date date,
    receive_no character varying(50)
);


ALTER TABLE public.beone_import_header OWNER TO postgres;

--
-- Name: beone_import_header_import_header_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.beone_import_header_import_header_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.beone_import_header_import_header_id_seq OWNER TO postgres;

--
-- Name: beone_import_header_import_header_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.beone_import_header_import_header_id_seq OWNED BY public.beone_import_header.import_header_id;


--
-- Name: beone_inventory; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.beone_inventory (
    intvent_trans_id bigint NOT NULL,
    intvent_trans_no character varying(50) NOT NULL,
    item_id integer,
    trans_date date NOT NULL,
    keterangan character varying(100) NOT NULL,
    qty_in double precision,
    value_in double precision,
    qty_out double precision,
    value_out double precision,
    sa_qty double precision,
    sa_unit_price double precision,
    sa_amount double precision,
    flag integer,
    update_by integer,
    update_date date,
    gudang_id integer DEFAULT 1 NOT NULL
);


ALTER TABLE public.beone_inventory OWNER TO postgres;

--
-- Name: beone_inventory_intvent_trans_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.beone_inventory_intvent_trans_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.beone_inventory_intvent_trans_id_seq OWNER TO postgres;

--
-- Name: beone_inventory_intvent_trans_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.beone_inventory_intvent_trans_id_seq OWNED BY public.beone_inventory.intvent_trans_id;


--
-- Name: beone_item; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.beone_item (
    item_id integer NOT NULL,
    nama character varying(100),
    item_code character varying(20),
    saldo_qty double precision,
    saldo_idr double precision,
    keterangan character varying(200),
    flag integer,
    item_type_id integer,
    hscode character varying(50),
    satuan_id integer,
    item_category_id integer DEFAULT 1 NOT NULL
);


ALTER TABLE public.beone_item OWNER TO postgres;

--
-- Name: beone_item_category; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.beone_item_category (
    item_category_id integer NOT NULL,
    nama character varying(100),
    keterangan character varying(200),
    flag integer
);


ALTER TABLE public.beone_item_category OWNER TO postgres;

--
-- Name: beone_item_item_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.beone_item_item_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.beone_item_item_id_seq OWNER TO postgres;

--
-- Name: beone_item_item_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.beone_item_item_id_seq OWNED BY public.beone_item.item_id;


--
-- Name: beone_item_type; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.beone_item_type (
    item_type_id integer NOT NULL,
    nama character varying(100),
    keterangan character varying(200),
    flag integer
);


ALTER TABLE public.beone_item_type OWNER TO postgres;

--
-- Name: beone_item_type_item_type_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.beone_item_type_item_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.beone_item_type_item_type_id_seq OWNER TO postgres;

--
-- Name: beone_item_type_item_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.beone_item_type_item_type_id_seq OWNED BY public.beone_item_type.item_type_id;


--
-- Name: beone_kode_trans; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.beone_kode_trans (
    kode_trans_id integer NOT NULL,
    nama character varying,
    coa_id integer,
    kode_bank character varying(20),
    in_out integer,
    flag integer
);


ALTER TABLE public.beone_kode_trans OWNER TO postgres;

--
-- Name: beone_kode_trans_kode_trans_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.beone_kode_trans_kode_trans_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.beone_kode_trans_kode_trans_id_seq OWNER TO postgres;

--
-- Name: beone_kode_trans_kode_trans_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.beone_kode_trans_kode_trans_id_seq OWNED BY public.beone_kode_trans.kode_trans_id;


--
-- Name: beone_komposisi; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.beone_komposisi (
    komposisi_id integer NOT NULL,
    item_jadi_id integer,
    item_baku_id integer,
    qty_item_baku double precision,
    flag integer
);


ALTER TABLE public.beone_komposisi OWNER TO postgres;

--
-- Name: beone_komposisi_komposisi_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.beone_komposisi_komposisi_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.beone_komposisi_komposisi_id_seq OWNER TO postgres;

--
-- Name: beone_komposisi_komposisi_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.beone_komposisi_komposisi_id_seq OWNED BY public.beone_komposisi.komposisi_id;


--
-- Name: beone_konfigurasi_perusahaan; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.beone_konfigurasi_perusahaan (
    nama_perusahaan character varying(100),
    alamat_perusahaan character varying(500),
    kota_perusahaan character varying(100),
    provinsi_perusahaan character varying(100),
    kode_pos_perusahaan character varying(50),
    logo_perusahaan character varying(1000)
);


ALTER TABLE public.beone_konfigurasi_perusahaan OWNER TO postgres;

--
-- Name: beone_konversi_stok_detail; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.beone_konversi_stok_detail (
    konversi_stok_detail_id integer NOT NULL,
    konversi_stok_header_id integer,
    item_id integer,
    qty double precision,
    qty_real double precision,
    satuan_qty integer,
    gudang_id integer DEFAULT 1 NOT NULL,
    qty_allocation integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.beone_konversi_stok_detail OWNER TO postgres;

--
-- Name: beone_konversi_stok_detail_konversi_stok_detail_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.beone_konversi_stok_detail_konversi_stok_detail_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.beone_konversi_stok_detail_konversi_stok_detail_id_seq OWNER TO postgres;

--
-- Name: beone_konversi_stok_detail_konversi_stok_detail_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.beone_konversi_stok_detail_konversi_stok_detail_id_seq OWNED BY public.beone_konversi_stok_detail.konversi_stok_detail_id;


--
-- Name: beone_konversi_stok_header; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.beone_konversi_stok_header (
    konversi_stok_header_id integer NOT NULL,
    item_id integer,
    konversi_stok_no character varying(255),
    konversi_stok_date date,
    flag integer,
    qty double precision,
    satuan_qty integer,
    gudang_id integer DEFAULT 0 NOT NULL,
    qty_real integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.beone_konversi_stok_header OWNER TO postgres;

--
-- Name: beone_konversi_stok_header_konversi_stok_header_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.beone_konversi_stok_header_konversi_stok_header_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.beone_konversi_stok_header_konversi_stok_header_id_seq OWNER TO postgres;

--
-- Name: beone_konversi_stok_header_konversi_stok_header_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.beone_konversi_stok_header_konversi_stok_header_id_seq OWNED BY public.beone_konversi_stok_header.konversi_stok_header_id;


--
-- Name: beone_log; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.beone_log (
    log_id bigint NOT NULL,
    log_user character varying(100),
    log_tipe integer,
    log_desc character varying(250),
    log_time timestamp without time zone
);


ALTER TABLE public.beone_log OWNER TO postgres;

--
-- Name: beone_log_log_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.beone_log_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.beone_log_log_id_seq OWNER TO postgres;

--
-- Name: beone_log_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.beone_log_log_id_seq OWNED BY public.beone_log.log_id;


--
-- Name: beone_opname_detail; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.beone_opname_detail (
    opname_detail_id integer NOT NULL,
    opname_header_id integer,
    item_id integer,
    qty_existing double precision,
    qty_opname double precision,
    qty_selisih double precision,
    status_opname integer,
    flag integer
);


ALTER TABLE public.beone_opname_detail OWNER TO postgres;

--
-- Name: beone_opname_detail_opname_detail_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.beone_opname_detail_opname_detail_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.beone_opname_detail_opname_detail_id_seq OWNER TO postgres;

--
-- Name: beone_opname_detail_opname_detail_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.beone_opname_detail_opname_detail_id_seq OWNED BY public.beone_opname_detail.opname_detail_id;


--
-- Name: beone_opname_header; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.beone_opname_header (
    opname_header_id integer NOT NULL,
    document_no character varying(50),
    opname_date date,
    total_item_opname integer,
    total_item_opname_match integer,
    total_item_opname_plus integer,
    total_item_opname_min integer,
    update_by integer,
    flag integer,
    keterangan character varying(100),
    update_date date
);


ALTER TABLE public.beone_opname_header OWNER TO postgres;

--
-- Name: beone_opname_header_opname_header_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.beone_opname_header_opname_header_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.beone_opname_header_opname_header_id_seq OWNER TO postgres;

--
-- Name: beone_opname_header_opname_header_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.beone_opname_header_opname_header_id_seq OWNED BY public.beone_opname_header.opname_header_id;


--
-- Name: beone_peleburan; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.beone_peleburan (
    peleburan_id integer NOT NULL,
    peleburan_no character varying(20),
    peleburan_date date,
    keterangan character varying(100),
    item_id integer,
    qty_peleburan bigint,
    update_by integer,
    update_date date,
    flag integer
);


ALTER TABLE public.beone_peleburan OWNER TO postgres;

--
-- Name: beone_peleburan_peleburan_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.beone_peleburan_peleburan_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.beone_peleburan_peleburan_id_seq OWNER TO postgres;

--
-- Name: beone_peleburan_peleburan_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.beone_peleburan_peleburan_id_seq OWNED BY public.beone_peleburan.peleburan_id;


--
-- Name: beone_pemakaian_bahan_detail; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.beone_pemakaian_bahan_detail (
    pemakaian_detail_id bigint NOT NULL,
    pemakaian_header_id integer,
    item_id integer,
    qty double precision,
    unit_price double precision
);


ALTER TABLE public.beone_pemakaian_bahan_detail OWNER TO postgres;

--
-- Name: beone_pemakaian_bahan_detail_pemakaian_detail_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.beone_pemakaian_bahan_detail_pemakaian_detail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.beone_pemakaian_bahan_detail_pemakaian_detail_id_seq OWNER TO postgres;

--
-- Name: beone_pemakaian_bahan_detail_pemakaian_detail_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.beone_pemakaian_bahan_detail_pemakaian_detail_id_seq OWNED BY public.beone_pemakaian_bahan_detail.pemakaian_detail_id;


--
-- Name: beone_pm_detail; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.beone_pm_detail (
    pm_detail_id integer NOT NULL,
    item_id integer,
    qty double precision,
    satuan_id integer,
    pm_header_id integer
);


ALTER TABLE public.beone_pm_detail OWNER TO postgres;

--
-- Name: beone_pm_detail_pm_detail_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.beone_pm_detail_pm_detail_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.beone_pm_detail_pm_detail_id_seq OWNER TO postgres;

--
-- Name: beone_pm_detail_pm_detail_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.beone_pm_detail_pm_detail_id_seq OWNED BY public.beone_pm_detail.pm_detail_id;


--
-- Name: beone_pm_header; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.beone_pm_header (
    pm_header_id bigint NOT NULL,
    no_pm character varying(50),
    tgl_pm date,
    customer_id integer,
    qty double precision,
    keterangan_artikel character varying,
    status integer
);


ALTER TABLE public.beone_pm_header OWNER TO postgres;

--
-- Name: beone_pm_header_pm_header_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.beone_pm_header_pm_header_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.beone_pm_header_pm_header_id_seq OWNER TO postgres;

--
-- Name: beone_pm_header_pm_header_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.beone_pm_header_pm_header_id_seq OWNED BY public.beone_pm_header.pm_header_id;


--
-- Name: beone_po_import_detail; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.beone_po_import_detail (
    purchase_detail_id bigint NOT NULL,
    purchase_header_id integer,
    item_id integer,
    qty double precision,
    price double precision,
    amount double precision,
    flag integer
);


ALTER TABLE public.beone_po_import_detail OWNER TO postgres;

--
-- Name: beone_po_import_detail_purchase_detail_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.beone_po_import_detail_purchase_detail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.beone_po_import_detail_purchase_detail_id_seq OWNER TO postgres;

--
-- Name: beone_po_import_detail_purchase_detail_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.beone_po_import_detail_purchase_detail_id_seq OWNED BY public.beone_po_import_detail.purchase_detail_id;


--
-- Name: beone_po_import_header; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.beone_po_import_header (
    purchase_header_id bigint NOT NULL,
    purchase_no character varying(50),
    trans_date date,
    supplier_id integer,
    keterangan character varying(250),
    grandtotal double precision,
    flag integer,
    update_by integer,
    update_date date
);


ALTER TABLE public.beone_po_import_header OWNER TO postgres;

--
-- Name: beone_po_import_header_purchase_header_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.beone_po_import_header_purchase_header_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.beone_po_import_header_purchase_header_id_seq OWNER TO postgres;

--
-- Name: beone_po_import_header_purchase_header_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.beone_po_import_header_purchase_header_id_seq OWNED BY public.beone_po_import_header.purchase_header_id;


--
-- Name: beone_produksi_detail; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.beone_produksi_detail (
    produksi_detail_id bigint NOT NULL,
    produksi_header_id integer,
    pemakaian_header_id integer
);


ALTER TABLE public.beone_produksi_detail OWNER TO postgres;

--
-- Name: beone_produksi_detail_produksi_detail_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.beone_produksi_detail_produksi_detail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.beone_produksi_detail_produksi_detail_id_seq OWNER TO postgres;

--
-- Name: beone_produksi_detail_produksi_detail_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.beone_produksi_detail_produksi_detail_id_seq OWNED BY public.beone_produksi_detail.produksi_detail_id;


--
-- Name: beone_produksi_header; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.beone_produksi_header (
    produksi_header_id bigint NOT NULL,
    nomor_produksi character varying(20),
    tgl_produksi date,
    tipe_produksi integer,
    item_produksi integer,
    qty_hasil_produksi double precision,
    keterangan character varying(250),
    flag integer,
    updated_by integer,
    udpated_date date,
    pm_header_id integer
);


ALTER TABLE public.beone_produksi_header OWNER TO postgres;

--
-- Name: beone_produksi_header_produksi_header_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.beone_produksi_header_produksi_header_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.beone_produksi_header_produksi_header_id_seq OWNER TO postgres;

--
-- Name: beone_produksi_header_produksi_header_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.beone_produksi_header_produksi_header_id_seq OWNED BY public.beone_produksi_header.produksi_header_id;


--
-- Name: beone_purchase_detail; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.beone_purchase_detail (
    purchase_detail_id bigint NOT NULL,
    purchase_header_id integer,
    item_id integer,
    qty double precision,
    price double precision,
    amount double precision,
    flag integer
);


ALTER TABLE public.beone_purchase_detail OWNER TO postgres;

--
-- Name: beone_purchase_detail_purchase_detail_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.beone_purchase_detail_purchase_detail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.beone_purchase_detail_purchase_detail_id_seq OWNER TO postgres;

--
-- Name: beone_purchase_detail_purchase_detail_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.beone_purchase_detail_purchase_detail_id_seq OWNED BY public.beone_purchase_detail.purchase_detail_id;


--
-- Name: beone_purchase_header; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.beone_purchase_header (
    purchase_header_id bigint NOT NULL,
    purchase_no character varying(50),
    trans_date date,
    supplier_id integer,
    keterangan character varying(200),
    update_by integer,
    update_date date,
    flag integer,
    ppn integer,
    subtotal double precision,
    ppn_value double precision,
    grandtotal double precision,
    realisasi integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.beone_purchase_header OWNER TO postgres;

--
-- Name: beone_purchase_header_purchase_header_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.beone_purchase_header_purchase_header_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.beone_purchase_header_purchase_header_id_seq OWNER TO postgres;

--
-- Name: beone_purchase_header_purchase_header_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.beone_purchase_header_purchase_header_id_seq OWNED BY public.beone_purchase_header.purchase_header_id;


--
-- Name: beone_received_import; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.beone_received_import (
    received_id bigint NOT NULL,
    nomor_aju character varying(50),
    nomor_dokumen_pabean character varying(10),
    tanggal_received date,
    status_received integer,
    flag integer,
    update_by integer,
    update_date date,
    nomor_received character varying(50),
    tpb_header_id integer,
    kurs double precision,
    po_import_header_id integer
);


ALTER TABLE public.beone_received_import OWNER TO postgres;

--
-- Name: beone_received_import_received_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.beone_received_import_received_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.beone_received_import_received_id_seq OWNER TO postgres;

--
-- Name: beone_received_import_received_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.beone_received_import_received_id_seq OWNED BY public.beone_received_import.received_id;


--
-- Name: beone_role; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.beone_role (
    role_id integer NOT NULL,
    nama_role character varying(50),
    keterangan character varying(200),
    master_menu integer,
    pembelian_menu integer,
    penjualan_menu integer,
    inventory_menu integer,
    produksi_menu integer,
    asset_menu integer,
    jurnal_umum_menu integer,
    kasbank_menu integer,
    laporan_inventory integer,
    laporan_keuangan integer,
    konfigurasi integer,
    import_menu integer,
    eksport_menu integer
);


ALTER TABLE public.beone_role OWNER TO postgres;

--
-- Name: beone_role_role_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.beone_role_role_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.beone_role_role_id_seq OWNER TO postgres;

--
-- Name: beone_role_role_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.beone_role_role_id_seq OWNED BY public.beone_role.role_id;


--
-- Name: beone_role_user; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.beone_role_user (
    role_id integer NOT NULL,
    nama_role character varying(50),
    keterangan character varying(200),
    master_menu integer,
    user_add integer,
    user_edit integer,
    user_delete integer,
    role_add integer,
    role_edit integer,
    role_delete integer,
    customer_add integer,
    customer_edit integer,
    customer_delete integer,
    supplier_add integer,
    supplier_edit integer,
    supplier_delete integer,
    item_add integer,
    item_edit integer,
    item_delete integer,
    jenis_add integer,
    jenis_edit integer,
    jenis_delete integer,
    satuan_add integer,
    satuan_edit integer,
    satuan_delete integer,
    gudang_add integer,
    gudang_edit integer,
    gudang_delete integer,
    master_import integer,
    po_add integer,
    po_edit integer,
    po_delete integer,
    tracing integer,
    terima_barang integer,
    master_pembelian integer,
    pembelian_add integer,
    pembelian_edit integer,
    pembelian_delete integer,
    kredit_note_add integer,
    kredit_note_edit integer,
    kredit_note_delete integer,
    master_eksport integer,
    eksport_add integer,
    eksport_edit integer,
    eksport_delete integer,
    kirim_barang integer,
    menu_penjualan integer,
    penjualan_add integer,
    penjualan_edit integer,
    penjualan_delete integer,
    debit_note_add integer,
    debit_note_edit integer,
    debit_note_delete integer,
    menu_inventory integer,
    pindah_gudang integer,
    stockopname_add integer,
    stockopname_edit integer,
    stockopname_delete integer,
    stockopname_opname integer,
    adjustment_add integer,
    adjustment_edit integer,
    adjustment_delete integer,
    pemusnahan_add integer,
    pemusnahan_edit integer,
    pemusnahan_delete integer,
    recal_inventory integer,
    menu_produksi integer,
    produksi_add integer,
    produksi_edit integer,
    produksi_delete integer,
    menu_asset integer,
    menu_jurnal_umum integer,
    jurnal_umum_add integer,
    jurnal_umum_edit integer,
    jurnal_umum_delete integer,
    menu_kas_bank integer,
    kas_bank_add integer,
    kas_bank_edit integer,
    kas_bank_delete integer,
    menu_laporan_inventory integer,
    menu_laporan_keuangan integer,
    menu_konfigurasi integer
);


ALTER TABLE public.beone_role_user OWNER TO postgres;

--
-- Name: beone_role_user_role_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.beone_role_user_role_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.beone_role_user_role_id_seq OWNER TO postgres;

--
-- Name: beone_role_user_role_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.beone_role_user_role_id_seq OWNED BY public.beone_role_user.role_id;


--
-- Name: beone_sales_detail; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.beone_sales_detail (
    sales_detail_id bigint NOT NULL,
    sales_header_id integer,
    item_id integer,
    qty double precision,
    price double precision,
    amount double precision,
    flag integer
);


ALTER TABLE public.beone_sales_detail OWNER TO postgres;

--
-- Name: beone_sales_detail_sales_detail_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.beone_sales_detail_sales_detail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.beone_sales_detail_sales_detail_id_seq OWNER TO postgres;

--
-- Name: beone_sales_detail_sales_detail_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.beone_sales_detail_sales_detail_id_seq OWNED BY public.beone_sales_detail.sales_detail_id;


--
-- Name: beone_sales_header; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.beone_sales_header (
    sales_header_id bigint NOT NULL,
    sales_no character varying(50),
    trans_date date,
    customer_id integer,
    keterangan character varying(200),
    ppn double precision,
    subtotal double precision,
    ppn_value double precision,
    grandtotal double precision,
    update_by integer,
    update_date date,
    flag integer,
    biaya double precision,
    status integer,
    realisasi integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.beone_sales_header OWNER TO postgres;

--
-- Name: beone_sales_header_sales_header_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.beone_sales_header_sales_header_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.beone_sales_header_sales_header_id_seq OWNER TO postgres;

--
-- Name: beone_sales_header_sales_header_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.beone_sales_header_sales_header_id_seq OWNED BY public.beone_sales_header.sales_header_id;


--
-- Name: beone_satuan_item; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.beone_satuan_item (
    satuan_id integer NOT NULL,
    satuan_code character varying(5),
    keterangan character varying(100),
    flag integer
);


ALTER TABLE public.beone_satuan_item OWNER TO postgres;

--
-- Name: beone_satuan_item_satuan_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.beone_satuan_item_satuan_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.beone_satuan_item_satuan_id_seq OWNER TO postgres;

--
-- Name: beone_satuan_item_satuan_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.beone_satuan_item_satuan_id_seq OWNED BY public.beone_satuan_item.satuan_id;


--
-- Name: beone_subkon_in_detail; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.beone_subkon_in_detail (
    subkon_in_detail_id bigint NOT NULL,
    subkon_in_header_id integer,
    item_id integer,
    qty double precision,
    flag integer
);


ALTER TABLE public.beone_subkon_in_detail OWNER TO postgres;

--
-- Name: beone_subkon_in_detail_subkon_in_detail_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.beone_subkon_in_detail_subkon_in_detail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.beone_subkon_in_detail_subkon_in_detail_id_seq OWNER TO postgres;

--
-- Name: beone_subkon_in_detail_subkon_in_detail_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.beone_subkon_in_detail_subkon_in_detail_id_seq OWNED BY public.beone_subkon_in_detail.subkon_in_detail_id;


--
-- Name: beone_subkon_in_header; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.beone_subkon_in_header (
    subkon_in_id bigint NOT NULL,
    no_subkon_in character varying(50),
    no_subkon_out character varying(50),
    trans_date date,
    keterangan character varying(250),
    nomor_aju character varying(50),
    biaya_subkon double precision,
    supplier_id integer,
    flag integer,
    update_by integer,
    update_date date
);


ALTER TABLE public.beone_subkon_in_header OWNER TO postgres;

--
-- Name: beone_subkon_in_header_subkon_in_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.beone_subkon_in_header_subkon_in_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.beone_subkon_in_header_subkon_in_id_seq OWNER TO postgres;

--
-- Name: beone_subkon_in_header_subkon_in_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.beone_subkon_in_header_subkon_in_id_seq OWNED BY public.beone_subkon_in_header.subkon_in_id;


--
-- Name: beone_subkon_out_detail; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.beone_subkon_out_detail (
    subkon_out_detail_id bigint NOT NULL,
    subkon_out_header_id integer,
    item_id integer,
    qty double precision,
    unit_price double precision,
    flag integer
);


ALTER TABLE public.beone_subkon_out_detail OWNER TO postgres;

--
-- Name: beone_subkon_out_detail_subkon_out_detail_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.beone_subkon_out_detail_subkon_out_detail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.beone_subkon_out_detail_subkon_out_detail_id_seq OWNER TO postgres;

--
-- Name: beone_subkon_out_detail_subkon_out_detail_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.beone_subkon_out_detail_subkon_out_detail_id_seq OWNED BY public.beone_subkon_out_detail.subkon_out_detail_id;


--
-- Name: beone_subkon_out_header; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.beone_subkon_out_header (
    subkon_out_id bigint NOT NULL,
    no_subkon_out character varying(50),
    trans_date date,
    supplier_id integer,
    keterangan character varying(250),
    nomor_aju character varying(50),
    flag integer,
    update_by integer,
    update_date date
);


ALTER TABLE public.beone_subkon_out_header OWNER TO postgres;

--
-- Name: beone_subkon_out_header_subkon_out_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.beone_subkon_out_header_subkon_out_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.beone_subkon_out_header_subkon_out_id_seq OWNER TO postgres;

--
-- Name: beone_subkon_out_header_subkon_out_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.beone_subkon_out_header_subkon_out_id_seq OWNED BY public.beone_subkon_out_header.subkon_out_id;


--
-- Name: beone_tipe_coa; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.beone_tipe_coa (
    tipe_coa_id integer NOT NULL,
    nama character varying(50)
);


ALTER TABLE public.beone_tipe_coa OWNER TO postgres;

--
-- Name: beone_tipe_coa_tipe_coa_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.beone_tipe_coa_tipe_coa_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.beone_tipe_coa_tipe_coa_id_seq OWNER TO postgres;

--
-- Name: beone_tipe_coa_tipe_coa_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.beone_tipe_coa_tipe_coa_id_seq OWNED BY public.beone_tipe_coa.tipe_coa_id;


--
-- Name: beone_tipe_coa_transaksi; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.beone_tipe_coa_transaksi (
    tipe_coa_trans_id integer NOT NULL,
    nama character varying(50),
    flag integer
);


ALTER TABLE public.beone_tipe_coa_transaksi OWNER TO postgres;

--
-- Name: beone_tipe_coa_transaksi_tipe_coa_trans_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.beone_tipe_coa_transaksi_tipe_coa_trans_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.beone_tipe_coa_transaksi_tipe_coa_trans_id_seq OWNER TO postgres;

--
-- Name: beone_tipe_coa_transaksi_tipe_coa_trans_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.beone_tipe_coa_transaksi_tipe_coa_trans_id_seq OWNED BY public.beone_tipe_coa_transaksi.tipe_coa_trans_id;


--
-- Name: beone_transfer_stock; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.beone_transfer_stock (
    transfer_stock_id bigint NOT NULL,
    transfer_no character varying(50),
    transfer_date date,
    coa_kode_biaya integer,
    keterangan character varying(200),
    update_by integer,
    update_date date,
    flag integer
);


ALTER TABLE public.beone_transfer_stock OWNER TO postgres;

--
-- Name: beone_transfer_stock_detail; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.beone_transfer_stock_detail (
    transfer_stock_detail_id bigint NOT NULL,
    transfer_stock_header_id integer,
    tipe_transfer_stock character varying(10),
    item_id integer,
    qty double precision,
    gudang_id integer,
    biaya double precision,
    update_by integer,
    update_date date,
    flag integer,
    persen_produksi integer
);


ALTER TABLE public.beone_transfer_stock_detail OWNER TO postgres;

--
-- Name: beone_transfer_stock_detail_transfer_stock_detail_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.beone_transfer_stock_detail_transfer_stock_detail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.beone_transfer_stock_detail_transfer_stock_detail_id_seq OWNER TO postgres;

--
-- Name: beone_transfer_stock_detail_transfer_stock_detail_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.beone_transfer_stock_detail_transfer_stock_detail_id_seq OWNED BY public.beone_transfer_stock_detail.transfer_stock_detail_id;


--
-- Name: beone_transfer_stock_transfer_stock_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.beone_transfer_stock_transfer_stock_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.beone_transfer_stock_transfer_stock_id_seq OWNER TO postgres;

--
-- Name: beone_transfer_stock_transfer_stock_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.beone_transfer_stock_transfer_stock_id_seq OWNED BY public.beone_transfer_stock.transfer_stock_id;


--
-- Name: beone_user; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.beone_user (
    user_id integer NOT NULL,
    username character varying(50),
    password character varying(50),
    nama character varying(100),
    role_id integer,
    update_by integer,
    update_date date,
    flag integer
);


ALTER TABLE public.beone_user OWNER TO postgres;

--
-- Name: beone_user_user_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.beone_user_user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.beone_user_user_id_seq OWNER TO postgres;

--
-- Name: beone_user_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.beone_user_user_id_seq OWNED BY public.beone_user.user_id;


--
-- Name: beone_voucher_detail; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.beone_voucher_detail (
    voucher_detail_id bigint NOT NULL,
    voucher_header_id integer,
    coa_id_lawan integer,
    coa_no_lawan character varying(20),
    jumlah_valas double precision,
    kurs double precision,
    jumlah_idr double precision,
    keterangan_detail character varying(200)
);


ALTER TABLE public.beone_voucher_detail OWNER TO postgres;

--
-- Name: beone_voucher_detail_voucher_detail_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.beone_voucher_detail_voucher_detail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.beone_voucher_detail_voucher_detail_id_seq OWNER TO postgres;

--
-- Name: beone_voucher_detail_voucher_detail_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.beone_voucher_detail_voucher_detail_id_seq OWNED BY public.beone_voucher_detail.voucher_detail_id;


--
-- Name: beone_voucher_header; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.beone_voucher_header (
    voucher_header_id bigint NOT NULL,
    voucher_number character varying(20),
    voucher_date date,
    tipe integer,
    keterangan character varying(200),
    coa_id_cash_bank integer,
    coa_no character varying(20),
    update_by integer,
    update_date date
);


ALTER TABLE public.beone_voucher_header OWNER TO postgres;

--
-- Name: beone_voucher_header_voucher_header_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.beone_voucher_header_voucher_header_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.beone_voucher_header_voucher_header_id_seq OWNER TO postgres;

--
-- Name: beone_voucher_header_voucher_header_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.beone_voucher_header_voucher_header_id_seq OWNED BY public.beone_voucher_header.voucher_header_id;


--
-- Name: beone_adjustment adjustment_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beone_adjustment ALTER COLUMN adjustment_id SET DEFAULT nextval('public.beone_adjustment_adjustment_id_seq'::regclass);


--
-- Name: beone_coa coa_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beone_coa ALTER COLUMN coa_id SET DEFAULT nextval('public.beone_coa_coa_id_seq'::regclass);


--
-- Name: beone_coa_jurnal coa_jurnal_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beone_coa_jurnal ALTER COLUMN coa_jurnal_id SET DEFAULT nextval('public.beone_coa_jurnal_coa_jurnal_id_seq'::regclass);


--
-- Name: beone_country country_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beone_country ALTER COLUMN country_id SET DEFAULT nextval('public.beone_country_country_id_seq'::regclass);


--
-- Name: beone_custsup custsup_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beone_custsup ALTER COLUMN custsup_id SET DEFAULT nextval('public.beone_custsup_custsup_id_seq'::regclass);


--
-- Name: beone_export_detail export_detail_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beone_export_detail ALTER COLUMN export_detail_id SET DEFAULT nextval('public.beone_export_detail_export_detail_id_seq'::regclass);


--
-- Name: beone_export_header export_header_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beone_export_header ALTER COLUMN export_header_id SET DEFAULT nextval('public.beone_export_header_export_header_id_seq'::regclass);


--
-- Name: beone_gl gl_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beone_gl ALTER COLUMN gl_id SET DEFAULT nextval('public.beone_gl_gl_id_seq'::regclass);


--
-- Name: beone_gudang gudang_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beone_gudang ALTER COLUMN gudang_id SET DEFAULT nextval('public.beone_gudang_gudang_id_seq'::regclass);


--
-- Name: beone_gudang_detail gudang_detail_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beone_gudang_detail ALTER COLUMN gudang_detail_id SET DEFAULT nextval('public.beone_gudang_detail_gudang_detail_id_seq'::regclass);


--
-- Name: beone_hutang_piutang hutang_piutang_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beone_hutang_piutang ALTER COLUMN hutang_piutang_id SET DEFAULT nextval('public.beone_hutang_piutang_hutang_piutang_id_seq'::regclass);


--
-- Name: beone_import_detail import_detail_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beone_import_detail ALTER COLUMN import_detail_id SET DEFAULT nextval('public.beone_import_detail_import_detail_id_seq'::regclass);


--
-- Name: beone_import_header import_header_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beone_import_header ALTER COLUMN import_header_id SET DEFAULT nextval('public.beone_import_header_import_header_id_seq'::regclass);


--
-- Name: beone_inventory intvent_trans_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beone_inventory ALTER COLUMN intvent_trans_id SET DEFAULT nextval('public.beone_inventory_intvent_trans_id_seq'::regclass);


--
-- Name: beone_item item_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beone_item ALTER COLUMN item_id SET DEFAULT nextval('public.beone_item_item_id_seq'::regclass);


--
-- Name: beone_item_type item_type_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beone_item_type ALTER COLUMN item_type_id SET DEFAULT nextval('public.beone_item_type_item_type_id_seq'::regclass);


--
-- Name: beone_kode_trans kode_trans_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beone_kode_trans ALTER COLUMN kode_trans_id SET DEFAULT nextval('public.beone_kode_trans_kode_trans_id_seq'::regclass);


--
-- Name: beone_komposisi komposisi_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beone_komposisi ALTER COLUMN komposisi_id SET DEFAULT nextval('public.beone_komposisi_komposisi_id_seq'::regclass);


--
-- Name: beone_konversi_stok_detail konversi_stok_detail_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beone_konversi_stok_detail ALTER COLUMN konversi_stok_detail_id SET DEFAULT nextval('public.beone_konversi_stok_detail_konversi_stok_detail_id_seq'::regclass);


--
-- Name: beone_konversi_stok_header konversi_stok_header_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beone_konversi_stok_header ALTER COLUMN konversi_stok_header_id SET DEFAULT nextval('public.beone_konversi_stok_header_konversi_stok_header_id_seq'::regclass);


--
-- Name: beone_log log_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beone_log ALTER COLUMN log_id SET DEFAULT nextval('public.beone_log_log_id_seq'::regclass);


--
-- Name: beone_opname_detail opname_detail_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beone_opname_detail ALTER COLUMN opname_detail_id SET DEFAULT nextval('public.beone_opname_detail_opname_detail_id_seq'::regclass);


--
-- Name: beone_opname_header opname_header_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beone_opname_header ALTER COLUMN opname_header_id SET DEFAULT nextval('public.beone_opname_header_opname_header_id_seq'::regclass);


--
-- Name: beone_peleburan peleburan_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beone_peleburan ALTER COLUMN peleburan_id SET DEFAULT nextval('public.beone_peleburan_peleburan_id_seq'::regclass);


--
-- Name: beone_pemakaian_bahan_detail pemakaian_detail_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beone_pemakaian_bahan_detail ALTER COLUMN pemakaian_detail_id SET DEFAULT nextval('public.beone_pemakaian_bahan_detail_pemakaian_detail_id_seq'::regclass);


--
-- Name: beone_pemakaian_bahan_header pemakaian_header_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beone_pemakaian_bahan_header ALTER COLUMN pemakaian_header_id SET DEFAULT nextval('public.beone_header_pemakaian_bahan_pemakaian_header_id_seq'::regclass);


--
-- Name: beone_pm_detail pm_detail_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beone_pm_detail ALTER COLUMN pm_detail_id SET DEFAULT nextval('public.beone_pm_detail_pm_detail_id_seq'::regclass);


--
-- Name: beone_pm_header pm_header_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beone_pm_header ALTER COLUMN pm_header_id SET DEFAULT nextval('public.beone_pm_header_pm_header_id_seq'::regclass);


--
-- Name: beone_po_import_detail purchase_detail_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beone_po_import_detail ALTER COLUMN purchase_detail_id SET DEFAULT nextval('public.beone_po_import_detail_purchase_detail_id_seq'::regclass);


--
-- Name: beone_po_import_header purchase_header_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beone_po_import_header ALTER COLUMN purchase_header_id SET DEFAULT nextval('public.beone_po_import_header_purchase_header_id_seq'::regclass);


--
-- Name: beone_produksi_detail produksi_detail_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beone_produksi_detail ALTER COLUMN produksi_detail_id SET DEFAULT nextval('public.beone_produksi_detail_produksi_detail_id_seq'::regclass);


--
-- Name: beone_produksi_header produksi_header_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beone_produksi_header ALTER COLUMN produksi_header_id SET DEFAULT nextval('public.beone_produksi_header_produksi_header_id_seq'::regclass);


--
-- Name: beone_purchase_detail purchase_detail_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beone_purchase_detail ALTER COLUMN purchase_detail_id SET DEFAULT nextval('public.beone_purchase_detail_purchase_detail_id_seq'::regclass);


--
-- Name: beone_purchase_header purchase_header_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beone_purchase_header ALTER COLUMN purchase_header_id SET DEFAULT nextval('public.beone_purchase_header_purchase_header_id_seq'::regclass);


--
-- Name: beone_received_import received_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beone_received_import ALTER COLUMN received_id SET DEFAULT nextval('public.beone_received_import_received_id_seq'::regclass);


--
-- Name: beone_role role_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beone_role ALTER COLUMN role_id SET DEFAULT nextval('public.beone_role_role_id_seq'::regclass);


--
-- Name: beone_role_user role_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beone_role_user ALTER COLUMN role_id SET DEFAULT nextval('public.beone_role_user_role_id_seq'::regclass);


--
-- Name: beone_sales_detail sales_detail_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beone_sales_detail ALTER COLUMN sales_detail_id SET DEFAULT nextval('public.beone_sales_detail_sales_detail_id_seq'::regclass);


--
-- Name: beone_sales_header sales_header_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beone_sales_header ALTER COLUMN sales_header_id SET DEFAULT nextval('public.beone_sales_header_sales_header_id_seq'::regclass);


--
-- Name: beone_satuan_item satuan_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beone_satuan_item ALTER COLUMN satuan_id SET DEFAULT nextval('public.beone_satuan_item_satuan_id_seq'::regclass);


--
-- Name: beone_subkon_in_detail subkon_in_detail_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beone_subkon_in_detail ALTER COLUMN subkon_in_detail_id SET DEFAULT nextval('public.beone_subkon_in_detail_subkon_in_detail_id_seq'::regclass);


--
-- Name: beone_subkon_in_header subkon_in_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beone_subkon_in_header ALTER COLUMN subkon_in_id SET DEFAULT nextval('public.beone_subkon_in_header_subkon_in_id_seq'::regclass);


--
-- Name: beone_subkon_out_detail subkon_out_detail_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beone_subkon_out_detail ALTER COLUMN subkon_out_detail_id SET DEFAULT nextval('public.beone_subkon_out_detail_subkon_out_detail_id_seq'::regclass);


--
-- Name: beone_subkon_out_header subkon_out_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beone_subkon_out_header ALTER COLUMN subkon_out_id SET DEFAULT nextval('public.beone_subkon_out_header_subkon_out_id_seq'::regclass);


--
-- Name: beone_tipe_coa tipe_coa_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beone_tipe_coa ALTER COLUMN tipe_coa_id SET DEFAULT nextval('public.beone_tipe_coa_tipe_coa_id_seq'::regclass);


--
-- Name: beone_tipe_coa_transaksi tipe_coa_trans_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beone_tipe_coa_transaksi ALTER COLUMN tipe_coa_trans_id SET DEFAULT nextval('public.beone_tipe_coa_transaksi_tipe_coa_trans_id_seq'::regclass);


--
-- Name: beone_transfer_stock transfer_stock_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beone_transfer_stock ALTER COLUMN transfer_stock_id SET DEFAULT nextval('public.beone_transfer_stock_transfer_stock_id_seq'::regclass);


--
-- Name: beone_transfer_stock_detail transfer_stock_detail_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beone_transfer_stock_detail ALTER COLUMN transfer_stock_detail_id SET DEFAULT nextval('public.beone_transfer_stock_detail_transfer_stock_detail_id_seq'::regclass);


--
-- Name: beone_user user_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beone_user ALTER COLUMN user_id SET DEFAULT nextval('public.beone_user_user_id_seq'::regclass);


--
-- Name: beone_voucher_detail voucher_detail_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beone_voucher_detail ALTER COLUMN voucher_detail_id SET DEFAULT nextval('public.beone_voucher_detail_voucher_detail_id_seq'::regclass);


--
-- Name: beone_voucher_header voucher_header_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beone_voucher_header ALTER COLUMN voucher_header_id SET DEFAULT nextval('public.beone_voucher_header_voucher_header_id_seq'::regclass);


--
-- Data for Name: beone_adjustment; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.beone_adjustment (adjustment_id, adjustment_no, adjustment_date, keterangan, item_id, qty_adjustment, posisi_qty, update_by, update_date, flag) FROM stdin;
\.
COPY public.beone_adjustment (adjustment_id, adjustment_no, adjustment_date, keterangan, item_id, qty_adjustment, posisi_qty, update_by, update_date, flag) FROM '$$PATH$$/3312.dat';

--
-- Data for Name: beone_coa; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.beone_coa (coa_id, nama, nomor, tipe_akun, debet_valas, debet_idr, kredit_valas, kredit_idr, dk, tipe_transaksi) FROM stdin;
\.
COPY public.beone_coa (coa_id, nama, nomor, tipe_akun, debet_valas, debet_idr, kredit_valas, kredit_idr, dk, tipe_transaksi) FROM '$$PATH$$/3314.dat';

--
-- Data for Name: beone_coa_jurnal; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.beone_coa_jurnal (coa_jurnal_id, nama_coa_jurnal, keterangan_coa_jurnal, coa_id, coa_no) FROM stdin;
\.
COPY public.beone_coa_jurnal (coa_jurnal_id, nama_coa_jurnal, keterangan_coa_jurnal, coa_id, coa_no) FROM '$$PATH$$/3316.dat';

--
-- Data for Name: beone_country; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.beone_country (country_id, nama, country_code, flag) FROM stdin;
\.
COPY public.beone_country (country_id, nama, country_code, flag) FROM '$$PATH$$/3318.dat';

--
-- Data for Name: beone_custsup; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.beone_custsup (custsup_id, nama, alamat, tipe_custsup, negara, saldo_hutang_idr, saldo_hutang_valas, saldo_piutang_idr, saldo_piutang_valas, flag, pelunasan_hutang_idr, pelunasan_hutang_valas, pelunasan_piutang_idr, pelunasan_piutang_valas, status_lunas_piutang, status_lunas_hutang) FROM stdin;
\.
COPY public.beone_custsup (custsup_id, nama, alamat, tipe_custsup, negara, saldo_hutang_idr, saldo_hutang_valas, saldo_piutang_idr, saldo_piutang_valas, flag, pelunasan_hutang_idr, pelunasan_hutang_valas, pelunasan_piutang_idr, pelunasan_piutang_valas, status_lunas_piutang, status_lunas_hutang) FROM '$$PATH$$/3320.dat';

--
-- Data for Name: beone_export_detail; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.beone_export_detail (export_detail_id, export_header_id, item_id, qty, satuan_qty, price, pack_qty, satuan_pack, origin_country, doc, volume, netto, brutto, flag, sales_header_id, gudang_id) FROM stdin;
\.
COPY public.beone_export_detail (export_detail_id, export_header_id, item_id, qty, satuan_qty, price, pack_qty, satuan_pack, origin_country, doc, volume, netto, brutto, flag, sales_header_id, gudang_id) FROM '$$PATH$$/3322.dat';

--
-- Data for Name: beone_export_header; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.beone_export_header (export_header_id, jenis_bc, car_no, bc_no, bc_date, kontrak_no, kontrak_date, jenis_export, invoice_no, invoice_date, surat_jalan_no, surat_jalan_date, receiver_id, country_id, price_type, amount_value, valas_value, insurance_type, insurance_value, freight, flag, status, delivery_date, update_by, update_date, delivery_no, vessel, port_loading, port_destination, container, no_container, no_seal) FROM stdin;
\.
COPY public.beone_export_header (export_header_id, jenis_bc, car_no, bc_no, bc_date, kontrak_no, kontrak_date, jenis_export, invoice_no, invoice_date, surat_jalan_no, surat_jalan_date, receiver_id, country_id, price_type, amount_value, valas_value, insurance_type, insurance_value, freight, flag, status, delivery_date, update_by, update_date, delivery_no, vessel, port_loading, port_destination, container, no_container, no_seal) FROM '$$PATH$$/3324.dat';

--
-- Data for Name: beone_gl; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.beone_gl (gl_id, gl_date, coa_id, coa_no, coa_id_lawan, coa_no_lawan, keterangan, debet, kredit, pasangan_no, gl_number, update_by, update_date) FROM stdin;
\.
COPY public.beone_gl (gl_id, gl_date, coa_id, coa_no, coa_id_lawan, coa_no_lawan, keterangan, debet, kredit, pasangan_no, gl_number, update_by, update_date) FROM '$$PATH$$/3326.dat';

--
-- Data for Name: beone_gudang; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.beone_gudang (gudang_id, nama, keterangan, flag) FROM stdin;
\.
COPY public.beone_gudang (gudang_id, nama, keterangan, flag) FROM '$$PATH$$/3328.dat';

--
-- Data for Name: beone_gudang_detail; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.beone_gudang_detail (gudang_detail_id, gudang_id, trans_date, item_id, qty_in, qty_out, nomor_transaksi, update_by, update_date, flag, keterangan, kode_tracing) FROM stdin;
\.
COPY public.beone_gudang_detail (gudang_detail_id, gudang_id, trans_date, item_id, qty_in, qty_out, nomor_transaksi, update_by, update_date, flag, keterangan, kode_tracing) FROM '$$PATH$$/3329.dat';

--
-- Data for Name: beone_hutang_piutang; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.beone_hutang_piutang (hutang_piutang_id, custsup_id, trans_date, nomor, keterangan, valas_trans, idr_trans, valas_pelunasan, idr_pelunasan, tipe_trans, update_by, update_date, flag, status_lunas) FROM stdin;
\.
COPY public.beone_hutang_piutang (hutang_piutang_id, custsup_id, trans_date, nomor, keterangan, valas_trans, idr_trans, valas_pelunasan, idr_pelunasan, tipe_trans, update_by, update_date, flag, status_lunas) FROM '$$PATH$$/3334.dat';

--
-- Data for Name: beone_import_detail; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.beone_import_detail (import_detail_id, item_id, qty, satuan_qty, price, pack_qty, satuan_pack, origin_country, volume, netto, brutto, hscode, tbm, ppnn, tpbm, cukai, sat_cukai, cukai_value, bea_masuk, sat_bea_masuk, flag, item_type_id, import_header_id, gudang_id) FROM stdin;
\.
COPY public.beone_import_detail (import_detail_id, item_id, qty, satuan_qty, price, pack_qty, satuan_pack, origin_country, volume, netto, brutto, hscode, tbm, ppnn, tpbm, cukai, sat_cukai, cukai_value, bea_masuk, sat_bea_masuk, flag, item_type_id, import_header_id, gudang_id) FROM '$$PATH$$/3336.dat';

--
-- Data for Name: beone_import_header; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.beone_import_header (import_header_id, jenis_bc, car_no, bc_no, bc_date, invoice_no, invoice_date, kontrak_no, kontrak_date, purpose_id, supplier_id, price_type, "from", amount_value, valas_value, value_added, discount, insurance_type, insurace_value, freight, flag, update_by, update_date, status, receive_date, receive_no) FROM stdin;
\.
COPY public.beone_import_header (import_header_id, jenis_bc, car_no, bc_no, bc_date, invoice_no, invoice_date, kontrak_no, kontrak_date, purpose_id, supplier_id, price_type, "from", amount_value, valas_value, value_added, discount, insurance_type, insurace_value, freight, flag, update_by, update_date, status, receive_date, receive_no) FROM '$$PATH$$/3338.dat';

--
-- Data for Name: beone_inventory; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.beone_inventory (intvent_trans_id, intvent_trans_no, item_id, trans_date, keterangan, qty_in, value_in, qty_out, value_out, sa_qty, sa_unit_price, sa_amount, flag, update_by, update_date, gudang_id) FROM stdin;
\.
COPY public.beone_inventory (intvent_trans_id, intvent_trans_no, item_id, trans_date, keterangan, qty_in, value_in, qty_out, value_out, sa_qty, sa_unit_price, sa_amount, flag, update_by, update_date, gudang_id) FROM '$$PATH$$/3340.dat';

--
-- Data for Name: beone_item; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.beone_item (item_id, nama, item_code, saldo_qty, saldo_idr, keterangan, flag, item_type_id, hscode, satuan_id, item_category_id) FROM stdin;
\.
COPY public.beone_item (item_id, nama, item_code, saldo_qty, saldo_idr, keterangan, flag, item_type_id, hscode, satuan_id, item_category_id) FROM '$$PATH$$/3342.dat';

--
-- Data for Name: beone_item_category; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.beone_item_category (item_category_id, nama, keterangan, flag) FROM stdin;
\.
COPY public.beone_item_category (item_category_id, nama, keterangan, flag) FROM '$$PATH$$/3343.dat';

--
-- Data for Name: beone_item_type; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.beone_item_type (item_type_id, nama, keterangan, flag) FROM stdin;
\.
COPY public.beone_item_type (item_type_id, nama, keterangan, flag) FROM '$$PATH$$/3345.dat';

--
-- Data for Name: beone_kode_trans; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.beone_kode_trans (kode_trans_id, nama, coa_id, kode_bank, in_out, flag) FROM stdin;
\.
COPY public.beone_kode_trans (kode_trans_id, nama, coa_id, kode_bank, in_out, flag) FROM '$$PATH$$/3347.dat';

--
-- Data for Name: beone_komposisi; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.beone_komposisi (komposisi_id, item_jadi_id, item_baku_id, qty_item_baku, flag) FROM stdin;
\.
COPY public.beone_komposisi (komposisi_id, item_jadi_id, item_baku_id, qty_item_baku, flag) FROM '$$PATH$$/3349.dat';

--
-- Data for Name: beone_konfigurasi_perusahaan; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.beone_konfigurasi_perusahaan (nama_perusahaan, alamat_perusahaan, kota_perusahaan, provinsi_perusahaan, kode_pos_perusahaan, logo_perusahaan) FROM stdin;
\.
COPY public.beone_konfigurasi_perusahaan (nama_perusahaan, alamat_perusahaan, kota_perusahaan, provinsi_perusahaan, kode_pos_perusahaan, logo_perusahaan) FROM '$$PATH$$/3351.dat';

--
-- Data for Name: beone_konversi_stok_detail; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.beone_konversi_stok_detail (konversi_stok_detail_id, konversi_stok_header_id, item_id, qty, qty_real, satuan_qty, gudang_id, qty_allocation) FROM stdin;
\.
COPY public.beone_konversi_stok_detail (konversi_stok_detail_id, konversi_stok_header_id, item_id, qty, qty_real, satuan_qty, gudang_id, qty_allocation) FROM '$$PATH$$/3352.dat';

--
-- Data for Name: beone_konversi_stok_header; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.beone_konversi_stok_header (konversi_stok_header_id, item_id, konversi_stok_no, konversi_stok_date, flag, qty, satuan_qty, gudang_id, qty_real) FROM stdin;
\.
COPY public.beone_konversi_stok_header (konversi_stok_header_id, item_id, konversi_stok_no, konversi_stok_date, flag, qty, satuan_qty, gudang_id, qty_real) FROM '$$PATH$$/3354.dat';

--
-- Data for Name: beone_log; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.beone_log (log_id, log_user, log_tipe, log_desc, log_time) FROM stdin;
\.
COPY public.beone_log (log_id, log_user, log_tipe, log_desc, log_time) FROM '$$PATH$$/3356.dat';

--
-- Data for Name: beone_opname_detail; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.beone_opname_detail (opname_detail_id, opname_header_id, item_id, qty_existing, qty_opname, qty_selisih, status_opname, flag) FROM stdin;
\.
COPY public.beone_opname_detail (opname_detail_id, opname_header_id, item_id, qty_existing, qty_opname, qty_selisih, status_opname, flag) FROM '$$PATH$$/3358.dat';

--
-- Data for Name: beone_opname_header; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.beone_opname_header (opname_header_id, document_no, opname_date, total_item_opname, total_item_opname_match, total_item_opname_plus, total_item_opname_min, update_by, flag, keterangan, update_date) FROM stdin;
\.
COPY public.beone_opname_header (opname_header_id, document_no, opname_date, total_item_opname, total_item_opname_match, total_item_opname_plus, total_item_opname_min, update_by, flag, keterangan, update_date) FROM '$$PATH$$/3360.dat';

--
-- Data for Name: beone_peleburan; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.beone_peleburan (peleburan_id, peleburan_no, peleburan_date, keterangan, item_id, qty_peleburan, update_by, update_date, flag) FROM stdin;
\.
COPY public.beone_peleburan (peleburan_id, peleburan_no, peleburan_date, keterangan, item_id, qty_peleburan, update_by, update_date, flag) FROM '$$PATH$$/3362.dat';

--
-- Data for Name: beone_pemakaian_bahan_detail; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.beone_pemakaian_bahan_detail (pemakaian_detail_id, pemakaian_header_id, item_id, qty, unit_price) FROM stdin;
\.
COPY public.beone_pemakaian_bahan_detail (pemakaian_detail_id, pemakaian_header_id, item_id, qty, unit_price) FROM '$$PATH$$/3364.dat';

--
-- Data for Name: beone_pemakaian_bahan_header; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.beone_pemakaian_bahan_header (pemakaian_header_id, nomor_pemakaian, keterangan, tgl_pemakaian, update_by, update_date, nomor_pm, amount_pemakaian, status) FROM stdin;
\.
COPY public.beone_pemakaian_bahan_header (pemakaian_header_id, nomor_pemakaian, keterangan, tgl_pemakaian, update_by, update_date, nomor_pm, amount_pemakaian, status) FROM '$$PATH$$/3332.dat';

--
-- Data for Name: beone_pm_detail; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.beone_pm_detail (pm_detail_id, item_id, qty, satuan_id, pm_header_id) FROM stdin;
\.
COPY public.beone_pm_detail (pm_detail_id, item_id, qty, satuan_id, pm_header_id) FROM '$$PATH$$/3366.dat';

--
-- Data for Name: beone_pm_header; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.beone_pm_header (pm_header_id, no_pm, tgl_pm, customer_id, qty, keterangan_artikel, status) FROM stdin;
\.
COPY public.beone_pm_header (pm_header_id, no_pm, tgl_pm, customer_id, qty, keterangan_artikel, status) FROM '$$PATH$$/3368.dat';

--
-- Data for Name: beone_po_import_detail; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.beone_po_import_detail (purchase_detail_id, purchase_header_id, item_id, qty, price, amount, flag) FROM stdin;
\.
COPY public.beone_po_import_detail (purchase_detail_id, purchase_header_id, item_id, qty, price, amount, flag) FROM '$$PATH$$/3370.dat';

--
-- Data for Name: beone_po_import_header; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.beone_po_import_header (purchase_header_id, purchase_no, trans_date, supplier_id, keterangan, grandtotal, flag, update_by, update_date) FROM stdin;
\.
COPY public.beone_po_import_header (purchase_header_id, purchase_no, trans_date, supplier_id, keterangan, grandtotal, flag, update_by, update_date) FROM '$$PATH$$/3372.dat';

--
-- Data for Name: beone_produksi_detail; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.beone_produksi_detail (produksi_detail_id, produksi_header_id, pemakaian_header_id) FROM stdin;
\.
COPY public.beone_produksi_detail (produksi_detail_id, produksi_header_id, pemakaian_header_id) FROM '$$PATH$$/3374.dat';

--
-- Data for Name: beone_produksi_header; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.beone_produksi_header (produksi_header_id, nomor_produksi, tgl_produksi, tipe_produksi, item_produksi, qty_hasil_produksi, keterangan, flag, updated_by, udpated_date, pm_header_id) FROM stdin;
\.
COPY public.beone_produksi_header (produksi_header_id, nomor_produksi, tgl_produksi, tipe_produksi, item_produksi, qty_hasil_produksi, keterangan, flag, updated_by, udpated_date, pm_header_id) FROM '$$PATH$$/3376.dat';

--
-- Data for Name: beone_purchase_detail; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.beone_purchase_detail (purchase_detail_id, purchase_header_id, item_id, qty, price, amount, flag) FROM stdin;
\.
COPY public.beone_purchase_detail (purchase_detail_id, purchase_header_id, item_id, qty, price, amount, flag) FROM '$$PATH$$/3378.dat';

--
-- Data for Name: beone_purchase_header; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.beone_purchase_header (purchase_header_id, purchase_no, trans_date, supplier_id, keterangan, update_by, update_date, flag, ppn, subtotal, ppn_value, grandtotal, realisasi) FROM stdin;
\.
COPY public.beone_purchase_header (purchase_header_id, purchase_no, trans_date, supplier_id, keterangan, update_by, update_date, flag, ppn, subtotal, ppn_value, grandtotal, realisasi) FROM '$$PATH$$/3380.dat';

--
-- Data for Name: beone_received_import; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.beone_received_import (received_id, nomor_aju, nomor_dokumen_pabean, tanggal_received, status_received, flag, update_by, update_date, nomor_received, tpb_header_id, kurs, po_import_header_id) FROM stdin;
\.
COPY public.beone_received_import (received_id, nomor_aju, nomor_dokumen_pabean, tanggal_received, status_received, flag, update_by, update_date, nomor_received, tpb_header_id, kurs, po_import_header_id) FROM '$$PATH$$/3382.dat';

--
-- Data for Name: beone_role; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.beone_role (role_id, nama_role, keterangan, master_menu, pembelian_menu, penjualan_menu, inventory_menu, produksi_menu, asset_menu, jurnal_umum_menu, kasbank_menu, laporan_inventory, laporan_keuangan, konfigurasi, import_menu, eksport_menu) FROM stdin;
\.
COPY public.beone_role (role_id, nama_role, keterangan, master_menu, pembelian_menu, penjualan_menu, inventory_menu, produksi_menu, asset_menu, jurnal_umum_menu, kasbank_menu, laporan_inventory, laporan_keuangan, konfigurasi, import_menu, eksport_menu) FROM '$$PATH$$/3384.dat';

--
-- Data for Name: beone_role_user; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.beone_role_user (role_id, nama_role, keterangan, master_menu, user_add, user_edit, user_delete, role_add, role_edit, role_delete, customer_add, customer_edit, customer_delete, supplier_add, supplier_edit, supplier_delete, item_add, item_edit, item_delete, jenis_add, jenis_edit, jenis_delete, satuan_add, satuan_edit, satuan_delete, gudang_add, gudang_edit, gudang_delete, master_import, po_add, po_edit, po_delete, tracing, terima_barang, master_pembelian, pembelian_add, pembelian_edit, pembelian_delete, kredit_note_add, kredit_note_edit, kredit_note_delete, master_eksport, eksport_add, eksport_edit, eksport_delete, kirim_barang, menu_penjualan, penjualan_add, penjualan_edit, penjualan_delete, debit_note_add, debit_note_edit, debit_note_delete, menu_inventory, pindah_gudang, stockopname_add, stockopname_edit, stockopname_delete, stockopname_opname, adjustment_add, adjustment_edit, adjustment_delete, pemusnahan_add, pemusnahan_edit, pemusnahan_delete, recal_inventory, menu_produksi, produksi_add, produksi_edit, produksi_delete, menu_asset, menu_jurnal_umum, jurnal_umum_add, jurnal_umum_edit, jurnal_umum_delete, menu_kas_bank, kas_bank_add, kas_bank_edit, kas_bank_delete, menu_laporan_inventory, menu_laporan_keuangan, menu_konfigurasi) FROM stdin;
\.
COPY public.beone_role_user (role_id, nama_role, keterangan, master_menu, user_add, user_edit, user_delete, role_add, role_edit, role_delete, customer_add, customer_edit, customer_delete, supplier_add, supplier_edit, supplier_delete, item_add, item_edit, item_delete, jenis_add, jenis_edit, jenis_delete, satuan_add, satuan_edit, satuan_delete, gudang_add, gudang_edit, gudang_delete, master_import, po_add, po_edit, po_delete, tracing, terima_barang, master_pembelian, pembelian_add, pembelian_edit, pembelian_delete, kredit_note_add, kredit_note_edit, kredit_note_delete, master_eksport, eksport_add, eksport_edit, eksport_delete, kirim_barang, menu_penjualan, penjualan_add, penjualan_edit, penjualan_delete, debit_note_add, debit_note_edit, debit_note_delete, menu_inventory, pindah_gudang, stockopname_add, stockopname_edit, stockopname_delete, stockopname_opname, adjustment_add, adjustment_edit, adjustment_delete, pemusnahan_add, pemusnahan_edit, pemusnahan_delete, recal_inventory, menu_produksi, produksi_add, produksi_edit, produksi_delete, menu_asset, menu_jurnal_umum, jurnal_umum_add, jurnal_umum_edit, jurnal_umum_delete, menu_kas_bank, kas_bank_add, kas_bank_edit, kas_bank_delete, menu_laporan_inventory, menu_laporan_keuangan, menu_konfigurasi) FROM '$$PATH$$/3386.dat';

--
-- Data for Name: beone_sales_detail; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.beone_sales_detail (sales_detail_id, sales_header_id, item_id, qty, price, amount, flag) FROM stdin;
\.
COPY public.beone_sales_detail (sales_detail_id, sales_header_id, item_id, qty, price, amount, flag) FROM '$$PATH$$/3388.dat';

--
-- Data for Name: beone_sales_header; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.beone_sales_header (sales_header_id, sales_no, trans_date, customer_id, keterangan, ppn, subtotal, ppn_value, grandtotal, update_by, update_date, flag, biaya, status, realisasi) FROM stdin;
\.
COPY public.beone_sales_header (sales_header_id, sales_no, trans_date, customer_id, keterangan, ppn, subtotal, ppn_value, grandtotal, update_by, update_date, flag, biaya, status, realisasi) FROM '$$PATH$$/3390.dat';

--
-- Data for Name: beone_satuan_item; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.beone_satuan_item (satuan_id, satuan_code, keterangan, flag) FROM stdin;
\.
COPY public.beone_satuan_item (satuan_id, satuan_code, keterangan, flag) FROM '$$PATH$$/3392.dat';

--
-- Data for Name: beone_subkon_in_detail; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.beone_subkon_in_detail (subkon_in_detail_id, subkon_in_header_id, item_id, qty, flag) FROM stdin;
\.
COPY public.beone_subkon_in_detail (subkon_in_detail_id, subkon_in_header_id, item_id, qty, flag) FROM '$$PATH$$/3394.dat';

--
-- Data for Name: beone_subkon_in_header; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.beone_subkon_in_header (subkon_in_id, no_subkon_in, no_subkon_out, trans_date, keterangan, nomor_aju, biaya_subkon, supplier_id, flag, update_by, update_date) FROM stdin;
\.
COPY public.beone_subkon_in_header (subkon_in_id, no_subkon_in, no_subkon_out, trans_date, keterangan, nomor_aju, biaya_subkon, supplier_id, flag, update_by, update_date) FROM '$$PATH$$/3396.dat';

--
-- Data for Name: beone_subkon_out_detail; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.beone_subkon_out_detail (subkon_out_detail_id, subkon_out_header_id, item_id, qty, unit_price, flag) FROM stdin;
\.
COPY public.beone_subkon_out_detail (subkon_out_detail_id, subkon_out_header_id, item_id, qty, unit_price, flag) FROM '$$PATH$$/3398.dat';

--
-- Data for Name: beone_subkon_out_header; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.beone_subkon_out_header (subkon_out_id, no_subkon_out, trans_date, supplier_id, keterangan, nomor_aju, flag, update_by, update_date) FROM stdin;
\.
COPY public.beone_subkon_out_header (subkon_out_id, no_subkon_out, trans_date, supplier_id, keterangan, nomor_aju, flag, update_by, update_date) FROM '$$PATH$$/3400.dat';

--
-- Data for Name: beone_tipe_coa; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.beone_tipe_coa (tipe_coa_id, nama) FROM stdin;
\.
COPY public.beone_tipe_coa (tipe_coa_id, nama) FROM '$$PATH$$/3402.dat';

--
-- Data for Name: beone_tipe_coa_transaksi; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.beone_tipe_coa_transaksi (tipe_coa_trans_id, nama, flag) FROM stdin;
\.
COPY public.beone_tipe_coa_transaksi (tipe_coa_trans_id, nama, flag) FROM '$$PATH$$/3404.dat';

--
-- Data for Name: beone_transfer_stock; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.beone_transfer_stock (transfer_stock_id, transfer_no, transfer_date, coa_kode_biaya, keterangan, update_by, update_date, flag) FROM stdin;
\.
COPY public.beone_transfer_stock (transfer_stock_id, transfer_no, transfer_date, coa_kode_biaya, keterangan, update_by, update_date, flag) FROM '$$PATH$$/3406.dat';

--
-- Data for Name: beone_transfer_stock_detail; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.beone_transfer_stock_detail (transfer_stock_detail_id, transfer_stock_header_id, tipe_transfer_stock, item_id, qty, gudang_id, biaya, update_by, update_date, flag, persen_produksi) FROM stdin;
\.
COPY public.beone_transfer_stock_detail (transfer_stock_detail_id, transfer_stock_header_id, tipe_transfer_stock, item_id, qty, gudang_id, biaya, update_by, update_date, flag, persen_produksi) FROM '$$PATH$$/3407.dat';

--
-- Data for Name: beone_user; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.beone_user (user_id, username, password, nama, role_id, update_by, update_date, flag) FROM stdin;
\.
COPY public.beone_user (user_id, username, password, nama, role_id, update_by, update_date, flag) FROM '$$PATH$$/3410.dat';

--
-- Data for Name: beone_voucher_detail; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.beone_voucher_detail (voucher_detail_id, voucher_header_id, coa_id_lawan, coa_no_lawan, jumlah_valas, kurs, jumlah_idr, keterangan_detail) FROM stdin;
\.
COPY public.beone_voucher_detail (voucher_detail_id, voucher_header_id, coa_id_lawan, coa_no_lawan, jumlah_valas, kurs, jumlah_idr, keterangan_detail) FROM '$$PATH$$/3412.dat';

--
-- Data for Name: beone_voucher_header; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.beone_voucher_header (voucher_header_id, voucher_number, voucher_date, tipe, keterangan, coa_id_cash_bank, coa_no, update_by, update_date) FROM stdin;
\.
COPY public.beone_voucher_header (voucher_header_id, voucher_number, voucher_date, tipe, keterangan, coa_id_cash_bank, coa_no, update_by, update_date) FROM '$$PATH$$/3414.dat';

--
-- Name: beone_adjustment_adjustment_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.beone_adjustment_adjustment_id_seq', 10, true);


--
-- Name: beone_coa_coa_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.beone_coa_coa_id_seq', 1, false);


--
-- Name: beone_coa_jurnal_coa_jurnal_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.beone_coa_jurnal_coa_jurnal_id_seq', 12, true);


--
-- Name: beone_country_country_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.beone_country_country_id_seq', 3, true);


--
-- Name: beone_custsup_custsup_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.beone_custsup_custsup_id_seq', 34, true);


--
-- Name: beone_export_detail_export_detail_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.beone_export_detail_export_detail_id_seq', 39, true);


--
-- Name: beone_export_header_export_header_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.beone_export_header_export_header_id_seq', 34, true);


--
-- Name: beone_gl_gl_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.beone_gl_gl_id_seq', 2529, true);


--
-- Name: beone_gudang_detail_gudang_detail_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.beone_gudang_detail_gudang_detail_id_seq', 6228, true);


--
-- Name: beone_gudang_gudang_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.beone_gudang_gudang_id_seq', 6, true);


--
-- Name: beone_header_pemakaian_bahan_pemakaian_header_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.beone_header_pemakaian_bahan_pemakaian_header_id_seq', 35, true);


--
-- Name: beone_hutang_piutang_hutang_piutang_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.beone_hutang_piutang_hutang_piutang_id_seq', 437, true);


--
-- Name: beone_import_detail_import_detail_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.beone_import_detail_import_detail_id_seq', 39, true);


--
-- Name: beone_import_header_import_header_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.beone_import_header_import_header_id_seq', 31, true);


--
-- Name: beone_inventory_intvent_trans_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.beone_inventory_intvent_trans_id_seq', 1235, true);


--
-- Name: beone_item_item_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.beone_item_item_id_seq', 2107, true);


--
-- Name: beone_item_type_item_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.beone_item_type_item_type_id_seq', 16, true);


--
-- Name: beone_kode_trans_kode_trans_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.beone_kode_trans_kode_trans_id_seq', 25, true);


--
-- Name: beone_komposisi_komposisi_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.beone_komposisi_komposisi_id_seq', 6, true);


--
-- Name: beone_konversi_stok_detail_konversi_stok_detail_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.beone_konversi_stok_detail_konversi_stok_detail_id_seq', 6, true);


--
-- Name: beone_konversi_stok_header_konversi_stok_header_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.beone_konversi_stok_header_konversi_stok_header_id_seq', 3, true);


--
-- Name: beone_log_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.beone_log_log_id_seq', 413, true);


--
-- Name: beone_opname_detail_opname_detail_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.beone_opname_detail_opname_detail_id_seq', 6037, true);


--
-- Name: beone_opname_header_opname_header_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.beone_opname_header_opname_header_id_seq', 13, true);


--
-- Name: beone_peleburan_peleburan_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.beone_peleburan_peleburan_id_seq', 6, true);


--
-- Name: beone_pemakaian_bahan_detail_pemakaian_detail_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.beone_pemakaian_bahan_detail_pemakaian_detail_id_seq', 52, true);


--
-- Name: beone_pm_detail_pm_detail_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.beone_pm_detail_pm_detail_id_seq', 1, false);


--
-- Name: beone_pm_header_pm_header_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.beone_pm_header_pm_header_id_seq', 13, true);


--
-- Name: beone_po_import_detail_purchase_detail_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.beone_po_import_detail_purchase_detail_id_seq', 113, true);


--
-- Name: beone_po_import_header_purchase_header_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.beone_po_import_header_purchase_header_id_seq', 108, true);


--
-- Name: beone_produksi_detail_produksi_detail_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.beone_produksi_detail_produksi_detail_id_seq', 48, true);


--
-- Name: beone_produksi_header_produksi_header_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.beone_produksi_header_produksi_header_id_seq', 46, true);


--
-- Name: beone_purchase_detail_purchase_detail_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.beone_purchase_detail_purchase_detail_id_seq', 96, true);


--
-- Name: beone_purchase_header_purchase_header_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.beone_purchase_header_purchase_header_id_seq', 65, true);


--
-- Name: beone_received_import_received_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.beone_received_import_received_id_seq', 225, true);


--
-- Name: beone_role_role_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.beone_role_role_id_seq', 6, true);


--
-- Name: beone_role_user_role_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.beone_role_user_role_id_seq', 3, true);


--
-- Name: beone_sales_detail_sales_detail_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.beone_sales_detail_sales_detail_id_seq', 26, true);


--
-- Name: beone_sales_header_sales_header_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.beone_sales_header_sales_header_id_seq', 20, true);


--
-- Name: beone_satuan_item_satuan_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.beone_satuan_item_satuan_id_seq', 3, true);


--
-- Name: beone_subkon_in_detail_subkon_in_detail_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.beone_subkon_in_detail_subkon_in_detail_id_seq', 8, true);


--
-- Name: beone_subkon_in_header_subkon_in_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.beone_subkon_in_header_subkon_in_id_seq', 4, true);


--
-- Name: beone_subkon_out_detail_subkon_out_detail_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.beone_subkon_out_detail_subkon_out_detail_id_seq', 37, true);


--
-- Name: beone_subkon_out_header_subkon_out_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.beone_subkon_out_header_subkon_out_id_seq', 18, true);


--
-- Name: beone_tipe_coa_tipe_coa_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.beone_tipe_coa_tipe_coa_id_seq', 1, false);


--
-- Name: beone_tipe_coa_transaksi_tipe_coa_trans_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.beone_tipe_coa_transaksi_tipe_coa_trans_id_seq', 20, true);


--
-- Name: beone_transfer_stock_detail_transfer_stock_detail_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.beone_transfer_stock_detail_transfer_stock_detail_id_seq', 656, true);


--
-- Name: beone_transfer_stock_transfer_stock_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.beone_transfer_stock_transfer_stock_id_seq', 299, true);


--
-- Name: beone_user_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.beone_user_user_id_seq', 22, true);


--
-- Name: beone_voucher_detail_voucher_detail_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.beone_voucher_detail_voucher_detail_id_seq', 215, true);


--
-- Name: beone_voucher_header_voucher_header_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.beone_voucher_header_voucher_header_id_seq', 142, true);


--
-- Name: beone_adjustment beone_adjustment_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beone_adjustment
    ADD CONSTRAINT beone_adjustment_pkey PRIMARY KEY (adjustment_id);


--
-- Name: beone_coa_jurnal beone_coa_jurnal_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beone_coa_jurnal
    ADD CONSTRAINT beone_coa_jurnal_pkey PRIMARY KEY (coa_jurnal_id);


--
-- Name: beone_coa beone_coa_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beone_coa
    ADD CONSTRAINT beone_coa_pkey PRIMARY KEY (coa_id);


--
-- Name: beone_country beone_country_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beone_country
    ADD CONSTRAINT beone_country_pkey PRIMARY KEY (country_id);


--
-- Name: beone_export_detail beone_export_detail_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beone_export_detail
    ADD CONSTRAINT beone_export_detail_pkey PRIMARY KEY (export_detail_id);


--
-- Name: beone_gl beone_gl_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beone_gl
    ADD CONSTRAINT beone_gl_pkey PRIMARY KEY (gl_id);


--
-- Name: beone_gudang_detail beone_gudang_detail_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beone_gudang_detail
    ADD CONSTRAINT beone_gudang_detail_pkey PRIMARY KEY (gudang_detail_id);


--
-- Name: beone_gudang beone_gudang_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beone_gudang
    ADD CONSTRAINT beone_gudang_pkey PRIMARY KEY (gudang_id);


--
-- Name: beone_pemakaian_bahan_header beone_header_pemakaian_bahan_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beone_pemakaian_bahan_header
    ADD CONSTRAINT beone_header_pemakaian_bahan_pkey PRIMARY KEY (pemakaian_header_id);


--
-- Name: beone_hutang_piutang beone_hutang_piutang_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beone_hutang_piutang
    ADD CONSTRAINT beone_hutang_piutang_pkey PRIMARY KEY (hutang_piutang_id);


--
-- Name: beone_import_detail beone_import_detail_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beone_import_detail
    ADD CONSTRAINT beone_import_detail_pkey PRIMARY KEY (import_detail_id);


--
-- Name: beone_import_header beone_import_header_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beone_import_header
    ADD CONSTRAINT beone_import_header_pkey PRIMARY KEY (import_header_id);


--
-- Name: beone_inventory beone_inventory_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beone_inventory
    ADD CONSTRAINT beone_inventory_pkey PRIMARY KEY (intvent_trans_id);


--
-- Name: beone_item_category beone_item_category_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beone_item_category
    ADD CONSTRAINT beone_item_category_pkey PRIMARY KEY (item_category_id);


--
-- Name: beone_item beone_item_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beone_item
    ADD CONSTRAINT beone_item_pkey PRIMARY KEY (item_id);


--
-- Name: beone_item_type beone_item_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beone_item_type
    ADD CONSTRAINT beone_item_type_pkey PRIMARY KEY (item_type_id);


--
-- Name: beone_kode_trans beone_kode_trans_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beone_kode_trans
    ADD CONSTRAINT beone_kode_trans_pkey PRIMARY KEY (kode_trans_id);


--
-- Name: beone_komposisi beone_komposisi_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beone_komposisi
    ADD CONSTRAINT beone_komposisi_pkey PRIMARY KEY (komposisi_id);


--
-- Name: beone_konversi_stok_detail beone_konversi_stok_detail_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beone_konversi_stok_detail
    ADD CONSTRAINT beone_konversi_stok_detail_pkey PRIMARY KEY (konversi_stok_detail_id);


--
-- Name: beone_konversi_stok_header beone_konversi_stok_header_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beone_konversi_stok_header
    ADD CONSTRAINT beone_konversi_stok_header_pkey PRIMARY KEY (konversi_stok_header_id);


--
-- Name: beone_log beone_log_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beone_log
    ADD CONSTRAINT beone_log_pkey PRIMARY KEY (log_id);


--
-- Name: beone_opname_detail beone_opname_detail_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beone_opname_detail
    ADD CONSTRAINT beone_opname_detail_pkey PRIMARY KEY (opname_detail_id);


--
-- Name: beone_opname_header beone_opname_header_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beone_opname_header
    ADD CONSTRAINT beone_opname_header_pkey PRIMARY KEY (opname_header_id);


--
-- Name: beone_peleburan beone_peleburan_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beone_peleburan
    ADD CONSTRAINT beone_peleburan_pkey PRIMARY KEY (peleburan_id);


--
-- Name: beone_pemakaian_bahan_detail beone_pemakaian_bahan_detail_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beone_pemakaian_bahan_detail
    ADD CONSTRAINT beone_pemakaian_bahan_detail_pkey PRIMARY KEY (pemakaian_detail_id);


--
-- Name: beone_pm_detail beone_pm_detail_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beone_pm_detail
    ADD CONSTRAINT beone_pm_detail_pkey PRIMARY KEY (pm_detail_id);


--
-- Name: beone_pm_header beone_pm_header_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beone_pm_header
    ADD CONSTRAINT beone_pm_header_pkey PRIMARY KEY (pm_header_id);


--
-- Name: beone_po_import_detail beone_po_import_detail_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beone_po_import_detail
    ADD CONSTRAINT beone_po_import_detail_pkey PRIMARY KEY (purchase_detail_id);


--
-- Name: beone_po_import_header beone_po_import_header_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beone_po_import_header
    ADD CONSTRAINT beone_po_import_header_pkey PRIMARY KEY (purchase_header_id);


--
-- Name: beone_produksi_detail beone_produksi_detail_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beone_produksi_detail
    ADD CONSTRAINT beone_produksi_detail_pkey PRIMARY KEY (produksi_detail_id);


--
-- Name: beone_produksi_header beone_produksi_header_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beone_produksi_header
    ADD CONSTRAINT beone_produksi_header_pkey PRIMARY KEY (produksi_header_id);


--
-- Name: beone_purchase_detail beone_purchase_detail_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beone_purchase_detail
    ADD CONSTRAINT beone_purchase_detail_pkey PRIMARY KEY (purchase_detail_id);


--
-- Name: beone_purchase_header beone_purchase_header_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beone_purchase_header
    ADD CONSTRAINT beone_purchase_header_pkey PRIMARY KEY (purchase_header_id);


--
-- Name: beone_received_import beone_received_import_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beone_received_import
    ADD CONSTRAINT beone_received_import_pkey PRIMARY KEY (received_id);


--
-- Name: beone_role beone_role_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beone_role
    ADD CONSTRAINT beone_role_pkey PRIMARY KEY (role_id);


--
-- Name: beone_role_user beone_role_user_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beone_role_user
    ADD CONSTRAINT beone_role_user_pkey PRIMARY KEY (role_id);


--
-- Name: beone_sales_detail beone_sales_detail_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beone_sales_detail
    ADD CONSTRAINT beone_sales_detail_pkey PRIMARY KEY (sales_detail_id);


--
-- Name: beone_sales_header beone_sales_header_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beone_sales_header
    ADD CONSTRAINT beone_sales_header_pkey PRIMARY KEY (sales_header_id);


--
-- Name: beone_satuan_item beone_satuan_item_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beone_satuan_item
    ADD CONSTRAINT beone_satuan_item_pkey PRIMARY KEY (satuan_id);


--
-- Name: beone_subkon_in_detail beone_subkon_in_detail_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beone_subkon_in_detail
    ADD CONSTRAINT beone_subkon_in_detail_pkey PRIMARY KEY (subkon_in_detail_id);


--
-- Name: beone_subkon_in_header beone_subkon_in_header_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beone_subkon_in_header
    ADD CONSTRAINT beone_subkon_in_header_pkey PRIMARY KEY (subkon_in_id);


--
-- Name: beone_subkon_out_detail beone_subkon_out_detail_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beone_subkon_out_detail
    ADD CONSTRAINT beone_subkon_out_detail_pkey PRIMARY KEY (subkon_out_detail_id);


--
-- Name: beone_subkon_out_header beone_subkon_out_header_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beone_subkon_out_header
    ADD CONSTRAINT beone_subkon_out_header_pkey PRIMARY KEY (subkon_out_id);


--
-- Name: beone_tipe_coa beone_tipe_coa_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beone_tipe_coa
    ADD CONSTRAINT beone_tipe_coa_pkey PRIMARY KEY (tipe_coa_id);


--
-- Name: beone_transfer_stock_detail beone_transfer_stock_detail_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beone_transfer_stock_detail
    ADD CONSTRAINT beone_transfer_stock_detail_pkey PRIMARY KEY (transfer_stock_detail_id);


--
-- Name: beone_transfer_stock beone_transfer_stock_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beone_transfer_stock
    ADD CONSTRAINT beone_transfer_stock_pkey PRIMARY KEY (transfer_stock_id);


--
-- Name: beone_user beone_user_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beone_user
    ADD CONSTRAINT beone_user_pkey PRIMARY KEY (user_id);


--
-- Name: beone_voucher_detail beone_voucher_detail_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beone_voucher_detail
    ADD CONSTRAINT beone_voucher_detail_pkey PRIMARY KEY (voucher_detail_id);


--
-- Name: beone_voucher_header beone_voucher_header_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.beone_voucher_header
    ADD CONSTRAINT beone_voucher_header_pkey PRIMARY KEY (voucher_header_id);


--
-- Name: beone_export_detail trg_create_beone_export_detail; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER trg_create_beone_export_detail BEFORE INSERT OR UPDATE ON public.beone_export_detail FOR EACH ROW EXECUTE FUNCTION public.trg_create_beone_export_detail();


--
-- Name: beone_export_header trg_create_beone_export_header; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER trg_create_beone_export_header BEFORE INSERT OR UPDATE ON public.beone_export_header FOR EACH ROW EXECUTE FUNCTION public.trg_create_beone_export_header();


--
-- Name: beone_import_detail trg_create_beone_import_detail; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER trg_create_beone_import_detail BEFORE INSERT OR UPDATE ON public.beone_import_detail FOR EACH ROW EXECUTE FUNCTION public.trg_create_beone_import_detail();


--
-- Name: beone_import_header trg_create_beone_import_header; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER trg_create_beone_import_header BEFORE INSERT OR UPDATE ON public.beone_import_header FOR EACH ROW EXECUTE FUNCTION public.trg_create_beone_import_header();


--
-- Name: beone_konversi_stok_detail trg_create_beone_konversi_stok_detail; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER trg_create_beone_konversi_stok_detail BEFORE INSERT OR UPDATE ON public.beone_konversi_stok_detail FOR EACH ROW EXECUTE FUNCTION public.trg_create_beone_konversi_stok_detail();


--
-- Name: beone_konversi_stok_header trg_create_beone_konversi_stok_header; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER trg_create_beone_konversi_stok_header BEFORE INSERT OR UPDATE ON public.beone_konversi_stok_header FOR EACH ROW EXECUTE FUNCTION public.trg_create_beone_konversi_stok_header();


--
-- Name: beone_transfer_stock_detail trg_create_beone_transfer_stock_detail; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER trg_create_beone_transfer_stock_detail BEFORE INSERT OR UPDATE ON public.beone_transfer_stock_detail FOR EACH ROW EXECUTE FUNCTION public.trg_create_beone_transfer_stock_detail();


--
-- Name: beone_export_detail trg_delete_beone_export_detail; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER trg_delete_beone_export_detail BEFORE DELETE ON public.beone_export_detail FOR EACH ROW EXECUTE FUNCTION public.trg_delete_beone_export_detail();


--
-- Name: beone_import_detail trg_delete_beone_import_detail; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER trg_delete_beone_import_detail BEFORE DELETE ON public.beone_import_detail FOR EACH ROW EXECUTE FUNCTION public.trg_delete_beone_import_detail();


--
-- Name: beone_konversi_stok_detail trg_delete_beone_konversi_stok_detail; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER trg_delete_beone_konversi_stok_detail BEFORE DELETE ON public.beone_konversi_stok_detail FOR EACH ROW EXECUTE FUNCTION public.trg_delete_beone_konversi_stok_detail();


--
-- Name: beone_konversi_stok_header trg_delete_beone_konversi_stok_header; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER trg_delete_beone_konversi_stok_header BEFORE DELETE ON public.beone_konversi_stok_header FOR EACH ROW EXECUTE FUNCTION public.trg_delete_beone_konversi_stok_header();


--
-- Name: beone_transfer_stock_detail trg_delete_beone_transfer_stock_detail; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER trg_delete_beone_transfer_stock_detail BEFORE DELETE ON public.beone_transfer_stock_detail FOR EACH ROW EXECUTE FUNCTION public.trg_delete_beone_transfer_stock_detail();


--
-- PostgreSQL database dump complete
--

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            