<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gj_controller extends CI_Controller {

	function __construct(){
		parent::__construct();
		if($this->session->userdata('status') != "login"){
			redirect(base_url("Login_controller"));
		}
	}

	public function index()
	{
		$this->load->model('Gj_model');
		$this->load->model('COA_model');
		$this->load->view('Header');

		$data['list_coa'] = $this->COA_model->load_COA();

		if(isset($_POST['submit_journal'])){
			//proses simpan dilakukan
			$this->Gj_model->simpan($_POST);
			//redirect("Gj_controller");
		}

		$this->load->view('Gj_form_view',$data);
		$this->load->view('Footer');
	}

	public function get_nomor_journal(){
		$this->load->view('get_nomor_journal');
	}

	public function List_gj()
	{
		$this->load->model('Gj_model');
		$this->load->view('Header');

		$data['list_gj'] = $this->Gj_model->load_gj();

		$this->load->view('Gj_view', $data);
		$this->load->view('Footer');
	}

	public function add()
	{
		$this->load->model('Gj_model');
		$this->load->model('COA_model');
		$this->load->view('Header');

		$data['list_coa'] = $this->COA_model->load_COA();

		if(isset($_POST['submit_journal'])){
			//proses simpan dilakukan
			$this->Gj_model->simpan($_POST);
			redirect("Gj_controller");
		}

		$this->load->view('Gj_form_view',$data);
		$this->load->view('Footer');
	}

	public function edit($gj_id, $no_journal){
		$this->load->model("Gj_model");
		$this->load->model('COA_model');
		$this->load->view('Header');

		$data['list_coa'] = $this->COA_model->load_COA();
		$data['default'] = $this->Gj_model->get_default($gj_id);

		if(isset($_POST['submit_journal'])){
			$this->Gj_model->update($_POST, $no_journal);
			redirect("Gj_controller/List_gj");
		}
		$this->load->view("Gj_form_edit_view",$data);
		$this->load->view('Footer');
	}


	public function delete($gj_id, $gj_number){
		$this->load->model("Gj_model");
		$this->Gj_model->delete($gj_id, $gj_number);
		redirect("Gj_controller/List_gj");
	}


	public function Filter_report_general_journal()
	{
		$this->load->model('COA_model');
		$this->load->view('Header');

		$data['list_coa'] = $this->COA_model->load_COA();

		if(isset($_GET['submit_filter'])){
			//proses filter dilakukan
				redirect("Gj_controller/Report_general_journal?tglawal=".$_GET['tanggal_awal']."&tglakhir=".$_GET['tanggal_akhir']."&coa_id=".$_GET['coa_id']);
		}

		if(isset($_GET['submit_excel'])){
			//proses filter dilakukan
				redirect("Gj_controller/Report_general_journal_excel?tglawal=".$_GET['tanggal_awal']."&tglakhir=".$_GET['tanggal_akhir']."&coa_id=".$_GET['coa_id']);
		}

		$this->load->view('Report_filter_general_journal', $data);
		$this->load->view('Footer');
	}

	public function Report_general_journal()
	{
		$this->load->model('Gj_model');
		$this->load->view('Header');

		$data['list_akun_gl'] = $this->Gj_model->load_akun_general_journal();

		$this->load->view('Report_general_journal', $data);
		$this->load->view('Footer');
	}


	public function Report_general_journal_excel()
	{
		$this->load->model('Gj_model');
		$this->load->view('Header');

		$data['list_akun_gl'] = $this->Gj_model->load_akun_general_journal();

		$this->load->view('Report_general_journal_excel', $data);
		$this->load->view('Footer');
	}

	public function Filter_report_neraca_mutasi()
	{
		$this->load->model('COA_model');
		$this->load->view('Header');

		$data['list_coa'] = $this->COA_model->load_COA();

		if(isset($_GET['submit_filter'])){
			//proses filter dilakukan
				redirect("Gj_controller/Report_neraca_mutasi?tglawal=".$_GET['tanggal_awal']."&tglakhir=".$_GET['tanggal_akhir']);
		}

		$this->load->view('Report_filter_neraca_mutasi', $data);
		$this->load->view('Footer');
	}

	public function Report_neraca_mutasi()
	{
		$this->load->model('COA_model');
		//$this->load->model('Gj_model');
		$this->load->view('Header');

		$data['list_coa'] = $this->COA_model->load_coa();

		$this->load->view('Report_neraca_mutasi', $data);
		$this->load->view('Footer');
	}

	public function Filter_report_neraca()
	{
		$this->load->model('COA_model');
		$this->load->view('Header');

		if(isset($_GET['submit_filter'])){
				redirect("Gj_controller/Report_neraca?tgl=".$_GET['tanggal']);
		}

		$this->load->view('Report_filter_satu_tanggal');
		$this->load->view('Footer');
	}

	public function Report_neraca()
	{
		$this->load->model('COA_model');
		$this->load->view('Header');

		$data['akun_kas_bank'] = $this->COA_model->load_COA_kas_bank();
		$data['akun_persediaan'] = $this->COA_model->load_COA_persediaan();
		$data['akun_aset'] = $this->COA_model->load_COA_aset();
		$data['akun_hutang'] = $this->COA_model->load_COA_hutang();
		$data['akun_hutang_pajak'] = $this->COA_model->load_COA_hutang_pajak();
		$data['akun_modal'] = $this->COA_model->load_COA_modal();
		$data['akun_biaya_setor'] = $this->COA_model->load_COA_biaya_setor();
		$data['akun_hutang_lainnya'] = $this->COA_model->load_COA_hutang_lainnya();
		$data['akun_penyusutan'] = $this->COA_model->load_COA_penyusutan();
		$data['akun_piutang'] = $this->COA_model->load_COA_piutang();
		$data['akun_uang_muka'] = $this->COA_model->load_COA_uang_muka();
		$data['akun_uang_muka_pajak'] = $this->COA_model->load_COA_uang_muka_pajak();
		$data['akun_uang_muka_penjualan'] = $this->COA_model->load_COA_uang_muka_penjualan();

		$data['saldo_awal_coa_penjualan'] = $this->COA_model->load_COA_penjualan();
		$data['list_coa_biaya_admin_umum'] = $this->COA_model->load_COA_biaya_admin_umum();
		$data['list_coa_pendapatan_lain'] = $this->COA_model->load_COA_pendapatan_lain();

		$this->load->view('Report_neraca',$data);
		$this->load->view('Footer');
	}

/************************** REPORT LABA RUGI ****************************************/
	public function Filter_report_laba_rugi()
	{
		$this->load->model('COA_model');
		$this->load->view('Header');

		if(isset($_GET['submit_filter'])){
				redirect("Gj_controller/Report_laba_rugi?tglawal=".$_GET['tanggal_awal']."&tglakhir=".$_GET['tanggal_akhir']);
		}

		$this->load->view('Report_filter_laba_rugi');
		$this->load->view('Footer');
	}

	public function Report_laba_rugi()
	{
		$this->load->model('COA_model');
		$this->load->view('Header');

		$data['list_coa_penjualan'] = $this->COA_model->load_COA_penjualan();
		$data['list_coa_biaya_admin_umum'] = $this->COA_model->load_COA_biaya_admin_umum();
		$data['list_coa_pendapatan_lain'] = $this->COA_model->load_COA_pendapatan_lain();

		$this->load->view('Report_laba_rugi', $data);
		$this->load->view('Footer');
	}
/************************** END REPORT LABA RUGI ****************************************/

/************************** REPORT MODAL ****************************************/
public function Filter_report_modal()
{
	$this->load->model('COA_model');
	$this->load->view('Header');

	$data['judul'] = 'Perubahan Modal';

	if(isset($_GET['submit_filter'])){
			redirect("Gj_controller/Report_modal?tglawal=".$_GET['tanggal_awal']."&tglakhir=".$_GET['tanggal_akhir']);
	}

	$this->load->view('Report_filter_tanggal', $data);
	$this->load->view('Footer');
}

public function Report_modal()
{
	$this->load->model('COA_model');
	$this->load->model('Gj_model');
	$this->load->view('Header');

	$data['saldo_awal_coa_penjualan'] = $this->COA_model->load_COA_penjualan();
	$data['list_coa_biaya_admin_umum'] = $this->COA_model->load_COA_biaya_admin_umum();
	$data['list_coa_pendapatan_lain'] = $this->COA_model->load_COA_pendapatan_lain();

	$this->load->view('Report_modal', $data);
	$this->load->view('Footer');
}
/************************** END REPORT MODAL ****************************************/

public function Filter_report_calk()
{
	$this->load->model('COA_model');
	$this->load->view('Header');

	if(isset($_GET['submit_filter'])){
			redirect("Gj_controller/Report_calk?tgl=".$_GET['tanggal']);
	}

	$this->load->view('Report_filter_satu_tanggal');
	$this->load->view('Footer');
}

public function Report_calk()
{
	$this->load->model('COA_model');
	$this->load->view('Header');

	$data['akun_kas_bank'] = $this->COA_model->load_COA_kas_bank();
	$data['akun_persediaan'] = $this->COA_model->load_COA_persediaan();
	$data['akun_aset'] = $this->COA_model->load_COA_aset();
	$data['akun_hutang'] = $this->COA_model->load_COA_hutang();
	$data['akun_hutang_pajak'] = $this->COA_model->load_COA_hutang_pajak();
	$data['akun_modal'] = $this->COA_model->load_COA_modal();
	$data['akun_biaya_setor'] = $this->COA_model->load_COA_biaya_setor();
	$data['akun_hutang_lainnya'] = $this->COA_model->load_COA_hutang_lainnya();
	$data['akun_penyusutan'] = $this->COA_model->load_COA_penyusutan();
	$data['akun_piutang'] = $this->COA_model->load_COA_piutang();
	$data['akun_uang_muka'] = $this->COA_model->load_COA_uang_muka();
	$data['akun_uang_muka_pajak'] = $this->COA_model->load_COA_uang_muka_pajak();
	$data['akun_uang_muka_penjualan'] = $this->COA_model->load_COA_uang_muka_penjualan();

	$data['saldo_awal_coa_penjualan'] = $this->COA_model->load_COA_penjualan();
	$data['list_coa_biaya_admin_umum'] = $this->COA_model->load_COA_biaya_admin_umum();
	$data['list_coa_pendapatan_lain'] = $this->COA_model->load_COA_pendapatan_lain();

	$this->load->view('Report_calk',$data);
	$this->load->view('Footer');
}


public function Filter_report_hpp()
{
	//$this->load->model('COA_model');
	$this->load->view('Header');

	if(isset($_GET['submit_filter'])){
			redirect("Gj_controller/Report_hpp?tgl=".$_GET['tanggal']);
	}

	$this->load->view('Report_filter_satu_tanggal');
	$this->load->view('Footer');
}


public function Report_hpp()
{
	//$this->load->model('Inventin_model');
	$this->load->view('Header');

	/*$data['bahan_baku'] = $this->Inventin_model->load_bahan_baku();
	$data['pembelian_bahan_baku'] = $this->Inventin_model->load_pembelian_bahan_baku();
	$data['pemakaian_bahan_baku'] = $this->Inventin_model->load_pemakaian_bahan_baku();*/

	$this->load->view('Report_hpp');
	$this->load->view('Footer');
}

}
