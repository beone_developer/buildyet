<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bc25_controller extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('status') != "login") {
			redirect(base_url("Login_controller"));
		} else {
			$this->load->model('bc25_model');
		}
	}

	public function index()
	{
		$data['judul'] = 'Dokumen 2.5';
		$this->load->view('Header', $data);

		$data['ptb'] = $this->bc25_model->select();

		$this->load->view('bc25_tree_view', $data);
		$this->load->view('Footer');
	}

	public function form()
	{
		$data['judul'] = 'Dokumen 2.5';
		$this->load->view('Header', $data);
		$data['jenis_tpb'] = $this->bc25_model->get_jenis_tpb();
		$data['jenis_api'] = $this->bc25_model->get_jenis_api();
		$data['jenis_id'] = $this->bc25_model->get_jenis_id();
		$data['jenis_lokasi_bayar'] = $this->bc25_model->get_jenis_lokasi_bayar();
		$data['jenis_pembayar'] = $this->bc25_model->get_jenis_pembayar();
		$data['jenis_cara_angkut'] = $this->bc25_model->get_jenis_cara_angkut();
		$data['default_pengusaha'] = $this->bc25_model->get_default_pengusaha();

		$data['harga_pungutan'] = $this->bc25_model->get_harga_pungutan('0');
		$data['kemasan'] = $this->bc25_model->get_kemasan('0');
		$data['kontainer'] = $this->bc25_model->get_kontainer('0');
		$data['dokumen_luar'] = $this->bc25_model->get_dokumen('0');
		$data['dokumen_modal_luar'] = $this->bc25_model->get_dokumen_modal('0');
		// $data['ptb'] = $this->bc25_model->select();
		if (isset($_POST['submit_bc25'])) {
			$this->bc25_model->simpan($_POST);
			redirect("Bc25_controller");
		}

		$this->load->view('bc25_form_view', $data);
		$this->load->view('Footer');
	}

	public function edit($ID)
	{
		$data['judul'] = 'Dokumen 2.5';
		$this->load->view('Header', $data);

		$data['default'] = $this->bc25_model->get_default($ID);
		$data['jenis_tpb'] = $this->bc25_model->get_jenis_tpb();
		$data['jenis_api'] = $this->bc25_model->get_jenis_api();
		$data['jenis_id'] = $this->bc25_model->get_jenis_id();
		$data['jenis_lokasi_bayar'] = $this->bc25_model->get_jenis_lokasi_bayar();
		$data['jenis_pembayar'] = $this->bc25_model->get_jenis_pembayar();
		$data['jenis_cara_angkut'] = $this->bc25_model->get_jenis_cara_angkut();
		$data['kemasan'] = $this->bc25_model->get_kemasan($ID);
		$data['kontainer'] = $this->bc25_model->get_kontainer($ID);
		$data['dokumen_luar'] = $this->bc25_model->get_dokumen($ID);
		$data['default_dok_inv'] = $this->bc25_model->get_default_dokinv($ID);
		$data['default_dok_pl'] = $this->bc25_model->get_default_dokpl($ID);
		$data['default_dok_ktr'] = $this->bc25_model->get_default_dokktr($ID);
		$data['harga_pungutan'] = $this->bc25_model->get_harga_pungutan($ID);
		$data['dokumen_modal_luar'] = $this->bc25_model->get_dokumen_modal($ID);
		$data['kemasan_modal_luar'] = $this->bc25_model->get_kemasan_modal($ID);
		$data['modal_daftar_respon'] = $this->bc25_model->get_daftar_respon($ID);
		$data['modal_daftar_status'] = $this->bc25_model->get_daftar_status($ID);
		$data['ptb_kontainer'] = $this->bc25_model->get_kontainer($ID);

		$data['default_pengusaha'] = $this->bc25_model->get_default_pengusaha();
		if (isset($_POST['submit_bc25'])) {
			$this->bc25_model->update($_POST, $ID);
			redirect("Bc25_controller");
		}

		$this->load->view('bc25_form_view', $data);
		$this->load->view('Footer');
	}

	public function update_dokumen($ID_HEADER)
	{
		if (isset($_POST['submit_modaldokumen'])) {
			$this->bc25_model->updatedokumen($_POST);
			redirect("Bc25_controller/edit/$ID_HEADER");
		}
	}

	public function update_kemasan($ID_HEADER)
	{
		if (isset($_POST['submit_modalkemasan'])) {
			$this->bc25_model->updatekemasan($_POST);
			redirect("Bc25_controller/edit/$ID_HEADER");
		}
	}

	public function delete($id)
	{
		$this->bc25_model->delete($id);
		redirect("Bc25_controller");
	}

	public function delete_dokumen($id, $ID_HEADER)
	{

		$this->bc25_model->delete_dokumen($id);
		redirect("Bc25_controller/edit/$ID_HEADER");
	}

	public function delete_kemasan($id, $ID_HEADER)
	{
		$this->bc25_model->delete_kemasan($id);
		redirect("Bc25_controller/edit/$ID_HEADER");
	}

	public function delete_kontainer($id, $ID_HEADER)
	{
//        $ID = $_GET['id'];
		$this->bc25_model->delete_kontainer($id);
		redirect("Bc25_controller/edit/$ID_HEADER");
	}


	public function form_barang($ID)
	{
		$data['judul'] = 'Dokumen 2.5';
		$this->load->view('Header', $data);

		$data['default'] = $this->bc25_model->get_default_barang($ID);

		// if(isset($_POST['submit_bc25'])){
		// 	$this->bc25_model->update($_POST);
		// 	redirect("Bc25_controller");
		// }

		$this->load->view('barang)form_view', $data);
		$this->load->view('Footer');
	}


	// --------------- DOKUMEN MODAL ---------------------
	public function editmodal($ID)
	{

		$data['dokumenedit'] = $this->bc25_model->get_default_modal_dokumen($ID);
		header('Content-Type: application/json');
		echo json_encode($data);
	}

	public function editkemasan($ID)
	{

		$data['kemasanedit'] = $this->bc25_model->get_default_modal_kemasan($ID);
		header('Content-Type: application/json');
		echo json_encode($data);
	}

// --------------------------------------------------------

	public function filter_pabean_pemasukan()
	{
		$this->load->view('Header');
		if (isset($_GET['submit_filter'])) {
			redirect("Bc_controller/Rpt_pabean_pemasukan?tglawal=" . $_GET['tanggal_awal'] . "&tglakhir=" . $_GET['tanggal_akhir']);
		}
		$this->load->view('Report_filter_tanggal');
		$this->load->view('Footer');
	}

	public function Rpt_pabean_pemasukan()
	{
		$this->load->view('Header');
		$this->load->view('Rpt_pabean_pemasukan');
		$this->load->view('Footer');
	}


	public function filter_pabean_pengeluaran()
	{
		$this->load->view('Header');
		if (isset($_GET['submit_filter'])) {
			redirect("Bc_controller/Rpt_pabean_pengeluaran?tglawal=" . $_GET['tanggal_awal'] . "&tglakhir=" . $_GET['tanggal_akhir']);
		}
		$this->load->view('Report_filter_tanggal');
		$this->load->view('Footer');
	}

	public function Rpt_pabean_pengeluaran()
	{
		$this->load->view('Header');
		$this->load->view('Rpt_pabean_pengeluaran');
		$this->load->view('Footer');
	}


}
