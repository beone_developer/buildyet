<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Assets_controller extends CI_Controller {

	function __construct(){
		parent::__construct();
		if($this->session->userdata('status') != "login"){
			redirect(base_url("Login_controller"));
		}
	}

	public function Assets_bahan_baku()
	{
		$this->load->model('Assets_model');
		$this->load->view('Header');
		$data['list_assets_bahan_baku'] = $this->Assets_model->load_assets_bahan_baku();
		$this->load->view('Assets_bahan_baku', $data);
		$this->load->view('Footer');
	}

	public function Assets_wip()
	{
		$this->load->model('Assets_model');
		$this->load->view('Header');
		$data['list_assets_wip'] = $this->Assets_model->load_assets_wip();
		$this->load->view('Assets_wip', $data);
		$this->load->view('Footer');
	}

	public function Assets_barang_jadi()
	{
		$this->load->model('Assets_model');
		$this->load->view('Header');
		$data['list_assets_barang_jadi'] = $this->Assets_model->load_assets_barang_jadi();
		$this->load->view('Assets_barang_jadi', $data);
		$this->load->view('Footer');
	}


	public function Assets_bahan_penolong()
	{
		$this->load->model('Assets_model');
		$this->load->view('Header');
		$data['list_assets_bahan_penolong'] = $this->Assets_model->load_assets_bahan_penolong();
		$this->load->view('Assets_bahan_penolong', $data);
		$this->load->view('Footer');
	}


	public function Assets_mesin_sparepart()
	{
		$this->load->model('Assets_model');
		$this->load->view('Header');
		$data['list_assets_mesin_sparepart'] = $this->Assets_model->load_assets_mesin_sparepart();
		$this->load->view('Assets_mesin_sparepart', $data);
		$this->load->view('Footer');
	}

	public function Assets_peralatan_pabrik()
	{
		$this->load->model('Assets_model');
		$this->load->view('Header');
		$data['list_assets_peralatan_pabrik'] = $this->Assets_model->load_assets_peralatan_pabrik();
		$this->load->view('Assets_peralatan_pabrik', $data);
		$this->load->view('Footer');
	}

}
