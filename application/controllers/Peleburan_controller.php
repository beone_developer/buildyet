<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Peleburan_controller extends CI_Controller {

	function __construct(){
		parent::__construct();
		if($this->session->userdata('status') != "login"){
			redirect(base_url("Login_controller"));
		}
	}

	public function index()
	{
		$this->load->model('Peleburan_model');
		$this->load->view('Header');

		$data['list_item'] = $this->Peleburan_model->load_item();

		if(isset($_POST['submit_peleburan'])){
			$this->Peleburan_model->simpan($_POST);
			redirect("Peleburan_controller");
		}

		$this->load->view('Peleburan_form_view', $data);
		$this->load->view('Footer');
	}

	public function List_peleburan()
	{
		$this->load->model('Peleburan_model');
		$this->load->view('Header');

		$data['List_peleburan'] = $this->Peleburan_model->load_peleburan();

		$this->load->view('Peleburan_view', $data);
		$this->load->view('Footer');
	}

	public function get_nomor_peleburan(){
		$this->load->view('get_nomor_peleburan');
	}

	public function delete($peleburan_id, $peleburan_no){
		$this->load->model("Peleburan_model");
		$this->Peleburan_model->delete($peleburan_id, $peleburan_no);
		redirect("Peleburan_controller/List_peleburan");
	}

}
