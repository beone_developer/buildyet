<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kredit_note_controller extends CI_Controller {

	function __construct(){
		parent::__construct();
		if($this->session->userdata('status') != "login"){
			redirect(base_url("Login_controller"));
		}
	}

	public function index()
	{
		$this->load->model('Custsup_model');
		$this->load->model('COA_model');
		$this->load->model('Kredit_note_model');
		$this->load->view('Header');

		$data['list_supplier'] = $this->Custsup_model->load_supplier();
		$data['list_coa'] = $this->COA_model->load_COA();

		if(isset($_POST['submit_kredit_note'])){
			$this->Kredit_note_model->simpan_kredit_note($_POST);
			redirect("Kredit_note_controller");
		}

		$this->load->view('Kredit_note_form_view',$data);
		$this->load->view('Footer');
	}

	public function get_nomor_kredit_note(){
		$this->load->view('get_nomor_kredit_note');
	}

	public function List_kredit_note()
	{
		$this->load->model('Kredit_note_model');
		$this->load->view('Header');

		$data['List_kredit_note'] = $this->Kredit_note_model->load_kredit_note();

		$this->load->view('Kredit_note_view', $data);
		$this->load->view('Footer');
	}

	public function edit($hp_id, $kdn_no){
		$this->load->model('Custsup_model');
		$this->load->model('COA_model');
		$this->load->model('Kredit_note_model');
		$this->load->view('Header');

		$data['list_supplier'] = $this->Custsup_model->load_supplier();
		$data['list_coa'] = $this->COA_model->load_COA();
		$data['default'] = $this->Kredit_note_model->get_default($hp_id);

		if(isset($_POST['submit_kredit_note'])){
			$this->Kredit_note_model->update($_POST, $kdn_no);
			redirect("Kredit_note_controller/List_kredit_note");
		}
		$this->load->view("Kredit_note_form_edit_view",$data);
		$this->load->view('Footer');
	}

	public function delete($hp_id, $kdn_no){
		$this->load->model("Kredit_note_model");
		$this->Kredit_note_model->delete($hp_id, $kdn_no);
		redirect("Kredit_note_controller/List_kredit_note");
	}


}
