<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class preview_BC40 extends CI_Controller {

    function __construct() {
        parent::__construct();
        if ($this->session->userdata('status') != "login") {
            redirect(base_url("Login_controller"));
        } else {
            $this->load->model('preview_bc40_model');
        }
    }

    public function index() {
        $aju = $_GET['aju'];
        $id = $_GET['id'];
        $kode = $_GET['kode'];
        
        $data['judul'] = 'Preview BC 4.0';
		$this->load->view('Header', $data);

		$data['ptb_edit'] = $this->preview_bc40_model->select($aju,$id);
	    $data['ptb_tujuan'] = $this->preview_bc40_model->get_tujuan($aju,$id);
	    $data['ptb_packing'] = $this->preview_bc40_model->get_packing($aju,$id);
	    $data['ptb_other'] = $this->preview_bc40_model->get_another($aju, $id);
	    $data['ptb_lc'] = $this->preview_bc40_model->get_lc($aju);
	    $data['ptb_kontainer'] = $this->preview_bc40_model->get_kontainer($aju);
	    $data['ptb_kemasan'] = $this->preview_bc40_model->get_kemasan($aju);
        $data['modal_daftar_respon'] = $this->preview_bc40_model->get_daftar_respon($id);
        $data['modal_dokumen'] = $this->preview_bc40_model->get_dokumen_modal($id);
        $data['modal_kemasan'] = $this->preview_bc40_model->get_modal_kemasan($id);

        if ($kode == 2){
        	$this->load->view('bc40_preview', $data);
        }
        if ($kode == 1){
            $this->load->view('bc40_update', $data);
        }

        $this->load->view('Footer');
    }
    
    public function edit() {
        $id = $_POST['id'];
        $nomor_pendaftaran = $_POST['nomor_pendaftaran'];
        $tanggal_pendaftaran= $_POST['tanggal_pendaftaran'];
        $kppbc_pengawas = $_POST['kppbc_pengawas'];
        $kode_gudang_plb = $_POST['kode_gudang_plb'];
        $kode_tujuan_pengiriman = $_POST['kode_tujuan_pengiriman'];
        $kode_id_pengusaha = $_POST['kode_id_pengusaha'];
        $id_pengusaha = $_POST['id_pengusaha'];
        $nama_pengusaha = $_POST['nama_pengusaha'];
        $alamat_pengusaha = $_POST['alamat_pengusaha'];
        $ptb_pengusaha = $_POST['ptb_pengusaha'];
        $jenis_npwp_pengirim = $_POST['jenis_npwp_pengirim'];
        $nomor_npwp_pengirim = $_POST['nomor_npwp_pengirim'];
        $nama_pengirim = $_POST['nama_pengirim'];
        $alamat_pengirim = $_POST['alamat_pengirim'];
        $nomor_dokumen = $_POST['nomor_dokumen'];
        $tanggal_dokumen = $_POST['tanggal_dokumen'];
        $k1 = $_POST['k1'];
        $k2 = $_POST['k2'];
        $fp1 = $_POST['fp1'];
        $fp2 = $_POST['fp2'];
        $fp1 = $_POST['skep1'];
        $fp2 = $_POST['skep2'];
        $volume = $_POST['volume'];
        $sarana_angkut = $_POST['sarana_angkut'];
        $nomor_polisi = $_POST['nomor_polisi'];
        $harga_penyerahan = $_POST['harga_penyerahan'];
        $bruto = $_POST['bruto'];
        $netto = $_POST['netto'];
        $jml_barang = $_POST['jml_barang'];
        $kota_ttd = $_POST['kota_ttd'];
        $tgl_ttd = $_POST['tgl_ttd'];
        $pemberitahu = $_POST['pemberitahu'];
        $jabatan = $_POST['jabatan'];
        
        $data['judul'] = 'Update 4.0';
        $this->load->view('Header', $data);

        $data['ptb'] = $this->preview_bc40_model->update($id,
        $nomor_pendaftaran,
        $tanggal_pendaftaran,
        $kppbc_pengawas,
        $kode_gudang_plb,
        $kode_tujuan_pengiriman,
        $kode_id_pengusaha,
        $id_pengusaha,
        $nama_pengusaha,
        $alamat_pengusaha,
        // $ptb_pengusaha,
        // $jenis_npwp_pengirim,
        // $nomor_npwp_pengirim,
        $nama_pengirim,
        $alamat_pengirim
        // $nomor_dokumen,
        // $tanggal_dokumen
        // $k1,
        // $k2,
        // $fp1,
        // $fp2,
        // $fp1,
        // $fp2,
        // $volume,
        // $sarana_angkut,
        // $nomor_polisi,
        // $harga_penyerahan,
        // $bruto,
        // $netto,
        // $jml_barang,
        // $kota_ttd,
        // $tgl_ttd,
        // $pemberitahu,
        // $jabatan
    );
        redirect("BC_40");


        $this->load->view('bc40_form_view', $data);
        $this->load->view('Footer');
    }

    public function delete($id){
        $this->preview_bc40_model->delete($id);
        redirect("preview_BC40");
    }

    // ========== MODAL ============
    // MODAL DOKUMEN
    public function editmodal($id){

        $data['dokumenedit'] = $this->preview_bc40_model->get_default_modal_dokumen($id);
        header('Content-Type: application/json');
        echo json_encode( $data );
    }

    public function update(){

        $KODE_JENIS_DOKUMEN = $_POST['dokumen1'];
        $NOMOR_DOKUMEN = $_POST['nomor1'];
        $TANGGAL_DOKUMEN = $_POST['tgl1'];

        $data['dokumen'] = $this->preview_bc40_model->update_dokumen($KODE_JENIS_DOKUMEN, 
                                                                     $NOMOR_DOKUMEN, 
                                                                     $TANGGAL_DOKUMEN);
        redirect('BC_40');
        
    }

    public function delete_dokumen($id){

        $data['judul'] = 'Detail Barang';
        $data['tipe'] = "New";
        $this->load->view('Header', $data);

        $data['default'] = $this->preview_bc40_model->delete_dokumen($id);

        redirect("BC_40");
        $this->load->view('Footer');
    
    }

    // MODAL KEMASAN
    public function editmodalkemasan($id){

        $data['kemasanedit'] = $this->preview_bc40_model->get_default_modal_kemasan($id);
        header('Content-Type: application/json');
        echo json_encode( $data );

    }

    public function update_modal_kemasan(){
        $JUMLAH_KEMASAN = $_POST['jumlah'];
        $KODE_JENIS_KEMASAN = $_POST['jenis'];
        $MERK_KEMASAN = $_POST['merk'];

        $data['kemasan'] = $this->preview_bc40_model->update_kemasan($JUMLAH_KEMASAN, 
                                                                     $KODE_JENIS_KEMASAN, 
                                                                     $MERK_KEMASAN);
        redirect('BC_40');
    }

    public function delete_kemasan($id){

        $data['judul'] = 'Detail Barang';
        $data['tipe'] = "New";
        $this->load->view('Header', $data);

        $data['default'] = $this->preview_bc40_model->delete_kemasan($id);

        redirect("BC_40");
        $this->load->view('Footer');
    
    }

}
