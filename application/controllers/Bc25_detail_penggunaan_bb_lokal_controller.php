<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class Bc25_detail_penggunaan_bb_lokal_controller extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		if($this->session->userdata('status') != "login"){
			redirect(base_url("Login_controller"));
		}
		 // else{
	  //       $this->load->model('Ref_tarif_hs_model');
	  //   }
	}

	public function index()
	{
		$data['judul'] = 'Bahan Baku Lokal';
		$data['tipe'] = "New";
		$this->load->view('Header', $data);

		$this->load->view('bc25_detail_penggunaan_bb_lokal_view', $data);
		$this->load->view('Footer'); 
	}
}


 ?>