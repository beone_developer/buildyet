<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inventin_multi_controller extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('status') != "login") {
			redirect(base_url("Login_controller"));
		}
	}

	public function index()
	{
		$this->load->model('Import_model');
		$this->load->view('Header');
		$data['list_import_header'] = $this->Import_model->load_import_header_received();
		$this->load->view('Incoming_item_multi', $data);
		$this->load->view('Footer');
	}

	public function Tracing($no_aju)
	{
		$this->load->model('Inventin_model');
		$this->load->view('Header');
		$data['list_tracing'] = $this->Inventin_model->load_tracing_doc($no_aju);
		$this->load->view('Tracing_doc', $data);
		$this->load->view('Footer');
	}

	public function Received($import_header_id)
	{
		$this->load->model('Inventin_model');
		$this->load->model('Po_import_model');
		$this->load->model('Custsup_model');
		$this->load->view('Header');

		$data['kurs'] = $this->Inventin_model->get_kurs($import_header_id);
		$data['supplier'] = $this->Inventin_model->get_supplier($import_header_id);
		$data['pengirim'] = $this->Inventin_model->get_pengirim($import_header_id);
		$data['item'] = $this->Inventin_model->get_item($import_header_id);
		$data['nomor_aju'] = $this->Inventin_model->get_nomor_aju($import_header_id);
		$data['tanggal_aju'] = $this->Inventin_model->get_tanggal($import_header_id);
		$data['list_nomor_po'] = $this->Po_import_model->load_po_import();
		$data['list_supplier'] = $this->Custsup_model->load_supplier();
		$data['id_import'] = $import_header_id;

		if (isset($_POST['submit_in'])) {
			$result = $this->Inventin_model->received($_POST, $import_header_id);
			if ($result['is_error']) {
				$this->session->set_flashdata('error', $result['message']);
				redirect($_SERVER['HTTP_REFERER']);
				return '';
			}
			redirect("Inventin_controller");
		}
		$this->load->view('Incoming_item_received_multi', $data);
		$this->load->view('Footer');
	}

	public function get_nomor_receiving()
	{
		$this->load->view('get_nomor_receiving');
	}

	public function index_deliverd()
	{
		$this->load->model('Export_model');
		$this->load->view('Header');
		$data['list_export_header'] = $this->Export_model->load_export_header_deliverd();
		$this->load->view('Outgoing_item', $data);
		$this->load->view('Footer');
	}

	public function Deliverd($export_header_id)
	{
		$this->load->model('Inventin_model');
		$this->load->view('Header');

		if (isset($_POST['submit_out'])) {
			$this->Inventin_model->deliverd($_POST, $export_header_id);
			redirect("Inventin_controller/index_deliverd");
		}

		$this->load->view('Outgoing_item_deliverd');
		$this->load->view('Footer');
	}

	public function Report_kartu_stock()
	{
		$this->load->view('Header');
		$this->load->view('Report_Kartu_Stock');
		$this->load->view('Footer');
	}

	public function Filter_report_kartu_stock()
	{
		$this->load->view('Header');

		if (isset($_GET['submit_filter'])) {
			redirect("Inventin_controller/Report_kartu_stock?tglawal=" . $_GET['tanggal_awal'] . "&tglakhir=" . $_GET['tanggal_akhir'] . "&item_id=" . $_GET['coa_id'] . "&tipe_item=" . $_GET['tipe_item']);
		}

		$this->load->view('Report_filter_Kartu_Stock');
		$this->load->view('Footer');
	}

	public function Filter_report_rekap_item()
	{
		$this->load->view('Header');

		$data['judul'] = 'Rekap Item';

		if (isset($_GET['submit_filter'])) {
			redirect("Inventin_controller/Report_rekap_item?tglawal=" . $_GET['tanggal_awal'] . "&tglakhir=" . $_GET['tanggal_akhir']);
		}

		$this->load->view('Report_filter_tanggal', $data);
		$this->load->view('Footer');
	}


	public function Report_rekap_item()
	{
		$this->load->model('Item_model');
		$this->load->view('Header');

		$data['list_item'] = $this->Item_model->load_item();

		$this->load->view('Report_rekap_item', $data);
		$this->load->view('Footer');
	}

}
