<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class BC_40 extends CI_Controller {
    

    function __construct() {
        parent::__construct();
        if ($this->session->userdata('status') != "login") {
            redirect(base_url("Login_controller"));
        } else {
            $this->load->model('BC_40_model');
        }
    }

    public function index() {

        $data['judul'] = 'Dokumen 4.0';
        $this->load->view('Header', $data);

        $data['ptb_261'] = $this->BC_40_model->select();

        $this->load->view('bc40_tree_view', $data);
        $this->load->view('Footer');
    }

    public function bc40_form() {

        $data['judul'] = 'Dokumen 4.0';
        $this->load->view('Header', $data);

//		$data['ptb'] = $this->bc23_model->select();

        $this->load->view('bc40_form_view', $data);
        $this->load->view('Footer');
    }

    public function delete($id) {

        $data['judul'] = 'Delete 4.0';
        $this->load->view('Header', $data);
        $data['ptb'] = $this->BC_40_model->delete($id);
        redirect("BC_40");
//        $this->load->view('bc23_form_view', $data);
        $this->load->view('Footer');
    }

    public function daftar_respon() {
        $data['judul'] = 'Respon 4.0';
        $this->load->view('Header', $data);
//        $data['ptb'] = $this->bc23_model->daftar_respon($id);
        $this->load->view('bc23_respon', $data);
        $this->load->view('Footer');
    }

}
