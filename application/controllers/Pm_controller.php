<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class pm_controller extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('status') != "login") {
			redirect(base_url("Login_controller"));
		}
	}

	public function index()
	{
		$this->load->model('Pm_model');
		$this->load->model('Custsup_model');
		$this->load->model('Item_model');
		// $this->load->model('Pm_detail_model');
		$this->load->view('Header');

		$data['tipe'] = "Tambah";
		$data['list_customer'] = $this->Custsup_model->load_receiver();
		$data['list_pm'] = $this->Pm_model->load_pm();
		$data['list_item'] = $this->Item_model->load_item();
		$data['list_satuan'] = $this->Item_model->load_satuan();
		

		if (isset($_POST['submit_pm'])) {
			$this->Pm_model->simpan($_POST);
			redirect("Pm_controller");
		}

		$this->load->view('Pm_form_view', $data);
		$this->load->view('Footer');
	}


	public function index_pm()
	{
		$this->load->model('Pm_model');
		$this->load->view('Header');

		$data['list_pm'] = $this->Pm_model->load_pm();

		$this->load->view('Pm_list_view', $data);
		$this->load->view('Footer');
	}


	public function delete_pm($pm_id)
	{
		$this->load->model("Pm_model");
		$this->Pm_model->delete($pm_id);
		redirect("Pm_controller/index_pm");
	}

	public function get_nomor_pm()
	{
		$this->load->view('get_nomor_pm');
	}

	public function edit($pm_id)
	{
		$this->load->model('Item_model');
		$this->load->model('Pm_model');
		$this->load->view('Header');

		$data['list_item'] = $this->Item_model->load_item();
		$data['list_satuan'] = $this->Item_model->load_satuan();
		$data['default'] = $this->Pm_model->get_default_header($pm_id);
		$data['default_detail'] = $this->Pm_model->get_default_detail($pm_id);

		if(isset($_POST['submit_pm'])){
			$this->Pm_model->update($_POST, $pm_id);
			redirect("Pm_controller/index_pm");
		}

		$this->load->view('Pm_form_edit_view', $data);
		$this->load->view('Footer');
	}
}
