<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class COA_controller extends CI_Controller {
	private $filename = "format"; // Kita tentukan nama filenya


	function __construct(){
		parent::__construct();
		if($this->session->userdata('status') != "login"){
			redirect(base_url("Login_controller"));
		}
	}

	public function index()
	{
		$this->load->model('COA_model');
		$this->load->view('Header');

		$data['list_coa'] = $this->COA_model->load_coa();

		if(isset($_POST['preview'])){
			//proses simpan dilakukan
			//$this->COA_controller->import();
			redirect("COA_controller/import");
		}

		$this->load->view('COA_view', $data);
		$this->load->view('Footer');
	}

	public function add()
	{
		$this->load->model('COA_model');
		$this->load->view('Header');

		$data['list_coa'] = $this->COA_model->load_tipe_coa();

		if(isset($_POST['submit_coa'])){
			//proses simpan dilakukan
			$this->COA_model->simpan($_POST);
			redirect("COA_controller/add");
		}

		$this->load->view('COA_form_view', $data);
		$this->load->view('Footer');
	}

	public function edit($coa_id){
		$this->load->model('COA_model');
		$this->load->view('Header');

		$data['list_coa'] = $this->COA_model->load_tipe_coa();
		$data['default'] = $this->COA_model->get_default($coa_id);
		//yang penting diletakkan sebelum load view

		if(isset($_POST['submit_coa'])){
			$this->COA_model->update_coa($_POST, $coa_id);
			redirect("COA_controller");
		}

		$this->load->view("COA_form_view",$data);
		$this->load->view('Footer');
	}

	public function delete($coa_id){
		$this->load->model("COA_model");
		$this->COA_model->delete($coa_id);
		redirect("COA_controller/index/".$coa_id);
	}


	public function index_coa_jurnal(){
		$this->load->model('COA_model');
		$this->load->view('Header');

		$data['list_coa_jurnal'] = $this->COA_model->load_coa_jurnal();
		$data['tipe'] = "List";

		$this->load->view('COA_jurnal_view',$data);
		$this->load->view('Footer');
	}

	public function edit_coa_jurnal($coa_jurnal_id){
		$this->load->model('COA_model');
		$this->load->view('Header');

		$data['list_coa_jurnal'] = $this->COA_model->load_coa_jurnal();
		$data['default'] = $this->COA_model->get_default_coa_jurnal($coa_jurnal_id);
		$data['list_coa'] = $this->COA_model->load_COA();
		$data['tipe'] = "Ubah";

		if(isset($_POST['submit_coa_jurnal'])){
			$this->COA_model->update_coa_jurnal($_POST, $coa_jurnal_id);
			redirect("COA_controller/index_coa_jurnal");
		}

		$this->load->view('COA_jurnal_view',$data);
		$this->load->view('Footer');
	}


	/***************************************************************************/
	public function import(){
		$this->load->model('COA_model');
		// Load plugin PHPExcel nya
		include APPPATH.'third_party/PHPExcel/PHPExcel.php';

		$excelreader = new PHPExcel_Reader_Excel2007();
		$loadexcel = $excelreader->load('excel/'.$this->filename.'.xlsx'); // Load file yang telah diupload ke folder excel
		//$loadexcel = $excelreader->load('excel/format.xlsx');

		$sheet = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);

		// Buat sebuah variabel array untuk menampung array data yg akan kita insert ke database
		$data = array();

		$numrow = 1;
		foreach($sheet as $row){
			// Cek $numrow apakah lebih dari 1
			// Artinya karena baris pertama adalah nama-nama kolom
			// Jadi dilewat saja, tidak usah diimport
			if($numrow > 1){
				// Kita push (add) array data ke variabel data
				array_push($data, array(
					'coa_id'=>$row['A'], // Insert data nis dari kolom A di excel
					'nama'=>$row['B'], // Insert data nama dari kolom B di excel
					'nomor'=>$row['C'], // Insert data jenis kelamin dari kolom C di excel
					'tipe_akun'=>$row['D'], // Insert data alamat dari kolom D di excel
					'debet_valas'=>$row['E'],
					'debet_idr'=>$row['F'],
					'kredit_valas'=>$row['G'],
					'kredit_idr'=>$row['H'],
					'dk'=>$row['I'],
					'tipe_transaksi'=>$row['J'],
				));
			}

			$numrow++; // Tambah 1 setiap kali looping
		}

		// Panggil fungsi insert_multiple yg telah kita buat sebelumnya di model
		$this->COA_model->insert_multiple($data);

		redirect("COA_controller"); // Redirect ke halaman awal (ke controller fungsi index)
	}
	/**************************************************************************/

}
