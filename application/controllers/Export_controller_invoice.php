<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Export_controller_invoice extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('status') != "login") {
			redirect(base_url("Login_controller"));
		}
	}

	public function index()
	{
		$this->load->model('Export_model');
		$this->load->view('Header');

		$data['list_export_header'] = $this->Export_model->load_export_header();

		$this->load->view('Export_view', $data);
		$this->load->view('Footer');
	}

	public function index_export_delete()
	{
		$this->load->model('Export_model');
		$this->load->view('Header');

		$data['list_export_header'] = $this->Export_model->load_export_header_delete();

		$this->load->view('Export_delete_view', $data);
		$this->load->view('Footer');
	}


	public function index_list()
	{
		$this->load->model('Export_model');
		$this->load->view('Header');

		$data['list_export_header'] = $this->Export_model->load_export_list_header();
		$data['tipe'] = "Ubah";

		$this->load->view('Export_list_view', $data);
		$this->load->view('Footer');
	}

	public function Add()
	{
		$this->load->model('Custsup_model');
		$this->load->model('Export_model');
		$this->load->view('Header');

		$data['list_receiver'] = $this->Custsup_model->load_receiver();
		$data['list_country'] = $this->Custsup_model->load_country();
		$data['tipe'] = "Tambah";

		if (isset($_POST['submit_export'])) {
			//proses simpan dilakukan
			$this->Export_model->simpan($_POST);
			redirect("Export_controller_invoice");
		}

		$this->load->view('Export_form_view', $data);
		$this->load->view('Footer');
	}


	public function Edit($export_header_id)
	{
		$this->load->model('Custsup_model');
		$this->load->model('Export_model');
		$this->load->view('Header');

		$data['list_receiver'] = $this->Custsup_model->load_receiver();
		$data['list_country'] = $this->Custsup_model->load_country();
		$data['default'] = $this->Export_model->get_default($export_header_id);
		$data['tipe'] = "Ubah";

		if (isset($_POST['submit_export'])) {
			//proses simpan dilakukan
			$this->Export_model->update($_POST, $export_header_id);
			redirect("Export_controller_invoice");
		}

		$this->load->view('Export_form_view', $data);
		$this->load->view('Footer');
	}


	public function Export_detail($export_header_id)
	{
		$this->load->model('Export_model');
		$this->load->view('Header');

		$data['list_export_header_detail'] = $this->Export_model->load_export_header_detail($export_header_id);
		$data['list_export_detail_invoice'] = $this->Export_model->load_export_detail_invoice($export_header_id);

		$this->load->view('Export_detail_view_invoice', $data);
		$this->load->view('Footer');
	}

	public function Export_detail_delete($export_header_id)
	{
		$this->load->model('Export_model');
		$this->load->view('Header');

		$data['list_export_header_detail'] = $this->Export_model->load_export_header_detail($export_header_id);
		$data['list_export_detail'] = $this->Export_model->load_export_detail($export_header_id);

		$this->load->view('Export_detail_delete_view', $data);
		$this->load->view('Footer');
	}


	public function Detail($export_header_id)
	{
		$this->load->model('Export_model');
		$this->load->model('Item_model');
		$this->load->model('Custsup_model');
		$this->load->view('Header');

		$data['export_header'] = $this->Export_model->load_export_header_detail($export_header_id);
		$data['list_item'] = $this->Item_model->load_item();
		$data['list_item_type'] = $this->Item_model->load_item_type();
		$data['list_satuan'] = $this->Item_model->load_satuan();
		$data['list_country'] = $this->Custsup_model->load_country();
		$data['tipe'] = "Tambah";

		if (isset($_POST['submit_export'])) {
			//proses simpan dilakukan
			// echo json_encode($_POST);
			// exit;
			$this->Export_model->simpan_detail($_POST, $export_header_id);
			redirect("Export_controller_invoice/Export_detail/" . intval($export_header_id));
		}

		$this->load->view('Export_form_detail_invoice', $data);
		$this->load->view('Footer');
	}


	public function Edit_detail($export_header_id, $sales_header_id)
	{
		$this->load->model('Export_model');
		$this->load->model('Item_model');
		$this->load->model('Custsup_model');
		$this->load->view('Header');

		$data['export_header'] = $this->Export_model->load_export_header_detail($export_header_id);
		$data['export_detail'] = $this->Export_model->load_export_hanya_detail($sales_header_id);
		$data['list_item'] = $this->Item_model->load_item();
		$data['list_item_type'] = $this->Item_model->load_item_type();
		$data['list_satuan'] = $this->Item_model->load_satuan();
		$data['list_country'] = $this->Custsup_model->load_country();
		$data['default'] = $this->Export_model->get_default_detail($sales_header_id);
		$data['tipe'] = "Ubah";

		if (isset($_POST['submit_export'])) {
			//proses simpan dilakukan
			$this->Export_model->update_detail($_POST, $sales_header_id);
			redirect("Export_controller_invoice/Export_detail/" . intval($export_header_id));
		}

		$this->load->view('Export_form_detail_invoice', $data);
		$this->load->view('Footer');
	}


	public function delete_header($export_header_id)
	{
		$this->load->model("Export_model");
		$this->Export_model->delete_header($export_header_id);
		redirect("Export_controller_invoice");
	}

	public function delete_header_log($export_header_id)
	{
		$this->load->model("Export_model");
		$this->Export_model->delete_header_log($export_header_id);
		redirect("Export_controller_invoice/index_export_delete");
	}

	public function delete_detail($export_header_id, $sales_header_id)
	{
		$this->load->model("Export_model");
		$this->Export_model->delete_detail_invoice($sales_header_id);
		redirect("Export_controller_invoice/Export_detail/" . $export_header_id);
	}

	public function Export_print($export_header_id)
	{
		$this->load->model('Export_model');
		$this->load->view('Header');

		$data['data_export'] = $this->Export_model->load_export_print($export_header_id);

		$this->load->view('export_invoice_print_view', $data);
		$this->load->view('Footer');
	}

	public function Tracing($no_bc)
	{
		$this->load->model('Export_model');
		$this->load->view('Header');
		$data['list_tracing'] = $this->Export_model->load_tracing_doc($no_bc);
		$this->load->view('Tracing_doc_export', $data);
		$this->load->view('Footer');
	}

}
