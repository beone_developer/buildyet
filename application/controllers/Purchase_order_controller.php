<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Purchase_order_controller extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('status') != "login") {
			redirect(base_url("Login_controller"));
		}
	}

	public function index()
	{
		$this->load->model('Purchase_order_model');
		$this->load->model('Custsup_model');
		$this->load->model('Item_model');
		$data['list_supplier'] = $this->Custsup_model->load_supplier();
		$data['list_satuan'] = $this->Item_model->load_satuan();

		if (isset($_POST['submit_purchase'])) {
			$result = $this->Purchase_order_model->simpan($_POST);
			if ($result['is_error']) {
				$this->session->set_flashdata('error', $result['message']);
			} else {
				$this->session->set_flashdata('success', $result['message']);
			}
			redirect("Purchase_order_controller");
		}

		$this->load->view('Header');
		$this->load->view('Purchase_order_form_view', $data);
		$this->load->view('Footer');
	}

	public function index_purchase_order()
	{
		$this->load->model('Purchase_order_model');
		$this->load->view('Header');

		$data['list_purchase_header'] = $this->Purchase_order_model->load_purchase_header();

		$this->load->view('Purchase_order_list_view', $data);
		$this->load->view('Footer');
	}


	public function edit($purchase_id)
	{
		$this->load->model('Item_model');
		$this->load->model('Purchase_order_model');
		$this->load->model('Custsup_model');
		$data['list_supplier'] = $this->Custsup_model->load_supplier();
		$data['list_satuan'] = $this->Item_model->load_satuan();
		$this->load->view('Header');

		$data['list_item'] = $this->Item_model->load_item();
		$data['default'] = $this->Purchase_order_model->get_default_header($purchase_id);
		$data['default_detail'] = $this->Purchase_order_model->get_default_detail($purchase_id);
		if (isset($_POST['submit_purchase'])) {
			$this->Purchase_order_model->update($_POST, $purchase_id);
			redirect("Purchase_order_controller/index_purchase_order");
		}

		$this->load->view('Purchase_order_form_edit_view', $data);
		$this->load->view('Footer');
	}

	public function realisasi($purchase_id)
	{
		$this->load->model("Purchase_order_model");
		$this->Purchase_order_model->realisasi($purchase_id);
		redirect("Purchase_order_controller/index_purchase_order");
	}

	public function delete($purchase_id, $purchase_no)
	{
		$this->load->model("Purchase_order_model");
		$this->Purchase_order_model->delete($purchase_id, $purchase_no);
		redirect("Purchase_order_controller/index_purchase_order");
	}

	public function purchase_order_print($purchase_id)
	{
		$this->load->library('pdfgenerator');
		$this->load->model('Purchase_order_model');

		$data['default'] = $this->Purchase_order_model->get_default_header($purchase_id);
		$data['default_detail'] = $this->Purchase_order_model->get_default_detail($purchase_id);

		$html = $this->load->view('Rpt_purchase_order_print', $data, true);

		$this->pdfgenerator->generate($html, 'Purchase Order');
	}

	public function search_item()
	{
		$q = $this->input->get('q');
		$this->load->model('Purchase_order_model');
		$result = $this->Purchase_order_model->get_item($q);
		return $this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
}
