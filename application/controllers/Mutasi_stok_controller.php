<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mutasi_stok_controller extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('status') != "login") {
			redirect(base_url("Login_controller"));
		}
	}

	public function index()
	{
		$this->load->model('Item_model');
		$this->load->model('Komposisi_model');
		$this->load->model('Mutasi_stok_model');
		$this->load->model('Gudang_model');

		$data['list_item'] = $this->Item_model->load_item();
		$data['list_gudang'] = $this->Gudang_model->load_gudang();

		if (isset($_POST['submit_mutasi'])) {
			$result = $this->Mutasi_stok_model->simpan($_POST);
			if ($result['is_error']) {
				$this->session->set_flashdata('error', $result['message']);
			}
			redirect("Mutasi_stok_controller");
		}

		$this->load->view('Header');
		$this->load->view('Mutasi_stok_form_view', $data);
		$this->load->view('Footer');
	}

	public function index_mutasi_stok()
	{
		$this->load->model('Mutasi_stok_model');
		$this->load->view('Header');

		$data['list_konversi_stok'] = $this->Mutasi_stok_model->load_mutasi();

		$this->load->view('Mutasi_stok_list_view', $data);
		$this->load->view('Footer');
	}

	public function delete($mutasi_stok_header_id)
	{
		$this->load->model('Mutasi_stok_model');
		$this->Mutasi_stok_model->delete($mutasi_stok_header_id);
		redirect("Mutasi_stok_controller/index_mutasi_stok");
	}

	public function loadKomposisi($item_jadi_id)
	{
		$this->load->model('Komposisi_model');
		$data = $this->Komposisi_model->get_default_komposisi($item_jadi_id);
		header('Content-Type: application/json');
		echo json_encode($data);
	}

	public function edit($mutasi_stok_header_id)
	{
		$this->load->model('Mutasi_stok_model');
		$this->load->model('Item_model');
		$this->load->view('Header');

		$data['list_item'] = $this->Item_model->load_item();
		$data['default'] = $this->Mutasi_stok_model->get_default_header($mutasi_stok_header_id);
		$data['default_detail'] = $this->Mutasi_stok_model->get_default_detail($mutasi_stok_header_id);

		if (isset($_POST['submit_mutasi'])) {
			$this->Mutasi_stok_model->update($_POST, $mutasi_stok_header_id);
			redirect("Mutasi_stok_controller/index_mutasi_stok");
		}

		$this->load->view('Mutasi_stok_form_edit_view', $data);
		$this->load->view('Footer');
	}

	public function search_item()
	{
		$q = $this->input->get('q');
		$g = $this->input->get('gudang');
		$this->load->model('Mutasi_stok_model');
		$result = $this->Mutasi_stok_model->get_item($q, $g);
		return $this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

}
