<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Po_import_controller extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('status') != "login") {
			redirect(base_url("Login_controller"));
		}
	}

	public function index()
	{
		$this->load->model('Po_import_model');
		$this->load->model('Custsup_model');
		$this->load->view('Header');

		$data['list_supplier'] = $this->Custsup_model->load_supplier();
		if (isset($_POST['submit_purchase'])) {
			$this->Po_import_model->simpan($_POST);
			redirect("Po_import_controller");
		}

		$this->load->view('Purchase_import_form_view', $data);
		$this->load->view('Footer');
	}

	public function index_po_import()
	{
		$this->load->model('Po_import_model');
		$this->load->view('Header');

		$data['list_po_import'] = $this->Po_import_model->load_po_import();

		$this->load->view('Po_import_list_view', $data);
		$this->load->view('Footer');
	}

	public function po_import_print($purchase_header_id)
	{
		$this->load->model('Po_import_model');
		$this->load->view('Header');

		$data['data_po_import'] = $this->Po_import_model->load_data_po_import();

		$this->load->view('po_import_print_view', $data);
		$this->load->view('Footer');
	}

	public function delete($purchase_header_id)
	{
		$this->load->model("Po_import_model");
		$this->Po_import_model->delete($purchase_header_id);
		redirect("Po_import_controller/index_po_import");
	}


}
