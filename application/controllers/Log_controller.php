<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Log_controller extends CI_Controller {

	function __construct(){
		parent::__construct();
		if($this->session->userdata('status') != "login"){
			redirect(base_url("Login_controller"));
		}
	}

	public function index()
	{
		$this->load->model('Log_model');
		$this->load->view('Header');

		$data['list_log'] = $this->Log_model->load_log();

		$this->load->view('Log_view',$data);
		$this->load->view('Footer');
	}

}
