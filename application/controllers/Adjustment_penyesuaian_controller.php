<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adjustment_penyesuaian_controller extends CI_Controller {

	function __construct(){
		parent::__construct();
		if($this->session->userdata('status') != "login"){
			redirect(base_url("Login_controller"));
		}
	}

	public function index()
	{
		$this->load->model('Adjustment_model');
		$this->load->model('Gudang_model');
		$this->load->view('Header');

		$data['list_item'] = $this->Adjustment_model->load_item_penyesuaian();
		$data['list_gudang'] = $this->Gudang_model->load_gudang();

		if(isset($_POST['submit_adjustment'])){
			$this->Adjustment_model->simpan($_POST);
			redirect("Adjustment_controller");
		}

		$this->load->view('Adjustment_penyesuaian_form_view', $data);
		$this->load->view('Footer');
	}

	public function get_nomor_penyesuaian(){
		$this->load->view('get_nomor_penyesuaian');
	}

}
