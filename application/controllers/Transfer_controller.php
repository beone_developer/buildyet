<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transfer_controller extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('status') != "login") {
			redirect(base_url("Login_controller"));
		}
	}

	public function index()
	{
		$this->load->model('Transfer_model');
		$this->load->view('Header');

		if (isset($_GET['submit_transfer'])) {
			redirect("Transfer_controller/detail?tgl=" . $_GET['tanggal'] . "&produksi=" . $_GET['kode_biaya'] . "&no=" . $_GET['transfer_no'] . "&ket=" . $_GET['keterangan']);
		}

		$this->load->view('Stock_transfer_awal_view');
		$this->load->view('Footer');
	}


	public function detail()
	{
		$this->load->model('Gudang_model');
		$this->load->model('Transfer_model');

		$data['list_gudang'] = $this->Gudang_model->load_gudang();

		if (isset($_POST['submit_transfer'])) {
			$result = $this->Transfer_model->simpan($_POST);
			if ($result['is_error']) {
				$this->session->set_flashdata('error', $result['message']);
			}
			redirect("Transfer_controller");
		}

		$this->load->view('Header');
		$this->load->view('Stock_transfer_view', $data);
		$this->load->view('Footer');
	}

	public function edit($transfer_id)
	{
		$this->load->model('Gudang_model');
		$this->load->model('Transfer_model');
		$this->load->view('Header');

		$data['list_gudang'] = $this->Gudang_model->load_gudang();
		$data['default'] = $this->Transfer_model->get_default_header($transfer_id);
		$data['default_detail'] = $this->Transfer_model->get_default_detail($transfer_id);
		$data['default_detail_hasil'] = $this->Transfer_model->get_default_detail_hasil($transfer_id);

		if (isset($_POST['submit_transfer'])) {
			$this->Transfer_model->update($_POST, $transfer_id);
			redirect("Transfer_controller/index_transfer");
		}

		$this->load->view('Stock_transfer_edit_view', $data);
		$this->load->view('Footer');
	}

	public function index_transfer()
	{
		$this->load->model('Transfer_model');
		$this->load->view('Header');

		$data['list_stock_transfer'] = $this->Transfer_model->load_header_stock_transfer();
		$data['print_stock_transfer'] = $this->Transfer_model->load_stock_transfer();

		$this->load->view('Stock_list_view', $data);
		$this->load->view('Footer');
	}

	public function transfer_print($transfer_id)
	{
		$this->load->model('Transfer_model');
		$this->load->view('Header');

		$data['print_transfer'] = $this->Transfer_model->load_stock_transfer_print($transfer_id);

		$this->load->view('Stock_transfer_print_view', $data);
		$this->load->view('Footer');
	}


	public function recal_inventory()
	{
		$this->load->model('Recal_model');
		$this->load->view('Header');


		if (isset($_POST['submit_recal_inventory'])) {
			$this->Recal_model->recal_inventory($_POST);
			redirect("Transfer_controller/recal_inventory");
		}

		$this->load->view('recal_inventory_view');
		$this->load->view('Footer');
	}

	public function get_item_asal()
	{
		$this->load->view('get_item_asal');
	}

	public function get_item_hasil()
	{
		$this->load->view('get_item_hasil');
	}

	public function delete($transfer_id, $transfer_no)
	{
		$this->load->model("Transfer_model");
		$this->Transfer_model->delete($transfer_id, $transfer_no);
		redirect("Transfer_controller/index_transfer");
	}

	public function delete_log($transfer_id)
	{
		$this->load->model("Transfer_model");
		$this->Transfer_model->delete_log($transfer_id);
		redirect("Transfer_controller/index_transfer_delete");
	}


	public function index_transfer_delete()
	{
		$this->load->model('Transfer_model');
		$this->load->view('Header');

		$data['list_stock_transfer'] = $this->Transfer_model->load_header_stock_transfer_delete();
		$data['print_stock_transfer'] = $this->Transfer_model->load_stock_transfer();

		$this->load->view('Stock_list_delete_view', $data);
		$this->load->view('Footer');
	}

	public function detail_delete($transfer_id)
	{
		$this->load->model('Gudang_model');
		$this->load->model('Transfer_model');
		$this->load->view('Header');

		$data['list_gudang'] = $this->Gudang_model->load_gudang();
		$data['default'] = $this->Transfer_model->get_default_header($transfer_id);
		$data['default_detail'] = $this->Transfer_model->get_default_detail($transfer_id);
		$data['default_detail_hasil'] = $this->Transfer_model->get_default_detail_hasil($transfer_id);

		$this->load->view('Stock_transfer_edit_delete_view', $data);
		$this->load->view('Footer');
	}


}
