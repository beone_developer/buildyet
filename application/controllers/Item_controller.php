<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Item_controller extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('status') != "login") {
			redirect(base_url("Login_controller"));
		}
	}

	public function index()
	{
		$this->load->model('Item_model');
		$this->load->model('Gudang_model');
		$this->load->view('Header');

		$data['list_item'] = $this->Item_model->load_item();
		$data['list_gudang'] = $this->Gudang_model->load_gudang();
		$data['list_tipe_item'] = $this->Item_model->load_item_type();
		$data['list_category_item'] = $this->Item_model->load_item_category();
		$data['list_satuan'] = $this->Item_model->load_satuan();
		$data['tipe'] = "Tambah";

		if (isset($_POST['submit_item'])) {
			$this->Item_model->simpan($_POST);
			redirect("Item_controller");
		}

		/* import item
		if(isset($_POST['preview'])){
			redirect("Item_controller/import");
		}*/

		$this->load->view('Item_view', $data);
		$this->load->view('Footer');
	}

	public function Edit($item_id)
	{
		$this->load->model('Item_model');
		$this->load->view('Header');

		$data['list_item'] = $this->Item_model->load_item();
		$data['list_tipe_item'] = $this->Item_model->load_item_type();
		$data['list_category_item'] = $this->Item_model->load_item_category();
		$data['list_satuan'] = $this->Item_model->load_satuan();
		$data['default'] = $this->Item_model->get_default($item_id);
		$data['tipe'] = "Ubah";

		if (isset($_POST['submit_item'])) {
			$this->Item_model->update($_POST, $item_id);
			redirect("Item_controller");
		}

		$this->load->view('Item_view', $data);
		$this->load->view('Footer');
	}

	public function delete($item_id)
	{
		$this->load->model("Item_model");
		$this->Item_model->delete($item_id);
		redirect("Item_controller");
	}

	public function index_satuan()
	{
		$this->load->model('Item_model');
		$this->load->view('Header');

		$data['list_satuan'] = $this->Item_model->load_satuan();
		$data['tipe'] = "Tambah";

		if (isset($_POST['submit_satuan'])) {
			$this->Item_model->simpan_satuan($_POST);
			redirect("Item_controller/index_satuan");
		}

		$this->load->view('Item_satuan_view', $data);
		$this->load->view('Footer');
	}

	public function Edit_satuan($satuan_id)
	{
		$this->load->model('Item_model');
		$this->load->view('Header');

		$data['list_satuan'] = $this->Item_model->load_satuan();
		$data['default'] = $this->Item_model->get_default_satuan($satuan_id);
		$data['tipe'] = "Ubah";

		if (isset($_POST['submit_satuan'])) {
			$this->Item_model->update_satuan($_POST, $satuan_id);
			redirect("Item_controller/index_satuan");
		}

		$this->load->view('Item_satuan_view', $data);
		$this->load->view('Footer');
	}

	public function delete_satuan($satuan_id)
	{
		$this->load->model("Item_model");
		$this->Item_model->delete_satuan($satuan_id);
		redirect("Item_controller/index_satuan");
	}

	public function index_item_type()
	{
		$this->load->model('Item_model');
		$this->load->view('Header');

		$data['list_item_type'] = $this->Item_model->load_item_type();
		$data['tipe'] = "Tambah";

		if (isset($_POST['submit_item_type'])) {
			$this->Item_model->simpan_item_type($_POST);
			redirect("Item_controller/index_item_type");
		}

		$this->load->view('Item_type_view', $data);
		$this->load->view('Footer');
	}

	public function Edit_item_type($item_type_id)
	{
		$this->load->model('Item_model');
		$this->load->view('Header');

		$data['list_item_type'] = $this->Item_model->load_item_type();
		$data['default'] = $this->Item_model->get_default_item_type($item_type_id);
		$data['tipe'] = "Ubah";

		if (isset($_POST['submit_item_type'])) {
			$this->Item_model->update_item_type($_POST, $item_type_id);
			redirect("Item_controller/index_item_type");
		}

		$this->load->view('Item_type_view', $data);
		$this->load->view('Footer');
	}

	public function delete_item_type($item_type_id)
	{
		$this->load->model("Item_model");
		$this->Item_model->delete_item_type($item_type_id);
		redirect("Item_controller/index_item_type");
	}

	public function index_item_category()
	{
		$this->load->model('Item_model');
		$this->load->view('Header');

		$data['list_item_category'] = $this->Item_model->load_item_category();
		$data['tipe'] = "Tambah";

		if (isset($_POST['submit_item_category'])) {
			$this->Item_model->simpan_item_category($_POST);
			redirect("Item_controller/index_item_category");
		}

		$this->load->view('Item_category_view', $data);
		$this->load->view('Footer');
	}

	public function Edit_item_category($item_category_id)
	{
		$this->load->model('Item_model');
		$this->load->view('Header');

		$data['list_item_category'] = $this->Item_model->load_item_category();
		$data['default'] = $this->Item_model->get_default_item_category($item_category_id);
		$data['tipe'] = "Ubah";

		if (isset($_POST['submit_item_category'])) {
			$this->Item_model->update_item_category($_POST, $item_category_id);
			redirect("Item_controller/index_item_category");
		}

		$this->load->view('Item_category_view', $data);
		$this->load->view('Footer');
	}

	public function delete_item_category($item_category_id)
	{
		$this->load->model("Item_model");
		$this->Item_model->delete_item_category($item_category_id);
		redirect("Item_controller/index_item_category");
	}

	/***************************************************************************/
	public function import()
	{
		$this->load->model('Item_model');
		// Load plugin PHPExcel nya
		include APPPATH . 'third_party/PHPExcel/PHPExcel.php';

		$excelreader = new PHPExcel_Reader_Excel2007();
		$loadexcel = $excelreader->load('excel/' . $this->filename . 'item.xlsx'); // Load file yang telah diupload ke folder excel
		//$loadexcel = $excelreader->load('excel/format.xlsx');

		$sheet = $loadexcel->getActiveSheet()->toArray(null, true, true, true);

		// Buat sebuah variabel array untuk menampung array data yg akan kita insert ke database
		$data = array();

		$numrow = 1;
		foreach ($sheet as $row) {
			// Cek $numrow apakah lebih dari 1
			// Artinya karena baris pertama adalah nama-nama kolom
			// Jadi dilewat saja, tidak usah diimport
			if ($numrow > 1) {
				// Kita push (add) array data ke variabel data
				array_push($data, array(
					'item_id' => $row['A'], // Insert data nis dari kolom A di excel
					'nama' => $row['B'], // Insert data nama dari kolom B di excel
					'item_code' => $row['C'], // Insert data jenis kelamin dari kolom C di excel
					'saldo_qty' => $row['D'], // Insert data alamat dari kolom D di excel
					'saldo_idr' => $row['E'],
					'keterangan' => $row['F'],
					'flag' => $row['G'],
					'item_type_id' => $row['H'],
					'hscode' => $row['I'],
					'satuan_id' => $row['J'],
					'item_category_id' => $row['K'],
				));
			}

			$numrow++; // Tambah 1 setiap kali looping
		}

		// Panggil fungsi insert_multiple yg telah kita buat sebelumnya di model
		$this->Item_model->insert_multiple($data);

		redirect("Item_controller"); // Redirect ke halaman awal (ke controller fungsi index)
	}

	/**************************************************************************/

	public function search_item()
	{
		$q = $this->input->get('q');
		$this->load->model('Item_model');
		$result = $this->Item_model->get_item($q);
		return $this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
}
