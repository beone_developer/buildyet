<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hp_controller extends CI_Controller {

	function __construct(){
		parent::__construct();
		if($this->session->userdata('status') != "login"){
			redirect(base_url("Login_controller"));
		}
	}

	public function Report_kartu_stock()
	{
		$this->load->view('Header');
		$this->load->view('Report_Kartu_Stock');
		$this->load->view('Footer');
	}

	public function Filter_report_hutang()
	{
		$this->load->view('Header');

		$data['judul'] = 'Kartu Hutang';

		if(isset($_GET['submit_filter'])){
				redirect("Hp_controller/Report_hutang_piutang?tglawal=".$_GET['tanggal_awal']."&tglakhir=".$_GET['tanggal_akhir']."&hp=1");
		}

		$this->load->view('Report_filter_tanggal', $data);
		$this->load->view('Footer');
	}

	public function Filter_report_piutang()
	{
		$this->load->view('Header');

		$data['judul'] = 'Kartu Piutang';

		if(isset($_GET['submit_filter'])){
				redirect("Hp_controller/Report_hutang_piutang?tglawal=".$_GET['tanggal_awal']."&tglakhir=".$_GET['tanggal_akhir']."&hp=0");
		}

		$this->load->view('Report_filter_tanggal', $data);
		$this->load->view('Footer');
	}

	public function Report_hutang_piutang()
	{
		$this->load->view('Header');
		$this->load->view('Report_hutang_piutang');
		$this->load->view('Footer');
	}

	public function Filter_report_rekap_hutang()
	{
		$this->load->view('Header');

		$data['judul'] = 'Rekap Hutang';

		if(isset($_GET['submit_filter'])){
				redirect("Hp_controller/Report_rekap_hutang?tglawal=".$_GET['tanggal_awal']."&tglakhir=".$_GET['tanggal_akhir']);
		}

		$this->load->view('Report_filter_tanggal', $data);
		$this->load->view('Footer');
	}

	public function Report_rekap_hutang()
	{
		$this->load->model('Custsup_model');
		$this->load->view('Header');

		$data['list_supplier'] = $this->Custsup_model->load_supplier();

		$this->load->view('Report_rekap_hutang', $data);
		$this->load->view('Footer');
	}


	public function Filter_report_rekap_piutang()
	{
		$this->load->view('Header');

		$data['judul'] = 'Rekap Piutang';

		if(isset($_GET['submit_filter'])){
				redirect("Hp_controller/Report_rekap_piutang?tglawal=".$_GET['tanggal_awal']."&tglakhir=".$_GET['tanggal_akhir']);
		}

		$this->load->view('Report_filter_tanggal', $data);
		$this->load->view('Footer');
	}

	public function Report_rekap_piutang()
	{
		$this->load->model('Custsup_model');
		$this->load->view('Header');

		$data['list_customer'] = $this->Custsup_model->load_receiver();

		$this->load->view('Report_rekap_piutang', $data);
		$this->load->view('Footer');
	}

}
