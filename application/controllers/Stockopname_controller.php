<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Stockopname_controller extends CI_Controller {

	function __construct(){
		parent::__construct();
		if($this->session->userdata('status') != "login"){
			redirect(base_url("Login_controller"));
		}
	}

	public function index()
	{
		$this->load->model('Stockopname_model');
		$this->load->view('Header');
		$data['list_stockopname'] = $this->Stockopname_model->load_stock_opname();
		$data['tipe'] = "Tambah";

		if(isset($_POST['submit_opname'])){
			$this->Stockopname_model->simpan($_POST);
			redirect("Stockopname_controller");
		}

		$this->load->view('Stockopname_view', $data);
		$this->load->view('Footer');
	}


	public function Stockopname_detail($opname_header_id)
	{
		$this->load->model('Stockopname_model');
		$this->load->view('Header');
		$data['list_detail_stockopname'] = $this->Stockopname_model->load_stock_detail_opname($opname_header_id);
		$data['tipe'] = "List";
		$this->load->view('Stockopname_form_view', $data);
		$this->load->view('Footer');
	}

	public function Edit($opname_header_id, $opname_detail_id)
	{
		$this->load->model('Stockopname_model');
		$this->load->view('Header');

		$data['list_detail_stockopname'] = $this->Stockopname_model->load_stock_detail_opname($opname_header_id);
		$data['default'] = $this->Stockopname_model->get_default($opname_detail_id);
		$data['tipe'] = "Ubah";

		if(isset($_POST['submit_opname'])){
			$this->Stockopname_model->proses_opname($_POST, $opname_detail_id);
			redirect("Stockopname_controller/Stockopname_detail/".$opname_header_id);
		}

		$this->load->view('Stockopname_form_view',$data);
		$this->load->view('Footer');
	}

	public function Edit_header($opname_header_id)
	{
		$this->load->model('Stockopname_model');
		$this->load->view('Header');
		$data['list_stockopname'] = $this->Stockopname_model->load_stock_opname();
		$data['default'] = $this->Stockopname_model->get_default_header($opname_header_id);
		$data['tipe'] = "Ubah";

		if(isset($_POST['submit_opname'])){
			$this->Stockopname_model->update($_POST, $opname_header_id);
			redirect("Stockopname_controller");
		}

		$this->load->view('Stockopname_view', $data);
		$this->load->view('Footer');
	}

	public function delete($opname_header_id){
		$this->load->model("Stockopname_model");
		$this->Stockopname_model->delete($opname_header_id);
		redirect("Stockopname_controller");
	}


}
