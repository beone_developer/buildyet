<?php
 require_once 'dompdf/autoload.inc.php';
 use Dompdf\Dompdf;

class PdfGenerator extends Dompdf
{
  public function __construct()
  {
    parent::__construct();
  } 
  public function generate($html,$filename)
  { 
    $dompdf = new Dompdf();
    $dompdf->load_html($html);
    $dompdf->render();
    $dompdf->stream($filename.'.pdf',array("Attachment"=>0));
  }
}