<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cb
{
	private $app;
	private $db;
	private $q_result = [];

	public function __construct()
	{
		$this->app = &get_instance();
		$this->db = $this->app->db;
	}

	function get_stok($item_id, $gudang_id, $trans_date, $trans_no)
	{
		$query = $this->db->query("select get_stok(" . $this->db->escape($item_id) . ", " . $this->db->escape($gudang_id) . ", " . $this->db->escape($trans_date) . ", " . $this->db->escape($trans_no) . ") as stok_akhir");
		return $query->row()->stok_akhir;
	}

	function begin_trans()
	{
		$this->q_result = [];
		$this->set_reporting(false);
		$this->db->trans_begin();
	}

	function commit_trans()
	{
		$result = $this->q_result;
		$is_error = false;
		$message = "";
		$query = "";
		foreach ($result as $item => $value) {
			if ($value['is_error']) {
				$is_error = true;
				$query = $value['query'];
				$message = $value['message'];
				break;
			}
		}
		$res = [
			'query' => $query,
			'is_error' => $is_error,
			'message' => $message,
			'all' => $result,
		];
		if ($is_error) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
		}
		return $res;
	}

	function example()
	{
		$this->begin_trans();
		$this->query("select 0/0");
		$this->query("select 0/0");
		$this->query("select 0/0");
		$result = $this->commit_trans();
		return $result;
	}

	function query($q)
	{
		$is_error = false;
		$message = "";
		$sql = $this->db->query($q);
		if ($this->db->trans_status() === FALSE) {
			$is_error = true;
			$message = $this->get_error();

			//REDIRECT BACK IF ERROR
			$this->app->session->set_flashdata('error', $message);
			redirect($_SERVER['HTTP_REFERER']);

		} else {
			$is_error = false;
		}
		$q = ['query' => $q, 'is_error' => $is_error, 'message' => $message, 'sql' => $sql];
		$this->q_result[] = $q;
		return $q;
	}

	function get_error()
	{
		$error = $this->db->error();
		$message = $error['message'];
		$message = trim(preg_replace('/\s+/', ' ', $message));
		$needle_start = 'ERROR: ';
		$needle_end = 'CONTEXT: ';
		$needle_abort = "transaction is aborted";
		if (strpos($message, $needle_abort) > 1) {
			return "There's an error when processing your query.";
		}
		$pos_start = strpos($message, $needle_start) + strlen($needle_start);
		$pos_end = strpos($message, $needle_end);
		if ($pos_end > 0) {
			$pos_end = $pos_end - $pos_start - 1;
		} else {
			$pos_end = strlen($message);
		}
		$message = substr($message, $pos_start, $pos_end);
		return $message;
	}

	function set_reporting($is_on)
	{
		if ($is_on) {
			$this->db->db_debug = TRUE;
			error_reporting(-1);
			ini_set('display_errors', 1);
		} else {
			$this->db->db_debug = FALSE;
			ini_set('display_errors', 0);
			if (version_compare(PHP_VERSION, '5.3', '>=')) {
				error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED & ~E_STRICT & ~E_USER_NOTICE & ~E_USER_DEPRECATED);
			} else {
				error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_USER_NOTICE);
			}
		}
	}

	function api_result($query, $param)
	{
		$total_count = 0;
		$items = [];
		if (strlen($param) > 2) {
			$sql = $this->query($query)['sql'];
			$total_count = $sql->num_rows();
			$items = $sql->result();
		} else {
			return ['message' => 'something wrong happend', 'errors' => 'field query mandatory!'];
		}
		$result = [
			'total_count' => $total_count,
			'items' => $items,
		];
		return $result;
	}

	function getLastTransNoLike($inisial, $prefix)
	{
		$q = '';
		if ($inisial == 'BO') {
			$q = "SELECT import_no as kode_nota FROM beone_import_header WHERE import_no ilike '$prefix%' LIMIT 1";
		}
		$urutan = 0;
		$gen_no = $this->query($q)['sql']->row();
		if ($gen_no) {
			$urutan = substr($gen_no->kode_nota, 10, 4);
		}
		return $urutan;
	}

	function genNewTransNo($inisial, $trans_date)
	{
		$tgl = $trans_date;
		$thn = substr($tgl, 8, 2);
		$bln = substr($tgl, 0, 2);
		$prefix = $inisial . "/" . $thn . "/" . $bln . "/";

		$last_urutan = $this->getLastTransNoLike($inisial, $prefix);

		$no_lanjutan = $last_urutan + 1;
		$digit = strlen($no_lanjutan);

		$jml_nol = 4 - $digit;
		$cetak_nol = "";
		for ($i = 1; $i <= $jml_nol; $i++) {
			$cetak_nol = $cetak_nol . "0";
		}
		$nomorpo = $prefix . $cetak_nol . $no_lanjutan;
		return $nomorpo;
	}

//	function commit_trans()
//	{
//		$is_error = false;
//		$message = "";
//		if ($this->db->trans_status() === FALSE) {
//			$is_error = true;
//			$message = $this->get_error();
//			$this->db->trans_rollback();
//		} else {
//			$this->db->trans_commit();
//		}
//		return ['is_error' => $is_error, 'message' => $message];
//	}

//	function example($item_id, $gudang_id, $trans_date, $trans_no, $keterangan, $qty_in, $qty_out)
//	{
//		$this->begin_trans();
//		$this->db->query("call set_stok(" . $this->db->escape($item_id) . ", " . $this->db->escape($gudang_id) . ", " . $this->db->escape($trans_date) . ", " . $this->db->escape($trans_no) . ", " . $this->db->escape($keterangan) . ", " . $this->db->escape($qty_in) . ", " . $this->db->escape($qty_out) . ")");
//		$result = $this->commit_trans();
//		return $result;
//	}

//	function upgradeDB()
//	{
//		$sql = file_get_contents('dbScript/000004.sql');
//		echo $sql;
//		return;
////		$files = glob('dbScript/*.{sql}', GLOB_BRACE);
////		foreach ($files as $file) {
////			$sql = file_get_contents($file);
////			$sqls = explode(';', $sql);
////			array_pop($sqls);
////			foreach ($sqls as $statement) {
////				$stmt = $statement . ";";
////				$this->db->query($stmt);
////			}
////		}
//	}

//	function strpos_all($haystack, $needle, $split_by)
//	{
//		$offset = 0;
//		$allPos = [];
//		$arrPos = [];
//		$loop = 0;
//		while (($pos = strpos($haystack, $needle, $offset)) !== FALSE) {
//			$offset = $pos + 1;
//			$arrPos[] = $pos;
//			if ($split_by > 0) {
//				if ($loop % $split_by) {
//					$allPos[] = $arrPos;
//					$arrPos = [];
//				}
//			} else {
//				$allPos[] = $pos;
//			}
//			$loop++;
//		}
//		return $allPos;
//	}
}
