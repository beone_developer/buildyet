<?php

class bc25_detail_model extends CI_Model {

//    var $tabel = "usr";
 //   private $another;
    function __construct(){
        parent::__construct();
        $this->db = $this ->load -> database('default', TRUE);
        $this->mysql = $this ->load -> database('mysql', TRUE);
    }

    public function get_default_barang($ID){
        $sql = $this->mysql->query("select * from tpb_barang where id_header = ".intval($ID)." order by ID asc limit 1");
        if($sql->num_rows() > 0)
            return $sql->row_array();
        return false;
    }
    public function get_default_barangprev($ID_HEADER, $ID){
        $sql = $this->mysql->query("select * from tpb_barang 
            where (ID < ".intval($ID)." OR ID = (SELECT MIN(ID) FROM tpb_barang where id_header = ".intval($ID_HEADER).")) and id_header = ".intval($ID_HEADER)." 
                order by ID desc limit 1");
        if($sql->num_rows() > 0)
            return $sql->row_array();
        return false;
    }

    public function get_default_barangnext($ID_HEADER, $ID){
        $sql = $this->mysql->query("select * from tpb_barang 
            where (ID > ".intval($ID)." OR ID = (SELECT MAX(ID) FROM tpb_barang where id_header = ".intval($ID_HEADER).")) and id_header = ".intval($ID_HEADER)." 
                order by ID asc limit 1");
        if($sql->num_rows() > 0)
            return $sql->row_array();
        return false;
    }

    public function get_default_barang_tarifcukai($ID){
        $sql = $this->mysql->query("select * from tpb_barang_tarif tbt
                join tpb_barang tb on tb.ID = tbt.ID_BARANG
                where id_barang = (select ID from tpb_barang where id_header = ".intval($ID)." order by ID asc limit 1) and JENIS_TARIF = 'CUKAI'");
        if($sql->num_rows() > 0)
            return $sql->row_array();
        return false;
    }
    
        public function get_default_barang_tarifBM($ID){
        $sql = $this->mysql->query("select * from tpb_barang_tarif tbt
                join tpb_barang tb on tb.ID = tbt.ID_BARANG
                where id_barang = (select ID from tpb_barang where id_header = ".intval($ID)." order by ID asc limit 1) and JENIS_TARIF = 'BM'");
        if($sql->num_rows() > 0)
            return $sql->row_array();
        return false;
    }

    public function get_default_barang_tarifPPN($ID){
        $sql = $this->mysql->query("select * from tpb_barang_tarif tbt
                join tpb_barang tb on tb.ID = tbt.ID_BARANG
                where id_barang = (select ID from tpb_barang where id_header = ".intval($ID)." order by ID asc limit 1) and JENIS_TARIF = 'PPN'");
        if($sql->num_rows() > 0)
            return $sql->row_array();
        return false;
    }

    public function get_default_barang_tarifPPH($ID){
        $sql = $this->mysql->query("select * from tpb_barang_tarif tbt
                join tpb_barang tb on tb.ID = tbt.ID_BARANG
                where id_barang = (select ID from tpb_barang where id_header = ".intval($ID)." order by ID asc limit 1) and JENIS_TARIF = 'PPH'");
        if($sql->num_rows() > 0)
            return $sql->row_array();
        return false;
    }

    public function get_default_barang_tarifPPNBM($ID){
        $sql = $this->mysql->query("select * from tpb_barang_tarif tbt
                join tpb_barang tb on tb.ID = tbt.ID_BARANG
                where id_barang = (select ID from tpb_barang where id_header = ".intval($ID)." order by ID asc limit 1) and JENIS_TARIF = 'PPNBM'");
        if($sql->num_rows() > 0)
            return $sql->row_array();
        return false;
    }

    }

?>
