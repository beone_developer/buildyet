<?php
defined('BASEPATH') OR exit('No direct script access allowed');

Class Mutasi_stok_model extends CI_Model
{
	public function load_mutasi()
	{
		$sql = $this->db->query("SELECT ms.mutasi_stok_header_id,ms.mutasi_stok_no,ms.trans_date,ms.keterangan
								FROM public.beone_mutasi_stok_header ms
								WHERE ms.flag = 1");
		return $sql->result_array();
	}
	// $sql = $this->db->query("SELECT ks.mutasi_stok_header_id,i.nama as item_name,ks.konversi_stok_no,ks.konversi_stok_date,ks.qty,s.satuan_code as satuan 
	// 							FROM public.beone_mutasi_stok_header ks
	// 							INNER JOIN  public.beone_item i on ks.item_id = i.item_id
	// 							INNER JOIN  public.beone_satuan_item s on ks.satuan_qty = s.satuan_id
	// 							WHERE ks.flag = 1");

	public function simpan($post)
	{

		$mutasi_stok_no = $this->get_no_mutasi();
		$tanggal_awal = $this->db->escape($post['mutasi_stok_date']);
		$qty = $this->db->escape($post['qty']);
		$gudang_asal_id = $this->db->escape($post['gudangAsal']);
		$gudang_tujuan_id = $this->db->escape($post['gudangTujuan']);
		$keterangan = $this->db->escape($post['keterangan']);

		$tgl_bulan = substr($tanggal_awal, 1, 2);
		$tgl_hari = substr($tanggal_awal, 4, 2);
		$tgl_tahun = substr($tanggal_awal, 7, 4);

		$tanggal = $tgl_tahun . "-" . $tgl_bulan . "-" . $tgl_hari;
		$tanggal_awal_tahun = $tgl_tahun . "-01-01";

		$this->cb->begin_trans();

		$sql_header = $this->cb->query("INSERT INTO public.beone_mutasi_stok_header(mutasi_stok_header_id,mutasi_stok_no,trans_date,keterangan)
										VALUES (DEFAULT, '$mutasi_stok_no', '$tanggal', $keterangan)");

		$header_id = $this->db->query("SELECT * FROM public.beone_mutasi_stok_header ORDER BY mutasi_stok_header_id DESC LIMIT 1");
		$hasil_header_id = $header_id->row_array();
		$hid = $hasil_header_id['mutasi_stok_header_id'];

		foreach ($_POST['rows'] as $key => $count) {
			$item_id = $_POST['item_id_' . $count];
			$qty_ = $_POST['qty_' . $count];
			$qty_real_ = $_POST['qty_real_' . $count];

			$qty_ex = str_replace(".", "", $qty_);
			$qty_real_ex = str_replace(".", "", $qty_real_);
			$qty_allocation_ex = str_replace(".", "", $qty_allocation_);

			$qty = str_replace(",", ".", $qty_ex);
			$qty_allocation = str_replace(",", ".", $qty_allocation_ex);

			$sql_detail = $this->cb->query("INSERT INTO public.beone_mutasi_stok_detail(mutasi_stok_header_id, item_id, qty, gudang_id_asal,gudang_id_tujuan) 
				VALUES ($hid, $item_id, $qty,$gudang_asal_id,$gudang_tujuan_id)");

		}

		$result = $this->cb->commit_trans();
		return $result;
	}

	public function update($post, $mutasi_stok_header_id)
	{
		// $item_id = $this->db->escape($post['item_id']);
		// $konversi_stok_no = $this->db->escape($post['nomorks']);
		// $tanggal_awal = $this->db->escape($post['konversi_stok_date']);
		// $qty = $this->db->escape($post['qty']);
		// $satuan = $this->db->escape($post['satuan_qty']);

		// $tgl_bulan = substr($tanggal_awal, 1, 2);
		// $tgl_hari = substr($tanggal_awal, 4, 2);
		// $tgl_tahun = substr($tanggal_awal, 7, 4);

		// $tanggal = $tgl_tahun . "-" . $tgl_bulan . "-" . $tgl_hari;
		// $tanggal_awal_tahun = $tgl_tahun . "-01-01";

		// $sql_header = $this->db->query("UPDATE public.beone_mutasi_stok_header set qty=$qty,satuan_qty=$satuan where mutasi_stok_header_id=" . intval($mutasi_stok_header_id));

		$gudang_asal_id = $this->db->escape($post['gudangAsal']);
		$gudang_tujuan_id = $this->db->escape($post['gudangTujuan']);

		// var_dump($_POST);
		// exit;

		$sql_detail_del = $this->db->query("DELETE FROM public.beone_mutasi_stok_detail WHERE mutasi_stok_header_id =" . intval($mutasi_stok_header_id));
		foreach ($_POST['rows'] as $key => $count) {
			$item_id = $_POST['item_id_' . $count];
			$qty_ = $_POST['qty_' . $count];
			// $qty_real_ = $_POST['qty_real_' . $count];

			$qty_ex = str_replace(".", "", $qty_);
			// $qty_real_ex = str_replace(".", "", $qty_real_);
			// $qty_allocation_ex = str_replace(".", "", $qty_allocation_);

			$qty = str_replace(",", ".", $qty_ex);
			// $qty_allocation = str_replace(",", ".", $qty_allocation_ex);

			$sql_detail = $this->cb->query("INSERT INTO public.beone_mutasi_stok_detail(mutasi_stok_header_id, item_id, qty, gudang_id_asal,gudang_id_tujuan) 
				VALUES ($mutasi_stok_header_id, $item_id, $qty,$gudang_asal_id,$gudang_tujuan_id)");
		}

	}

	public function delete($mutasi_stok_header_id)
	{
		$sql = $this->db->query("UPDATE public.beone_mutasi_stok_header set flag = 0 WHERE mutasi_stok_header_id = " . intval($mutasi_stok_header_id));
		// helper_log($tipe = "delete", $str = "Hapus Pembelian, No ".$purchase_no);
	}

	public function get_default_header($mutasi_stok_header_id)
	{
		$sql = $this->db->query("SELECT ms.mutasi_stok_header_id,ms.mutasi_stok_no,ms.trans_date,ms.keterangan,d.gudang_id_asal, g.nama as ngudangasal,di.gudang_id_tujuan,gi.nama as ngudangtujuan
								FROM public.beone_mutasi_stok_header ms
								LEFT JOIN public.beone_mutasi_stok_detail d ON ms.mutasi_stok_header_id = d.mutasi_stok_header_id
								LEFT JOIN public.beone_gudang g ON d.gudang_id_asal = g.gudang_id
								LEFT JOIN public.beone_mutasi_stok_detail di ON ms.mutasi_stok_header_id = di.mutasi_stok_header_id
								LEFT JOIN public.beone_gudang gi ON di.gudang_id_tujuan = gi.gudang_id
								WHERE ms.flag = 1 AND ms.mutasi_stok_header_id=" . intval($mutasi_stok_header_id));
		if ($sql->num_rows() > 0)
			// var_dump($sql->row_array());
			// exit;
			return $sql->row_array();
		return false;
	}

	public function get_default_detail($mutasi_stok_header_id)
	{
		$sql = $this->db->query("SELECT d.mutasi_stok_detail_id,d.item_id,i.nama as item_name,d.qty
								FROM public.beone_mutasi_stok_detail d 
								INNER JOIN public.beone_mutasi_stok_header h ON h.mutasi_stok_header_id = d.mutasi_stok_header_id
								INNER JOIN  public.beone_item i on d.item_id = i.item_id 
								WHERE h.flag = 1 AND d.mutasi_stok_header_id = " . intval($mutasi_stok_header_id));
		return $sql->result_array();
	}

	public function get_no_mutasi()
	{
		/******************* GENERATE PO NUMBER ******************************/
		$tgl = date('m/d/Y');
		$thn = substr($tgl, 8, 2);
		$bln = substr($tgl, 0, 2);

		//LOOKUP KODE
		$lookup_kode = $this->db->query("SELECT * FROM public.beone_mutasi_stok_header ORDER BY mutasi_stok_header_id DESC LIMIT 1");
		$hasil_lookup_kode = $lookup_kode->row_array();

		//PENENTUAN BENTUK KODE
		$kode_awal = "MS/" . $thn . "/" . $bln . "/";


		$urutan = substr($hasil_lookup_kode['mutasi_stok_no'], 10, 4);
		$no_lanjutan = $urutan + 1;

		$digit = strlen($no_lanjutan);
		$jml_nol = 4 - $digit;

		$cetak_nol = "";

		for ($i = 1; $i <= $jml_nol; $i++) {
			$cetak_nol = $cetak_nol . "0";
		}
		$nomorks = $kode_awal . $cetak_nol . $no_lanjutan;
		return $nomorks;
	}

	public function get_item($q, $g)
	{
		$like = "'%" . $q . "%'";
		$this->db->query('drop table if exists temp_res;');
		$query = "select x.item_id, x.gudang_id, sum(x.stok_akhir) as stok
		into temporary temp_res
		from (
				 select it.item_id, 1 as gudang_id, coalesce(sum(it.saldo_qty), 0) stok_akhir
				 from beone_item it
				 group by it.item_id
				 union
				 select i.item_id, i.gudang_id, coalesce(sum(i.qty_in) - sum(i.qty_out), 0) stok_akhir
				 from beone_inventory i
				 group by i.item_id, i.gudang_id
			 ) x
		group by x.item_id, x.gudang_id;
		
		SELECT i.item_id as id, i.item_code as code, concat(i.nama, ' | Stok :', st.stok) as text
		FROM beone_item i,
			 temp_res st
		where i.item_id = st.item_id
		  and st.stok > 0  AND i.nama ilike $like or i.item_code ilike $like";
		return $this->cb->api_result($query, $q);
	}
}

?>
