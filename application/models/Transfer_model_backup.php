<?php
//Model_data.php
defined('BASEPATH') OR exit('No direct script access allowed');

Class Transfer_model extends CI_Model{


	public function simpan($post){
		$tgl = $this->db->escape($post['tanggal']);
		$kode_biaya = $this->db->escape($post['kode_biaya']);
		$keterangan = $this->db->escape($post['keterangan']);
		$transfer_no = $this->db->escape($post['transfer_no']);
		$update_date = date('Y-m-d');

		$tgl_bulan = substr($tgl, 1, 2);
		$tgl_hari = substr($tgl, 4, 2);
		$tgl_tahun = substr($tgl, 7, 4);

		$tanggal = $tgl_tahun."-".$tgl_bulan."-".$tgl_hari;
		$tgl_awal_tahun = $tgl_tahun."-01-"."01";

		$item = $this->input->post("item", true);
		$qty_ = $this->input->post("qty", true);
		$qty = str_replace(".", "", $qty_);
		$gudang = $this->input->post("gudang", true);

		$item_hasil = $this->input->post("item_hasil", true);

		$qty_hasil_ = $this->input->post("qty_hasil", true);
		$qty_hasil_ex = str_replace(".", "", $qty_hasil_);
		$qty_hasil = str_replace(",", ".", $qty_hasil_ex);

		$gudang_hasil = $this->input->post("gudang_hasil", true);

		$biaya_hasil_ = $this->input->post("biaya_hasil", true);
		$biaya_hasil_ex = str_replace(".", "", $biaya_hasil_);
		$biaya_hasil = str_replace(",", ".", $biaya_hasil_ex);

		$persen_hasil_ = $this->input->post("persen_hasil", true);
		$persen_hasil_ex = str_replace(".", "", $persen_hasil_);
		$persen_hasil = str_replace(",", ".", $persen_hasil_ex);


		$sql_header = $this->db->query("INSERT INTO public.beone_transfer_stock(
																	transfer_stock_id, transfer_no, transfer_date, coa_kode_biaya, keterangan, update_by, update_date, flag)
																	VALUES (DEFAULT, $transfer_no, '$tanggal', $kode_biaya, $keterangan, 1, '$update_date', 1);");

		$header_id = $this->db->query("SELECT * FROM public.beone_transfer_stock ORDER BY transfer_stock_id DESC LIMIT 1");
		$hasil_header_id = $header_id->row_array();
		$hid = $hasil_header_id['transfer_stock_id'];

		$amount_unit_price = 0;
		$up = 0;
		/******************************* DARI ITEM ********************************************/
		foreach($qty as $key => $value){
			if($value){
				//INSERT TABEL TRANSFER STOCK
				$sql_detail = $this->db->query("INSERT INTO public.beone_transfer_stock_detail(
																					transfer_stock_detail_id, transfer_stock_header_id, tipe_transfer_stock, item_id, qty, gudang_id, biaya, update_by, update_date, flag)
																					VALUES (DEFAULT, $hid, 'BB', $item[$key], $qty[$key], $gudang[$key], 0, 1, '$update_date', 1);");


				//SALDO AKHIR UNIT PRICE SAMPAI TGL SEKARANG DARI AWAL TAHUN
				//SALDO AWAL ITEM
				$sai = $this->db->query("SELECT * FROM public.beone_item WHERE item_id =".intval($item[$key]));
				$saldo_awal_item = $sai->row_array();

				//SALDO MUTASI ITEM DARI AWAL TAHUN SAMPAI SEKARANG
				$sm = $this->db->query("SELECT SUM(qty_in) as qtyin, SUM(value_in) as idrin, SUM(qty_out) as qtyout, SUM(value_out) as idrout FROM public.beone_inventory WHERE trans_date BETWEEN '$tgl_awal_tahun' AND '$tanggal' AND item_id =".intval($item[$key]));
				$saldo_mutasi_item = $sm->row_array();

				//cek saldo awal
				$inv = $this->db->query("SELECT * FROM public.beone_inventory WHERE flag = 1 AND item_id = $item[$key] ORDER BY intvent_trans_id DESC LIMIT 1");
				$hasil_inv = $inv->row_array();

				if ($hasil_inv['qty_in'] == NULL){
					//INSERT DI KARTO STOCK
							if ($saldo_awal_item['saldo_idr'] == 0 OR $saldo_awal_item['saldo_qty'] == 0){
									$unit_price = 0;
							}else{
									$unit_price = $saldo_awal_item['saldo_idr'] / $saldo_awal_item['saldo_qty'];
							}
					$sa_qty = $saldo_awal_item['saldo_qty'];
					$sa_akhir_qty = $sa_qty-$qty[$key];
					$sa_akhir_amount = ($sa_qty-$qty[$key])*$unit_price;

					$sql_inventory = $this->db->query("INSERT INTO public.beone_inventory(intvent_trans_id, intvent_trans_no, item_id, trans_date, keterangan, qty_in, value_in, qty_out, value_out, sa_qty, sa_unit_price, sa_amount, flag, update_by, update_date) VALUES (DEFAULT, $transfer_no, $item[$key], '$tanggal', $keterangan, 0, 0, $qty[$key], $unit_price, $sa_akhir_qty, $unit_price, $sa_akhir_amount, 1, 1, '$update_date')");
					$up = $up + $unit_price;
					$amount_unit_price = $amount_unit_price + ($qty[$key] * $unit_price);
				}else{
					$sa_qty = $hasil_inv['sa_qty'];
					$sa_akhir_qty = $sa_qty-$qty[$key];
					$sa_akhir_amount = ($sa_qty-$qty[$key])*$hasil_inv['sa_unit_price'];
					$unit_price = $hasil_inv['sa_unit_price'];

					$sql_inventory = $this->db->query("INSERT INTO public.beone_inventory(intvent_trans_id, intvent_trans_no, item_id, trans_date, keterangan, qty_in, value_in, qty_out, value_out, sa_qty, sa_unit_price, sa_amount, flag, update_by, update_date) VALUES (DEFAULT, $transfer_no, $item[$key], '$tanggal', $keterangan, 0, 0, $qty[$key], $unit_price, $sa_akhir_qty, $unit_price, $sa_akhir_amount, 1, 1, '$update_date')");
					$up = $up + $unit_price;
					$amount_unit_price = $amount_unit_price + ($qty[$key] * $unit_price);
				}



				if ($post['kode_biaya'] == 1){// PEMAKAIAN BAHAN BAKU

					//INSERT DI KARTO STOCK
					$sql_ledger_debet = $this->db->query("INSERT INTO public.beone_gl(
																						gl_id, gl_date, coa_id, coa_no, coa_id_lawan, coa_no_lawan, keterangan, debet, kredit, pasangan_no, gl_number, update_by, update_date)
																						VALUES (DEFAULT, '$tanggal', 63, '511-01', 11, '116-01', $keterangan, $amount_unit_price, 0, $transfer_no, $transfer_no, 1, '$update_date')");

					$sql_ledger_kredit = $this->db->query("INSERT INTO public.beone_gl(
																						gl_id, gl_date, coa_id, coa_no, coa_id_lawan, coa_no_lawan, keterangan, debet, kredit, pasangan_no, gl_number, update_by, update_date)
																						VALUES (DEFAULT, '$tanggal', 11, '116-01', 63, '511-01', $keterangan, 0, $amount_unit_price, $transfer_no, $transfer_no, 1, '$update_date')");
				}else{//PEMAKAIAN WIP
					$sql_ledger_debet = $this->db->query("INSERT INTO public.beone_gl(
																						gl_id, gl_date, coa_id, coa_no, coa_id_lawan, coa_no_lawan, keterangan, debet, kredit, pasangan_no, gl_number, update_by, update_date)
																						VALUES (DEFAULT, '$tanggal', 14, '116-08', 13, '116-07', $keterangan, $amount_unit_price, 0, $transfer_no, $transfer_no, 1, '$update_date')");

					$sql_ledger_kredit = $this->db->query("INSERT INTO public.beone_gl(
																						gl_id, gl_date, coa_id, coa_no, coa_id_lawan, coa_no_lawan, keterangan, debet, kredit, pasangan_no, gl_number, update_by, update_date)
																						VALUES (DEFAULT, '$tanggal', 13, '116-07', 14, '116-08', $keterangan, 0, $amount_unit_price, $transfer_no, $transfer_no, 1, '$update_date')");
				}

			}
		}
		/******************************* END DARI ITEM ********************************************/

		/******************************* MENJADI ITEM ********************************************/
		$ctr_hasil = 0;
		foreach($qty_hasil as $key => $value){
			$ctr_hasil = $ctr_hasil + 1;

			if($value){
				$sql_detail2 = $this->db->query("INSERT INTO public.beone_transfer_stock_detail(
																					transfer_stock_detail_id, transfer_stock_header_id, tipe_transfer_stock, item_id, qty, gudang_id, biaya, update_by, update_date, flag)
																					VALUES (DEFAULT, $hid, 'WIP', $item_hasil[$key], $qty_hasil[$key], 1, $biaya_hasil[$key], 1, '$update_date', 1);");

					//SALDO AKHIR UNIT PRICE SAMPAI TGL SEKARANG DARI AWAL TAHUN
					//SALDO AWAL ITEM
					$sai = $this->db->query("SELECT * FROM public.beone_item WHERE item_id =".intval($item_hasil[$key]));
					$saldo_awal_item = $sai->row_array();

					//cek saldo awal
					$inv = $this->db->query("SELECT * FROM public.beone_inventory WHERE flag = 1 AND item_id = $item_hasil[$key] ORDER BY intvent_trans_id DESC LIMIT 1");
					$hasil_inv = $inv->row_array();

					if ($post['kode_biaya'] == 1){// PEMAKAIAN BAHAN BAKU
						//persen amount
						$persen_amount = ($amount_unit_price * $persen_hasil[$key]) / 100;

						//INSERT DI KARTO STOCK
						$amount_hasil = ($persen_amount + $biaya_hasil[$key]) / $qty_hasil[$key];
						$sa_akhir_qty = $qty_hasil[$key] + $hasil_inv['sa_qty'];
						$unit_price = $amount_hasil;
						$sa_akhir_amount = $sa_akhir_qty * $unit_price;
						$sql_inventory2 = $this->db->query("INSERT INTO public.beone_inventory(intvent_trans_id, intvent_trans_no, item_id, trans_date, keterangan, qty_in, value_in, qty_out, value_out, sa_qty, sa_unit_price, sa_amount, flag, update_by, update_date) VALUES (DEFAULT, $transfer_no, $item_hasil[$key], '$tanggal', $keterangan, $qty_hasil[$key], $amount_hasil, 0, 0, $sa_akhir_qty, $unit_price, $sa_akhir_amount, 1, 1, '$update_date')");


						if ($ctr_hasil <= 1){
						//INSERT DI BUKU BESAR
						$amount_wip = $persen_amount + $biaya_hasil[$key];
						$sql_ledger_debet = $this->db->query("INSERT INTO public.beone_gl(
																							gl_id, gl_date, coa_id, coa_no, coa_id_lawan, coa_no_lawan, keterangan, debet, kredit, pasangan_no, gl_number, update_by, update_date)
																							VALUES (DEFAULT, '$tanggal', 13, '116-07', 61, '510-00', $keterangan, $amount_wip, 0, $transfer_no, $transfer_no, 1, '$update_date')");

						$sql_ledger_kredit = $this->db->query("INSERT INTO public.beone_gl(
																							gl_id, gl_date, coa_id, coa_no, coa_id_lawan, coa_no_lawan, keterangan, debet, kredit, pasangan_no, gl_number, update_by, update_date)
																							VALUES (DEFAULT, '$tanggal', 61, '510-00', 13, '116-07', $keterangan, 0, $amount_wip, $transfer_no, $transfer_no, 1, '$update_date')");
							}
					}else{//PEMAKAIAN WIP

						//INSERT DI KARTO STOCK
						$amount_hasil = ($persen_amount + $biaya_hasil[$key]) / $qty_hasil[$key];
						$amount_wip = $persen_amount + $biaya_hasil[$key];

						$sql_inventory2 = $this->db->query("INSERT INTO public.beone_inventory(intvent_trans_id, intvent_trans_no, item_id, trans_date, keterangan, qty_in, value_in, qty_out, value_out, sa_qty, sa_unit_price, sa_amount, flag, update_by, update_date) VALUES (DEFAULT, $transfer_no, $item_hasil[$key], '$tanggal', $keterangan, $qty_hasil[$key], $amount_hasil, 0, 0, $sa_akhir_qty, $unit_price, $sa_akhir_amount, 1, 1, '$update_date')");


						if ($ctr_hasil <= 1){
						$sql_ledger_debet = $this->db->query("INSERT INTO public.beone_gl(
																							gl_id, gl_date, coa_id, coa_no, coa_id_lawan, coa_no_lawan, keterangan, debet, kredit, pasangan_no, gl_number, update_by, update_date)
																							VALUES (DEFAULT, '$tanggal', 14, '116-08', 13, '116-07', $keterangan, $amount_wip, 0, $transfer_no, $transfer_no, 1, '$update_date')");

						$sql_ledger_kredit = $this->db->query("INSERT INTO public.beone_gl(
																							gl_id, gl_date, coa_id, coa_no, coa_id_lawan, coa_no_lawan, keterangan, debet, kredit, pasangan_no, gl_number, update_by, update_date)
																							VALUES (DEFAULT, '$tanggal', 13, '116-07', 14, '116-08', $keterangan, 0, $amount_wip, $transfer_no, $transfer_no, 1, '$update_date')");
						}
					}

			}
		}
		/******************************* END MENJADI ITEM ********************************************/

		if($sql_detail2)
			return true;
		return false;
	}

	public function load_header_stock_transfer(){
		$sql = $this->db->query("SELECT *
															FROM public.beone_transfer_stock
															WHERE flag=1");
		return $sql->result_array();
	}

	public function load_stock_transfer(){
		$sql = $this->db->query("SELECT h.transfer_stock_id, h.transfer_no, h.transfer_date, h.coa_kode_biaya, h.keterangan, h.update_by, h.update_date, h.flag, d.tipe_transfer_stock, d.item_id, d.qty, d.gudang_id, d.biaya
															FROM public.beone_transfer_stock h INNER JOIN public.beone_transfer_stock_detail d ON h.transfer_stock_id = d.transfer_stock_header_id
															WHERE h.flag=1");
		return $sql->result_array();
	}

	public function load_stock_transfer_print($transfer_id){
		$sql = $this->db->query("SELECT h.transfer_stock_id, h.transfer_no, h.transfer_date, h.coa_kode_biaya, h.keterangan, h.update_by, h.update_date, h.flag, d.tipe_transfer_stock, d.item_id, i.nama as namaitem, i.item_code, d.qty, d.gudang_id, d.biaya
															FROM public.beone_transfer_stock h INNER JOIN public.beone_transfer_stock_detail d ON h.transfer_stock_id = d.transfer_stock_header_id INNER JOIN public.beone_item i ON d.item_id = i.item_id
															WHERE h.flag=1 AND h.transfer_stock_id =".intval($transfer_id));
		return $sql->result_array();
	}


}
?>
