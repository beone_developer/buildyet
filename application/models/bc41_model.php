<?php

class bc41_model extends CI_Model {

//    var $tabel = "usr";
 //   private $another;
    function __construct(){
        parent::__construct();
        $this->db = $this ->load -> database('default', TRUE);
        $this->mysql = $this ->load -> database('mysql', TRUE);
    }

    public function select() {
   $sql = $this->mysql->query("SELECT th.* , rs.URAIAN_STATUS
    FROM tpb_header th
    left join referensi_status rs on rs.kode_status = th.KODE_STATUS and rs.KODE_DOKUMEN = th.KODE_DOKUMEN_PABEAN
    WHERE KODE_DOKUMEN_PABEAN = 41");
        return $sql->result_array();
	}
    public function get_default_pengusaha(){
        $sql = $this->mysql->query("select rp.*, rki.URAIAN_KODE_ID as uraian_id_pengusaha 
                from referensi_pengusaha rp 
                join referensi_kode_id rki on rki.KODE_ID = rp.KODE_ID
                where rp.id = 1 limit 1");
        if($sql->num_rows() > 0)
            return $sql->row_array();
        return false;
    }

    public function get_default($ID){
        $sql = $this->mysql->query("SELECT th.*, rjt.URAIAN_JENIS_TPB, rjapengusaha.URAIAN_JENIS_API as uraian_api_pengusaha, rjapemilik.URAIAN_JENIS_API as uraian_api_pemilik, rkipengusaha.URAIAN_KODE_ID as uraian_id_pengusaha, rkipemilik.URAIAN_KODE_ID as uraian_id_pemilik, rkipenerima.URAIAN_KODE_ID as uraian_id_penerima, rs.URAIAN_STATUS, rlb.URAIAN_LOKASI_BAYAR, rp.URAIAN_PEMBAYAR, rca.URAIAN_CARA_ANGKUT, rtt.URAIAN_TUJUAN_TPB, rtp.URAIAN_TUJUAN_PENGIRIMAN, rsts.URAIAN_STATUS as uraian_status_perbaikan
            FROM tpb_header TH 
            left join referensi_jenis_tpb rjt on rjt.KODE_JENIS_TPB = th.KODE_JENIS_TPB 
            left join referensi_jenis_api rjapengusaha on rjapengusaha.KODE_JENIS_API = th.KODE_JENIS_API_PENGUSAHA
            left join referensi_jenis_api rjapemilik on rjapemilik.KODE_JENIS_API = th.KODE_JENIS_API_PEMILIK
            left join referensi_kode_id rkipengusaha on rkipengusaha.KODE_ID = th.KODE_ID_PENGUSAHA
            left join referensi_kode_id rkipemilik on rkipemilik.KODE_ID = th.KODE_ID_PEMILIK
            left join referensi_kode_id rkipenerima on rkipenerima.KODE_ID = th.KODE_ID_PENERIMA_BARANG
            left join referensi_status rs on rs.kode_status = th.KODE_STATUS and rs.KODE_DOKUMEN = th.KODE_DOKUMEN_PABEAN
            left join referensi_lokasi_bayar rlb on rlb.KODE_LOKASI_BAYAR = th.KODE_LOKASI_BAYAR
            left join referensi_pembayar rp on rp.KODE_PEMBAYAR = th.KODE_PEMBAYAR
            left join referensi_cara_angkut rca on rca.KODE_CARA_ANGKUT = th.KODE_CARA_ANGKUT
            left join referensi_tujuan_tpb rtt on rtt.KODE_TUJUAN_TPB = th.KODE_TUJUAN_TPB
            left join referensi_tujuan_pengiriman rtp on rtp.KODE_TUJUAN_PENGIRIMAN = th.KODE_TUJUAN_PENGIRIMAN and rtp.KODE_DOKUMEN = th.KODE_DOKUMEN_PABEAN
            left join referensi_status rsts on rsts.KODE_STATUS = th.KODE_STATUS_PERBAIKAN and rsts.KODE_DOKUMEN = th.KODE_DOKUMEN_PABEAN
            WHERE TH.KODE_DOKUMEN_PABEAN = 41 and TH.ID = ".intval($ID));
        if($sql->num_rows() > 0)
            return $sql->row_array();
        return false;
    }

    public function get_jenis_tpb(){
        $sql = $this->mysql->query("SELECT * FROM referensi_jenis_tpb");
        return $sql->result_array();
    }

    public function get_tujuan_tpb(){
        $sql = $this->mysql->query("SELECT * FROM referensi_tujuan_tpb");
        return $sql->result_array();
    }

    public function get_tujuan_pengiriman(){
        $sql = $this->mysql->query("SELECT * FROM referensi_tujuan_pengiriman where kode_dokumen = 41");
        return $sql->result_array();
    }




    
    public function get_jenis_api(){
        $sql = $this->mysql->query("select * from referensi_jenis_api");
        return $sql->result_array();
    }

    public function get_jenis_id(){
        $sql = $this->mysql->query("select * from referensi_kode_id");
        return $sql->result_array();
    }
    public function get_jenis_lokasi_bayar(){
        $sql = $this->mysql->query("select * from referensi_lokasi_bayar");
        return $sql->result_array();
    }
    public function get_jenis_pembayar(){
        $sql = $this->mysql->query("select * from referensi_pembayar");
        return $sql->result_array();
    }
    public function get_jenis_cara_angkut(){
        $sql = $this->mysql->query("select * from referensi_cara_angkut");
        return $sql->result_array();
    }

    public function get_kemasan($id){
        $sql = $this->mysql->query("select * from tpb_kemasan tk
         left join referensi_kemasan rk on tk.KODE_JENIS_KEMASAN = rk.KODE_KEMASAN
         where tk.id_header = ".intval($id));
        return $sql->result_array();
    }
    // public function get_kontainer($id){
    //     $sql = $this->mysql->query("select * from tpb_kontainer tko
    //         left join referensi_ukuran_kontainer ruk on tko.KODE_UKURAN_KONTAINER = ruk.KODE_UKURAN_KONTAINER
    //         where id_header = ".intval($id));
    //     return $sql->result_array();
    // }
    public function get_dokumen($id){
        $sql = $this->mysql->query("select * from tpb_dokumen td
            left join referensi_dokumen rd on rd.kode_dokumen = td.KODE_JENIS_DOKUMEN
            where id_header = ".intval($id));
        return $sql->result_array();
    }
    public function get_dokumen_luar($id){
        $sql = $this->mysql->query("select * from tpb_dokumen td
            left join referensi_dokumen rd on rd.kode_dokumen = td.KODE_JENIS_DOKUMEN
            where rd.tipe_dokumen <> 02 and id_header = ".intval($id));
        return $sql->result_array();
    }
    public function get_default_dokfp($id){
        $sql = $this->mysql->query("select * from tpb_dokumen td
            left join referensi_dokumen rd on rd.kode_dokumen = td.KODE_JENIS_DOKUMEN
            where URAIAN_DOKUMEN = 'FAKTUR PAJAK' and id_header = ".intval($id)." order by seri_dokumen asc limit 1");
        if($sql->num_rows() > 0)
            return $sql->row_array();
        return false;
    }
    public function get_default_dokpl($id){
        $sql = $this->mysql->query("select * from tpb_dokumen td
            left join referensi_dokumen rd on rd.kode_dokumen = td.KODE_JENIS_DOKUMEN
            where td.KODE_JENIS_DOKUMEN = ('217') and id_header = ".intval($id)." order by seri_dokumen asc limit 1");
        if($sql->num_rows() > 0)
            return $sql->row_array();
        return false;
    }
    public function get_default_dokktr($id){
        $sql = $this->mysql->query("select * from tpb_dokumen td
            left join referensi_dokumen rd on rd.kode_dokumen = td.KODE_JENIS_DOKUMEN
            where td.KODE_JENIS_DOKUMEN = ('315') and id_header = ".intval($id)." order by seri_dokumen asc limit 1");
        if($sql->num_rows() > 0)
            return $sql->row_array();
        return false;
    }
    public function get_default_dokbc40($id){
        $sql = $this->mysql->query("select * from tpb_dokumen td
            left join referensi_dokumen rd on rd.kode_dokumen = td.KODE_JENIS_DOKUMEN
            where td.KODE_JENIS_DOKUMEN = ('40') and id_header = ".intval($id)." order by seri_dokumen asc limit 1");
        if($sql->num_rows() > 0)
            return $sql->row_array();
        return false;
    }
    public function get_default_dokskep($id){
        $sql = $this->mysql->query("select * from tpb_dokumen td
            left join referensi_dokumen rd on rd.kode_dokumen = td.KODE_JENIS_DOKUMEN
            where td.KODE_JENIS_DOKUMEN = ('911') and id_header = ".intval($id)." order by seri_dokumen asc limit 1");
        if($sql->num_rows() > 0)
            return $sql->row_array();
        return false;
    }



    // ============ MODAL =============
    public function get_dokumen_modal($ID){
        $sql = $this->mysql->query("SELECT td.*, rd.KODE_DOKUMEN, rd.URAIAN_DOKUMEN FROM tpb_dokumen td
            LEFT JOIN referensi_dokumen rd on rd.kode_dokumen = td.KODE_JENIS_DOKUMEN
            WHERE id_header = ".intval($ID));
        
        return $sql->result_array();
    }

    public function get_default_modal_dokumen($ID){
        $sql = $this->mysql->query("SELECT td.*, rd.KODE_DOKUMEN, rd.URAIAN_DOKUMEN FROM tpb_dokumen td
            LEFT JOIN referensi_dokumen rd on rd.kode_dokumen = td.KODE_JENIS_DOKUMEN
            WHERE td.id = ".intval($ID));

        if($sql->num_rows() > 0)
            return $sql->row_array();
        return false;

    }

    public function get_kemasan_modal($ID){
        $sql = $this->mysql->query("SELECT tk.*, rk.KODE_KEMASAN, rk.URAIAN_KEMASAN from tpb_kemasan tk
         left join referensi_kemasan rk on tk.KODE_JENIS_KEMASAN = rk.KODE_KEMASAN
         where tk.id_header = ".intval($ID));
        
        return $sql->result_array();
    }

    public function get_default_modal_kemasan($ID){

        $sql = $this->mysql->query("SELECT tk.*, rk.KODE_KEMASAN, rk.URAIAN_KEMASAN from tpb_kemasan tk
         left join referensi_kemasan rk on tk.KODE_JENIS_KEMASAN = rk.KODE_KEMASAN
         where tk.ID = ".intval($ID));

        if($sql->num_rows() > 0)
            return $sql->row_array();
        return false;

    }

    public function get_daftar_respon($id){

       $sql = $this->mysql->query("SELECT tr.*, rp.URAIAN_RESPON
                                   FROM tpb_respon tr
                                   LEFT JOIN referensi_respon rp on tr.KODE_RESPON = rp.KODE_RESPON
                                   WHERE ID_HEADER = ".intval($id));

       return $sql->result_array();
   }

   public function get_daftar_status($id){

       $sql = $this->mysql->query("SELECT tds.*, rs.URAIAN_STATUS
                                   from tpb_detil_status tds
                                   left join referensi_status rs on rs.kode_status = tds.KODE_STATUS
                                   where id_header = ".intval($id));

       return $sql->result_array();
   }

   public function get_kontainer($id) {
     $sql = $this->mysql->query("SELECT tk.id as id_kontainer, tk.NOMOR_KONTAINER, ruk.URAIAN_UKURAN_KONTAINER, rtk.URAIAN_TIPE_KONTAINER from tpb_kontainer tk 
                    left join referensi_ukuran_kontainer ruk on tk.KODE_UKURAN_KONTAINER = ruk.KODE_UKURAN_KONTAINER
                    left join referensi_tipe_kontainer rtk on tk.KODE_TIPE_KONTAINER = rtk.KODE_TIPE_KONTAINER
                    where tk.id_header = ".intval($id));
     
       return $sql->result_array();
    }


    public function simpan($post){
        $KODE_STATUS = $this->mysql->escape($post['status']);
        $KODE_STATUS_PERBAIKAN = $this->mysql->escape($post['status_perbaikan']);
        $NOMOR_AJU = $this->mysql->escape($post['nomor_pengajuan']);
        $NOMOR_DAFTAR = $this->mysql->escape($post['nomor_pendaftaran']);
        $TANGGAL_DAFTAR = $this->mysql->escape($post['tanggal_pendaftaran']);
        $KODE_KANTOR = $this->mysql->escape($post['kantor_pabean']);
        $KODE_JENIS_TPB = $this->mysql->escape($post['jenis_tpb']);
        $KODE_TUJUAN_PENGIRIMAN = $this->mysql->escape($post['tujuan_pengiriman']);
        $KODE_ID_PENGUSAHA = $this->mysql->escape($post['kode_id_pengusaha']);
        $ID_PENGUSAHA = $this->mysql->escape($post['npwp_pengusaha']);
        $NAMA_PENGUSAHA = $this->mysql->escape($post['nama_pengusaha']);
        $ALAMAT_PENGUSAHA = $this->mysql->escape($post['alamat_pengusaha']);
        $NOMOR_IJIN_TPB = $this->mysql->escape($post['no_izin']);
        $KODE_ID_PENERIMA_BARANG = $this->mysql->escape($post['kode_id_penerima']);
        $ID_PENERIMA_BARANG = $this->mysql->escape($post['npwp_penerima']);
        $NAMA_PENERIMA_BARANG = $this->mysql->escape($post['nama_penerima']);
        $ALAMAT_PENERIMA_BARANG = $this->mysql->escape($post['alamat_penerima']);
        $NAMA_PENGANGKUT = $this->mysql->escape($post['pengangkut_darat']);
        $NOMOR_POLISI = $this->mysql->escape($post['nopol']);
        $HARGA_PENYERAHAN = $this->mysql->escape($post['harga_penyerahan']);
        $VOLUME = $this->mysql->escape($post['volume']);
        $BRUTO = $this->mysql->escape($post['bruto']);
        $NETTO = $this->mysql->escape($post['netto']);
        $JUMLAH_BARANG = $this->mysql->escape($post['jumlah_barang']);
        $KOTA_TTD = $this->mysql->escape($post['kota_ttd']);
        $NAMA_TTD = $this->mysql->escape($post['nama_ttd']);
        $JABATAN_TTD = $this->mysql->escape($post['jabatan_ttd']);
        $TANGGAL_TTD = $this->mysql->escape($post['tgl_ttd']);
        // dokumen
        // $NOMOR_POLISI = $this->mysql->escape($post['nosegel']);
        // $NOMOR_POLISI = $this->mysql->escape($post['jenis_segel']);
        // $NOMOR_POLISI = $this->mysql->escape($post['catatan_bc']);


        //insert akun
        $sql = $this->mysql->query("INSERT INTO tpb_header(KODE_STATUS, KODE_STATUS_PERBAIKAN, NOMOR_AJU, NOMOR_DAFTAR, TANGGAL_DAFTAR, KODE_KANTOR, KODE_JENIS_TPB, KODE_TUJUAN_PENGIRIMAN, KODE_ID_PENGUSAHA, ID_PENGUSAHA, NAMA_PENGUSAHA, ALAMAT_PENGUSAHA, NOMOR_IJIN_TPB, KODE_ID_PENERIMA_BARANG, ID_PENERIMA_BARANG, NAMA_PENERIMA_BARANG, ALAMAT_PENERIMA_BARANG, HARGA_PENYERAHAN, NAMA_PENGANGKUT, NOMOR_POLISI, VOLUME,  BRUTO, NETTO, JUMLAH_BARANG, KOTA_TTD, NAMA_TTD, JABATAN_TTD, KODE_DOKUMEN_PABEAN, ID_MODUL, TANGGAL_TTD, VERSI_MODUL) VALUES ('0', $KODE_STATUS_PERBAIKAN, $NOMOR_AJU, $NOMOR_DAFTAR, $TANGGAL_DAFTAR, $KODE_KANTOR, $KODE_JENIS_TPB, $KODE_TUJUAN_PENGIRIMAN, $KODE_ID_PENGUSAHA, $ID_PENGUSAHA, $NAMA_PENGUSAHA, $ALAMAT_PENGUSAHA, $NOMOR_IJIN_TPB, $KODE_ID_PENERIMA_BARANG, $ID_PENERIMA_BARANG, $NAMA_PENERIMA_BARANG, $ALAMAT_PENERIMA_BARANG, $HARGA_PENYERAHAN, $NAMA_PENGANGKUT, $NOMOR_POLISI, $VOLUME, $BRUTO, $NETTO, $JUMLAH_BARANG, $KOTA_TTD, $NAMA_TTD, $JABATAN_TTD, '41', '15346', $TANGGAL_TTD, '3.1.8')");

        if($sql)
            return true;
        return false;
    }

    public function update($post, $ID){
        $KODE_STATUS = $this->mysql->escape($post['status']);
        $KODE_STATUS_PERBAIKAN = $this->mysql->escape($post['status_perbaikan']);
        $NOMOR_AJU = $this->mysql->escape($post['nomor_pengajuan']);
        $NOMOR_DAFTAR = $this->mysql->escape($post['nomor_pendaftaran']);
        $TANGGAL_DAFTAR = $this->mysql->escape($post['tanggal_pendaftaran']);
        $KODE_KANTOR = $this->mysql->escape($post['kantor_pabean']);
        $KODE_JENIS_TPB = $this->mysql->escape($post['jenis_tpb']);
        $KODE_TUJUAN_PENGIRIMAN = $this->mysql->escape($post['tujuan_pengiriman']);
        $KODE_ID_PENGUSAHA = $this->mysql->escape($post['kode_id_pengusaha']);
        $ID_PENGUSAHA = $this->mysql->escape($post['npwp_pengusaha']);
        $NAMA_PENGUSAHA = $this->mysql->escape($post['nama_pengusaha']);
        $ALAMAT_PENGUSAHA = $this->mysql->escape($post['alamat_pengusaha']);
        $NOMOR_IJIN_TPB = $this->mysql->escape($post['no_izin']);
        $KODE_ID_PENERIMA_BARANG = $this->mysql->escape($post['kode_id_penerima']);
        $ID_PENERIMA_BARANG = $this->mysql->escape($post['npwp_penerima']);
        $NAMA_PENERIMA_BARANG = $this->mysql->escape($post['nama_penerima']);
        $ALAMAT_PENERIMA_BARANG = $this->mysql->escape($post['alamat_penerima']);
        $NAMA_PENGANGKUT = $this->mysql->escape($post['pengangkut_darat']);
        $NOMOR_POLISI = $this->mysql->escape($post['nopol']);
        $HARGA_PENYERAHAN = $this->mysql->escape($post['harga_penyerahan']);
        $VOLUME = $this->mysql->escape($post['volume']);
        $BRUTO = $this->mysql->escape($post['bruto']);
        $NETTO = $this->mysql->escape($post['netto']);
        $JUMLAH_BARANG = $this->mysql->escape($post['jumlah_barang']);
        $KOTA_TTD = $this->mysql->escape($post['kota_ttd']);
        $NAMA_TTD = $this->mysql->escape($post['nama_ttd']);
        $JABATAN_TTD = $this->mysql->escape($post['jabatan_ttd']);
        $TANGGAL_TTD = $this->mysql->escape($post['tgl_ttd']);
        // dokumen
        // $NOMOR_POLISI = $this->mysql->escape($post['nosegel']);
        // $NOMOR_POLISI = $this->mysql->escape($post['jenis_segel']);
        // $NOMOR_POLISI = $this->mysql->escape($post['catatan_bc']);


        //insert akun
        $sql = $this->mysql->query("UPDATE tpb_header SET KODE_STATUS =$KODE_STATUS, KODE_STATUS_PERBAIKAN = $KODE_STATUS_PERBAIKAN, NOMOR_AJU = $NOMOR_AJU, NOMOR_DAFTAR = $NOMOR_DAFTAR, TANGGAL_DAFTAR = $TANGGAL_DAFTAR, KODE_KANTOR = $KODE_KANTOR, KODE_JENIS_TPB = $KODE_JENIS_TPB, KODE_TUJUAN_PENGIRIMAN = $KODE_TUJUAN_PENGIRIMAN, KODE_ID_PENGUSAHA = $KODE_ID_PENGUSAHA, ID_PENGUSAHA = $ID_PENGUSAHA, NAMA_PENGUSAHA = $NAMA_PENGUSAHA, ALAMAT_PENGUSAHA = $ALAMAT_PENGUSAHA, NOMOR_IJIN_TPB = $NOMOR_IJIN_TPB, KODE_ID_PENERIMA_BARANG = $KODE_ID_PENERIMA_BARANG, ID_PENERIMA_BARANG = $ID_PENERIMA_BARANG, NAMA_PENERIMA_BARANG = $NAMA_PENERIMA_BARANG, ALAMAT_PENERIMA_BARANG = $ALAMAT_PENERIMA_BARANG, HARGA_PENYERAHAN = $HARGA_PENYERAHAN, NAMA_PENGANGKUT = $NAMA_PENGANGKUT, NOMOR_POLISI = $NOMOR_POLISI, VOLUME = $VOLUME,  BRUTO = $BRUTO, NETTO = $NETTO, JUMLAH_BARANG = $JUMLAH_BARANG, KOTA_TTD = $KOTA_TTD, NAMA_TTD = $NAMA_TTD, JABATAN_TTD = $JABATAN_TTD, KODE_DOKUMEN_PABEAN = '41', ID_MODUL = '15346', VERSI_MODUL = '3.1.8' WHERE ID = ".intval($ID));

        if($sql)
            return true;
        return false;
    }

    public function updatedokumen($post){
        $ID = $this->mysql->escape($post['id_dokumen']);
        $KODE_JENIS_DOKUMEN = $this->mysql->escape($post['jenis_dokumen']);
        $NOMOR_DOKUMEN = $this->mysql->escape($post['nomor_dokumen']);
        $TANGGAL_DOKUMEN = $this->mysql->escape($post['tgl_dokumen']);

        $sql = $this->mysql->query("UPDATE tpb_dokumen SET KODE_JENIS_DOKUMEN = $KODE_JENIS_DOKUMEN, NOMOR_DOKUMEN = $NOMOR_DOKUMEN, TANGGAL_DOKUMEN = $TANGGAL_DOKUMEN WHERE ID = $ID");

        if($sql)
            return true;
        return false;
    }

    public function updatekemasan($post){
        $ID = $this->mysql->escape($post['id_kemasan']);
        $JUMLAH_KEMASAN = $this->mysql->escape($post['jumlah_kemasan']);
        $KODE_JENIS_KEMASAN = $this->mysql->escape($post['jenis_kemasan']);
        $MERK_KEMASAN = $this->mysql->escape($post['merk_kemasan']);

        $sql = $this->mysql->query("UPDATE tpb_kemasan SET JUMLAH_KEMASAN = $JUMLAH_KEMASAN, KODE_JENIS_KEMASAN = $KODE_JENIS_KEMASAN, MERK_KEMASAN = $MERK_KEMASAN WHERE ID = $ID");

        if($sql)
            return true;
        return false;
    }


    public function delete($id) {
        
        $delete_pungutan = "delete from tpb_pungutan where ID_HEADER = '" . $id . "'";
        $data1 = $this->mysql->query($delete_pungutan);
        $delete_kontainer = "delete from tpb_kontainer where ID_HEADER = '" . $id . "'";
        $data2 = $this->mysql->query($delete_kontainer);
        $delete_kemasan = "delete from tpb_kemasan where ID_HEADER = '" . $id . "'";
        $data3 = $this->mysql->query($delete_kemasan);
        $delete_dokumen = "delete from tpb_dokumen where ID_HEADER = '" . $id . "'";
        $data4 = $this->mysql->query($delete_dokumen);
        $delete_brgtarif = "delete from tpb_barang_tarif where ID_HEADER = '" . $id . "'";
        $data5 = $this->mysql->query($delete_brgtarif);
        $delete_dtl= "delete from tpb_detil_status where ID_HEADER = '" . $id . "'";
        $data7 = $this->mysql->query($delete_dtl);
        $delete_val= "delete from hasil_validasi_header where ID_HEADER = '" . $id . "'";
        $data8 = $this->mysql->query($delete_val);
        $delete_bbdok= "delete from tpb_bahan_baku_dokumen where ID_HEADER = '" . $id . "'";
        $data10 = $this->mysql->query($delete_bbdok);
        $delete_bbtar= "delete from tpb_bahan_baku_tarif where ID_HEADER = '" . $id . "'";
        $data11 = $this->mysql->query($delete_bbtar);
        $delete_bb= "delete from tpb_bahan_baku where ID_HEADER = '" . $id . "'";
        $data9 = $this->mysql->query($delete_bb);
        $delete_brg= "delete from tpb_barang where ID_HEADER = '" . $id . "'";
        $data6 = $this->mysql->query($delete_brg);
        $delete_brgdok= "delete from tpb_barang_dokumen where ID_HEADER = '" . $id . "'";
        $data12 = $this->mysql->query($delete_brgdok);
        $delete_brgter= "delete from tpb_barang_penerima where ID_HEADER = '" . $id . "'";
        $data13 = $this->mysql->query($delete_brgter);
        $delete_jamin= "delete from tpb_jaminan where ID_HEADER = '" . $id . "'";
        $data14 = $this->mysql->query($delete_jamin);
        $delete_billing= "delete from tpb_npwp_billing where ID_HEADER = '" . $id . "'";
        $data15 = $this->mysql->query($delete_billing);
        $delete_penerima= "delete from tpb_penerima where ID_HEADER = '" . $id . "'";
        $data16 = $this->mysql->query($delete_penerima);
        $delete_respon = "delete from tpb_respon where ID_HEADER = '" . $id . "'";
        $data17 = $this->mysql->query($delete_respon);
        
        $delete = "delete from tpb_header where ID = '" . $id . "'";
        $data = $this->mysql->query($delete);
        if ($data)
            return true;
        return false;
    }


    public function delete_dokumen($id) {
        $select = "delete from tpb_dokumen where id = '" . $id . "'";
        $data = $this->mysql->query($select);
        if ($data)
            return true;
        return false;
    }

    public function delete_kemasan($id) {
        $select = "delete from tpb_kemasan where id = '" . $id . "'";
        $data = $this->mysql->query($select);
        if ($data)
            return true;
        return false;
    }

    
    // public function delete($id){
    //     $sql = $this->mysql->query("DELETE FROM tpb_header WHERE ID = ".intval($id));
    // }
   
    
    
    }

?>
