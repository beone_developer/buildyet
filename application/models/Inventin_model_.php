<?php
//Model_data.php
defined('BASEPATH') OR exit('No direct script access allowed');

Class Inventin_model extends CI_Model{
	function __construct(){
			parent::__construct();
			$this->db = $this ->load -> database('default', TRUE);
			$this->mysql = $this ->load -> database('mysql', TRUE);
	}

	public function received($post, $import_header_id){
		$session_id = $this->session->userdata('user_id');
		$received_date = $this->db->escape($post['received_date']);
		$received_no = $this->db->escape($post['received_no']);
		$kurs = $this->db->escape($post['kurs']);
		$supplier_id = $this->db->escape($post['supplier']);
		//$jml_item = $this->db->escape($post['jml_item']);
		$nomor_aju = $this->db->escape($post['nomor_aju']);
		$harga_invoice = $this->db->escape($post['harga_invoice']);
		$purchase_header_id = $this->db->escape($post['nomor_po']);

		$sql_po = $this->db->query("SELECT purchase_no FROM public.beone_po_import_header WHERE purchase_header_id = $purchase_header_id");
		$hasil_po = $sql_po->row_array();
		$nomor_po = $hasil_po['purchase_no'];

		$tgl_bulan = substr($received_date, 1, 2);
		$tgl_hari = substr($received_date, 4, 2);
		$tgl_tahun = substr($received_date, 7, 4);
		$update_date = date('Y-m-d');


		$tanggal = $tgl_tahun."-".$tgl_bulan."-".$tgl_hari;

		//select header tpb
		$select = "select * from tpb_header where id = ".intval($import_header_id);
		$header_tpb_ = $this->mysql->query($select);
		$header_tpb = $header_tpb_->row_array();

		//insert table received import
		$sql_received = $this->db->query("INSERT INTO public.beone_received_import(
																			received_id, nomor_aju, nomor_dokumen_pabean, tanggal_received, status_received, flag, update_by, update_date, nomor_received, tpb_header_id, kurs, po_import_header_id)
																			VALUES (DEFAULT, '$header_tpb[NOMOR_AJU]', $header_tpb[KODE_DOKUMEN_PABEAN], '$tanggal', 1, 1, $session_id, '$update_date', $received_no, $header_tpb[ID], $kurs, $purchase_header_id)");

		helper_log($tipe = "import", $str = "Terima Barang Import, No AJU ".$header_tpb['NOMOR_AJU']);

		$sql_update_po = $this->db->query("UPDATE public.beone_po_import_header SET flag = 0 WHERE purchase_header_id = $purchase_header_id");

		$hrg_invoice_valas = 0;
		$hrg_invoice_idr = 0;

//------------------------------------------------- item -------------------------------------------------
$hrg_invoice_valas = 0;
$hrg_invoice_idr = 0;
foreach ($_POST['rows'] as $key => $count ){
		 $item = $_POST['item_id_'.$count];
		 $qty_ = $_POST['qty_'.$count];
		 $price_ = $_POST['price_'.$count];
		 $amount_ = $_POST['amount_'.$count];

		 $qty_ex = str_replace(".", "", $qty_);
		 $price_ex = str_replace(".", "", $price_);
		 $amount_ex = str_replace(".", "", $amount_);

		 $qty = str_replace(",", ".", $qty_ex);
		 $price = str_replace(",", ".", $price_ex);
		 $amount = str_replace(",", ".", $amount_ex);

		 $price_idr = $price * $post['kurs'];
		 $amount_idr = $amount * $post['kurs'];

		 $hrg_invoice_valas = $hrg_invoice_valas + $amount;
		 $hrg_invoice_idr = $hrg_invoice_idr + $amount_idr;


		 //insert mutasi gudang detail (ubah penerimaan pertama kali di bahan baku)
			 $sql_gudang_detail = $this->db->query("INSERT INTO public.beone_gudang_detail(
																						 gudang_detail_id, gudang_id, trans_date, item_id, qty_in, qty_out, nomor_transaksi, update_by, update_date, flag, keterangan)
																						 VALUES (DEFAULT, 1, '$tanggal', $item, $qty, 0, $nomor_aju, $session_id, '$update_date', 1, 'IMPORT')");

		 //cek saldo awal
		 $inv = $this->db->query("SELECT * FROM public.beone_inventory WHERE flag = 1 AND item_id = $item ORDER BY trans_date DESC, intvent_trans_id DESC LIMIT 1");
		 $hasil_inv = $inv->row_array();
		 //end cek saldo awal

		 //SALDO AWAL ITEM
		 $sai = $this->db->query("SELECT * FROM public.beone_item WHERE item_id =".intval($item));
		 $saldo_awal_item = $sai->row_array();

		 if ($hasil_inv['qty_in'] == NULL){//menggunakan saldo awal item
			 $unit_price = $price;

			 $sa_qty= $saldo_awal_item['saldo_qty'];
			 $s_akhir_qty = $sa_qty+$qty;
			 $s_akhir_amount = $saldo_awal_item['saldo_idr'] + ($qty * $unit_price);

			 if ($s_akhir_amount == 0 OR $s_akhir_qty == 0){
					 $unit_price_akhir = 0;
			 }else{
					 $unit_price_akhir = $s_akhir_amount / $s_akhir_qty;
			 }

			 //$jml_hutang = $qty * $price;
			 $jml_hutang = $qty * $price_idr;

			 $sql = $this->db->query("INSERT INTO public.beone_inventory(intvent_trans_id, intvent_trans_no, item_id, trans_date, keterangan, qty_in, value_in, qty_out, value_out, sa_qty, sa_unit_price, sa_amount, flag, update_by, update_date)
			 VALUES (DEFAULT, '$header_tpb[NOMOR_AJU]', $item, '$tanggal', 'IMPOR', $qty, $unit_price, 0, 0, $s_akhir_qty, $unit_price_akhir, $s_akhir_amount,1, $session_id, '$update_date')");

		 }else{//menggunakan saldo awal inventory
			 $sa_qty = $hasil_inv['sa_qty'];
			 $s_akhir_qty = $sa_qty+$qty;
			 //$unit_price_awal = $hasil_inv['sa_unit_price'];
			 $unit_price_awal = ($qty * $price_idr) / $qty;
			 $s_akhir_amount = $hasil_inv['sa_amount'] + ($qty * $price_idr);

			 //$jml_hutang = $qty * $price;
			 $jml_hutang = $qty * $price_idr;

			 //$unit_price = $hasil_inv['sa_unit_price'];
			 if ($s_akhir_amount == 0 OR $s_akhir_qty == 0){
					 $unit_price_akhir = 0;
			 }else{
					 $unit_price_akhir = $s_akhir_amount / $s_akhir_qty;
			 }

			 $sql = $this->db->query("INSERT INTO public.beone_inventory(intvent_trans_id, intvent_trans_no, item_id, trans_date, keterangan, qty_in, value_in, qty_out, value_out, sa_qty, sa_unit_price, sa_amount, flag, update_by, update_date)
			 VALUES (DEFAULT, '$header_tpb[NOMOR_AJU]', $item, '$tanggal', 'IMPOR', $qty, $unit_price_awal, 0, 0, $s_akhir_qty, $unit_price_akhir, $s_akhir_amount,1, $session_id, '$update_date')");

 }

		//------------------------------------------------- item -------------------------------------------------

				$sql_hp = $this->db->query("INSERT INTO public.beone_hutang_piutang(hutang_piutang_id, custsup_id, trans_date, nomor, keterangan, valas_trans, idr_trans, valas_pelunasan, idr_pelunasan, tipe_trans, update_by, update_date, flag, status_lunas)
				VALUES (DEFAULT, $supplier_id, '$tanggal','$header_tpb[NOMOR_AJU]', 'IMPOR', $hrg_invoice_valas, $hrg_invoice_idr, 0, 0, 1, $session_id,'$update_date', 1, 0)");

				$coa_jurnal_persediaan = $this->db->query("SELECT * FROM public.beone_coa_jurnal WHERE coa_jurnal_id = 1"); //coa jurnal persediaan
				$coa_persediaan = $coa_jurnal_persediaan->row_array();
				$cj_id = $coa_persediaan['coa_id'];
				$cj_no = $coa_persediaan['coa_no'];

				$coa_jurnal_hutang_usaha_import = $this->db->query("SELECT * FROM public.beone_coa_jurnal WHERE coa_jurnal_id = 8"); //coa jurnal hutang usaha import
				$coa_hutang_usaha_import = $coa_jurnal_hutang_usaha_import->row_array();
				$chui_id = $coa_hutang_usaha_import['coa_id'];
				$chui_no = $coa_hutang_usaha_import['coa_no'];

				//insert akun ke general ledger (buku besar)
				$sql_debet = $this->db->query("INSERT INTO public.beone_gl VALUES (DEFAULT, '$tanggal', $cj_id, '$cj_no', $chui_id, '$chui_no', $received_no, $amount_idr ,0, '$header_tpb[NOMOR_AJU]', '$header_tpb[NOMOR_AJU]' ,$session_id, '$update_date')");

				//insert lawan akun ke general ledger (buku besar)
				$sql_kredit = $this->db->query("INSERT INTO public.beone_gl VALUES (DEFAULT, '$tanggal', $chui_id, '$chui_no', $cj_id, '$cj_no', $received_no, 0 ,$amount_idr, '$header_tpb[NOMOR_AJU]', '$header_tpb[NOMOR_AJU]' ,$session_id, '$update_date')");

		if($sql_received)
			return true;
		return false;
	}
}


	public function deliverd($post, $export_header_id){
		$session_id = $this->session->userdata('user_id');
		$deliverd_date = $this->db->escape($post['deliverd_date']);
		$deliverd_no = $this->db->escape($post['deliverd_no']);

		$tgl_bulan = substr($deliverd_date, 1, 2);
		$tgl_hari = substr($deliverd_date, 4, 2);
		$tgl_tahun = substr($deliverd_date, 7, 4);


		$tanggal = $tgl_tahun."-".$tgl_bulan."-".$tgl_hari;

		//insert table export detail
		$sql = $this->db->query("UPDATE public.beone_export_header SET status=1, delivery_no=$deliverd_no, delivery_date='$tanggal' WHERE export_header_id = ".intval($export_header_id));

		$id = intval($export_header_id);
		$detail = $this->db->query("SELECT  h.bc_no, h.bc_date, h.delivery_date, d.item_id, d.qty, d.price, h.valas_value, h.amount_value, h.receiver_id, d.doc
																FROM public.beone_export_header h INNER JOIN public.beone_export_detail d ON h.export_header_id = d.export_header_id
																WHERE h.export_header_id = $id");

		$total_piutang_eksport = 0;
		$cust = 0;
		$amount_idr = 0;
		$value_out = 0;
		foreach($detail->result_array() as $row){
				$item = $row['item_id'];
				$doc = $row['doc'];
				$qty = $row['qty'];
				$price = $row['price'];
				$bc = $row['bc_no'];
				$date = $row['delivery_date'];
				$valas = $row['valas_value'];
				$amount = $row['amount_value'];
				$customer = $row['receiver_id'];
				$update_date = date('Y-m-d');
				//$ctr = $ctr + 1; //counter supaya hutang hanya diinsert sekali
				$cust = $customer;
				$amount_idr = $amount;

				//cek saldo awal
				$inv = $this->db->query("SELECT * FROM public.beone_inventory WHERE flag = 1 AND item_id = $row[item_id] ORDER BY trans_date DESC, intvent_trans_id DESC LIMIT 1");
				$hasil_inv = $inv->row_array();
				//end cek saldo awal

				//SALDO AWAL ITEM
				$sai = $this->db->query("SELECT * FROM public.beone_item WHERE item_id =".intval($item));
				$saldo_awal_item = $sai->row_array();

				if ($hasil_inv['qty_in'] == NULL){
						if ($saldo_awal_item['saldo_idr'] == 0 OR $saldo_awal_item['saldo_qty'] == 0){
								$unit_price_awal = 0; // salah, harusnya amount saat ini dibagi qty saat ini
						}else{
								$unit_price_awal = $saldo_awal_item['saldo_idr'] / $saldo_awal_item['saldo_qty'];
						}

						$sa_akhir_qty = $saldo_awal_item['saldo_qty'] - $qty;
						$sa_akhir_amount = $saldo_awal_item['saldo_idr'] - ($qty * $unit_price_awal);
						$unit_price_akhir = $sa_akhir_amount / $sa_akhir_qty;

						$sql_inventory = $this->db->query("INSERT INTO public.beone_inventory(intvent_trans_id, intvent_trans_no, item_id, trans_date, keterangan, qty_in, value_in, qty_out, value_out, sa_qty, sa_unit_price, sa_amount, flag, update_by, update_date) VALUES (DEFAULT, $deliverd_no, $item, '$tanggal', 'KETERANGAN EXPORT', 0, 0, $qty, $unit_price_awal, $sa_akhir_qty, $unit_price_akhir, $sa_akhir_amount, 1, $session_id, '$update_date')");
						$total_piutang_eksport = $total_piutang_eksport + ($qty * $unit_price_awal);
				}else{
						$sa_qty = $hasil_inv['sa_qty'];
						$sa_akhir_qty = $sa_qty-$qty;

						$sa_amount = $hasil_inv['sa_amount'];
						$sa_akhir_amount = $hasil_inv['sa_amount'] - ($qty * $hasil_inv['sa_unit_price']);

						$unit_price_awal = $hasil_inv['sa_unit_price'];
						if ($sa_akhir_amount == 0 OR $sa_akhir_qty == 0){
								$unit_price_akhir = 0;
								$sa_akhir_amount = 0;
						}else{
								$unit_price_akhir = $sa_akhir_amount / $sa_akhir_qty;
						}

						$sql_inventory = $this->db->query("INSERT INTO public.beone_inventory(intvent_trans_id, intvent_trans_no, item_id, trans_date, keterangan, qty_in, value_in, qty_out, value_out, sa_qty, sa_unit_price, sa_amount, flag, update_by, update_date) VALUES (DEFAULT, $deliverd_no, $item, '$tanggal', 'KETERANGAN EXPORT', 0, 0, $qty, $unit_price_awal, $sa_akhir_qty, $unit_price_akhir, $sa_akhir_amount, 1, $session_id, '$update_date')");
						$total_piutang_eksport = $total_piutang_eksport + ($qty * $unit_price_awal);
				}


					//insert mutasi gudang detail
					$sql_gudang_detail = $this->db->query("INSERT INTO public.beone_gudang_detail(
																								gudang_detail_id, gudang_id, trans_date, item_id, qty_in, qty_out, nomor_transaksi, update_by, update_date, flag, keterangan)
																								VALUES (DEFAULT, 2, '$date', $item, 0, $qty, '$bc', $session_id, '$update_date', 1, '$doc')");

					//amount hpp
					$inv_hpp = $this->db->query("SELECT * FROM public.beone_inventory WHERE flag = 1 AND item_id = $row[item_id] ORDER BY trans_date DESC, intvent_trans_id DESC LIMIT 1");
					$hasil_inv_hpp = $inv_hpp->row_array();
					$vo = $hasil_inv['value_out'];
					$value_out = ($value_out*1) + ($vo*1);
					//end amount hpp
		}

		$sql = $this->db->query("INSERT INTO public.beone_hutang_piutang(hutang_piutang_id, custsup_id, trans_date, nomor, keterangan, valas_trans, idr_trans, valas_pelunasan, idr_pelunasan, tipe_trans, update_by, update_date, flag, status_lunas)
		VALUES (DEFAULT, $cust, '$date','$bc', 'EKSPOR', $valas, 0, 0, 0, 0, $session_id,'$update_date', 1, 0)");

		$coa_jurnal_hpp = $this->db->query("SELECT * FROM public.beone_coa_jurnal WHERE coa_jurnal_id = 4"); //coa HPP
		$coa_hpp = $coa_jurnal_hpp->row_array();
		$chpp_id = $coa_hpp['coa_id'];
		$chpp_no = $coa_hpp['coa_no'];

		$coa_jurnal_persediaan_barang_jadi = $this->db->query("SELECT * FROM public.beone_coa_jurnal WHERE coa_jurnal_id = 5"); //coa Persediaan Barang Jadi
		$coa_persediaan_barang_jadi = $coa_jurnal_persediaan_barang_jadi->row_array();
		$cpbj_id = $coa_persediaan_barang_jadi['coa_id'];
		$cpbj_no = $coa_persediaan_barang_jadi['coa_no'];


		$coa_jurnal_piutang_usaha = $this->db->query("SELECT * FROM public.beone_coa_jurnal WHERE coa_jurnal_id = 6"); //coa Piutang Usaha
		$coa_piutang_usaha = $coa_jurnal_piutang_usaha->row_array();
		$cpu_id = $coa_piutang_usaha['coa_id'];
		$cpu_no = $coa_piutang_usaha['coa_no'];

		$coa_jurnal_penjualan_ekspor = $this->db->query("SELECT * FROM public.beone_coa_jurnal WHERE coa_jurnal_id = 12"); //coa Penjualan Ekspor
		$coa_penjualan_ekspor = $coa_jurnal_penjualan_ekspor->row_array();
		$cpe_id = $coa_penjualan_ekspor['coa_id'];
		$cpe_no = $coa_penjualan_ekspor['coa_no'];


		$sql_debet1 = $this->db->query("INSERT INTO public.beone_gl VALUES (DEFAULT, '$date', $chpp_id, '$chpp_no', $cpbj_id, '$cpbj_no', '$bc', $value_out ,0, '$bc', '$bc' ,$session_id, '$update_date')");
		$sql_kredit1 = $this->db->query("INSERT INTO public.beone_gl VALUES (DEFAULT, '$date', $cpbj_id, '$cpbj_no', $chpp_id, '$chpp_no', '$bc', 0 ,$value_out, '$bc', '$bc' ,$session_id, '$update_date')");

		$sql_debet2 = $this->db->query("INSERT INTO public.beone_gl VALUES (DEFAULT, '$date', $cpu_id, '$cpu_no', $cpe_id, '$cpe_no', '$bc', $amount_idr ,0, '$bc', '$bc' ,$session_id, '$update_date')");
		$sql_kredit2 = $this->db->query("INSERT INTO public.beone_gl VALUES (DEFAULT, '$date', $cpe_id, '$cpe_no', $cpu_id, '$cpu_no', '$bc', 0 ,$amount_idr, '$bc', '$bc' ,$session_id, '$update_date')");

		if($sql)
			return true;
		return false;
	}

	public function load_inventory(){
		$sql = $this->db->query("SELECT intvent_trans_id, intvent_trans_no, item_id, trans_date, keterangan, qty_in, value_in, qty_out, value_out, sa_qty, sa_unit_price, sa_amount, flag, update_by, update_date
															FROM public.beone_inventory WHERE flag = 1 ORDER BY trans_date ASC");
		return $sql->result_array();
	}

	public function get_kurs($id_header){
		$sql = $this->mysql->query("select ndpbm from tpb_header where id = ".intval($id_header));
		if($sql->num_rows() > 0)
			return $sql->row_array();
		return false;
	}

	public function get_supplier($id_header){
		$sql = $this->mysql->query("select nama_pemasok from tpb_header where id = ".intval($id_header));
		if($sql->num_rows() > 0)
			return $sql->row_array();
		return false;
	}

	public function get_pengirim($id_header){
		$sql = $this->mysql->query("select nama_pengirim from tpb_header where id = ".intval($id_header));
		if($sql->num_rows() > 0)
			return $sql->row_array();
		return false;
	}

	public function get_tanggal($id_header){
		$sql = $this->mysql->query("select tanggal_aju from tpb_header where id = ".intval($id_header));
		if($sql->num_rows() > 0)
			return $sql->row_array();
		return false;
	}

	public function get_penerima($id_header){
		$sql = $this->mysql->query("select nama_penerima_barang from tpb_header where id = ".intval($id_header));
		if($sql->num_rows() > 0)
			return $sql->row_array();
		return false;
	}

	public function get_nomor_aju($id_header){
		$sql = $this->mysql->query("select nomor_aju, harga_invoice, harga_penyerahan from tpb_header where id = ".intval($id_header));
		if($sql->num_rows() > 0)
			return $sql->row_array();
		return false;
	}

	public function get_item($id_header){
		$sql = $this->mysql->query("select uraian, jumlah_satuan, harga_satuan, harga_penyerahan, harga_invoice from tpb_barang where id_header = ".intval($id_header));
		return $sql->result_array();
	}

	public function load_tracing_doc($no_aju){
		$sql = $this->db->query("SELECT d.nomor_transaksi, d.gudang_detail_id, d.gudang_id, gd.nama as ngudang, d.trans_date, d.item_id, i.nama as nitem, d.qty_in, d.qty_out, d.nomor_transaksi, d.update_by, u.nama as nuser, d.update_date, d.flag, d.keterangan, d.kode_tracing
															FROM public.beone_gudang_detail d INNER JOIN public.beone_gudang gd ON d.gudang_id = gd.gudang_id INNER JOIN public.beone_item i ON d.item_id = i.item_id INNER JOIN public.beone_user u ON d.update_by = u.user_id
															WHERE d.kode_tracing <> '' AND d.qty_in <> 0 AND d.nomor_transaksi = '$no_aju' ORDER BY trans_date asc, gudang_detail_id asc");
		return $sql->result_array();
	}


}
?>
