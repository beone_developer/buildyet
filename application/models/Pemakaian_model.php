<?php
//Model_data.php
defined('BASEPATH') OR exit('No direct script access allowed');

Class Pemakaian_model extends CI_Model
{
	public function simpan($post)
	{
		$session_id = $this->session->userdata('user_id');
		$tgl = $this->db->escape($post['tanggal']);
		$pm = $this->db->escape($post['nomor_pm']);
		$keterangan = $this->db->escape($post['keterangan']);
		$pemakaian_no = $post['nomor_pemakaian'];
		$keterangan = $this->db->escape($post['keterangan']);
		$jenis_pemakaian = $this->db->escape($post['jenis_pemakaian']);
		$update_date = date('Y-m-d');

		$tgl_bulan = substr($tgl, 1, 2);
		$tgl_hari = substr($tgl, 4, 2);
		$tgl_tahun = substr($tgl, 7, 4);

		$tanggal = $tgl_tahun . "-" . $tgl_bulan . "-" . $tgl_hari;
		$tgl_awal_tahun = $tgl_tahun . "-01-" . "01";

		$sql_header = $this->db->query("INSERT INTO public.beone_pemakaian_bahan_header(
																		pemakaian_header_id, nomor_pemakaian, keterangan, tgl_pemakaian, update_by, update_date, nomor_pm, status)
																		VALUES (DEFAULT, '$pemakaian_no', $keterangan, '$tanggal', '$session_id', '$update_date', $pm, 1)");

		$header_id = $this->db->query("SELECT * FROM public.beone_pemakaian_bahan_header ORDER BY pemakaian_header_id DESC LIMIT 1");
		$hasil_header_id = $header_id->row_array();
		$hid = $hasil_header_id['pemakaian_header_id'];

		helper_log($tipe = "add", $str = "Pemakaian " . $pemakaian_no);

		$amount_unit_price = 0;
		$up = 0;

		$amount_pemakaian = 0;

		foreach ($_POST['rows'] as $key => $count) {
			$item_id = $_POST['item_id_' . $count];
			$qty_ = $_POST['qty_' . $count];

			$qty_ex = str_replace(".", "", $qty_);
			$qty = str_replace(",", ".", $qty_ex);


			//SALDO AWAL ITEM
			$sai = $this->db->query("SELECT * FROM public.beone_item WHERE item_id =" . intval($item_id));
			$saldo_awal_item = $sai->row_array();

			//cek saldo awal
			$inv = $this->db->query("SELECT * FROM public.beone_inventory WHERE flag = 1 AND item_id = $item_id ORDER BY trans_date DESC, intvent_trans_id DESC LIMIT 1");
			$hasil_inv = $inv->row_array();


			if ($hasil_inv['qty_in'] == NULL) {
				$sa_akhir_qty = $saldo_awal_item['saldo_qty'];
				$sa_akhir_amount = $saldo_awal_item['saldo_idr'];
				$unit_price_akhir = $sa_akhir_amount / $sa_akhir_qty;
			} else {
				$sa_akhir_qty = $qty + $hasil_inv['sa_qty'];
				$sa_akhir_amount = $hasil_inv['sa_amount'];
				$unit_price_akhir = $hasil_inv['sa_unit_price'];
			}

			$sql_detail = $this->db->query("INSERT INTO public.beone_pemakaian_bahan_detail(
																			pemakaian_detail_id, pemakaian_header_id, item_id, qty, unit_price)
																			VALUES (DEFAULT, $hid, $item_id, $qty, $unit_price_akhir)");

			$amount_pemakaian = $amount_pemakaian + ($qty * $unit_price_akhir);

			//cek saldo awal
			$inv = $this->db->query("SELECT * FROM public.beone_inventory WHERE flag = 1 AND item_id = $item_id ORDER BY trans_date DESC, intvent_trans_id DESC LIMIT 1");
			$hasil_inv = $inv->row_array();
			//end cek saldo awal

			//SALDO AWAL ITEM
			$sai = $this->db->query("SELECT * FROM public.beone_item WHERE item_id =" . intval($item_id));
			$saldo_awal_item = $sai->row_array();

			if ($hasil_inv['qty_in'] == NULL) {
				if ($saldo_awal_item['saldo_idr'] == 0 OR $saldo_awal_item['saldo_qty'] == 0) {
					$unit_price_awal = 0; // salah, harusnya amount saat ini dibagi qty saat ini
				} else {
					$unit_price_awal = $saldo_awal_item['saldo_idr'] / $saldo_awal_item['saldo_qty'];
				}

				$sa_akhir_qty = $saldo_awal_item['saldo_qty'] - $qty;
				$sa_akhir_amount = $saldo_awal_item['saldo_idr'] - ($qty * $unit_price_awal);
				$unit_price_akhir = $sa_akhir_amount / $sa_akhir_qty;

				$sql_inventory = $this->db->query("INSERT INTO public.beone_inventory(intvent_trans_id, intvent_trans_no, item_id, trans_date, keterangan, qty_in, value_in, qty_out, value_out, sa_qty, sa_unit_price, sa_amount, flag, update_by, update_date) VALUES (DEFAULT, '$pemakaian_no', $item_id, '$tanggal', 'PEMAKAIAN', 0, 0, $qty, $unit_price_awal, $sa_akhir_qty, $unit_price_akhir, $sa_akhir_amount, 1, $session_id, '$update_date')");
			} else {
				$sa_qty = $hasil_inv['sa_qty'];
				$sa_akhir_qty = $sa_qty - $qty;

				$sa_amount = $hasil_inv['sa_amount'];
				$sa_akhir_amount = $hasil_inv['sa_amount'] - ($qty * $hasil_inv['sa_unit_price']);

				$unit_price_awal = $hasil_inv['sa_unit_price'];
				if ($sa_akhir_amount == 0 OR $sa_akhir_qty == 0) {
					$unit_price_akhir = 0;
					$sa_akhir_amount = 0;
				} else {
					$unit_price_akhir = $sa_akhir_amount / $sa_akhir_qty;
				}

				$sql_inventory = $this->db->query("INSERT INTO public.beone_inventory(intvent_trans_id, intvent_trans_no, item_id, trans_date, keterangan, qty_in, value_in, qty_out, value_out, sa_qty, sa_unit_price, sa_amount, flag, update_by, update_date) VALUES (DEFAULT, '$pemakaian_no', $item_id, '$tanggal', 'PEMAKAIAN', 0, 0, $qty, $unit_price_awal, $sa_akhir_qty, $unit_price_akhir, $sa_akhir_amount, 1, $session_id, '$update_date')");
			}

			if ($post['pemakaian_bahan_baku_wip'] == "wip") {
				$sql_gudang_detail = $this->db->query("INSERT INTO public.beone_gudang_detail(
																						gudang_detail_id, gudang_id, trans_date, item_id, qty_in, qty_out, nomor_transaksi, update_by, update_date, flag)
																						VALUES (DEFAULT, 3, '$tanggal', $item_id, 0, $qty, '$pemakaian_no', $session_id, '$update_date', 1)");
			} else {
				$sql_gudang_detail = $this->db->query("INSERT INTO public.beone_gudang_detail(
																						gudang_detail_id, gudang_id, trans_date, item_id, qty_in, qty_out, nomor_transaksi, update_by, update_date, flag)
																						VALUES (DEFAULT, 1, '$tanggal', $item_id, 0, $qty, '$pemakaian_no', $session_id, '$update_date', 1)");
			}

		}

		$sql_update_amount = $this->db->query("UPDATE public.beone_pemakaian_bahan_header
																				SET amount_pemakaian=$amount_pemakaian
																				WHERE pemakaian_header_id = " . intval($hid));

		$id_akun = 0;
		$no_akun = "";
		$id_lawan_akun = 0;
		$no_lawan_akun = "";

		/*if ($post['jenis_pemakaian'] == '1'){//pemakaian bahan baku upper
				$id_akun = 63;
				$no_akun = '510.02.01';
				$id_lawan_akun = 12;
				$no_lawan_akun = '116.01.01';
		}else if ($post['jenis_pemakaian'] == '2'){//pemakaian bahan baku bottom
				$id_akun = 78;
				$no_akun = '520.02.01';
				$id_lawan_akun = 12;
				$no_lawan_akun = '116.01.01';
		}else if ($post['jenis_pemakaian'] == '3'){//pemakaian bahan baku peralatan part upper
			$id_akun = 64;
			$no_akun = '510.02.03';
			$id_lawan_akun = 12;
			$no_lawan_akun = '116.01.01';
		}else if ($post['jenis_pemakaian'] == '4'){//pemakaian bahan baku peralatan part bottom
			$id_akun = 79;
			$no_akun = '520.02.03';
			$id_lawan_akun = 12;
			$no_lawan_akun = '116.01.01';
		}*/

		if ($post['pemakaian_bahan_baku_wip'] == "bb") {
			$id_akun = 19;
			$no_akun = '116.08.01'; //BARANG DALAM PROSES
			$id_lawan_akun = 12;
			$no_lawan_akun = '116.01.01'; //PERSEDIAAN BAHAN BAKU
		} else {
			$id_akun = 19;
			$no_akun = '116.08.01'; //BARANG DALAM PROSES
			$id_lawan_akun = 17;
			$no_lawan_akun = '116.05.01'; //PERSEDIAAN WIP
		}

		$sql_ledger_debet = $this->db->query("INSERT INTO public.beone_gl(
																							gl_id, gl_date, coa_id, coa_no, coa_id_lawan, coa_no_lawan, keterangan, debet, kredit, pasangan_no, gl_number, update_by, update_date)
																							VALUES (DEFAULT, '$tanggal', $id_akun, '$no_akun', $id_lawan_akun, '$no_lawan_akun', $keterangan, $amount_pemakaian, 0, '$pemakaian_no', '$pemakaian_no', 1, '$update_date')");

		$sql_ledger_kredit = $this->db->query("INSERT INTO public.beone_gl(
																			gl_id, gl_date, coa_id, coa_no, coa_id_lawan, coa_no_lawan, keterangan, debet, kredit, pasangan_no, gl_number, update_by, update_date)
																			VALUES (DEFAULT, '$tanggal', $id_lawan_akun, '$no_lawan_akun', $id_akun, '$no_akun', $keterangan, 0, $amount_pemakaian, '$pemakaian_no', '$pemakaian_no', 1, '$update_date')");

		if ($sql_header)
			return true;
		return false;
	}


	public function delete($pm_id)
	{
		$sql = $this->db->query("DELETE FROM public.beone_pm_header WHERE pm_header_id = " . intval($pm_id));
		if ($sql)
			return true;
		return false;
	}

	public function load_header_pm()
	{
		$sql = $this->db->query("SELECT pm_header_id, no_pm FROM public.beone_pm_header");
		return $sql->result_array();
	}

	public function load_pemakaian()
	{
		$sql = $this->db->query("SELECT * FROM public.beone_pemakaian_bahan_header");
		return $sql->result_array();
	}
}

?>
