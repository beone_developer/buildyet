<?php
//Model_data.php
defined('BASEPATH') OR exit('No direct script access allowed');

Class Produksi_model extends CI_Model
{
	public function simpan($post)
	{
		$session_id = $this->session->userdata('user_id');
		$tgl = $this->db->escape($post['tanggal']);
		$pm = $this->db->escape($post['nomor_pm']);
		$item = $this->db->escape($post['item_modal']);
		$qty_ = $post['qty_modal'];
		$keterangan = $this->db->escape($post['keterangan']);
		$nomor_produksi = $post['nomor_produksi'];
		$jenis_produksi = $post['jenis_produksi'];
		$update_date = date('Y-m-d');

		$qty_ex = str_replace(".", "", $qty_);
		$qty = str_replace(",", ".", $qty_ex);

		$tgl_bulan = substr($tgl, 1, 2);
		$tgl_hari = substr($tgl, 4, 2);
		$tgl_tahun = substr($tgl, 7, 4);

		$tanggal = $tgl_tahun . "-" . $tgl_bulan . "-" . $tgl_hari;
		$tgl_awal_tahun = $tgl_tahun . "-01-" . "01";

		$sql_header = $this->db->query("INSERT INTO public.beone_produksi_header(
																		produksi_header_id, nomor_produksi, tgl_produksi, tipe_produksi, item_produksi, qty_hasil_produksi, keterangan, flag, updated_by, udpated_date,pm_header_id)
																		VALUES (DEFAULT, '$nomor_produksi', '$tanggal', 1, $item, $qty, $keterangan, 1, $session_id, '$update_date',$pm)");

		$header_id = $this->db->query("SELECT * FROM public.beone_produksi_header ORDER BY produksi_header_id DESC LIMIT 1");
		$hasil_header_id = $header_id->row_array();
		$hid = $hasil_header_id['produksi_header_id'];

		$amount_unit_price = 0;
		$up = 0;

		$amount_pemakaian = 0;

		foreach ($_POST['rows'] as $key => $count) {
			$pemakaian_id = $_POST['pemakaian_id_' . $count];

			$sql_detail = $this->db->query("INSERT INTO public.beone_produksi_detail(
																			produksi_detail_id, produksi_header_id, pemakaian_header_id)
																			VALUES (DEFAULT, $hid, $pemakaian_id)");

			$header_pemakaian = $this->db->query("SELECT pemakaian_header_id, amount_pemakaian
																						FROM public.beone_pemakaian_bahan_header
																						WHERE pemakaian_header_id = " . intval($pemakaian_id));
			$hasil_pemakaian = $header_pemakaian->row_array();
			$total_amount_pemakaian = $hasil_pemakaian['amount_pemakaian'];

			$amount_pemakaian = $amount_pemakaian + $total_amount_pemakaian;

			$update_header_pemakaian = $this->db->query("UPDATE public.beone_pemakaian_bahan_header
																										SET status=0
																										WHERE pemakaian_header_id = " . intval($pemakaian_id));
		}


		//SALDO AWAL ITEM
		$q = "SELECT * FROM public.beone_item WHERE item_id = $item";
		$sai = $this->db->query($q);
		$saldo_awal_item = $sai->row_array();

		//cek saldo awal
		$q = "SELECT * FROM public.beone_inventory WHERE flag = 1 AND item_id = $item ORDER BY trans_date DESC, intvent_trans_id DESC LIMIT 1";
		$inv = $this->db->query($q);
		$hasil_inv = $inv->row_array();

		if (!isset($hasil_inv['qty_in'])) {
			$sa_akhir_qty = $saldo_awal_item['saldo_qty'] + $qty;
			$sa_akhir_amount = ($saldo_awal_item['saldo_idr'] * 1) + ($amount_pemakaian * 1);
			//$unit_price_akhir = $sa_akhir_amount / $sa_akhir_qty;

			if ($saldo_awal_item['saldo_idr'] == 0 OR $saldo_awal_item['saldo_qty'] == 0) {
				$unit_price_akhir = ($amount_pemakaian * 1) / $qty;
				$unit_price_awal = $saldo_awal_item['saldo_idr'] / max($saldo_awal_item['saldo_qty'], 1);
			} else {
				$unit_price_akhir = $sa_akhir_amount / $sa_akhir_qty;
				$unit_price_awal = $sa_akhir_amount / $sa_akhir_qty;
			}
		} else {
			$sa_akhir_qty = $qty + $hasil_inv['sa_qty'];
			$sa_akhir_amount = ($hasil_inv['sa_amount'] * 1) + ($amount_pemakaian * 1);

			$unit_price_awal = $hasil_inv['sa_amount'] / $hasil_inv['sa_qty'];
			$unit_price_akhir = $sa_akhir_amount / $sa_akhir_qty;
		}
//		if ($hasil_inv['qty_in'] == NULL) {
//			$sa_akhir_qty = $saldo_awal_item['saldo_qty'] + $qty;
//			$sa_akhir_amount = ($saldo_awal_item['saldo_idr'] * 1) + ($amount_pemakaian * 1);
//			//$unit_price_akhir = $sa_akhir_amount / $sa_akhir_qty;
//
//			if ($saldo_awal_item['saldo_idr'] == 0 OR $saldo_awal_item['saldo_qty'] == 0) {
//				$unit_price_akhir = ($amount_pemakaian * 1) / $qty;
//				$unit_price_awal = $saldo_awal_item['saldo_idr'] / $saldo_awal_item['saldo_qty'];
//			} else {
//				$unit_price_akhir = $sa_akhir_amount / $sa_akhir_qty;
//				$unit_price_awal = $sa_akhir_amount / $sa_akhir_qty;
//			}
//		} else {
//			$sa_akhir_qty = $qty + $hasil_inv['sa_qty'];
//			$sa_akhir_amount = ($hasil_inv['sa_amount'] * 1) + ($amount_pemakaian * 1);
//
//			$unit_price_awal = $hasil_inv['sa_amount'] / $hasil_inv['sa_qty'];
//			$unit_price_akhir = $sa_akhir_amount / $sa_akhir_qty;
//		}

		$sql_inventory = $this->db->query("INSERT INTO public.beone_inventory(
																	intvent_trans_id, intvent_trans_no, item_id, trans_date, keterangan, qty_in, value_in, qty_out, value_out, sa_qty, sa_unit_price, sa_amount, flag, update_by, update_date)
																	VALUES (DEFAULT, '$nomor_produksi', $item, '$tanggal', $keterangan, $qty, $unit_price_awal, 0, 0, $sa_akhir_qty, $unit_price_akhir, $sa_akhir_amount, 1, $session_id, '$update_date')");

		if ($jenis_produksi == "wip") {//produksi wip
			$sql_gudang_detail = $this->db->query("INSERT INTO public.beone_gudang_detail(
																							gudang_detail_id, gudang_id, trans_date, item_id, qty_in, qty_out, nomor_transaksi, update_by, update_date, flag, keterangan)
																							VALUES (DEFAULT, 3, '$tanggal', $item, $qty, 0, '$nomor_produksi', $session_id, '$update_date', 1, $keterangan)");

			//$item_wip = $this->db->query("SELECT * FROM public.beone_item WHERE item_id = $item");
			//$hasil_item_wip = $item_wip->row_array();
			//$jenis_wip = substr($hasil_item_wip['item_code'],0,2);

			if ($post['jenis_bahan_produksi'] == 1) {//upper
				$amount_produksi = $this->db->query("SELECT SUM(debet_idr) - SUM(kredit_idr) as hasil
																									FROM public.beone_coa WHERE nomor LIKE '510%'");
				$hasil_amount_produksi = $amount_produksi->row_array();
				$hasil_biaya_produksi = $hasil_amount_produksi['hasil'] + $amount_pemakaian;

				$id_akun = 64; //PEMAKAIAN BAHAN BAKU UPPER
				$no_akun = '510.02.01';
				$id_lawan_akun = 19; //BARANG DALAM PROSES
				$no_lawan_akun = '116.08.01';

				$id_akun2 = 17; //PERSEDIAAN WIP
				$no_akun2 = '116.05.01';
				$id_lawan_akun2 = 77; //HARGA POKOK PRODUKSI UPPER (UC)
				$no_lawan_akun2 = '511.01.01';
			} else { //Bottom
				$amount_produksi = $this->db->query("SELECT SUM(debet_idr) - SUM(kredit_idr) as hasil
																									FROM public.beone_coa WHERE nomor LIKE '520%'");
				$hasil_amount_produksi = $amount_produksi->row_array();
				$hasil_biaya_produksi = $hasil_amount_produksi['hasil'] + $amount_pemakaian;


				$id_akun = 79; //PEMAKAIAN BAHAN BAKU BOTTOM
				$no_akun = '520.02.01';
				$id_lawan_akun = 19; //BARANG DALAM PROSES
				$no_lawan_akun = '116.08.01';

				$id_akun2 = 17; //PERSEDIAAN WIP
				$no_akun2 = '116.05.01';
				$id_lawan_akun2 = 84; //HARGA POKOK PRODUKSI BOTTOM (BC)
				$no_lawan_akun2 = '521.01.01';
			}

			//PEMAKAIAN BAHAN BAKU >< BARANG DALAM PROSES
			$sql_ledger_debet = $this->db->query("INSERT INTO public.beone_gl(
																									gl_id, gl_date, coa_id, coa_no, coa_id_lawan, coa_no_lawan, keterangan, debet, kredit, pasangan_no, gl_number, update_by, update_date)
																									VALUES (DEFAULT, '$tanggal', $id_akun, '$no_akun', $id_lawan_akun, '$no_lawan_akun', $keterangan, $amount_pemakaian, 0, '$nomor_produksi', '$nomor_produksi', 1, '$update_date')");

			$sql_ledger_kredit = $this->db->query("INSERT INTO public.beone_gl(
																					gl_id, gl_date, coa_id, coa_no, coa_id_lawan, coa_no_lawan, keterangan, debet, kredit, pasangan_no, gl_number, update_by, update_date)
																					VALUES (DEFAULT, '$tanggal', $id_lawan_akun, '$no_lawan_akun', $id_akun, '$no_akun', $keterangan, 0, $amount_pemakaian, '$nomor_produksi', '$nomor_produksi', 1, '$update_date')");


			//PERSEDIAN WIP >< HARGA POKOK PRODUKSI
			$sql_ledger_debet2 = $this->db->query("INSERT INTO public.beone_gl(
																									gl_id, gl_date, coa_id, coa_no, coa_id_lawan, coa_no_lawan, keterangan, debet, kredit, pasangan_no, gl_number, update_by, update_date)
																									VALUES (DEFAULT, '$tanggal', $id_akun2, '$no_akun2', $id_lawan_akun2, '$no_lawan_akun2', $keterangan, $hasil_biaya_produksi, 0, '$nomor_produksi', '$nomor_produksi', 1, '$update_date')");

			$sql_ledger_kredit2 = $this->db->query("INSERT INTO public.beone_gl(
																					gl_id, gl_date, coa_id, coa_no, coa_id_lawan, coa_no_lawan, keterangan, debet, kredit, pasangan_no, gl_number, update_by, update_date)
																					VALUES (DEFAULT, '$tanggal', $id_lawan_akun2, '$no_lawan_akun2', $id_akun2, '$no_akun2', $keterangan, 0, $hasil_biaya_produksi, '$nomor_produksi', '$nomor_produksi', 1, '$update_date')");

		} else {
			$sql_gudang_detail = $this->db->query("INSERT INTO public.beone_gudang_detail(
																							gudang_detail_id, gudang_id, trans_date, item_id, qty_in, qty_out, nomor_transaksi, update_by, update_date, flag, keterangan)
																							VALUES (DEFAULT, 2, '$tanggal', $item, $qty, 0, '$nomor_produksi', $session_id, '$update_date', 1, $keterangan)");


			$biaya = $this->db->query("SELECT SUM(debet_idr) - SUM(kredit_idr) as hasil
																								FROM public.beone_coa WHERE nomor LIKE '530%'");
			$hasil_biaya = $biaya->row_array();
			$total_biaya_produksi = $hasil_biaya['hasil'] + $amount_pemakaian;


			$id_akun = 18; //PERSEDIAAN BARANG JADI
			$no_akun = '116.06.01';
			$id_lawan_akun = 17; //PERSEDIAAN WIP
			$no_lawan_akun = '116.05.01';

			$id_akun2 = 18; //PERSEDIAAN BARANG JADI
			$no_akun2 = '116.06.01';
			$id_lawan_akun2 = 92; //HARGA POKOK PRODUKSI BOTTOM (BC)
			$no_lawan_akun2 = '531.01.01';

			/*
			PERSEDIAAN BARANG JADI >< PERSEDIAAN WIP
			PERSEDIAAN BARANG JADI >< HP PRODUKSI BARANG JADI

			$id_akun2 = 17; //PERSEDIAAN WIP
			$no_akun2 = '116.05.01';
			$id_lawan_akun2 = 84; //HARGA POKOK PRODUKSI BOTTOM (BC)
			$no_lawan_akun2 = '521.01.01';
			*/

			//$gl_persediaan_bj = ($amount_pemakaian*1) + ($total_biaya_produksi*1);

			//PERSEDIAAN BARANG JADI >< PERSEDIAAN WIP
			$sql_ledger_debet = $this->db->query("INSERT INTO public.beone_gl(
																										gl_id, gl_date, coa_id, coa_no, coa_id_lawan, coa_no_lawan, keterangan, debet, kredit, pasangan_no, gl_number, update_by, update_date)
																										VALUES (DEFAULT, '$tanggal', $id_akun, '$no_akun', $id_lawan_akun, '$no_lawan_akun', $keterangan, $amount_pemakaian, 0, '$nomor_produksi', '$nomor_produksi', 1, '$update_date')");

			$sql_ledger_kredit = $this->db->query("INSERT INTO public.beone_gl(
																						gl_id, gl_date, coa_id, coa_no, coa_id_lawan, coa_no_lawan, keterangan, debet, kredit, pasangan_no, gl_number, update_by, update_date)
																						VALUES (DEFAULT, '$tanggal', $id_lawan_akun, '$no_lawan_akun', $id_akun, '$no_akun', $keterangan, 0, $amount_pemakaian, '$nomor_produksi', '$nomor_produksi', 1, '$update_date')");


			//PERSEDIAAN BARANG JADI >< HARGA POKOK PRODUKSI
			$sql_ledger_debet2 = $this->db->query("INSERT INTO public.beone_gl(
																										gl_id, gl_date, coa_id, coa_no, coa_id_lawan, coa_no_lawan, keterangan, debet, kredit, pasangan_no, gl_number, update_by, update_date)
																										VALUES (DEFAULT, '$tanggal', $id_akun2, '$no_akun2', $id_lawan_akun2, '$no_lawan_akun2', $keterangan, $total_biaya_produksi, 0, '$nomor_produksi', '$nomor_produksi', 1, '$update_date')");

			$sql_ledger_kredit2 = $this->db->query("INSERT INTO public.beone_gl(
																						gl_id, gl_date, coa_id, coa_no, coa_id_lawan, coa_no_lawan, keterangan, debet, kredit, pasangan_no, gl_number, update_by, update_date)
																						VALUES (DEFAULT, '$tanggal', $id_lawan_akun2, '$no_lawan_akun2', $id_akun2, '$no_akun2', $keterangan, 0, $total_biaya_produksi, '$nomor_produksi', '$nomor_produksi', 1, '$update_date')");

		}


		if ($sql_header)
			return true;
		return false;
	}


}

?>
