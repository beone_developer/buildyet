<?php
//Model_data.php
defined('BASEPATH') OR exit('No direct script access allowed');

Class Gj_model extends CI_Model{

	public function simpan($post){
		$session_id = $this->session->userdata('user_id');
		$tanggal_awal = $this->db->escape($post['tanggal_journal']);
		$tgl_bulan = substr($tanggal_awal, 1, 2);
		$tgl_hari = substr($tanggal_awal, 4, 2);
		$tgl_tahun = substr($tanggal_awal, 7, 4);

		$tanggal = $tgl_tahun."-".$tgl_bulan."-".$tgl_hari;
		$no_journal = $this->db->escape($post['nomor_journal']);
		$coa_id = $this->db->escape($post['coa_id']);
		$coa_lawan = $this->db->escape($post['coa_id_lawan']);
		$keterangan = $this->db->escape($post['keterangan_journal']);

		$idr_ = $this->db->escape($post['jumlah_idr']);
		$idr_ex = str_replace(".", "", $idr_);
		$idr = str_replace(",", ".", $idr_ex);
		//$idr = round($idr_round, 2);

		$update_date = date('Y-m-d');

		$sql_coa_no = $this->db->query("SELECT * FROM public.beone_coa WHERE coa_id = $coa_id");
		$hasil_coa_no = $sql_coa_no->row_array();
		$coa_no = $hasil_coa_no['nomor'];

		$sql_coa_lawan_no = $this->db->query("SELECT * FROM public.beone_coa WHERE coa_id = $coa_lawan");
		$hasil_coa_lawan_no = $sql_coa_lawan_no->row_array();
		$coa_no_lawan = $hasil_coa_lawan_no['nomor'];

		//insert akun
		$sql = $this->db->query("INSERT INTO public.beone_gl VALUES (DEFAULT, '$tanggal', $coa_id, '$coa_no', $coa_lawan, '$coa_no_lawan', $keterangan, $idr ,0, 'BGJ', $no_journal ,$session_id, '$update_date')");

		//insert lawan akun
		$sql_lawan = $this->db->query("INSERT INTO public.beone_gl VALUES (DEFAULT, '$tanggal', $coa_lawan, '$coa_no_lawan', $coa_id, '$coa_no', $keterangan, 0 ,$idr, 'BGJ', $no_journal ,$session_id, '$update_date')");

		if($sql && $sql_lawan)
			return true;
		return false;
	}


	public function update($post, $no_journal){
		$session_id = $this->session->userdata('user_id');
		$tanggal_awal = $this->db->escape($post['tanggal_journal']);
		$tgl_bulan = substr($tanggal_awal, 1, 2);
		$tgl_hari = substr($tanggal_awal, 4, 2);
		$tgl_tahun = substr($tanggal_awal, 7, 4);

		$tanggal = $tgl_tahun."-".$tgl_bulan."-".$tgl_hari;
		$coa_id = $this->db->escape($post['coa_id']);
		$coa_lawan = $this->db->escape($post['coa_id_lawan']);
		$keterangan = $this->db->escape($post['keterangan_journal']);

		$idr_ = $this->db->escape($post['jumlah_idr']);
		$idr_ex = str_replace(".", "", $idr_);
		$idr = str_replace(",", ".", $idr_ex);

		$update_date = date('Y-m-d');

		$sql_coa_no = $this->db->query("SELECT * FROM public.beone_coa WHERE coa_id = $coa_id");
		$hasil_coa_no = $sql_coa_no->row_array();
		$coa_no = $hasil_coa_no['nomor'];

		$sql_coa_lawan_no = $this->db->query("SELECT * FROM public.beone_coa WHERE coa_id = $coa_lawan");
		$hasil_coa_lawan_no = $sql_coa_lawan_no->row_array();
		$coa_no_lawan = $hasil_coa_lawan_no['nomor'];

		$sql_dk = $this->db->query("SELECT * FROM public.beone_gl WHERE gl_number = '$no_journal'");

		foreach($sql_dk->result_array() as $row){
			$id = $row['gl_id'];
			if ($row['debet'] == 0){ //posisi kredit
						$sql_k = $this->db->query("UPDATE public.beone_gl SET coa_id=$coa_id, coa_no='$coa_no', coa_id_lawan=$coa_lawan, coa_no_lawan='$coa_no_lawan', keterangan=$keterangan, kredit=$idr, update_by=$session_id, update_date='$update_date' WHERE gl_id=$id");
			}else{
				//posisi debet
						$sql_d = $this->db->query("UPDATE public.beone_gl SET coa_id=$coa_lawan, coa_no='$coa_no_lawan', coa_id_lawan=$coa_id, coa_no_lawan='$coa_no', keterangan=$keterangan, debet=$idr, update_by=$session_id, update_date='$update_date' WHERE gl_id=$id");
			}
		}

			return true;
	}


	public function load_akun_general_journal(){
		$sql = $this->db->query("SELECT DISTINCT g.coa_id, g.coa_no, c.nama FROM public.beone_gl g INNER JOIN public.beone_coa c ON g.coa_id = c.coa_id ORDER BY g.coa_no");
		return $sql->result_array();
	}



	public function load_gj(){
		$sql = $this->db->query("SELECT * FROM public.beone_gl WHERE pasangan_no LIKE 'BGJ%'");
		return $sql->result_array();
	}

	public function delete($gj_id, $gj_number){
		$sql_gl = $this->db->query("DELETE FROM public.beone_gl WHERE gl_number = '$gj_number'");
	}

	public function get_default($gj_id){
		$sql = $this->db->query("SELECT * FROM public.beone_gl WHERE gl_id = ".intval($gj_id));
		if($sql->num_rows() > 0)
			return $sql->row_array();
		return false;
	}


}
?>
