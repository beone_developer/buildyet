<?php
//Model_data.php
defined('BASEPATH') OR exit('No direct script access allowed');

Class COA_model extends CI_Model{


	public function get_default($coa_id){
		$sql = $this->db->query("SELECT * FROM public.beone_coa WHERE coa_id = ".intval($coa_id));
		if($sql->num_rows() > 0)
			return $sql->row_array();
		return false;
	}

	public function load_tipe_COA(){
		$sql = $this->db->query("SELECT * FROM public.beone_tipe_coa");
		return $sql->result_array();
	}

	public function load_COA(){
		$sql = $this->db->query("SELECT * FROM public.beone_coa ORDER BY coa_id ASC");
		return $sql->result_array();
	}

	//COA Penjualan untuk laporan laba rugi
	public function load_COA_penjualan(){
		$sql = $this->db->query("SELECT * FROM public.beone_coa WHERE tipe_akun = 4");
		return $sql->result_array();
	}


	//COA Biaya Admin & Umum untuk laporan laba rugi
	public function load_COA_biaya_admin_umum(){
		$sql = $this->db->query("SELECT * FROM public.beone_coa WHERE tipe_akun = 6");
		return $sql->result_array();
	}

	//COA Pendapatan & Biaya Lain untuk laporan laba rugi
	public function load_COA_pendapatan_lain(){
		$sql = $this->db->query("SELECT * FROM public.beone_coa WHERE tipe_akun = 7");
		return $sql->result_array();
	}

	public function load_COA_kas_bank(){
		$sql = $this->db->query("SELECT * FROM public.beone_coa WHERE tipe_transaksi = 1");
		return $sql->result_array();
	}

	public function load_COA_persediaan(){
		$sql = $this->db->query("SELECT * FROM public.beone_coa WHERE tipe_transaksi = 6");
		return $sql->result_array();
	}

	public function load_COA_aset(){
		$sql = $this->db->query("SELECT * FROM public.beone_coa WHERE tipe_transaksi =  7");
		return $sql->result_array();
	}

	public function load_COA_hutang(){
		$sql = $this->db->query("SELECT * FROM public.beone_coa WHERE tipe_transaksi = 8");
		return $sql->result_array();
	}

	public function load_COA_hutang_pajak(){
		$sql = $this->db->query("SELECT * FROM public.beone_coa WHERE tipe_transaksi = 9");
		return $sql->result_array();
	}

	public function load_COA_modal(){
		$sql = $this->db->query("SELECT * FROM public.beone_coa WHERE tipe_transaksi = 3");
		return $sql->result_array();
	}

	public function load_COA_hutang_lainnya(){
		$sql = $this->db->query("SELECT * FROM public.beone_coa WHERE tipe_transaksi = 15");
		return $sql->result_array();
	}

	public function load_COA_biaya_setor(){
		$sql = $this->db->query("SELECT * FROM public.beone_coa WHERE tipe_transaksi = 10");
		return $sql->result_array();
	}

	public function load_COA_penyusutan(){
		$sql = $this->db->query("SELECT * FROM public.beone_coa WHERE tipe_transaksi = 2 ");
		return $sql->result_array();
	}

	public function load_COA_piutang(){
		$sql = $this->db->query("SELECT * FROM public.beone_coa WHERE tipe_transaksi = 11");
		return $sql->result_array();
	}

	public function load_COA_uang_muka(){
		$sql = $this->db->query("SELECT * FROM public.beone_coa WHERE tipe_transaksi = 12");
		return $sql->result_array();
	}

	public function load_COA_uang_muka_pajak(){
		$sql = $this->db->query("SELECT * FROM public.beone_coa WHERE tipe_transaksi = 13");
		return $sql->result_array();
	}

	public function load_COA_uang_muka_penjualan(){
		$sql = $this->db->query("SELECT * FROM public.beone_coa WHERE tipe_transaksi = 14");
		return $sql->result_array();
	}

	public function simpan($post){
		$tipe = $this->db->escape($post['coa_id_cash_bank']);
		$nomor = $this->db->escape($post['nomor_akun']);
		$nm = $this->db->escape($post['nama_akun']);
		$nama = strtoupper($nm); //konversi huruf besar
		$valas = $this->db->escape($post['valas']);
		$saldo = $this->db->escape($post['saldo']);
		//$debetkredit = $this->db->escape($post['debetkredit']);
		$debetkredit = $_POST['debetkredit'];

		if ($debetkredit == 1){
				$debet_valas = $valas;
				$debet_idr = $saldo;
				$kredit_valas = 0;
				$kredit_idr = 0;
		}else{
			$debet_valas = 0;
			$debet_idr = 0;
			$kredit_valas = $valas;
			$kredit_idr = $saldo;
		}

		//insert table voucher
		$sql = $this->db->query("INSERT INTO public.beone_coa VALUES (DEFAULT, $nama, $nomor, $tipe, $debet_valas, $debet_idr, $kredit_valas, $kredit_idr)");

		if($sql)
			return true;
		return false;
	}


	  public function update_coa($post, $coa_id){
		$tipe = $this->db->escape($post['coa_id_cash_bank']);
		$nomor = $this->db->escape($post['nomor_akun']);
		$nama = $this->db->escape($post['nama_akun']);
		$valas = $this->db->escape($post['valas']);
		$saldo = $this->db->escape($post['saldo']);
		$debetkredit = $this->db->escape($post['debetkredit']);

		if ($debetkredit == 1){
				$debet_valas = $valas;
				$debet_idr = $saldo;
				$kredit_valas = 0;
				$kredit_idr = 0;
		}else{
			$debet_valas = 0;
			$debet_idr = 0;
			$kredit_valas = intval($valas);
			$kredit_idr = intval($saldo);
		}

		$sql = $this->db->query("UPDATE public.beone_coa SET nomor = $nomor, nama = $nama, debet_valas = $debet_valas, debet_idr = $debet_idr, kredit_valas = $kredit_valas, kredit_idr = $kredit_idr WHERE coa_id = ".intval($coa_id));
		return true;

	}


	public function delete($coa_id){
		$sql = $this->db->query("DELETE FROM public.beone_coa WHERE coa_id = ".intval($coa_id));
	}

	public function load_coa_jurnal(){
		$sql = $this->db->query("SELECT j.coa_jurnal_id, j.nama_coa_jurnal, j.keterangan_coa_jurnal, j.coa_id, c.nama as ncoa, j.coa_no
														FROM public.beone_coa_jurnal j INNER JOIN public.beone_coa c ON j.coa_id = c.coa_id");
		return $sql->result_array();
	}

	public function get_default_coa_jurnal($coa_jurnal_id){
		$sql = $this->db->query("SELECT j.coa_jurnal_id, j.nama_coa_jurnal, j.keterangan_coa_jurnal, j.coa_id, c.nama as ncoa, j.coa_no
														FROM public.beone_coa_jurnal j INNER JOIN public.beone_coa c ON j.coa_id = c.coa_id WHERE j.coa_jurnal_id = ".intval($coa_jurnal_id));
		if($sql->num_rows() > 0)
			return $sql->row_array();
		return false;
	}

	public function update_coa_jurnal($post, $coa_jurnal_id){
		$nama_transaksi = $this->db->escape($post['nama_transaksi']);
		$keterangan = $this->db->escape($post['keterangan']);
		$coa_id = $this->db->escape($post['coa']);

		$sql_coa_no = $this->db->query("SELECT * FROM public.beone_coa WHERE coa_id = ".intval($post['coa']));
		$hasil_coa_no = $sql_coa_no->row_array();
		$coa_no = $hasil_coa_no['nomor'];

		$sql = $this->db->query("UPDATE public.beone_coa_jurnal SET nama_coa_jurnal=$nama_transaksi, keterangan_coa_jurnal=$keterangan, coa_id=$coa_id, coa_no='$coa_no'  WHERE coa_jurnal_id = ".intval($coa_jurnal_id));

		if($sql)
			return true;
		return false;
	}


	/****************************************************************************/
	// Fungsi untuk melakukan proses upload file
	public function upload_file($filename){
		$this->load->library('upload'); // Load librari upload

		$config['upload_path'] = './excel/';
		$config['allowed_types'] = 'xlsx';
		$config['max_size']	= '2048';
		$config['overwrite'] = true;
		$config['file_name'] = $filename;

		$this->upload->initialize($config); // Load konfigurasi uploadnya
		if($this->upload->do_upload('file')){ // Lakukan upload dan Cek jika proses upload berhasil
			// Jika berhasil :
			$return = array('result' => 'success', 'file' => $this->upload->data(), 'error' => '');
			return $return;
		}else{
			// Jika gagal :
			$return = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());
			return $return;
		}
	}

	// Buat sebuah fungsi untuk melakukan insert lebih dari 1 data
	/*public function insert_multiple($data){
		$this->db->insert_batch('beone_coa', $data);
	}*/

	public function insert_multiple($data){
		//$this->db->insert_batch('beone_coa', $data);
		foreach($data as $row){
			$sql = $this->db->query("INSERT INTO public.beone_coa VALUES ($row[coa_id], '$row[nama]', '$row[nomor]', $row[tipe_akun], $row[debet_valas], $row[debet_idr], $row[kredit_valas], $row[kredit_idr], '$row[dk]', $row[tipe_transaksi])");
		}}
	/****************************************************************************/

}
?>
