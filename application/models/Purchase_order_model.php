<?php
//Model_data.php
defined('BASEPATH') OR exit('No direct script access allowed');

Class Purchase_order_model extends CI_Model
{
	public function simpan($post)
	{
		$session_id = $this->session->userdata('user_id');
		$tanggal_awal = $this->db->escape($post['tanggal']);
		$supplier = $this->db->escape($post['supplier']);
		$keterangan = $this->db->escape($post['keterangan_header']);
		$purch_no = $this->generate_po_number();
		$ppn = 0; //$this->db->escape($post['ppn']);
		$ppn_value_ = 0;//$this->db->escape($post['ppn_value']);
		$subtotal_ = $this->db->escape($post['subtotal']);
		$subtotal2_ = $this->db->escape($post['subtotal2']);
//		$grandtotal_ = $this->db->escape($post['grandtotal']);
		$grandtotal_ = $post['subtotal'];// $this->db->escape($post['grandtotal']);
		$grandtotal2_ = $post['subtotal2'];
		$update_date = date('Y-m-d');

		// $ppn_value_ex = str_replace(".", "", $ppn_value_);
		// $ppn_value = str_replace(",", ".", $ppn_value_ex);

		$subtotal_ex = str_replace(".", "", $subtotal_);
		$subtotal = str_replace(",", ".", $subtotal_ex);

		$grandtotal_ex = str_replace(".", "", $grandtotal_);
		$grandtotal = str_replace(",", ".", $grandtotal_ex);

		$subtotal_ex2 = str_replace(".", "", $subtotal2_);
		$subtotal2 = str_replace(",", ".", $subtotal_ex2);

		$grandtotal_ex2 = str_replace(".", "", $grandtotal2_);
		$grandtotal2 = str_replace(",", ".", $grandtotal_ex2);

		if ($grandtotal <= 0) {
			$grandtotal = $grandtotal2;
		}

		$grandtotal = $this->db->escape($grandtotal);
		$grandtotal2 = $this->db->escape($grandtotal2);

		$tgl_bulan = substr($tanggal_awal, 1, 2);
		$tgl_hari = substr($tanggal_awal, 4, 2);
		$tgl_tahun = substr($tanggal_awal, 7, 4);

		$tanggal = $tgl_tahun . "-" . $tgl_bulan . "-" . $tgl_hari;
		$tanggal_awal_tahun = $tgl_tahun . "-01-01";
		$this->cb->begin_trans();
		$sql_header = $this->cb->query("INSERT INTO public.beone_po_import_header(purchase_header_id, purchase_no, trans_date, supplier_id, keterangan, update_by, update_date, flag, grandtotal, total_hitung)
																		VALUES (DEFAULT, '$purch_no', '$tanggal', $supplier, $keterangan, $session_id, '$tanggal', '1', $grandtotal, $grandtotal2)");
		$header_id = $this->db->query("SELECT * FROM public.beone_po_import_header ORDER BY purchase_header_id DESC LIMIT 1");
		$hasil_header_id = $header_id->row_array();
		$hid = $hasil_header_id['purchase_header_id'];

		foreach ($_POST['rows'] as $key => $count) {
			$item_id = $_POST['item_id_' . $count];
			$qty_ = $_POST['qty_' . $count];
			// $satuan = $_POST['satuan_id_' . $count];
			$price_ = $_POST['price_' . $count];
			$amount_ = $_POST['amount_' . $count];

			$qty_ex = str_replace(".", "", $qty_);
			$price_ex = str_replace(".", "", $price_);
			$amount_ex = str_replace(".", "", $amount_);

			$qty = str_replace(",", ".", $qty_ex);
			$price = str_replace(",", ".", $price_ex);
			$amount = str_replace(",", ".", $amount_ex);

			$sql_detail = $this->cb->query("INSERT INTO public.beone_po_import_detail(purchase_header_id, item_id, qty, price, amount, flag)	VALUES ($hid, $item_id, $qty, $price, $amount, '1')");
		}

		$result = $this->cb->commit_trans();
		$result['message'] = 'Nota : ' . $purch_no . ' berhasil dibuat.';
		return $result;
	}


	public function update($post, $purchase_id)
	{
		$session_id = $this->session->userdata('user_id');
		$tanggal_awal = $this->db->escape($post['tanggal']);

		$supplier = $this->db->escape($post['supplier']);
		$keterangan = $this->db->escape($post['keterangan_header']);
		$purch_no = $this->db->escape($post['nomor_po']);
		$ppn = 0;//$this->db->escape($post['ppn']);
		$ppn_value_ = 0;//$this->db->escape($post['ppn_value']);
		$subtotal_ = $this->db->escape($post['subtotal']);
		$subtotal2_ = $this->db->escape($post['subtotal2']);

		$grandtotal_ = $post['subtotal'];// $this->db->escape($post['grandtotal']);
		$grandtotal2_ = $post['subtotal2'];

		$update_date = date('Y-m-d');

		$subtotal_ex = str_replace(".", "", $subtotal_);
		$subtotal = str_replace(",", ".", $subtotal_ex);

		$grandtotal_ex = str_replace(".", "", $grandtotal_);
		$grandtotal = str_replace(",", ".", $grandtotal_ex);

		$subtotal_ex2 = str_replace(".", "", $subtotal2_);
		$subtotal2 = str_replace(",", ".", $subtotal_ex2);

		$grandtotal_ex2 = str_replace(".", "", $grandtotal2_);
		$grandtotal2 = str_replace(",", ".", $grandtotal_ex2);

		if ($grandtotal <= 0) {
			$grandtotal = $grandtotal2;
		}

		$grandtotal = $this->db->escape($grandtotal);
		$grandtotal2 = $this->db->escape($grandtotal2);

		$tgl_bulan = substr($tanggal_awal, 1, 2);
		$tgl_hari = substr($tanggal_awal, 4, 2);
		$tgl_tahun = substr($tanggal_awal, 7, 4);

		$tanggal = $tgl_tahun . "-" . $tgl_bulan . "-" . $tgl_hari;
		$tanggal_awal_tahun = $tgl_tahun . "-01-01";

		$q = "UPDATE public.beone_po_import_header SET trans_date='$tanggal', supplier_id=$supplier, update_by=$session_id, update_date='$update_date', grandtotal=$grandtotal, total_hitung=$grandtotal2, keterangan=$keterangan WHERE purchase_header_id = " . intval($purchase_id);

		$sql_header = $this->db->query($q);

		helper_log($tipe = "edit", $str = "Ubah Pembelian, No " . $post['nomor_po']);

		$sql_detail_del = $this->db->query("DELETE FROM public.beone_po_import_detail WHERE purchase_header_id =" . intval($purchase_id));

		foreach ($_POST['rows'] as $key => $count) {
			$item_id = $_POST['item_id_' . $count];
			$qty_ = $_POST['qty_' . $count];
			// $satuan = $_POST['satuan_id_' . $count];
			$price_ = $_POST['price_' . $count];
			$amount_ = $_POST['amount_' . $count];


			$qty_ex = str_replace(".", "", $qty_);
			$price_ex = str_replace(".", "", $price_);
			$amount_ex = str_replace(".", "", $amount_);

			$qty = str_replace(",", ".", $qty_ex);
			$price = str_replace(",", ".", $price_ex);
			$amount = str_replace(",", ".", $amount_ex);

			// $qty = explode(".",$qty1)[0];
			// $price = explode(".",$price1)[0];
			// $amount = explode(".",$amount1)[0];

			$sql_detail = $this->db->query("INSERT INTO public.beone_po_import_detail(purchase_detail_id, purchase_header_id, item_id, qty, price, amount, flag)	VALUES (DEFAULT, $purchase_id, $item_id, $qty, $price, $amount, '1')");


		}

		if ($sql_header && $sql_detail)
			return true;
		return false;
	}


	public function load_purchase_header()
	{
		$sql = $this->db->query("SELECT h.purchase_header_id, h.purchase_no, h.trans_date, h.keterangan, h.supplier_id, h.flag, c.nama as nsupplier
															FROM public.beone_po_import_header h INNER JOIN public.beone_custsup c ON h.supplier_id = c.custsup_id WHERE h.flag = 1");
		return $sql->result_array();
	}

	public function get_default_header($purchase_id)
	{
		$sql = $this->db->query("SELECT h.purchase_header_id, h.purchase_no, h.trans_date, h.keterangan, h.supplier_id, h.grandtotal as grandtotal, c.nama as nsupplier
															FROM public.beone_po_import_header h INNER JOIN public.beone_custsup c ON h.supplier_id = c.custsup_id WHERE h.flag = 1 AND h.purchase_header_id = " . intval($purchase_id));
		if ($sql->num_rows() > 0)
			return $sql->row_array();
		return false;
	}

	public function get_default_detail($purchase_id)
	{
		$sql = $this->db->query("SELECT d.purchase_detail_id, d.item_id, d.qty, d.price, d.amount
										FROM public.beone_po_import_header h 
										INNER JOIN public.beone_po_import_detail d ON h.purchase_header_id = d.purchase_header_id
										WHERE h.flag = 1 AND d.purchase_header_id = " . intval($purchase_id));
		return $sql->result_array();
	}

	public function realisasi($purchase_id)
	{
		$sql = $this->db->query("UPDATE beone_po_import_header SET flag = 0 WHERE purchase_header_id =" . intval($purchase_id));
	}

	public function delete($purchase_id, $purchase_no)
	{
		$sql = $this->db->query("DELETE FROM public.beone_po_import_header WHERE purchase_header_id = " . intval($purchase_id));
		$sql_detail = $this->db->query("DELETE FROM public.beone_po_import_detail WHERE purchase_header_id = " . intval($purchase_id));

		$purch_no = str_replace("-", "/", $purchase_no);

		helper_log($tipe = "delete", $str = "Hapus Pembelian, No " . $purchase_no);

		$sql_gl = $this->db->query("DELETE FROM public.beone_gl WHERE gl_number = '$purch_no'");
		$sql_hp = $this->db->query("DELETE FROM public.beone_hutang_piutang WHERE nomor = '$purch_no'");
		$sql_del_gudang = $this->db->query("DELETE FROM public.beone_gudang_detail WHERE nomor_transaksi = '$purch_no'");
		$sql_inventory = $this->db->query("DELETE FROM public.beone_inventory WHERE intvent_trans_no = '$purch_no'");
	}

	public function generate_po_number()
	{
		/******************* GENERATE PO NUMBER ******************************/
		$tgl = date('m/d/Y');
		$thn = substr($tgl, 8, 2);
		$bln = substr($tgl, 0, 2);

		//LOOKUP KODE
		$lookup_kode = $this->db->query("SELECT * FROM public.beone_po_import_header ORDER BY purchase_header_id DESC LIMIT 1");
		$hasil_lookup_kode = $lookup_kode->row_array();

		//PENENTUAN BENTUK KODE
		$kode_awal = "PL/" . $thn . "/" . $bln . "/";


		$urutan = substr($hasil_lookup_kode['purchase_no'], 10, 4);
		$no_lanjutan = $urutan + 1;

		$digit = strlen($no_lanjutan);
		$jml_nol = 4 - $digit;

		$cetak_nol = "";

		for ($i = 1; $i <= $jml_nol; $i++) {
			$cetak_nol = $cetak_nol . "0";
		}
		$nomorpo = $kode_awal . $cetak_nol . $no_lanjutan;
		return $nomorpo;
		// echo "<h3><b>".$nomorpo."</b></h3>";
		/*************************************************/
	}

	public function get_item($q)
	{
		$like = "'%" . $q . "%'";
		$query = "SELECT i.item_id as id, i.item_code as code, i.nama as text FROM beone_item i WHERE i.nama ilike $like or i.item_code ilike $like";
		return $this->cb->api_result($query, $q);
	}
}

?>
