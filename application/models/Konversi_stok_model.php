<?php
defined('BASEPATH') OR exit('No direct script access allowed');

Class Konversi_stok_model extends CI_Model
{
	public function load_konversi()
	{
		$sql = $this->db->query("SELECT ks.konversi_stok_header_id,i.nama as item_name,ks.konversi_stok_no,ks.konversi_stok_date,ks.qty,s.satuan_code as satuan 
								FROM public.beone_konversi_stok_header ks
								INNER JOIN  public.beone_item i on ks.item_id = i.item_id
								INNER JOIN  public.beone_satuan_item s on ks.satuan_qty = s.satuan_id
								WHERE ks.flag = 1");
		return $sql->result_array();
	}

	public function simpan($post)
	{

		$item_id = $this->db->escape($post['item_id']);
		$konversi_stok_no = $this->db->escape($post['nomorks']);
		$tanggal_awal = $this->db->escape($post['konversi_stok_date']);
		$qty = $this->db->escape($post['qty']);
		$qty_real = $this->db->escape($post['qty_real']);
		$satuan = $this->db->escape($post['satuan_qty']);
		$gudang = $this->db->escape($post['gudang']);

		$tgl_bulan = substr($tanggal_awal, 1, 2);
		$tgl_hari = substr($tanggal_awal, 4, 2);
		$tgl_tahun = substr($tanggal_awal, 7, 4);

		$tanggal = $tgl_tahun . "-" . $tgl_bulan . "-" . $tgl_hari;
		$tanggal_awal_tahun = $tgl_tahun . "-01-01";

		$this->cb->begin_trans();

		$sql_header = $this->cb->query("INSERT INTO public.beone_konversi_stok_header(konversi_stok_header_id,item_id,konversi_stok_no,konversi_stok_date,flag,qty,qty_real,satuan_qty,gudang_id)
										VALUES (DEFAULT, $item_id, $konversi_stok_no,'$tanggal','1',$qty,$qty_real,$satuan,$gudang)");

		$header_id = $this->db->query("SELECT konversi_stok_header_id FROM public.beone_konversi_stok_header ORDER BY konversi_stok_header_id DESC LIMIT 1");
		$hasil_header_id = $header_id->row_array();
		$hid = $hasil_header_id['konversi_stok_header_id'];
//		$hid = $sql_header['sql'];
		// $qty = $this->db->escape($post['base_qty_']);
		// $allocation_qty = $this->db->escape($post['allocation_qty_']);
		// $real_qty = $this->db->escape($post['qty_real_']);
		foreach ($_POST['rows'] as $key => $count) {
			$item_id = $_POST['item_id_' . $count];
			$qty_ = $_POST['base_qty_' . $count];
			$qty_real_ = $_POST['qty_real_' . $count];
			$qty_allocation_ = $_POST['allocation_qty_' . $count];
			$satuan_qty_ = $_POST['satuan_qty_' . $count];
			$gudang_id_ = $_POST['gudang_id_' . $count];

			$qty_ex = str_replace(".", "", $qty_);
			$qty_real_ex = str_replace(".", "", $qty_real_);
			$qty_allocation_ex = str_replace(".", "", $qty_allocation_);

			$qty = str_replace(",", ".", $qty_ex);
			$qty_allocation = str_replace(",", ".", $qty_allocation_ex);

			$sql_detail = $this->cb->query("INSERT INTO public.beone_konversi_stok_detail(konversi_stok_header_id, item_id, qty, qty_real, qty_allocation,gudang_id,satuan_qty) 
				VALUES ($hid, $item_id, $qty, $qty_real,$qty_allocation,$gudang,'1')");

		}

		$result = $this->cb->commit_trans();
		return $result;
	}

	public function update($post, $konversi_stok_header_id)
	{
		$item_id = $this->db->escape($post['item_id']);
		$konversi_stok_no = $this->db->escape($post['nomorks']);
		$tanggal_awal = $this->db->escape($post['konversi_stok_date']);
		$qty = $this->db->escape($post['qty']);
		$satuan = $this->db->escape($post['satuan_qty']);

		$tgl_bulan = substr($tanggal_awal, 1, 2);
		$tgl_hari = substr($tanggal_awal, 4, 2);
		$tgl_tahun = substr($tanggal_awal, 7, 4);

		$tanggal = $tgl_tahun . "-" . $tgl_bulan . "-" . $tgl_hari;
		$tanggal_awal_tahun = $tgl_tahun . "-01-01";

		$sql_header = $this->db->query("UPDATE public.beone_konversi_stok_header set qty=$qty,satuan_qty=$satuan where konversi_stok_header_id=" . intval($konversi_stok_header_id));

		foreach ($_POST['rows'] as $key => $count) {
			$item_id = $_POST['item_id_' . $count];
			$qty_ = $_POST['qty_' . $count];
			$qty_real_ = $_POST['qty_real_' . $count];

			$qty_ex = str_replace(".", "", $qty_);
			$qty_real_ex = str_replace(".", "", $qty_real_);

			$qty = str_replace(",", ".", $qty_ex);
			$qty_real = str_replace(",", ".", $qty_real_ex);

			$sql_detail_del = $this->db->query("DELETE FROM public.beone_konversi_stok_detail WHERE konversi_stok_header_id =" . intval($konversi_stok_header_id));

			$sql_detail = $this->db->query("INSERT INTO public.beone_konversi_stok_detail(konversi_stok_header_id, item_id, qty, qty_real,satuan_qty)
											VALUES ($konversi_stok_header_id, $item_id, $qty, $qty_real,$satuan)");

		}

	}

	public function delete($konversi_stok_header_id)
	{
		$sql = $this->db->query("UPDATE public.beone_konversi_stok_header set flag = 0 WHERE konversi_stok_header_id = " . intval($konversi_stok_header_id));
		// helper_log($tipe = "delete", $str = "Hapus Pembelian, No ".$purchase_no);
	}

	public function get_default_header($konversi_stok_header_id)
	{
		$sql = $this->db->query("SELECT ks.konversi_stok_header_id,ks.item_id, i.nama as item_name,ks.konversi_stok_no,ks.konversi_stok_date,ks.qty,ks.satuan_qty,s.satuan_code as satuan 
								FROM public.beone_konversi_stok_header ks
								INNER JOIN  public.beone_item i on ks.item_id = i.item_id
								INNER JOIN  public.beone_satuan_item s on ks.satuan_qty = s.satuan_id
								WHERE ks.flag = 1 AND ks.konversi_stok_header_id=" . intval($konversi_stok_header_id));
		if ($sql->num_rows() > 0)
			return $sql->row_array();
		return false;
	}

	public function get_default_detail($konversi_stok_header_id)
	{
		$sql = $this->db->query("SELECT d.konversi_stok_detail_id,d.item_id,i.nama as item_name,d.qty,d.qty_real,d.satuan_qty,s.satuan_code as satuan
								FROM public.beone_konversi_stok_detail d 
								INNER JOIN public.beone_konversi_stok_header h ON h.konversi_stok_header_id = d.konversi_stok_header_id
								INNER JOIN  public.beone_item i on d.item_id = i.item_id 
								INNER JOIN  public.beone_satuan_item s on d.satuan_qty = s.satuan_id
								WHERE h.flag = 1 AND d.konversi_stok_header_id = " . intval($konversi_stok_header_id));
		return $sql->result_array();
	}
}

?>
