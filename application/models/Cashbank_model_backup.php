<?php
//Model_data.php
defined('BASEPATH') OR exit('No direct script access allowed');

Class Cashbank_model extends CI_Model{

	public function simpan($post){
		$tanggal_awal = $this->db->escape($post['tanggal_voucher']);
		$tgl_bulan = substr($tanggal_awal, 1, 2);
		$tgl_hari = substr($tanggal_awal, 4, 2);
		$tgl_tahun = substr($tanggal_awal, 7, 4);

		$tanggal = $tgl_tahun."-".$tgl_bulan."-".$tgl_hari;
		$tipe = $this->db->escape($post['tipe_voucher']);
		$voucher = $this->db->escape($post['nomor_voucher']);
		$coa_cb = $this->db->escape($post['coa_id_cash_bank']);
		$coa_lawan = $this->db->escape($post['coa_id_lawan']);
		$keterangan = $this->db->escape($post['keterangan_voucher']);

		$valas_ = $this->db->escape($post['jumlah_valas']);
		$valas = str_replace(".", "", $valas_);

		$kurs_ = $this->db->escape($post['kurs']);
		$kurs = str_replace(".", "", $kurs_);

		$idr_ = $this->db->escape($post['jumlah_idr']);
		$idr = str_replace(".", "", $idr_);

		$update_date = date('Y/m/d');

		$sql_coa_no = $this->db->query("SELECT * FROM public.beone_coa WHERE coa_id = $coa_cb");
		$hasil_coa_no = $sql_coa_no->row_array();
		$coa_no = $hasil_coa_no['nomor'];

		$sql_coa_lawan_no = $this->db->query("SELECT * FROM public.beone_coa WHERE coa_id = $coa_lawan");
		$hasil_coa_lawan_no = $sql_coa_lawan_no->row_array();
		$coa_no_lawan = $hasil_coa_lawan_no['nomor'];

		//insert table voucher
		$sql = $this->db->query("INSERT INTO public.beone_voucher VALUES (DEFAULT, $voucher, '$tanggal', $tipe, $keterangan, $coa_cb, '$coa_no', $coa_lawan, '$coa_no_lawan', $valas, $kurs, $idr, 1, '$update_date')");


		//******************** GENERATE NO JOURNAL ********************

		/*$tgl = date('m/d/Y');
		$thn = substr($tgl,8,2);
		$bln = substr($tgl,0,2);


		$kode_awal = "BGJ"."-".$thn."-".$bln."-";

		$sql_jnumb = $this->db->query("SELECT * FROM public.beone_gl WHERE gl_number LIKE '$kode_awal%' ORDER BY gl_number DESC LIMIT 1");
		$hasil_jnumb = $sql_jnumb->row_array();

		$urutan = substr($hasil_jnumb['gl_number'],10,4);
		$no_lanjutan = $urutan+1;

		$digit = strlen($no_lanjutan);
		$jml_nol = 4-$digit;

		$cetak_nol = "";

		for ($i = 1; $i <= $jml_nol; $i++) {
		    $cetak_nol = $cetak_nol."0";
		}

		$no_journal = $kode_awal.$cetak_nol.$no_lanjutan;*/

		//*******************************************************

		//menambahkn nomor voucher untuk keterangan apabila journal dari voucher
		$ket_voucher = $post['keterangan_voucher']." (".$post['nomor_voucher'].")";

		if ($post['tipe_voucher'] == 1){
			//insert akun ke general ledger (buku besar)
			$sql = $this->db->query("INSERT INTO public.beone_gl VALUES (DEFAULT, '$tanggal', $coa_cb, '$coa_no', $coa_lawan, '$coa_no_lawan', '$ket_voucher', $idr ,0, $voucher, $voucher ,1, '$update_date')");

			//insert lawan akun ke general ledger (buku besar)
			$sql = $this->db->query("INSERT INTO public.beone_gl VALUES (DEFAULT, '$tanggal', $coa_lawan, '$coa_no_lawan', $coa_cb, '$coa_no', '$ket_voucher', 0 ,$idr, $voucher, $voucher ,1, '$update_date')");
		}else{
			//insert akun ke general ledger (buku besar)
			$sql = $this->db->query("INSERT INTO public.beone_gl VALUES (DEFAULT, '$tanggal', $coa_cb, '$coa_no', $coa_lawan, '$coa_no_lawan', '$ket_voucher', 0 ,$idr, $voucher, $voucher ,1, '$update_date')");

			//insert lawan akun ke general ledger (buku besar)
			$sql = $this->db->query("INSERT INTO public.beone_gl VALUES (DEFAULT, '$tanggal', $coa_lawan, '$coa_no_lawan', $coa_cb, '$coa_no', '$ket_voucher', $idr ,0, $voucher, $voucher ,1, '$update_date')");
		}


		if($sql)
			return true;
		return false;
	}

	public function update($post, $voucher_id){
		$coa_lawan = $this->db->escape($post['coa_id_lawan']);
		$keterangan = $this->db->escape($post['keterangan_voucher']);
		$valas_ = $this->db->escape($post['jumlah_valas']);
		$kurs_ = $this->db->escape($post['kurs']);
		$idr_ = $this->db->escape($post['jumlah_idr']);

		$valas = str_replace(".", "", $valas_);
		$kurs = str_replace(".", "", $kurs_);
		$idr = str_replace(".", "", $idr_);

		$sql_coa_lawan_no = $this->db->query("SELECT * FROM public.beone_coa WHERE coa_id = $coa_lawan");
		$hasil_coa_lawan_no = $sql_coa_lawan_no->row_array();
		$coa_no_lawan = $hasil_coa_lawan_no['nomor'];

		$update_date = date('Y-m-d');

		$sql = $this->db->query("UPDATE public.beone_voucher SET keterangan = $keterangan, coa_id_lawan = $coa_lawan, coa_no_lawan = '$coa_no_lawan', jumlah_valas = $valas, kurs = $kurs, jumlah_idr = $idr, update_date = '$update_date' WHERE voucher_id = ".intval($voucher_id));

		$no_voucher = $this->db->query("SELECT * FROM public.beone_voucher WHERE voucher_id =".intval($voucher_id));
		$hasil_no_voucher = $no_voucher->row_array();
		$nov = $hasil_no_voucher['voucher_number'];

		$sql_gl_no = $this->db->query("SELECT * FROM public.beone_gl WHERE pasangan_no = '$nov'");
		//$hasil_gl_no = $sql_gl_no->row_array();



		//Mencari posisi akun lawan
		foreach($sql_gl_no->result_array() as $row){
			$id = $row['gl_id'];
			$kt = $post['keterangan_voucher']." (".$row['pasangan_no'].")";
			if ($row['debet'] == 0){ //posisi kredit
					$sql_k = $this->db->query("UPDATE public.beone_gl SET coa_id_lawan=$coa_lawan, coa_no_lawan='$coa_no_lawan', keterangan='$kt', kredit=$idr, update_by=1, update_date='$update_date' WHERE gl_id=$id");
			}else{//posisi debet
					$sql_k = $this->db->query("UPDATE public.beone_gl SET coa_id_lawan=$coa_lawan, coa_no_lawan='$coa_no_lawan', keterangan='$kt', debet=$idr, update_by=1, update_date='$update_date' WHERE gl_id=$id");
			}
		}

		return true;

	}


	public function simpan_kode_cashbank($post){
		$nama = $this->db->escape($post['nama_item']);
		$kode = $this->db->escape($post['kode']);
		$akun = $this->db->escape($post['akun']);
		$inout = $this->db->escape($post['inout']);

		//insert table voucher
		$sql = $this->db->query("INSERT INTO public.beone_kode_trans(kode_trans_id, nama, coa_id, kode_bank, in_out, flag) VALUES (DEFAULT, $nama, $akun, $kode, $inout, 1)");

		if($sql)
			return true;
		return false;
	}

	public function update_kode_cashbank($post, $kode_trans_id){
		$nama = $this->db->escape($post['nama_item']);
		$kode = $this->db->escape($post['kode']);
		$akun = $this->db->escape($post['akun']);
		$inout = $this->db->escape($post['inout']);

		$sql = $this->db->query("UPDATE public.beone_kode_trans	SET nama=$nama, coa_id=$akun, kode_bank=$kode, in_out=$inout WHERE kode_trans_id = ".intval($kode_trans_id));

		if($sql)
			return true;
		return false;
	}

	public function delete_kode_cashbank($kode_trans_id){
		$sql = $this->db->query("DELETE FROM public.beone_kode_trans WHERE kode_trans_id = ".intval($kode_trans_id));
	}

	public function get_default($voucher_id){
		$sql = $this->db->query("SELECT * FROM public.beone_voucher WHERE voucher_id = ".intval($voucher_id));
		if($sql->num_rows() > 0)
			return $sql->row_array();
		return false;
	}

	public function load_voucher(){
		$sql = $this->db->query("SELECT * FROM public.beone_voucher");
		return $sql->result_array();
	}

	public function delete($voucher_id, $voucher_number){
		$sql = $this->db->query("DELETE FROM public.beone_voucher WHERE voucher_id = ".intval($voucher_id));

		$sql_gl = $this->db->query("DELETE FROM public.beone_gl WHERE pasangan_no = '$voucher_number'");
	}


	public function load_kode_cashbank(){
		$sql = $this->db->query("SELECT k.kode_trans_id, k.nama, k.coa_id, k.kode_bank, k.in_out, k.flag, c.nama as ncoa FROM public.beone_kode_trans k INNER JOIN public.beone_coa c ON k.coa_id = c.coa_id WHERE k.flag=1");
		return $sql->result_array();
	}

	public function get_default_kode_cashbank($kode_trans_id){
		$sql = $this->db->query("SELECT k.kode_trans_id, k.nama, k.coa_id, k.kode_bank, k.in_out, k.flag, c.nama as ncoa FROM public.beone_kode_trans k INNER JOIN public.beone_coa c ON k.coa_id = c.coa_id WHERE k.kode_trans_id = ".intval($kode_trans_id));
		if($sql->num_rows() > 0)
			return $sql->row_array();
		return false;
	}

}
?>
