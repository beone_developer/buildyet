<?php
//Model_data.php
defined('BASEPATH') OR exit('No direct script access allowed');

Class Perusahaan_model extends CI_Model{

	public function simpan($post){
		$nama = $this->db->escape($post['nama_perusahaan']);
		$alamat = $this->db->escape($post['alamat_perusahaan']);
		$kota = $this->db->escape($post['kota']);
		$provinsi = $this->db->escape($post['provinsi']);
		$kode_pos = $this->db->escape($post['kode_pos']);

		$sql = $this->db->query("UPDATE public.beone_konfigurasi_perusahaan
														SET nama_perusahaan=$nama, alamat_perusahaan=$alamat, kota_perusahaan=$kota, provinsi_perusahaan=$provinsi, kode_pos_perusahaan=$kode_pos");

		if($sql)
			return true;
		return false;
	}

	public function load_data_perusahaan(){
		$sql = $this->db->query("SELECT * FROM public.beone_konfigurasi_perusahaan");
		if($sql->num_rows() > 0)
			return $sql->row_array();
		return false;
	}

}
?>
