<?php
//Model_data.php
defined('BASEPATH') OR exit('No direct script access allowed');

Class Assets_model extends CI_Model{

	public function load_assets_bahan_baku(){
		$sql = $this->db->query("SELECT i.item_id, i.nama, i.item_code, i.saldo_qty, i.saldo_idr, i.Keterangan, i.hscode, i.flag, i.item_type_id, t.nama as ntipe, s.satuan_code as nsatuan
															FROM public.beone_item i INNER JOIN public.beone_item_type t ON i.item_type_id = t.item_type_id INNER JOIN public.beone_satuan_item s ON s.satuan_id = i.satuan_id
															WHERE i.flag=1 AND i.item_type_id = 1 ORDER BY i.item_id ASC");
		return $sql->result_array();
	}

	public function load_assets_wip(){
		$sql = $this->db->query("SELECT i.item_id, i.nama, i.item_code, i.saldo_qty, i.saldo_idr, i.Keterangan, i.hscode, i.flag, i.item_type_id, t.nama as ntipe, s.satuan_code as nsatuan
															FROM public.beone_item i INNER JOIN public.beone_item_type t ON i.item_type_id = t.item_type_id INNER JOIN public.beone_satuan_item s ON s.satuan_id = i.satuan_id
															WHERE i.flag=1 AND i.item_type_id = 2 ORDER BY i.item_id ASC");
		return $sql->result_array();
	}

	public function load_assets_barang_jadi(){
		$sql = $this->db->query("SELECT i.item_id, i.nama, i.item_code, i.saldo_qty, i.saldo_idr, i.Keterangan, i.hscode, i.flag, i.item_type_id, t.nama as ntipe, s.satuan_code as nsatuan
															FROM public.beone_item i INNER JOIN public.beone_item_type t ON i.item_type_id = t.item_type_id INNER JOIN public.beone_satuan_item s ON s.satuan_id = i.satuan_id
															WHERE i.flag=1 AND i.item_type_id = 3 ORDER BY i.item_id ASC");
		return $sql->result_array();
	}

	public function load_assets_bahan_penolong(){
		$sql = $this->db->query("SELECT i.item_id, i.nama, i.item_code, i.saldo_qty, i.saldo_idr, i.Keterangan, i.hscode, i.flag, i.item_type_id, t.nama as ntipe, s.satuan_code as nsatuan
															FROM public.beone_item i INNER JOIN public.beone_item_type t ON i.item_type_id = t.item_type_id INNER JOIN public.beone_satuan_item s ON s.satuan_id = i.satuan_id
															WHERE i.flag=1 AND i.item_type_id = 11 ORDER BY i.item_id ASC");
		return $sql->result_array();
	}

	public function load_assets_mesin_sparepart(){
		$sql = $this->db->query("SELECT i.item_id, i.nama, i.item_code, i.saldo_qty, i.saldo_idr, i.Keterangan, i.hscode, i.flag, i.item_type_id, t.nama as ntipe, s.satuan_code as nsatuan
															FROM public.beone_item i INNER JOIN public.beone_item_type t ON i.item_type_id = t.item_type_id INNER JOIN public.beone_satuan_item s ON s.satuan_id = i.satuan_id
															WHERE i.flag=1 AND i.item_type_id = 12 ORDER BY i.item_id ASC");
		return $sql->result_array();
	}


	public function load_assets_peralatan_pabrik(){
		$sql = $this->db->query("SELECT i.item_id, i.nama, i.item_code, i.saldo_qty, i.saldo_idr, i.Keterangan, i.hscode, i.flag, i.item_type_id, t.nama as ntipe, s.satuan_code as nsatuan
															FROM public.beone_item i INNER JOIN public.beone_item_type t ON i.item_type_id = t.item_type_id INNER JOIN public.beone_satuan_item s ON s.satuan_id = i.satuan_id
															WHERE i.flag=1 AND i.item_type_id = 4 ORDER BY i.item_id ASC");
		return $sql->result_array();
	}

}
?>
