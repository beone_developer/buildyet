<?php
//Model_data.php
defined('BASEPATH') OR exit('No direct script access allowed');

Class Purchase_model extends CI_Model{


	public function simpan($post){
		$session_id = $this->session->userdata('user_id');
		$tanggal_awal = $this->db->escape($post['tanggal']);
		$supplier = $this->db->escape($post['supplier']);
		$keterangan = $this->db->escape($post['keterangan_header']);
		$purch_no = $this->db->escape($post['nomor_po']);
		$ppn = $this->db->escape($post['ppn']);
		$ppn_value_ = $this->db->escape($post['ppn_value']);
		$subtotal_ = $this->db->escape($post['subtotal']);
		$grandtotal_ = $this->db->escape($post['grandtotal']);
		$update_date = date('Y-m-d');

		$ppn_value_ex = str_replace(".", "", $ppn_value_);
		$ppn_value = str_replace(",", ".", $ppn_value_ex);

		$subtotal_ex = str_replace(".", "", $subtotal_);
		$subtotal = str_replace(",", ".", $subtotal_ex);

		$grandtotal_ex = str_replace(".", "", $grandtotal_);
		$grandtotal = str_replace(",", ".", $grandtotal_ex);

		$tgl_bulan = substr($tanggal_awal, 1, 2);
		$tgl_hari = substr($tanggal_awal, 4, 2);
		$tgl_tahun = substr($tanggal_awal, 7, 4);

		$tanggal = $tgl_tahun."-".$tgl_bulan."-".$tgl_hari;
		$tanggal_awal_tahun = $tgl_tahun."-01-01";

		$sql_header = $this->db->query("INSERT INTO public.beone_purchase_header(purchase_header_id, purchase_no, trans_date, supplier_id, keterangan, update_by, update_date, flag, ppn, subtotal, ppn_value, grandtotal)
																		VALUES (DEFAULT, $purch_no, '$tanggal', $supplier, $keterangan, $session_id, '$tanggal', 1, $ppn, $subtotal, $ppn_value, $grandtotal)");

		helper_log($tipe = "add", $str = "Pembelian, No ".$post['nomor_po']);

		$header_id = $this->db->query("SELECT * FROM public.beone_purchase_header ORDER BY purchase_header_id DESC LIMIT 1");
		$hasil_header_id = $header_id->row_array();
		$hid = $hasil_header_id['purchase_header_id'];

		foreach ($_POST['rows'] as $key => $count ){
							 $item_id = $_POST['item_id_'.$count];
							 $qty_ = $_POST['qty_'.$count];
							 $price_ = $_POST['price_'.$count];
							 $amount_ = $_POST['amount_'.$count];

					 		 $qty_ex = str_replace(".", "", $qty_);
							 $price_ex = str_replace(".", "", $price_);
							 $amount_ex = str_replace(".", "", $amount_);

							 $qty = str_replace(",", ".", $qty_ex);
							 $price = str_replace(",", ".", $price_ex);
							 $amount = str_replace(",", ".", $amount_ex);

							 $sql_detail = $this->db->query("INSERT INTO public.beone_purchase_detail(purchase_header_id, item_id, qty, price, amount, flag)	VALUES ($hid, $item_id, $qty, $price, $amount, 1)");


							 $saldo_awal_inventory = $this->db->query("SELECT * FROM public.beone_inventory WHERE item_id = $item_id AND trans_date BETWEEN '$tanggal_awal_tahun' AND '$tanggal' ORDER BY trans_date DESC, intvent_trans_id DESC LIMIT 1");
		 		 			 $sa_inventory = $saldo_awal_inventory->row_array();
		 		 			 $sa_qty = $sa_inventory['sa_qty'];
							 $sa_amount = $sa_inventory['sa_amount'];

							 $sa_akhir_qty_round = $sa_qty + $qty;
							 $sa_akhir_qty = round($sa_akhir_qty_round, 2);

							 $sa_akhir_amount_round = $sa_amount + $amount;
							 $sa_akhir_amount = round($sa_akhir_amount_round, 2);

							 $unit_price_round = $sa_akhir_amount / $sa_akhir_qty;
							 $unit_price = round ($unit_price_round, 2);

							 if ($post['ppn'] == 2){
								 	$price_include_tax = $price / 1.1;
								 	$sql_inventory = $this->db->query("INSERT INTO public.beone_inventory(intvent_trans_id, intvent_trans_no, item_id, trans_date, keterangan, qty_in, value_in, qty_out, value_out, sa_qty, sa_unit_price, sa_amount, flag, update_by, update_date) VALUES (DEFAULT, $purch_no, $item_id, '$tanggal', $keterangan, $qty, $price_include_tax, 0, 0, $sa_akhir_qty, $unit_price, $sa_akhir_amount, 1, $session_id, '$update_date')");
							 }else{
								 	$sql_inventory = $this->db->query("INSERT INTO public.beone_inventory(intvent_trans_id, intvent_trans_no, item_id, trans_date, keterangan, qty_in, value_in, qty_out, value_out, sa_qty, sa_unit_price, sa_amount, flag, update_by, update_date) VALUES (DEFAULT, $purch_no, $item_id, '$tanggal', $keterangan, $qty, $price, 0, 0, $sa_akhir_qty, $unit_price, $sa_akhir_amount, 1, $session_id, '$update_date')");
							 }

							 //insert mutasi gudang detail
							 $sql_gudang_detail = $this->db->query("INSERT INTO public.beone_gudang_detail(
																										 gudang_detail_id, gudang_id, trans_date, item_id, qty_in, qty_out, nomor_transaksi, update_by, update_date, flag, keterangan)
																										 VALUES (DEFAULT, 4, '$tanggal', $item_id, $qty, 0, $purch_no, $session_id, '$update_date', 1, 'PURCHASE')");

					 }

					$coa_jurnal_persediaan = $this->db->query("SELECT * FROM public.beone_coa_jurnal WHERE coa_jurnal_id = 1"); //coa jurnal persediaan
		 			$coa_persediaan = $coa_jurnal_persediaan->row_array();
		 			$cj_id = $coa_persediaan['coa_id'];
		 			$cj_no = $coa_persediaan['coa_no'];

		 			$coa_jurnal_ppn = $this->db->query("SELECT * FROM public.beone_coa_jurnal WHERE coa_jurnal_id = 2"); //coa jurnal ppn
		 			$coa_ppn = $coa_jurnal_ppn->row_array();
		 			$cj_id_ppn = $coa_ppn['coa_id'];
		 			$cj_no_ppn = $coa_ppn['coa_no'];

		 			$coa_jurnal_hutang_usaha = $this->db->query("SELECT * FROM public.beone_coa_jurnal WHERE coa_jurnal_id = 3"); //coa jurnal hutang usaha
		 			$coa_hutang_usaha = $coa_jurnal_hutang_usaha->row_array();
		 			$cj_id_hutang = $coa_hutang_usaha['coa_id'];
		 			$cj_no_hutang = $coa_hutang_usaha['coa_no'];


								$sql_ledger_debet = $this->db->query("INSERT INTO public.beone_gl(
																								 gl_id, gl_date, coa_id, coa_no, coa_id_lawan, coa_no_lawan, keterangan, debet, kredit, pasangan_no, gl_number, update_by, update_date)
																								 VALUES (DEFAULT, '$tanggal', $cj_id, '$cj_no', $cj_id_hutang, '$cj_no_hutang', $keterangan, $subtotal, 0, $purch_no, $purch_no, $session_id, '$update_date')");

							  $sql_ledger_kredit = $this->db->query("INSERT INTO public.beone_gl(
																								gl_id, gl_date, coa_id, coa_no, coa_id_lawan, coa_no_lawan, keterangan, debet, kredit, pasangan_no, gl_number, update_by, update_date)
																								VALUES (DEFAULT, '$tanggal', $cj_id_hutang, '$cj_no_hutang', $cj_id, '$cj_no', $keterangan, 0, $subtotal, $purch_no, $purch_no, $session_id, '$update_date')");


							 $sql_ledger_debet2 = $this->db->query("INSERT INTO public.beone_gl(
																							 gl_id, gl_date, coa_id, coa_no, coa_id_lawan, coa_no_lawan, keterangan, debet, kredit, pasangan_no, gl_number, update_by, update_date)
																							 VALUES (DEFAULT, '$tanggal', $cj_id_ppn, '$cj_no_ppn', $cj_id_hutang, '$cj_no_hutang', $keterangan, $ppn_value, 0, $purch_no, $purch_no, $session_id, '$update_date')");

							 $sql_ledger_kredit2 = $this->db->query("INSERT INTO public.beone_gl(
 																							gl_id, gl_date, coa_id, coa_no, coa_id_lawan, coa_no_lawan, keterangan, debet, kredit, pasangan_no, gl_number, update_by, update_date)
 																							VALUES (DEFAULT, '$tanggal', $cj_id_hutang, '$cj_no_hutang', $cj_id_ppn, '$cj_no_ppn', $keterangan, 0, $ppn_value, $purch_no, $purch_no, $session_id, '$update_date')");


						$sql_hutang = $this->db->query("INSERT INTO public.beone_hutang_piutang(
																						hutang_piutang_id, custsup_id, trans_date, nomor, keterangan, valas_trans, idr_trans, valas_pelunasan, idr_pelunasan, tipe_trans, update_by, update_date, flag, status_lunas)
																						VALUES (DEFAULT, $supplier, '$tanggal', $purch_no, $keterangan, 0, $grandtotal, 0, 0, 1, $session_id, '$update_date', 1, 0)");

		if($sql_header && $sql_detail)
			return true;
		return false;
	}



	public function update($post, $purchase_id){
		$session_id = $this->session->userdata('user_id');
		$tanggal_awal = $this->db->escape($post['tanggal']);
		$supplier = $this->db->escape($post['supplier']);
		$keterangan = $this->db->escape($post['keterangan_header']);
		$purch_no = $this->db->escape($post['nomor_po']);
		$ppn = $this->db->escape($post['ppn']);
		$ppn_value_ = $this->db->escape($post['ppn_value']);
		$subtotal_ = $this->db->escape($post['subtotal']);
		$grandtotal_ = $this->db->escape($post['grandtotal']);
		$update_date = date('Y-m-d');

		$ppn_value_ex = str_replace(".", "", $ppn_value_);
		$ppn_value = str_replace(",", ".", $ppn_value_ex);

		$subtotal_ex = str_replace(".", "", $subtotal_);
		$subtotal = str_replace(",", ".", $subtotal_ex);

		$grandtotal_ex = str_replace(".", "", $grandtotal_);
		$grandtotal = str_replace(",", ".", $grandtotal_ex);

		$tgl_bulan = substr($tanggal_awal, 1, 2);
		$tgl_hari = substr($tanggal_awal, 4, 2);
		$tgl_tahun = substr($tanggal_awal, 7, 4);

		$tanggal = $tgl_tahun."-".$tgl_bulan."-".$tgl_hari;
		$tanggal_awal_tahun = $tgl_tahun."-01-01";

		$sql_header = $this->db->query("UPDATE public.beone_purchase_header
																		SET update_by=$session_id, update_date='$update_date', ppn=$ppn, subtotal=$subtotal, ppn_value=$ppn_value, grandtotal=$grandtotal
																		WHERE purchase_header_id = ".intval($purchase_id));

		helper_log($tipe = "edit", $str = "Ubah Pembelian, No ".$post['nomor_po']);

		foreach ($_POST['rows'] as $key => $count ){
							 $item_id = $_POST['item_id_'.$count];
							 $qty_ = $_POST['qty_'.$count];
							 $price_ = $_POST['price_'.$count];
							 $amount_ = $_POST['amount_'.$count];

							 $qty = str_replace(",", "", $qty_);
							 $price = str_replace(",", "", $price_);
							 $amount = str_replace(",", "", $amount_);

							 $sql_detail_del = $this->db->query("DELETE FROM public.beone_purchase_detail WHERE purchase_header_id =".intval($purchase_id));

								$sql_detail = $this->db->query("INSERT INTO public.beone_purchase_detail(purchase_detail_id, purchase_header_id, item_id, qty, price, amount, flag)	VALUES (DEFAULT, $purchase_id, $item_id, $qty, $price, $amount, 1)");

							 $sql_del_inventory = $this->db->query("DELETE FROM public.beone_inventory WHERE intvent_trans_no = $purch_no");

							 $sql_del_hp = $this->db->query("DELETE FROM public.beone_hutang_piutang WHERE nomor = $purch_no");

							 $saldo_awal_inventory = $this->db->query("SELECT * FROM public.beone_inventory WHERE item_id = $item_id AND trans_date BETWEEN '$tanggal_awal_tahun' AND '$tanggal' ORDER BY trans_date DESC, intvent_trans_id DESC LIMIT 1");
		 		 			 $sa_inventory = $saldo_awal_inventory->row_array();
		 		 			 $sa_qty = $sa_inventory['sa_qty'];
							 $sa_amount = $sa_inventory['sa_amount'];

							 $sa_akhir_qty = $sa_qty + $qty;
							 //$sa_akhir_qty = round($sa_akhir_qty_round, 2);

							 $sa_akhir_amount = $sa_amount + $amount;
							 //$sa_akhir_amount = round($sa_akhir_amount_round, 2);

							 $unit_price = $sa_akhir_amount / $sa_akhir_qty;
							 //$unit_price = round ($unit_price_round, 2);

							 $sql_inventory = $this->db->query("INSERT INTO public.beone_inventory(intvent_trans_id, intvent_trans_no, item_id, trans_date, keterangan, qty_in, value_in, qty_out, value_out, sa_qty, sa_unit_price, sa_amount, flag, update_by, update_date) VALUES (DEFAULT, $purch_no, $item_id, '$tanggal', $keterangan, $qty, $price, 0, 0, $sa_akhir_qty, $unit_price, $sa_akhir_amount, 1, $session_id, '$update_date')");

							 $sql_del_gudang = $this->db->query("DELETE FROM public.beone_gudang_detail WHERE nomor_transaksi = $purch_no");

							 //insert mutasi gudang detail
 							$sql_gudang_detail = $this->db->query("INSERT INTO public.beone_gudang_detail(
 																										gudang_detail_id, gudang_id, trans_date, item_id, qty_in, qty_out, nomor_transaksi, update_by, update_date, flag, keterangan)
 																										VALUES (DEFAULT, 4, '$tanggal', $item_id, $qty, 0, $purch_no, $session_id, '$update_date', 1, 'PURCHASE')");
					 }

					$coa_jurnal_persediaan = $this->db->query("SELECT * FROM public.beone_coa_jurnal WHERE coa_jurnal_id = 1"); //coa jurnal persediaan
		 			$coa_persediaan = $coa_jurnal_persediaan->row_array();
		 			$cj_id = $coa_persediaan['coa_id'];
		 			$cj_no = $coa_persediaan['coa_no'];

		 			$coa_jurnal_ppn = $this->db->query("SELECT * FROM public.beone_coa_jurnal WHERE coa_jurnal_id = 2"); //coa jurnal ppn
		 			$coa_ppn = $coa_jurnal_ppn->row_array();
		 			$cj_id_ppn = $coa_ppn['coa_id'];
		 			$cj_no_ppn = $coa_ppn['coa_no'];

		 			$coa_jurnal_hutang_usaha = $this->db->query("SELECT * FROM public.beone_coa_jurnal WHERE coa_jurnal_id = 3"); //coa jurnal hutang usaha
		 			$coa_hutang_usaha = $coa_jurnal_hutang_usaha->row_array();
		 			$cj_id_hutang = $coa_hutang_usaha['coa_id'];
		 			$cj_no_hutang = $coa_hutang_usaha['coa_no'];


					$sql_del_ledger = $this->db->query("DELETE FROM public.beone_gl WHERE gl_number = $purch_no");

								$sql_ledger_debet = $this->db->query("INSERT INTO public.beone_gl(
																								 gl_id, gl_date, coa_id, coa_no, coa_id_lawan, coa_no_lawan, keterangan, debet, kredit, pasangan_no, gl_number, update_by, update_date)
																								 VALUES (DEFAULT, '$tanggal', $cj_id, '$cj_no', $cj_id_hutang, '$cj_no_hutang', $keterangan, $ppn_value, 0, $purch_no, $purch_no, $session_id, '$update_date')");

							  $sql_ledger_kredit = $this->db->query("INSERT INTO public.beone_gl(
																								gl_id, gl_date, coa_id, coa_no, coa_id_lawan, coa_no_lawan, keterangan, debet, kredit, pasangan_no, gl_number, update_by, update_date)
																								VALUES (DEFAULT, '$tanggal', $cj_id_hutang, '$cj_no_hutang', $cj_id, '$cj_no', $keterangan, 0, $ppn_value, $purch_no, $purch_no, $session_id, '$update_date')");

							 $sql_ledger_debet2 = $this->db->query("INSERT INTO public.beone_gl(
																							 gl_id, gl_date, coa_id, coa_no, coa_id_lawan, coa_no_lawan, keterangan, debet, kredit, pasangan_no, gl_number, update_by, update_date)
																							 VALUES (DEFAULT, '$tanggal', $cj_id_ppn, '$cj_no_ppn', $cj_id_hutang, '$cj_no_hutang', $keterangan, $subtotal, 0, $purch_no, $purch_no, $session_id, '$update_date')");

							 $sql_ledger_kredit2 = $this->db->query("INSERT INTO public.beone_gl(
 																							gl_id, gl_date, coa_id, coa_no, coa_id_lawan, coa_no_lawan, keterangan, debet, kredit, pasangan_no, gl_number, update_by, update_date)
 																							VALUES (DEFAULT, '$tanggal', $cj_id_hutang, '$cj_no_hutang', $cj_id_ppn, '$cj_no_ppn', $keterangan, 0, $subtotal, $purch_no, $purch_no, $session_id, '$update_date')");


						$sql_hutang = $this->db->query("INSERT INTO public.beone_hutang_piutang(
																						hutang_piutang_id, custsup_id, trans_date, nomor, keterangan, valas_trans, idr_trans, valas_pelunasan, idr_pelunasan, tipe_trans, update_by, update_date, flag, status_lunas)
																						VALUES (DEFAULT, $supplier, '$tanggal', $purch_no, $keterangan, 0, $grandtotal, 0, 0, 1, $session_id, '$update_date', 1, 0)");

		if($sql_header && $sql_detail)
			return true;
		return false;
	}


	public function load_purchase_header(){
		$sql = $this->db->query("SELECT h.purchase_header_id, h.purchase_no, h.trans_date, h.keterangan, h.supplier_id, c.nama as nsupplier
															FROM public.beone_purchase_header h INNER JOIN public.beone_custsup c ON h.supplier_id = c.custsup_id WHERE h.flag = 1");
		return $sql->result_array();
	}

	public function get_default_header($purchase_id){
		$sql = $this->db->query("SELECT h.purchase_header_id, h.purchase_no, h.trans_date, h.keterangan, h.supplier_id, h.ppn, c.nama as nsupplier
															FROM public.beone_purchase_header h INNER JOIN public.beone_custsup c ON h.supplier_id = c.custsup_id WHERE h.flag = 1 AND h.purchase_header_id = ".intval($purchase_id));
		if($sql->num_rows() > 0)
			return $sql->row_array();
		return false;
	}

	public function get_default_detail($purchase_id){
		$sql = $this->db->query("SELECT h.purchase_header_id, h.purchase_no, h.trans_date, h.keterangan, h.supplier_id, d.item_id, d.qty, d.price, d.amount
															FROM public.beone_purchase_header h INNER JOIN public.beone_purchase_detail d ON h.purchase_header_id = d.purchase_header_id WHERE h.flag = 1 AND d.purchase_header_id = ".intval($purchase_id));
		return $sql->result_array();
	}

	public function delete($purchase_id, $purchase_no){
		$sql = $this->db->query("DELETE FROM public.beone_purchase_header WHERE purchase_header_id = ".intval($purchase_id));
		$sql_detail = $this->db->query("DELETE FROM public.beone_purchase_detail WHERE purchase_header_id = ".intval($purchase_id));

		$purch_no = str_replace("-", "/", $purchase_no);

		helper_log($tipe = "delete", $str = "Hapus Pembelian, No ".$purchase_no);

		$sql_gl = $this->db->query("DELETE FROM public.beone_gl WHERE gl_number = '$purch_no'");
		$sql_hp = $this->db->query("DELETE FROM public.beone_hutang_piutang WHERE nomor = '$purch_no'");
		$sql_del_gudang = $this->db->query("DELETE FROM public.beone_gudang_detail WHERE nomor_transaksi = '$purch_no'");
		$sql_inventory = $this->db->query("DELETE FROM public.beone_inventory WHERE intvent_trans_no = '$purch_no'");
	}


}
?>
