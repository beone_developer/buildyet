<?php
//Model_data.php
defined('BASEPATH') OR exit('No direct script access allowed');

Class Adjustment_model extends CI_Model{

	public function load_item_dari_stockopname(){
		$tgl_sekarang = date('Y-m-d');

		$tahun = substr($tgl_sekarang, 0, 4);
		$tgl_awal_tahun = $tahun."-01-01";

		$sql_hasil_opname_header = $this->db->query("SELECT * FROM public.beone_opname_header WHERE flag = 1 ORDER BY opname_date DESC, opname_header_id DESC LIMIT 1");
		$hasil_header = $sql_hasil_opname_header->row_array();
		$header_id = $hasil_header['opname_header_id'];

		$sql_list_item_selisih_opname = $this->db->query("SELECT d.item_id, i.nama as nitem, d.qty_existing, d.qty_opname, d.qty_selisih, d.status_opname
																											FROM public.beone_opname_detail d INNER JOIN public.beone_item i ON d.item_id = i.item_id
																											WHERE d.flag = 1 AND d.qty_selisih <> 0 AND d.opname_header_id = ".intval($header_id));

		return $sql_list_item_selisih_opname->result_array();
	}

	public function load_adjustment(){
		$sql = $this->db->query("SELECT a.adjustment_id, a.adjustment_no, a.adjustment_date, a.keterangan, a.item_id, i.nama as nitem, a.qty_adjustment, a.posisi_qty, a.update_by, a.update_date, a.flag
															FROM public.beone_adjustment a INNER JOIN public.beone_item i ON a.item_id = i.item_id
															WHERE a.flag = 1");

		return $sql->result_array();
	}

	public function load_item_penyesuaian(){
		$sql = $this->db->query("SELECT * FROM public.beone_item");
		return $sql->result_array();
	}


	public function simpan($post){
		$session_id = $this->session->userdata('user_id');
		$item = $this->db->escape($post['item']);
		$adjustment_no = $this->db->escape($post['nomor_adjustment']);
		$posisi = $this->db->escape($post['posisi']);
		//$qty_adjustment = $this->db->escape($post['qty_adjustment']);
		$keterangan = $this->db->escape($post['Keterangan_adjustment']);
		$gudang = $this->db->escape($post['gudang']);
		$update_date = date('Y-m-d');

		$qty_adjustment_ = str_replace(".", "", $post['qty_adjustment']);
		$qty_adjustment = str_replace(",", ".", $qty_adjustment_);

		$tanggal_awal = $this->db->escape($post['tanggal']);
		$tgl_bulan = substr($tanggal_awal, 1, 2);
		$tgl_hari = substr($tanggal_awal, 4, 2);
		$tgl_tahun = substr($tanggal_awal, 7, 4);

		$tanggal = $tgl_tahun."-".$tgl_bulan."-".$tgl_hari;

		$sql = $this->db->query("INSERT INTO public.beone_adjustment(
															adjustment_id, adjustment_no, adjustment_date, keterangan, item_id, qty_adjustment, posisi_qty, update_by, update_date, flag)
															VALUES (DEFAULT, $adjustment_no, '$tanggal', $keterangan, $item, $qty_adjustment, $posisi, $session_id, '$update_date', 1)");

		/*$sql_opnae_detail = $this->db->query("UPDATE public.beone_opname_detail
																					SET flag=0
																					WHERE opname_detail_id = ".intval());*/

	 //0 minus, 1 plus
		if ($post['posisi'] == 0){
			$sql_gudang_detail = $this->db->query("INSERT INTO public.beone_gudang_detail(
																						gudang_detail_id, gudang_id, trans_date, item_id, qty_in, qty_out, nomor_transaksi, update_by, update_date, flag, keterangan, kode_tracing)
																						VALUES (DEFAULT, $gudang, '$tanggal', $item, 0, $qty_adjustment, 'ADJUSTMENT', $session_id, '$update_date', 1, $keterangan, 'ADJUSTMENT')");
		}else{
			$sql_gudang_detail = $this->db->query("INSERT INTO public.beone_gudang_detail(
																						gudang_detail_id, gudang_id, trans_date, item_id, qty_in, qty_out, nomor_transaksi, update_by, update_date, flag, keterangan, kode_tracing)
																						VALUES (DEFAULT, $gudang, '$tanggal', $item, $qty_adjustment, 0, 'ADJUSTMENT', $session_id, '$update_date', 1, $keterangan, 'ADJUSTMENT')");
		}



//************************* INVENTORY ******************************************************
		$count_transksi_inventory = $this->db->query("SELECT COUNT(intvent_trans_id) as jml FROM public.beone_inventory WHERE flag = 1 AND item_id = $item");
		$hasil_count_inventory = $count_transksi_inventory->row_array();

		$sql_saldo_awal_item = $this->db->query("SELECT * FROM public.beone_item WHERE flag = 1 AND item_id = $item");
		$hasil_saldo_awal_item = $sql_saldo_awal_item->row_array();

		$sql_saldo_awal = $this->db->query("SELECT * FROM public.beone_inventory WHERE flag = 1 AND item_id = $item ORDER BY trans_date DESC, intvent_trans_id DESC LIMIT 1");
		$hasil_saldo_awal = $sql_saldo_awal->row_array();

		if ($hasil_count_inventory['jml'] == 0){// menggunakan saldo awal item
				$saldo_awal_qty = $hasil_saldo_awal_item['saldo_qty'];
				$saldo_awal_amount = $hasil_saldo_awal_item['saldo_idr'];

				if ($saldo_awal_qty == 0 OR $saldo_awal_amount == 0){
						$saldo_awal_unit_price = 0;
				}else{
						$saldo_awal_unit_price = $saldo_awal_amount / $saldo_awal_qty;
				}
		}else{// menggunakan saldo dari transaksi akhir inventory
				$saldo_awal_qty = $hasil_saldo_awal['sa_qty'];
				$saldo_awal_unit_price = $hasil_saldo_awal['sa_unit_price'];
				$saldo_awal_amount = $hasil_saldo_awal['sa_amount'];
		}


		if ($post['posisi'] == 0){//minus (pengurangan inventory / qty out)
				$saldo_akhir_qty = $saldo_awal_qty - $qty_adjustment;
				$saldo_akhir_amount = $saldo_awal_amount - ($qty_adjustment * $saldo_awal_unit_price);
				$saldo_akhir_unit_price = $saldo_akhir_amount / $saldo_akhir_qty;

				$sql_inventory = $this->db->query("INSERT INTO public.beone_inventory(
																				intvent_trans_id, intvent_trans_no, item_id, trans_date, keterangan, qty_in, value_in, qty_out, value_out, sa_qty, sa_unit_price, sa_amount, flag, update_by, update_date)
																				VALUES (DEFAULT, $adjustment_no, $item, '$tanggal', $keterangan, 0, 0, $qty_adjustment, $saldo_awal_unit_price, $saldo_akhir_qty, $saldo_akhir_unit_price, $saldo_akhir_amount, 1, $session_id, '$update_date')");

				/*$sql_opname = $this->db->query("UPDATE public.beone_opname_detail
																				SET qty_existing=?, qty_selisih=?, status_opname=?
																				WHERE <condition>");*/

		}else{//plus (penambahan inventory / qty in)
				$saldo_akhir_qty = $saldo_awal_qty + $qty_adjustment;
				$saldo_akhir_amount = $saldo_awal_amount + ($qty_adjustment * $saldo_awal_unit_price);
				$saldo_akhir_unit_price = $saldo_akhir_amount / $saldo_akhir_qty;

				$sql_inventory = $this->db->query("INSERT INTO public.beone_inventory(
																				intvent_trans_id, intvent_trans_no, item_id, trans_date, keterangan, qty_in, value_in, qty_out, value_out, sa_qty, sa_unit_price, sa_amount, flag, update_by, update_date)
																				VALUES (DEFAULT, $adjustment_no, $item, '$tanggal', $keterangan, $qty_adjustment, $saldo_awal_unit_price, 0, 0, $saldo_akhir_qty, $saldo_akhir_unit_price, $saldo_akhir_amount, 1, $session_id, '$update_date')");
		}
//************************* END INVENTORY ******************************************************

	if($sql AND $sql_inventory)
			return true;
		return false;
	}

	public function delete($adjustment_id, $adjustment_no){
		$sql = $this->db->query("DELETE FROM public.beone_adjustment WHERE adjustment_id = ".intval($adjustment_id));
		$sql_inv = $this->db->query("DELETE FROM public.beone_inventory WHERE intvent_trans_no = '$adjustment_no'");
		//$sql_gl = $this->db->query("DELETE FROM public.beone_gl WHERE pasangan_no = '$transfer_no'");
	}


}
?>
