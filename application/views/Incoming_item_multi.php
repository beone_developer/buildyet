<h3>Incoming Item</h3>
<div class="portlet light bordered">
	<div class="portlet-title">
		<div class="tools"></div>
	</div>

	<table class="table table-striped table-bordered table-hover" id="sample_2">
		<thead>
		<tr>
			<th width="10%">
				<center><small>Tgl Pengajuan</small></center>
			</th>
			<th width="20%">
				<center><small>No Pengajuan</small></center>
			</th>
			<th width="5%">
				<center><small>No Daftar</small></center>
			</th>
			<th width="5%">
				<center><small>Kode <br/> Pabean</small></center>
			</th>
			<th width="15%">
				<center><small>Supplier</small></center>
			</th>
			<th width="10%">
				<center><small>Berat</small></center>
			</th>
			<th width="10%">
				<center><small>Currency</small></center>
			</th>
			<th width="10%">
				<center><small>Amount</small></center>
			</th>
			<th width="15%">
				<center><small>Action</small></center>
			</th>
		</tr>
		</thead>
		<tbody>
		<?php
		$no = 0;
		foreach ($list_import_header as $row) {
			?>
			<tr>
				<td><small><?php echo $row['TANGGAL_AJU']; ?></small></td>
				<td><small><?php echo $row['NOMOR_AJU']; ?></small></td>
				<td><small><?php echo $row['NOMOR_DAFTAR']; ?></small></td>
				<td><small><?php echo "BC " . $row['KODE_DOKUMEN_PABEAN']; ?></small></td>
				<td><small><?php
						if ($row['KODE_DOKUMEN_PABEAN'] == 23) {
							echo $row['NAMA_PEMASOK']; //bc23
						} else {
							echo $row['NAMA_PENGIRIM']; //bc40
						}

						?></small></td>
				<td><small><?php echo number_format($row['BRUTO'], 2); ?></small></td>
				<td><small><?php
						if ($row['KODE_DOKUMEN_PABEAN'] == 23) {
							echo 'USD';
						} else {
							echo 'IDR';
						}
						?></small></td>
				<td><small><?php
						if ($row['KODE_DOKUMEN_PABEAN'] == 23) {
							echo number_format($row['HARGA_INVOICE'], 2);
						} else {
							echo number_format($row['HARGA_PENYERAHAN'], 2);
						}
						?></small></td>
				<?php
				$sql_received = $this->db->query("SELECT COUNT(tpb_header_id) as jml FROM public.beone_received_import WHERE tpb_header_id =" . intval($row['ID']));
				$hasil_received = $sql_received->row_array();
				$id = $hasil_received['jml'];
				if ($id > 0) {
					?>
					<td>
<!--						--><?php //if (helper_security("tracing") == 1) { ?><!--<a-->
<!--							href='--><?php //echo base_url('Inventin_controller/Tracing/' . $row['NOMOR_AJU'] . ''); ?><!--'-->
<!--							class='btn btn-circle yellow'><small>Tracing</small></a>--><?php //} ?>
						<a href='#' class='btn btn-circle green'><small>Sudah Terima</small></a></td>
				<?php } else {
					?>
					<td><?php if (helper_security("terima_barang") == 1) { ?><a
							href='<?php echo base_url('Inventin_multi_controller/Received/' . $row['ID'] . ''); ?>'
							class='btn btn-circle red'><small>Terima</small></a><?php } ?> </td>
				<?php } ?>
			</tr>
			<?php
		}
		?>
		</tbody>
	</table>
</div>

<script>
	function dialogHapus(urlHapus) {
		if (confirm("Apakah anda yakin ingin menghapus ini ?")) {
			document.location = urlHapus;
		}
	}
</script>
