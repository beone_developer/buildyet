<!-- BEGIN PAGE TITLE-->
<!-- END PAGE TITLE-->
<!-- END PAGE HEADER-->
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"></script>

<div class="portlet box blue ">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-gift"></i> Form Mutasi Stock
		</div>
		<div class="tools">
			<a href="" class="collapse"> </a>
			<a href="#portlet-config" data-toggle="modal" class="config"> </a>
			<a href="" class="reload"> </a>
			<a href="" class="remove"> </a>
		</div>
	</div>
	<div class="portlet-body form">
		<form role="form" method="post">
			<div class="form-body">
				<?php
				$error = $this->session->flashdata('error');
				if (isset($error)) {
					echo '<div class="alert alert-danger" role ="alert">' . $error . '</div>';
				}
				?>

				<div class="row">
					<div class="col-md-3">
						<div class="form-group">
							<label>Gudang Asal</label>
							<select id='gudangAsal' class='form-control input-sm select2-multiple' name='gudangAsal'>
								<option value=0><?php echo " - Pilih Gudang - "; ?></option>
								<?php foreach ($list_gudang as $gudang) { ?>
									<option
										value="<?php echo $gudang['gudang_id']; ?>"><?php echo $gudang['nama']; ?></option>
								<?php } ?>
								<
							</select>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label>Gudang Tujuan</label>
							<select id='gudangTujuan' class='form-control input-sm select2-multiple'
									name='gudangTujuan'>
								<option value=0><?php echo " - Pilih Gudang - "; ?></option>
								<?php foreach ($list_gudang as $gudang) { ?>
									<option
										value="<?php echo $gudang['gudang_id']; ?>"><?php echo $gudang['nama']; ?></option>
								<?php } ?>
								<
							</select>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="form-group">
							<label>Tanggal</label>
							<div class="input-group">
                            <span class="input-group-addon input-circle-left">
                                <i class="fa fa-calendar"></i>
                            </span>
								<input
									class="form-control form-control-inline input-medium date-picker input-circle-right"
									size="16" type="text" name="mutasi_stok_date" value="<?php echo date('m/d/Y'); ?>"
									readonly
									required/>
								<span class="help-block"></span>
							</div>
						</div>
					</div>
					<div class="col-sm-3">
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6">
						<div class="form-group">
							<label>Keterangan</label>
							<input type="text" class="form-control" placeholder="keterangan" name="keterangan" required>
						</div>
					</div>
				</div>
				<hr
				/ style="border-color: #3598DC;">
				<table id="datatable" class="table striped hovered cell-hovered">
					<thead>
					<tr>
						<td width="5%">ID</td>
						<td width="65%">Item</td>
						<td width="15%">Qty</td>
						<td width="15%"></td>
					</tr>
					</thead>
					<tbody id="container">

					</tbody>
				</table>

			</div>
			<div class="form-actions">
				<a class="btn blue" data-toggle="modal" href="#responsive"> Tambah Data </a>
				<?php //if (helper_security("pembelian_add") == 1) { ?>
				<button type="submit" class="btn red" name="submit_mutasi">Submit</button>
				<?php //} ?>
			</div>
		</form>
	</div>
</div>


<!--------------------------- MODAL ADD ITEM--------------------------------------------->
<div id="responsive" class="modal fade" tabindex="-1" data-width="760">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
		<h4 class="modal-title">Detail Item</h4>
	</div>
	<div class="modal-body">
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label>Item</label>
					<select id="item_modal"
							class="form-control input-sm s2-ajax"
							data-s2-url="<?php echo base_url('Mutasi_stok_controller/search_item'); ?>" required>
					</select>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label>Quantity</label>
					<input type='text' class='form-control' placeholder="Quantity" name='qty_modal' id='qty_modal'
						   required>
				</div>
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
		<button type="button" class="btn green" name="add_btn" id="add_btn">Insert</button>
	</div>
</div>
<script>
	$(document).ready(function () {
		var count = 0;

		$("#add_btn").click(function () {
			count += 1;

			var item = document.getElementById('item_modal');
			var qty = document.getElementById('qty_modal');
			var namaItem = $('#item_modal option:selected').text();

			$('#container').append('<tr class="records" id="' + count + '">'
				+ '<td><input class="form-control" id="item_id_' + count + '" name="item_id_' + count + '" type="text" value="' + item.value + '" readonly></td>'
				+ '<td><input class="form-control" id="item_' + count + '" name="item_' + count + '" type="text" value="' + namaItem + '" readonly></td>'
				+ '<td><input class="form-control" id="qty_' + count + '" name="qty_' + count + '" type="text" value="' + qty.value + '" readonly></td>'
				+ '<td><input id="rows_' + count + '" name="rows[]" value="' + count + '" type="hidden"></td>'
				+ '<td><button type="button" class="btn red" onclick="hapus(' + count + ')">X</button></td></tr>'
			);

			eraseText();
			$('#responsive').modal('hide');
		});

		// $(".remove_item").live('click', function (ev) {
		// 	if (ev.type == 'click') {
		// 		$(this).parents(".records").fadeOut();
		// 		$(this).parents(".records").remove();
		// 	}
		// });

		$('#gudangAsal').on('change', function () {
			$("#item_modal").attr('data-s2-p-gudang', $(this).val());
		});
	});

	function eraseText() {
		document.getElementById("item_modal").value = "";
		document.getElementById("qty_modal").value = "";
	}

	function hapus(rowid) {
		var row = document.getElementById(rowid);
		row.parentNode.removeChild(row);
	}

</script>
