<h3>List Sales Order</h3>
<div class="portlet light bordered">
  <div class="portlet-title">
      <div class="tools"> </div>
  </div>

<table class="table table-striped table-bordered table-hover" id="sample_1">
        <thead>
          <tr>
              <th width='15%'><center>No Penjualan</center></th>
              <th width='25%'><center>Customer</center></th>
              <th width='15%'><center>Tanggal</center></th>
              <th width='25%'><center>Keterangan</center></th>
              <th width='20%'><center>Action</center></th>
              <th width='20%'><center>Action Realisasi</center></th>
          </tr>
        </thead>
        <tbody>
          <?php 	foreach($list_sales_order_header as $row){ ?>
            <tr>
                <td><small><center><?php echo $row['sales_no'];?></center></small></td>
                <td><small><?php echo $row['ncustomer'];?></small></td>
                <td><small><center><?php echo $row['trans_date'];?></center></small></td>
                <td><small><?php echo $row['keterangan'];?></small></td>
                <?php
                  $sales_no = str_replace("/", "-", $row['sales_no']); //konfersi karena akan dianggap parameter
                ?>
                <td><center>
                    <?php if(helper_security("penjualan_edit") == 1){?>
                      <?php if($row['realisasi'] == 0){?>
                        <a href='<?php echo base_url('Sales_order_controller/edit/'.$row['sales_header_id'].'');?>' class='btn blue'><i class="fa fa-pencil"></i> </a>
                      <?php }?>
                    <?php }?>
                    <?php if($row['realisasi'] == 0){?>
                      <?php if(helper_security("penjualan_delete") == 1){?>
                    <?php }?>
                    <a href="javascript:dialogHapus('<?php echo base_url('Sales_order_controller/delete/'.$row['sales_header_id'].'/'.$sales_no.'');?>')" class='btn red'><i class="fa fa-trash-o"></i> </a>
                    <?php }?>
                    <a href='<?php echo base_url('Sales_order_controller/sales_order_print/'.$row['sales_header_id'].'');?>' class='btn yellow'><i class="fa fa-print"></i> </a>
                </center></td>
                <td>
                  <center>
                    <?php if($row['realisasi'] == 0){?>
                      <a href="javascript:dialogReal('<?php echo base_url('Sales_order_controller/realisasi/'.$row['sales_header_id'].'/'.$sales_no.'');?>')" class='btn blue'><i class="fa fa-check"></i> </a>
                    <?php }?>
                    <?php if($row['realisasi'] == 1){?>
                      <a href='<?php echo base_url('Sales_order_controller/sales_order_printinv/'.$row['sales_header_id'].'');?>' class='btn yellow'><i class="fa fa-print"></i> </a>
                    <?php }?>
                  <center>
                </td>
            </tr>
            <?php
              }
            ?>
        </tbody>
    </table>
</div>

<script>
	function dialogHapus(urlHapus) {
	  if (confirm("Apakah anda yakin ingin menghapus ini ?")) {
		document.location = urlHapus;
	  }
  }

  // function dialogHapus(urlHapus){
  //   var reason = prompt("Apakah anda yakin ingin menghapus ini ?", "Masukkan keterangan hapus");
  //   document.getElementById("reason").value = reason;
  //   // document.getElementById("form").submit();
	// 	document.location = urlHapus;
  // }

  function dialogReal(urlReal) {
	  if (confirm("Apakah anda yakin ingin merealisasi ini ?")) {
		document.location = urlReal;
	  }
	}
</script>
