<h3>List Subkontrak Out</h3>
<div class="portlet light bordered">
  <div class="portlet-title">
      <div class="tools"> </div>
  </div>

<table class="table table-striped table-bordered table-hover" id="sample_2">
        <thead>
          <tr>
              <th width="20%"><center><small>No Pengajuan</small></center></th>
              <th width="20%"><center><small>No Subkontrak</small></center></th>
              <th width="15%"><center><small>Tgl Subkontrak</small></center></th>
              <th width="15%"><center><small>Pengirim</small></center></th>
							<th width="20%"><center><small>Action</small></center></th>
          </tr>
        </thead>
        <tbody>
          <?php
              $list_subkontrak = $this->db->query("SELECT h.subkon_out_id, h.no_subkon_out, h.trans_date, h.supplier_id, c.nama as nsupplier, h.keterangan, h.nomor_aju, h.flag, h.update_by, h.update_date
                                                  	FROM public.beone_subkon_out_header h INNER JOIN public.beone_custsup c ON h.supplier_id = c.custsup_id
                                                    WHERE h.flag = 1");

                $no=0;
								foreach($list_subkontrak->result_array() as $row){
					?>
            <tr>
								<td><small><?php echo $row['nomor_aju'];?></small></td>
                <td><small><?php echo $row['no_subkon_out'];?></small></td>
                <td><small><?php echo $row['trans_date'];?></small></td>
                <td><small><?php echo $row['nsupplier'];?></small></td>
                <td><a href='<?php echo base_url('Subkontrak_controller/Kirim/'.$row['ID'].'');?>' class='btn yellow'><small><i class='fa fa-eye'></i></small></a></td>
            </tr>
            <?php
              }
            ?>
        </tbody>
    </table>
</div>
