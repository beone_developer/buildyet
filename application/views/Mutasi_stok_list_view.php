<h3>List Konversi Stock</h3>
<div class="portlet light bordered">
  <div class="portlet-title">
      <div class="tools"> </div>
  </div>

<table class="table table-striped table-bordered table-hover" id="sample_1">
        <thead>
          <tr>
              <th width='15%'><center>No Mutasi</center></th>
              <th width='15%'><center>Tanggal</center></th>
              <th width='10%'><center>Keterangan</center></th>
              <th width='25%'><center>Action</center></th>
          </tr>
        </thead>
        <tbody>
          <?php 	foreach($list_konversi_stok as $row){ ?>
            <tr>
                <td><small><center><?php echo $row['mutasi_stok_no'];?></center></small></td>
                <td><small><?php echo $row['trans_date'];?></small></td>
                <td><small><center><?php echo $row['keterangan'];?></center></small></td>
                <?php
                  $mutasi_stok_no = str_replace("/", "-", $row['mutasi_stok_no']); //konfersi karena akan dianggap parameter
                ?>
                <td><center>
                  <?php if(helper_security("pembelian_edit") == 1){?>
                  <a href='<?php echo base_url('Mutasi_stok_controller/edit/'.$row['mutasi_stok_header_id'].'');?>' class='btn blue'><i class="fa fa-pencil"></i> </a>
                  <?php }?>
                  <?php if(helper_security("pembelian_delete") == 1){?>
                  <a href="javascript:dialogHapus('<?php echo base_url('Mutasi_stok_controller/delete/'.$row['mutasi_stok_header_id'].'/'.$mutasi_stok_no.'');?>')" class='btn red'><i class="fa fa-trash-o"></i> </a>
                  <?php }?>
                  <a href='<?php echo base_url('Mutasi_stok_controller/purchase_print/'.$row['mutasi_stok_header_id'].'');?>' class='btn yellow'><i class="fa fa-print"></i> </a></center></td>
            </tr>
            <?php
              }
            ?>
        </tbody>
    </table>
</div>

<script>
	function dialogHapus(urlHapus) {
	  if (confirm("Apakah anda yakin ingin menghapus ini ?")) {
		document.location = urlHapus;
	  }
	}
</script>
