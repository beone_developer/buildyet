<!-- BEGIN PAGE TITLE-->
<!-- END PAGE TITLE-->
<!-- END PAGE HEADER-->
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"></script>

<div class="portlet box blue ">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-gift"></i> Form Konversi Stock
		</div>
		<div class="tools">
			<a href="" class="collapse"> </a>
			<a href="#portlet-config" data-toggle="modal" class="config"> </a>
			<a href="" class="reload"> </a>
			<a href="" class="remove"> </a>
		</div>
	</div>
	<div class="portlet-body form">
		<form role="form" method="post">
			<div class="form-body">
				<?php
				$error = $this->session->flashdata('error');
				if (isset($error)) {
					echo '<div class="alert alert-danger" role ="alert">' . $error . '</div>';
				}
				?>
				<div class="row">
					<div class="col-sm-4">
						<?php
						/******************* GENERATE PO NUMBER ******************************/
						$tgl = date('m/d/Y');
						$thn = substr($tgl, 8, 2);
						$bln = substr($tgl, 0, 2);

						//LOOKUP KODE
						$lookup_kode = $this->db->query("SELECT * FROM public.beone_konversi_stok_header ORDER BY konversi_stok_header_id DESC LIMIT 1");
						$hasil_lookup_kode = $lookup_kode->row_array();

						//PENENTUAN BENTUK KODE
						$kode_awal = "KS/" . $thn . "/" . $bln . "/";


						$urutan = substr($hasil_lookup_kode['konversi_stok_no'], 10, 4);
						$no_lanjutan = $urutan + 1;

						$digit = strlen($no_lanjutan);
						$jml_nol = 4 - $digit;

						$cetak_nol = "";

						for ($i = 1; $i <= $jml_nol; $i++) {
							$cetak_nol = $cetak_nol . "0";
						}
						$nomorks = $kode_awal . $cetak_nol . $no_lanjutan;
						echo "<h3><b>" . $nomorks . "</b></h3>";
						echo "<div class='col-md-4 invisible'><input type='text' class='form-control' placeholder='Quantity'
						 name='nomorks' id='nomorks' value=" . $nomorks . "> </div>";
						/*************************************************/
						?>
					</div>


					<div class="col-sm-8">
						<div class="form-group">
							<label>Tanggal</label>
							<div class="input-group">
                            <span class="input-group-addon input-circle-left">
                                <i class="fa fa-calendar"></i>
                            </span>
								<input
									class="form-control form-control-inline input-medium date-picker input-circle-right"
									size="16" type="text" name="konversi_stok_date" value="<?php echo date('m/d/Y'); ?>"
									readonly
									required/>
								<span class="help-block"></span>
							</div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-sm-4">
						<div class="form-group">
							<label>Item</label>
							<div class="input-group">
								<select id="item_id" class="form-control input-sm select2-multiple"
										name="item_id" onchange="loadKomposisi(this.value)" required>
									<option value=0><?php echo " - Pilih Item - "; ?></option>
									<?php  foreach($list_item as $row){ echo '<option value='.$row['item_id'].'>'.$row['ntipe'].' - '.$row['nama'].'</option>';} ?>
                            
								</select>
							</div>
						</div>
					</div>

					<div class="col-md-2">
						<div class="form-group">
							<label>Quantity</label>
							<input type='text' class='form-control' placeholder="Quantity" name='qty' id='qty' value="0"
								   onchange="changeQty(1)"
								   required>
						</div>
					</div>
					<!-- <div class="col-md-3">
						<div class="form-group">
							<label>UoM</label>
							<select id='satuan_qty' class='form-control input-sm select2-multiple' name='satuan_qty'
									onchange="changeSatuan()">
								<option
									value="<?= isset($default['satuan_id']) ? $default['satuan_id'] : "" ?>"><?= isset($default['satuan_code']) ? $default['satuan_code'] : "" ?></option>
								<?php foreach ($list_satuan as $satuan) { ?>
									<option
										value="<?php echo $satuan['satuan_id']; ?>"><?php echo $satuan['satuan_code']; ?></option>
								<?php } ?>
								<
							</select>
						</div>
					</div> -->
					<div class="col-md-2">
						<div class="form-group">
							<label>Real Quantity</label>
							<input type='text' class='form-control' placeholder="Real Quantity" name='qty_real'
								   id='qty_real' value="0"
								   onchange="changeQty(1)"
								   required>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label>Gudang</label>
							<select id='gudang' class='form-control input-sm select2-multiple' name='gudang'
									onchange="changeGudang()">
								<option value=0><?php echo " - Pilih Gudang - "; ?></option>
								<?php foreach ($list_gudang as $gudang) { ?>
									<option
										value="<?php echo $gudang['gudang_id']; ?>"><?php echo $gudang['nama']; ?></option>
								<?php } ?>
								<
							</select>
						</div>
					</div>
				</div>

				<hr
				/ style="border-color: #3598DC;">
				<table id="datatable" class="table striped hovered cell-hovered">
					<thead>
					<tr>
						<td width="5%">ID</td>
						<td width="20%">Type</td>
						<td width="20%">Item</td>
						<td width="10%">Base Qty</td>
						<td width="10%">Allocation Qty</td>
						<td width="10%">Real Qty</td>
						<td width="15%">Gudang</td>
					</tr>
					</thead>
					<tbody id="container">

					</tbody>
				</table>

			</div>
			<div class="form-actions">
				<?php //if (helper_security("pembelian_add") == 1) { ?>
				<button type="submit" class="btn red" name="submit_konversi">Submit</button>
				<?php //} ?>
			</div>
		</form>
	</div>
</div>

<script>
	function loadKomposisi(item_id) {
		$url = "<?php echo base_url('Konversi_stok_controller/loadKomposisi/') ?>" + item_id;
		$.ajax({
			url: $url,
			type: "POST",
			// contentType: "application/json",
			dataType: "json",
			success: function ($data) {
				$qty_real = $('#qty').val();
				$satuan_qty = $('#gudang option:selected').text();
				$appendDetail = "";
				$($data).each(function (count, val) {
					$appendDetail += '<tr class="records" id="' + count + '">'
						+ '<td><input class="form-control" id="item_id_' + count + '" name="item_id_' + count + '" type="text" value="' + val['item_baku_id'] + '" readonly></td>'
						+ '<td><input class="form-control" id="item_type_' + count + '" name="item_type_' + count + '" type="text" value="' + val['ntipe'] + '" readonly></td>'
						+ '<td><input class="form-control" id="item_' + count + '" name="item_' + count + '" type="text" value="' + val['item_baku'] + '" readonly></td>'
						+ '<td><input class="form-control" id="base_qty_' + count + '" name="base_qty_' + count + '" type="text" value="' + val['qty_item_baku'] + '" onchange="changeQty(' + count + ')"></td>'
						+ '<td><input class="form-control" id="allocation_qty_' + count + '" name="allocation_qty_' + count + '" type="text" value="0" readonly></td>'
						+ '<td><input class="form-control" id="qty_real_' + count + '" name="qty_real_' + count + '" type="text" value="0" readonly></td>'
						+ '<td><input class="form-control" id="gudang_id_' + count + '" name="gudang_id_' + count + '" type="text" value="' + $satuan_qty + '" readonly></td>'
						+ '<td><input id="rows_' + count + '" name="rows[]" value="' + count + '" type="hidden"></td>'
					// + '<td><button type="button" class="btn red" onclick="hapus('+count+')">X</button></td></tr>'
				});
				$('#container').html($appendDetail);
			},
			error: function (jqXHR, textStatus, errorThrown) {
				alert('Error get data from ajax ' + errorThrown);
			}
		});
	}

	function changeSatuan() {
		var satuan_qty = $('#satuan_qty option:selected').text();
		$('tr').each(function (i) {
			$('#satuan_qty_' + i).val(satuan_qty);
		});
	}

	function changeGudang() {
		var satuan_qty = $('#gudang option:selected').text();
		$('tr').each(function (i) {
			$('#gudang_id_' + i).val(satuan_qty);
		});
	}

	function changeQtyHeader() {
		$('#qty').prop('disabled', true);
		// var qty_proses = $('#processed_qty').val();
		// if (qty_proses == 0){
		// 	qty_proses = 1;
		// }
		// var mutasi = qty_header / qty_proses;
		// var qty = $('#base_qty_' + row_id).val();
		// var alocation = qty * qty_header;
		// $('#allocation_qty_' + row_id).val(alocation);
		// $('#qty_real_' + row_id).val(alocation / mutasi);
	}

	function changeQty(row_id) {
		$('tr').each(function (i) {
			var qty_header = $('#qty').val();
			var qty_proses = $('#qty_real').val();
			if (qty_proses == 0) {
				qty_proses = 1;
			}
			var mutasi = qty_header / qty_proses;
			var qty = $('#base_qty_' + i).val();
			var alocation = qty * qty_header;
			$('#allocation_qty_' + i).val(alocation);
			$('#qty_real_' + i).val(alocation / mutasi);
		});
	}


	function hapus(rowid) {
		console.log(rowid)
		var row = document.getElementById(rowid);
		row.parentNode.removeChild(row);
	}
</script>
