<h3>List Pindah Gudang</h3>
<div class="portlet light bordered">
  <div class="portlet-title">
      <div class="tools"> </div>
  </div>

<table class="table table-striped table-bordered table-hover" id="sample_1">
        <thead>
          <tr>
              <th width='20%'><center>Nomor Pengajuan</center></th>
              <th width='20%'><center>Tanggal</center></th>
              <th width='40%'><center>Keterangan</center></th>
              <th width='20%'><center>Action</center></th>
          </tr>
        </thead>
        <tbody>
          <?php 	foreach($List_pindah_gudang as $row){ ?>
            <tr>
                <td><?php echo $row['nomor_transaksi'];?></td>
                <td><center><?php echo $row['trans_date'];?></center></td>
                <td><?php echo $row['keterangan'];?></td>
                <td><center><a href='<?php echo base_url('Pindah_gudang_controller/pindah_print/'.$row['gudang_detail_id'].'');?>' class='btn yellow'><i class="fa fa-print"></i> </a></center></td>
            </tr>
            <?php
              }
            ?>
        </tbody>
    </table>
</div>
