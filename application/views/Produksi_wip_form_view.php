<!-- BEGIN PAGE TITLE-->
<!-- END PAGE TITLE-->
<!-- END PAGE HEADER-->
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"></script>

<div class="portlet box blue ">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-gift"></i> Produksi Barang Jadi
		</div>
		<div class="tools">
			<a href="" class="collapse"> </a>
			<a href="#portlet-config" data-toggle="modal" class="config"> </a>
			<a href="" class="reload"> </a>
			<a href="" class="remove"> </a>
		</div>
	</div>
	<div class="portlet-body form">


		<form role="form" method="post">
			<div class="form-body">

				<div class="row">
					<div name="txtnopemakaian" id="txtnopemakaian"><b>Nomor Produksi</b></div>

					<div class="col-sm-8">
						<div class="form-group">
							<label>Tanggal</label>
							<div class="input-group">
                            <span class="input-group-addon input-circle-left">
                                <i class="fa fa-calendar"></i>
                            </span>
								<input
									class="form-control form-control-inline input-medium date-picker input-circle-right"
									size="16" type="text" name="tanggal" value="<?php echo date('m/d/Y'); ?>"
									onChange="tampilnomor(this.value);" readonly required/>
								<span class="help-block"></span>
							</div>
						</div>
					</div>
				</div>

				<div class="row">

					<?php
					$list_item3 = $this->db->query("SELECT * FROM public.beone_item WHERE item_type_id = 3");
					?>
					<div class="col-sm-4">
						<div class="form-group">
							<label>Item</label>
							<select id='item_modal' class='form-control input-sm select2-multiple' name='item_modal'>
								<option disabled="disabled"><?php echo " - Pilih Item - "; ?></option>
								<?php foreach ($list_item3->result_array() as $row) { ?>
									<option
										value="<?php echo $row['item_id']; ?>"><?php echo $row['nama'] . " (" . $row['item_code'] . ")"; ?></option>
									<?php
								}
								?>
							</select>
						</div>
					</div>

					<div class="col-sm-4">
						<div class="form-group">
							<label>Qty</label>
							<input type="text" class="form-control" placeholder="Qty" id="qty_modal" name="qty_modal"
								   required>
						</div>
					</div>

					<div class="col-sm-4">
						<div class="form-group">
							<label>Keterangan</label>
							<input type="text" class="form-control" placeholder="Keterangan" id="keterangan"
								   name="keterangan" required>
							<input type"text" name="jenis_produksi" value="bj" hidden>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-4">
						<div class="form-group">
							<label>PM</label>
							<div class="input-group">
								<select id="select2-single-input-sm" class="form-control input-sm select2-multiple"
										name="nomor_pm" required>
									<option value=""></option>
									<?php foreach ($list_pm as $row) { ?>
										<option
											value="<?php echo $row['pm_header_id']; ?>"><?php echo $row['no_pm']; ?></option>
									<?php } ?>
								</select>
							</div>
						</div>
					</div>
				</div>


				<hr
				/ style="border-color: #3598DC;">
				<table id="datatable" class="table striped hovered cell-hovered">
					<thead>
					<tr>
						<td width="50%">Pemakaian Bahan</td>
						<td width="10%"></td>
					</tr>
					</thead>
					<tbody id="container">

					</tbody>
				</table>

				<div class="row">

				</div>


			</div>
			<div class="form-actions">
				<a class="btn blue" data-toggle="modal" href="#responsive"> Tambah Data </a>
				<?php if (helper_security("po_add") == 1) { ?>
					<button type="submit" class="btn red" name="submit_proudksi">Submit</button>
				<?php } ?>
			</div>
		</form>
	</div>
</div>

<?php
$list_item2 = $this->db->query("SELECT * FROM public.beone_pemakaian_bahan_header WHERE status = 1 AND nomor_pemakaian LIKE 'PKW%'");
?>

<!--------------------------- MODAL ADD ITEM--------------------------------------------->
<div id="responsive" class="modal fade" tabindex="-1" data-width="760">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
		<h4 class="modal-title">Detail Pemakaian</h4>
	</div>
	<div class="modal-body">
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label>Pemakaian Bahan</label>
					<select id='pemakaian_bahan' class='form-control input-sm select2-multiple' name='pemakaian_bahan'>
						<option value=""></option>
						<?php foreach ($list_item2->result_array() as $row2) { ?>
							<option
								value="<?php echo $row2['pemakaian_header_id']; ?>"><?php echo $row2['nomor_pemakaian'] . " (" . $row2['keterangan'] . ")"; ?></option>
							<?php
						}
						?>
					</select>
				</div>
			</div>
		</div>

	</div>
	<div class="modal-footer">
		<button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
		<button type="button" class="btn green" name="add_btn" id="add_btn">Insert</button>
	</div>
</div>


<script>
	$(document).ready(function () {
		var count = 0;

		$("#add_btn").click(function () {
			count += 1;

			var pemakaian = document.getElementById('pemakaian_bahan');
			//var qty = document.getElementById('qty_modal');
			var namaPemakaian = $('#pemakaian_bahan option:selected').text();

			if (pemakaian.value == "") {
				alert("Silahkan isi pemakaian bahan..!!!");
				pemakaian_bahan.focus();
				return false;
			}

			$('#container').append(
				'<tr class="records" id="' + count + '">'
				+ '<td><input class="form-control" id="pemakaian_bahan_' + count + '" name="pemakaian_bahan_' + count + '" type="text" value="' + namaPemakaian + '" readonly></td>'
				+ '<td><button type="button" class="btn red" onclick="hapus(' + count + ')">X</button></td>'
				+ '<td><input id="rows_' + count + '" name="rows[]" value="' + count + '" type="hidden"></td>'
				+ '<td><input class="form-control" id="pemakaian_id_' + count + '" name="pemakaian_id_' + count + '" type="hidden" value="' + pemakaian.value + '"></td></tr>'
			);

			//eraseText();
			$('#responsive').modal('hide');
		});

	});


	function eraseText() {
		$("#pemakaian_bahan").select2("val", " ");
	}


	function hapus(rowid) {
		var row = document.getElementById(rowid);
		row.parentNode.removeChild(row);
	}
</script>

<script type="text/javascript">
	var qty_modal = document.getElementById('qty_modal');
	qty_modal.addEventListener('keyup', function (e) {
		qty_modal.value = formatRupiah(this.value, 'Rp. ');
	});


	/* Fungsi formatRupiah */
	function formatRupiah(angka, prefix) {
		var number_string = angka.replace(/[^,\d]/g, '').toString(),
			split = number_string.split(','),
			sisa = split[0].length % 3,
			rupiah = split[0].substr(0, sisa),
			ribuan = split[0].substr(sisa).match(/\d{3}/gi);

		// tambahkan titik jika yang di input sudah menjadi angka ribuan
		if (ribuan) {
			separator = sisa ? '.' : '';
			rupiah += separator + ribuan.join('.');
		}

		rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
		return prefix == undefined ? rupiah : (rupiah ? rupiah : '');
	}
</script>

<script>
	function tampilnomor(str) {
		if (str == "") {
			document.getElementById("txtnopemakaian").innerHTML = "";
			return;
		}
		if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp = new XMLHttpRequest();
		} else {// code for IE6, IE5
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange = function () {
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
				document.getElementById("txtnopemakaian").innerHTML = xmlhttp.responseText;
			}
		}
		xmlhttp.open("GET", "Produksi_controller/get_nomor_produksi?q=" + str, true);
		xmlhttp.send();
	}
</script>
