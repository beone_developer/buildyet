<div class="portlet box blue ">
      <div class="portlet-title">
          <div class="caption">
              <i class="fa fa-gift"></i> Form Adjustment</div>
          <div class="tools">
              <a href="" class="collapse"> </a>
              <a href="#portlet-config" data-toggle="modal" class="config"> </a>
              <a href="" class="reload"> </a>
              <a href="" class="remove"> </a>
          </div>
      </div>
      <div class="portlet-body form">
          <form role="form" method="post">
              <div class="form-body">
                <div class="row">

                  <div class="col-sm-4">
                      <div class="form-group">
                            <label>Tanggal</label>
                            <div class="input-group">
                              <span class="input-group-addon input-circle-left">
                                  <i class="fa fa-calendar"></i>
                              </span>
                              <input class="form-control form-control-inline input-medium date-picker input-circle-right" name="tanggal" size="16" type="text" value="<?php echo date('m/d/Y');?>" readonly/>
                              <span class="help-block"></span>
                            </div>
                      </div>

                          <div name="txtNoAdjustment" id="txtNoAdjustment"><b>Nomor Adjustment</b></div>

                  </div>


                  <div class="col-sm-8" id="detail_cash_bank">
                      <div class="portlet light bordered">

                        <div class="row">
                            <div class="col-sm-12">
                              <div class="form-group">
                                  <label>Item</label>
                                  <div class="input-group">
                                     <select id="select2-single-input-sm" class="form-control input-sm select2-multiple" name="item" onChange="tampilkode(this.value);" required>
                                        <option value=""></option>
                                       <?php 	foreach($list_item as $row){ ?>
                                         <option value="<?php echo $row['item_id'];?>"><?php echo $row['nitem']." | Qty Ext = ".$row['qty_existing']." | Qty Opn = ".$row['qty_opname']." | Selisih = ".$row['qty_selisih'];?></option>
                                       <?php } ?>
                                     </select>
                               </div>
                             </div>
                            </div>
                        </div>

                        <div class="row">
                          <div class="col-sm-12">
                            <div class="form-group">
                              <label>Keterangan</label>
                              <input type="text" class="form-control" placeholder="Keterangan" name="Keterangan_adjustment" required>
                            </div>
                          </div>
                          <div class="col-sm-12">
                              <div class="row">
                                <div class="col-sm-4">
                                  <div class="form-group">
                                      <label>Gudang</label>
                                      <div class="input-group">
                                         <select id="select2-single-input-sm" class="form-control input-sm select2-multiple" name="gudang" required>
                                            <option value=""></option>
                                           <?php 	foreach($list_gudang as $row){ ?>
                                             <option value="<?php echo $row['gudang_id'];?>"><?php echo $row['nama'];?></option>
                                           <?php } ?>
                                         </select>
                                   </div>
                                 </div>
                                </div>
                                <div class="col-sm-4">
                                  <div class="form-group">
                                      <label>Posisi</label>
                                      <div class="input-group">
                                         <select id="select2-single-input-sm" class="form-control input-sm select2-multiple" name="posisi" required>
                                              <option value=""></option>
                                              <option value=0>MINUS (-)</option>
                                              <option value=1>PLUS (+)</option>
                                         </select>
                                   </div>
                                 </div>
                                </div>
                                <div class="col-sm-4">
                                  <div class="form-group">
                                  <label>Qty Adjustment</label>
                                  <input type="text" class="form-control" placeholder="Qty Adjustment" name="qty_adjustment" id="qty_adjustment" required>
                                  </div>
                                </div>
                              </div>
                          </div>
                        </div>

                      </div>
                  </div>
              </div>
            </div>

            <div class="form-actions">
                <button type="button" class="btn default">Cancel</button>
                <?php if(helper_security("adjustment_add") == 1){?>
                <button type="submit" class="btn red" name="submit_adjustment">Submit</button>
                <?php }?>
            </div>
          </form>
      </div>
    </div>

    <script>
				function tampilkode(str)
				{
				if (str=="")
				  {
				  document.getElementById("txtNoAdjustment").innerHTML="";
				  return;
				  }
				if (window.XMLHttpRequest)
				  {// code for IE7+, Firefox, Chrome, Opera, Safari
				  xmlhttp=new XMLHttpRequest();
				  }
				else
				  {// code for IE6, IE5
				  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
				  }
				xmlhttp.onreadystatechange=function()
				  {
				  if (xmlhttp.readyState==4 && xmlhttp.status==200)
					{
					document.getElementById("txtNoAdjustment").innerHTML=xmlhttp.responseText;
					}
				  }
				xmlhttp.open("GET","Adjustment_controller/get_nomor_adjustment?q="+str,true);
				xmlhttp.send();
				}
				</script>


        <script type="text/javascript">

    		var qty_adjustment = document.getElementById('qty_adjustment');
    		  qty_adjustment.addEventListener('keyup', function(e){
    			qty_adjustment.value = formatRupiah(this.value, 'Rp. ');
    		});

    		/* Fungsi formatRupiah */
    		function formatRupiah(angka, prefix){
    			var number_string = angka.replace(/[^,\d]/g, '').toString(),
    			split   		= number_string.split(','),
    			sisa     		= split[0].length % 3,
    			rupiah     		= split[0].substr(0, sisa),
    			ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);

    			// tambahkan titik jika yang di input sudah menjadi angka ribuan
    			if(ribuan){
    				separator = sisa ? '.' : '';
    				rupiah += separator + ribuan.join('.');
    			}

    			rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
    			return prefix == undefined ? rupiah : (rupiah ? rupiah : '');
    		}
    	</script>
