<?php
  $tgl_awal = $_GET['tglawal'];
  $tgl_akhir = $_GET['tglakhir'];
  $coa_id_gl = $_GET['coa_id'];

  $thn_awal = substr($tgl_awal,6,4);
  $bln_awal = substr($tgl_awal,0,2);
  $day_awal = substr($tgl_awal,3,2);
  $tgl_awal_formated = $thn_awal."-".$bln_awal."-".$day_awal;

  $thn_akhir = substr($tgl_akhir,6,4);
  $bln_akhir = substr($tgl_akhir,0,2);
  $day_akhir = substr($tgl_akhir,3,2);
  $tgl_akhir_formated = $thn_akhir."-".$bln_akhir."-".$day_akhir;
?>
          <h1>
              <center><b>Report General Ledger</b></center>
          </h1>
          <hr />

              <div class="row">
                  <div class="col-xs-12">
                    <?php
                    if ($coa_id_gl == 0){
                      foreach($list_akun_gl as $row_gl){
                      echo "<h4><b>".$row_gl['nama']." (".$row_gl['coa_no'].")</b></h4>";
                    ?>
                      <table class="table table-striped table-hover">
                          <thead>
                              <tr>
                                  <th> # </th>
                                  <th> Tanggal </th>
                                  <th> Keterangan </th>
                                  <th class="hidden-xs"> COA Lawan </th>
                                  <th class="hidden-xs"> Debit </th>
                                  <th class="hidden-xs"> Kredit </th>
                                  <th> Saldo </th>
                              </tr>
                          </thead>

                          <tbody>
                            <?php
                            //untuk counter nomor urut
                            $no = 0;

                            //query menampilkan list data akun berdasarkan coa_id
                            $sql_saldo_awal = $this->db->query("SELECT * FROM public.beone_coa WHERE coa_id =".intval($row_gl['coa_id']));
                        		$hasil_saldo_awal = $sql_saldo_awal->row_array();

                            //mencari rumus posisi debet kredit dari tipe coa
                            $sql_posisi_dk = $this->db->query("SELECT * FROM public.beone_coa WHERE coa_id =".intval($row_gl['coa_id']));
                            $hasil_posisi_dk = $sql_posisi_dk->row_array();

                            /************************** SALDO AWAL COA PENENTUAN DEBET KREDIT (AKUN PENYUSUTAN NILAI KREDIT) ****************************************/
                            if ($hasil_posisi_dk['tipe_akun'] == 1 OR $hasil_posisi_dk['tipe_akun'] == 5 OR $hasil_posisi_dk['tipe_akun'] == 6 OR $hasil_posisi_dk['tipe_akun'] == 7 OR $hasil_posisi_dk['tipe_akun'] == 9){
                                if ($hasil_posisi_dk['nomor'] == '118.01.01' OR $hasil_posisi_dk['nomor'] == '118.01.02'){
                                  $saldo_awal_coa = $hasil_saldo_awal['kredit_idr']*-1;
                                }else{
                                  $saldo_awal_coa = $hasil_saldo_awal['debet_idr'];
                                }
                            }elseif($hasil_posisi_dk['tipe_akun'] == 2 OR $hasil_posisi_dk['tipe_akun'] == 3 OR $hasil_posisi_dk['tipe_akun'] == 4 OR $hasil_posisi_dk['tipe_akun'] == 8){
                              $saldo_awal_coa = $hasil_saldo_awal['kredit_idr']*-1;
                            }
                            /************************************ END SALDO AWAL COA PENENTUAN DEBET KREDIT (AKUN PENYUSUTAN NILAI KREDIT) *******************************************/

                            //query list total mutasi debet kredit per tanggal dari GL
                            $sql_saldo_mutasi = $this->db->query("SELECT sum(debet) as totald, sum(kredit) as totalk FROM public.beone_gl WHERE gl_date BETWEEN '2019-01-01' AND '$tgl_awal_formated' AND coa_id =".intval($row_gl['coa_id']));
                            $hasil_saldo_mutasi = $sql_saldo_mutasi->row_array();

                            $total_debet_mutasi = $hasil_saldo_mutasi['totald'];
                            $total_kredit_mutasi = $hasil_saldo_mutasi['totalk'];

                            /************************** SALDO AWAL MUTASI PENENTUAN DEBET KREDIT (AKUN PENYUSUTAN NILAI KREDIT) ****************************************/
                            if ($hasil_posisi_dk['tipe_akun'] == 1 OR $hasil_posisi_dk['tipe_akun'] == 5 OR $hasil_posisi_dk['tipe_akun'] == 6 OR $hasil_posisi_dk['tipe_akun'] == 7 OR $hasil_posisi_dk['tipe_akun'] == 9){
                              if ($hasil_posisi_dk['nomor'] == '118.01.01' OR $hasil_posisi_dk['nomor'] == '118.01.02'){
                                  $saldo_awal = ($saldo_awal_coa + $total_kredit_mutasi) - $total_debet_mutasi;
                              }else{
                                  $saldo_awal = ($saldo_awal_coa + $total_debet_mutasi) - $total_kredit_mutasi;
                              }
                            }elseif($hasil_posisi_dk['tipe_akun'] == 2 OR $hasil_posisi_dk['tipe_akun'] == 3 OR $hasil_posisi_dk['tipe_akun'] == 4 OR $hasil_posisi_dk['tipe_akun'] == 8){
                              $saldo_awal = ($saldo_awal_coa + $total_kredit_mutasi) - $total_debet_mutasi;
                            }
                            /************************** END SALDO AWAL MUTASI PENENTUAN DEBET KREDIT (AKUN PENYUSUTAN NILAI KREDIT) ****************************************/

                            //nilai saldo awal di general ledger per akun
                            $saldo_awal_fix = $saldo_awal;
                            $total_debit = 0;
                            $total_kredit = 0;
                            ?>

                            <!-- Menampilkan Saldo Awal -->
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td><b><?php echo number_format($saldo_awal,2);?></b></td>
                            </tr>

                            <?php
                            $sql_gl = $this->db->query("SELECT coa_id, gl_date, keterangan, coa_no, coa_no_lawan, debet, kredit FROM public.beone_gl WHERE coa_id =".intval($row_gl['coa_id']));
                            foreach($sql_gl -> result_array() as $row){
                            $no ++;
                            ?>
                              <tr>
                                  <td> <?php echo $no;?> </td>
                                  <td> <?php echo $row['gl_date'];?> </td>
                                  <td> <?php echo $row['keterangan'];?> </td>
                                  <td class="hidden-xs"> <?php echo $row['coa_no_lawan'];?> </td>
                                  <td class="hidden-xs"> <?php echo number_format($row['debet'], 2);?> </td>
                                  <td class="hidden-xs"> <?php echo number_format($row['kredit'],2);?> </td>
                                  <td> <?php
                                      /**** SALDO AKHIR ***/
                                      if ($hasil_posisi_dk['tipe_akun'] == 1 OR $hasil_posisi_dk['tipe_akun'] == 5 OR $hasil_posisi_dk['tipe_akun'] == 6 OR $hasil_posisi_dk['tipe_akun'] == 7 OR $hasil_posisi_dk['tipe_akun'] == 9){
                                          if ($hasil_posisi_dk['nomor'] == '118.01.01' OR $hasil_posisi_dk['nomor'] == '118.01.02'){
                                              $sa = ($saldo_awal+$row['kredit'])-$row['debet']; echo number_format($sa,2);
                                          }else{
                                              $sa = ($saldo_awal+$row['debet'])-$row['kredit']; echo number_format($sa,2);
                                          }
                                      }else{
                                          $sa = ($saldo_awal+$row['kredit'])-$row['debet']; echo number_format($sa,2);
                                      }
                                      ?>

                                  </td>
                              </tr>
                            <?php
                              $saldo_awal = $sa;
                              $total_debit = $total_debit + $row['debet'];
                              $total_kredit = $total_kredit + $row['kredit'];
                              }
                            ?>

                            <!-- Menampilkan Saldo Akhir -->
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td><b><?php echo number_format($total_debit,2);?></b></td>
                                <td><b><?php echo number_format($total_kredit,2);?></b></td>
                                <?php $saldo_akhir = ($saldo_awal_fix+$total_debit)-$total_kredit;?>
                                <td><b><?php echo number_format($saldo_akhir,2);?></b></td>
                            </tr>
                          </tbody>
                      </table>
                      <br />
                    <?php }
                      }
                      else{
                        /*************************************** FILTER PER AKUN *************************************************************/
                        echo "UNDER MAINTENANCE";
                        /**************************************** END FILTER PER AKUN *****************************************************************/
                      }
                       ?>
                  </div>
              </div>


          </div>
