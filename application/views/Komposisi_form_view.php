<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"></script>

<div class="portlet box blue ">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i> Form Komposisi</div>
            <div class="tools">
                <a href="" class="collapse"> </a>
                <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                <a href="" class="reload"> </a>
                <a href="" class="remove"> </a>
            </div>
        </div>
    </div>
    <div class="portlet-body form">
        <form role="form" method="post">
            <div class="form-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Item Baku</label>
                            <select id='item_baku' class='form-control input-sm select2-multiple' name='item_baku'>
                            <option value=0><?php echo " - Pilih Item - ";?></option>
                            <?php  foreach($list_item as $row){ echo '<option value='.$row['item_id'].'>'.$row['ntipe'].' - '.$row['nama'].'</option>';} ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Item Jadi</label>
                            <select id='item_jadi' class='form-control input-sm select2-multiple' name='item_jadi'>
                            <option value=0><?php echo " - Pilih Item - ";?></option>
                            <?php  foreach($list_item as $row){ echo '<option value='.$row['item_id'].'>'.$row['ntipe'].' - '.$row['nama'].'</option>';} ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Quantity</label>
                            <input type='text' class='form-control' placeholder="Quantity" name='qty' id='qty' required>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-actions">
                <button type="submit" class="btn red" name="submit_komposisi">Submit</button>
            </div>
        </form>
        
<table class="table table-striped table-bordered table-hover" id="sample_1">
        <thead>
          <tr>
              <th width='10%'><center>Type Item baku</center></th>
              <th width='25%'><center>Item Baku</center></th>
              <th width='10%'><center>Type Item jadi</center></th>
              <th width='25%'><center>Item Jadi</center></th>
              <th width='10%'><center>Quantity</center></th>
              <th width='20%'><center>Action</center></th>
          </tr>
        </thead>
        <tbody>
          <?php foreach($list_komposisi as $row){ ?>
            <tr>
                <td><small><?php echo $row['type_item_baku'];?></small></td>
                <td><small><?php echo $row['item_baku'];?></small></td>
                <td><small><?php echo $row['type_item_jadi'];?></small></td>
                <td><small><?php echo $row['item_jadi'];?></small></td>
                <td><small><?php echo $row['qty_item_baku'];?></small></td>
                <td>
                    <a href='<?php echo base_url('Komposisi_controller/edit/'.$row['komposisi_id'].'');?>' class='btn blue'><i class="fa fa-pencil"></i> </a>
                    <a href="javascript:dialogHapus('<?php echo base_url('Komposisi_controller/delete/'.$row['komposisi_id'].'');?>')" class='btn red'><i class="fa fa-trash-o"></i> </a>
                    <!-- <a href='<?php echo base_url('Purchase_order_controller/purchase_order_print/'.$row['purchase_header_id'].'');?>' class='btn yellow'><i class="fa fa-print"></i> </a></center> -->
                </td>
            </tr>
            <?php
              }
            ?>
        </tbody>
    </table>
    </div>
</div>


<script>
	function dialogHapus(urlHapus) {
	  if (confirm("Apakah anda yakin ingin menghapus ini ?")) {
		document.location = urlHapus;
	  }
	}
</script>