<h3>List Konversi Stock</h3>
<div class="portlet light bordered">
  <div class="portlet-title">
      <div class="tools"> </div>
  </div>

<table class="table table-striped table-bordered table-hover" id="sample_1">
        <thead>
          <tr>
              <th width='15%'><center>No Konversi</center></th>
              <th width='25%'><center>Item</center></th>
              <th width='15%'><center>Tanggal</center></th>
              <th width='10%'><center>Qty</center></th>
              <th width='10%'><center>Satuan Qty</center></th>
              <th width='25%'><center>Action</center></th>
          </tr>
        </thead>
        <tbody>
          <?php 	foreach($list_konversi_stok as $row){ ?>
            <tr>
                <td><small><center><?php echo $row['konversi_stok_no'];?></center></small></td>
                <td><small><?php echo $row['item_name'];?></small></td>
                <td><small><center><?php echo $row['konversi_stok_date'];?></center></small></td>
                <td><small><?php echo $row['qty'];?></small></td>
                <td><small><?php echo $row['satuan'];?></small></td>
                <?php
                  $konversi_stok_no = str_replace("/", "-", $row['konversi_stok_no']); //konfersi karena akan dianggap parameter
                ?>
                <td><center>
                  <?php if(helper_security("pembelian_edit") == 1){?>
                  <a href='<?php echo base_url('Konversi_stok_controller/edit/'.$row['konversi_stok_header_id'].'');?>' class='btn blue'><i class="fa fa-pencil"></i> </a>
                  <?php }?>
                  <?php if(helper_security("pembelian_delete") == 1){?>
                  <a href="javascript:dialogHapus('<?php echo base_url('Konversi_stok_controller/delete/'.$row['konversi_stok_header_id'].'/'.$konversi_stok_no.'');?>')" class='btn red'><i class="fa fa-trash-o"></i> </a>
                  <?php }?>
                  <a href='<?php echo base_url('Purchase_controller/purchase_print/'.$row['konversi_stok_header_id'].'');?>' class='btn yellow'><i class="fa fa-print"></i> </a></center></td>
            </tr>
            <?php
              }
            ?>
        </tbody>
    </table>
</div>

<script>
	function dialogHapus(urlHapus) {
	  if (confirm("Apakah anda yakin ingin menghapus ini ?")) {
		document.location = urlHapus;
	  }
	}
</script>
