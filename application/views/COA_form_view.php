<h2>Form Chart Of Account</h2>
<div class="portlet box blue ">
      <div class="portlet-title">
          <div class="caption">
              <i class="fa fa-gift"></i> Form Chart Of Acccount </div>
          <div class="tools">
              <a href="" class="collapse"> </a>
              <a href="#portlet-config" data-toggle="modal" class="config"> </a>
              <a href="" class="reload"> </a>
              <a href="" class="remove"> </a>
          </div>
      </div>
      <div class="portlet-body form">
          <form role="form" method="post">
              <div class="form-body">
                <div class="row">

                  <div class="col-sm-6">
                    <div class="form-group">
                        <label>Tipe Akun</label>
                        <div class="input-group">
                           <select id="select2-single-input-sm" class="form-control input-sm select2-multiple" name="coa_id_cash_bank" required>
                             <?php 	foreach($list_coa as $row){ ?>
                               <option value="<?php echo $row['tipe_coa_id'];?>"><?php echo $row['nama'];?></option>
                             <?php } ?>
                           </select>
                     </div>
                   </div>

                   <div class="row">
                        <div class="col-sm-6">
                          <div class="form-group">
                            <label>Nomor Akun</label>
                            <input type="text" class="form-control" placeholder="Nomor Akun" id="nomor_akun" name="nomor_akun" required value="<?=isset($default['nomor'])? $default['nomor'] : ""?>">
                          </div>
                        </div>

                        <div class="col-sm-6">
                          <div class="form-group">
                            <label>Nama Akun</label>
                            <input type="text" class="form-control" placeholder="Nama Akun" name="nama_akun" required value="<?=isset($default['nama'])? $default['nama'] : ""?>">
                          </div>
                        </div>
                   </div>

                   <div class="row">
                        <div class="col-sm-6">
                          <div class="form-group">
                            <label>Saldo Valas</label>
                            <input type="text" class="form-control" placeholder="valas" name="valas" value=0>
                          </div>
                        </div>

                        <div class="col-sm-6">
                          <div class="form-group">
                            <label>Saldo IDR</label>
                            <input type="text" class="form-control" placeholder="Saldo IDR" name="saldo" value=0 required>
                          </div>
                        </div>
                   </div>

                   <div class="form-group">
                         <label>Posisi Saldo</label>
                         <div class="mt-radio-inline">
                             <label class="mt-radio">
                                 <input type="radio" name="debetkredit" id="optionsRadios4" value="1" checked> Debit
                                 <span></span>
                             </label>
                             <label class="mt-radio">
                                 <input type="radio" name="debetkredit" id="optionsRadios5" value="0" > Credit
                                 <span></span>
                             </label>
                         </div>
                     </div>
                  </div>

                  <div class="col-sm-6"></div>


              </div>
            </div>

            <div class="form-actions">
                <a href='<?php echo base_url('COA_controller'); ?>' class='btn default'><i class="icon-arrow-left"></i> Back</a>
                <button type="submit" class="btn red" name="submit_coa"><i class="icon-note"></i> Save</button>
            </div>
          </form>
      </div>
    </div>
