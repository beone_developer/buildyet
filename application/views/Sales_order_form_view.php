<!-- BEGIN PAGE TITLE-->
<!-- END PAGE TITLE-->
<!-- END PAGE HEADER-->
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"></script>
<?php
$list_item = $this->db->query("SELECT * FROM public.beone_item");
?>

<div class="portlet box blue ">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-gift"></i> Form Sales Order
		</div>
		<div class="tools">
			<a href="" class="collapse"> </a>
			<a href="#portlet-config" data-toggle="modal" class="config"> </a>
			<a href="" class="reload"> </a>
			<a href="" class="remove"> </a>
		</div>
	</div>
	<div class="portlet-body form">

		<form role="form" method="post" onsubmit="return validasi_input(this)">
			<div class="form-body">
				<?php
				$error = $this->session->flashdata('error');
				if (isset($error)) {
					echo '<div class="alert alert-danger" role ="alert">' . $error . '</div>';
				}
				$success = $this->session->flashdata('success');
				if (isset($success)) {
					echo '<div class="alert alert-success" role ="alert">' . $success . '</div>';
				}
				?>
				<div class="row">
					<div class="col-sm-4">
						<?php
						/******************* GENERATE PO NUMBER ******************************/
						$tgl = date('m/d/Y');
						$thn = substr($tgl, 8, 2);
						$bln = substr($tgl, 0, 2);

						//LOOKUP KODE
						$lookup_kode = $this->db->query("SELECT * FROM public.beone_sales_header ORDER BY sales_header_id DESC LIMIT 1");
						$hasil_lookup_kode = $lookup_kode->row_array();

						//PENENTUAN BENTUK KODE
						$kode_awal = "SO/" . $thn . "/" . $bln . "/";

						$urutan = 0;
						if (isset($hasil_lookup_kode['sales_no'])) {
							$urutan = substr($hasil_lookup_kode['sales_no'], 10, 4);
						}
						$no_lanjutan = $urutan + 1;

						$digit = strlen($no_lanjutan);
						$jml_nol = 4 - $digit;

						$cetak_nol = "";

						for ($i = 1; $i <= $jml_nol; $i++) {
							$cetak_nol = $cetak_nol . "0";
						}
						$nomorpo = $kode_awal . $cetak_nol . $no_lanjutan;
						echo "<h3><b>" . $nomorpo . "</b></h3>";
						/*************************************************/
						?>
					</div>


					<div class="col-sm-8">
						<div class="form-group">
							<label>Tanggal</label>
							<div class="input-group">
                            <span class="input-group-addon input-circle-left">
                                <i class="fa fa-calendar"></i>
                            </span>
								<input
									class="form-control form-control-inline input-medium date-picker input-circle-right"
									size="16" type="text" name="tanggal" value="<?php echo date('m/d/Y'); ?>" readonly
									required/>
								<span class="help-block"></span>
							</div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-sm-4">
						<div class="form-group">
							<label>Customer</label>
							<div class="input-group">
								<select id="select2-single-input-sm" class="form-control input-sm select2-multiple"
										name="customer" required>
									<?php
									foreach ($list_customer

									as $row){
									?>
									<option
										value='<?php echo $row['custsup_id']; ?>'><?php echo $row['nama']; ?></opiton>
										<?php
										}
										?>
								</select>
							</div>
						</div>
					</div>

					<div class="col-sm-8">
						<div class="form-group">
							<label>Keterangan</label>
							<input type="text" class="form-control" placeholder="Keterangan" id="keterangan_header"
								   name="keterangan_header">
							<input type"text" name="nomor_so" value='<?php echo $nomorpo; ?>' hidden>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-sm-12">
						<div class="form-group">
							<label>PPN</label>
							<div class="mt-radio-inline">
								<label class="mt-radio">
									<input type="radio" name="ppn" id="optionsRadios4" value="0"
										   onchange="cekppn(value)" required> Tanpa PPN
									<span></span>
								</label>
								<label class="mt-radio">
									<input type="radio" name="ppn" id="optionsRadios5" value="1"
										   onchange="cekppn(value)" required> Belum PPN
									<span></span>
								</label>
								<label class="mt-radio">
									<input type="radio" name="ppn" id="optionsRadios6" value="2"
										   onchange="cekppn(value)" required> Termasuk PPN
									<span></span>
								</label>
							</div>
						</div>
					</div>
				</div>

				<hr
				/ style="border-color: #3598DC;">
				<table id="datatable" class="table striped hovered cell-hovered">
					<thead>
					<tr>
						<td width="5%">ID</td>
						<td width="20%">Item</td>
						<td width="15%">Qty</td>
						<td width="15%">Price</td>
						<td width="15%">Amount</td>
						<td width="5%"></td>
					</tr>
					</thead>
					<tbody id="container">

					</tbody>
				</table>

				<div class="row">
					<div class="col-sm-4"></div>
					<div class="col-sm-4"></div>
					<div class="col-sm-4">
						<input type="hidden" class="form-control" placeholder="Sub Total" name="subtotal_backup"
							   id="subtotal_backup" value="0" readonly>
						<input type="text" class="form-control" placeholder="Sub Total" name="subtotal" id="subtotal"
							   value="0" readonly>
						<br/>
						<input type="text" class="form-control" placeholder="PPN" name="ppn_value" id="ppn" readonly>
						<hr
						/ style="border-color: #3598DC;">
						<input type="text" class="form-control" placeholder="Grand Total" name="grandtotal"
							   id="grandtotal" readonly>
					</div>
				</div>


			</div>
			<div class="form-actions">
				<a class="btn blue" data-toggle="modal" href="#responsive"> Tambah Data </a>
				<?php if (helper_security("penjualan_add") == 1) { ?>
					<button type="submit" class="btn red" name="submit_sales">Submit</button>
				<?php } ?>
			</div>
		</form>
	</div>
</div>


<!--------------------------- MODAL ADD ITEM--------------------------------------------->
<?php
$list_item2 = $this->db->query("SELECT d.gudang_id, d.item_id, i.nama as nitem, SUM(qty_in) - SUM(qty_out) as jml_qty, d.nomor_transaksi
                                          FROM public.beone_gudang_detail d INNER JOIN public.beone_item i ON d.item_id = i.item_id
                                          WHERE d.flag = 1 AND d.gudang_id = 2 GROUP BY i.nama, d.item_id, d.gudang_id, d.nomor_transaksi");

$gudang = 2;

$gudang_asal = $this->db->query("SELECT * FROM public.beone_gudang WHERE gudang_id = " . intval($gudang));
$hasil_gudang_asal = $gudang_asal->row_array();
$gudang_id = $hasil_gudang_asal['gudang_id'];
$nama = $hasil_gudang_asal['nama'];
?>

<div id="responsive" class="modal fade" tabindex="-1" data-width="760">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
		<h4 class="modal-title">Detail Item</h4>
	</div>
	<div class="modal-body">
		<div class="row">
			<div class="col-sm-12">
				<!--<div name="txtItemAsal" id="txtItemAsal"><b>Item</b></div>-->
				<div class="form-group">
					<label>Item</label>
					<input type="hidden" class="form-control" name="nodoc" id="nodoc" required>
					<select id="item_modal" class="form-control input-sm s2-ajax"
							name="item_modal"
							data-s2-url="<?php echo base_url('Sales_order_controller/search_item'); ?>"
							onchange="copydoc();"
							required>
					</select>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label>Quantity</label>
					<input type='text' class='form-control' placeholder="Quantity" name='qty_modal' id='qty_modal'
						   onchange="autoAmount()" required>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label>Price</label>
					<input type='text' class='form-control' placeholder="Price" name='price_modal' id='price_modal'
						   onchange="autoAmount()" required>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label>Amount</label>
					<input type='text' class='form-control' placeholder="Amount" name='amount_modal' id='amount_modal'
						   required>
				</div>
			</div>
		</div>

	</div>
	<div class="modal-footer">
		<button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
		<button type="button" class="btn green" name="add_btn" id="add_btn">Insert</button>
	</div>
</div>

<script type="text/javascript">
	function validasi_input(form) {
		if (form.grandtotal.value == 0) {
			alert("Data detail penjualan belum ada...!!!");
			form.grandtotal.focus();
			return (false);
		}
		return (true);
	}
</script>


<script>
	$(document).ready(function () {
		var count = 0;

		$("#add_btn").click(function () {
			count += 1;

			var item = document.getElementById('item_modal');
			var qty = document.getElementById('qty_modal');
			var nodoc = document.getElementById('nodoc');
			var price = document.getElementById('price_modal');
			var amount = document.getElementById('amount_modal');
			var namaItem = $('#item_modal option:selected').text();


			$('#container').append(
				'<tr class="records" id="' + count + '">'
				+ '<td><input class="form-control" id="item_id_' + count + '" name="item_id_' + count + '" type="text" value="' + item.value + '" readonly></td>'
				+ '<td><input class="form-control" id="item_' + count + '" name="item_' + count + '" type="text" value="' + namaItem + '" readonly></td>'
				+ '<td><input class="form-control" id="qty_' + count + '" name="qty_' + count + '" type="text" value="' + qty.value + '" readonly></td>'
				+ '<td><input class="form-control" id="price_' + count + '" name="price_' + count + '" type="text" value="' + price.value + '" readonly></td>'
				+ '<td><input class="form-control" id="nodoc_' + count + '" name="nodoc_' + count + '" type="hidden" value="' + nodoc.value + '" readonly></td>'
				+ '<td><input class="form-control" id="amount_' + count + '" name="amount_' + count + '" type="text" value="' + amount.value + '" readonly></td>'
				+ '<td><input id="rows_' + count + '" name="rows[]" value="' + count + '" type="hidden"></td>'
				+ '<td><button type="button" class="btn red" onclick="hapus(' + count + ')">X</button></td></tr>'
			);

			autoSubtotal();
			eraseText();
			cekppn1();
			cekGrandTotal();
			$('#responsive').modal('hide');
		});

		$(".remove_item").live('click', function (ev) {
			if (ev.type == 'click') {
				$(this).parents(".records").fadeOut();
				$(this).parents(".records").remove();
			}
		});
	});


	function eraseText() {
		document.getElementById("item_modal").value = "";
		document.getElementById("qty_modal").value = "";
		document.getElementById("price_modal").value = "";
		document.getElementById("amount_modal").value = "";
	}


	function hapus(rowid) {
		autominSubtotal("amount_" + rowid);
		var row = document.getElementById(rowid);
		row.parentNode.removeChild(row);

		cekppn1();
		cekGrandTotal();
		//var amnt = document.getElementById("amount_"+rowid).value;
	}
</script>

<script type="text/javascript">
	var qty_modal = document.getElementById('qty_modal');
	qty_modal.addEventListener('keyup', function (e) {
		qty_modal.value = formatRupiah(this.value, 'Rp. ');
	});

	var price_modal = document.getElementById('price_modal');
	price_modal.addEventListener('keyup', function (e) {
		price_modal.value = formatRupiah(this.value, 'Rp. ');
	});

	var amount_modal = document.getElementById('amount_modal');
	amount_modal.addEventListener('keyup', function (e) {
		amount_modal.value = formatRupiah(this.value, 'Rp. ');
	});

	var subtotal = document.getElementById('subtotal');
	subtotal.addEventListener('keyup', function (e) {
		subtotal.value = formatRupiah(this.value, 'Rp. ');
	});

	var grandtotal = document.getElementById('grandtotal');
	grandtotal.addEventListener('keyup', function (e) {
		grandtotal.value = formatRupiah(this.value, 'Rp. ');
	});


	/* Fungsi formatRupiah */
	function formatRupiah(angka, prefix) {
		var number_string = angka.replace(/[^,\d]/g, '').toString(),
			split = number_string.split(','),
			sisa = split[0].length % 3,
			rupiah = split[0].substr(0, sisa),
			ribuan = split[0].substr(sisa).match(/\d{3}/gi);

		// tambahkan titik jika yang di input sudah menjadi angka ribuan
		if (ribuan) {
			separator = sisa ? '.' : '';
			rupiah += separator + ribuan.join('.');
		}

		rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
		return prefix == undefined ? rupiah : (rupiah ? rupiah : '');
	}
</script>

<script>
	function autoAmount() {
		var a = document.getElementById('qty_modal').value;
		var b = document.getElementById('price_modal').value;

		var c = a.split('.').join('');
		var d = b.split('.').join('');
		document.getElementById('amount_modal').value = c * d;
	}
</script>


<script>
	function autoSubtotal() {
		var a = document.getElementById('subtotal').value;
		var b = document.getElementById('amount_modal').value;

		var c_ = a.split('.').join('');
		var c = c_.split(',').join('.');

		var d_ = b.split('.').join('');
		var d = d_.split(',').join('.');

		document.getElementById('subtotal').value = (c * 1) + (d * 1);
		document.getElementById('subtotal_backup').value = (c * 1) + (d * 1);
	}

	function autominSubtotal(amnt) {
		var a = document.getElementById('subtotal').value;
		var b = document.getElementById(amnt).value;

		var c = a.split('.').join('');
		var d = b.split('.').join('');
		document.getElementById('subtotal').value = (c * 1) - (d * 1);
		document.getElementById('subtotal_backup').value = (c * 1) - (d * 1);
	}

	function cekppn(ppn) {
		var b = document.getElementById('subtotal').value;
		var bx = document.getElementById('subtotal_backup').value;
		var d = b.split('.').join('');
		var bxx = bx.split('.').join('');

		if (ppn == 0) {//tanpa ppn
			document.getElementById('ppn').value = d * 0;

			var zx = document.getElementById('subtotal_backup').value;
			document.getElementById('subtotal').value = zx;
		} else if (ppn == 1) {//menggunakan ppn
			var ddc = bxx * 0.1;
			document.getElementById('ppn').value = ddc;

			var zx = document.getElementById('subtotal_backup').value;
			document.getElementById('subtotal').value = zx;
		} else if (ppn == 2) {//include ppn
			var dd = Math.round(d / 1.1);
			var zz = Math.round((dd * 10) / 100);
			document.getElementById('subtotal').value = dd;
			document.getElementById('ppn').value = zz;
		}

		cekGrandTotal();
	}

	function cekppn1() {
		var ppn1 = document.getElementById('optionsRadios4').checked;
		var ppn2 = document.getElementById('optionsRadios5').checked;
		var ppn3 = document.getElementById('optionsRadios6').checked;


		var b = document.getElementById('subtotal').value;
		var d = b.split('.').join('');

		if (ppn1 == true) {
			document.getElementById('ppn').value = d * 0;
		} else if (ppn2 == true) {
			var ddc = d * 0.1;
			document.getElementById('ppn').value = ddc;
		} else if (ppn3 == true) {
			var dd = Math.round(d / 1.1);
			var zz = Math.round((dd * 10) / 100);
			document.getElementById('subtotal').value = dd;
			document.getElementById('ppn').value = zz;
		}
	}


	function cekGrandTotal() {
		var subtotal_ = document.getElementById('subtotal').value;
		var ppn_ = document.getElementById('ppn').value;

		var subtotal = subtotal_.split('.').join('');
		var ppn = ppn_.split('.').join('');

		var gt = (subtotal * 1) + (ppn * 1);
		var grandtotal = document.getElementById('grandtotal').value = gt;
		//var grandtotal = document.getElementById('grandtotal').value = gt.toLocaleString(undefined, {maximumFractionDigits:2});

	}

</script>

<script>
	function copydoc() {
		var namaItem = $('#item_modal option:selected').text();
		var nodoc = document.getElementById('nodoc').value = namaItem;
	}
</script>
