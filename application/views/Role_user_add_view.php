<!-- BEGIN PAGE TITLE-->
<!-- END PAGE TITLE-->
<!-- END PAGE HEADER-->
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"></script>

<div class="portlet light bordered">
  <div class="portlet-title">
          <div class="caption">
              <i class="fa fa-gift"></i> Role User</div>
          <div class="tools">
              <a href="" class="collapse"> </a>
              <a href="#portlet-config" data-toggle="modal" class="config"> </a>
              <a href="" class="reload"> </a>
              <a href="" class="remove"> </a>
          </div>
      </div>
      <div class="portlet-body form">

          <form role="form" method="post">
              <div class="form-body">
                  <div class="row">
                    <div class="col-sm-4">
                      <div class="form-group">
                          <label>Nama Role</label>
                          <input type="text" class="form-control" placeholder="Nama Role" id="nama_role" name="nama_role" value="<?=isset($default['nama_role'])? $default['nama_role'] : ""?>">
                        </div>
                    </div>

                      <div class="col-sm-8">
                        <div class="form-group">
                            <label>Keterangan</label>
                            <input type="text" class="form-control" placeholder="Keterangan" id="keterangan" name="keterangan" value="<?=isset($default['keterangan'])? $default['keterangan'] : ""?>">
                          </div>
                      </div>
                    </div>

                    <div class="form-actions">
                    <?php if(helper_security("role_add") == 1){?>
                    <button type="submit" class="btn red" name="submit_role">Simpan</button>
                    <?php }?>
                  </div>
              </div>
          </form>
  </div>

  <hr / style="border-color: #3598DC;">

  <table class="table table-striped table-bordered table-hover" id="sample_1">
          <thead>
            <tr>
                <th><center>Nama</center></th>
                <th><center>Keterangan</center></th>
                <th><center>Action</center></th>
            </tr>
          </thead>
          <tbody>
            <?php 	foreach($list_role as $row){ ?>
              <tr>
                  <td><?php echo $row['nama_role'];?></td>
                  <td><?php echo $row['keterangan'];?></td>
                  <td>
                      <?php if(helper_security("role_edit") == 1){?>
                      <a href='<?php echo base_url('User_controller/Role_user_edit/'.$row['role_id'].'');?>' class='btn blue'><i class="fa fa-pencil"></i></a>
                      <?php }?>
                      <?php if(helper_security("role_delete") == 1){?>
                      <a href="javascript:dialogHapus('<?php echo base_url('User_controller/Role_delete/'.$row['role_id'].'');?>')" class='btn red'><i class="fa fa-trash-o"></i></a></td>
                      <?php }?>
              </tr>
              <?php
                }
              ?>
          </tbody>
      </table>
</div>

<script>
	function dialogHapus(urlHapus) {
	  if (confirm("Apakah anda yakin ingin menghapus ini ?")) {
		document.location = urlHapus;
	  }
	}
</script>
