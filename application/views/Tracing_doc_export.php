<h3>Tracing Document Export</h3>
<div class="portlet light bordered">
  <div class="portlet-title">
      <div class="tools"> </div>
  </div>

<table class="table table-striped table-bordered table-hover" id="sample_1">
        <thead>
          <tr>
              <th width="5%"><center><small>No</small></center></th>
              <th width="10%"><center><small>Tanggal</small></center></th>
              <th width="15%"><center><small>User</small></center></th>
              <th width="15%"><center><small>Keterangan</small></center></th>
              <th width="10%"><center><small>Gudang</small></center></th>
              <th width="15%"><center><small>Item</small></center></th>
              <th width="10%"><center><small>Qty In</small></center></th>
              <th width="10%"><center><small>Qty Out</small></center></th>
          </tr>
        </thead>
        <tbody>
          <?php
                $no=0;
                $no_transaksi = "";
								foreach($list_tracing as $row){
                $no = $no + 1;
                $no_transaksi = $row['keterangan'];
					?>
            <tr>
                <td><small><?php echo $no;?></small></td>
								<td><small><?php echo $row['trans_date'];?></small></td>
                <td><small><?php echo $row['nuser'];?></small></td>
                <td><small><?php echo $row['nomor_transaksi'];?></small></td>
								<td><small><?php echo $row['ngudang'];?></small></td>
                <td><small><?php echo $row['nitem'];?></small></td>
								<td><small><?php echo number_format($row['qty_in'],2);?></small></td>
                <td><small><?php echo number_format($row['qty_out'],2);?></small></td>
            </tr>
            <?php
              }


              $no_transaksi_wip = "";
              $sqlwip = $this->db->query("SELECT d.gudang_detail_id, d.gudang_id, gd.nama as ngudang, d.trans_date, d.item_id, i.nama as nitem, d.qty_in, d.qty_out, d.nomor_transaksi, d.update_by, u.nama as nuser, d.update_date, d.flag, d.keterangan
          															FROM public.beone_gudang_detail d INNER JOIN public.beone_gudang gd ON d.gudang_id = gd.gudang_id INNER JOIN public.beone_item i ON d.item_id = i.item_id INNER JOIN public.beone_user u ON d.update_by = u.user_id
          															WHERE d.nomor_transaksi = '$no_transaksi' ORDER BY trans_date asc, gudang_detail_id asc");
              foreach($sqlwip->result_array() as $row2){
              $no = $no + 1;
              $no_transaksi_wip = $row2['keterangan'];
            ?>
            <tr>
                <td><small><?php echo $no;?></small></td>
                <td><small><?php echo $row2['trans_date'];?></small></td>
                <td><small><?php echo $row2['nuser'];?></small></td>
                <td><small><?php echo $row2['nomor_transaksi'];?></small></td>
                <td><small><?php echo $row2['ngudang'];?></small></td>
                <td><small><?php echo $row2['nitem'];?></small></td>
                <td><small><?php echo number_format($row2['qty_in'],2);?></small></td>
                <td><small><?php echo number_format($row2['qty_out'],2);?></small></td>
            </tr>
            <?php
              }
              $no_transaksi_bb = "";
              $sqlbj = $this->db->query("SELECT d.gudang_detail_id, d.gudang_id, gd.nama as ngudang, d.trans_date, d.item_id, i.nama as nitem, d.qty_in, d.qty_out, d.nomor_transaksi, d.update_by, u.nama as nuser, d.update_date, d.flag, d.keterangan
          															FROM public.beone_gudang_detail d INNER JOIN public.beone_gudang gd ON d.gudang_id = gd.gudang_id INNER JOIN public.beone_item i ON d.item_id = i.item_id INNER JOIN public.beone_user u ON d.update_by = u.user_id
          															WHERE d.nomor_transaksi = '$no_transaksi_wip' ORDER BY trans_date asc, gudang_detail_id asc");
              foreach($sqlbj->result_array() as $row3){
                $no = $no + 1;
                $no_transaksi_bb = $row3['keterangan'];
            ?>

            <tr>
                <td><small><?php echo $no;?></small></td>
                <td><small><?php echo $row3['trans_date'];?></small></td>
                <td><small><?php echo $row3['nuser'];?></small></td>
                <td><small><?php echo $row3['nomor_transaksi'];?></small></td>
                <td><small><?php echo $row3['ngudang'];?></small></td>
                <td><small><?php echo $row3['nitem'];?></small></td>
                <td><small><?php echo number_format($row3['qty_in'],2);?></small></td>
                <td><small><?php echo number_format($row3['qty_out'],2);?></small></td>
            </tr>

            <?php
              }
            ?>
        </tbody>
    </table>
</div>
