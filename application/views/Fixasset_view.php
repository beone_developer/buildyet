<h3>Master Fixed Asset <b><?=$tipe?></b></h3>
<div class="portlet light bordered">
  <div class="portlet-title">


    <form role="form" method="post">
        <div class="form-body">
          <div class="row">
            <div class="col-sm-4">
                <label>Nama Asset</label>
                <input type="text" class="form-control" placeholder="Nama Asset" id="nama_asset" value="<?=isset($default['nama_asset'])? $default['nama_asset'] : ""?>" name="nama_asset" required>
            </div>
            <div class="col-sm-4">
              <div class="form-group">
                    <label>Tanggal Perolehan</label>
                    <div class="input-group">
                      <span class="input-group-addon input-circle-left">
                          <i class="fa fa-calendar"></i>
                      </span>
                      <input class="form-control form-control-inline input-medium date-picker input-circle-right" size="16" type="text" name="tanggal_perolehan" value="<?php echo date('d/m/Y');?>" readonly required/>
                      <span class="help-block"></span>
                    </div>
              </div>
            </div>
            <div class="col-sm-4">
                <label>Harga Perolehan</label>
                <input type="text" class="form-control" placeholder="Harga Perolehan" id="harga_perolehan" value="<?=isset($default['harga_perolehan'])? $default['harga_perolehan'] : ""?>" name="harga_perolehan" required>
            </div>
            </div>
            </div>
        <br />
        <div class="form-actions">
            <a href='<?php echo base_url('Fixasset_controller/fixasset');?>' class='btn default'> Cancel</a>
            <button type="submit" class="btn blue" name="submit_fixasset">Submit</button>
        </div>
      </form>

      <hr />
      <br />

      <div class="tools"> </div>
  </div>

<table class="table table-striped table-bordered table-hover" id="sample_1">
        <thead>
          <tr>
              <th><center>Nama Asset</center></th>
              <th><center>Tanggal Perolehan</center></th>
              <th><center>Harga Perolehan</center></th>
              <th><center>Action</center></th>
          </tr>
        </thead>
        <tbody>
          <?php 	foreach($list_fixasset as $row){ ?>
            <tr>
                <td><?php echo $row['nama_asset'];?></td>
                <td><?php echo $row['tanggal_perolehan'];?></td>
                <td><?php echo number_format($row['harga_perolehan'],2);?></td>
                <td>
                    <a href='<?php echo base_url('Fixasset_controller/Edit_fixasset/'.$row['fix_asset_id'].'');?>' class='btn blue'><i class="fa fa-pencil"></i></a>
                    <a href="javascript:dialogHapus('<?php echo base_url('Fixasset_controller/delete_fixasset/'.$row['fix_asset_id'].'');?>')" class='btn red'><i class="fa fa-trash-o"></i></a>
                </td>
            </tr>
            <?php
              }
            ?>
        </tbody>
    </table>
</div>

<script>
	function dialogHapus(urlHapus) {
	  if (confirm("Apakah anda yakin ingin menghapus ini ?")) {
		document.location = urlHapus;
	  }
	}
</script>


<script type="text/javascript">

var harga_perolehan = document.getElementById('harga_perolehan');
  harga_perolehan.addEventListener('keyup', function(e){
  harga_perolehan.value = formatRupiah(this.value, 'Rp. ');
});

/* Fungsi formatRupiah */
function formatRupiah(angka, prefix){
  var number_string = angka.replace(/[^,\d]/g, '').toString(),
  split   		= number_string.split(','),
  sisa     		= split[0].length % 3,
  rupiah     		= split[0].substr(0, sisa),
  ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);

  // tambahkan titik jika yang di input sudah menjadi angka ribuan
  if(ribuan){
    separator = sisa ? '.' : '';
    rupiah += separator + ribuan.join('.');
  }

  rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
  return prefix == undefined ? rupiah : (rupiah ? rupiah : '');
}
</script>
