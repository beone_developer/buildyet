<!-- BEGIN PAGE TITLE-->
<!-- END PAGE TITLE-->
<!-- END PAGE HEADER-->
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"></script>

<div class="portlet light bordered">
  <div class="portlet-title">
          <div class="caption">
              <i class="fa fa-gift"></i> Role User</div>
          <div class="tools">
              <a href="" class="collapse"> </a>
              <a href="#portlet-config" data-toggle="modal" class="config"> </a>
              <a href="" class="reload"> </a>
              <a href="" class="remove"> </a>
          </div>
      </div>
      <div class="portlet-body form">

          <form role="form" method="post">
              <div class="form-body">
                  <div class="row">
                    <div class="col-sm-4">
                      <div class="form-group">
                          <label>Nama Role</label>
                          <input type="text" class="form-control" placeholder="Nama Role" id="nama_role" name="nama_role" value="<?=isset($default['nama_role'])? $default['nama_role'] : ""?>">
                        </div>
                    </div>

                      <div class="col-sm-8">
                        <div class="form-group">
                            <label>Keterangan</label>
                            <input type="text" class="form-control" placeholder="Keterangan" id="keterangan" name="keterangan" value="<?=isset($default['keterangan'])? $default['keterangan'] : ""?>">
                          </div>
                      </div>
                    </div>

                      <button type="submit" class="btn red" name="update_role">Update</button>

                    <hr / style="border-color: #3598DC;">

                    <div class="row">
                        <div class="col-sm-3">
                          <!------------------- MASTER MENU ---------------------------------->
                          <?php if ($default['master_menu'] == 0){?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'master_menu'.'');?>" class='btn-block btn red btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">MASTER MENU<br /><small>(blok akses)</small></a>
                          <?php }else{?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'master_menu'.'');?>" class='btn-block btn blue btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">MASTER MENU<br /><small>(akses)</small></a>
                          <?php }?>
                          <!------------------- END MASTER_MENU---------------------------------->
                        </div>

                        <div class="col-sm-3">
                          <!------------------- Master User Add ---------------------------------->
                          <?php if ($default['user_add'] == 0){?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'user_add'.'');?>" class='btn-block btn red btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Master User Add<br /><small>(blok akses)</small></a>
                          <?php }else{?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'user_add'.'');?>" class='btn-block btn blue btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Master User Add<br /><small>(akses)</small></a>
                          <?php }?>
                          <!------------------- END Master User Add---------------------------------->
                        </div>

                        <div class="col-sm-3">
                          <!------------------- Master User Edit ---------------------------------->
                          <?php if ($default['user_edit'] == 0){?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'user_edit'.'');?>" class='btn-block btn red btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Master User Edit<br /><small>(blok akses)</small></a>
                          <?php }else{?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'user_edit'.'');?>" class='btn-block btn blue btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Master User Edit<br /><small>(akses)</small></a>
                          <?php }?>
                          <!------------------- END Master User Edit---------------------------------->
                        </div>

                        <div class="col-sm-3">
                          <!------------------- Master User Delete ---------------------------------->
                          <?php if ($default['user_delete'] == 0){?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'user_delete'.'');?>" class='btn-block btn red btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Master User Delete<br /><small>(blok akses)</small></a>
                          <?php }else{?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'user_delete'.'');?>" class='btn-block btn blue btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Master User Delete<br /><small>(akses)</small></a>
                          <?php }?>
                          <!------------------- END Master User Delete ---------------------------------->
                        </div>
                    </div>

                    <br />

                    <div class="row">
                        <div class="col-sm-3"></div>

                        <div class="col-sm-3">
                          <!------------------- Master Role Add ---------------------------------->
                          <?php if ($default['role_add'] == 0){?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'role_add'.'');?>" class='btn-block btn red btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Master Role Add<br /><small>(blok akses)</small></a>
                          <?php }else{?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'role_add'.'');?>" class='btn-block btn blue btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Master Role Add<br /><small>(akses)</small></a>
                          <?php }?>
                          <!------------------- END Master Role Add ---------------------------------->
                        </div>

                        <div class="col-sm-3">
                          <!------------------- Master Role Edit ---------------------------------->
                          <?php if ($default['role_edit'] == 0){?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'role_edit'.'');?>" class='btn-block btn red btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Master Role Edit<br /><small>(blok akses)</small></a>
                          <?php }else{?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'role_edit'.'');?>" class='btn-block btn blue btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Master Role Edit<br /><small>(akses)</small></a>
                          <?php }?>
                          <!------------------- END Master Role Edit ---------------------------------->
                        </div>

                        <div class="col-sm-3">
                          <!------------------- Master Role Delete ---------------------------------->
                          <?php if ($default['role_delete'] == 0){?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'role_delete'.'');?>" class='btn-block btn red btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Master Role Delete<br /><small>(blok akses)</small></a>
                          <?php }else{?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'role_delete'.'');?>" class='btn-block btn blue btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Master Role Delete<br /><small>(akses)</small></a>
                          <?php }?>
                          <!------------------- END Master Role Delete ---------------------------------->
                        </div>

                    </div>

                    <br />

                    <div class="row">
                        <div class="col-sm-3"></div>

                        <div class="col-sm-3">
                          <!------------------- Master Customer Add ---------------------------------->
                          <?php if ($default['customer_add'] == 0){?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'customer_add'.'');?>" class='btn-block btn red btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Master Customer Add<br /><small>(blok akses)</small></a>
                          <?php }else{?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'customer_add'.'');?>" class='btn-block btn blue btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Master Customer Add<br /><small>(akses)</small></a>
                          <?php }?>
                          <!------------------- END Master Customer Add ---------------------------------->
                        </div>

                        <div class="col-sm-3">
                          <!------------------- Master Customer Edit ---------------------------------->
                          <?php if ($default['customer_edit'] == 0){?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'customer_edit'.'');?>" class='btn-block btn red btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Master Customer Edit<br /><small>(blok akses)</small></a>
                          <?php }else{?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'customer_edit'.'');?>" class='btn-block btn blue btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Master Customer Edit<br /><small>(akses)</small></a>
                          <?php }?>
                          <!------------------- END Master Customer Edit ---------------------------------->
                        </div>

                        <div class="col-sm-3">
                          <!------------------- Master Customer Delete ---------------------------------->
                          <?php if ($default['customer_delete'] == 0){?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'customer_delete'.'');?>" class='btn-block btn red btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Master Customer Delete<br /><small>(blok akses)</small></a>
                          <?php }else{?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'customer_delete'.'');?>" class='btn-block btn blue btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Master Customer Delete<br /><small>(akses)</small></a>
                          <?php }?>
                          <!------------------- END Master Customer Delete ---------------------------------->
                        </div>

                    </div>

                    <br />

                    <div class="row">
                        <div class="col-sm-3"></div>

                        <div class="col-sm-3">
                          <!------------------- Master Supplier Add ---------------------------------->
                          <?php if ($default['supplier_add'] == 0){?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'supplier_add'.'');?>" class='btn-block btn red btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Master Supplier Add<br /><small>(blok akses)</small></a>
                          <?php }else{?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'supplier_add'.'');?>" class='btn-block btn blue btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Master Supplier Add<br /><small>(akses)</small></a>
                          <?php }?>
                          <!------------------- END Master Supplier Add ---------------------------------->
                        </div>

                        <div class="col-sm-3">
                          <!------------------- Master Supplier Edit ---------------------------------->
                          <?php if ($default['supplier_edit'] == 0){?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'supplier_edit'.'');?>" class='btn-block btn red btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Master Supplier Edit<br /><small>(blok akses)</small></a>
                          <?php }else{?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'supplier_edit'.'');?>" class='btn-block btn blue btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Master Supplier Edit<br /><small>(akses)</small></a>
                          <?php }?>
                          <!------------------- END Master Supplier Edit ---------------------------------->
                        </div>

                        <div class="col-sm-3">
                          <!------------------- Master Supplier Delete ---------------------------------->
                          <?php if ($default['supplier_delete'] == 0){?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'supplier_delete'.'');?>" class='btn-block btn red btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Master Supplier Delete<br /><small>(blok akses)</small></a>
                          <?php }else{?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'supplier_delete'.'');?>" class='btn-block btn blue btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Master Supplier Delete<br /><small>(akses)</small></a>
                          <?php }?>
                          <!------------------- END Master Supplier Delete ---------------------------------->
                        </div>

                    </div>

                    <br />

                    <div class="row">
                        <div class="col-sm-3"></div>

                        <div class="col-sm-3">
                          <!------------------- Master Item Add ---------------------------------->
                          <?php if ($default['item_add'] == 0){?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'item_add'.'');?>" class='btn-block btn red btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Master Item Add<br /><small>(blok akses)</small></a>
                          <?php }else{?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'item_add'.'');?>" class='btn-block btn blue btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Master Item Add<br /><small>(akses)</small></a>
                          <?php }?>
                          <!------------------- END Master Item Add ---------------------------------->
                        </div>

                        <div class="col-sm-3">
                          <!------------------- Master Item Edit ---------------------------------->
                          <?php if ($default['item_edit'] == 0){?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'item_edit'.'');?>" class='btn-block btn red btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Master Item Edit<br /><small>(blok akses)</small></a>
                          <?php }else{?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'item_edit'.'');?>" class='btn-block btn blue btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Master Item Edit<br /><small>(akses)</small></a>
                          <?php }?>
                          <!------------------- END Master Item Edit ---------------------------------->
                        </div>

                        <div class="col-sm-3">
                          <!------------------- Master Item Delete ---------------------------------->
                          <?php if ($default['item_delete'] == 0){?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'item_delete'.'');?>" class='btn-block btn red btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Master Item Delete<br /><small>(blok akses)</small></a>
                          <?php }else{?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'item_delete'.'');?>" class='btn-block btn blue btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Master Item Delete<br /><small>(akses)</small></a>
                          <?php }?>
                          <!------------------- END Master Item Delete ---------------------------------->
                        </div>

                    </div>

                    <br />

                    <div class="row">
                        <div class="col-sm-3"></div>

                        <div class="col-sm-3">
                          <!------------------- Master Jenis Add ---------------------------------->
                          <?php if ($default['jenis_add'] == 0){?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'jenis_add'.'');?>" class='btn-block btn red btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Master Jenis Add<br /><small>(blok akses)</small></a>
                          <?php }else{?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'jenis_add'.'');?>" class='btn-block btn blue btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Master Jenis Add<br /><small>(akses)</small></a>
                          <?php }?>
                          <!------------------- END Master Jenis Add ---------------------------------->
                        </div>

                        <div class="col-sm-3">
                          <!------------------- Master Jenis Edit ---------------------------------->
                          <?php if ($default['jenis_edit'] == 0){?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'jenis_edit'.'');?>" class='btn-block btn red btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Master Jenis Edit<br /><small>(blok akses)</small></a>
                          <?php }else{?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'jenis_edit'.'');?>" class='btn-block btn blue btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Master Jenis Edit<br /><small>(akses)</small></a>
                          <?php }?>
                          <!------------------- END Master Jenis Edit ---------------------------------->
                        </div>

                        <div class="col-sm-3">
                          <!------------------- Master Jenis Delete ---------------------------------->
                          <?php if ($default['jenis_delete'] == 0){?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'jenis_delete'.'');?>" class='btn-block btn red btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Master Jenis Delete<br /><small>(blok akses)</small></a>
                          <?php }else{?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'jenis_delete'.'');?>" class='btn-block btn blue btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Master Jenis Delete<br /><small>(akses)</small></a>
                          <?php }?>
                          <!------------------- END Master Jenis Delete ---------------------------------->
                        </div>

                    </div>

                    <br />

                    <div class="row">
                        <div class="col-sm-3"></div>

                        <div class="col-sm-3">
                          <!------------------- Master Satuan Add ---------------------------------->
                          <?php if ($default['satuan_add'] == 0){?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'satuan_add'.'');?>" class='btn-block btn red btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Master Satuan Add<br /><small>(blok akses)</small></a>
                          <?php }else{?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'satuan_add'.'');?>" class='btn-block btn blue btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Master Satuan Add<br /><small>(akses)</small></a>
                          <?php }?>
                          <!------------------- END Master Satuan Add ---------------------------------->
                        </div>

                        <div class="col-sm-3">
                          <!------------------- Master Satuan Edit ---------------------------------->
                          <?php if ($default['satuan_edit'] == 0){?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'satuan_edit'.'');?>" class='btn-block btn red btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Master Satuan Edit<br /><small>(blok akses)</small></a>
                          <?php }else{?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'satuan_edit'.'');?>" class='btn-block btn blue btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Master Satuan Edit<br /><small>(akses)</small></a>
                          <?php }?>
                          <!------------------- END Master Satuan Edit ---------------------------------->
                        </div>

                        <div class="col-sm-3">
                          <!------------------- Master Satuan Delete ---------------------------------->
                          <?php if ($default['satuan_delete'] == 0){?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'satuan_delete'.'');?>" class='btn-block btn red btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Master Satuan Delete<br /><small>(blok akses)</small></a>
                          <?php }else{?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'satuan_delete'.'');?>" class='btn-block btn blue btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Master Satuan Delete<br /><small>(akses)</small></a>
                          <?php }?>
                          <!------------------- END Master Satuan Delete ---------------------------------->
                        </div>

                    </div>

                    <br />

                    <div class="row">
                        <div class="col-sm-3"></div>

                        <div class="col-sm-3">
                          <!------------------- Master Gudang Add ---------------------------------->
                          <?php if ($default['gudang_add'] == 0){?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'gudang_add'.'');?>" class='btn-block btn red btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Master Gudang Add<br /><small>(blok akses)</small></a>
                          <?php }else{?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'gudang_add'.'');?>" class='btn-block btn blue btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Master Gudang Add<br /><small>(akses)</small></a>
                          <?php }?>
                          <!------------------- END Master Gudang Add ---------------------------------->
                        </div>

                        <div class="col-sm-3">
                          <!------------------- Master Gudang Edit ---------------------------------->
                          <?php if ($default['gudang_edit'] == 0){?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'gudang_edit'.'');?>" class='btn-block btn red btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Master Gudang Edit<br /><small>(blok akses)</small></a>
                          <?php }else{?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'gudang_edit'.'');?>" class='btn-block btn blue btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Master Gudang Edit<br /><small>(akses)</small></a>
                          <?php }?>
                          <!------------------- END Master Gudang Edit ---------------------------------->
                        </div>

                        <div class="col-sm-3">
                          <!------------------- Master Gudang Delete ---------------------------------->
                          <?php if ($default['gudang_delete'] == 0){?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'gudang_delete'.'');?>" class='btn-block btn red btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Master Gudang Delete<br /><small>(blok akses)</small></a>
                          <?php }else{?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'gudang_delete'.'');?>" class='btn-block btn blue btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Master Gudang Delete<br /><small>(akses)</small></a>
                          <?php }?>
                          <!------------------- END Master Gudang Delete ---------------------------------->
                        </div>

                    </div>

                    <br />

                    <div class="row">
                        <div class="col-sm-3">
                          <!------------------- Master Menu Import  ---------------------------------->
                          <?php if ($default['master_import'] == 0){?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'master_import'.'');?>" class='btn-block btn red btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Menu Import<br /><small>(blok akses)</small></a>
                          <?php }else{?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'master_import'.'');?>" class='btn-block btn blue btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Menu Import <br /><small>(akses)</small></a>
                          <?php }?>
                          <!------------------- END Master Menu Import  ---------------------------------->
                        </div>

                        <div class="col-sm-3">
                          <!------------------- Master PO Add ---------------------------------->
                          <?php if ($default['po_add'] == 0){?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'po_add'.'');?>" class='btn-block btn red btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">PO Add<br /><small>(blok akses)</small></a>
                          <?php }else{?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'po_add'.'');?>" class='btn-block btn blue btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">PO Add<br /><small>(akses)</small></a>
                          <?php }?>
                          <!------------------- END Master PO Add ---------------------------------->
                        </div>

                        <div class="col-sm-3">
                          <!------------------- Master PO Edit ---------------------------------->
                          <?php if ($default['po_edit'] == 0){?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'po_edit'.'');?>" class='btn-block btn red btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">PO Edit<br /><small>(blok akses)</small></a>
                          <?php }else{?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'po_edit'.'');?>" class='btn-block btn blue btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">PO Edit<br /><small>(akses)</small></a>
                          <?php }?>
                          <!------------------- END Master PO Edit ---------------------------------->
                        </div>

                        <div class="col-sm-3">
                          <!------------------- Master PO Delete ---------------------------------->
                          <?php if ($default['po_delete'] == 0){?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'po_delete'.'');?>" class='btn-block btn red btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">PO Delete<br /><small>(blok akses)</small></a>
                          <?php }else{?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'po_delete'.'');?>" class='btn-block btn blue btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">PO Delete<br /><small>(akses)</small></a>
                          <?php }?>
                          <!------------------- END Master PO Delete ---------------------------------->
                        </div>

                    </div>

                    <br />

                    <div class="row">
                        <div class="col-sm-3"></div>

                        <div class="col-sm-3">
                          <!------------------- Tracing ---------------------------------->
                          <?php if ($default['tracing'] == 0){?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'tracing'.'');?>" class='btn-block btn red btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Tracing<br /><small>(blok akses)</small></a>
                          <?php }else{?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'tracing'.'');?>" class='btn-block btn blue btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Tracing<br /><small>(akses)</small></a>
                          <?php }?>
                          <!------------------- END Tracing ---------------------------------->
                        </div>

                        <div class="col-sm-3">
                          <!------------------- Terima Barang ---------------------------------->
                          <?php if ($default['terima_barang'] == 0){?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'terima_barang'.'');?>" class='btn-block btn red btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Terima Barang<br /><small>(blok akses)</small></a>
                          <?php }else{?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'terima_barang'.'');?>" class='btn-block btn blue btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Terima Barang<br /><small>(akses)</small></a>
                          <?php }?>
                          <!------------------- END Terima Barang ---------------------------------->
                        </div>

                        <div class="col-sm-3"></div>

                    </div>

                    <br />

                    <div class="row">
                        <div class="col-sm-3">
                          <!------------------- Master Menu Eksport  ---------------------------------->
                          <?php if ($default['master_eksport'] == 0){?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'master_eksport'.'');?>" class='btn-block btn red btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Menu Eksport<br /><small>(blok akses)</small></a>
                          <?php }else{?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'master_eksport'.'');?>" class='btn-block btn blue btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Menu Eksport <br /><small>(akses)</small></a>
                          <?php }?>
                          <!------------------- END Master Menu Eksport  ---------------------------------->
                        </div>

                        <div class="col-sm-3">
                          <!------------------- Master Eksport Add ---------------------------------->
                          <?php if ($default['eksport_add'] == 0){?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'eksport_add'.'');?>" class='btn-block btn red btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Eksport Add<br /><small>(blok akses)</small></a>
                          <?php }else{?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'eksport_add'.'');?>" class='btn-block btn blue btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Eksport Add<br /><small>(akses)</small></a>
                          <?php }?>
                          <!------------------- END Master Eksport Add ---------------------------------->
                        </div>

                        <div class="col-sm-3">
                          <!------------------- Master Eksport Edit ---------------------------------->
                          <?php if ($default['eksport_edit'] == 0){?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'eksport_edit'.'');?>" class='btn-block btn red btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Eksport Edit<br /><small>(blok akses)</small></a>
                          <?php }else{?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'eksport_edit'.'');?>" class='btn-block btn blue btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Eksport Edit<br /><small>(akses)</small></a>
                          <?php }?>
                          <!------------------- END Master Eksport Edit ---------------------------------->
                        </div>

                        <div class="col-sm-3">
                          <!------------------- Master Eksport Delete ---------------------------------->
                          <?php if ($default['eksport_delete'] == 0){?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'eksport_delete'.'');?>" class='btn-block btn red btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Eksport Delete<br /><small>(blok akses)</small></a>
                          <?php }else{?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'eksport_delete'.'');?>" class='btn-block btn blue btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Eksport Delete<br /><small>(akses)</small></a>
                          <?php }?>
                          <!------------------- END Master Eksport Delete ---------------------------------->
                        </div>

                    </div>

                    <br />

                    <div class="row">
                        <div class="col-sm-3"></div>

                        <div class="col-sm-3">
                          <!------------------- Kirim Barang ---------------------------------->
                          <?php if ($default['kirim_barang'] == 0){?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'kirim_barang'.'');?>" class='btn-block btn red btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Kirim Barang<br /><small>(blok akses)</small></a>
                          <?php }else{?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'kirim_barang'.'');?>" class='btn-block btn blue btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Kirim Barang<br /><small>(akses)</small></a>
                          <?php }?>
                          <!------------------- END Kirim Barang ---------------------------------->
                        </div>

                        <div class="col-sm-3"></div>

                        <div class="col-sm-3"></div>

                    </div>

                    <br />

                    <div class="row">
                        <div class="col-sm-3">
                          <!------------------- Master Menu Pembelian  ---------------------------------->
                          <?php if ($default['master_pembelian'] == 0){?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'master_pembelian'.'');?>" class='btn-block btn red btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Menu Pembelian<br /><small>(blok akses)</small></a>
                          <?php }else{?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'master_pembelian'.'');?>" class='btn-block btn blue btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Menu Pembelian <br /><small>(akses)</small></a>
                          <?php }?>
                          <!------------------- END Master Menu Pembelian  ---------------------------------->
                        </div>

                        <div class="col-sm-3">
                          <!------------------- Master Pembelian Add ---------------------------------->
                          <?php if ($default['pembelian_add'] == 0){?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'pembelian_add'.'');?>" class='btn-block btn red btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Pembelian Add<br /><small>(blok akses)</small></a>
                          <?php }else{?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'pembelian_add'.'');?>" class='btn-block btn blue btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Pembelian Add<br /><small>(akses)</small></a>
                          <?php }?>
                          <!------------------- END Master Pembelian Add ---------------------------------->
                        </div>

                        <div class="col-sm-3">
                          <!------------------- Master Pembelian Edit ---------------------------------->
                          <?php if ($default['pembelian_edit'] == 0){?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'pembelian_edit'.'');?>" class='btn-block btn red btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Pembelian Edit<br /><small>(blok akses)</small></a>
                          <?php }else{?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'pembelian_edit'.'');?>" class='btn-block btn blue btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Pembelian Edit<br /><small>(akses)</small></a>
                          <?php }?>
                          <!------------------- END Master Pembelian Edit ---------------------------------->
                        </div>

                        <div class="col-sm-3">
                          <!------------------- Master Pembelian Delete ---------------------------------->
                          <?php if ($default['pembelian_delete'] == 0){?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'pembelian_delete'.'');?>" class='btn-block btn red btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Pembelian Delete<br /><small>(blok akses)</small></a>
                          <?php }else{?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'pembelian_delete'.'');?>" class='btn-block btn blue btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Pembelian Delete<br /><small>(akses)</small></a>
                          <?php }?>
                          <!------------------- END Master PO Delete ---------------------------------->
                        </div>

                    </div>

                    <br />

                    <div class="row">
                        <div class="col-sm-3"></div>

                        <div class="col-sm-3">
                          <!------------------- Master Kredit Note Add ---------------------------------->
                          <?php if ($default['kredit_note_add'] == 0){?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'kredit_note_add'.'');?>" class='btn-block btn red btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Kredit Note Add<br /><small>(blok akses)</small></a>
                          <?php }else{?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'kredit_note_add'.'');?>" class='btn-block btn blue btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Kredit Note Add<br /><small>(akses)</small></a>
                          <?php }?>
                          <!------------------- END Master Kredit Note Add ---------------------------------->
                        </div>

                        <div class="col-sm-3">
                          <!------------------- Master Kredit Note Edit ---------------------------------->
                          <?php if ($default['kredit_note_edit'] == 0){?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'kredit_note_edit'.'');?>" class='btn-block btn red btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Kredit Note Edit<br /><small>(blok akses)</small></a>
                          <?php }else{?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'kredit_note_edit'.'');?>" class='btn-block btn blue btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Kredit Note Edit<br /><small>(akses)</small></a>
                          <?php }?>
                          <!------------------- END Master Kredit Note Edit ---------------------------------->
                        </div>

                        <div class="col-sm-3">
                          <!------------------- Master Kredit Note Delete ---------------------------------->
                          <?php if ($default['kredit_note_delete'] == 0){?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'kredit_note_delete'.'');?>" class='btn-block btn red btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Kredit Note Delete<br /><small>(blok akses)</small></a>
                          <?php }else{?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'kredit_note_delete'.'');?>" class='btn-block btn blue btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Kredit Note Delete<br /><small>(akses)</small></a>
                          <?php }?>
                          <!------------------- END Master Kredit Note Delete ---------------------------------->
                        </div>

                    </div>

                    <br />

                    <div class="row">
                        <div class="col-sm-3">
                          <!------------------- Master Menu Penjualan  ---------------------------------->
                          <?php if ($default['menu_penjualan'] == 0){?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'menu_penjualan'.'');?>" class='btn-block btn red btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Menu Penjualan<br /><small>(blok akses)</small></a>
                          <?php }else{?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'menu_penjualan'.'');?>" class='btn-block btn blue btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Menu Penjualan <br /><small>(akses)</small></a>
                          <?php }?>
                          <!------------------- END Master Menu Penjualan  ---------------------------------->
                        </div>

                        <div class="col-sm-3">
                          <!------------------- Master Penjualan Add ---------------------------------->
                          <?php if ($default['penjualan_add'] == 0){?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'penjualan_add'.'');?>" class='btn-block btn red btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Penjualan Add<br /><small>(blok akses)</small></a>
                          <?php }else{?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'penjualan_add'.'');?>" class='btn-block btn blue btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Penjualan Add<br /><small>(akses)</small></a>
                          <?php }?>
                          <!------------------- END Master Penjualan Add ---------------------------------->
                        </div>

                        <div class="col-sm-3">
                          <!------------------- Master Penjualan Edit ---------------------------------->
                          <?php if ($default['penjualan_edit'] == 0){?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'penjualan_edit'.'');?>" class='btn-block btn red btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Penjualan Edit<br /><small>(blok akses)</small></a>
                          <?php }else{?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'penjualan_edit'.'');?>" class='btn-block btn blue btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Penjualan Edit<br /><small>(akses)</small></a>
                          <?php }?>
                          <!------------------- END Master Penjualan Edit ---------------------------------->
                        </div>

                        <div class="col-sm-3">
                          <!------------------- Master Penjualan Delete ---------------------------------->
                          <?php if ($default['penjualan_delete'] == 0){?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'penjualan_delete'.'');?>" class='btn-block btn red btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Penjualan Delete<br /><small>(blok akses)</small></a>
                          <?php }else{?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'penjualan_delete'.'');?>" class='btn-block btn blue btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Penjualan Delete<br /><small>(akses)</small></a>
                          <?php }?>
                          <!------------------- END Master Penjualan Delete ---------------------------------->
                        </div>

                    </div>

                    <br />

                    <div class="row">
                        <div class="col-sm-3"></div>

                        <div class="col-sm-3">
                          <!------------------- Master Debit Note Add ---------------------------------->
                          <?php if ($default['debit_note_add'] == 0){?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'debit_note_add'.'');?>" class='btn-block btn red btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Debit Note Add<br /><small>(blok akses)</small></a>
                          <?php }else{?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'debit_note_add'.'');?>" class='btn-block btn blue btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Debit Note Add<br /><small>(akses)</small></a>
                          <?php }?>
                          <!------------------- END Master Debit Note Add ---------------------------------->
                        </div>

                        <div class="col-sm-3">
                          <!------------------- Master Debit Note Edit ---------------------------------->
                          <?php if ($default['debit_note_edit'] == 0){?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'debit_note_edit'.'');?>" class='btn-block btn red btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Debit Note Edit<br /><small>(blok akses)</small></a>
                          <?php }else{?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'debit_note_edit'.'');?>" class='btn-block btn blue btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Debit Note Edit<br /><small>(akses)</small></a>
                          <?php }?>
                          <!------------------- END Master Debit Note Edit ---------------------------------->
                        </div>

                        <div class="col-sm-3">
                          <!------------------- Master Debit Note Delete ---------------------------------->
                          <?php if ($default['debit_note_delete'] == 0){?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'debit_note_delete'.'');?>" class='btn-block btn red btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Debit Note Delete<br /><small>(blok akses)</small></a>
                          <?php }else{?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'debit_note_delete'.'');?>" class='btn-block btn blue btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Debit Note Delete<br /><small>(akses)</small></a>
                          <?php }?>
                          <!------------------- END Master Debit Note Delete ---------------------------------->
                        </div>

                    </div>

                    <br />

                    <div class="row">
                        <div class="col-sm-3">
                          <!------------------- Master Menu Inventory  ---------------------------------->
                          <?php if ($default['menu_inventory'] == 0){?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'menu_inventory'.'');?>" class='btn-block btn red btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Menu Inventory<br /><small>(blok akses)</small></a>
                          <?php }else{?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'menu_inventory'.'');?>" class='btn-block btn blue btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Menu Inventory <br /><small>(akses)</small></a>
                          <?php }?>
                          <!------------------- END Master Menu Inventory  ---------------------------------->
                        </div>

                        <div class="col-sm-3">
                          <!------------------- Pindah Gudang ---------------------------------->
                          <?php if ($default['pindah_gudang'] == 0){?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'pindah_gudang'.'');?>" class='btn-block btn red btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Pindah Gudang<br /><small>(blok akses)</small></a>
                          <?php }else{?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'pindah_gudang'.'');?>" class='btn-block btn blue btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Pindah Gudang<br /><small>(akses)</small></a>
                          <?php }?>
                          <!------------------- END Pindah Gudang ---------------------------------->
                        </div>

                        <div class="col-sm-3">
                          <!------------------- Recal ---------------------------------->
                          <?php if ($default['recal_inventory'] == 0){?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'recal_inventory'.'');?>" class='btn-block btn red btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Recal Inventory<br /><small>(blok akses)</small></a>
                          <?php }else{?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'recal_inventory'.'');?>" class='btn-block btn blue btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Recal Inventory<br /><small>(akses)</small></a>
                          <?php }?>
                          <!------------------- END Recal ---------------------------------->
                        </div>

                        <div class="col-sm-3"></div>

                    </div>

                    <br />

                    <div class="row">
                        <div class="col-sm-3"></div>

                        <div class="col-sm-3">
                          <!------------------- Master Opname Add ---------------------------------->
                          <?php if ($default['stockopname_add'] == 0){?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'stockopname_add'.'');?>" class='btn-block btn red btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Opname Add<br /><small>(blok akses)</small></a>
                          <?php }else{?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'stockopname_add'.'');?>" class='btn-block btn blue btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Opname Add<br /><small>(akses)</small></a>
                          <?php }?>
                          <!------------------- END Master Opname Add ---------------------------------->
                        </div>

                        <div class="col-sm-3">
                          <!------------------- Master Opname Edit ---------------------------------->
                          <?php if ($default['stockopname_edit'] == 0){?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'stockopname_edit'.'');?>" class='btn-block btn red btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Opname Edit<br /><small>(blok akses)</small></a>
                          <?php }else{?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'stockopname_edit'.'');?>" class='btn-block btn blue btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Opname Edit<br /><small>(akses)</small></a>
                          <?php }?>
                          <!------------------- END Master Opname Edit ---------------------------------->
                        </div>

                        <div class="col-sm-3">
                          <!------------------- Master Opname Delete ---------------------------------->
                          <?php if ($default['stockopname_delete'] == 0){?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'stockopname_delete'.'');?>" class='btn-block btn red btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Opname Delete<br /><small>(blok akses)</small></a>
                          <?php }else{?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'stockopname_delete'.'');?>" class='btn-block btn blue btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Opname Delete<br /><small>(akses)</small></a>
                          <?php }?>
                          <!------------------- END Master Opname Delete ---------------------------------->
                        </div>

                    </div>

                    <br />

                    <div class="row">
                        <div class="col-sm-3"></div>

                        <div class="col-sm-3">
                          <!------------------- Master Adjustment Add ---------------------------------->
                          <?php if ($default['adjustment_add'] == 0){?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'adjustment_add'.'');?>" class='btn-block btn red btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Adjustment Add<br /><small>(blok akses)</small></a>
                          <?php }else{?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'adjustment_add'.'');?>" class='btn-block btn blue btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Adjustment Add<br /><small>(akses)</small></a>
                          <?php }?>
                          <!------------------- END Master Adjustment Add ---------------------------------->
                        </div>

                        <div class="col-sm-3">
                          <!------------------- Master Adjustment Edit ---------------------------------->
                          <?php if ($default['adjustment_edit'] == 0){?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'adjustment_edit'.'');?>" class='btn-block btn red btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Adjustment Edit<br /><small>(blok akses)</small></a>
                          <?php }else{?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'adjustment_edit'.'');?>" class='btn-block btn blue btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Adjustment Edit<br /><small>(akses)</small></a>
                          <?php }?>
                          <!------------------- END Master Adjustment Edit ---------------------------------->
                        </div>

                        <div class="col-sm-3">
                          <!------------------- Master Adjustment Delete ---------------------------------->
                          <?php if ($default['adjustment_delete'] == 0){?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'adjustment_delete'.'');?>" class='btn-block btn red btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Adjustment Delete<br /><small>(blok akses)</small></a>
                          <?php }else{?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'adjustment_delete'.'');?>" class='btn-block btn blue btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Adjustment Delete<br /><small>(akses)</small></a>
                          <?php }?>
                          <!------------------- END Master Adjustment Delete ---------------------------------->
                        </div>

                    </div>

                    <br />

                    <div class="row">
                        <div class="col-sm-3"></div>

                        <div class="col-sm-3">
                          <!------------------- Master Pemusnahan Add ---------------------------------->
                          <?php if ($default['pemusnahan_add'] == 0){?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'pemusnahan_add'.'');?>" class='btn-block btn red btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Pemusnahan Add<br /><small>(blok akses)</small></a>
                          <?php }else{?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'pemusnahan_add'.'');?>" class='btn-block btn blue btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Pemusnahan Add<br /><small>(akses)</small></a>
                          <?php }?>
                          <!------------------- END Master Pemusnahan Add ---------------------------------->
                        </div>

                        <div class="col-sm-3">
                          <!------------------- Master Pemusnahan Edit ---------------------------------->
                          <?php if ($default['pemusnahan_edit'] == 0){?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'pemusnahan_edit'.'');?>" class='btn-block btn red btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Pemusnahan Edit<br /><small>(blok akses)</small></a>
                          <?php }else{?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'pemusnahan_edit'.'');?>" class='btn-block btn blue btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Pemusnahan Edit<br /><small>(akses)</small></a>
                          <?php }?>
                          <!------------------- END Master Pemusnahan Edit ---------------------------------->
                        </div>

                        <div class="col-sm-3">
                          <!------------------- Master Pemusnahan Delete ---------------------------------->
                          <?php if ($default['pemusnahan_delete'] == 0){?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'pemusnahan_delete'.'');?>" class='btn-block btn red btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Pemusnahan Delete<br /><small>(blok akses)</small></a>
                          <?php }else{?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'pemusnahan_delete'.'');?>" class='btn-block btn blue btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Pemusnahan Delete<br /><small>(akses)</small></a>
                          <?php }?>
                          <!------------------- END Master Pemusnahan Delete ---------------------------------->
                        </div>

                    </div>

                    <br />

                    <div class="row">
                        <div class="col-sm-3">
                          <!------------------- Master Menu Produksi  ---------------------------------->
                          <?php if ($default['menu_produksi'] == 0){?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'menu_produksi'.'');?>" class='btn-block btn red btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Menu Produksi<br /><small>(blok akses)</small></a>
                          <?php }else{?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'menu_produksi'.'');?>" class='btn-block btn blue btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Menu Produksi <br /><small>(akses)</small></a>
                          <?php }?>
                          <!------------------- END Master Menu Produksi  ---------------------------------->
                        </div>

                        <div class="col-sm-3">
                          <!------------------- Master Produksi Add ---------------------------------->
                          <?php if ($default['produksi_add'] == 0){?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'produksi_add'.'');?>" class='btn-block btn red btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Produksi Add<br /><small>(blok akses)</small></a>
                          <?php }else{?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'produksi_add'.'');?>" class='btn-block btn blue btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Produksi Add<br /><small>(akses)</small></a>
                          <?php }?>
                          <!------------------- END Master Produksi Add ---------------------------------->
                        </div>

                        <div class="col-sm-3">
                          <!------------------- Master Produksi Edit ---------------------------------->
                          <?php if ($default['produksi_edit'] == 0){?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'produksi_edit'.'');?>" class='btn-block btn red btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Produksi Edit<br /><small>(blok akses)</small></a>
                          <?php }else{?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'produksi_edit'.'');?>" class='btn-block btn blue btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Produksi Edit<br /><small>(akses)</small></a>
                          <?php }?>
                          <!------------------- END Master Produksi Edit ---------------------------------->
                        </div>

                        <div class="col-sm-3">
                          <!------------------- Master Produksi Delete ---------------------------------->
                          <?php if ($default['produksi_delete'] == 0){?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'produksi_delete'.'');?>" class='btn-block btn red btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Produksi Delete<br /><small>(blok akses)</small></a>
                          <?php }else{?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'produksi_delete'.'');?>" class='btn-block btn blue btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Produksi Delete<br /><small>(akses)</small></a>
                          <?php }?>
                          <!------------------- END Master Produksi Delete ---------------------------------->
                        </div>

                    </div>



                    <br />

                    <div class="row">
                        <div class="col-sm-3">
                          <!------------------- Master Menu Jurnal Umum  ---------------------------------->
                          <?php if ($default['menu_jurnal_umum'] == 0){?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'menu_jurnal_umum'.'');?>" class='btn-block btn red btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Menu Jurnal Umum<br /><small>(blok akses)</small></a>
                          <?php }else{?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'menu_jurnal_umum'.'');?>" class='btn-block btn blue btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Menu Jurnal Umum <br /><small>(akses)</small></a>
                          <?php }?>
                          <!------------------- END Master Menu Jurnal Umum  ---------------------------------->
                        </div>

                        <div class="col-sm-3">
                          <!------------------- Master Jurnal Umum Add ---------------------------------->
                          <?php if ($default['jurnal_umum_add'] == 0){?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'jurnal_umum_add'.'');?>" class='btn-block btn red btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Jurnal Umum Add<br /><small>(blok akses)</small></a>
                          <?php }else{?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'jurnal_umum_add'.'');?>" class='btn-block btn blue btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Jurnal Umum Add<br /><small>(akses)</small></a>
                          <?php }?>
                          <!------------------- END Master Jurnal Umum Add ---------------------------------->
                        </div>

                        <div class="col-sm-3">
                          <!------------------- Master Jurnal Umum Edit ---------------------------------->
                          <?php if ($default['jurnal_umum_edit'] == 0){?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'jurnal_umum_edit'.'');?>" class='btn-block btn red btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Jurnal Umum Edit<br /><small>(blok akses)</small></a>
                          <?php }else{?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'jurnal_umum_edit'.'');?>" class='btn-block btn blue btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Jurnal Umum Edit<br /><small>(akses)</small></a>
                          <?php }?>
                          <!------------------- END Master Jurnal Umum Edit ---------------------------------->
                        </div>

                        <div class="col-sm-3">
                          <!------------------- Master Jurnal Umum Delete ---------------------------------->
                          <?php if ($default['jurnal_umum_delete'] == 0){?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'jurnal_umum_delete'.'');?>" class='btn-block btn red btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Jurnal Umum Delete<br /><small>(blok akses)</small></a>
                          <?php }else{?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'jurnal_umum_delete'.'');?>" class='btn-block btn blue btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Jurnal Umum Delete<br /><small>(akses)</small></a>
                          <?php }?>
                          <!------------------- END Master Jurnal Umum Delete ---------------------------------->
                        </div>

                    </div>



                    <br />

                    <div class="row">
                        <div class="col-sm-3">
                          <!------------------- Master Menu Kas Bank  ---------------------------------->
                          <?php if ($default['menu_kas_bank'] == 0){?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'menu_kas_bank'.'');?>" class='btn-block btn red btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Menu Kas Bank<br /><small>(blok akses)</small></a>
                          <?php }else{?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'menu_kas_bank'.'');?>" class='btn-block btn blue btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Menu Kas Bank <br /><small>(akses)</small></a>
                          <?php }?>
                          <!------------------- END Master Menu Kas Bank  ---------------------------------->
                        </div>

                        <div class="col-sm-3">
                          <!------------------- Master Kas Bank Add ---------------------------------->
                          <?php if ($default['kas_bank_add'] == 0){?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'kas_bank_add'.'');?>" class='btn-block btn red btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Kas Bank Add<br /><small>(blok akses)</small></a>
                          <?php }else{?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'kas_bank_add'.'');?>" class='btn-block btn blue btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Kas Bank Add<br /><small>(akses)</small></a>
                          <?php }?>
                          <!------------------- END Master Kas Bank Add ---------------------------------->
                        </div>

                        <div class="col-sm-3">
                          <!------------------- Master Kas Bank Edit ---------------------------------->
                          <?php if ($default['kas_bank_edit'] == 0){?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'kas_bank_edit'.'');?>" class='btn-block btn red btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Kas Bank Edit<br /><small>(blok akses)</small></a>
                          <?php }else{?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'kas_bank_edit'.'');?>" class='btn-block btn blue btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Kas Bank Edit<br /><small>(akses)</small></a>
                          <?php }?>
                          <!------------------- END Master Kas Bank Edit ---------------------------------->
                        </div>

                        <div class="col-sm-3">
                          <!------------------- Master Kas Bank Delete ---------------------------------->
                          <?php if ($default['kas_bank_delete'] == 0){?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'kas_bank_delete'.'');?>" class='btn-block btn red btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Kas Bank Delete<br /><small>(blok akses)</small></a>
                          <?php }else{?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'kas_bank_delete'.'');?>" class='btn-block btn blue btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Kas Bank Delete<br /><small>(akses)</small></a>
                          <?php }?>
                          <!------------------- END Master Kas Bank Delete ---------------------------------->
                        </div>

                    </div>


                    <br />

                    <div class="row">
                        <div class="col-sm-3">
                          <!------------------- Master Menu Konfigurasi  ---------------------------------->
                          <?php if ($default['menu_konfigurasi'] == 0){?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'menu_konfigurasi'.'');?>" class='btn-block btn red btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Menu Konfigurasi<br /><small>(blok akses)</small></a>
                          <?php }else{?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'menu_konfigurasi'.'');?>" class='btn-block btn blue btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Menu Konfigurasi <br /><small>(akses)</small></a>
                          <?php }?>
                          <!------------------- END Master Konfigurasi  ---------------------------------->
                        </div>

                        <div class="col-sm-3">
                          <!------------------- Master Asset ---------------------------------->
                          <?php if ($default['menu_asset'] == 0){?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'menu_asset'.'');?>" class='btn-block btn red btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Menu Asset<br /><small>(blok akses)</small></a>
                          <?php }else{?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'menu_asset'.'');?>" class='btn-block btn blue btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Menu Asset<br /><small>(akses)</small></a>
                          <?php }?>
                          <!------------------- END Master Asset ---------------------------------->
                        </div>

                        <div class="col-sm-3">
                          <!------------------- Master Menu Laporan Inventory ---------------------------------->
                          <?php if ($default['menu_laporan_inventory'] == 0){?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'menu_laporan_inventory'.'');?>" class='btn-block btn red btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Laporan Inventory<br /><small>(blok akses)</small></a>
                          <?php }else{?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'menu_laporan_inventory'.'');?>" class='btn-block btn blue btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Laporan Inventory<br /><small>(akses)</small></a>
                          <?php }?>
                          <!------------------- END Master Menu Laporan Inventory ---------------------------------->
                        </div>

                        <div class="col-sm-3">
                          <!------------------- Master Menu Laporan Keuangan ---------------------------------->
                          <?php if ($default['menu_laporan_keuangan'] == 0){?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'menu_laporan_keuangan'.'');?>" class='btn-block btn red btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Laporan Keuangan<br /><small>(blok akses)</small></a>
                          <?php }else{?>
                              <a href="<?php echo base_url('User_controller/update_security/'.$default['role_id'].'/'.'menu_laporan_keuangan'.'');?>" class='btn-block btn blue btn-lg mt-ladda-btn ladda-button btn-circle' data-style="zoom-in" data-size="l">Laporan Keuangan<br /><small>(akses)</small></a>
                          <?php }?>
                          <!------------------- END Master Menu Laporan Keuangan ---------------------------------->
                        </div>

                    </div>



              </div>
          </form>
  </div>
</div>
