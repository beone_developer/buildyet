<div class="portlet light bordered">
	<div class="portlet-title">
	
		
		<div class="row">
			<p class="text-center" style="font-size: 17px"><b>PEMBERITAHUAN IMPOR BARANG DARI TEMPAT PENIMBUNAN BERIKAT</b></p>
		</div>
		
		<form role="form" method="post">
			<div class="form-group row">
				<label for="status" class="col-sm-2 col-form-label">Status</label>
				<div class="col-sm-2">
					<input style="border: none;" type="text" readonly class="form-control" id="uraian_status" name="uraian_status" value="<?=isset($default['URAIAN_STATUS'])? $default['URAIAN_STATUS'] : ""?>">
				</div>
				<div class="col-sm-2">
					<input style="border: none;" type="hidden" readonly class="form-control" id="status" name="status" value="<?=isset($default['KODE_STATUS'])? $default['KODE_STATUS'] : ""?>">
				</div>
			</div>

			<div class="form-group row">
				<label for="status_perbaikan" class="col-sm-2 col-form-label">Status Perbaikan</label>
				<div class="col-sm-2">
					<input style="border: none;" type="hidden" readonly class="form-control" id="status_perbaikan" name="status_perbaikan" value="<?=isset($default['KODE_STATUS_PERBAIKAN'])? $default['KODE_STATUS_PERBAIKAN'] : ""?>">
				<input style="border: none;" type="text" readonly class="form-control" name="uraian_status_perbaikan" value="<?=isset($default['uraian_status_perbaikan'])? $default['uraian_status_perbaikan'] : ""?>">
				</div>

				<div style="margin-top: -10px;" class="col-sm-2 col-sm-offset-6">
                <a href="#modal-daftar-respon" data-target="#modal-daftar-respon" data-toggle="modal">
                    <p><b>DAFTAR RESPON</b></p>
                </a>
            </div>
			</div>


			
			<!-- FORM NOMOR PENGAJUAN -->
			<div style="margin-top: 30px;">
				<div class="form-group row">
					<label for="nomor_pengajuan" class="col-sm-2 col-form-label">Nomor Pengajuan</label>
					<div class="col-sm-4">
						<input type="text" class="form-control" id="nomor_pengajuan" name="nomor_pengajuan" value="<?=isset($default['NOMOR_AJU'])? $default['NOMOR_AJU'] : ""?>">
					</div>
				</div>

				<div class="form-group row">
					<label for="nomor_pendaftaran" class="col-sm-2 col-form-label">Nomor Pendaftaran</label>
					<div class="col-sm-4">
						<input type="text" class="form-control" id="nomor_pendaftaran" name="nomor_pendaftaran" value="<?=isset($default['NOMOR_DAFTAR'])? $default['NOMOR_DAFTAR'] : ""?>">
					</div>
				</div>

				<div class="form-group row">
					<label for="tanggal_pendaftaran" class="col-sm-2 col-form-label">Tanggal Pendaftaran</label>
					<div class="col-sm-4">
						<input type="text" class="form-control" id="tanggal_pendaftaran" name="tanggal_pendaftaran" value="<?=isset($default['TANGGAL_DAFTAR'])? $default['TANGGAL_DAFTAR'] : ""?>" placeholder="DDMMYY">
					</div>
				</div>		
			</div>
			<!-- TUTUP FORM NOMOR PENGAJUAN -->
			
			<!-- FORM KANTOR PABEAN -->
			<div style="margin-top: 30px;">
				<div class="form-group row">
					<label for="kppbc_bongkar" class="col-sm-2 col-form-label">Kantor Pabean </label>
					<div class="col-sm-4">
						<input type="text" class="form-control" id="kantor_pabean" name="kantor_pabean" value="<?=isset($default['KODE_KANTOR'])? $default['KODE_KANTOR'] : ""?>">
					</div>
				</div>

				<div class="form-group row">
					<label for="tujuan" class="col-sm-2 col-form-label">Jenis TPB</label>
					<div class="col-sm-4">
						<select id="jenis_tpb" name="jenis_tpb" class="form-control">
					        <option value="<?=isset($default['KODE_JENIS_TPB'])? $default['KODE_JENIS_TPB'] : ""?>"><?=isset($default['URAIAN_JENIS_TPB'])? $default['URAIAN_JENIS_TPB'] : ""?></option>
	                         <?php foreach($jenis_tpb as $row){ ?>
	                           <option value="<?php echo $row['KODE_JENIS_TPB'];?>"><?php echo $row['URAIAN_JENIS_TPB'];?></option>
	                         <?php } ?>
				        </select>
					</div>
				</div>		
			</div>
			<!-- TUTUP FORM KANTOR PABEAN -->
			

			<div class="row">
				<div class="col-sm-12">

					<!-- FORM LEFT SIDE -->
					<ol>
						<div class="col-sm-6">
							<p><b>PENGUSAHA TPB</b></p>
								<!-- FORM PENGUSAHA TPB -->
								<div>
									<div class="form-group row">
										<li>
											<label for="nama" class="col-sm-3 col-form-label">NPWP</label>
											<div class="col-sm-4">
												<select id="kode_id_pengusaha" name="kode_id_pengusaha" class="form-control">
											        <option value="<?=isset($default['KODE_ID_PENGUSAHA'])? $default['KODE_ID_PENGUSAHA'] : $default_pengusaha['KODE_ID']?>"><?=isset($default['uraian_id_pengusaha'])? $default['uraian_id_pengusaha'] : $default_pengusaha['uraian_id_pengusaha']?></option>
	                         							<?php foreach($jenis_id as $row){ ?>
	                           						<option value="<?php echo $row['KODE_ID'];?>"><?php echo $row['URAIAN_KODE_ID'];?></option>
	                        							<?php } ?>
										        </select>
											</div>
											<div class="col-sm-4">
												<input type="text" class="form-control" id="npwp_pengusaha" name="npwp_pengusaha" value="<?=isset($default['ID_PENGUSAHA'])? $default['ID_PENGUSAHA'] : $default_pengusaha['NPWP']?>">
											</div>
										</li>
									</div>
								
									<div class="form-group row">
										<li>
											<label for="nama" class="col-sm-3 col-form-label">Nama</label>
											<div class="col-sm-8">
												<input type="text" class="form-control" id="nama_pengusaha" name="nama_pengusaha" value="<?=isset($default['NAMA_PENGUSAHA'])? $default['NAMA_PENGUSAHA'] : $default_pengusaha['NAMA']?>">
											</div>
										</li>
									</div>

									<div class="form-group row">
										<label for="alamat" class="col-sm-3 col-form-label">Alamat</label>
										<div class="col-sm-8">
											<input type="text" class="form-control" id="alamat_pengusaha" name="alamat_pengusaha" value="<?=isset($default['ALAMAT_PENGUSAHA'])? $default['ALAMAT_PENGUSAHA'] : $default_pengusaha['ALAMAT']?>">
										</div>
									</div>

									<div class="form-group row">
										<label for="alamat" class="col-sm-3 col-form-label">No. Izin</label>
										<div class="col-sm-8">
											<input type="text" class="form-control" id="no_izin" name="no_izin" value="<?=isset($default['NOMOR_IJIN_TPB'])? $default['NOMOR_IJIN_TPB'] : $default_pengusaha['NOMOR_SKEP']?>">
										</div>
									</div>

									<div class="form-group row">
										<li>
											<label for="nama" class="col-sm-3 col-form-label">API</label>
											<div class="col-sm-4">
												<select id="kode_api_pengusaha" name="kode_api_pengusaha" class="form-control">
											        <option value="<?=isset($default['KODE_JENIS_API_PENGUSAHA'])? $default['KODE_JENIS_API_PENGUSAHA'] : $default_pengusaha['ID_PENGENAL']?>"><?=isset($default['uraian_api_pengusaha'])? $default['uraian_api_pengusaha'] : $default_pengusaha['URAIAN_JENIS_API']?></option>
	                         							<?php foreach($jenis_api as $row){ ?>
	                           						<option value="<?php echo $row['KODE_JENIS_API'];?>"><?php echo $row['URAIAN_JENIS_API'];?></option>
	                        							<?php } ?>
										        </select>
											</div>
											<div class="col-sm-4">
												<input type="text" class="form-control" id="api_pengusaha" name="api_pengusaha" value="<?=isset($default['API_PENGUSAHA'])? $default['API_PENGUSAHA'] : $default_pengusaha['NOMOR_PENGENAL']?>">
											</div>
										</li>
									</div>

								</div>
								<!-- TUTUP FORM PENGUSAHA TPB -->		
							
								<!-- FORM PEMILIK BARANG -->
								<div>
									<p style="margin-left: -38px"><b>PEMILIK BARANG</b></p>
										
									<div class="form-group row">
										<li>
											<label for="nama" class="col-sm-3 col-form-label">NPWP</label>
											<div class="col-sm-4">
												<select id="kode_id_pemilik" name="kode_id_pemilik" class="form-control">
											        <option value="<?=isset($default['KODE_ID_PEMILIK'])? $default['KODE_ID_PEMILIK'] : ""?>"><?=isset($default['uraian_id_pemilik'])? $default['uraian_id_pemilik'] : ""?></option>
	                         							<?php foreach($jenis_id as $row){ ?>
	                           						<option value="<?php echo $row['KODE_ID'];?>"><?php echo $row['URAIAN_KODE_ID'];?></option>
	                        							<?php } ?>
										        </select>
											</div>
											<div class="col-sm-4">
												<input type="text" class="form-control" id="npwp_pemilik" name="npwp_pemilik" value="<?=isset($default['ID_PEMILIK'])? $default['ID_PEMILIK'] : ""?>">
											</div>
										</li>
									</div>	
									
									<div class="form-group row">
										<li>
											<label for="nama" class="col-sm-3 col-form-label">Nama</label>
											<div class="col-sm-8">
												<input type="text" class="form-control" id="nama_pemilik" name="nama_pemilik" value="<?=isset($default['NAMA_PEMILIK'])? $default['NAMA_PEMILIK'] : ""?>">
											</div>
										</li>
									</div>

									<div class="form-group row">
										<label for="alamat" class="col-sm-3 col-form-label">Alamat</label>
										<div class="col-sm-8">
											<input type="text" class="form-control" id="alamat_pemilik" name="alamat_pemilik" value="<?=isset($default['ALAMAT_PEMILIK'])? $default['ALAMAT_PEMILIK'] : ""?>">
										</div>
									</div>

									<div class="form-group row">
										<li>
											<label for="nama" class="col-sm-3 col-form-label">API</label>
											<div class="col-sm-4">
												<select id="kode_api_pemilik" name="kode_api_pemilik" class="form-control">
											        <option value="<?=isset($default['KODE_JENIS_API_PEMILIK'])? $default['KODE_JENIS_API_PEMILIK'] : ""?>"><?=isset($default['uraian_api_pemilik'])? $default['uraian_api_pemilik'] : ""?></option>
	                         							<?php foreach($jenis_api as $row){ ?>
	                           						<option value="<?php echo $row['KODE_JENIS_API'];?>"><?php echo $row['URAIAN_JENIS_API'];?></option>
	                        							<?php } ?>

										        </select>
											</div>
											<div class="col-sm-4">
												<input type="text" class="form-control" id="api_pemilik" name="api_pemilik" value="<?=isset($default['API_PEMILIK'])? $default['API_PEMILIK'] : ""?>">
											</div>
										</li>
									</div>	
								</div>
								<!-- TUTUP FORM PEMILIK BARANG -->

								<!-- FORM PENERIMA BARANG -->
								<div>
									<p style="margin-left: -38px"><b>PENERIMA BARANG</b></p>
										
									<div class="form-group row">
										<li>
											<label for="nama" class="col-sm-3 col-form-label">NPWP</label>
											<div class="col-sm-4">
												<select id="kode_id_penerima" name="kode_id_penerima" class="form-control">
											        <option value="<?=isset($default['KODE_ID_PENERIMA'])? $default['KODE_ID_PENERIMA'] : ""?>"><?=isset($default['uraian_id_penerima'])? $default['uraian_id_penerima'] : ""?></option>
	                         							<?php foreach($jenis_id as $row){ ?>
	                           						<option value="<?php echo $row['KODE_ID'];?>"><?php echo $row['URAIAN_KODE_ID'];?></option>
	                        							<?php } ?>
										        </select>
											</div>
											<div class="col-sm-4">
												<input type="text" class="form-control" id="npwp_penerima" name="npwp_penerima" value="<?=isset($default['ID_PENERIMA_BARANG'])? $default['ID_PENERIMA_BARANG'] : ""?>">
											</div>
										</li>
									</div>	
									
									<div class="form-group row">
										<li>
											<label for="nama" class="col-sm-3 col-form-label">Nama</label>
											<div class="col-sm-8">
												<input type="text" class="form-control" id="nama_penerima" name="nama_penerima" value="<?=isset($default['NAMA_PENERIMA_BARANG'])? $default['NAMA_PENERIMA_BARANG'] : ""?>">
											</div>
										</li>
									</div>

									<div class="form-group row">
										<label for="alamat" class="col-sm-3 col-form-label">Alamat</label>
										<div class="col-sm-8">
											<input type="text" class="form-control" id="alamat_penerima" name="alamat_penerima" value="<?=isset($default['ALAMAT_PENERIMA_BARANG'])? $default['ALAMAT_PENERIMA_BARANG'] : ""?>">
										</div>
									</div>

									<div class="form-group row">
										<label for="alamat" class="col-sm-3 col-form-label">NIPER</label>
										<div class="col-sm-8">
											<input type="text" class="form-control" id="niper_penerima" name="niper_penerima" value="<?=isset($default['NIPER_PENERIMA'])? $default['NIPER_PENERIMA'] : ""?>">
										</div>
									</div>

									<div class="form-group row">
										<li>
											<label for="nama" class="col-sm-3 col-form-label">API</label>
											<div class="col-sm-4">
												<select id="kode_api_penerima" name="kode_api_penerima" class="form-control">
											        <option value="<?=isset($default['KODE_JENIS_API_PENERIMA'])? $default['KODE_JENIS_API_PENERIMA'] : ""?>"><?=isset($default['uraian_api_penerima'])? $default['uraian_api_penerima'] : ""?></option>
	                         							<?php foreach($jenis_api as $row){ ?>
	                           						<option value="<?php echo $row['KODE_JENIS_API'];?>"><?php echo $row['URAIAN_JENIS_API'];?></option>
	                        							<?php } ?>

										        </select>
											</div>
											<div class="col-sm-4">
												<input type="text" class="form-control" id="api_penerima" name="api_penerima" value="<?=isset($default['API_PENERIMA'])? $default['API_PENERIMA'] : ""?>">
											</div>
										</li>
									</div>	
								</div>
								<!-- TUTUP FORM PENERIMA BARANG -->
								
								<!-- FORM KONTAINER -->
								<div>
									<a href="#modal2" data-target="#modal2" data-toggle="modal"><p style="margin-left: -38px"><b>KONTAINER</b></p></a>
								<div class="form-group row">
								<div class="col-sm-10">
									<div class="table-wrapper-scroll-y my-custom-scrollbar">
									<table class="table table-sm">
									  <thead>
									    <tr class="bg-success">
									      <th scope="col">Nomor Cont</th>
									      <th scope="col">Ukuran</th>
									      <th scope="col">Tipe</th>
									    </tr>
									  </thead>
									  <tbody>
									    <?php   foreach($kontainer as $row){ ?>
									    <tr>
									      <th><?php echo $row['NOMOR_KONTAINER'];?></th>
									      <td><?php echo $row['KODE_UKURAN_KONTAINER'];?></td>
									      <td><?php echo $row['URAIAN_UKURAN_KONTAINER'];?></td>
									    </tr>
									    <?php
	                    				}
	                  					?>
							  		</tbody>
					 				</table>
					 			</div>
								</div>
								</div>	

								</div>
								<!-- TUTUP FORM KONTAINER -->
								
								<!-- FORM PENGANGKUTAN -->
								<div>
									<a href="<?php if(isset($default['ID'])){
										echo base_url('Bc25_detail_controller/form/'.$default['ID'].'');
									}else{echo "#";} ?>"><p style="margin-left: -38px"><b>BARANG</b></p></a>
								<div class="form-group row">
								<li>
									<label for="kppbc_pengawas" class="col-sm-3 col-form-label">Bruto (Kg)</label>
									<div class="col-sm-3">
										<input type="text" class="form-control" id="bruto" name="bruto" value="<?=isset($default['BRUTO'])? $default['BRUTO'] : ""?>">
									</div>
								</li>
								</div>

								<div class="form-group row">
								<li>
									<label for="kppbc_pengawas" class="col-sm-3 col-form-label">Netto (Kg)</label>
									<div class="col-sm-3">
										<input type="text" class="form-control" id="netto" name="netto" value="<?=isset($default['NETTO'])? $default['NETTO'] : ""?>">
									</div>
								</li>
								</div>

								<div class="form-group row">
								<li>
									<label for="kppbc_pengawas" class="col-sm-3 col-form-label">Jumlah Barang</label>
									<div class="col-sm-3">
										<input type="text" class="form-control" id="jumlah_barang" name="jumlah_barang" value="<?=isset($default['JUMLAH_BARANG'])? $default['JUMLAH_BARANG'] : ""?>">
									</div>
								</li>
								</div>	
									
								</div>
								<!-- TUTUP FORM PENGANGKUTAN -->	
						</div>
						<!-- TUTUP FORM LEFT SIDE -->


						<!-- FORM RIGHT SIDE -->
						<div class="col-sm-6" style="padding-right: 50px;">
							<a href="#modal" data-target="#modal" data-toggle="modal"><p style="margin-left: -38px"><b>DOKUMEN</b></p></a>
							<div class="form-group row">
								<li>
									<label for="nama" class="col-sm-3 col-form-label">Invoice</label>
									<div class="col-sm-4">
										<input type="text" <?=isset($default['ID'])? "" : "readonly=true"?> class="form-control" id="invoice" name="invoice" value="<?=isset($default_dok_inv['NOMOR_DOKUMEN'])? $default_dok_inv['NOMOR_DOKUMEN'] : ""?>">
									</div>
									<div class="col-sm-4">
										<input type="text" <?=isset($default['ID'])? "" : "readonly=true"?> class="form-control" id="tgl_invoice" name="tgl_invoice" value="<?=isset($default_dok_inv['TANGGAL_DOKUMEN'])? $default_dok_inv['TANGGAL_DOKUMEN'] : ""?>">
									</div>
								</li>
							</div>

							<div class="form-group row">
								<li>
									<label for="kppbc_pengawas" class="col-sm-3 col-form-label">Packing List</label>
									<div class="col-sm-4">
										<input type="text" <?=isset($default['ID'])? "" : "readonly=true"?> class="form-control" id="packing_list" name="packing_list" value="<?=isset($default_dok_pl['NOMOR_DOKUMEN'])? $default_dok_pl['NOMOR_DOKUMEN'] : ""?>">
									</div>
									<div class="col-sm-4">
										<input type="text" <?=isset($default['ID'])? "" : "readonly=true"?> class="form-control" id="tgl_packing_list" name="tgl_packing_list" value="<?=isset($default_dok_pl['TANGGAL_DOKUMEN'])? $default_dok_pl['TANGGAL_DOKUMEN'] : ""?>">
									</div>
								</li>
							</div>

							<div class="form-group row">
								<li>
									<label for="kppbc_pengawas" class="col-sm-3 col-form-label">Kontrak</label>
									<div class="col-sm-4">
										<input type="text" <?=isset($default['ID'])? "" : "readonly=true"?> class="form-control" id="kontrak" name="kontrak" value="<?=isset($default_dok_ktr['NOMOR_DOKUMEN'])? $default_dok_ktr['NOMOR_DOKUMEN'] : ""?>">
									</div>
									<div class="col-sm-4">
										<input type="text" <?=isset($default['ID'])? "" : "readonly=true"?> class="form-control" id="tgl_kontrak" name="tgl_kontrak" value="<?=isset($default_dok_ktr['TANGGAL_DOKUMEN'])? $default_dok_ktr['TANGGAL_DOKUMEN'] : ""?>">
									</div>
								</li>
							</div>
							<div class="form-group row">
								<li>
									<label for="kppbc_pengawas" class="col-sm-3 col-form-label">Fasilitas Impor</label>
									<div class="col-sm-3">
										<input type="text" <?=isset($default['ID'])? "" : "readonly=true"?> class="form-control" id="fasilitas_impor" name="fasilitas_impor" value="">
									</div>
									<div class="col-sm-4">
										<input type="text" <?=isset($default['ID'])? "" : "readonly=true"?> class="form-control" id="tgl_fasilitas_impor" name="tgl_fasilitas_impor" value="<?=isset($default['JUMLAH_BARANG'])? $default['JUMLAH_BARANG'] : ""?>">
									</div>
									<div class="col-sm-2">
										<input type="text" <?=isset($default['ID'])? "" : "readonly=true"?> class="form-control" id="fasilitas_impor2" name="fasilitas_impor2" value="">
									</div>
								</li>
							</div>

							<div class="form-group row">
								<li>
									<label for="tujuan" class="col-sm-10 col-form-label">Surat Keputusan / Dokumen Lainnya</label>
								</li>
							</div>

							<div class="form-group row">
								<div class="col-sm-10">
									<div class="table-wrapper-scroll-y my-custom-scrollbar">
									<table class="table table-sm">
									  <thead>
									    <tr class="bg-success">
									      <th scope="col">Jenis Dokumen</th>
									      <th scope="col">Nomor Dokumen</th>
									      <th scope="col">Tanggal</th>
									    </tr>
									  </thead>
									  <tbody>
									    <?php   foreach($dokumen_luar as $row){ ?>
									    <tr>
									      <th><?php echo $row['URAIAN_DOKUMEN'];?></th>
									      <td><?php echo $row['NOMOR_DOKUMEN'];?></td>
									      <td><?php echo $row['TANGGAL_DOKUMEN'];?></td>

									    </tr>
									    <?php
	                    				}
	                  					?>
									  </tbody>
									</table>
								</div>
								</div>
							</div>


							<p><b>HARGA</b></p>
							<div class="form-group row">
								<li>
									<label for="nama" class="col-sm-3 col-form-label">Valuta</label>
									<div class="col-sm-4">
										<input type="text" class="form-control" id="valuta" name="valuta" value="<?=isset($default['KODE_VALUTA'])? $default['KODE_VALUTA'] : ""?>">
									</div>
								</li>
							</div>

							<div class="form-group row">
								<li>
									<label for="kppbc_pengawas" class="col-sm-3 col-form-label">NDPBM</label>
									<div class="col-sm-3">
										<input type="text" class="form-control" id="ndpbm" name="ndpbm" value="<?=isset($default['NDPBM'])? $default['NDPBM'] : ""?>">
									</div>
								</li>
							</div>

							<div class="form-group row">
								<li>
									<label for="kppbc_pengawas" class="col-sm-3 col-form-label">Nilai CIF</label>
									<div class="col-sm-3">
										<input type="text" class="form-control" id="nilai_cif" name="nilai_cif" value="<?=isset($default['CIF'])? $default['CIF'] : ""?>">
									</div>
								</li>
							</div>

							<div class="form-group row">
								<li>
									<label for="kppbc_pengawas" class="col-sm-3 col-form-label">Harga Penyerahan</label>
									<div class="col-sm-3">
										<input type="text" class="form-control" id="harga_penyerahan" name="harga_penyerahan" value="<?=isset($default['HARGA_PENYERAHAN'])? $default['HARGA_PENYERAHAN'] : ""?>">
									</div>
								</li>
							</div>

							<div class="form-group row">
					<label for="tujuan" class="col-sm-2 col-form-label">Jenis Sarana Pengangkut</label>
					<div class="col-sm-4">
						<select id="kode_sarana_pengangkut" name="kode_sarana_pengangkut" class="form-control">
					        <option value="<?=isset($default['KODE_CARA_ANGKUT'])? $default['KODE_CARA_ANGKUT'] : ""?>"><?=isset($default['URAIAN_CARA_ANGKUT'])? $default['URAIAN_CARA_ANGKUT'] : ""?></option>
	                         <?php 	foreach($jenis_cara_angkut as $row){ ?>
	                           <option value="<?php echo $row['KODE_CARA_ANGKUT'];?>"><?php echo $row['URAIAN_CARA_ANGKUT'];?></option>
	                         <?php } ?>
				        </select>
					</div>
				</div>

				<a href="#modalkemasan" data-target="#modalkemasan" data-toggle="modal"><p style="margin-left: -38px"><b>KEMASAN</b></p></a>
				<div class="form-group row">
					<div class="col-sm-10">
						<div class="table-wrapper-scroll-y my-custom-scrollbar">
						<table class="table table-sm">
						  <thead>
						    <tr class="bg-success">
						      <th scope="col">Jumlah</th>
						      <th scope="col">Kode Jenis</th>
						      <th scope="col">Jenis</th>
						    </tr>
						  </thead>
						  <tbody>
						  	 <?php   foreach($kemasan as $row){ ?>
						    <tr>
						      <th><?php echo $row['JUMLAH_KEMASAN'];?></th>
						      <td><?php echo $row['KODE_JENIS_KEMASAN'];?></td>
						      <td><?php echo $row['URAIAN_KEMASAN'];?></td>
						    </tr>
						    <?php
	        				}
	      					?>
						  </tbody>
					 	</table>
					 </div>
					</div>
				</div>
				<p><b>BILING</b></p>

				<div class="form-group row">
				<label for="tujuan" class="col-sm-2 col-form-label">Pembayaran</label>
					<div class="col-sm-4">
						<select id="kode_lokasi_bayar" name="kode_lokasi_bayar" class="form-control">
					        <option value="<?=isset($default['KODE_LOKASI_BAYAR'])? $default['KODE_LOKASI_BAYAR'] : ""?>"><?=isset($default['URAIAN_LOKASI_BAYAR'])? $default['URAIAN_LOKASI_BAYAR'] : ""?></option>
	                         <?php 	foreach($jenis_lokasi_bayar as $row){ ?>
	                           <option value="<?php echo $row['KODE_LOKASI_BAYAR'];?>"><?php echo $row['URAIAN_LOKASI_BAYAR'];?></option>
	                         <?php } ?>
				        </select>
					</div>
				</div>

				<div class="form-group row">
				<label for="tujuan" class="col-sm-2 col-form-label">Wajib Bayar</label>
					<div class="col-sm-4">
						<select id="jenis_pembayar" name="jenis_pembayar" class="form-control">
					        <option value="<?=isset($default['KODE_PEMBAYAR'])? $default['KODE_PEMBAYAR'] : ""?>"><?=isset($default['URAIAN_PEMBAYAR'])? $default['URAIAN_PEMBAYAR'] : ""?></option>
	                         <?php 	foreach($jenis_pembayar as $row){ ?>
	                           <option value="<?php echo $row['KODE_PEMBAYAR'];?>"><?php echo $row['URAIAN_PEMBAYAR'];?></option>
	                         <?php } ?>
				        </select>
					</div>
				</div>

						</ol>			
					</div>	

					<!-- TUTUP FORM RIGHT SIDE -->
<div>
								
							<div class="form-group row">
							<div class="col-sm-10">
								<table class="table table-sm">
								  <thead>
								    <tr class="bg-success">
								      <th scope="col">Jenis Pungutan</th>
								      <th scope="col">Dibayar (Rp)</th>
								      <th scope="col">Dibebaskan (Rp)</th>
								      <th scope="col">Ditanggung (Rp)</th>
								      <th scope="col">Sudah Dilunasi (Rp)</th>

								    </tr>
								  </thead>
								  <tbody>
								    <?php   foreach($harga_pungutan as $row){ ?>
								    <tr>
								      <th><?php echo $row['jenis_tarif'];?></th>
								      <td><?php echo $row['Dibayar'];?></td>
								      <td><?php echo $row['Dibebaskan'];?></td>
								      <td><?php echo $row['Ditanggung'];?></td>
								      <td><?php echo $row['SudahDilunasi'];?></td>
								    </tr>
								    <?php
                    				}
                  					?>
                  					<tr>
								      <th scope="col">Total</th>
								      <td><?=isset($default['TOTAL_BAYAR'])? $default['TOTAL_BAYAR'] : "0"?></td>
								      <td><?=isset($default['TOTAL_BEBAS'])? $default['TOTAL_BEBAS'] : "0"?></td>
								      <td><?=isset($default['TOTAL_TANGGUNG'])? $default['TOTAL_TANGGUNG'] : "0"?></td>
								      <td><?=isset($default['TOTAL_DILUNASI'])? $default['TOTAL_DILUNASI'] : "0"?></td>
								    </tr>
						  		</tbody>
				 				</table>
							</div>
							</div>	

							</div>
							<div class="col-sm-6">
							<div class="form-group row">						
						<label for="kppbc_pengawas" class="col-sm-8 col-form-label">Dengan ini saya menyatakan bertanggung jawab atas kebenaran hal-hal yang diberitahukan dalam dokumen ini</label>
						</div>
						<div class="form-group row">	
							<li>
								<div class="col-sm-4">
									<input type="text" class="form-control" id="kota_ttd" name="kota_ttd" value="<?=isset($default['KOTA_TTD'])? $default['KOTA_TTD'] : ""?>">
								</div>
								<div class="col-sm-4">
									<input placeholder="YYYY-MM-DD" type="text" class="form-control" id="tgl_ttd" name="tgl_ttd" value="<?=isset($default['TANGGAL_TTD'])? $default['TANGGAL_TTD'] : date("Y-m-d")?>">
								</div>
							</li>
						</div>
						<div class="form-group row">	
							<label for="kppbc_pengawas" class="col-sm-3 col-form-label">Pemberitahu</label>
								<div class="col-sm-5">
									<input type="text" class="form-control" id="nama_ttd" name="nama_ttd" value="<?=isset($default['NAMA_TTD'])? $default['NAMA_TTD'] : ""?>">
								</div>
						</div>
						<div class="form-group row">	
							<label for="kppbc_pengawas" class="col-sm-3 col-form-label">Jabatan</label>
								<div class="col-sm-5">
									<input type="text" class="form-control" id="jabatan_ttd" name="jabatan_ttd" value="<?=isset($default['JABATAN_TTD'])? $default['JABATAN_TTD'] : ""?>">
								</div>
						</div>
					</div>
			</div>
		</div>
		<div class="form-actions">
            <a href='<?php echo base_url('Bc25_controller');?>' class='btn default'> Cancel</a>
            <button type="submit" class="btn blue" name="submit_bc25">Simpan</button>
        </div>
	</form>


		<!-- MODAL DOKUMEN -->
		
		<div id="modal" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog">
		    <div class="modal-content">
		      <div class="modal-header">
		        <h3 class="modal-title"><b>Dokumen</b></h3>
		      </div>
		      <div class="modal-body">
				<div class="row">
					<div class="col-sm-12">
						<form action="<?php echo base_url('Bc25_controller/update_dokumen/'.$default['ID'].'');?>" method="post"  enctype="multipart/form-data" role="form">
							<div class="form-body">
								<div class="row">
									<div class="col-sm-12">
										<label>Dokumen</label>
										<input type="hidden" class="form-control" id="id_dokumen" name="id_dokumen" value="">
										<input type="text" class="form-control" id="jenis_dokumen" name="jenis_dokumen">
									</div>
								</div>
							</div>

							<div class="form-body">
								<div class="row">
									<div class="col-sm-12">
										<label>Nomor</label>
										<input type="text" class="form-control" id="nomor_dokumen" name="nomor_dokumen" >
									</div>
								</div>
							</div>

							<div class="form-body">
								<div class="row">
									<div class="col-sm-12">
										<label>Tanggal</label>
										<input type="text" class="form-control" id="tgl_dokumen" name="tgl_dokumen" placeholder="DDMMYY" >
									</div>
								</div>
							</div>
					</div>	
				</div>
				
				<div class="row">
					<div class="table-wrapper-scroll-y my-custom-scrollbar">
						<table class="table table-striped table-bordered table-sm">
						    <thead>
						      <tr>
						          <th scope="col"><center><p style="font-size: 12px;">Seri</p></center></th>
							      <th scope="col"><center><p style="font-size: 12px;">Kode Dokumen</p></center></th>
							      <th scope="col"><center><p style="font-size: 12px;">Jenis Dokumen</p></center></th>
							      <th scope="col"><center><p style="font-size: 12px;">Nomor</p></center></th>
							      <th scope="col"><center><p style="font-size: 12px;">Tanggal</p></center></th>
						      </tr>
						    </thead>
						    <tbody>
						      <?php foreach ($dokumen_modal_luar as $row) { ?>
						        <tr>
						            <td><p style="font-size: 12px"><?php echo $row['SERI_DOKUMEN'];?></p></td>
						            <td><p style="font-size: 12px"><?php echo $row['KODE_JENIS_DOKUMEN'];?></p></td>
						            <td><p style="font-size: 12px"><?php echo $row['TIPE_DOKUMEN'];?></p></td>
						            <td><p style="font-size: 12px"><?php echo $row['NOMOR_DOKUMEN'];?></p></td>
						            <td><p style="font-size: 12px"><?php echo $row['TANGGAL_DOKUMEN'];?></p></td>
						            
						            <td>
						            	<button type="button" class="btn blue" onclick="tampilkanEditDokumen(<?php echo $row['ID'];?>)"><i class="fa fa-pencil"></i></button>
					                    <a href="javascript:dialogHapus('<?php echo base_url('Bc25_controller/delete_dokumen/' . $row['ID'] . '/'.$default['ID'].''); ?>')" class='btn red'><i class="fa fa-trash-o"></i></a>
						            </td>
						        </tr>
						       <?php } ?>
						    </tbody>
						</table>
					</div>
			      </div>	
				</div>

		      <div class="modal-footer">
		        <button type="submit" class="btn blue" name="submit_modaldokumen">Simpan</button>
		        <a href='<?php echo base_url('Bc25_controller');?>' class='btn default'> Cancel</a>
		        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		      </div>
		      </form>
		    </div>
		  <script>
        function dialogHapus(urlHapus) {
            if (confirm("Apakah anda yakin ingin menghapus ini ?")) {
                document.location = urlHapus;
            }
        }
    </script>
		</div>

		<!-- MODAL KEMASAN -->
		
		<div id="modalkemasan" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog">
		    <div class="modal-content">
		      <div class="modal-header">
		        <h3 class="modal-title"><b>Kemasan</b></h3>
		      </div>
		      <div class="modal-body">
				<div class="row">
					<div class="col-sm-12">
						<form action="<?php echo base_url('Bc25_controller/update_kemasan/'.$default['ID'].'');?>" method="post">
							<div class="form-body">
								<div class="row">
									<div class="col-sm-12">
										<label>Jumlah</label>
										<input type="hidden" class="form-control" id="id_kemasan" name="id_kemasan" value="">
										<input type="text" class="form-control" id="jumlah_kemasan" name="jumlah_kemasan" value="">
									</div>
								</div>
							</div>

							<div class="form-body">
								<div class="row">
									<div class="col-sm-12">
										<label>Jenis</label>
										<input type="text" class="form-control" id="jenis_kemasan" name="jenis_kemasan" value="">
									</div>
								</div>
							</div>

							<div class="form-body">
								<div class="row">
									<div class="col-sm-12">
										<label>Merk</label>
										<input type="text" class="form-control" id="merk_kemasan" name="merk_kemasan" value="">
									</div>
								</div>
							</div>
					</div>	
				</div>
				
				<div class="row">
					<div class="table-wrapper-scroll-y my-custom-scrollbar">
						<table class="table table-striped table-bordered table-sm">
						    <thead>
						      <tr>
						          <th scope="col"><center><p style="font-size: 12px;">No</p></center></th>
							      <th scope="col"><center><p style="font-size: 12px;">Jumlah</p></center></th>
							      <th scope="col"><center><p style="font-size: 12px;">Kode</p></center></th>
							      <th scope="col"><center><p style="font-size: 12px;">Uraian</p></center></th>
							      <th scope="col"><center><p style="font-size: 12px;">Merk Kemasan</p></center></th>
						      </tr>
						    </thead>
						    <tbody>
						      <?php $a=0; foreach ($kemasan_modal_luar as $row) { $a++; ?>
						        <tr>
						            <td><p style="font-size: 12px"><?php echo $a;?></p></td>
						            <td><p style="font-size: 12px"><?php echo $row['JUMLAH_KEMASAN'];?></p></td>
						            <td><p style="font-size: 12px"><?php echo $row['KODE_JENIS_KEMASAN'];?></p></td>
						            <td><p style="font-size: 12px"><?php echo $row['URAIAN_KEMASAN'];?></p></td>
						            <td><p style="font-size: 12px"><?php echo $row['MERK_KEMASAN'];?></p></td>
						            
						            <td>
					                    <button type="button" class="btn blue" onclick="tampilkanEditKemasan(<?php echo $row['ID'];?>)"><i class="fa fa-pencil"></i></button>
					                    <a href="javascript:dialogHapus('<?php echo base_url('Bc25_controller/delete_kemasan/' . $row['ID'] . '/'.$default['ID'].''); ?>')" class='btn red'><i class="fa fa-trash-o"></i></a>
						            </td>
						        </tr>
						       <?php } ?>
						    </tbody>
						</table>
					</div>
			      </div>	
				</div>

		      <div class="modal-footer">
		        <button type="submit" class="btn blue" name="submit_modalkemasan">Simpan</button>
		        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		      </div>
		      </form>
		    </div>
		  <script>
        function dialogHapus(urlHapus) {
            if (confirm("Apakah anda yakin ingin menghapus ini ?")) {
                document.location = urlHapus;
            }
        }
    </script>
		</div>


		<!-- MODAL DAFTAR RESPON -->
        
        <div id="modal-daftar-respon" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog">
            <div class="modal-content">
              <div class="modal-header">
                <h3 class="modal-title"><b>Respon</b></h3>

                <div class="row">
                    <div class="col-sm-6">
                        <a href="#" class="btn btn-primary btn-sm" role="button" aria-pressed="true">Respon</a>
                        <!-- <a href="#" class="btn btn-primary btn-sm" role="button" aria-pressed="true">Status</a> -->
                    </div>
                    
                </div>
              </div>
              <div class="modal-body">
                
                <div class="row">
                    <div class="table-wrapper-scroll-y my-custom-scrollbar">
                        <table class="table table-striped table-bordered table-sm">
                            <thead>
                              <tr>
                                  <th scope="col"><center><p style="font-size: 12px;">Kode</p></center></th>
                                  <th scope="col"><center><p style="font-size: 12px;">Uraian</p></center></th>
                                  <th scope="col"><center><p style="font-size: 12px;">Waktu</p></center></th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php foreach ($modal_daftar_respon as $data) { ?>
                                <tr>
                                    <td><p style="font-size: 12px"><?php echo $data['KODE_RESPON'];?></p></td>
                                    <td><p style="font-size: 12px"><?php echo $data['URAIAN_RESPON'];?></p></td>
                                    <td><p style="font-size: 12px"><?php echo $data['WAKTU_RESPON'];?></p></td>
                                </tr>
                               <?php } ?>
                            </tbody>
                        </table>
                    </div>
                  </div>    
                </div>

              <!-- <div class="modal-footer">
                <button type="button" class="btn btn-primary">Save changes</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              </div> -->
            </div>
        </div>
        <!-- TUTUP MODAL DAFTAR RESPON -->

        <!-- MODAL KONTAINER -->

<div id="modal2" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h3 class="modal-title"><b>KONTAINER</b></h3>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-sm-12">
                    <form action="preview_BC261/update_popup_kontainer" method = "POST">
                        <div class="form-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <input type="hidden" class="form-control" id="id_kontainer" name="id_kontainer" value="">
                                </div>
                            </div>
                        </div>
                        <div class="form-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Nomor Kontainer</label>
                                    <input type="text" class="form-control" id="nomor_kontainer" name="nomor_kontainer" value="">
                                </div>
                            </div>
                        </div>

                        <div class="form-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Ukuran</label>
                                    <input type="text" class="form-control" id="ukuran_kontainer" name="ukuran_kontainer" value="">
                                </div>
                            </div>
                        </div>
                        <div class="form-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Tipe Kontainer</label>
                                    <input type="text" class="form-control" id="tipe_kontainer" name="tipe_kontainer" value="">
                                </div>
                            </div>
                        </div>



                        <div class="modal-footer">
                            <button type="submit" class="btn blue" name="edit_bc261">Save</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </form>
                </div>	
            </div>
            <br></br>
            <div class="row">
                <div class="table-wrapper-scroll-y my-custom-scrollbar">
                    <table class="table table-striped table-bordered table-hover">

                        <thead>
                            <tr class="bg-success">
                                <th scope="col">Nomor Kontainer</th>
                                <th scope="col">Ukuran</th>
                                <th scope="col">Tipe</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <?php
                                $n = 0;
                                if ($ptb_kontainer) {
                                    foreach ($ptb_kontainer as $data) {
                                        $n = $n + 1;
                                        ?>

                                        <td width="30%"><?php echo $data->NOMOR_KONTAINER; ?></td>
                                        <td width="30%"><?php echo $data->URAIAN_UKURAN_KONTAINER; ?></td>
                                        <td width="30%"><?php echo $data->URAIAN_TIPE_KONTAINER; ?></td>
                                        <td><button type="button" class="btn btn-dark" onclick="tampilkanEditKontainer(<?php echo $data->id_kontainer; ?>)"><i class="fa fa-info"></i></button>
                                            <a href="javascript:dialogHapus('<?php echo base_url('Bc25_controller/delete_kontainer/' . $data->id_kontainer . '/'.$default['ID'].''); ?>')" class='btn red'><i class="fa fa-trash-o"></i></a></td>


                                        <?php
                                    }
                                }
                                ?>
                            </tr>	    

                        </tbody>
                    </table>



                </div>
            </div>	
        </div>

        <div class="modal2-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
    </div>
    <script>
        function dialogHapus(urlHapus) {
            if (confirm("Apakah anda yakin ingin menghapus ini ?")) {
                document.location = urlHapus;
            }
        }
    </script>
</div>


	</div>
</div>

<script type="text/javascript">
  function tampilkanEditDokumen(id){
// alert(id);
$.ajax({
        url : "<?php echo base_url('Bc25_controller/editmodal/')?>" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
 			$("#id_dokumen").empty();
			$("#id_dokumen").val(data.dokumenedit.ID);
            $("#jenis_dokumen").empty();
			$("#jenis_dokumen").val(data.dokumenedit.KODE_JENIS_DOKUMEN);
			$("#nomor_dokumen").empty();
			$("#nomor_dokumen").val(data.dokumenedit.NOMOR_DOKUMEN);
			$("#tgl_dokumen").empty();
			$("#tgl_dokumen").val(data.dokumenedit.TANGGAL_DOKUMEN);
			$("#modal").modal('show');
 
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}

function tampilkanEditKemasan(id){
// alert(id);
$.ajax({
        url : "<?php echo base_url('Bc25_controller/editkemasan/')?>" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
 			$("#id_kemasan").empty();
			$("#id_kemasan").val(data.kemasanedit.ID);
            $("#jenis_kemasan").empty();
			$("#jenis_kemasan").val(data.kemasanedit.KODE_JENIS_KEMASAN);
			$("#jumlah_kemasan").empty();
			$("#jumlah_kemasan").val(data.kemasanedit.JUMLAH_KEMASAN);
			$("#merk_kemasan").empty();
			$("#merk_kemasan").val(data.kemasanedit.MERK_KEMASAN);
			$("#modalkemasan").modal('show');
 
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}
</script>
