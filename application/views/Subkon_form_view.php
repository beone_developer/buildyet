<!-- BEGIN PAGE TITLE-->
<!-- END PAGE TITLE-->
<!-- END PAGE HEADER-->
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"></script>
<?php $list_item = $this->db->query("SELECT * FROM public.beone_item");?>

<div class="portlet box blue ">
      <div class="portlet-title">
          <div class="caption">
              <i class="fa fa-gift"></i> Form Subkon</div>
          <div class="tools">
              <a href="" class="collapse"> </a>
              <a href="#portlet-config" data-toggle="modal" class="config"> </a>
              <a href="" class="reload"> </a>
              <a href="" class="remove"> </a>
          </div>
      </div>
      <div class="portlet-body form">




          <form role="form" method="post">
              <div class="form-body">

                <div class="row">
                  <div class="col-sm-4">
                      <?php
                      /******************* GENERATE SK NUMBER ******************************/
                      $tgl = date('m/d/Y');
                      $thn = substr($tgl,6,4);
                      $bln = substr($tgl,0,2);

                      if ($bln == 1){
                          $romawi = "I";
                      }else if($bln == 2){
                          $romawi = "II";
                      }else if($bln == 3){
                          $romawi = "III";
                      }else if($bln == 4){
                          $romawi = "IV";
                      }else if($bln == 5){
                          $romawi = "V";
                      }else if($bln == 6){
                          $romawi = "VI";
                      }else if($bln == 7){
                          $romawi = "VII";
                      }else if($bln == 8){
                          $romawi = "VIII";
                      }else if($bln == 9){
                          $romawi = "IX";
                      }else if ($bln == 10){
                          $romawi = "X";
                      }else if($bln == 11){
                          $romawi = "XI";
                      }else{
                          $romawi = "XII";
                      }

                      //PENENTUAN BENTUK KODE
                      //$kode_awal = "PO/".$thn."/".$bln."/";
                      $kode_awal = "/".$romawi."/BYI/NIP/".$thn;
                      $no_po = "SK-";

                      //LOOKUP KODE
                      $lookup_kode = $this->db->query("SELECT * FROM public.beone_subkon_header WHERE subkon_no LIKE '%$kode_awal' ORDER BY subkon_header_id DESC LIMIT 1");
                      $hasil_lookup_kode = $lookup_kode->row_array();

                      $urutan = substr($hasil_lookup_kode['subkon_no'], 3, 2);
                      $no_lanjutan = $urutan + 1;

                      $digit = strlen($no_lanjutan);
                      $jml_nol = 2-$digit;

                      $cetak_nol = "";

                      for ($i = 1; $i <= $jml_nol; $i++) {
                          $cetak_nol = $cetak_nol."0";
                      }
                      $nomorsk = "SK-".$cetak_nol.$no_lanjutan.$kode_awal;
                      echo "<h3><b>".$nomorsk."</b></h3>";
                      /*************************************************/
                      ?>
                  </div>



                  <div class="col-sm-8">
                    <div class="form-group">
                          <label>Tanggal</label>
                          <div class="input-group">
                            <span class="input-group-addon input-circle-left">
                                <i class="fa fa-calendar"></i>
                            </span>
                            <input class="form-control form-control-inline input-medium date-picker input-circle-right" size="16" type="text" name="tanggal" value="<?php echo date('m/d/Y');?>" readonly required/>
                            <span class="help-block"></span>
                          </div>
                    </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-sm-4">
                      <div class="form-group">
                            <label>Supplier</label>
                            <div class="input-group">
                               <select id="select2-single-input-sm" class="form-control input-sm select2-multiple" name="supplier" required>
                                    <option value=""></option>
                                 <?php
                                   $supplier = $this->db->query("SELECT * FROM public.beone_custsup WHERE custsup_id =".intval($default['supplier_id']));
                                   $hasil_supplier = $supplier->row_array();

                                   foreach($list_supplier as $row){
                                   ?>
                                    <option value='<?php echo $row['custsup_id'];?>'><?php echo $row['nama'];?></opiton>
                                    <?php
                                    }
                                    ?>
                               </select>
                         </div>
                       </div>
                    </div>

                      <div class="col-sm-8">
                        <div class="form-group">
                            <label>Keterangan</label>
                            <input type="text" class="form-control" placeholder="Keterangan" id="keterangan" name="keterangan" required>
                            <input type"text" name="nomor_sk" value='<?php echo $nomorsk;?>' hidden>
                            <input type"text" name="jml_item" id="jml_item" value=0 hidden>
                          </div>
                      </div>
                    </div>


                    <hr / style="border-color: #3598DC;">
                    <table  id="datatable" class="table striped hovered cell-hovered">
            						<thead>
            							<tr>
                            <td width="5%">ID</td>
            								<td width="50%">Item</td>
            								<td width="30%">Qty</td>
                            <td width="10%"></td>
            							</tr>
            						 </thead>
                         <tbody id="container">

            						</tbody>
            				</table>


              </div>
              <div class="form-actions">
                <a class="btn blue" data-toggle="modal" href="#responsive"> Tambah Data </a>
                <?php if(helper_security("po_add") == 1){?>
                <button type="submit" class="btn red" name="submit_subkontrak">Submit</button>
                <?php }?>
              </div>
          </form>
  </div>
</div>


<!--------------------------- MODAL ADD ITEM--------------------------------------------->
<?php
    $list_item2 = $this->db->query("SELECT d.gudang_id, d.item_id, i.nama as nitem, SUM(qty_in) - SUM(qty_out) as jml_qty, d.nomor_transaksi
                                        FROM public.beone_gudang_detail d INNER JOIN public.beone_item i ON d.item_id = i.item_id
                                        WHERE d.flag = 1 AND d.gudang_id = 1 GROUP BY i.nama, d.item_id, d.gudang_id, d.nomor_transaksi");

    $gudang = 1;

    $gudang_asal = $this->db->query("SELECT * FROM public.beone_gudang WHERE gudang_id = ".intval($gudang));
    $hasil_gudang_asal = $gudang_asal->row_array();
    $gudang_id = $hasil_gudang_asal['gudang_id'];
    $nama = $hasil_gudang_asal['nama'];
?>

<div id="responsive" class="modal fade" tabindex="-1" data-width="960">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Detail Item</h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-8">
            <div class="form-group">
                <label>Item</label>
                <select id='item_modal' class='form-control input-sm select2-multiple' name='item_modal'>
                  <option value=""></option>
                  <?php  foreach($list_item2->result_array() as $row){
                      if ($row['jml_qty'] <> 0){
                  ?>
                      <option value="<?php echo $row['item_id'];?>"><?php echo $row['nitem']." | Ready Stock = ".number_format($row['jml_qty'],2)." | Doc :".$row['nomor_transaksi'];?></option>
                  <?php
                      }
                    }
                  ?>
                </select>
            </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                <label>Quantity</label>
                <input type='text' class='form-control' placeholder="Quantity" name='qty_modal' id='qty_modal' onchange="autoAmount()" required>
                </div>
            </div>
        </div>

    </div>
    <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
        <button type="button" class="btn green" name="add_btn" id="add_btn">Insert</button>
    </div>
</div>


<script>
$(document).ready(function() {
    		var count = 0;

    		$("#add_btn").click(function(){
					count += 1;

          var item = document.getElementById('item_modal');
          var qty = document.getElementById('qty_modal');
          var jml_item = document.getElementById('jml_item').value;
          document.getElementById('jml_item').value = (jml_item*1) + (1*1);
          var namaItem = $('#item_modal option:selected').text();

		   		$('#container').append(
							 '<tr class="records" id="'+count+'">'
						 + '<td><input class="form-control" id="item_id_' + count + '" name="item_id_'+count+'" type="text" value="'+item.value+'" readonly></td>'
						 + '<td><input class="form-control" id="item_' + count + '" name="item_'+count+'" type="text" value="'+namaItem+'" readonly></td>'
						 + '<td><input class="form-control" id="qty_' + count + '" name="qty_'+count+'" type="text" value="'+qty.value+'" readonly></td>'
             + '<td><input id="rows_' + count + '" name="rows[]" value="'+ count +'" type="hidden"></td>'
						 + '<td><button type="button" class="btn red" onclick="hapus('+count+')">X</button></td></tr>'
					);

          eraseText();
          $('#responsive').modal('hide');
				});

		});


    function eraseText() {
     document.getElementById("item_modal").value = "";
     document.getElementById("qty_modal").value = "";
     $("#item_modal").select2("val", " ");
    }

    function hapus(rowid)
    {
        var jml_item = document.getElementById('jml_item').value;
        document.getElementById('jml_item').value = (jml_item*1) - (1*1);

        var row = document.getElementById(rowid);
        row.parentNode.removeChild(row);
    }

</script>

<script type="text/javascript">
var qty_modal = document.getElementById('qty_modal');
  qty_modal.addEventListener('keyup', function(e){
  qty_modal.value = formatRupiah(this.value, 'Rp. ');
});



/* Fungsi formatRupiah */
function formatRupiah(angka, prefix){
  var number_string = angka.replace(/[^,\d]/g, '').toString(),
  split   		= number_string.split(','),
  sisa     		= split[0].length % 3,
  rupiah     		= split[0].substr(0, sisa),
  ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);

  // tambahkan titik jika yang di input sudah menjadi angka ribuan
  if(ribuan){
    separator = sisa ? '.' : '';
    rupiah += separator + ribuan.join('.');
  }

  rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
  return prefix == undefined ? rupiah : (rupiah ? rupiah : '');
}
</script>
