<form method="post" enctype="multipart/form-data">
	<input type="file" name="file">
	<input type="submit" name="preview" value="Import">
</form>
<h3>Gudang Management <b><?=$tipe?></b></h3>
<div class="portlet light bordered">
  <div class="portlet-title">


    <form role="form" method="post">
        <div class="form-body">
          <div class="row">
            <div class="col-sm-4">
                <label>Nama Gudang</label>
                <input type="text" class="form-control" placeholder="Nama Gudang" id="nama_gudang" value="<?=isset($default['nama'])? $default['nama'] : ""?>" name="nama_gudang" required>
            </div>
            <div class="col-sm-4">
                <label>Keterangan</label>
                <input type="text" class="form-control" placeholder="Keterangan" id="keterangan" value="<?=isset($default['keterangan'])? $default['keterangan'] : ""?>" name="keterangan">
            </div>
            <div class="col-sm-4"></div>
            </div>
            </div>
        <br />
        <div class="form-actions">
            <a href='<?php echo base_url('Gudang_controller');?>' class='btn default'> Cancel</a>
            <?php if(helper_security("gudang_add") == 1){?>
            <button type="submit" class="btn blue" name="submit_gudang">Submit</button>
            <?php }?>
        </div>
      </form>

      <hr />
      <br />

      <div class="tools"> </div>
  </div>

<table class="table table-striped table-bordered table-hover" id="sample_1">
        <thead>
          <tr>
              <th><center>Gudang</center></th>
              <th><center>Keterangan</center></th>
              <th><center>Action</center></th>
          </tr>
        </thead>
        <tbody>
          <?php 	foreach($list_gudang as $row){ ?>
            <tr>
                <td><?php echo $row['nama'];?></td>
                <td><?php echo $row['keterangan'];?></td>
                <?php if ($row['gudang_id'] >= 5){?>
                <td>
                    <?php if(helper_security("gudang_edit") == 1){?>
                    <a href='<?php echo base_url('gudang_controller/Edit/'.$row['gudang_id'].'');?>' class='btn blue'><i class="fa fa-pencil"></i></a>
                    <?php }?>
                    <?php if(helper_security("gudang_delete") == 1){?>
                    <a href="javascript:dialogHapus('<?php echo base_url('gudang_controller/delete/'.$row['gudang_id'].'');?>')" class='btn red'><i class="fa fa-trash-o"></i></a></td>
                    <?php }?>
                <?php }else{?>
                <td><?php if(helper_security("gudang_edit") == 1){?><a href='<?php echo base_url('gudang_controller/Edit/'.$row['gudang_id'].'');?>' class='btn blue'><i class="fa fa-pencil"></i></a><?php }?></td>
                <?php }?>
            </tr>
            <?php
              }
            ?>
        </tbody>
    </table>
</div>

<script>
	function dialogHapus(urlHapus) {
	  if (confirm("Apakah anda yakin ingin menghapus ini ?")) {
		document.location = urlHapus;
	  }
	}
</script>
