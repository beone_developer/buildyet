<h3>List Pembelian</h3>
<div class="portlet light bordered">
	<div class="portlet-title">
		<div class="tools"></div>
	</div>

	<table class="table table-striped table-bordered table-hover" id="sample_1">
		<thead>
		<tr>
			<th width='15%'>
				<center>No Pembelian</center>
			</th>
			<th width='25%'>
				<center>Supplier</center>
			</th>
			<th width='15%'>
				<center>Tanggal</center>
			</th>
			<th width='25%'>
				<center>Keterangan</center>
			</th>
			<th width='20%'>
				<center>Action</center>
			</th>
			<!--			<th width='20%'>-->
			<!--				<center>Action Realisasi</center>-->
			<!--			</th>-->
		</tr>
		</thead>
		<tbody>
		<?php foreach ($list_purchase_header as $row) { ?>
			<tr>
				<td><small>
						<center><?php echo $row['purchase_no']; ?></center>
					</small></td>
				<td><small><?php echo $row['nsupplier']; ?></small></td>
				<td><small>
						<center><?php echo $row['trans_date']; ?></center>
					</small></td>
				<td><small><?php echo $row['keterangan']; ?></small></td>
				<?php
				$purch_no = str_replace("/", "-", $row['purchase_no']); //konfersi karena akan dianggap parameter
				?>
				<td>
					<center>
						<?php if ($row['flag'] == 1) { ?>
							<?php if (helper_security("pembelian_edit") == 1) { ?>
								<a href='<?php echo base_url('Purchase_order_controller/edit/' . $row['purchase_header_id'] . ''); ?>'
								   class='btn blue'><i class="fa fa-pencil"></i> </a>
							<?php } ?>
						<?php } ?>
						<?php if ($row['flag'] == 1) { ?>
							<?php if (helper_security("pembelian_delete") == 1) { ?>
								<a href="javascript:dialogHapus('<?php echo base_url('Purchase_order_controller/delete/' . $row['purchase_header_id'] . '/' . $purch_no . ''); ?>')"
								   class='btn red'><i class="fa fa-trash-o"></i> </a>
							<?php } ?>
						<?php } ?>
						<a href='<?php echo base_url('Purchase_order_controller/purchase_order_print/' . $row['purchase_header_id'] . ''); ?>'
						   class='btn yellow'><i class="fa fa-print"></i> </a></center>
				</td>
				<!--				<td>-->
				<!--					<center>-->
				<!--						--><?php //if ($row['flag'] == 0) { ?>
				<!--							<a href="javascript:dialogReal('-->
				<?php //echo base_url('Purchase_order_controller/realisasi/' . $row['purchase_header_id'] . ''); ?><!--')"-->
				<!--							   class='btn blue'><i class="fa fa-check"></i> </a>-->
				<!--						--><?php //} ?>
				<!--						--><?php //if ($row['flag'] == 1){ ?>
				<!--						<a href='-->
				<?php //echo base_url('Purchase_order_controller/purchase_order_print/' . $row['purchase_header_id'] . ''); ?><!--'-->
				<!--						   class='btn yellow'><i class="fa fa-print"></i> </a>-->
				<!--					</center>-->
				<!--					--><?php //} ?>
				<!--				</td>-->
			</tr>
			<?php
		}
		?>
		</tbody>
	</table>
</div>

<script>
	function dialogHapus(urlHapus) {
		if (confirm("Apakah anda yakin ingin menghapus ini ?")) {
			document.location = urlHapus;
		}
	}

	function dialogReal(urlReal) {
		if (confirm("Apakah anda yakin ingin merealisasi ini ?")) {
			document.location = urlReal;
		}
	}
</script>
