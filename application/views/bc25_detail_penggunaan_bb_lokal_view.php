<div class="row">
	<div class="col-sm-3">
		<a href="<?php echo base_url('Bc25_detail_controller');?>" class="btn btn-primary btn-sm" role="button" aria-pressed="true">Detail Barang</a>
	</div>
	<div style="margin-left: -176px;" class="col-sm-3">
		<a href="<?php echo base_url('Bc25_detail_penggunaan_bb_impor_controller');?>" class="btn btn-primary btn-sm" role="button" aria-pressed="true">Penggunaan Bahan Baku Impor</a>
	</div>
	<!-- <div style="margin-left: -75px;" class="col-sm-3">
		<a href="#" class="btn btn-primary btn-sm active" role="button" aria-pressed="true">Penggunaan Bahan Baku Lokal</a>
	</div>	 -->
</div>
<br>

<div class="portlet light bordered">
	<div class="portlet-title">
		
		<form action="" method="">
			
			<div class="row">
				<label for="status" class="col-sm-2 col-form-label">Status</label>
				<div class="col-sm-2">
					<input style="border: none; margin-top: -5px;" type="text" readonly class="form-control" id="status" name="status" value="LENGKAP">
				</div>
			</div>
			<br>

			<div class="form-group">
				<div class="row">
					<div class="col-sm-1">
						<label style="margin-top: 5px;">Detail Ke</label>
					</div>

					<div class="col-sm-1">
						<input type="text" readonly class="form-control" id="status" 
						name="status" value="">
					</div>

					<div class="col-sm-1">
						<label style="margin-top: 5px;">Dari</label>
					</div>

					<div class="col-sm-1">
						<input type="text" readonly class="form-control" id="status" 
						name="status" value="">
					</div>

					<div class="col-sm-2 col-sm-offset-1">
						<label style="margin-top: 5px;">Bahan Baku Ke</label>
					</div>

					<div class="col-sm-1">
						<input type="text" readonly class="form-control" id="status" 
						name="status" value="">
					</div>

					<div class="col-sm-1">
						<label style="margin-top: 5px;">Dari</label>
					</div>

					<div class="col-sm-1">
						<input type="text" readonly class="form-control" id="status" 
						name="status" value="">
					</div>
				</div>
			</div>
			
			<div class="form-group">
				<div class="row">
					<div class="col-sm-3">
						<label>Dok Asal</label>
						<input type="text" class="form-control" id="status" 
						name="status" value="">
					</div>
					<div class="col-sm-3">
						<input style="margin-top: 24px; border: none;" type="text" readonly class="form-control" id="status" 
						name="status" value="BC 2.3">
					</div>

					<div class="col-sm-3">
						<label>KPPBC Dok</label>
						<input type="text" class="form-control" id="status" 
						name="status" value="">
					</div>
					<div class="col-sm-3">
						<input style="margin-top: 24px; border: none;" type="text" readonly class="form-control" id="status" 
						name="status" value="KPPHGSS">
					</div>
					
				</div>
			</div>
				
			<div class="form-group">
				<div class="row">
					<div class="col-sm-2">
						<label>No / Tanggal Dok</label>
						<input type="text" class="form-control" id="status" 
						name="status" value="" placeholder="No">
					</div>
					<div class="col-sm-2">
						<input style="margin-top: 24px;" type="text" readonly class="form-control" id="status" 
						name="status" value="" placeholder="Tgl Dok">
					</div>

					<div class="col-sm-5"> 
						<label>No Aju</label>
						<input type="text" class="form-control" id="status" 
						name="status" value="">
					</div>
					<div class="col-sm-3">
						<label>Urut Ke</label>
						<input type="text" readonly class="form-control" id="status" 
						name="status" value="">
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="row">
					<div class="col-sm-6">
						<p><b>DATA BAHAN BAKU</b></p>
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="row">
					<div class="col-sm-6">
						<label>Kode</label>
						<input type="text" readonly class="form-control" id="status" 
						name="status" value="">
					</div>
					<div class="col-sm-6">
						<label>Nomor HS</label>
						<input type="text" readonly class="form-control" id="status" 
						name="status" value="">
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="row">
					<div class="col-sm-12">
						<label>Uraian Barang</label>
						<input type="text" readonly class="form-control" id="status" 
						name="status" value="">
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="row">
					<div class="col-sm-3">
						<label>Tipe</label>
						<input type="text"  class="form-control" id="status" name="status" value="">
					</div>
					<div class="col-sm-3">
						<label>Ukuran</label>
						<input type="text"  class="form-control" id="status" name="status" value="">
					</div>
					<div class="col-sm-3">
						<label>Spf Lain</label>
						<input type="text"  class="form-control" id="status" name="status" value="">
					</div>
					<div class="col-sm-3">
						<label>Merk</label>
						<input type="text"  class="form-control" id="status" name="status" value="">
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="row">
					<div class="col-sm-6">
						<p><b>HARGA & SATUAN</b></p>
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="row">
					<div class="col-sm-4">
						<label>Harga Perolehan</label>
						<input type="text"  class="form-control" id="status" name="status" value="">
					</div>
					<div class="col-sm-4">
						<label>Harga Penyerahan Rp.</label>
						<input type="text"  class="form-control" id="status" name="status" value="">
					</div>
					<div class="col-sm-4">
						<label>Netto</label>
						<input type="text"  class="form-control" id="status" name="status" value="">
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="row">
					<div class="col-sm-6">
						<label>Jumlah Satuan</label>
						<input type="text"  class="form-control" id="status" name="status" value="">
					</div>
					<div class="col-sm-6">
						<label>Satuan</label>
						<input type="text"  class="form-control" id="status" name="status" value="">
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="row">
					<div class="col-sm-6">
						<label><b>NILAI PPN</b></label>	
					</div>
					<div class="col-sm-6">
						<label><b>FASILITAS & SKEMA TARIF</b></label>	
					</div>
				</div>
			</div>

			<div class="form-body">
				<div class="row">
					<!-- FORM LEFT SIDE -->
					<div class="col-sm-6">

						<div class="form-group">
							<div class="row">
								<div class="col-sm-3">
									<label>PPN</label>
									<input type="text" class="form-control" name="harga_penyerahan" value="">
								</div>
								
								<div class="col-sm-1">
									<p style="margin-top: 30px; font-size: 15px;">%</p>
								</div>

								<div class="col-sm-4">
									<input style="margin-top: 25px;" type="text" class="form-control" name="harga_penyerahan" value="">
								</div>

								<div class="col-sm-3">
									<input style="margin-top: 25px;" type="text" class="form-control" name="harga_penyerahan" value="">
								</div>
								
								<div class="col-sm-1">
									<p style="margin-top: 30px; font-size: 15px;">%</p>
								</div>
							</div>
						</div>
						
						<div style="margin-top: -10px" class="form-group">
							<div class="row">
								<div class="col-sm-4">
									<label>PPN Bayar</label>
									
									<input style="border: none;" type="text" class="form-control" name="harga_penyerahan" readonly="" value="Rp">
								</div>

								<div class="col-sm-8">
									<input style="margin-top: 25px;" type="text" class="form-control" name="harga_penyerahan" value="">
								</div>
								
							</div>
						</div>

						<div style="margin-top: -10px" class="form-group">
							<div class="row">
								<div class="col-sm-4">
									<label>PPN Fasilitas</label>
									
									<input style="border: none;" type="text" class="form-control" name="harga_penyerahan" readonly="" value="Rp">
								</div>

								<div class="col-sm-8">
									<input style="margin-top: 25px;" type="text" class="form-control" name="harga_penyerahan" value="">
								</div>
								
							</div>
						</div>
					</div>
					<!-- TUTUP FORM LEFT SIDE -->
					
					<!-- FORM RIGHT SIDE -->
					<div class="col-sm-6">
						<div class="form-group">
							<div class="col-sm-6">
								<label>Fasilitas</label>
								<input type="text" class="form-control" name="harga_penyerahan" value="">
							</div>

							<div class="col-sm-6">
								<label>Skm Trf</label>
								<input type="text" class="form-control" name="harga_penyerahan" value="">
							</div>
						</div>
						<br><br><br>
						<div class="form-group">
							<div class="col-sm-12">
								<table class="table table-sm">
								  <thead>
								    <tr class="bg-success">
								      <th scope="col">Jenis</th>
								      <th scope="col">Nomor</th>
								      <th scope="col">Tanggal</th>
								    </tr>
								  </thead>
								  <tbody>
								    
								    <tr>
								      <th>1</th>
								      <td>2</td>
								      <td>3</td>
								    </tr>

								    <tr>
								      <th>1</th>
								      <td>2</td>
								      <td>3</td>
								    </tr>

								    <tr>
								      <th>1</th>
								      <td>2</td>
								      <td>3</td>
								    </tr>

								    <tr>
								      <th>1</th>
								      <td>2</td>
								      <td>3</td>
								    </tr>

								    <tr>
								      <th>1</th>
								      <td>2</td>
								      <td>3</td>
								    </tr>
								    
						  		  </tbody>
				 				</table>
							</div>
						</div>

					</div>
					<!-- TUTUP FORM RIGHT SIDE -->
				</div>
			</div>

		</form>

	</div>
</div>