<h3>List Kredit Note</h3>
<div class="portlet light bordered">
  <div class="portlet-title">
      <div class="tools"> </div>
  </div>

<table class="table table-striped table-bordered table-hover" id="sample_1">
        <thead>
          <tr>
              <th><center>Nomor</center></th>
              <th><center>Tanggal</center></th>
              <th><center>Keterangan</center></th>
              <th><center>Action</center></th>
          </tr>
        </thead>
        <tbody>
          <?php 	foreach($List_kredit_note as $row){ ?>
            <tr>
                <td><?php echo $row['nomor'];?></td>
                <td><?php echo $row['trans_date'];?></td>
                <td><?php echo $row['keterangan'];?></td>
                <td>
                    <?php if(helper_security("kredit_note_edit") == 1){?>
                    <a href='<?php echo base_url('Kredit_note_controller/edit/'.$row['hutang_piutang_id'].'/'.$row['nomor'].'');?>' class='btn blue'><i class="fa fa-pencil"></i></a>
                    <?php }?>
                    <?php if(helper_security("kredit_note_delete") == 1){?>
                    <a href="javascript:dialogHapus('<?php echo base_url('Kredit_note_controller/delete/'.$row['hutang_piutang_id'].'/'.$row['nomor'].'');?>')" class='btn red'><i class="fa fa-trash-o"></i></a>
                    <?php }?>
                </td>
            </tr>
            <?php
              }
            ?>
        </tbody>
    </table>
</div>

<script>
	function dialogHapus(urlHapus) {
	  if (confirm("Apakah anda yakin ingin menghapus ini ?")) {
		document.location = urlHapus;
	  }
	}
</script>
