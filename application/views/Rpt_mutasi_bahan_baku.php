<?php
$awal = helper_tanggalinsert($_GET['tglawal']);
$akhir = helper_tanggalinsert($_GET['tglakhir']);
?>
<h3>Report Mutasi Bahan Baku & Penolong</h3>
<h4>Periode <?php echo $awal;?> Sampai <?php echo $akhir;?></h4>
<div class="portlet light bordered">
  <div class="portlet-title">
      <div class="tools"> </div>
  </div>
  <a href="<?php echo base_url('Bc_controller/Print_mutasi_bahan_baku?tglawal='.$awal.'&tglakhir='.$akhir.''); ?>" class="btn btn-lg blue hidden-print margin-bottom-5"> Print
      <i class="fa fa-print"></i>
  </a>

<table class="table table-striped table-bordered table-hover" id="sample_2">
        <thead>
          <tr>
              <th colspan="1"><center></center></th>
              <th colspan="3"><center>Item</center></th>
              <th colspan="1"><center></center></th>
              <th colspan="1"><center></center></th>
              <th colspan="1"><center></center></th>
              <th colspan="1"><center></center></th>
              <th colspan="1"><center></center></th>
              <th colspan="1"><center></center></th>
              <th colspan="1"><center></center></th>
          <tr>
              <th><center><small>No</small></center></th>
							<th><center><small>Kode</small></center></th>
              <th><center><small>Nama</small></center></th>
              <th><center><small>Sat</small></center></th>
              <th><center><small>Saldo Awal</small></center></th>
              <th><center><small>Pemasukan</small></center></th>
              <th><center><small>Pengeluaran</small></center></th>
              <th><center><small>Penyesuaian</small></center></th>
              <th><center><small>Saldo Akhir</small></center></th>
              <th><center><small>Opname</small></center></th>
							<th><center><small>Selisih</small></center></th>
          </tr>
        </thead>
        <tbody>
          <?php
          $tgl_akhir_formated = date('d-m-Y');
          $thn_awal = substr($tgl_akhir_formated,6,4);
          $periode = $thn_awal."-01-01";

          $no = 0 ;

          $sql_saldo_awal_item = $this->db->query("SELECT i.item_code, i.item_id, i.saldo_qty, i.nama as nitem, s.keterangan
                                                    FROM public.beone_item i INNER JOIN public.beone_item_type t ON i.item_type_id = t.item_type_id INNER JOIN public.beone_satuan_item s ON i.satuan_id = s.satuan_id
                                                    WHERE i.item_type_id = 1 OR i.item_type_id = 11 ORDER BY i.item_code ASC");

          $date = $awal;
          $date1 = str_replace('-', '/', $date);
          $yesterday = date('Y-m-d',strtotime($date1 . "-1 days"));

           foreach($sql_saldo_awal_item->result_array() as $row){
             $no = $no + 1;

             $saldo_awal_sampai_tgl = $this->db->query("SELECT SUM(qty_in) as qin, SUM(qty_out) as qout FROM public.beone_inventory WHERE trans_date BETWEEN '$periode' AND '$yesterday' AND intvent_trans_no NOT LIKE 'PNY%' AND intvent_trans_no NOT LIKE 'ADJ%' AND item_id = ".intval($row['item_id']));
          	 $hasil_saldo_awal = $saldo_awal_sampai_tgl->row_array();

          	 $mutasi = $this->db->query("SELECT SUM(qty_in) as qin, SUM(qty_out) as qout FROM public.beone_inventory WHERE trans_date BETWEEN '$awal' AND '$akhir' AND intvent_trans_no NOT LIKE 'PNY%' AND intvent_trans_no NOT LIKE 'ADJ%' AND item_id = ".intval($row['item_id']));
          	 $hasil_mutasi = $mutasi->row_array();

          	 $adj = $this->db->query("SELECT SUM(qty_in) as qin, SUM(qty_out) as qout FROM public.beone_inventory WHERE trans_date BETWEEN '$awal' AND '$akhir' AND intvent_trans_no LIKE 'ADJ%' AND item_id = ".intval($row['item_id']));
          	 $hasil_adj = $adj->row_array();
          	 $opname = $hasil_adj['qin'] - $hasil_adj['qout'];

          	 $pny = $this->db->query("SELECT SUM(qty_in) as qin, SUM(qty_out) as qout FROM public.beone_inventory WHERE trans_date BETWEEN '$awal' AND '$akhir' AND intvent_trans_no LIKE 'PNY%' AND item_id = ".intval($row['item_id']));
          	 $hasil_pny = $pny->row_array();
          	 $penyesuaian = $hasil_pny['qin'] - $hasil_pny['qout'];

          	 $saldo_awal = ($row['saldo_qty'] + $hasil_saldo_awal['qin']) - $hasil_saldo_awal['qout'];
          	 $mutasi_in = $hasil_mutasi['qin'];
          	 $mutasi_out = $hasil_mutasi['qout'];
          	 //$saldo_akhir = ($saldo_awal + $mutasi_in) - $mutasi_out;
          	 $saldo_akhir = (($saldo_awal + $mutasi_in) - $mutasi_out) + $penyesuaian;

          	 $ktr = '';

          	 if (abs($opname) == 0 AND abs($penyesuaian) == 0){
          		$selisih = 0;
          	}else{
              $selisih = (($opname+$saldo_akhir) + $penyesuaian) - $saldo_akhir;

          			if ($selisih > 1){
          		 	 $ktr = 'Selisih Lebih';
          		 }else if($selisih < 1){
          		 	 $ktr = 'Selisih Kurang';
          		  }else{
          		 	 $ktr = '';
          		  }
          	}

					?>
            <tr>
                <td><center><small><?php echo $no;?></small></center></td>
                <td><small><?php echo $row['item_code'];?></small></td>
                <td><small><?php echo $row['nitem'];?></small></td>
								<td><small><?php echo $row['keterangan'];?></small></td>
                <td><small><?php echo number_format($saldo_awal,2);?></small></td>
								<td><small><?php echo number_format($mutasi_in,2);?></small></td>
                <td><small><?php echo number_format($mutasi_out,2);?></small></td>
                <td><small><?php echo number_format($penyesuaian,2);;?></small></td>
								<td><small><?php echo number_format($saldo_akhir,2);?></small></td>
                <!--<td><small><?php //echo number_format(abs($opname+$saldo_akhir), 2);?></small></td>
                <td><small><?php //echo number_format($selisih, 2);?></small></td>-->
                <td><small></small></td>
                <td><small></small></td>
            </tr>
            <?php
            }
            ?>
        </tbody>
    </table>
</div>
