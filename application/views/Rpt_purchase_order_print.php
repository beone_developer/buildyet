<!DOCTYPE html>
<html>
<head>
  <title>Report Table</title>
  <style type="text/css">
    #outtable{
      padding: 20px;
      border:1px solid #e3e3e3;
      width:600px;
      border-radius: 5px;
    }
 
    .short{
      width: 50px;
    }
 
    .normal{
      width: 150px;
    }

    p{
      font-family: arial;
      font-size:8pt;
      color:#5E5B5C;
    }
 
    table{
      border-collapse: collapse;
      border: 1px solid black;
      font-family: arial;
      font-size:11pt;
      color:#5E5B5C;
      width:100%;
    }
    .text-center{
        text-align:center;
    }
    
    .text-right{
        text-align:right;
    }
    
    .text-left{
        text-align:left;
    }
    
    .cell-border{
      border-collapse: collapse;
      border: 1px solid black;
      padding:5px;
    }
    .left-border{
      border-collapse: collapse;
      border-left: 1px solid black;
    }
    .top-border{
      border-collapse: collapse;
      border-top: 1px solid black;
    }
    .u{
        text-decoration: underline;
    }
    td{
        padding:5px;
    }
  </style>
</head>
<body>
    <div id="header">
        <table >
            <tbody>
                <tr>
                    <td width="70%"></td>
                    <td class="left-border" width="10%">No. PO</td>
                    <td width="2%">:</td>
                    <td width="28%"><?php echo $default['purchase_no'] ?></td>
                </tr>
                <tr>
                    <td width="70%"></td>
                    <td class="left-border" width="10%">Tanggal</td>
                    <td width="2%">:</td>
                    <td width="28%"><?php echo $default['trans_date'] ?></td>
                </tr>
                <tr>
                    <td width="70%"></td>
                    <td class="left-border" width="10%">No. PR</td>
                    <td width="2%">:</td>
                    <td width="28%">3325476</td>
                </tr>
            </tbody>
        </table>
        <table>
            <tbody>
                <tr>
                    <td width="50%" rowspan="3">
                        Kepada : <?php echo $default['nsupplier'] ?>
                    </td>
                    <td width="20%">Attention</td>
                    <td width="2%">:</td>
                    <td width="28%"></td>
                </tr>
                <tr>
                    <td width="20%">Fax</td>
                    <td width="2%">:</td>
                    <td width="28%"></td>
                </tr>
                <tr>
                    <td width="20%">Tgl. Kirim</td>
                    <td width="2%">:</td>
                    <td width="28%"><?php echo $default['trans_date'] ?></td>
                </tr>
                <tr>
                    <td width="50%"><u>Harap kirim barang-barang tersebut dibawah ini:<u></td>
                    <td width="20%">Jangka Waktu Kredit</td>
                    <td width="2%">:</td>
                    <td width="28%">0 Hari</td>
                </tr>
                <tr>
                    <td width="50%">Please supply the following goods/material</td>
                    <td width="20%"></td>
                    <td width="2%"></td>
                    <td width="28%" style="text-align:right;">Hal. : 1 dari 1</td>
                </tr>
            </tbody>
        </table>
        <table>
            <thead>
                <tr>
                    <th class="cell-border text-center" width="5%">No</th>
                    <th class="cell-border text-center" width="15%">Kode Barang</th>
                    <th class="cell-border text-center" width="25%">Keterangan <br>Discription</br></th>
                    <th class="cell-border text-center" width="10%">Jumlah <br>Quantity</br></th>
                    <th class="cell-border text-center" width="5%">Sat</th>
                    <th class="cell-border text-center" width="20%">Harga Satuan <br>Unit Price</br></th>
                    <th class="cell-border text-center" width="20%">Jumlah Uang <br>Total Amount</br></th>
                </tr>
            </thead>
            <tbody>
                <?php $no=1; ?>
                <?php foreach($default_detail as $user): 
                    $sql = $this->db->query("SELECT i.item_code, i.nama, s.satuan_code FROM beone_item i
                                            LEFT JOIN beone_po_import_detail d on i.item_id = d.item_id
                                            LEFT JOIN beone_satuan_item s on i.satuan_id = s.satuan_id
                                            WHERE d.purchase_detail_id =".intval($user['purchase_detail_id']));
                    $item = $sql->result_array();
                    ?>
                <tr>
                    <td class="left-border text-center"><?php echo $no; ?></td>
                    <td class="left-border text-center"><?php echo $item[0]['item_code']; ?></td>
                    <td class="left-border"><?php echo $item[0]['nama']; ?></td>
                    <td class="left-border text-center"><?php echo $user['qty']; ?></td>
                    <td class="left-border text-center"><?php echo $item[0]['satuan_code']; ?></td>
                    <td class="left-border text-right"><?php echo $user['price']; ?></td>
                    <td class="left-border text-right"><?php echo $user['amount']; ?></td>
                </tr>
                <?php $no++; ?>
                <?php endforeach; ?>
                <tr>
                    <td class="top-border" width="60%" colspan="5" rowspan="3">
                        Catatan : 
                    </td>
<!--                    <td class="top-border left-border text-right" width="20%">DPP</td>-->
<!--                    <td class="top-border left-border text-right" width="20%">--><?php //echo number_format((float)$default['subtotal'], 2, '.', ''); ?><!--</td>-->
                </tr>
<!--                <tr>-->
<!--                    <td class="left-border text-right" width="20%">PPN</td>-->
<!--                    <td class="left-border text-right u" width="20%">--><?php //echo number_format((float)$default['ppn_value'], 2, '.', ','); ?><!--</td>-->
<!--                </tr>-->
                <tr>
                    <td class="left-border text-right" width="20%">Total</td>
                    <td class="left-border text-right u" width="20%"><?php echo number_format((float)$default['grandtotal'], 2, '.', ''); ?></td>
                </tr>
            </tbody>
        </table>
        <table>
            <tbody>
                <tr>
                    <td width="40%" rowspan="5">
                        Bila pengiriman barang diatas bertahap harap dicantumkan nomor "PURCHASE ORDER"
                        ini pada setiap pengiriman & tagihan PO lembar asli ini mohon dikembalikan bersama
                        Tagihan / Faktur terakhir.
                    </td>
                    <td width="20%">Agreed by,</td>
                    <td class="text-center" width="20%" colspan="2">E.&.O.E.</td>
                </tr>
                <tr>
                    <td width="20%"></td>
                    <td width="20%"></td>
                    <td width="20%"></td>
                </tr>
                <tr>
                    <td width="20%"></td>
                    <td width="20%"></td>
                    <td width="20%"></td>
                </tr>
                <tr>
                    <td width="20%">_______________</td>
                    <td width="20%">_______________</td>
                    <td width="20%">_______________</td>
                </tr>
                <tr>
                    <td width="20%" style="text-decoration:italic;font-size:8pt;">SUPLIER</td>
                    <td width="20%" style="text-decoration:italic;font-size:8pt;">MANAGING DIRECTOR</td>
                    <td width="20%" style="text-decoration:italic;font-size:8pt;">PURCHASE DEPARTMENT</td>
                </tr>
            </tbody>
        </table>
        <p>Lembar : (1) Suplier, (2) Pembelian, (3) Hutang, (4) Gudang</p>
	 </div>
</body>
</html>
