<!-- BEGIN PAGE TITLE-->
<!-- END PAGE TITLE-->
<!-- END PAGE HEADER-->
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"></script>

<div class="portlet box blue ">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-gift"></i> Form Mutasi Stock
		</div>
		<div class="tools">
			<a href="" class="collapse"> </a>
			<a href="#portlet-config" data-toggle="modal" class="config"> </a>
			<a href="" class="reload"> </a>
			<a href="" class="remove"> </a>
		</div>
	</div>
	<div class="portlet-body form">


		<form role="form" method="post">
			<div class="form-body">

			<div class="row">
                  <div class="col-sm-5"><h3><b><?php echo $default['mutasi_stok_no'];?></b></h3></div>
                  <input type"text" name="mutasi_stok_no" value='<?php echo $default['mutasi_stok_no'];?>' hidden>
                  <div class="col-sm-7"><h3><?php echo $default['trans_date'];?></h3></div>
                  <input type"text" name="mutasi_stok_date" value='<?php echo helper_tanggalupdate($default['trans_date']);?>' hidden>
                </div>

				<div class="row">
				  <div class="col-sm-4"><h3><?php echo $default['ngudangasal'];?></h3></div>
                  <input type"text" name="gudangAsal" value='<?php echo $default['gudang_id_asal'];?>' hidden>
                  <div class="col-sm-4"><h3><?php echo $default['ngudangtujuan'];?></h3></div>
                  <input type"text" name="gudangTujuan" value='<?php echo $default['gudang_id_tujuan'];?>' hidden>
				</div>

				<hr
				/ style="border-color: #3598DC;">
				<table id="datatable" class="table striped hovered cell-hovered">
					<thead>
					<tr>
						<td width="5%">ID</td>
						<td width="20%">Item</td>
						<td width="15%">Qty</td>
						<td width="5%"></td>
					</tr>
					</thead>
					<tbody id="container">
					<?php
                            $ctr = 0;
                            foreach($default_detail as $row){
                            $ctr = $ctr + 1;
                            $nama_ctr = "".$ctr;
                            $nama_item = "item_".$nama_ctr;
                            $nama_item_id = "item_id_".$nama_ctr;
                            $nama_qty = "qty_".$nama_ctr;
                            $nama_gudang_asal = "gudang_asal_".$nama_ctr;
                            $nama_gudang_asal_id = "gudang_asal_id_".$nama_ctr;
                            $nama_gudang_tujuan = "gudang_tujuan_".$nama_ctr;
                            $nama_gudang_tujuan_id = "gudang_tujuan_id_".$nama_ctr;
                            $rowss = "rows_".$nama_ctr;
                            $detail_id = "detail_id_".$nama_ctr;
                          ?>

							<tr class="records" id="'+count+'">
								<td><input class="form-control" id='<?php echo $nama_item_id;?>' name='<?php echo $nama_item_id;?>' type="text" value='<?php echo $row['item_id'];?>' readonly></td>
								<td><input class="form-control" id='<?php echo $nama_item;?>' name='<?php echo $nama_item;?>' type="text" value='<?php echo $row['item_name'];?>' readonly></td>
								<td><input class="form-control" id='<?php echo $nama_qty;?>' name='<?php echo $nama_qty;?>' type="text" value='<?php echo number_format($row['qty'],2);?>' readonly></td>
								<!-- <td><input class="form-control" id='<?php echo $nama_gudang_asal;?>' name='<?php echo $nama_gudang_asal;?>' type="text" value='<?php echo $row['ngudang_asal'];?>' readonly></td>
								<td><input class="form-control" id='<?php echo $nama_gudang_tujuan;?>' name='<?php echo $nama_gudang_tujuan;?>' type="text" value='<?php echo $row['ngudang_tujuan'];?>' readonly></td> -->
								<td><input id='<?php echo $rowss;?>' name="rows[]" value='<?php echo $nama_ctr;?>' type="hidden"></td></td>
								<td><button type="button" class="btn red" onclick="hapus('<?php echo $nama_ctr;?>')">X</button></td>
								<!-- <td><input class="form-control" id='<?php echo $nama_gudang_asal_id;?>' name='<?php echo $nama_gudang_asal_id;?>' type="hidden" value='<?php echo $row['gudang_asal_id'];?>' readonly></td>
								<td><input class="form-control" id='<?php echo $nama_gudang_tujuan_id;?>' name='<?php echo $nama_gudang_tujuan_id;?>' type="hidden" value='<?php echo $row['gudang_tujuan_id'];?>' readonly></td> -->
								<td><input id='<?php echo $detail_id;?>' name='<?php echo $detail_id;?>' value='<?php echo $row['mutasi_stok_detail_id'];?>' type="hidden"></td></tr>
							</tr>
						<?php
                            }
                           ?>
					</tbody>
				</table>

			</div>
			<div class="form-actions">
                <a class="btn blue" data-toggle="modal" href="#responsive"> Tambah Data </a>
				<?php //if (helper_security("pembelian_add") == 1) { ?>
					<button type="submit" class="btn red" name="submit_mutasi">Submit</button>
				<?php //} ?>
			</div>
		</form>
	</div>
</div>


<!--------------------------- MODAL ADD ITEM--------------------------------------------->
<div id="responsive" class="modal fade" tabindex="-1" data-width="760">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Detail Item</h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-6">
				<div class="form-group">
					<label>Item</label>
					<select id='item_modal' class='form-control input-sm select2-multiple' name='item_modal' required>
					<option value=0><?php echo " - Pilih Item - ";?></option>
					<?php  foreach($list_item as $row){ echo '<option value='.$row['item_id'].'>'.$row['nama'].'</option>';} ?>
					</select>
				</div>
			</div>
            <div class="col-md-4">
                <div class="form-group">
                <label>Quantity</label>
                <input type='text' class='form-control' placeholder="Quantity" name='qty_modal' id='qty_modal' required>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
        <button type="button" class="btn green" name="add_btn" id="add_btn">Insert</button>
    </div>
</div>
<script>
	$(document).ready(function() {
		var count = -1;
		$('tr').each(function (i) {
			count += 1;
		});

    	$("#add_btn").click(function(){
					count += 1;
		
          var item = document.getElementById('item_modal');
          var qty = document.getElementById('qty_modal');
		  var namaItem = $('#item_modal option:selected').text();

					$('#container').append('<tr class="records" id="'+count+'">'
							+ '<td><input class="form-control" id="item_id_' + count + '" name="item_id_'+count+'" type="text" value="'+item.value+'" readonly></td>'
							+ '<td><input class="form-control" id="item_' + count + '" name="item_'+count+'" type="text" value="'+namaItem+'" readonly></td>'
							+ '<td><input class="form-control" id="qty_' + count + '" name="qty_'+count+'" type="text" value="'+qty.value+'" readonly></td>'
							+ '<td><input id="rows_' + count + '" name="rows[]" value="'+ count +'" type="hidden"></td>'
							+ '<td><button type="button" class="btn red" onclick="hapus('+count+')">X</button></td></tr>'
					);

          eraseText();
          $('#responsive').modal('hide');
				});

				$(".remove_item").live('click', function (ev) {
    			if (ev.type == 'click') {
	        	$(this).parents(".records").fadeOut();
	        	$(this).parents(".records").remove();
        	}
     		});
		});

    function eraseText() {
     document.getElementById("item_modal").value = "";
     document.getElementById("qty_modal").value = "";
	}

	function hapus(rowid)
    {
        var row = document.getElementById(rowid);
        row.parentNode.removeChild(row);
    }
</script>