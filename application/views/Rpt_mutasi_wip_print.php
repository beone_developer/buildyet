<?php
$awal = $_GET['tglawal'];
$tgl_awal = substr($awal,8,2);
$bulan_awal = substr($awal,5,2);
$tahun_awal = substr($awal,0,4);

$bln_awal = $bulan_awal * 1;

$akhir = $_GET['tglakhir'];
$tgl_akhir = substr($akhir,8,2);
$bulan_akhir = substr($akhir,5,2);
$tahun_akhir = substr($akhir,0,4);

$bln_akhir = $bulan_akhir * 1;

$periode = $tahun_awal."-01-01";

if ($bln_awal == 1){
	$spell_bulan_awal = "Januari";
}else if($bln_awal == 2){
	$spell_bulan_awal = "Februari";
}else if($bln_awal == 3){
	$spell_bulan_awal = "Maret";
}else if($bln_awal == 4){
	$spell_bulan_awal = "April";
}else if($bln_awal == 5){
	$spell_bulan_awal = "Mei";
}else if($bln_awal == 6){
	$spell_bulan_awal = "Juni";
}else if($bln_awal == 7){
	$spell_bulan_awal = "Juli";
}else if($bln_awal == 8){
	$spell_bulan_awal = "Agustus";
}else if($bln_awal == 9){
	$spell_bulan_awal = "September";
}else if($bln_awal == 10){
	$spell_bulan_awal = "Oktober";
}else if($bln_awal == 11){
	$spell_bulan_awal = "November";
}else if($bln_awal == 12){
	$spell_bulan_awal = "Desember";
}



if ($bln_akhir == 1){
	$spell_bulan_akhir = "Januari";
}else if($bln_akhir == 2){
	$spell_bulan_akhir = "Februari";
}else if($bln_akhir == 3){
	$spell_bulan_akhir = "Maret";
}else if($bln_akhir == 4){
	$spell_bulan_akhir = "April";
}else if($bln_akhir == 5){
	$spell_bulan_akhir = "Mei";
}else if($bln_akhir == 6){
	$spell_bulan_akhir = "Juni";
}else if($bln_akhir == 7){
	$spell_bulan_akhir = "Juli";
}else if($bln_akhir == 8){
	$spell_bulan_akhir = "Agustus";
}else if($bln_akhir == 9){
	$spell_bulan_akhir = "September";
}else if($bln_akhir == 10){
	$spell_bulan_akhir = "Oktober";
}else if($bln_akhir == 11){
	$spell_bulan_akhir = "November";
}else if($bln_akhir == 12){
	$spell_bulan_akhir = "Desember";
}


// intance object dan memberikan pengaturan halaman PDF
$pdf = new FPDF('p','mm','A4');


// membuat halaman baru
$pdf->AddPage();
// setting jenis font yang akan digunakan
$pdf->SetFont('Arial','B',12);
// mencetak string
$pdf->Cell(190,6,'LAPORAN POSISI BARANG DALAM PROSES (WIP)',0,1,'C');
$pdf->SetFont('Arial','B',12);
$pdf->Cell(190,6,'BUILDYET INDONESIA, PT',0,1,'C');
$pdf->SetFont('Arial','B',12);
$pdf->Cell(190,6,'WIP S.D '.$tgl_akhir.'-'.$spell_bulan_akhir.'-'.$tahun_akhir,0,1,'C');

// Memberikan space kebawah agar tidak terlalu rapat
$pdf->Cell(10,7,'',0,1);

$pdf->SetFont('Arial','B',8);
$pdf->Cell(15,12,'NO',1,0, 'C');
$pdf->Cell(30,12,'KODE BARANG',1,0, 'C');
$pdf->Cell(55,12,'NAMA BARANG',1,0, 'C');
$pdf->Cell(10,12,'SAT',1,0, 'C');
$pdf->Cell(35,12,'JUMLAH',1,0, 'C');
$pdf->Cell(40,12,'KETERANGAN',1,0, 'C');
$pdf->Ln();
$pdf->Cell(10,3,'',0,1);

$pdf->SetFont('Arial','',7);

$this->mysql = $this ->load -> database('mysql', TRUE);

$sql_item_keluar = $this->db->query("SELECT i.item_code, i.nama, i.item_id, sum(gd.qty_out) as hasil, i.nama as nitem, s.keterangan
																				FROM public.beone_item i INNER JOIN public.beone_gudang_detail gd ON i.item_id = gd.item_id INNER JOIN public.beone_satuan_item s ON i.satuan_id = s.satuan_id
																				WHERE gd.trans_date BETWEEN '$awal' AND '$akhir' AND gd.nomor_transaksi LIKE 'PAK%' AND gd.gudang_id = 1 group by i.item_id, s.keterangan ");

$sql_saldo_awal_item = $this->db->query("SELECT i.item_code, i.item_id, i.saldo_qty, i.nama as nitem, s.keterangan
																				FROM public.beone_item i INNER JOIN public.beone_item_type t ON i.item_type_id = t.item_type_id INNER JOIN public.beone_satuan_item s ON i.satuan_id = s.satuan_id
																				WHERE i.item_type_id = 2 ORDER BY i.item_code ASC");

$no = 0;

foreach($sql_item_keluar->result_array() as $row2){
	$no = $no + 1;

	 $pdf->Cell(15,4,$no,1,0, 'C');
	 $pdf->Cell(30,4,$row2['item_code'],1,0);
	 $pdf->Cell(55,4,$row2['nama'],1,0);
	 $pdf->Cell(10,4,$row2['keterangan'],1,0, 'C');
	 $pdf->Cell(35,4,number_format($row2['hasil'],4),1,0, 'R');
	 $pdf->Cell(40,4,' ',1,0, 'C');
	 $pdf->Ln();
}

 foreach($sql_saldo_awal_item->result_array() as $row){
	 $no = $no + 1;

	$saldo_awal_sampai_tgl = $this->db->query("SELECT SUM(qty_in) as qin, SUM(qty_out) as qout FROM public.beone_inventory WHERE trans_date BETWEEN '$periode' AND '$awal' AND item_id = ".intval($row['item_id']));
	$hasil_saldo_awal = $saldo_awal_sampai_tgl->row_array();

	$mutasi = $this->db->query("SELECT SUM(qty_in) as qin, SUM(qty_out) as qout FROM public.beone_inventory WHERE trans_date BETWEEN '$awal' AND '$akhir' AND item_id = ".intval($row['item_id']));
	$hasil_mutasi = $mutasi->row_array();

	$saldo_awal = ($row['saldo_qty'] + $hasil_saldo_awal['qin']) - $hasil_saldo_awal['qout'];
	$mutasi_in = $hasil_mutasi['qin'];
	$mutasi_out = $hasil_mutasi['qout'];
	$saldo_akhir = ($saldo_awal + $mutasi_in) - $mutasi_out;


		$pdf->Cell(15,4,$no,1,0, 'C');
		$pdf->Cell(30,4,$row['item_code'],1,0);
		$pdf->Cell(55,4,$row['nitem'],1,0);
		$pdf->Cell(10,4,$row['keterangan'],1,0, 'C');
		$pdf->Cell(35,4,number_format($saldo_akhir,4),1,0, 'R');
		$pdf->Cell(40,4,' ',1,0, 'C');
    $pdf->Ln();
}
date_default_timezone_set('Asia/Jakarta');
$jam=date("H_i_s");
$pdf->Output('','WIP_S.D_'.$tgl_akhir.'-'.$spell_bulan_akhir.'-'.$tahun_akhir.'_'.$jam.'.pdf');
?>
