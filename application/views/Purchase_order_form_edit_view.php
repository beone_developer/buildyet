<!-- BEGIN PAGE TITLE-->
<!-- END PAGE TITLE-->
<!-- END PAGE HEADER-->
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"></script>
<?php //$list_item = $this->db->query("SELECT * FROM public.beone_item"); ?>

<div class="portlet box blue ">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-gift"></i> Form Pembelian
		</div>
		<div class="tools">
			<a href="" class="collapse"> </a>
			<a href="#portlet-config" data-toggle="modal" class="config"> </a>
			<a href="" class="reload"> </a>
			<a href="" class="remove"> </a>
		</div>
	</div>
	<div class="portlet-body form">
		<form role="form" method="post">
			<div class="form-body">
				<?php
				$error = $this->session->flashdata('error');
				if (isset($error)) {
					echo '<div class="alert alert-danger" role ="alert">' . $error . '</div>';
				}
				$success = $this->session->flashdata('success');
				if (isset($success)) {
					echo '<div class="alert alert-success" role ="alert">' . $success . '</div>';
				}
				?>
				<!-- <div class="row">
					<div class="col-sm-5"><h3><b><?php echo $default['purchase_no']; ?></b></h3></div>
					<input type"text" name="nomor_po" value='<?php echo $default['purchase_no']; ?>' hidden>
					<div class="col-sm-7"><h3><?php echo $default['trans_date']; ?></h3></div>
					<input type"text" name="tanggal" value='<?php echo helper_tanggalupdate($default['trans_date']); ?>' hidden>
				</div>

				<div class="row">
					<div class="col-sm-5"><h4><?php echo $default['nsupplier']; ?></h4></div>
					<input type"text" name="supplier" value='<?php echo $default['supplier_id']; ?>' hidden>
					<div class="col-sm-7"><h3><?php echo $default['keterangan']; ?></h3></div>
					<input type"text" name="keterangan_header" value='<?php echo $default['keterangan']; ?>' hidden>
				</div> -->

				<div class="row">
					<div class="col-sm-4">
						<div class="col-sm-5"><h3><b><?php echo $default['purchase_no']; ?></b></h3></div>
						<input type"text" name="nomor_po" value='<?php echo $default['purchase_no']; ?>' hidden>
					</div>
					<div class="col-sm-8">
						<div class="form-group">
							<label>Tanggal</label>
							<div class="input-group">
                            <span class="input-group-addon input-circle-left">
                                <i class="fa fa-calendar"></i>
                            </span>
								<input
									class="form-control form-control-inline input-medium date-picker input-circle-right"
									size="16" type="text" name="tanggal"
									value="<?php $date = date_create($default['trans_date']);
									echo date_format($date, "m/d/Y"); ?>" readonly
									required/>
								<span class="help-block"></span>
							</div>
						</div>
					</div>
				</div>

				<div class="row">

					<div class="col-sm-4">
						<div class="form-group">
							<label>Supplier</label>
							<div class="input-group">
								<select id="select2-single-input-sm" class="form-control input-sm select2-multiple"
										name="supplier" required>
									<?php
									$supplier = $this->db->query("SELECT * FROM public.beone_custsup WHERE custsup_id =" . intval($default['supplier_id']));
									$hasil_supplier = $supplier->row_array();

									foreach ($list_supplier

									as $row){
									?>
									<option
										<?php echo ($default['supplier_id'] == $row['custsup_id']) ? 'selected="selected"' : ''; ?>
										value='<?php echo $row['custsup_id']; ?>'><?php echo $row['nama']; ?></opiton>
										<?php
										}
										?>
								</select>
							</div>
						</div>
					</div>

					<div class="col-sm-8">
						<div class="form-group">
							<label>Keterangan</label>
							<input type="text" class="form-control" placeholder="Keterangan" id="keterangan_header"
								   name="keterangan_header" value='<?php echo $default['keterangan']; ?>' required>
						</div>
					</div>
				</div>


				<hr
				/ style="border-color: #3598DC;">
				<table id="datatable" class="table striped hovered cell-hovered">
					<thead>
					<tr>
						<td width="20%">Item</td>
						<td width="15%">Qty</td>
						<!-- <td width="20%">Satuan Qty</td> -->
						<td width="15%">Price</td>
						<td width="15%">Amount</td>
						<td width="10%"></td>
					</tr>
					</thead>
					<tbody id="container">

					<?php
					$default_subtotal = 0;
					$ctr = 0;
					// $ppn_value = 0;

					foreach ($default_detail as $row) {
						$ctr = $ctr + 1;
						$nama_ctr = "" . $ctr;
						$nama_item = "item_" . $nama_ctr;
						$nama_item_id = "item_id_" . $nama_ctr;
						$nama_qty = "qty_" . $nama_ctr;
						$satuan = "satuan_" . $nama_ctr;
						$satuan_id = "satuan_id_" . $nama_ctr;
						$nama_price = "price_" . $nama_ctr;
						$nama_amount = "amount_" . $nama_ctr;
						$rowss = "rows_" . $nama_ctr;
						$detail_id = "detail_id_" . $nama_ctr;

						$default_subtotal = $default_subtotal + $row['amount'];

						$sql_cari_nama_item = $this->db->query("SELECT * FROM public.beone_item WHERE item_id = " . intval($row['item_id']));
						$hasil_cari_nama_item = $sql_cari_nama_item->row_array();
						$nitem = $hasil_cari_nama_item['nama'];

						?>

						<tr class='records' id='<?php echo $nama_ctr; ?>'>
							<td><input class="form-control" id='<?php echo $nama_item; ?>'
									   name='<?php echo $nama_item; ?>' type="text" value='<?php echo $nitem; ?>'
									   readonly></td>
							<td><input class="form-control" id='<?php echo $nama_qty; ?>'
									   name='<?php echo $nama_qty; ?>' type="text"
									   value='<?php echo number_format($row['qty'], 4, ',', '.'); ?>' readonly></td>
							<td><input class="form-control" id='<?php echo $nama_price; ?>'
									   name='<?php echo $nama_price; ?>' type="text"
									   value='<?php echo number_format($row['price'], 4, ',', '.'); ?>' readonly></td>
							<td><input class="form-control" id='<?php echo $nama_amount; ?>'
									   name='<?php echo $nama_amount; ?>' type="text"
									   value='<?php echo number_format($row['amount'], 4, ',', '.'); ?>' readonly></td>
							<td>
								<button type="button" class="btn red" onclick="hapus('<?php echo $nama_ctr; ?>')">X
								</button>
							</td>
							<td><input class="form-control" id='<?php echo $nama_item_id; ?>'
									   name='<?php echo $nama_item_id; ?>' type="hidden"
									   value='<?php echo $row['item_id']; ?>' readonly></td>
							<td><input class="form-control" id='<?php echo $satuan_id; ?>'
									   name='<?php echo $satuan_id; ?>' type="hidden"
									   value='<?php echo $row['satuan_id']; ?>' readonly></td>
							<td><input id='<?php echo $rowss; ?>' name="rows[]" value='<?php echo $nama_ctr; ?>'
									   type="hidden"></td>
							</td>
							<td><input id='<?php echo $detail_id; ?>' name='<?php echo $detail_id; ?>'
									   value='<?php echo $row['purchase_detail_id']; ?>' type="hidden"></td>
						</tr>

						<?php
					}

					// if ($default['ppn'] == 0){
					//    $ppn_value = 0;
					// }else if ($default['ppn'] == 1){
					//    $ppn_value = ($default_subtotal * 10) / 100;
					// }else{
					//    $default_subtotal =  $default_subtotal / 1.1;
					//    $ppn_value = ($default_subtotal * 10) / 100;
					// }

					$default_grandtotal = $default_subtotal;

					?>

					</tbody>
				</table>

				<div class="row">
					<div class="col-sm-4"></div>
					<div class="col-sm-4" style="text-align: right;"><b>Grand Total Custom</b></div>
					<div class="col-sm-4">
						<input type="text" class="form-control" placeholder="Sub Total" name="subtotal" id="subtotal"
							   value='<?php echo number_format($default['grandtotal'], 4, ',', '.'); ?>'>
						<br/>
						<input type="text" class="form-control" placeholder="Sub Total" name="subtotal2" id="subtotal2"
							   value='<?php echo number_format($default_subtotal, 4, ',', '.'); ?>' readonly>
						<br/>
						<hr style="border-color: #3598DC;">
						<input type="text" class="form-control" placeholder="Grand Total" name="grandtotal"
							   id="grandtotal" value='<?php echo number_format($default_grandtotal, 4, ',', '.'); ?>'
							   readonly>
					</div>
				</div>


			</div>
			<div class="form-actions">
				<a class="btn blue" data-toggle="modal" href="#responsive"> Tambah Data </a>
				<button type="submit" class="btn red" name="submit_purchase">Submit</button>
			</div>
		</form>
	</div>
</div>


<!--------------------------- MODAL ADD ITEM--------------------------------------------->
<div id="responsive" class="modal fade" tabindex="-1" data-width="760">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
		<h4 class="modal-title">Detail Item</h4>
	</div>

	<form role="form" method="post">
		<div class="modal-body">
		<div class="row">
			<div class="col-md-2"></div>
			<div class="col-md-4">
				<div class="form-group">
					<label>Item</label>
					<select id="item_modal" class="form-control input-sm s2-ajax"
							name="item_modal"
							data-s2-url="<?php echo base_url('Purchase_order_controller/search_item'); ?>" required>
					</select>
				</div>
			</div>
			<!-- <div class="col-md-4">
				<div class="form-group">
					<label>Satuan Item</label>
					<select id='satuan_modal' class='form-control input-sm select2-multiple' name='satuan_modal'>
						<option value="<?= isset($default['satuan_id']) ? $default['satuan_id'] : "" ?>"><?= isset($default['satuan_code']) ? $default['satuan_code'] : "" ?></option>
							<?php foreach ($list_satuan as $satuan) { ?>
								<option
									value="<?php echo $satuan['satuan_id']; ?>"><?php echo $satuan['satuan_code']; ?></option>
							<?php } ?>
					</select>
				</div>
			</div> -->
			<div class="col-md-2"></div>
		</div>

			<div class="row">
				<div class="col-md-2"></div>
				<div class="col-md-2">
					<div class="form-group">
						<label>Quantity</label>
						<input type='text' class='form-control' placeholder="Quantity" name='qty_modal' id='qty_modal'
							   onchange="autoAmount()" value="0" required>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<label>Price</label>
						<input type='text' class='form-control' placeholder="Price" name='price_modal' id='price_modal'
							   onchange="autoAmount()" value="0" required>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<label>Amount</label>
						<input type='text' class='form-control' placeholder="Amount" name='amount_modal'
							   id='amount_modal' value="0" readonly>
					</div>
				</div>
				<div class="col-md-2"></div>
			</div>

		</div>
		<div class="modal-footer">
			<button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
			<button type="button" class="btn green" name="add_btn" id="add_btn">Insert</button>
		</div>
	</form>
</div>


<script>
	$(document).ready(function () {
		var count = -1;
		$('tr').each(function (i) {
			count += 1;
		});

		$("#add_btn").click(function () {
			count += 1;
			var item = document.getElementById('item_modal');
			// var satuan = document.getElementById('satuan_modal');
			var qty = document.getElementById('qty_modal');
			var price = document.getElementById('price_modal');
			var amount = document.getElementById('amount_modal');
			var namaItem = $('#item_modal option:selected').text();
			// var satuanItem = $('#satuan_modal option:selected').text();

			$('#container').append(
				'<tr class="records" id="' + count + '">'
				+ '<td><input class="form-control" id="item_' + count + '" name="item_' + count + '" type="text" value="' + namaItem + '" readonly></td>'
				+ '<td><input class="form-control" id="qty_' + count + '" name="qty_' + count + '" type="text" value="' + qty.value + '" readonly></td>'
				+ '<td><input class="form-control" id="price_' + count + '" name="price_' + count + '" type="text" value="' + price.value + '" readonly></td>'
				+ '<td><input class="form-control" id="amount_' + count + '" name="amount_' + count + '" type="text" value="' + amount.value + '" readonly></td>'
				+ '<td><button type="button" class="btn red" onclick="hapus(' + count + ')">X</button></td>'
				+ '<td><input class="form-control" id="item_id_' + count + '" name="item_id_' + count + '" type="hidden" value="' + item.value + '" readonly></td>'
				+ '<td><input id="rows' + count + '" name="rows[]" value="' + count + '" type="hidden"></td></tr>'
			);

			autoSubtotal();
			eraseText();
			// cekGrandTotal();
			$('#responsive').modal('hide');
		});

		$(".remove_item").live('click', function (ev) {
			if (ev.type == 'click') {
				$(this).parents(".records").fadeOut();
				$(this).parents(".records").remove();
			}
		});
	});

	/*function hapus(x){
			alert(x);
			document.getElementById('datatable').deleteRow(x);
	}*/


	function eraseText() {
		document.getElementById("item_modal").value = "";
		document.getElementById("qty_modal").value = "";
		document.getElementById("price_modal").value = "";
		document.getElementById("amount_modal").value = "";
	}


	function hapus(rowid) {
		autominSubtotal("amount_" + rowid);
		var row = document.getElementById(rowid);
		row.parentNode.removeChild(row);

		// cekppn1();
		cekGrandTotal();
		//var amnt = document.getElementById("amount_"+rowid).value;
	}
</script>

<script type="text/javascript">
	var qty_modal = document.getElementById('qty_modal');
	qty_modal.addEventListener('keyup', function (e) {
		qty_modal.value = formatRupiah(this.value, 'Rp. ');
	});

	var price_modal = document.getElementById('price_modal');
	price_modal.addEventListener('keyup', function (e) {
		price_modal.value = formatRupiah(this.value, 'Rp. ');
	});

	var amount_modal = document.getElementById('amount_modal');
	amount_modal.addEventListener('keyup', function (e) {
		amount_modal.value = formatRupiah(this.value, 'Rp. ');
	});

	var subtotal = document.getElementById('subtotal');
	subtotal.addEventListener('keyup', function (e) {
		subtotal.value = formatRupiah(this.value, 'Rp. ');
		console.log(subtotal.value)
	});

	var grandtotal = document.getElementById('grandtotal');
	grandtotal.addEventListener('keyup', function (e) {
		grandtotal.value = formatRupiah(this.value, 'Rp. ');
	});


	/* Fungsi formatRupiah */
	function formatRupiah(angka, prefix) {
		var number_string = angka.replace(/[^,\d]/g, '').toString(),
			split = number_string.split(','),
			sisa = split[0].length % 3,
			rupiah = split[0].substr(0, sisa),
			ribuan = split[0].substr(sisa).match(/\d{3}/gi);

		// tambahkan titik jika yang di input sudah menjadi angka ribuan
		if (ribuan) {
			separator = sisa ? '.' : '';
			rupiah += separator + ribuan.join('.');
		}

		rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
		return prefix == undefined ? rupiah : (rupiah ? rupiah : '');
	}
</script>

<script>
	function autoAmount() {
		var a = document.getElementById('qty_modal').value;
		var b = document.getElementById('price_modal').value;

		var cx = a.split('.').join('');
		var dx = b.split('.').join('');

		var c = cx.split(',').join('.');
		var d = dx.split(',').join('.');

		var z = c * d;
		var zzz = z.toFixed(2);
		var zx = zzz.split('.').join(',');

		document.getElementById('amount_modal').value = zx;
	}
</script>


<script>
	function autoSubtotal() {
		var aa = document.getElementById('subtotal').value;
		var a = aa.split(',').join('.');
		var aa2 = document.getElementById('subtotal2').value;
		var a2 = aa2.split(',').join('.');

		var bb = document.getElementById('amount_modal').value;
		var b = bb.split(',').join('.');

		var zz = (a * 1) + (b * 1);
		var zz2 = (a2 * 1) + (b * 1);
		var zzz = zz.toFixed(2);
		var zzz2 = zz2.toFixed(2);
		var z = zzz.split('.').join(',');
		var z2 = zzz2.split('.').join(',');

		// document.getElementById('subtotal').value = z;
		document.getElementById('subtotal2').value = z2;
	}

	function autominSubtotal(amnt) {
		var aa = document.getElementById('subtotal').value;
		var a = aa.split(',').join('.');
		var aa2 = document.getElementById('subtotal2').value;
		var a2 = aa2.split(',').join('.');

		var bb = document.getElementById(amnt).value;
		var b = bb.split(',').join('.');

		var zz = (a * 1) - (b * 1);
		var zz2 = (a2 * 1) - (b * 1);
		var zzz = zz.toFixed(2);
		var zzz2 = zz2.toFixed(2);
		var z = zzz.split('.').join(',');
		var z2 = zzz2.split('.').join(',');

		// document.getElementById('subtotal').value = z;
		document.getElementById('subtotal2').value = z2;
	}

	function cekppn(ppn) {
		var b = document.getElementById('subtotal').value;
		var bx = document.getElementById('subtotal_backup').value;
		var d = b.split('.').join('');
		var bxx = bx.split('.').join('');

		if (ppn == 0) {//tanpa ppn
			document.getElementById('ppn').value = d * 0;

			var zx = document.getElementById('subtotal_backup').value;
			document.getElementById('subtotal').value = zx;
		} else if (ppn == 1) {//menggunakan ppn
			var ddc = bxx * 0.1;
			document.getElementById('ppn').value = ddc;

			var zx = document.getElementById('subtotal_backup').value;
			document.getElementById('subtotal').value = zx;
		} else if (ppn == 2) {//include ppn
			var dd = Math.round(d / 1.1);
			var zz = Math.round((dd * 10) / 100);
			document.getElementById('subtotal').value = dd;
			document.getElementById('ppn').value = zz;
		}

		cekGrandTotal();
	}

	// function cekppn1() {
	// 	var ppn1 = document.getElementById('optionsRadios4').checked;
	// 	var ppn2 = document.getElementById('optionsRadios5').checked;
	// 	var ppn3 = document.getElementById('optionsRadios6').checked;
	//
	//
	// 	var b = document.getElementById('subtotal').value;
	// 	var d = b.split('.').join('');
	//
	// 	if (ppn1 == true) {
	// 		document.getElementById('ppn').value = d * 0;
	// 	} else if (ppn2 == true) {
	// 		var ddc = d * 0.1;
	// 		document.getElementById('ppn').value = ddc.toFixed(2);
	// 	} else if (ppn3 == true) {
	// 		var dd = d / 1.1;
	// 		document.getElementById('ppn').value = dd.toFixed(2);
	// 	}
	// }


	// function cekGrandTotal() {
	// 	var subtotal_ = document.getElementById('subtotal').value;
	// 	var ppn_ = document.getElementById('ppn').value;
	//
	// 	var subtotal = subtotal_.split('.').join('');
	// 	var ppn = ppn_.split('.').join('');
	//
	// 	var gt = (subtotal * 1) + (ppn * 1);
	//
	// 	var grandtotal = document.getElementById('grandtotal').value = gt;
	// }

</script>
