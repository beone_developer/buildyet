<div class="row">
	<div class="col-sm-3">
		<a href="#" class="btn btn-primary btn-sm active" role="button" aria-pressed="true">Detail Barang</a>
	</div>
	<div style="margin-left: -176px;" class="col-sm-3">
		<a href="<?php echo base_url('Bc41_detail_penggunaan_bb_impor_controller/form/'.$default['ID_HEADER'].'/'.$default['ID'].'');?>" class="btn btn-primary btn-sm" role="button" aria-pressed="true">Penggunaan Bahan Baku Impor</a>
	</div>
	<!-- <div style="margin-left: -75px;" class="col-sm-3">
		<a href="<?php echo base_url('Bc41_detail_penggunaan_bb_lokal_controller/form/'.$default['ID_HEADER'].'/'.$default['ID'].'');?>" class="btn btn-primary btn-sm" role="button" aria-pressed="true">Penggunaan Bahan Baku Lokal</a>
	</div> -->	
</div>
<br>
<a href="<?php echo base_url('Bc41_detail_controller/prev/'.$default['ID_HEADER'].'/'.$default['ID'].'');?>" class="btn btn-primary btn-sm" role="button" aria-pressed="true">prev</a>
<a href="<?php echo base_url('Bc41_detail_controller/next/'.$default['ID_HEADER'].'/'.$default['ID'].'');?>" class="btn btn-primary btn-sm" role="button" aria-pressed="true">next</a>


<br>
<div class="portlet light bordered">
	<div class="portlet-title">
	
	<form role="form" method="post">
			<input type="hidden" class="form-control" id="ID" 
						name="ID" value="<?=isset($default['ID'])? $default['ID'] : ""?>">
		
		<div class="form-body">
			<div class="row">
				<label for="status" class="col-sm-2 col-form-label">Status</label>
				<div class="col-sm-2">
					<input style="border: none; margin-top: -5px;" type="text" readonly class="form-control" id="status" name="status" value="<?=isset($default[''])? $default[''] : ""?>">
				</div>
			</div>
		</div>
		<br>
		<br>
		<div class="form-body">
			<div class="row">
				<div class="col-sm-6">
					<label><b>DATA BARANG BC 4.1</b></label>
				</div>
			</div>
		</div>
		<br>
		<div class="form-body">
			<div class="row">
				<div class="col-sm-1">
					<label>Detail Ke</label>
					<input type="text" class="form-control" name="detail_ke" value="<?=isset($default[''])? $default[''] : ""?>">
					<br>
				</div>

				<div class="col-sm-2">
					<label>Dari</label>
					<input type="text" class="form-control" name="dari" value="<?=isset($default[''])? $default[''] : ""?>">
				</div>

			</div>
			<div class="row">
				<div class="col-sm-4">
					<label>Kode Barang</label>
					<input type="text" class="form-control" name="kode" value="<?=isset($default['KODE_BARANG'])? $default['KODE_BARANG'] : ""?>">
				</div>

			</div>
		</div>
		<br>
		<div class="form-body">
			<div class="row">
				<div class="col-sm-12">
					<label>Uraian Barang</label>
					<input type="text" class="form-control" name="uraian_barang" value="<?=isset($default['URAIAN'])? $default['URAIAN'] : ""?>">
				</div>
			</div>
		</div>
		<br>
		<div class="form-body">
			<div class="row">
				<div class="col-sm-3">
					<label>Merk</label>
					<input type="text" class="form-control" name="merk" value="<?=isset($default['MERK'])? $default['MERK'] : ""?>">
				</div>
				<div class="col-sm-3">
					<label>Tipe</label>
					<input type="text" class="form-control" name="tipe" value="<?=isset($default['TIPE'])? $default['TIPE'] : ""?>">
				</div>
				<div class="col-sm-3">
					<label>Ukuran</label>
					<input type="text" class="form-control" name="ukuran" value="<?=isset($default['UKURAN'])? $default['UKURAN'] : ""?>">
				</div>
				<div class="col-sm-3">
					<label>Spf Lain</label>
					<input type="text" class="form-control" name="spf_lain" value="<?=isset($default['SPESIFIKASI_LAIN'])? $default['SPESIFIKASI_LAIN'] : ""?>">
				</div>
			</div>
		</div>
		<br>
		<div class="form-body">
			<div class="row">
				<div class="col-sm-6">
					<label><b>HARGA</b></label>
				</div>
			</div>
		</div>
		<br>
		<div class="form-body">
			<div class="row">
				<div class="col-sm-4">
					<label>Jumlah Satuan</label>
					<input type="text" class="form-control" name="jumlah_satuan" value="<?=isset($default['JUMLAH_SATUAN'])? $default['JUMLAH_SATUAN'] : ""?>">
				</div>
				<div class="col-sm-4">
					<label>Netto (Kgm)</label>
					<input type="text" class="form-control" name="netto" value="<?=isset($default['NETTO'])? $default['NETTO'] : ""?>">
				</div>
				<div class="col-sm-4">
					<label>Harga Penyerahan Rp</label>
					<input type="text" class="form-control" name="harga_penyerahan" value="<?=isset($default['HARGA_PENYERAHAN'])? $default['HARGA_PENYERAHAN'] : ""?>">
				</div>
			</div>
		</div>
		<br>
		<div class="form-body">
			<div class="row">
				<div class="col-sm-2">
					<label>Jenis Satuan</label>
					<input type="text" class="form-control" name="jenis_satuan" value="<?=isset($default['KODE_SATUAN'])? $default['KODE_SATUAN'] : ""?>">
				</div>
				<div class="col-sm-2">
					<input style="margin-top: 23px; border: none;" readonly type="text" class="form-control" name="uraian_satuan" value="<?=isset($default[''])? $default[''] : ""?>" placeholder="">
				</div>
				<div class="col-sm-4">
					<label>Volume (M3)</label>
					<input type="text" class="form-control" name="volume" value="<?=isset($default['VOLUME'])? $default['VOLUME'] : ""?>">
				</div>
				
			</div>
		</div>
		
		<div class="form-body">
			<div class="row">
				<!-- FORM LEFT SIDE -->
			
				<!-- TUTUP FORM LEFT SIDE -->
				
				<!-- FORM RIGHT SIDE -->
				
				<!-- TUTUP FORM RIGHT SIDE -->
			</div>
		</div>
		<br>
		<div class="form-actions">
            <!-- <a href='<?php echo base_url('Bc41_detail_controller');?>' class='btn default'> Cancel</a> -->
            <button type="submit" class="btn blue" name="submit_detail">Simpan</button>
        </div>

	</form>

	</div>
</div>