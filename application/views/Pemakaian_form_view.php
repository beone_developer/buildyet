<!-- BEGIN PAGE TITLE-->
<!-- END PAGE TITLE-->
<!-- END PAGE HEADER-->
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"></script>
<?php $list_item = $this->db->query("SELECT * FROM public.beone_item");?>

<div class="portlet box blue ">
      <div class="portlet-title">
          <div class="caption">
              <i class="fa fa-gift"></i> Pemakaian Bahan Baku</div>
          <div class="tools">
              <a href="" class="collapse"> </a>
              <a href="#portlet-config" data-toggle="modal" class="config"> </a>
              <a href="" class="reload"> </a>
              <a href="" class="remove"> </a>
          </div>
      </div>
      <div class="portlet-body form">

          <form role="form" method="post">
              <div class="form-body">

                <div class="row">
                  <div name="txtnopemakaian" id="txtnopemakaian"><b>Nomor Pemakaian</b></div>

                  <div class="col-sm-8">
                    <div class="form-group">
                          <label>Tanggal</label>
                          <div class="input-group">
                            <span class="input-group-addon input-circle-left">
                                <i class="fa fa-calendar"></i>
                            </span>
                            <input class="form-control form-control-inline input-medium date-picker input-circle-right" size="16" type="text" name="tanggal" value="<?php echo date('m/d/Y');?>" onChange="tampilnomor(this.value);" readonly required/>
                            <span class="help-block"></span>
                          </div>
                    </div>
                    </div>
                  </div>

                  <div class="row">
                      <div class="col-sm-4">
                        <div class="form-group">
                            <label>Keterangan</label>
                            <input type="text" class="form-control" placeholder="Keterangan" id="keterangan" name="keterangan" required>
                            <input type"text" name="nomor_po" value='<?php echo $nomorpo;?>' hidden>
                            <input type"text" name="pemakaian_bahan_baku_wip" value='bb' hidden>
                          </div>
                      </div>

                      <!-- <div class="col-sm-4">
                          <div class="form-group">
                              <label>PM</label>
                              <div class="input-group">
                                 <select id="select2-single-input-sm" class="form-control input-sm select2-multiple" name="nomor_pm" required>
                                    <option value=""></option>
                                   <?php 	foreach($list_pm as $row){ ?>
                                     <option value="<?php echo $row['pm_header_id'];?>"><?php echo $row['no_pm'];?></option>
                                   <?php } ?>
                                 </select>
                           </div>
                         </div>
                      </div> -->

                      <!--<div class="col-sm-4">
                          <div class="form-group">
                              <label>Jenis Pemakaian Bahan</label>
                              <div class="input-group">
                                 <select id="select2-single-input-sm" class="form-control input-sm select2-multiple" name="jenis_pemakaian" required>
                                    <option value=""></option>
                                    <option value=1>Bahan Baku Upper</option>
                                    <option value=2>Bahan Baku Bottom</option>
                                    <option value=3>Bahan Baku Part & Peralatan Upper</option>
                                    <option value=4>Bahan Baku Part & Peralatan Bottom</option>
                                 </select>
                           </div>
                         </div>
                      </div>-->
                    </div>


                    <hr / style="border-color: #3598DC;">
                    <table  id="datatable" class="table striped hovered cell-hovered">
            						<thead>
            							<tr>
            								<td width="50%">Item</td>
            								<td width="40%">Qty</td>
                            <td width="10%"></td>
            							</tr>
            						 </thead>
                         <tbody id="container">

            						</tbody>
            				</table>

        <div class="row">

        </div>


              </div>
              <div class="form-actions">
                <a class="btn blue" data-toggle="modal" href="#responsive"> Tambah Data </a>
                <button type="submit" class="btn red" name="submit_pemakaian">Submit</button>
              </div>
          </form>
  </div>
</div>

<?php
$list_item2 = $this->db->query("SELECT d.gudang_id, d.item_id, i.nama as nitem, SUM(qty_in) - SUM(qty_out) as jml_qty
                                          FROM public.beone_gudang_detail d INNER JOIN public.beone_item i ON d.item_id = i.item_id
                                          WHERE d.flag = 1 AND d.gudang_id = 1 GROUP BY i.nama, d.item_id, d.gudang_id");
?>

<!--------------------------- MODAL ADD ITEM--------------------------------------------->
<div id="responsive" class="modal fade" tabindex="-1" data-width="760">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Detail Item</h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-8">
            <div class="form-group">
                <label>Item</label>
                <input type="hidden" class="form-control" name="hqty" id="hqty" required>
                <input type="hidden" class="form-control" name="nodoc" id="nodoc" required>
                <select id='item_modal' class='form-control input-sm select2-multiple' name='item_modal' onchange="copydoc();">
                  <option value=0><?php echo " - Pilih Item - ";?></option>
                  <?php  foreach($list_item2->result_array() as $row){
                      if ($row['jml_qty'] <> 0){
                  ?>
                      <option value="<?php echo $row['item_id'];?>"><?php echo $row['nitem']." | Ready Stock = ".number_format($row['jml_qty'],2);?></option>
                  <?php
                      }
                    }
                  ?>
                </select>
            </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                <label>Quantity</label>
                <input type='text' class='form-control' placeholder="Quantity" name='qty_modal' id='qty_modal' required>
                </div>
            </div>
        </div>

    </div>
    <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
        <button type="button" class="btn green" name="add_btn" id="add_btn">Insert</button>
    </div>
</div>


<script>
$(document).ready(function() {
    		var count = 0;

    		$("#add_btn").click(function(){
					count += 1;
          var item = document.getElementById('item_modal');
          var qty = document.getElementById('qty_modal');
          var namaItem = $('#item_modal option:selected').text();

          var qtyx = document.getElementById('hqty').value;//qty bantuan cek
          var qtyxx = qtyx.split(',').join('');

          var qtxxy = document.getElementById('qty_modal').value;
          var cek_qty_asal_ = qtxxy.split('.').join('');
          var cek_qty_asal = cek_qty_asal_.split(',').join('.');

          if (item.value == ""){
            alert("Silahkan isi item..!!!");
            item_hasil.focus();
            return false;
          }else if(qty.value == ""){
            alert("Silahkan isi qty..!!!");
            qty_hasil.focus();
            return false;
          }else if(cek_qty_asal*1 > qtyxx*1){
                alert("Qty melebihi stok..!!!");
                qty.focus();
                return false;
          }

		   		$('#container').append(
							 '<tr class="records" id="'+count+'">'
						 + '<td><input class="form-control" id="item_' + count + '" name="item_'+count+'" type="text" value="'+namaItem+'" readonly></td>'
						 + '<td><input class="form-control" id="qty_' + count + '" name="qty_'+count+'" type="text" value="'+qty.value+'" readonly></td>'
             + '<td><button type="button" class="btn red" onclick="hapus('+count+')">X</button></td>'
             + '<td><input id="rows_' + count + '" name="rows[]" value="'+ count +'" type="hidden"></td>'
             + '<td><input class="form-control" id="item_id_' + count + '" name="item_id_'+count+'" type="hidden" value="'+item.value+'"></td></tr>'
					);


          $('#responsive').modal('hide');
          eraseText();
				});

		});


    function eraseText() {
     $("#item_modal").select2("val", " ");
     document.getElementById("qty_modal").value = "";
    }


    function hapus(rowid)
    {
        var row = document.getElementById(rowid);
        row.parentNode.removeChild(row);
    }
</script>

<script type="text/javascript">
var qty_modal = document.getElementById('qty_modal');
  qty_modal.addEventListener('keyup', function(e){
  qty_modal.value = formatRupiah(this.value, 'Rp. ');
});



/* Fungsi formatRupiah */
function formatRupiah(angka, prefix){
  var number_string = angka.replace(/[^,\d]/g, '').toString(),
  split   		= number_string.split(','),
  sisa     		= split[0].length % 3,
  rupiah     		= split[0].substr(0, sisa),
  ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);

  // tambahkan titik jika yang di input sudah menjadi angka ribuan
  if(ribuan){
    separator = sisa ? '.' : '';
    rupiah += separator + ribuan.join('.');
  }

  rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
  return prefix == undefined ? rupiah : (rupiah ? rupiah : '');
}
</script>

<script>
    function tampilnomor(str)
    {
    if (str=="")
      {
      document.getElementById("txtnopemakaian").innerHTML="";
      return;
      }
    if (window.XMLHttpRequest)
      {// code for IE7+, Firefox, Chrome, Opera, Safari
      xmlhttp=new XMLHttpRequest();
      }
    else
      {// code for IE6, IE5
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    xmlhttp.onreadystatechange=function()
      {
      if (xmlhttp.readyState==4 && xmlhttp.status==200)
      {
      document.getElementById("txtnopemakaian").innerHTML=xmlhttp.responseText;
      }
      }
    xmlhttp.open("GET","Pemakaian_controller/get_nomor_pemakaian?q="+str,true);
    xmlhttp.send();
    }
    </script>

    <script>
    function copydoc(){
      var namaItem = $('#item_modal option:selected').text();
      var nodoc = document.getElementById('nodoc').value = namaItem;

      var qty = nodoc.split("|");
      var hqty = qty[1];
      var ss = hqty.split(" ");
      var xx = ss[4];
      var hhqty = document.getElementById('hqty').value = xx;
    }
    </script>
