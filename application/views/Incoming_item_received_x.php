<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"></script>
<h3>Incoming Item Received</h3>
<div class="portlet light bordered">
  <div class="portlet-title">
      <div class="tools"> </div>
  </div>
  <?php $list_item2 = $this->db->query("SELECT * FROM public.beone_item");?>


  <form role="form" method="post">
      <div class="form-body">

        <div class="row">
          <div class="col-sm-4">
                <div class="form-group">

                    <?php
                    /******************* GENERATE NOMOR BOM ******************************/

                    $tanggal_aju['tanggal_aju'];
                    $tgl = substr($tanggal_aju['tanggal_aju'],0,10);
                    $thn = substr($tgl,2,2);
                    $bln = substr($tgl,5,2);


                    $kode_awal = "RCV"."-".$thn."-".$bln."-";

                    $sql = $this->db->query("SELECT * FROM public.beone_received_import WHERE nomor_received LIKE '$kode_awal%' ORDER BY nomor_received DESC LIMIT 1");
                    $hasil = $sql->row_array();

                    $urutan = substr($hasil['nomor_received'],10,4);
                    $no_lanjutan = $urutan+1;

                    $digit = strlen($no_lanjutan);
                    $jml_nol = 4-$digit;

                    $cetak_nol = "";

                    for ($i = 1; $i <= $jml_nol; $i++) {
                        $cetak_nol = $cetak_nol."0";
                    }

                    $nomor_received = $kode_awal.$cetak_nol.$no_lanjutan;

                    /*************************************************/
                    ?>


                    <div name="txtnorc" id="txtnorc"><h3><b><?php echo $nomor_received;?></b></h3></div>
                    <input type="hidden" id="received_no" name="received_no" value='<?php echo $nomor_received; ?>'>
                    <!--<label>No Received</label>
                      <div class="input-group">
                          <span class="input-group-addon input-circle-left">
                              <i class="fa fa-car"></i>
                          </span>
                          <input type="text" class="form-control input-circle-right" placeholder="No Penerimaan Barang" name="received_no" required>
                      </div>-->
                  </div>
            </div>
        </div>

            <div class="row">
                <div class="col-sm-4">
                <div class="form-group">
                      <label>Received Date</label>
                      <div class="input-group">
                        <span class="input-group-addon input-circle-left">
                            <i class="fa fa-calendar"></i>
                        </span>
                        <input class="form-control form-control-inline input-medium date-picker input-circle-right" size="16" type="text" name="received_date" value="<?php echo date('m/d/Y');?>" readonly required/>
                        <span class="help-block"></span>
                      </div>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-sm-4">
                    <div class="form-group">
                        <label>Kurs</label>
                          <div class="input-group">
                              <span class="input-group-addon input-circle-left">
                                  <i class="fa fa-dollar"></i>
                              </span>
                              <input type="text" class="form-control input-circle-right" placeholder="Kurs saat ini" name="kurs" id="kurs" value="<?php if($kurs['ndpbm'] == NULL){echo 1;}else{echo $kurs['ndpbm']*1;}?>" required readonly>
                          </div>
                      </div>
                </div>
            </div>

            <div class="row">
              <!--<div class="col-sm-4">
                    <div class="form-group">
                    <label>Supplier TPB</label>
                      <input type='text' class='form-control' name='supplier_tpb' id='supplier_tpb' value="<?php //if($supplier['nama_pemasok'] == ""){echo $pengirim['nama_pengirim'];}else{echo $supplier['nama_pemasok'];}?>" required readonly>
                    </div>
                </div>-->
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Nomor PO</label>
                        <div class="input-group">
                           <select id="select2-single-input-sm" class="form-control input-sm select2-multiple" name="nomor_po" required>
                              <option value=""></option>
                             <?php 	foreach($list_nomor_po as $row){ ?>
                               <option value="<?php echo $row['purchase_header_id'];?>"><?php echo $row['purchase_no'];?></option>
                             <?php } ?>
                           </select>
                     </div>
                   </div>
                </div>
                <div class="col-sm-4">
                      <div class="form-group">
                        <label>Supplier</label>
                        <div class="input-group">
                           <select id="select2-single-input-sm" class="form-control input-sm select2-multiple" name="supplier" required>
                              <option value=""></option>
                             <?php 	foreach($list_supplier as $row){ ?>
                               <option value='<?php echo $row['custsup_id'];?>'><?php echo $row['nama'];?></opiton>
                             <?php } ?>
                           </select>
                     </div>
                      </div>
                  </div>
            </div>
            <hr />

            <h4>List Barang</h4>

            <table  id="datatable" class="table striped hovered cell-hovered">
                <thead>
                  <tr>
                    <td width="5%">ID</td>
                    <td width="20%">Item</td>
                    <td width="15%">Qty</td>
                    <td width="15%">Price</td>
                    <td width="15%">Amount</td>
                    <td width="5%"></td>
                  </tr>
                 </thead>
                 <tbody id="container">

                </tbody>
            </table>
            <div class="row">
              <div class="col-sm-4"></div>
              <div class="col-sm-4"></div>
              <div class="col-sm-4">
                  <hr / style="border-color: #3598DC;">
                  <input type="hidden" class="form-control" placeholder="Sub Total" name="subtotal_backup" id="subtotal_backup" value="0" readonly>
                  <input type="text" class="form-control" placeholder="Sub Total" name="subtotal" id="subtotal" value="0" readonly>
              </div>
            </div>


            <input type='hidden' class='form-control' name='nomor_aju' id='nomor_aju' value="<?php echo $nomor_aju['nomor_aju'];?>" required readonly>
            <input type='hidden' class='form-control' name='harga_invoice' id='harga_invoice' value="<?php if ($nomor_aju['harga_invoice'] == NULL){ echo $nomor_aju['harga_penyerahan'];}else{echo $nomor_aju['harga_invoice'];};?>" required readonly>
      </div>
      <div class="form-actions">
          <a class="btn blue" data-toggle="modal" href="#responsive"> Tambah Data </a>
          <button type="submit" class="btn red" name="submit_in">Submit</button>
      </div>
  </form>



  <!--------------------------- MODAL ADD ITEM--------------------------------------------->
  <div id="responsive" class="modal fade" tabindex="-1" data-width="760">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
          <h4 class="modal-title">Detail Item</h4>
      </div>
      <div class="modal-body">
          <div class="row">
              <div class="col-md-12">
              <div class="form-group">
                  <label>Item</label>
                  <select id='item_modal' class='form-control input-sm select2-multiple' name='item_modal'>
                    <option value=0><?php echo " - Pilih Item - ";?></option>
                    <?php  foreach($list_item2->result_array() as $row){ echo '<option value='.$row['item_id'].'>'.$row['nama']." [".$row['item_code']."]".'</option>';} ?>
                  </select>
              </div>
              </div>
          </div>

          <div class="row">
              <div class="col-md-4">
                  <div class="form-group">
                  <label>Quantity</label>
                  <input type='text' class='form-control' placeholder="Quantity" name='qty_modal' id='qty_modal' onchange="autoAmount()" required>
                  </div>
              </div>
              <div class="col-md-4">
                  <div class="form-group">
                  <label>Price</label>
                  <input type='text' class='form-control' placeholder="Price" name='price_modal' id='price_modal' onchange="autoAmount()" required>
                  </div>
              </div>
              <div class="col-md-4">
                  <div class="form-group">
                  <label>Amount</label>
                  <input type='text' class='form-control' placeholder="Amount" name='amount_modal' id='amount_modal' required>
                  </div>
              </div>
          </div>

      </div>
      <div class="modal-footer">
          <button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
          <button type="button" class="btn green" name="add_btn" id="add_btn">Insert</button>
      </div>
  </div>

  <script>
  $(document).ready(function() {
      		var count = 0;

      		$("#add_btn").click(function(){
  					count += 1;

            var item = document.getElementById('item_modal');
            var qty = document.getElementById('qty_modal');
            var price = document.getElementById('price_modal');
            var amount = document.getElementById('amount_modal');
            var namaItem = $('#item_modal option:selected').text();

  		   		$('#container').append(
  							 '<tr class="records" id="'+count+'">'
  						 + '<td><input class="form-control" id="item_id_' + count + '" name="item_id_'+count+'" type="text" value="'+item.value+'" readonly></td>'
  						 + '<td><input class="form-control" id="item_' + count + '" name="item_'+count+'" type="text" value="'+namaItem+'" readonly></td>'
  						 + '<td><input class="form-control" id="qty_' + count + '" name="qty_'+count+'" type="text" value="'+qty.value+'" readonly></td>'
  						 + '<td><input class="form-control" id="price_' + count + '" name="price_'+count+'" type="text" value="'+price.value+'" readonly></td>'
               + '<td><input class="form-control" id="amount_' + count + '" name="amount_'+count+'" type="text" value="'+amount.value+'" readonly></td>'
               + '<td><input id="rows_' + count + '" name="rows[]" value="'+ count +'" type="hidden"></td>'
  						 + '<td><button type="button" class="btn red" onclick="hapus('+count+')">X</button></td></tr>'
  					);

            autoSubtotal();
            eraseText();
            $('#responsive').modal('hide');
  				});

  		});


      function eraseText() {
       document.getElementById("item_modal").value = "";
       document.getElementById("qty_modal").value = "";
       document.getElementById("price_modal").value = "";
       document.getElementById("amount_modal").value = "";
      }


      function hapus(rowid)
      {
          autominSubtotal("amount_"+rowid);
          var row = document.getElementById(rowid);
          row.parentNode.removeChild(row);
      }
  </script>

  <script type="text/javascript">
  var qty_modal = document.getElementById('qty_modal');
    qty_modal.addEventListener('keyup', function(e){
    qty_modal.value = formatRupiah(this.value, 'Rp. ');
  });

  var price_modal = document.getElementById('price_modal');
    price_modal.addEventListener('keyup', function(e){
    price_modal.value = formatRupiah(this.value, 'Rp. ');
  });



  /* Fungsi formatRupiah */
  function formatRupiah(angka, prefix){
    var number_string = angka.replace(/[^,\d]/g, '').toString(),
    split   		= number_string.split(','),
    sisa     		= split[0].length % 3,
    rupiah     		= split[0].substr(0, sisa),
    ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);

    // tambahkan titik jika yang di input sudah menjadi angka ribuan
    if(ribuan){
      separator = sisa ? '.' : '';
      rupiah += separator + ribuan.join('.');
    }

    rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
    return prefix == undefined ? rupiah : (rupiah ? rupiah : '');
  }
  </script>

  <script>
      function autoAmount(){
        var a = document.getElementById('qty_modal').value;
        var b = document.getElementById('price_modal').value;

        var cx = a.split('.').join('');
        var dx = b.split('.').join('');

        var c = cx.split(',').join('.');
        var d = dx.split(',').join('.');

        var z = c * d;
        var zzz = z.toFixed(2);
        var zx = zzz.split('.').join(',');

        document.getElementById('amount_modal').value = zx;
      }
  </script>


  <script>
      function autoSubtotal(){
        var aa = document.getElementById('subtotal').value;
        var a = aa.split(',').join('.');

        var bb = document.getElementById('amount_modal').value;
        var b = bb.split(',').join('.');

        var zz = (a*1) + (b*1);
        var zzz = zz.toFixed(2);
        var z = zzz.split('.').join(',');

        document.getElementById('subtotal').value = z;
      }

      function autominSubtotal(amnt){
        var aa = document.getElementById('subtotal').value;
        var a = aa.split(',').join('.');

        var bb = document.getElementById(amnt).value;
        var b = bb.split(',').join('.');

        var zz = (a*1) - (b*1);
        var zzz = zz.toFixed(2);
        var z = zzz.split('.').join(',');

        document.getElementById('subtotal').value = z;
      }

  </script>
