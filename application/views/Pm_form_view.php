<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"></script>
<?php $list_item = $this->db->query("SELECT * FROM public.beone_item"); ?>

<h3>PM Form <b><?= $tipe ?></b></h3>
<div class="portlet light bordered">
	<div class="portlet-title">


		<form role="form" method="post">
			<div class="form-body">
				<div class="row">
					<div name="txtnopm" id="txtnopm"><b>Nomor PM</b></div>
					<!--<input type="text" class="form-control" placeholder="Nomor PM" id="nomor_pm" value="<? //isset($default['nomor_pm'])? $default['nomor_pm'] : ""?>" name="nomor_pm" required>-->

					<div class="col-sm-4">
						<div class="form-group">
							<label>Tanggal</label>
							<div class="input-group">
                      <span class="input-group-addon input-circle-left">
                          <i class="fa fa-calendar"></i>
                      </span>
								<input
									class="form-control form-control-inline input-medium date-picker input-circle-right"
									size="16" type="text" name="tanggal" value="<?php echo date('m/d/Y'); ?>"
									onChange="tampilnomor(this.value);" readonly required/>
								<span class="help-block"></span>
							</div>
						</div>
					</div>
					<div class="col-sm-4"></div>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-4">
					<div class="form-group">
						<label>Customer</label>
						<div class="input-group">
							<select id="select2-single-input-sm" class="form-control input-sm select2-multiple"
									name="customer" required>
								<option value=""></option>
								<?php
								foreach ($list_customer

								as $row){
								?>
								<option value='<?php echo $row['custsup_id']; ?>'><?php echo $row['nama']; ?></opiton>
									<?php
									}
									?>
							</select>
						</div>
					</div>
				</div>

				<div class="col-md-4">
					<div class="form-group">
						<label>Keterangan Artikel</label>
						<input type='text' class='form-control' placeholder="Keterangan Artikel" name='keterangan'
							   id='keterangan' required>
					</div>
				</div>
				<div class="col-md-4 invisible">
					<div class="form-group">
						<label>Quantity</label>
						<input type='text' class='form-control' placeholder="Quantity" name='qty' id='qty' required>
					</div>
				</div>

			</div>


			<hr
			/ style="border-color: #3598DC;">
			<table id="datatable" class="table striped hovered cell-hovered">
				<thead>
				<tr>
					<td width="5%">ID</td>
					<td width="20%">Item</td>
					<td width="15%">Qty</td>
					<td width="15%">Satuan</td>
					<td width="5%"></td>
				</tr>
				</thead>
				<tbody id="container">

				</tbody>
			</table>


			<br/>
			<div class="form-actions">
				<a class="btn blue" data-toggle="modal" href="#responsive"> Tambah Data </a>
				<?php if (helper_security("satuan_add") == 1) { ?>
					<button type="submit" class="btn blue" name="submit_pm">Submit</button>
				<?php } ?>
			</div>
		</form>

		<hr/>
		<br/>

		<div class="tools"></div>
	</div>
</div>

<!--------------------------- MODAL ADD ITEM--------------------------------------------->
<div id="responsive" class="modal fade" tabindex="-1" data-width="760">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
		<h4 class="modal-title">Detail Item</h4>
	</div>
	<div class="modal-body">
		<div class="row">
			<div class="col-md-2"></div>
			<div class="col-md-8">
				<h3><b id='name-modal'></b></h3>
				<input type='hidden' class='form-control' placeholder="No. PM" name='no_modal' id='no_modal'>
			</div>
			<div class="col-md-2"></div>
		</div>
		<div class="row">
			<div class="col-md-2"></div>
			<div class="col-md-8">
				<div class="form-group">
					<label>Item</label>
					<select id='item_modal' class='form-control input-sm select2-multiple' name='item_modal'>
						<option value=0><?php echo " - Pilih Item - "; ?></option>
						<?php foreach ($list_item->result_array() as $item) {
							echo '<option value=' . $item['item_id'] . '>' . $item['nama'] . '</option>';
						} ?>
					</select>
				</div>
			</div>
			<div class="col-md-2"></div>
		</div>

		<div class="row">
			<div class="col-md-2"></div>
			<div class="col-md-2">
				<div class="form-group">
					<label>Quantity</label>
					<input type='text' class='form-control' placeholder="Quantity" name='qty_modal' id='qty_modal'
						   required>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label>Satuan</label>
					<select id='satuan_modal' class='form-control input-sm select2-multiple' name='satuan_modal'>
						<option
							value="<?= isset($default['satuan_id']) ? $default['satuan_id'] : "" ?>"><?= isset($default['satuan_code']) ? $default['satuan_code'] : "" ?></option>
						<?php foreach ($list_satuan as $satuan) { ?>
							<option
								value="<?php echo $satuan['satuan_id']; ?>"><?php echo $satuan['satuan_code']; ?></option>
						<?php } ?>
						<
					</select>
				</div>
			</div>
			<div class="col-md-2"></div>
		</div>
	</div>
	<div class="modal-footer">
		<button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
		<button type="button" class="btn green" name="add_btn" id="add_btn">Insert</button>
	</div>
</div>

<script>
	$(document).ready(function () {
		var count = 0;

		$("#add_btn").click(function () {
			count += 1;

			var item = document.getElementById('item_modal');
			var qty = document.getElementById('qty_modal');
			var satuan = document.getElementById('satuan_modal');
			var namaItem = $('#item_modal option:selected').text();
			$('#container').append(
				'<tr class="records" id="' + count + '">'
				+ '<td><input class="form-control" id="item_id_' + count + '" name="item_id_' + count + '" type="text" value="' + item.value + '" readonly></td>'
				+ '<td><input class="form-control" id="item_' + count + '" name="item_' + count + '" type="text" value="' + namaItem + '" readonly></td>'
				+ '<td><input class="form-control" id="qty_' + count + '" name="qty_' + count + '" type="text" value="' + qty.value + '" readonly></td>'
				+ '<td><input class="form-control" id="satuan_' + count + '" name="satuan_' + count + '" type="text" value="' + satuan.value + '" readonly></td>'
				+ '<td><input id="rows_' + count + '" name="rows[]" value="' + count + '" type="hidden"></td>'
				+ '<td><button type="button" class="btn red" onclick="hapus(' + count + ')">X</button></td></tr>'
			);
			$('#responsive').modal('hide');
		});

		$(".remove_item").live('click', function (ev) {
			if (ev.type == 'click') {
				$(this).parents(".records").fadeOut();
				$(this).parents(".records").remove();
			}
		});
	});

	function hapus(rowid) {
		var row = document.getElementById(rowid);
		row.parentNode.removeChild(row);
	}
</script>

<script>
	function dialogHapus(urlHapus) {
		if (confirm("Apakah anda yakin ingin menghapus ini ?")) {
			document.location = urlHapus;
		}
	}
</script>

<script type="text/javascript">
	var qty = document.getElementById('qty');
	qty.addEventListener('keyup', function (e) {
		qty.value = formatRupiah(this.value, 'Rp. ');
	});

	/* Fungsi formatRupiah */
	function formatRupiah(angka, prefix) {
		var number_string = angka.replace(/[^,\d]/g, '').toString(),
			split = number_string.split(','),
			sisa = split[0].length % 3,
			rupiah = split[0].substr(0, sisa),
			ribuan = split[0].substr(sisa).match(/\d{3}/gi);

		// tambahkan titik jika yang di input sudah menjadi angka ribuan
		if (ribuan) {
			separator = sisa ? '.' : '';
			rupiah += separator + ribuan.join('.');
		}

		rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
		return prefix == undefined ? rupiah : (rupiah ? rupiah : '');
	}
</script>

<script>
	function tampilnomor(str) {
		if (str == "") {
			document.getElementById("txtnopm").innerHTML = "";
			return;
		}
		if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp = new XMLHttpRequest();
		} else {// code for IE6, IE5
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange = function () {
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
				document.getElementById("txtnopm").innerHTML = xmlhttp.responseText;
			}
		}
		xmlhttp.open("GET", "pm_controller/get_nomor_pm?q=" + str, true);
		xmlhttp.send();
	}
</script>
