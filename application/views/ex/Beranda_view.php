<!doctype html>
<html class="no-js" lang="">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>BeOne Project | Sistem Antrian</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon
		============================================ -->
    <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
    <!-- Google Fonts
		============================================ -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,700,900" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Ranga&display=swap" rel="stylesheet">
    <!-- Bootstrap CSS
		============================================ -->	
    <link rel="stylesheet" href="<?php echo base_url();?>assets/green/css/bootstrap.min.css">
    <!-- Bootstrap CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/green/css/font-awesome.min.css">
    <!-- owl.carousel CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/green/css/owl.carousel.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/green/css/owl.theme.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/green/css/owl.transitions.css">
    <!-- meanmenu CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/green/css/meanmenu/meanmenu.min.css">
    <!-- animate CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/green/css/animate.css">
    <!-- normalize CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/green/css/normalize.css">
    <!-- mCustomScrollbar CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/green/css/scrollbar/jquery.mCustomScrollbar.min.css">
    <!-- jvectormap CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/green/css/jvectormap/jquery-jvectormap-2.0.3.css">
    <!-- notika icon CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/green/css/notika-custom-icon.css">
    <!-- wave CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/green/css/wave/waves.min.css">
	<link rel="stylesheet" href="<?php echo base_url();?>assets/green/css/wave/button.css">

    <!-- main CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/green/css/main.css">
    <!-- style CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/green/style.css">
    <!-- responsive CSS
		============================================ -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/green/css/responsive.css">
    <!-- modernizr JS
		============================================ -->
    <script src="<?php echo base_url();?>assets/green/js/vendor/modernizr-2.8.3.min.js"></script>
</head>

<body style="background-image: url(<?php echo base_url();?>assets/green/img/Orthopedi_blur.jpg); background-size: cover;">
<!--<body style="background: -moz-linear-gradient(0deg, #a0db8e 0%, #b4eeb4 100%); background: -webkit-linear-gradient(0deg, #a0db8e 0%, #b4eeb4 100%); background: -ms-linear-gradient(0deg, #a0db8e 0%, #b4eeb4 100%);">-->
    
   
   <!-------------------------------------------------------------- ISI ----------------------------------------------------------------------------->
   
    <div class="notika-status-area">
        <div class="container" >
		<center>
			<div class="row" style="width:40%; height:40%;">
				<img src="<?php echo base_url();?>assets/green/img/logo/Logo BeOne.png">
			</div>
		</center>
		</div>
	</div>
   
   <!-- BUTTON AREA -->
    <div class="notika-status-area" style="margin: 10px; margin-right: 50px; margin-left: 50px; padding: 20px;">
        <div class="container" >
			<div class="row">
			<center>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<div class="material-design-btn">
						<a href="<?php echo base_url();?>Ambil_controller" class="btn notika-btn-amber" style="padding:30px; width:100%;"><h2 style="font-family: Times new Roman;"><i class="fa fa-print" style="font-size: 70px;"></i><br/> ANTRIAN </h2></a>
					</div>
				</div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<div class="material-design-btn">
						<a href="<?php echo base_url();?>Display_controller" class="btn notika-btn-green" style="padding:30px; width:100%;"><h2 style="font-family: Times new Roman;"><i class="fa fa-users" style="font-size: 70px;"></i><br/> DISPLAY LED </h2></a>
					</div>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<div class="material-design-btn">
						<a href="<?php echo base_url();?>Layanan_controller" class="btn notika-btn-blue" style="padding:30px; width:100%;"><h2 style="font-family: Times new Roman;"><i class="fa fa-heartbeat" style="font-size: 70px;"></i><br/> LAYANAN </h2></a>
					</div>
				</div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<div class="material-design-btn">
						<a href="<?php echo base_url();?>Dashboard_controller" class="btn notika-btn-red" style="padding:30px; width:100%;"><h2 style="font-family: Times new Roman;"><i class="fa fa-pie-chart" style="font-size: 70px;"></i><br/> DASHBOARD</h2></a>
					</div>
				</div>
			</center>
            </div>
			
        
		</div>
    </div>
    <!-- End BUTTON AREA -->
   
   <!-------------------------------------------------------------- END ISI --------------------------------------------------------------------------->  
   
   
    <!-- jquery
		============================================ -->
    <script src="<?php echo base_url();?>assets/green/js/vendor/jquery-1.12.4.min.js"></script>
    <!-- bootstrap JS
		============================================ -->
    <script src="<?php echo base_url();?>assets/green/js/bootstrap.min.js"></script>
    <!-- wow JS
		============================================ -->
    <script src="<?php echo base_url();?>assets/green/js/wow.min.js"></script>
    <!-- price-slider JS
		============================================ -->
    <script src="<?php echo base_url();?>assets/green/js/jquery-price-slider.js"></script>
    <!-- owl.carousel JS
		============================================ -->
    <script src="<?php echo base_url();?>assets/green/js/owl.carousel.min.js"></script>
    <!-- scrollUp JS
		============================================ -->
    <script src="<?php echo base_url();?>assets/green/js/jquery.scrollUp.min.js"></script>
    <!-- meanmenu JS
		============================================ -->
    <script src="<?php echo base_url();?>assets/green/js/meanmenu/jquery.meanmenu.js"></script>
    <!-- counterup JS
		============================================ -->
    <script src="<?php echo base_url();?>assets/green/js/counterup/jquery.counterup.min.js"></script>
    <script src="<?php echo base_url();?>assets/green/js/counterup/waypoints.min.js"></script>
    <script src="<?php echo base_url();?>assets/green/js/counterup/counterup-active.js"></script>
    <!-- mCustomScrollbar JS
		============================================ -->
    <script src="<?php echo base_url();?>assets/green/js/scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
    <!-- jvectormap JS
		============================================ -->
    <script src="<?php echo base_url();?>assets/green/js/jvectormap/jquery-jvectormap-2.0.2.min.js"></script>
    <script src="<?php echo base_url();?>assets/green/js/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
    <script src="<?php echo base_url();?>assets/green/js/jvectormap/jvectormap-active.js"></script>
    <!-- sparkline JS
		============================================ -->
    <script src="<?php echo base_url();?>assets/green/js/sparkline/jquery.sparkline.min.js"></script>
    <script src="<?php echo base_url();?>assets/green/js/sparkline/sparkline-active.js"></script>
    <!-- sparkline JS
		============================================ -->
    <script src="<?php echo base_url();?>assets/green/js/flot/jquery.flot.js"></script>
    <script src="<?php echo base_url();?>assets/green/js/flot/jquery.flot.resize.js"></script>
    <script src="<?php echo base_url();?>assets/green/js/flot/curvedLines.js"></script>
    <script src="<?php echo base_url();?>assets/green/js/flot/flot-active.js"></script>
    <!-- knob JS
		============================================ -->
    <script src="<?php echo base_url();?>assets/green/js/knob/jquery.knob.js"></script>
    <script src="<?php echo base_url();?>assets/green/js/knob/jquery.appear.js"></script>
    <script src="<?php echo base_url();?>assets/green/js/knob/knob-active.js"></script>
    <!--  wave JS
		============================================ -->
    <script src="<?php echo base_url();?>assets/green/js/wave/waves.min.js"></script>
    <script src="<?php echo base_url();?>assets/green/js/wave/wave-active.js"></script>
    <!--  todo JS
		============================================ -->
    <script src="<?php echo base_url();?>assets/green/js/todo/jquery.todo.js"></script>
    <!-- plugins JS
		============================================ -->
    <script src="<?php echo base_url();?>assets/green/js/plugins.js"></script>
	<!--  Chat JS
		============================================ -->
    <script src="<?php echo base_url();?>assets/green/js/chat/moment.min.js"></script>
    <script src="<?php echo base_url();?>assets/green/js/chat/jquery.chat.js"></script>
    <!-- main JS
		============================================ -->
    <script src="<?php echo base_url();?>assets/green/js/main.js"></script>
	
</body>

</html>