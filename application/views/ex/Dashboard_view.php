<body>
    <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
    <!-- Start Header Top Area -->
    <div class="header-top-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div class="logo-area">
                        <a href="#"><img src="<?php echo base_url();?>assets/green/img/logo/beone.png" alt="" /></a>
                    </div>
                </div>
                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                    <div class="header-top-menu">
                        <ul class="nav navbar-nav notika-top-nav"></ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Header Top Area -->
    
    <!-- Main Menu area start-->
    <div class="main-menu-area mg-tb-40">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <ul class="nav nav-tabs notika-menu-wrap menu-it-icon-pro">
                        <li class="active"><a data-toggle="tab" href="#Home"><i class="notika-icon notika-house"></i> Master Layanan</a>
                        </li>
                        <li><a data-toggle="tab" href="#mailbox"><i class="notika-icon notika-user"></i> Master User</a>
                        </li>
                        <li><a data-toggle="tab" href="#Interface"><i class="notika-icon notika-edit"></i> Dashboard</a>
                        </li>
                        <li><a data-toggle="tab" href="#Charts"><i class="notika-icon notika-bar-chart"></i> Laporan</a>
                        </li>
                        <li><a data-toggle="tab" href="#Tables"><i class="notika-icon notika-windows"></i> Setting</a>
                        </li>
                    </ul>
                    
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Main Menu area End-->
	
        <!-- Data Table area Start-->
    <div class="data-table-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                    <div class="data-table-list">
                        <div class="basic-tb-hd">
                            <h2>Master Layanan</h2>
							<div class="material-design-btn">
								<a href="<?php echo base_url();?>Layanan_controller/add" class="btn notika-btn-green btn-reco-mg btn-button-mg">Tambah Data</a>
							</div>
                        </div>
                        <div class="table-responsive">
                            <table id="data-table-basic" class="table table-striped">
                                <thead>
                                    <tr>
                                        <th><center>Nama Poli</center></th>
                                        <th><center>Kode Poli</center></th>
                                        <th><center>Action</center></th>
                                    </tr>
                                </thead>
								
                                <tbody>
									<?php
										foreach($list_layanan as $row){
									?>
                                    <tr>
                                        <td><?php echo $row['nama'];?></td>
                                        <td><?php echo $row['kode'];?></td>
                                        <td>
											<div class="material-design-btn">
												<center>
												<a href="<?php echo base_url();?>Layanan_controller/edit/<?php echo $row['layanan_id'];?>" class="btn notika-btn-green btn-reco-mg btn-button-mg">Ubah</a> |
												<a href="javascript:dialogHapus('<?php echo base_url();?>Layanan_controller/hapus/<?php echo $row['layanan_id'];?>')" class="btn notika-btn-red btn-reco-mg btn-button-mg">Hapus</a>
												</center>
											</div>
										</td>
                                    </tr>
                                    <?php
										}
									?>
                                    
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th><center>Nama Poli</center></th>
                                        <th><center>Kode Poli</center></th>
                                        <th><center>Action</center></th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Data Table area End-->
    <!-- Start Footer area-->
    <div class="footer-copyright-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="footer-copy-right">
                        <p>Copyright © 2018 
. All rights reserved. Template by <a href="https://colorlib.com">Colorlib</a>.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Footer area-->
	
	<script>
	function dialogHapus(urlHapus) {
	  if (confirm("Apakah anda yakin ingin menghapus ini ?")) {
		document.location = urlHapus;
	  }
	}
</script>
   