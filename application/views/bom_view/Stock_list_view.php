<h3>List Transfer Item</h3>
<div class="portlet light bordered">
  <div class="portlet-title">
    <div class="caption font-dark">
        <a href='<?php echo base_url('Transfer_controller/index_transfer_delete');?>' class='btn yellow'><i class="fa fa-eye"></i> Show Cancel</a>
    </div>
      <div class="tools"> </div>
  </div>

<table class="table table-striped table-bordered table-hover" id="sample_1">
        <thead>
          <tr>
              <th width='20%'><center>No Produksi</center></th>
              <th width='20%'><center>Tanggal</center></th>
              <th width='40%'><center>Keterangan</center></th>
              <th width='20%'><center>Action</center></th>
          </tr>
        </thead>
        <tbody>
          <?php 	foreach($list_stock_transfer as $row){ ?>
            <tr>
                <td><center><?php echo $row['transfer_no'];?></center></td>
                <td><center><?php echo $row['transfer_date'];?></center></td>
                <td><?php echo $row['keterangan'];?></td>
                <td><center>
                      <?php if(helper_security("produksi_edit") == 1){?>
                      <a href='<?php echo base_url('Transfer_controller/edit/'.$row['transfer_stock_id'].'');?>' class='btn blue'><i class="fa fa-pencil"></i> </a>
                      <?php }?>
                      <?php if(helper_security("produksi_delete") == 1){?>
                      <a href="javascript:dialogHapus('<?php echo base_url('Transfer_controller/delete/'.$row['transfer_stock_id'].'/'.$row['transfer_no'].'');?>')" class='btn red'><i class="fa fa-trash-o"></i> </a>
                      <?php }?>
                      <a href='<?php echo base_url('Transfer_controller/transfer_print/'.$row['transfer_stock_id'].'');?>' class='btn yellow'><i class="fa fa-print"></i> </a>
                </center></td>
            </tr>
            <?php
              }
            ?>
        </tbody>
    </table>
</div>

<script>
	function dialogHapus(urlHapus) {
	  if (confirm("Apakah anda yakin ingin menghapus ini ?")) {
		document.location = urlHapus;
	  }
	}
</script>
