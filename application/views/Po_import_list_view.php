<h3>List Purchase Order Import</h3>
<div class="portlet light bordered">
  <div class="portlet-title">
      <div class="tools"> </div>
  </div>

<table class="table table-striped table-bordered table-hover" id="sample_1">
        <thead>
          <tr>
              <th width='20%'><center>No PO</center></th>
              <th width='20%'><center>Tanggal</center></th>
              <th width='40%'><center>Keterangan</center></th>
              <th width='20%'><center>Action</center></th>
          </tr>
        </thead>
        <tbody>
          <?php 	foreach($list_po_import as $row){ ?>
            <tr>
                <td><center><?php echo $row['purchase_no'];?></center></td>
                <td><center><?php echo $row['trans_date'];?></center></td>
                <td><?php echo $row['keterangan'];?></td>
                <td><center>
                    <?php if(helper_security("po_edit") == 1){?>
                    <a href='<?php echo base_url('po_import_controller/edit/'.$row['purchase_header_id'].'');?>' class='btn blue'><i class="fa fa-pencil"></i> </a>
                    <?php }?>
                    <?php if(helper_security("po_delete") == 1){?>
                    <a href="javascript:dialogHapus('<?php echo base_url('po_import_controller/delete/'.$row['purchase_header_id'].'');?>')" class='btn red'><i class="fa fa-trash-o"></i> </a>
                    <?php }?>
                    <a href='<?php echo base_url('po_import_controller/po_import_print/'.$row['purchase_header_id'].'');?>' class='btn yellow'><i class="fa fa-print"></i> </a></center></td>
            </tr>
            <?php
              }
            ?>
        </tbody>
    </table>
</div>

<script>
	function dialogHapus(urlHapus) {
	  if (confirm("Apakah anda yakin ingin menghapus ini ?")) {
		document.location = urlHapus;
	  }
	}
</script>
