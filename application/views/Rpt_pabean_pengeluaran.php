<?php
$awal = helper_tanggalinsert($_GET['tglawal']);
$akhir = helper_tanggalinsert($_GET['tglakhir']);
?>
<h3>Report Pabean Pengeluaran</h3>
<div class="portlet light bordered">
  <div class="portlet-title">
      <div class="tools"> </div>
  </div>
  <a href="<?php echo base_url('Bc_controller/Print_pabean_pengeluaran?tglawal='.$awal.'&tglakhir='.$akhir.''); ?>" class="btn btn-lg blue hidden-print margin-bottom-5"> Print
      <i class="fa fa-print"></i>
  </a>

<table class="table table-striped table-bordered table-hover" id="sample_2">
        <thead>
          <tr>
              <th colspan="1"></th>
              <th colspan="1"></th>
              <th colspan="2"><center>Doc</center></th>
              <th colspan="2"><center>Surat Jalan</center></th>
              <th colspan="1"></th>
              <th colspan="2"><center>Item</center></th>
              <th colspan="4"></th>
          </tr>
          <tr>
              <th><center><small>No</small></center></th>
							<th><center><small>Jenis BC</small></center></th>
              <th><center><small>No</small></center></th>
              <th><center><small>Date</small></center></th>
              <th><center><small>No</small></center></th>
              <th><center><small>Date</small></center></th>
              <th><center><small>Supplier</small></center></th>
              <th><center><small>Kode</small></center></th>
              <th><center><small>Nama</small></center></th>
              <th><center><small>Sat</small></center></th>
              <th><center><small>Qty</small></center></th>
              <th><center><small>Valas</small></center></th>
							<th><center><small>Price</small></center></th>
          </tr>
        </thead>
        <tbody>
          <?php
          $no = 0;
          $sql = $this->db->query("SELECT h.jenis_bc, h.car_no, h.bc_no, h.bc_date, h.delivery_date, h.delivery_no, h.receiver_id, h.surat_jalan_no, h.surat_jalan_date, d.qty, d.price, i.nama, i.item_code, c.nama as customer
                                  FROM public.beone_export_header h INNER JOIN public.beone_export_detail d ON h.export_header_id = d.export_header_id INNER JOIN public.beone_item i ON d.item_id = i.item_id INNER JOIN public.beone_custsup c ON h.receiver_id = c.custsup_id
                                  WHERE h.bc_date BETWEEN '$awal' AND '$akhir' ORDER BY h.bc_no ASC, h.delivery_date ASC");
								foreach($sql->result_array() as $row){
                $no = $no + 1;

								$view_bc = "";
								if ($row['jenis_bc'] == 1){
										$view_bc = "BC 3.0";
								}elseif($row['jenis_bc'] == 2){
										$view_bc = 	"BC 2.7";
								}elseif($row['jenis_bc'] == 3){
										$view_bc = 	"BC 4.1";
								}elseif($row['jenis_bc'] == 4){
										$view_bc = 	"BC 2.5";
								}
					?>
            <tr>
                <td><center><small><?php echo $no;?></small></center></td>
                <td><small><?php echo $view_bc;?></small></td>
                <td><small><?php echo $row['bc_no'];?></small></td>
								<td><small><?php echo $row['bc_date'];?></small></td>
                <td><small><?php echo $row['delivery_no'];?></small></td>
								<td><small><?php echo $row['delivery_date'];?></small></td>
                <td><small><?php echo $row['customer'];?></small></td>
                <td><small><?php echo $row['item_code'];?></small></td>
                <td><small><?php echo $row['nama'];?></small></td>
                <td><small><?php echo "KGM";?></small></td>
                <td><small><?php echo number_format($row['qty'],2);?></small></td>
                <td><small><?php echo "USD";?></small></td>
								<td><small><?php echo number_format($row['price']*$row['qty'],2);?></small></td>
            </tr>
            <?php
              }
            ?>
        </tbody>
    </table>
</div>
