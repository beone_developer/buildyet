<?php
// RUMUS LABA RUGI KOTOR = PENJUALAN - HPP
// RUMUS LABA RUGI USAHA = LABA RUGI KOTOR - BIAYA ADMIN DAN UMUM
// RUMUS LABA RUGI BERSIH = LABA RUGI USAHA + PENDAPATAN LAIN

/************************************* PENJUALAN ********************************************************/
$total_penjualan = 0; //TOTAL PENJUALAN = SALDO AWAL AKUN PENJALAN COA + MUTASI AKUN PENJUALAN DI GENERAL LEDGER

foreach($saldo_awal_coa_penjualan as $row){
    //nilai Saldo Awal dari COA Akun PENJUALAN
    $sql_posisi_dk = $this->db->query("SELECT * FROM public.beone_coa WHERE coa_id =".intval($row['coa_id']));
    $hasil_posisi_dk = $sql_posisi_dk->row_array();

    //nilai Mutasi Akun Penjualan di General Ledger
    $sql_total_mutasi_dk = $this->db->query("SELECT SUM(debet) as totmdebet, SUM(kredit) as totmkredit FROM public.beone_gl WHERE gl_date BETWEEN '$tgl_awal_formated' AND '$tgl_akhir_formated' AND coa_id =".intval($row['coa_id']));
    $hasil_total_mutasi_dk = $sql_total_mutasi_dk->row_array();

    if ($hasil_posisi_dk['tipe_akun'] == 1 OR $hasil_posisi_dk['tipe_akun'] == 5 OR $hasil_posisi_dk['tipe_akun'] == 6 OR $hasil_posisi_dk['tipe_akun'] == 7 OR $hasil_posisi_dk['tipe_akun'] == 9){
        if ($hasil_posisi_dk['nomor'] == '118.01.01' OR $hasil_posisi_dk['nomor'] == '118.01.02'){
          $saldo = $hasil_total_mutasi_dk['totmkredit'] - $hasil_total_mutasi_dk['totmdebet'];
        }else{
          $saldo = $hasil_total_mutasi_dk['totmdebet'] - $hasil_total_mutasi_dk['totmkredit'];
        }
    }elseif($hasil_posisi_dk['tipe_akun'] == 2 OR $hasil_posisi_dk['tipe_akun'] == 3 OR $hasil_posisi_dk['tipe_akun'] == 4 OR $hasil_posisi_dk['tipe_akun'] == 8){
        $saldo = $hasil_total_mutasi_dk['totmkredit'] - $hasil_total_mutasi_dk['totmdebet'];
    }

    //HASIL TOTAL PENJUALAN
    $total_penjualan = $total_penjualan + $saldo;
  }
/************************************* END PENJUALAN ********************************************************/

/***************************************************** TOTAL DEBET KREDIT AKUN HPP *********************************************************/
      //Tidak menggunakan foreach karena hanya menyorot 1 Akun (Akun HPP hanya ada 1)
      $total_hpp = 0;

      //posisi Debet Kredit Akun HPP
      $sql_posisi_dk = $this->db->query("SELECT * FROM public.beone_coa WHERE nomor = '511.01.01'");
      $hasil_posisi_dk = $sql_posisi_dk->row_array();

      //nilai Mutasi Akun HPP di General Ledger
      $sql_total_mutasi_dk = $this->db->query("SELECT SUM(debet) as totmdebet, SUM(kredit) as totmkredit FROM public.beone_gl WHERE gl_date BETWEEN '$tgl_awal_formated' AND '$tgl_akhir_formated' AND coa_id =".intval($hasil_posisi_dk['coa_id']));
      $hasil_total_mutasi_dk = $sql_total_mutasi_dk->row_array();

      if ($hasil_posisi_dk['tipe_akun'] == 1 OR $hasil_posisi_dk['tipe_akun'] == 5 OR $hasil_posisi_dk['tipe_akun'] == 6 OR $hasil_posisi_dk['tipe_akun'] == 7 OR $hasil_posisi_dk['tipe_akun'] == 9){
          if ($hasil_posisi_dk['nomor'] == '118.01.01' OR $hasil_posisi_dk['nomor'] == '118.01.02'){
            $saldo_HPP = $hasil_total_mutasi_dk['totmkredit']-$hasil_total_mutasi_dk['totmdebet'];
          }else{
            $saldo_HPP = $hasil_total_mutasi_dk['totmdebet']-$hasil_total_mutasi_dk['totmkredit'];
          }
      }elseif($hasil_posisi_dk['tipe_akun'] == 2 OR $hasil_posisi_dk['tipe_akun'] == 3 OR $hasil_posisi_dk['tipe_akun'] == 4 OR $hasil_posisi_dk['tipe_akun'] == 8){
          $saldo_HPP = $hasil_total_mutasi_dk['totmkredit']-$hasil_total_mutasi_dk['totmdebet'];
      }
/***************************************************** END TOTAL DEBET KREDIT AKUN HPP *********************************************************/

      $rugi_laba_kotor = $total_penjualan - $saldo_HPP;

/****************************************** ADMIN BIAYA UMUM ************************************/
      $total_biaya_admin_umum = 0;

      foreach($list_coa_biaya_admin_umum as $row){
        //posisi Debet Kredit Akun HPP
        $sql_posisi_dk = $this->db->query("SELECT * FROM public.beone_coa WHERE coa_id =".intval($row['coa_id']));
        $hasil_posisi_dk = $sql_posisi_dk->row_array();

        //nilai Mutasi Akun BIAYA ADMIN DAN UMUM di General Ledger
        $sql_dk_admin_umum = $this->db->query("SELECT SUM(debet) as totdebet, SUM(kredit) as totkredit FROM public.beone_gl WHERE gl_date BETWEEN '$tgl_awal_formated' AND '$tgl_akhir_formated' AND coa_id =".intval($row['coa_id']));
        $hasil_dk_admin_umum = $sql_dk_admin_umum->row_array();

        if ($hasil_posisi_dk['tipe_akun'] == 1 OR $hasil_posisi_dk['tipe_akun'] == 5 OR $hasil_posisi_dk['tipe_akun'] == 6 OR $hasil_posisi_dk['tipe_akun'] == 7 OR $hasil_posisi_dk['tipe_akun'] == 9){
            if ($hasil_posisi_dk['nomor'] == '118.01.01' OR $hasil_posisi_dk['nomor'] == '118.01.02'){
                $saldo_admin = $hasil_dk_admin_umum['totkredit'] - $hasil_dk_admin_umum['totdebet'];
            }else{
                $saldo_admin = $hasil_dk_admin_umum['totdebet'] - $hasil_dk_admin_umum['totkredit'];
            }
        }elseif($hasil_posisi_dk['tipe_akun'] == 2 OR $hasil_posisi_dk['tipe_akun'] == 3 OR $hasil_posisi_dk['tipe_akun'] == 4 OR $hasil_posisi_dk['tipe_akun'] == 8){
            $saldo_admin = $hasil_dk_admin_umum['totkredit'] - $hasil_dk_admin_umum['totdebet'];
        }
      $total_biaya_admin_umum = $total_biaya_admin_umum + $saldo_admin;
      }
/****************************************** ADMIN BIAYA UMUM ************************************/

      $rugi_laba_usaha = $rugi_laba_kotor - $total_biaya_admin_umum;

//**************************************** PENDAPATAN BIAYA LAIN ***************************
$total_pendapatan_biaya_lain = 0;


foreach($list_coa_pendapatan_lain as $row){
  //posisi Debet Kredit Akun PENDAPATAN LAIN LAIN
  $sql_posisi_dk = $this->db->query("SELECT * FROM public.beone_coa WHERE coa_id =".intval($row['coa_id']));
  $hasil_posisi_dk = $sql_posisi_dk->row_array();

  //nilai Mutasi Akun PENDAPATAN LAIN LAIN di General Ledger
  $sql_dk_pendapatan = $this->db->query("SELECT SUM(debet) as totpdebet, SUM(kredit) as totpkredit FROM public.beone_gl WHERE gl_date BETWEEN '$tgl_awal_formated' AND '$tgl_akhir_formated' AND coa_id =".intval($row['coa_id']));
  $hasil_dk_pendapatan = $sql_dk_pendapatan->row_array();

  if ($hasil_posisi_dk['tipe_akun'] == 1 OR $hasil_posisi_dk['tipe_akun'] == 5 OR $hasil_posisi_dk['tipe_akun'] == 6 OR $hasil_posisi_dk['tipe_akun'] == 7 OR $hasil_posisi_dk['tipe_akun'] == 9){
      if ($hasil_posisi_dk['nomor'] == '118.01.01' OR $hasil_posisi_dk['nomor'] == '118.01.02'){
        $saldo_pendapatan = $hasil_dk_pendapatan['totpkredit'] - $hasil_dk_pendapatan['totpdebet'];
      }else{
        $saldo_pendapatan = $hasil_dk_pendapatan['totpdebet'] - $hasil_dk_pendapatan['totpkredit'];
      }
  }elseif($hasil_posisi_dk['tipe_akun'] == 2 OR $hasil_posisi_dk['tipe_akun'] == 3 OR $hasil_posisi_dk['tipe_akun'] == 4 OR $hasil_posisi_dk['tipe_akun'] == 8){
      $saldo_pendapatan = $hasil_dk_pendapatan['totpkredit'] - $hasil_dk_pendapatan['totpdebet'];
  }

  $total_pendapatan_biaya_lain = $total_pendapatan_biaya_lain + $saldo_pendapatan;
}
//******************************************* END PENDAPATAN BIAYA LAIN ***************************

  $laba_rugi_bersih_usaha = $rugi_laba_usaha + $total_pendapatan_biaya_lain;
?>
