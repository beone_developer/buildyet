<h3>Fix Asset Akumulasi</h3>
<div class="portlet light bordered">
  <?php if($tipe == "Ubah"){?>
  <form role="form" method="post">
    <div class="form-body">
      <div class="row">
        <div class="col-sm-4">
            <label>Nama Asset</label>
            <input type="text" class="form-control" value="<?=isset($default['nama_asset'])? $default['nama_asset'] : ""?>" name="nama_asset" readonly required>
        </div>
        <div class="col-sm-4">
            <label>Perolehan</label>
            <input type="hidden" class="form-control" value="<?=isset($default['akm_id'])? $default['akm_id'] : ""?>" name="akm_id" readonly required>
            <input type="text" class="form-control" id='akm_penyusutan' value="<?=isset($default['akm_penyusutan'])? $default['akm_penyusutan'] : ""?>" name="akm_penyusutan" readonly required>
        </div>
        </div>

        <hr />

        <div class="row">
          <div class="col-sm-3">
              <label>Januari</label>
              <input type="text" class="form-control" value="<?=isset($default['jan'])? $default['jan'] : ""?>" name="jan" id="jan" required>
          </div>
          <div class="col-sm-3">
              <label>Februari</label>
              <input type="text" class="form-control" value="<?=isset($default['feb'])? $default['feb'] : ""?>" name="feb" id="feb" required>
          </div>
          <div class="col-sm-3">
              <label>Maret</label>
              <input type="text" class="form-control" value="<?=isset($default['mar'])? $default['mar'] : ""?>" name="mar" id="mar" required>
          </div>
          <div class="col-sm-3">
              <label>April</label>
              <input type="text" class="form-control" value="<?=isset($default['apr'])? $default['apr'] : ""?>" name="apr" id="apr" required>
          </div>
        </div>

        <div class="row">
          <div class="col-sm-3">
              <label>Mei</label>
              <input type="text" class="form-control" value="<?=isset($default['mei'])? $default['mei'] : ""?>" name="mei" id="mei" required>
          </div>
          <div class="col-sm-3">
              <label>Juni</label>
              <input type="text" class="form-control" value="<?=isset($default['jun'])? $default['jun'] : ""?>" name="jun" id="jun" required>
          </div>
          <div class="col-sm-3">
              <label>Juli</label>
              <input type="text" class="form-control" value="<?=isset($default['jul'])? $default['jul'] : ""?>" name="jul" id="jul" required>
          </div>
          <div class="col-sm-3">
              <label>Agustus</label>
              <input type="text" class="form-control" value="<?=isset($default['ags'])? $default['ags'] : ""?>" name="ags" id="ags" required>
          </div>
        </div>

        <div class="row">
          <div class="col-sm-3">
              <label>September</label>
              <input type="text" class="form-control" value="<?=isset($default['sep'])? $default['sep'] : ""?>" name="sep" id="sep" required>
          </div>
          <div class="col-sm-3">
              <label>Oktober</label>
              <input type="text" class="form-control" value="<?=isset($default['okt'])? $default['okt'] : ""?>" name="okt" id="okt" required>
          </div>
          <div class="col-sm-3">
              <label>November</label>
              <input type="text" class="form-control" value="<?=isset($default['nov'])? $default['nov'] : ""?>" name="nov" id="nov" required>
          </div>
          <div class="col-sm-3">
              <label>Desember</label>
              <input type="text" class="form-control" value="<?=isset($default['des'])? $default['des'] : ""?>" name="des" id="des" required>
          </div>
        </div>

    </div>
    <br />
    <div class="form-actions">
        <a href='<?php echo base_url('Fixasset_controller');?>' class='btn default'> Kembali</a>
        <button type="submit" class="btn blue" name="submit_fixasset">Update</button>
    </div>
  </form>
<?php }?>

<br />

<table class="table table-striped table-bordered table-hover" id="sample_2">
        <thead>
          <tr>
              <th><center><small>Nama Asset</small></center></th>
              <th><center><small>Perolehan</small></center></th>
              <th><center><small>Jan</small></center></th>
              <th><center><small>Feb</small></center></th>
              <th><center><small>Mar</small></center></th>
              <th><center><small>Apr</small></center></th>
              <th><center><small>Mei</small></center></th>
              <th><center><small>Jun</small></center></th>
              <th><center><small>Jul</small></center></th>
              <th><center><small>Ags</small></center></th>
              <th><center><small>Sep</small></center></th>
              <th><center><small>Okt</small></center></th>
              <th><center><small>Nov</small></center></th>
              <th><center><small>Des</small></center></th>
              <th><center><small>Nilai Buku</small></center></th>
              <th><center><small>Action</small></center></th>
          </tr>
        </thead>
        <tbody>
          <?php 	foreach($list_fixed_asset_akm as $row){ ?>
            <tr>
                <td><small><?php echo $row['nama_asset'];?></small></td>
                <td><small><?php echo number_format($row['akm_penyusutan'],2);?></small></td>
                <td><small><?php echo number_format($row['jan'],2);?></small></td>
                <td><small><?php echo number_format($row['feb'],2);?></small></td>
                <td><small><?php echo number_format($row['mar'],2);?></small></td>
                <td><small><?php echo number_format($row['apr'],2);?></small></td>
                <td><small><?php echo number_format($row['mei'],2);?></small></td>
                <td><small><?php echo number_format($row['jun'],2);?></small></td>
                <td><small><?php echo number_format($row['jul'],2);?></small></td>
                <td><small><?php echo number_format($row['ags'],2);?></small></td>
                <td><small><?php echo number_format($row['sep'],2);?></small></td>
                <td><small><?php echo number_format($row['okt'],2);?></small></td>
                <td><small><?php echo number_format($row['nov'],2);?></small></td>
                <td><small><?php echo number_format($row['des'],2);?></small></td>
                <td><small><?php echo number_format($row['nilai_buku'],2);?></small></td>
                <td><small><a href='<?php echo base_url('Fixasset_controller/Edit_asset_akm/'.$row['periode_id'].'/'.$row['fix_asset_id'].'');?>' class='btn blue'> Update </a></small></td>
            </tr>
            <?php
              }
            ?>
        </tbody>
    </table>
</div>


<script type="text/javascript">

var jan = document.getElementById('jan');
  jan.addEventListener('keyup', function(e){
  jan.value = formatRupiah(this.value, 'Rp. ');
});

var feb = document.getElementById('feb');
  feb.addEventListener('keyup', function(e){
  feb.value = formatRupiah(this.value, 'Rp. ');
});

var mar = document.getElementById('mar');
  mar.addEventListener('keyup', function(e){
  mar.value = formatRupiah(this.value, 'Rp. ');
});

var apr = document.getElementById('apr');
  apr.addEventListener('keyup', function(e){
  apr.value = formatRupiah(this.value, 'Rp. ');
});

var mei = document.getElementById('mei');
  mei.addEventListener('keyup', function(e){
  mei.value = formatRupiah(this.value, 'Rp. ');
});

var jun = document.getElementById('jun');
  jun.addEventListener('keyup', function(e){
  jun.value = formatRupiah(this.value, 'Rp. ');
});

var jul = document.getElementById('jul');
  jul.addEventListener('keyup', function(e){
  jul.value = formatRupiah(this.value, 'Rp. ');
});

var ags = document.getElementById('ags');
  ags.addEventListener('keyup', function(e){
  ags.value = formatRupiah(this.value, 'Rp. ');
});

var sep = document.getElementById('sep');
  sep.addEventListener('keyup', function(e){
  sep.value = formatRupiah(this.value, 'Rp. ');
});

var okt = document.getElementById('okt');
  okt.addEventListener('keyup', function(e){
  okt.value = formatRupiah(this.value, 'Rp. ');
});

var nov = document.getElementById('nov');
  nov.addEventListener('keyup', function(e){
  nov.value = formatRupiah(this.value, 'Rp. ');
});

var des = document.getElementById('des');
  des.addEventListener('keyup', function(e){
  des.value = formatRupiah(this.value, 'Rp. ');
});


/* Fungsi formatRupiah */
function formatRupiah(angka, prefix){
  var number_string = angka.replace(/[^,\d]/g, '').toString(),
  split   		= number_string.split(','),
  sisa     		= split[0].length % 3,
  rupiah     		= split[0].substr(0, sisa),
  ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);

  // tambahkan titik jika yang di input sudah menjadi angka ribuan
  if(ribuan){
    separator = sisa ? '.' : '';
    rupiah += separator + ribuan.join('.');
  }

  rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
  return prefix == undefined ? rupiah : (rupiah ? rupiah : '');
}
</script>
