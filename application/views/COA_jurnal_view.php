<h3>Konfigurasi COA Transaksi <b><?=$tipe?></b></h3>
<div class="portlet light bordered">
  <div class="portlet-title">

    <?php
      if ($tipe == "Ubah"){
    ?>
    <form role="form" method="post">
        <div class="form-body">
          <div class="row">
            <div class="col-sm-4">
                <label>Nama Transaksi</label>
                <input type="text" class="form-control" placeholder="Nama Transaksi" id="nama_transaksi" value="<?=isset($default['nama_coa_jurnal'])? $default['nama_coa_jurnal'] : ""?>" name="nama_transaksi" required>
            </div>
            <div class="col-sm-4">
                <label>Keterangan</label>
                <input type="text" class="form-control" placeholder="Nama Satuan" id="keterangan" value="<?=isset($default['keterangan_coa_jurnal'])? $default['keterangan_coa_jurnal'] : ""?>" name="keterangan" required>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label>COA</label>
                    <div class="input-group">
                       <select id="select2-single-input-sm" class="form-control input-sm select2-multiple" name="coa" required>
                         <option value="<?=isset($default['coa_id'])? $default['coa_id'] : ""?>"><?=isset($default['ncoa'])? $default['ncoa'] : ""?></option>
                         <?php 	foreach($list_coa as $row){ ?>
                           <option value="<?php echo $row['coa_id'];?>"><?php echo $row['nomor']." | ".$row['nama'];?></option>
                         <?php } ?>
                       </select>
                 </div>
               </div>
            </div>
            </div>
            </div>
        <br />
        <div class="form-actions">
            <a href='<?php echo base_url('COA_controller/index_coa_jurnal');?>' class='btn default'> Cancel</a>
            <button type="submit" class="btn blue" name="submit_coa_jurnal">Update</button>
        </div>
      </form>
      <?php
      }
      ?>

      <hr />
      <br />

      <div class="tools"> </div>
  </div>

<table class="table table-striped table-bordered table-hover" id="sample_1">
        <thead>
          <tr>
              <th><center>Transaksi</center></th>
              <th><center>Keterangan</center></th>
              <th><center>COA</center></th>
              <th><center>No COA</center></th>
              <th><center>Action</center></th>
          </tr>
        </thead>
        <tbody>
          <?php 	foreach($list_coa_jurnal as $row){ ?>
            <tr>
                <td><?php echo $row['nama_coa_jurnal'];?></td>
                <td><?php echo $row['keterangan_coa_jurnal'];?></td>
                <td><?php echo $row['ncoa'];?></td>
                <td><?php echo $row['coa_no'];?></td>
                <td><a href='<?php echo base_url('COA_controller/Edit_coa_jurnal/'.$row['coa_jurnal_id'].'');?>' class='btn blue'><i class="fa fa-pencil"></i></a></td>
            </tr>
            <?php
              }
            ?>
        </tbody>
    </table>
</div>
