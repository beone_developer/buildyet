<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"></script>
<?php $list_item = $this->db->query("SELECT * FROM public.beone_item");?>


<div class="portlet box blue ">
      <div class="portlet-title">
          <div class="caption">
              <i class="fa fa-gift"></i> Form PM</div>
          <div class="tools">
              <a href="" class="collapse"> </a>
              <a href="#portlet-config" data-toggle="modal" class="config"> </a>
              <a href="" class="reload"> </a>
              <a href="" class="remove"> </a>
          </div>
      </div>
      <div class="portlet-body form">

	  <form role="form" method="post">
			<div class="form-body">
				<div class="row">
                  <div class="col-sm-5"><h3><b><?php echo $default['no_pm'];?></b></h3></div>
                  <input type"text" name="no_pm" value='<?php echo $default['no_pm'];?>' hidden>
                  <div class="col-sm-7"><h3><?php echo $default['tgl_pm'];?></h3></div>
                  <input type"text" name="tanggal" value='<?php echo helper_tanggalupdate($default['tgl_pm']);?>' hidden>
                </div>

				<div class="row">
                    <div class="col-sm-5"><h4><?php echo $default['ncustomer'];?></h4></div>
                    <input type"text" name="customer" value='<?php echo $default['customer_id'];?>' hidden>
                    <div class="col-sm-7"><h3><?php echo $default['keterangan_artikel'];?></h3></div>
                    <input type"text" name="keterangan" value='<?php echo $default['keterangan_artikel'];?>' hidden>
                </div>
				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
							<label>Quantity</label>
							<input type='text' class='form-control' name='qty' id='qty' value='<?php echo $default['qty'];?>' readonly>
						</div>
					</div>
				</div>
			</div>



			<hr / style="border-color: #3598DC;">
            	<table  id="datatable" class="table striped hovered cell-hovered">
            		<thead>
            			<tr>
            				<td width="20%">Item</td>
            				<td width="15%">Qty</td>
            				<td width="15%">Satuan</td>
                            <td width="5%"></td>
            			</tr>
            		 </thead>
                    <tbody id="container">

					<?php
                            $ctr = 0;
                            foreach($default_detail as $row){
                            $ctr = $ctr + 1;
                            $nama_ctr = "x".$ctr;
                            $nama_item = "item_".$nama_ctr;
                            $nama_item_id = "item_id_".$nama_ctr;
                            $nama_qty = "qty_".$nama_ctr;
                            $nama_satuan = "satuan_".$nama_ctr;
                            $nama_satuan_id = "satuan_id_".$nama_ctr;
                            $rowss = "rows_".$nama_ctr;
                            $detail_id = "detail_id_".$nama_ctr;

                            $sql_cari_nama_item = $this->db->query("SELECT * FROM public.beone_item WHERE item_id = ".intval($row['item_id']));
                            $hasil_cari_nama_item = $sql_cari_nama_item->row_array();
							$nitem = $hasil_cari_nama_item['nama'];

							$sql_cari_nama_satuan = $this->db->query("SELECT * FROM public.beone_satuan_item WHERE satuan_id = ".intval($row['satuan_id']));
                            $hasil_cari_nama_satuan = $sql_cari_nama_satuan->row_array();
                            $nsatuan = $hasil_cari_nama_satuan['satuan_code'];
                          ?>

                          <tr class='records' id='<?php echo $nama_ctr;?>'>
                          <td><input class="form-control" id='<?php echo $nama_item;?>' name='<?php echo $nama_item;?>' type="text" value='<?php echo $nitem;?>' readonly></td>
                          <td><input class="form-control" id='<?php echo $nama_qty;?>' name='<?php echo $nama_qty;?>' type="text" value='<?php echo number_format($row['qty'],2);?>' readonly></td>
                          <td><input class="form-control" id='<?php echo $nama_satuan;?>' name='<?php echo $nama_satuan;?>' type="text" value='<?php echo $nsatuan;?>' readonly></td>
                          <td><button type="button" class="btn red" onclick="hapus('<?php echo $nama_ctr;?>')">X</button></td>
                          <td><input class="form-control" id='<?php echo $nama_item_id;?>' name='<?php echo $nama_item_id;?>' type="hidden" value='<?php echo $row['item_id'];?>' readonly></td>
                          <td><input class="form-control" id='<?php echo $nama_satuan_id;?>' name='<?php echo $nama_satuan_id;?>' type="hidden" value='<?php echo $row['satuan_id'];?>' readonly></td>
                          <td><input id='<?php echo $rowss;?>' name="rows[]" value='<?php echo $nama_ctr;?>' type="hidden"></td></td>
                          <td><input id='<?php echo $detail_id;?>' name='<?php echo $detail_id;?>' value='<?php echo $row['pm_detail_id'];?>' type="hidden"></td></tr>

                          <?php
                            }
                           ?>
            		</tbody>
            	</table>


			<br/>
			<div class="form-actions">
				<a class="btn blue" data-toggle="modal" href="#responsive"> Tambah Data </a>
				<?php if (helper_security("satuan_add") == 1) { ?>
					<button type="submit" class="btn blue" name="submit_pm">Submit</button>
				<?php } ?>
			</div>
		</form>
  </div>
</div>

<!--------------------------- MODAL ADD ITEM--------------------------------------------->
<div id="responsive" class="modal fade" tabindex="-1" data-width="760">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Detail Item</h4>
    </div>
    <div class="modal-body">
			<div class="row">
				<div class="col-md-2"></div>
				<div class="col-md-8">
					<h3><b id='name-modal'></b></h3>
					<input type='hidden' class='form-control' placeholder="No. PM" name='no_modal' id='no_modal'>
				</div>
				<div class="col-md-2"></div>
			</div>
			<div class="row">
				<div class="col-md-2"></div>
				<div class="col-md-8">
				<div class="form-group">
					<label>Item</label>
					<select id='item_modal' class='form-control input-sm select2-multiple' name='item_modal'>
					<option value=0><?php echo " - Pilih Item - ";?></option>
					<?php foreach($list_item->result_array() as $item){ echo '<option value='.$item['item_id'].'>'.$item['nama'].'</option>';} ?>
					</select>
				</div>
				</div>
				<div class="col-md-2"></div>
			</div>

			<div class="row">
				<div class="col-md-2"></div>
				<div class="col-md-2">
					<div class="form-group">
					<label>Quantity</label>
					<input type='text' class='form-control' placeholder="Quantity" name='qty_modal' id='qty_modal' required>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<label>Satuan</label>
						<select id='satuan_modal' class='form-control input-sm select2-multiple' name='satuan_modal'>
						<option value="<?=isset($default['satuan_id'])? $default['satuan_id'] : ""?>"><?=isset($default['satuan_code'])? $default['satuan_code'] : ""?></option>
							<?php 	foreach($list_satuan as $satuan){ ?>
								<option value="<?php echo $satuan['satuan_id'];?>"><?php echo $satuan['satuan_code'];?></option>
							<?php } ?>
							<
						</select>
					</div>
				</div>
				<div class="col-md-2"></div>
			</div>
		</div>
		<div class="modal-footer">
			<button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
			<button type="button" class="btn green" name="add_btn" id="add_btn">Insert</button>
		</div>
</div>

<script>
$(document).ready(function() {
    		var count = 0;

    		$("#add_btn").click(function(){
					count += 1;

          var item = document.getElementById('item_modal');
          var qty = document.getElementById('qty_modal');
          var satuan = document.getElementById('satuan_modal');
		  var namaItem = $('#item_modal option:selected').text();
		  var namaSatuan = $('#satuan_modal option:selected').text();
	
		  $('#container').append(
							 '<tr class="records" id="'+count+'">'
						 + '<td><input class="form-control" id="item_' + count + '" name="item_'+count+'" type="text" value="'+namaItem+'" readonly></td>'
						 + '<td><input class="form-control" id="qty_' + count + '" name="qty_'+count+'" type="text" value="'+qty.value+'" readonly></td>'
						 + '<td><input class="form-control" id="satuan_' + count + '" name="satuan_'+count+'" type="text" value="'+namaSatuan+'" readonly></td>'
						 + '<td><button type="button" class="btn red" onclick="hapus('+count+')">X</button></td>'
						 + '<td><input class="form-control" id="item_id_' + count + '" name="item_id_'+count+'" type="hidden" value="'+item.value+'" readonly></td>'
						 + '<td><input class="form-control" id="satuan_id_' + count + '" name="satuan_id_'+count+'" type="hidden" value="'+satuan.value+'" readonly></td>'
						 + '<td><input id="rows_' + count + '" name="rows[]" value="'+ count +'" type="hidden"></td></tr>'
					);

          $('#responsive').modal('hide');
				});

				$(".remove_item").live('click', function (ev) {
    			if (ev.type == 'click') {
	        	$(this).parents(".records").fadeOut();
	        	$(this).parents(".records").remove();
        	}
     		});
		});
		function hapus(rowid)
		{
			var row = document.getElementById(rowid);
			row.parentNode.removeChild(row);
		}
</script>

<script>
	function dialogHapus(urlHapus) {
		if (confirm("Apakah anda yakin ingin menghapus ini ?")) {
			document.location = urlHapus;
		}
	}
</script>

<script type="text/javascript">
	var qty = document.getElementById('qty');
	qty.addEventListener('keyup', function (e) {
		qty.value = formatRupiah(this.value, 'Rp. ');
	});

	/* Fungsi formatRupiah */
	function formatRupiah(angka, prefix) {
		var number_string = angka.replace(/[^,\d]/g, '').toString(),
			split = number_string.split(','),
			sisa = split[0].length % 3,
			rupiah = split[0].substr(0, sisa),
			ribuan = split[0].substr(sisa).match(/\d{3}/gi);

		// tambahkan titik jika yang di input sudah menjadi angka ribuan
		if (ribuan) {
			separator = sisa ? '.' : '';
			rupiah += separator + ribuan.join('.');
		}

		rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
		return prefix == undefined ? rupiah : (rupiah ? rupiah : '');
	}
</script>

<script>
	function tampilnomor(str) {
		if (str == "") {
			document.getElementById("txtnopm").innerHTML = "";
			return;
		}
		if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp = new XMLHttpRequest();
		} else {// code for IE6, IE5
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange = function () {
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
				document.getElementById("txtnopm").innerHTML = xmlhttp.responseText;
			}
		}
		xmlhttp.open("GET", "pm_controller/get_nomor_pm?q=" + str, true);
		xmlhttp.send();
	}
</script>
