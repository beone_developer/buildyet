<h3>Master Satuan <b><?=$tipe?></b></h3>
<div class="portlet light bordered">
  <div class="portlet-title">


    <form role="form" method="post">
        <div class="form-body">
          <div class="row">
            <div class="col-sm-4">
                <label>Kode Satuan</label>
                <input type="text" class="form-control" placeholder="Kode Satuan" id="satuan_code" value="<?=isset($default['satuan_code'])? $default['satuan_code'] : ""?>" name="satuan_code" required>
            </div>
            <div class="col-sm-4">
                <label>Keterangan</label>
                <input type="text" class="form-control" placeholder="Nama Satuan" id="keterangan" value="<?=isset($default['keterangan'])? $default['keterangan'] : ""?>" name="keterangan" required>
            </div>
            <div class="col-sm-4"></div>
            </div>
            </div>
        <br />
        <div class="form-actions">
            <a href='<?php echo base_url('Item_controller/index_satuan');?>' class='btn default'> Cancel</a>
            <?php if(helper_security("satuan_add") == 1){?>
            <button type="submit" class="btn blue" name="submit_satuan">Submit</button>
            <?php }?>
        </div>
      </form>

      <hr />
      <br />

      <div class="tools"> </div>
  </div>

<table class="table table-striped table-bordered table-hover" id="sample_1">
        <thead>
          <tr>
              <th><center>Satuan</center></th>
              <th><center>Keterangan</center></th>
              <th><center>Action</center></th>
          </tr>
        </thead>
        <tbody>
          <?php 	foreach($list_satuan as $row){ ?>
            <tr>
                <td><?php echo $row['satuan_code'];?></td>
                <td><?php echo $row['keterangan'];?></td>
                <td>
                    <?php if(helper_security("satuan_edit") == 1){?>
                    <a href='<?php echo base_url('Item_controller/Edit_satuan/'.$row['satuan_id'].'');?>' class='btn blue'><i class="fa fa-pencil"></i></a>
                    <?php }?>
                    <?php if(helper_security("satuan_delete") == 1){?>
                    <a href="javascript:dialogHapus('<?php echo base_url('Item_controller/delete_satuan/'.$row['satuan_id'].'');?>')" class='btn red'><i class="fa fa-trash-o"></i></a>
                    <?php }?>
                </td>
            </tr>
            <?php
              }
            ?>
        </tbody>
    </table>
</div>

<script>
	function dialogHapus(urlHapus) {
	  if (confirm("Apakah anda yakin ingin menghapus ini ?")) {
		document.location = urlHapus;
	  }
	}
</script>
