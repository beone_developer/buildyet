        <!-- END PAGE HEADER-->
        <?php
          $tgl = "";
          $ket = "";
          $no_aju = "";
          foreach($print_pindah as $row){
            $tgl = $row['trans_date'];
            $ket = $row['keterangan'];
            $no_aju = $row['nomor_transaksi'];
            $gd = $row['gudang_id'];
          }

          $this->mysql = $this ->load -> database('mysql', TRUE);
          $data_header_tpb = $this->mysql->query("select * from tpb_header where nomor_aju = '$no_aju'");
          $hasil_header_tpb = $data_header_tpb -> row_array();
          $jenis_bc = "BC ".$hasil_header_tpb['KODE_DOKUMEN_PABEAN'];
          $tgl_bc = $hasil_header_tpb['TANGGAL_AJU'];
          $id_header = $hasil_header_tpb['ID'];
          $no_daftar = $hasil_header_tpb['NOMOR_DAFTAR'];
          $tgl_daftar = date_create($hasil_header_tpb['TANGGAL_DAFTAR']);

          $received = $this->db->query("SELECT * FROM public.beone_received_import WHERE nomor_aju = '$no_aju'");
          $hasil_received = $received -> row_array();
          $no_received = $hasil_received['nomor_received'];
          $tgl_received = $hasil_received['tanggal_received'];

          $data_kontainer = $this->mysql->query("select * from tpb_kontainer where id_header = ".intval($id_header));
          $hasil_kontainer = $data_kontainer -> row_array();
          $no_kontainer = $hasil_kontainer['NOMOR_KONTAINER'];
          $ukuran_kontainer = $hasil_kontainer['KODE_UKURAN_KONTAINER'];

          $data_barang = $this->mysql->query("select * from tpb_barang where id_header = ".intval($id_header));
          $hasil_barang = $data_barang -> row_array();
          $gross_weight = $hasil_barang['JUMLAH_SATUAN'];
          $net_weight = $hasil_barang['NETTO'];

        ?>

        <div class="invoice">
<?php if ($ket == "IMPORT"){?>
            <center><h2><b>BUKTI PENERIMAAN BARANG<b></h2></center>
            <hr/>
            <div class="row">
                <div class="col-xs-2">
                    <ul class="list-unstyled">
                        <li> <h5> Kepada </h5> </li>
                        <li> <h5> Dari </h5> </li>
                    </ul>
                </div>
                <div class="col-xs-4">
                    <ul class="list-unstyled">
                        <li><h5>: BAGIAN GUDANG </h5> </li>
                        <li><h5>: IMPOR </h5> </li>
                    </ul>
                    <ul class="list-unstyled">

                        <li><h5>: <?php echo $jenis_bc.' / '.date_format($tgl_daftar,"d-m-Y");?> </h5> </li>
                    </ul>
                    <ul class="list-unstyled">
                        <li><h5>: <?php echo $no_daftar;?> </h5> </li>
                    </ul>
                </div>
                <div class="col-xs-2">
                    <ul class="list-unstyled">
                      <li> <h5> No Received </h5> </li>
                    </ul>
                    <ul class="list-unstyled">
                      <li> <h5> Received Date </h5> </li>
                    </ul>
                </div>
                <div class="col-xs-4">
                  <ul class="list-unstyled">
                      <li><h5>: <?php echo $no_received;?> </h5> </li>
                  </ul>
                  <ul class="list-unstyled">
                      <li><h5>: <?php echo date('d-m-Y', strtotime($tgl_received));?> </h5> </li>
                  </ul>
                </div>
            </div>
            <hr />
            <div class="row">
                <div class="col-xs-12">
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th> No </th>
                                <th class="hidden-xs"> Nomor Kontainer </th>
                                <th class="hidden-xs"> Nama Barang </th>
                                <th class="hidden-xs"> Ukuran </th>
                                <th class="hidden-xs"> Satuan </th>
                                <th class="hidden-xs"> Jumlah </th>
                                <th class="hidden-xs"> Keterangan </th>
                            </tr>
                        </thead>
                        <tbody>
                          <?php
                          $qty_out = 0;
                          $qty_in = 0;
                          $no = 0;
                          foreach($print_pindah as $row){
                                  $no = $no + 1;
                          ?>

                                  <tr>
                                    <td> <?php echo $no;?> </td>
                                    <td> <?php echo $no_kontainer;?> </td>
                                    <td class="hidden-xs"> <?php echo $row['nitem'];?> </td>
                                    <td class="hidden-xs"> <?php echo "1 X ".$ukuran_kontainer;?> </td>
                                    <td class="hidden-xs"> <?php echo "KGM";?> </td>
                                    <td class="hidden-xs"> <?php echo "GW = ".number_format($gross_weight, 2);?> </td>
                                    <td class="hidden-xs"> <?php echo $row['keterangan'];?> </td>
                                  <tr>
                                  <tr>
                                    <td> </td>
                                    <td> </td>
                                    <td class="hidden-xs">  </td>
                                    <td class="hidden-xs">  </td>
                                    <td class="hidden-xs">  </td>
                                    <td class="hidden-xs"> <?php echo "NW = ".number_format($net_weight, 2);?> </td>
                                    <td class="hidden-xs"> </td>
                                  <tr>
                          <?php
                                  $qty_out = $qty_out + $row['qty_out'];
                                  $qty_in = $qty_in + $row['qty_in'];

                              } ?>

                                  <!--<tr>
                                    <td> </td>
                                    <td> </td>
                                    <td class="hidden-xs"><b>TOTAL</b></td>
                                    <td class="hidden-xs"> </td>
                                    <td class="hidden-xs"><b> <?php //echo number_format($qty_in, 2);?> <b/> </td>
                                    <td class="hidden-xs"><b> <?php //echo number_format($qty_out, 2);?> <b/></td>
                                    <td class="hidden-xs"> </td>
                                  <tr>-->
                        </tbody>
                    </table>
                </div>
            </div>
<?php }else{?>
            <center><h2><b>BUKTI PENYERAHAN BARANG<b></h2></center>
            <hr/>
            <div class="row">
                <div class="col-xs-2">
                    <ul class="list-unstyled">
                        <li> <h5> Kepada </h5> </li>
                        <li> <h5> Dari </h5> </li>
                    </ul>
                </div>
                <div class="col-xs-4">
                    <ul class="list-unstyled">
                        <li><h5>: PRODUKSI I </h5> </li>
                        <li><h5>: WAREHOUSE </h5> </li>
                    </ul>
                </div>
                <div class="col-xs-2">
                    <ul class="list-unstyled">
                      <li> <h5> Tanggal </h5> </li>
                    </ul>
                </div>
                <div class="col-xs-4">
                  <ul class="list-unstyled">
                      <li><h5>: <?php echo helper_tanggalupdate($tgl);?> </h5> </li>
                  </ul>
                </div>
            </div>
            <hr />
            <div class="row">
                <div class="col-xs-12">
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th> No </th>
                                <th class="hidden-xs"> Kode Item </th>
                                <th class="hidden-xs"> Nama </th>
                                <th class="hidden-xs"> Satuan </th>
                                <th class="hidden-xs"> Qty In </th>
                                <th class="hidden-xs"> Qty Out </th>
                                <th class="hidden-xs"> Keterangan </th>
                            </tr>
                        </thead>
                        <tbody>
                          <?php
                          $qty_out = 0;
                          $qty_in = 0;
                          $no = 0;
                          foreach($print_pindah as $row){
                                  $no = $no + 1;
                          ?>

                                  <tr>
                                    <td> <?php echo $no;?> </td>
                                    <td> <?php echo $row['item_code'];?> </td>
                                    <td class="hidden-xs"> <?php echo $row['nitem'];?> </td>
                                    <td class="hidden-xs"> <?php echo "KG";?> </td>
                                    <td class="hidden-xs"> <?php echo number_format($row['qty_in'], 2);?> </td>
                                    <td class="hidden-xs"> <?php echo number_format($row['qty_out'], 2);?> </td>
                                    <td class="hidden-xs"> <?php echo $row['keterangan'];?> </td>
                                  <tr>
                          <?php
                                  $qty_out = $qty_out + $row['qty_out'];
                                  $qty_in = $qty_in + $row['qty_in'];

                              } ?>

                                  <tr>
                                    <td> </td>
                                    <td> </td>
                                    <td class="hidden-xs"><b>TOTAL</b></td>
                                    <td class="hidden-xs"> </td>
                                    <td class="hidden-xs"><b> <?php echo number_format($qty_in, 2);?> <b/> </td>
                                    <td class="hidden-xs"><b> <?php echo number_format($qty_out, 2);?> <b/></td>
                                    <td class="hidden-xs"> </td>
                                  <tr>
                        </tbody>
                    </table>
                </div>
            </div>

<?php }?>


            <br />
            <br />
            <br />
            <div class="row">
                <div class="col-xs-4">
                    <div class="well">
                      <center>
                      <strong>Mengetahui,</strong>
                      <br/>
                      <br/>
                      <br/>
                      <strong>( .................... )</strong>
                      </center>
                    </div>
                </div>
                <div class="col-xs-4 invoice-block">
                  <div class="well">
                  <center>
                  <strong>Menerima,</strong>
                  <br/>
                  <br/>
                  <br/>
                  <strong>( .................... )</strong>
                  </center>
                  </div>
                </div>
                <div class="col-xs-4">
                    <div class="well">
                      <center>
                      <strong>Menyerahkan,</strong>
                      <br/>
                      <br/>
                      <br/>
                      <strong>( .................... )</strong>
                      </center>
                    </div>
                </div>
            </div>
            <div class="row">
              <a class="btn btn-lg blue hidden-print margin-bottom-5" onclick="javascript:window.print();"> Print
                  <i class="fa fa-print"></i>
              </a>
              <!--<a class="btn btn-lg green hidden-print margin-bottom-5"> Submit Your Invoice
                  <i class="fa fa-check"></i>
              </a>-->
            </div>
        </div>
