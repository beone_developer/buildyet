<!DOCTYPE html>
<html>
<head>
  <title>Report Table</title>
  <style type="text/css">
    #outtable{
      padding: 20px;
      border:1px solid #e3e3e3;
      width:600px;
      border-radius: 5px;
    }
 
    .short{
      width: 50px;
    }
 
    .normal{
      width: 150px;
    }

    p{
      font-family: sans-serif;
      font-size:8pt;
      color:#5E5B5C;
      margin:5px;
    }
 
    table{
      border-collapse: collapse;
      border-top: 1px solid black;
      border-bottom: 1px solid black;
      font-family: sans-serif;
      font-size:9pt;
      color:#5E5B5C;
      width:100%;
    }
    .text-center{
        text-align:center;
    }
    
    .text-right{
        text-align:right;
    }
    
    .text-left{
        text-align:left;
    }
    
    .cell-border{
      border-collapse: collapse;
      border: 1px solid black;
      padding:5px;
    }
    .left-border{
      border-collapse: collapse;
      border-left: 1px solid black;
    }
    .top-border{
      border-collapse: collapse;
      border-top: 1px solid black;
    }
    .u{
        text-decoration: underline;
    }
    td{
        padding:5px;
    }
  </style>
</head>
<body>
    <div id="header">
    <div style="width: 100%; display: table;">
        <div style="display: table-row; font-family:sans-serif;">
            <div style="width: 200px; display: table-cell;"> Logo </div>
            <div style="display: table-cell;"><center> <h1 style="margin:5px;"><b>PT. BUILDYET INDONESIA</b></h1>
                <h5 style="margin:5px;">JL. RAYA LAMONGAN-GRESIK RT.000 / RW.000</h5>
                <h5 style="margin:5px;">REJOSARI, DEKET, LAMONGAN, 62291 INDONESIA TEL: 0322-3326001</h5>
            <center></div>
        </div>
    </div><br>
    <p><center>SALES CONTRACT </center></p>
    <p style="margin:0px;"><center>****************** </center></p><br>
    <div style="width: 100%; display: table;">
        <div style="display: table-row; font-family:sans-serif;">
            <div style="width: 450px; display: table-cell;">
                <p>CUSTOMER</p>
                <p><?php echo $default['ncustomer'] ?></p>
                <p>UNIT 706, HALESON BUILDING, NO 1 JUBILEE STREET, H.K</p>
                <p><b>RECEIVER</b> <?php echo $default['ncustomer'] ?></p>
                <p>8F., NO.67, ZHOUZI ST., NEIHU.DIST., TAIPEI CITY 11493,TAIWAN(R.O.C.)</p><br>
                <p style="margin-bottom:0px;"><b>Order No.:<?php echo $default['sales_no'] ?></b></p>
            </div>
                
            <div style="display: table-cell;">
                <p>TT RECEIPT DATE WITHIN 60 DAYS AFTER</p>
                <p><b>CUSTOMER CODE: TWPUT</b></p>
                <p><b>ULTIMATE CUSTOMER CODE:</b></p>
                <p><b>DATE: </b> <?php echo $default['trans_date'] ?></p>
                <p><b>FROM: </b> SURABAYA</p>
                <p><b>TO: </b> KEELUNG</p>
            </div>
        </div>
    </div><br>
        <hr style="margin-bottom:1px;margin-top:0px;">
        <table>
            <thead>
                <tr>
                    <th class="text-left" width="30%">Description Of Goods</th>
                    <th class="text-left" width="10%">Quantity</th>
                    <th class="text-center" width="15%">Unit Price</th>
                    <th class="text-center" width="5%"></th>
                    <th class="text-right" width="20%">Amount</th>
                    <th class="text-center" width="20%">Subtotal</th>
                </tr>
            </thead>
        </table>
        <table>
            <thead>
                <tr>
                    <th class="text-center" width="30%"></th>
                    <th class="text-left" width="10%">QTY</th>
                    <th class="text-center" width="15%">F.O.B UNIT PRICE</th>
                    <th class="text-right" width="5%"></th>
                    <th class="text-left" width="20%">TOTAL</th>
                    <th style="color:#FF33C4;" class="text-center" width="20%">(USD)</th>
                </tr>
            </thead>
        </table>
        <table style="border-top:0px;">
            <tbody>
            <?php foreach($default_detail as $user): 
                    $sql = $this->db->query("SELECT i.item_code, i.nama, s.satuan_code FROM beone_item i
                                            LEFT JOIN beone_sales_detail d on i.item_id = d.item_id
                                            LEFT JOIN beone_satuan_item s on i.satuan_id = s.satuan_id
                                            WHERE d.sales_detail_id =".intval($user['sales_detail_id']));
                    $item = $sql->result_array();
                    ?>
                <tr>
                    <td class="text-left" width="30%">
                    <table style="border:0px;">
                    <tbody>
                    <tr>
                    <th>FOOTWEAR</th>
                    <td></td>
                    </tr>
                    <tr>
                    <th>O/NO.:</th>
                    <td><?php echo $default['sales_no'] ?></td>
                    </tr>
                    <tr>
                    <th>ART NO.:</th>
                    <td>370990 01</td>
                    </tr>
                    <tr>
                    <th>STYLE:</th>
                    <td><?php echo $item[0]['nama']; ?></td>
                    </tr>
                    <tr>
                    <th>COLOUR:</th>
                    <td>PUMA BLACK-PUMA WHITE-PUMA GOLD-</td>
                    </tr>
                    <tr>
                    <th>SIZE:</th>
                    <td>3, 4, 5, 6</td>
                    </tr>
                    <tr>
                    <th>H.S.CODE:</th>
                    <td >640399</td>
                    </tr>
                    </tbody>
                    </table>
                    </td>
                    <td class="text-right" width="10%"><?php echo $user['qty']; ?></td>
                    <td class="text-center" width="15%"><?php echo $user['price']; ?></td>
                    <td class="text-right" width="5%">USD</td>
                    <td class="text-right" width="20%"><?php echo $user['amount']; ?></td>
                    <td class="text-right" width="20%"><?php echo number_format((float)$default['subtotal'], 2, '.', ''); ?></td>
                </tr>
                <?php endforeach; ?>
                
                <!-- <tr style="color:#339EFF">
                    <td width="60%" colspan="3"><i>
                        LESS 0.5% DEFECTIVE RETURNS ALLOWANCE
                    </i></td>
                    <td class="text-right" width="20%"><i>USD</i></td>
                    <td class="text-right" width="20%"><i>14,68</i></td>
                    <td></td>
                </tr> -->
                <tr>
                    <td class="top-border" width="60%" colspan="4"><i>
                        TOTAL:
                    </i></td>
                    <td class="top-border text-right" width="20%">USD</td>
                    <td class="top-border text-right" width="20%"><?php echo number_format((float)$default['grandtotal'], 2, '.', ''); ?></td>
                </tr>
            </tbody>
        </table>
        <!-- <p>a</p> -->
        <br><br>
        <!-- <p>b</p> -->

        <table style="border:0px;">
            <tbody>
                <tr>
                    <td width="20%">Seller</td>
                    <!-- <td width="20%"></td> -->
                    <td width="20%">Buyer</td>
                </tr>
                <tr>
                    <td width="20%"></td>
                    <!-- <td width="20%"></td> -->
                    <td width="20%"></td>
                </tr>
                <tr>
                    <td width="20%">_______________</td>
                    <!-- <td width="20%">_______________</td> -->
                    <td width="20%">_______________</td>
                </tr>
                <tr>
                    <td width="20%" style="text-decoration:italic;font-size:8pt;">PT.BUILDYET INDONESIA</td>
                    <!-- <td width="20%" style="text-decoration:italic;font-size:8pt;">MANAGING DIRECTOR</td> -->
                    <td width="20%" style="text-decoration:italic;font-size:8pt;">PURCHASE DEPARTMENT</td>
                </tr>
            </tbody>
        </table>
        <!-- <p>Lembar : (1) Suplier, (2) Pembelian, (3) Hutang, (4) Gudang</p> -->
	 </div>
</body>
</html>