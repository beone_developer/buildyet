<h3>List Hapus Transfer Item</h3>
<div class="portlet light bordered">
  <div class="portlet-title">
      <div class="tools"> </div>
  </div>

<table class="table table-striped table-bordered table-hover" id="sample_1">
        <thead>
          <tr>
              <th width='20%'><center>No Produksi</center></th>
              <th width='20%'><center>Tanggal</center></th>
              <th width='40%'><center>Keterangan</center></th>
              <th width='20%'><center>Action</center></th>
          </tr>
        </thead>
        <tbody>
          <?php 	foreach($list_stock_transfer as $row){ ?>
            <tr>
                <td><center><?php echo $row['transfer_no'];?></center></td>
                <td><center><?php echo $row['transfer_date'];?></center></td>
                <td><?php echo $row['keterangan'];?></td>
                <td><center><a href='<?php echo base_url('Transfer_controller/detail_delete/'.$row['transfer_stock_id'].'');?>' class='btn yellow'><i class="fa fa-eye"></i> </a> <a href="javascript:dialogHapus('<?php echo base_url('Transfer_controller/delete_log/'.$row['transfer_stock_id'].'');?>')" class='btn red'><i class="fa fa-trash-o"></i> </a></center></td>
            </tr>
            <?php
              }
            ?>
        </tbody>
    </table>
</div>

<script>
	function dialogHapus(urlHapus) {
	  if (confirm("Apakah anda yakin ingin menghapus ini ?")) {
		document.location = urlHapus;
	  }
	}
</script>
