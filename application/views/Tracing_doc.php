<h3>Tracing Document</h3>
          <?php
                $no=0;
                $no_transaksi = "";
								foreach($list_tracing as $row){
                $no_transaksi = $row['keterangan'];
                $kode_tracing = $row['kode_tracing'];

                $sqlwip = $this->db->query("SELECT d.gudang_detail_id, d.gudang_id, gd.nama as ngudang, d.trans_date, d.item_id, i.nama as nitem, d.qty_in, d.qty_out, d.nomor_transaksi, d.update_by, u.nama as nuser, d.update_date, d.flag, d.keterangan, d.kode_tracing
            															FROM public.beone_gudang_detail d INNER JOIN public.beone_gudang gd ON d.gudang_id = gd.gudang_id INNER JOIN public.beone_item i ON d.item_id = i.item_id INNER JOIN public.beone_user u ON d.update_by = u.user_id
            															WHERE d.kode_tracing = '$kode_tracing' ORDER BY trans_date asc, gudang_detail_id asc");
            ?>
          <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="tools"> </div>
            </div>
            <table class="table table-striped table-bordered table-hover" id="sample_2">
                    <thead>
                      <tr>
                          <th width="5%"><center><small>No</small></center></th>
                          <th width="10%"><center><small>Tanggal</small></center></th>
                          <th width="10%"><center><small>User</small></center></th>
                          <th width="15%"><center><small>Nomor Transaksi</small></center></th>
                          <th width="15%"><center><small>Keterangan</small></center></th>
                          <th width="10%"><center><small>Gudang</small></center></th>
                          <th width="15%"><center><small>Item</small></center></th>
                          <th width="5%"><center><small>Qty In</small></center></th>
                          <th width="5%"><center><small>Qty Out</small></center></th>
                      </tr>
                    </thead>
                    <tbody>

            <?php
                foreach($sqlwip->result_array() as $row2){
                $no = $no + 1;
            ?>
                <tr>
                    <td><small><?php echo $no;?></small></td>
                    <td><small><?php echo $row2['trans_date'];?></small></td>
                    <td><small><?php echo $row2['nuser'];?></small></td>
                    <td><small><?php echo $row2['nomor_transaksi'];?></small></td>
                    <td><small><?php echo $row2['keterangan'];?></small></td>
                    <td><small><?php echo $row2['ngudang'];?></small></td>
                    <td><small><?php echo $row2['nitem'];?></small></td>
                    <td><small><?php echo number_format($row2['qty_in'],2);?></small></td>
                    <td><small><?php echo number_format($row2['qty_out'],2);?></small></td>
                </tr>
            <?php
              }
              $no = 0;
            ?>

              </tbody>
          </table>
          </div>

            <?php
              }
              ?>
