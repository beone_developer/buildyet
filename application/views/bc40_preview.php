
<div class="portlet light bordered">
	<div class="portlet-title">
		
			<div class="row">
				<p class="text-center" style="font-size: 17px"><b>PEMBERITAHUAN PEMASUKAN BARANG ASAL TEMPAT LAIN DALAM DAERAH PABEAN <br> KE TEMPAT PENIMBUNAN BERIKAT</b></p>
			</div>
			
			<form action="">
				<div class="form-group row">
					<label for="status" class="col-sm-2 col-form-label">Status</label>
					<div class="col-sm-2">
						<input style="border: none;" type="text" readonly class="form-control" id="status" value="<?php 
		                                        $n = 0;
		                                            if ($ptb_edit) {
		                                                foreach ($ptb_edit as $data) {
		                                                    $n = $n + 1;
		                                                    echo $data->URAIAN_STATUS;
		                                                }
		                                            }
		                                                    ?>">
					</div>
				</div>

				<div class="form-group row">
					<label for="status_perbaikan" class="col-sm-2 col-form-label">Status Perbaikan</label>
					<div class="col-sm-2">
						<input style="border: none;" type="text" readonly class="form-control" id="status_perbaikan" value="-">
					</div>
				</div>
				
				<!-- FORM NOMOR PENGAJUAN -->
				<div style="margin-top: 30px;">
					<div class="form-group row">
						<label for="nomor_pengajuan" class="col-sm-2 col-form-label">Nomor Pengajuan</label>
						<div class="col-sm-4">
							<input type="text" readonly class="form-control" id="nomor_pengajuan" value = "<?php 
		                                        $n = 0;
		                                            if ($ptb_edit) {
		                                                foreach ($ptb_edit as $data) {
		                                                    $n = $n + 1;
		                                                    echo $data->nomor_aju;
		                                                }
		                                            }
		                                                    ?>">
						</div>
					</div>

					<div class="form-group row">
						<label for="nomor_pendaftaran" class="col-sm-2 col-form-label">Nomor Pendaftaran</label>
						<div class="col-sm-4">
							<input type="text" class="form-control" id="nomor_pendaftaran" value="<?php 
		                                        $n = 0;
		                                            if ($ptb_edit) {
		                                                foreach ($ptb_edit as $data) {
		                                                    $n = $n + 1;
		                                                    echo $data->nomor_daftar;
		                                                }
		                                            }
		                                                    ?>">
						</div>
					</div>

					<div class="form-group row">
						<label for="tanggal_pendaftaran" class="col-sm-2 col-form-label">Tanggal Pendaftaran</label>
						<div class="col-sm-4">
							<input type="text" class="form-control" id="tanggal_pendaftaran" value="<?php 
		                                                $n = 0;
		                                            if ($ptb_edit) {
		                                                foreach ($ptb_edit as $data) {
		                                                    $n = $n + 1;
		                                                    echo $data->tanggal_daftar;
		                                                }
		                                            }
		                                                    ?>" placeholder="DDMMYY">
						</div>
					</div>		
				</div>
				<!-- TUTUP FORM NOMOR PENGAJUAN -->
				
				<!-- FORM KPPBC BONGKAR -->
				<div style="margin-top: 30px;">
					

					<div class="form-group row">
						<label for="kppbc_pengawas" class="col-sm-2 col-form-label">Kantor Pabean</label>
						<div class="col-sm-4">
							<input type="text" class="form-control" id="kppbc_pengawas" value="<?php 
		                                                $n = 0;
		                                            if ($ptb_edit) {
		                                                foreach ($ptb_edit as $data) {
		                                                    $n = $n + 1;
		                                                    echo $data->kode_kantor_pabean." - ".$data->kantor_pabean;
		                                                }
		                                            }
		                                                    ?>">
						</div>
		                                <label for="tujuan" class="col-sm-2 col-form-label">Kode Gudang PLB </label>
						<div class="col-sm-2">
							<input type="text" class="form-control" id="nama" value ="" >
						</div>
					</div>
		                        
		                        <div class="form-group row">
						<label for="tujuan" class="col-sm-2 col-form-label">Jenis TPB </label>
						<div class="col-sm-4">
							<select id="inputState" class="form-control">
						        <option selected>Choose...</option>
						        <option>...</option>
					        </select>
						</div>
					</div>	
		                    
					<div class="form-group row">
						<label for="tujuan" class="col-sm-2 col-form-label">Tujuan Pengiriman </label>
						<div class="col-sm-4">
							<select id="inputState" class="form-control">
						        <option selected>Choose...</option>
						        <option>...</option>
					        </select>
						</div>
					</div>		
				</div>
				<!-- TUTUP FORM KPPBC BONGKAR -->
				

				<div class="row">
					<div class="col-sm-12">

						<!-- FORM LEFT SIDE -->
						<ol>
							<div class="col-sm-6">
								<p><b>PENGUSAHA TPB</b></p>
									<!-- FORM PENGUSAHA -->
									<div>
		                                                            <div class="form-group row">
											<li>
												<label for="nama" class="col-sm-3 col-form-label">NPWP</label>
												<div class="col-sm-4">
													<select id="inputState" class="form-control">
												        <option selected>NPWP</option>
												        <option>...</option>
											        </select>
												</div>
												<div class="col-sm-4">
													<input type="text" class="form-control" id="nama" value = "<?php
		                                                                                    if ($ptb_edit) {
		                                                                                         foreach ($ptb_edit as $data) {
		                                                                                            echo $data->id_pengusaha;
		                                                                                        }
		                                                                                    }
		                                                                                            ?>">
												</div>
											</li>
										</div>	
										<div class="form-group row">
											<li>
												<label for="nama" class="col-sm-3 col-form-label">Nama</label>
												<div class="col-sm-8">
													<input type="text" class="form-control" id="nama" value = "<?php 
		                                                                                        $n = 0;
		                                                                                    if ($ptb_edit) {
		                                                                                        foreach ($ptb_edit as $data) {
		                                                                                            $n = $n + 1;
		                                                                                            echo $data->NAMA_PENGUSAHA;
		                                                                                        }
		                                                                                    }
		                                                                                            ?>">
												</div>
											</li>
										</div>

										<div class="form-group row">
											<label for="alamat" class="col-sm-3 col-form-label">Alamat</label>
											<div class="col-sm-8">
												<input type="text" class="form-control" id="alamat" value = "<?php 
		                                                                                        $n = 0;
		                                                                                    if ($ptb_edit) {
		                                                                                        foreach ($ptb_edit as $data) {
		                                                                                            $n = $n + 1;
		                                                                                            echo $data->ALAMAT_PENGUSAHA;
		                                                                                        }
		                                                                                    }
		                                                                                            ?>">
											</div>
										</div>

										<div class="form-group row">
											<label for="Negara" class="col-sm-3 col-form-label">IJIN PTB</label>
											<div class="col-sm-4">
												<input type="text" class="form-control" id="negara" value = "<?php 
		                                                                                        $n = 0;
		                                                                                    if ($ptb_edit) {
		                                                                                        foreach ($ptb_edit as $data) {
		                                                                                            $n = $n + 1;
		                                                                                            echo $data->NOMOR_IJIN_TPB;
		                                                                                        }
		                                                                                    }
		                                                                                            ?>">
											</div>
		                                                                        
										</div>
		                                                         
		                                                            
									</div>
									<!-- TUTUP FORM PEMASOK -->		
								
									<!-- FORM Penerima barang -->
									<div>
										<p style="margin-left: -38px"><b>PENGIRIM BARANG</b></p>
											
										<div class="form-group row">
											<li>
												<label for="nama" class="col-sm-3 col-form-label">NPWP</label>
												<div class="col-sm-4">
													<select id="inputState" class="form-control">
												        <option selected>NPWP</option>
												        <option>...</option>
											        </select>
												</div>
												<div class="col-sm-4">
													<input type="text" class="form-control" id="nama" value = "<?php
		                                                                                    if ($ptb_edit) {
		                                                                                         foreach ($ptb_edit as $data) {
		                                                                                            echo $data->ID_PENGIRIM;
		                                                                                        }
		                                                                                    }
		                                                                                            ?>">
												</div>
											</li>
										</div>	
										
										<div class="form-group row">
											<li>
												<label for="alamat" class="col-sm-3 col-form-label">Nama</label>
												<div class="col-sm-8">
													<input type="text" class="form-control" id="alamat" value ="<?php
		                                                                                    if ($ptb_edit) {
		                                                                                         foreach ($ptb_edit as $data) {
		                                                                                            echo $data->NAMA_PENGIRIM;
		                                                                                        }
		                                                                                    }
		                                                                                            ?>" >
												</div>
											</li>
		                                                                        
										</div>
		                                                                <div class="form-group row">
											<li>
												<label for="alamat" class="col-sm-3 col-form-label">Alamat</label>
												<div class="col-sm-8">
													<input type="text" class="form-control" id="alamat" value ="<?php
		                                                                                    if ($ptb_edit) {
		                                                                                         foreach ($ptb_edit as $data) {
		                                                                                            echo $data->ALAMAT_PENGIRIM;
		                                                                                        }
		                                                                                    }
		                                                                                            ?>" >
												</div>
											</li>
		                                                                        
										</div>
									</div>
									<!-- TUTUP FORM IMPORTIR -->

									<!-- FORM PEMILIK -->
									<div>
										<p style="margin-left: -38px"><b>DOKUMEN [F6]</b></p>
											
										<div class="form-group row">
											<li>
												<label for="nama" class="col-sm-3 col-form-label">Packing List</label>
												<div class="col-sm-4">
													<input type="text" class="form-control" id="nama" value ="<?php
		                                                                                    if ($ptb_packing) {
		                                                                                         foreach ($ptb_packing as $data) {
		                                                                                            echo $data->nomor_dokumen;
		                                                                                        }
		                                                                                    }
		                                                                                            ?>" >
												</div>
		                                                                                <div class="col-sm-4">
													<input type="text" class="form-control" id="nama" value ="<?php
		                                                                                    if ($ptb_packing) {
		                                                                                         foreach ($ptb_packing as $data) {
		                                                                                            echo $data->tanggal_dokumen;
		                                                                                        }
		                                                                                    }
		                                                                                            ?>" >
												</div>
											</li>
										</div>	
										
										<div class="form-group row">
											<li>
												<label for="nama" class="col-sm-3 col-form-label">Kontrak</label>
												<div class="col-sm-4">
													<input type="text" class="form-control" id="nama" value ="" >
												</div>
		                                                                                <div class="col-sm-4">
													<input type="text" class="form-control" id="nama" value ="" >
												</div>
											</li>
										</div>	
		                                                                <div class="form-group row">
											<li>
												<label for="nama" class="col-sm-3 col-form-label">Faktur Pajak</label>
												<div class="col-sm-4">
													<input type="text" class="form-control" id="nama" value ="" >
												</div>
		                                                                                <div class="col-sm-4">
													<input type="text" class="form-control" id="nama" value ="" >
												</div>
											</li>
										</div>	
		                                                                <div class="form-group row">
											<li>
												<label for="nama" class="col-sm-3 col-form-label">SKEP</label>
												<div class="col-sm-4">
													<input type="text" class="form-control" id="nama" value ="" >
												</div>
		                                                                                <div class="col-sm-4">
													<input type="text" class="form-control" id="nama" value ="" >
												</div>
											</li>
										</div>

										<div class="form-group row">
		                                                                    <li>
									<div class="col-sm-10">
										<table class="table table-sm">
										  <thead>
										    <tr class="bg-success">
										      <th scope="col">Jenis Dokumen</th>
										      <th scope="col">Nomor Dokumen</th>
										      <th scope="col">Tanggal</th>
										    </tr>
										  </thead>
										  <tbody>
										    
		                                                                      <?php
		                                                                        $n = 0;
		                                                                        if ($ptb_other) {
		                                                                            foreach ($ptb_other as $data) {
		                                                                                $n = $n + 1;
		                                                                                ?>
		                                                                      <td width="30%"><?php echo $data->uraian_dokumen; ?></td>
		                                                                      <td width="30%"><?php echo $data->nomor_dokumen; ?></td>
		                                                                      <td width="30%"><?php echo $data->tanggal_dokumen; ?></td></tr>
		                                                                       <?php
		                                                                            }
		                                                                        }?>						    
										   
										  </tbody>
										</table>
									</div>
		                                                                        </li>
								</div>
									</div>
									
									
									<!-- FORM HARGA -->
									
									<!-- TUTUP FORM HARGA -->	
							</div>
							<!-- TUTUP FORM LEFT SIDE -->


							<!-- FORM RIGHT SIDE -->
							<div class="col-sm-6" style="padding-right: 50px;">
								<p><b>PENGANGKUTAN</b></p>
								<div class="form-group row">
									<li>
										<label for="nama" class="col-sm-4 col-form-label">Jenis Sarana Pengangkutan Darat</label>
										<div class="col-sm-4">
													<input type="text" class="form-control" id="nama" value ="<?php
		                                                                                    if ($ptb_edit) {
		                                                                                         foreach ($ptb_edit as $data) {
		                                                                                            echo $data->NAMA_PENGANGKUT;
		                                                                                        }
		                                                                                    }
		                                                                                            ?>" >
												</div>
									</li>
								</div>
		                                                <div class="form-group row">
									<li>
										<label for="nama" class="col-sm-4 col-form-label">Nomor Polisi</label>
										<div class="col-sm-4">
													<input type="text" class="form-control" id="nama" value ="<?php
		                                                                                    if ($ptb_edit) {
		                                                                                         foreach ($ptb_edit as $data) {
		                                                                                            echo $data->NOMOR_POLISI;
		                                                                                        }
		                                                                                    }
		                                                                                            ?>" >
												</div>
									</li>
								</div>
		                                                
		                                                   <div class="form-group row">
									
										<label for="tujuan" class="col-sm-10 col-form-label">HARGA</label>
									
								</div>
		                                                 <div class="form-group row">
									<li>
										<label for="nama" class="col-sm-4 col-form-label">Harga Penyerahan</label>
										<div class="col-sm-4">
													<input type="text" class="form-control" id="nama" value ="<?php
		                                                                                    if ($ptb_edit) {
		                                                                                         foreach ($ptb_edit as $data) {
		                                                                                            echo $data->HARGA_PENYERAHAN;
		                                                                                        }
		                                                                                    }
		                                                                                            ?>" >
												</div>
									</li>
								</div>

								

								
		                                                <div class="form-group row">
									<li>
										<label for="tujuan" class="col-sm-10 col-form-label">KEMASAN [F7]</label>
									</li>
								</div>

								<div class="form-group row">
									<div class="col-sm-10">
										<table class="table table-sm">
										  <thead>
										    <tr class="bg-success">
										      <th scope="col">Jumlah</th>
										      <th scope="col">Kode Jenis</th>
										      <th scope="col">Jenis</th>
										    </tr>
										  </thead>
										  <tbody>
										     <?php
		                                                                        $n = 0;
		                                                                        if ($ptb_kemasan) {
		                                                                            foreach ($ptb_kemasan as $data) {
		                                                                                $n = $n + 1;
		                                                                                ?>
		                                                                      <td width="30%"><?php echo $data->JUMLAH_KEMASAN; ?></td>
		                                                                      <td width="30%"><?php echo $data->KODE_JENIS_KEMASAN; ?></td>
		                                                                      <td width="30%"><?php echo $data->URAIAN_KEMASAN; ?></td></tr>
		                                                                       <?php
		                                                                            }
		                                                                        }?>
		                                                                     						    
										   
										  </tbody>
										</table>
									</div>
								</div>
                                     <!-- FORM BARANG -->
                                     <?php  foreach ($ptb_edit as $data) { ?>
									<div>
										<a href="./BC40_detail_barang_controller?id=<?php echo $data->id; ?>">
											<p style="margin-left: -38px"><b>BARANG</b></p>
										</a>
									<?php } ?>
											
										<div class="form-group row">
											<li>
												<label class="col-sm-3 col-form-label">Volume (m3)</label>
												<div class="col-sm-4">
													<input type="text" class="form-control" id="nama" value ="<?php
		                                                                                    if ($ptb_edit) {
		                                                                                         foreach ($ptb_edit as $data) {
		                                                                                            echo $data->VOLUME;
		                                                                                        }
		                                                                                    }
		                                                                                            ?>" >
												</div>
		                                                                        </li>
		                                                                </div>
		                                                                <div class="form-group row">
		                                                                        <li>
												<label class="col-sm-3 col-form-label">Berat Kotor (Kg)</label>
												<div class="col-sm-4">
													<input type="text" class="form-control" id="nama" value ="<?php
		                                                                                    if ($ptb_edit) {
		                                                                                         foreach ($ptb_edit as $data) {
		                                                                                            echo $data->bruto;
		                                                                                        }
		                                                                                    }
		                                                                                            ?>" >
												</div>
		                                                                               
											</li>
		                                                                        
										</div>	
		                                                                <div class="form-group row">
		                                                                        <li>
												<label class="col-sm-3 col-form-label">Berat Bersih (Kg)</label>
												<div class="col-sm-4">
													<input type="text" class="form-control" id="nama" value ="<?php
		                                                                                    if ($ptb_edit) {
		                                                                                         foreach ($ptb_edit as $data) {
		                                                                                            echo $data->NETTO;
		                                                                                        }
		                                                                                    }
		                                                                                            ?>" >
												</div>
		                                                                               
											</li>
		                                                                        
										</div>	
		                                                                <div class="form-group row">
		                                                                       
												<label class="col-sm-3 col-form-label">Jumlah Barang</label>
												<div class="col-sm-2">
													<input type="text" class="form-control" id="nama" value ="<?php
		                                                                                    if ($ptb_edit) {
		                                                                                         foreach ($ptb_edit as $data) {
		                                                                                            echo $data->JUMLAH_BARANG;
		                                                                                        }
		                                                                                    }
		                                                                                            ?>" >
												</div>
		                                                                               
											
		                                                                        
										</div>	
		                                                        </div>
		                                                 
		                                                  <!-- FORM PUNGUTAN-->
		<!--							<div class="form-group row">
									<li>
										<label for="tujuan" class="col-sm-10 col-form-label">Pngutan</label>
									</li>
								</div>-->

										
									
									
										

										
							</ol>
		                               
		                                
		                              
		                                
						</div>		
						<!-- TUTUP FORM RIGHT SIDE -->
		                                <p>Dengan saya menyatakan bertanggung jawab atas kebenaran
		                                 <br></br>hal hal yang diberitahukan dalam pemberitahuan pabean ini. </p>
		                                <div class="form-group row">
											<!--<li>-->
												<!--<label for="Negara" class="col-sm-3 col-form-label">Pelabuhan Bongkar</label>-->
												<div class="col-sm-2">
													<input type="text" class="form-control" id="alamat" Placeholder="kota" value = "<?php
		                                                                                    if ($ptb_edit) {
		                                                                                         foreach ($ptb_edit as $data) {
		                                                                                            echo $data->KOTA_TTD;
		                                                                                        }
		                                                                                    }
		                                                                                            ?>">
												</div>
												<div class="col-sm-2">
													<input type="text" class="form-control" id="alamat" Placeholder="Tanggal" value = "<?php
		                                                                                    if ($ptb_edit) {
		                                                                                         foreach ($ptb_edit as $data) {
		                                                                                            echo $data->TANGGAL_TTD;
		                                                                                        }
		                                                                                    }
		                                                                                            ?>">
												</div>
											<!--</li>-->
										</div>
		                                <div class="form-group row">
											<!--<li>-->
												<label for="Negara" class="col-sm-2 col-form-label">Pemberitahu</label>
												<div class="col-sm-2">
													<input type="text" class="form-control" id="alamat" Placeholder="nama" value = "<?php
		                                                                                    if ($ptb_edit) {
		                                                                                         foreach ($ptb_edit as $data) {
		                                                                                            echo $data->NAMA_TTD;
		                                                                                        }
		                                                                                    }
		                                                                                            ?>">
												</div>
											<!--</li>-->
										</div>
		                                <div class="form-group row">
											<!--<li>-->
												<label for="Negara" class="col-sm-2 col-form-label">Jabatan</label>
												<div class="col-sm-2">
													<input type="text" class="form-control" id="alamat" Placeholder="jabatan" value = "<?php
		                                                                                    if ($ptb_edit) {
		                                                                                         foreach ($ptb_edit as $data) {
		                                                                                            echo $data->JABATAN_TTD;
		                                                                                        }
		                                                                                    }
		                                                                                            ?>" >
												</div>
											<!--</li>-->
										</div>
									</div>
		                                
					</div>
				</div>
			</form>
	</div>
</div>
