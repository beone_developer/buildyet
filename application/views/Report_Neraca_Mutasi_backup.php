<?php
  $tgl_awal = $_GET['tglawal'];
  $tgl_akhir = $_GET['tglakhir'];
  $coa_id_gl = $_GET['coa_id'];

  $thn_awal = substr($tgl_awal,6,4);
  $bln_awal = substr($tgl_awal,0,2);
  $day_awal = substr($tgl_awal,3,2);
  $tgl_awal_formated = $thn_awal."-".$bln_awal."-".$day_awal;

  $thn_akhir = substr($tgl_akhir,6,4);
  $bln_akhir = substr($tgl_akhir,0,2);
  $day_akhir = substr($tgl_akhir,3,2);
  $tgl_akhir_formated = $thn_akhir."-".$bln_akhir."-".$day_akhir;
?>
    <!-- BEGIN PAGE TITLE-->
          <h1>
              <center><b>Report Neraca Mutasi</b></center>
          </h1>
          <hr />

              <div class="row">
                  <div class="col-xs-12">
                      <table class="table table-striped table-hover">
                          <thead>
                              <tr>
                                  <th> COA </th>
                                  <th> Akun </th>
                                  <th> SA Debit </th>
                                  <th class="hidden-xs"> SA Kredit </th>
                                  <th class="hidden-xs"> M Debit </th>
                                  <th class="hidden-xs"> M Kredit </th>
                                  <th> S Debit </th>
                                  <th> S Kredit </th>
                              </tr>
                          </thead>

                          <tbody>
                            <?php
                            foreach($list_coa as $row){
                            ?>
                              <tr>
                                  <td> <?php echo $row['nomor'];?> </td>
                                  <td> <?php echo $row['nama'];?> </td>
                                  <td> <?php echo number_format($row['debet_idr']);?> </td>
                                  <td class="hidden-xs"> <?php echo number_format($row['kredit_idr']);?> </td>

                                  <?php
                                    //mengambil nilai total debit kredit akun
                                    $sql_total_mutasi_debet = $this->db->query("SELECT SUM(debet) FROM public.beone_gl WHERE coa_id =".intval($row['coa_id']));
                                    $hasil_total_mutasi_debet = $sql_total_mutasi_debet->row_array();

                                    $sql_total_mutasi_kredit = $this->db->query("SELECT SUM(kredit) FROM public.beone_gl WHERE coa_id =".intval($row['coa_id']));
                                    $hasil_total_mutasi_kredit = $sql_total_mutasi_kredit->row_array();
                                  ?>

                                  <td class="hidden-xs"> <?php echo number_format($hasil_total_mutasi_debet['sum']);?> </td>
                                  <td class="hidden-xs"> <?php echo number_format($hasil_total_mutasi_kredit['sum']);?> </td>
                                  <td></td>
                                  <td></td>
                              </tr>
                            <?php
                              }
                            ?>
                          </tbody>
                      </table>
                  </div>
              </div>
          </div>
