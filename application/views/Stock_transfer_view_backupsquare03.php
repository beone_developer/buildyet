<!-- BEGIN PAGE TITLE-->
<!-- END PAGE TITLE-->
<!-- END PAGE HEADER-->
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"></script>
<?php $list_item = $this->db->query("SELECT * FROM public.beone_item");?>

<?php
/******************* GENERATE NOMOR BOM ******************************/
$tgl = date('m/d/Y');
$thn = substr($tgl,8,2);
$bln = substr($tgl,0,2);


$kode_awal = "BOM"."-".$thn."-".$bln."-";

$sql = $this->db->query("SELECT * FROM public.beone_transfer_stock WHERE transfer_no LIKE '$kode_awal%' ORDER BY transfer_no DESC LIMIT 1");
$hasil = $sql->row_array();

$urutan = substr($hasil['transfer_no'],10,4);
$no_lanjutan = $urutan+1;

$digit = strlen($no_lanjutan);
$jml_nol = 4-$digit;

$cetak_nol = "";

for ($i = 1; $i <= $jml_nol; $i++) {
    $cetak_nol = $cetak_nol."0";
}

$nomor_bom = $kode_awal.$cetak_nol.$no_lanjutan;

/*************************************************/
?>

<div class="portlet box blue ">
      <div class="portlet-title">
          <div class="caption">
              <i class="fa fa-gift"></i> Bill Of Material</div>
          <div class="tools">
              <a href="" class="collapse"> </a>
              <a href="#portlet-config" data-toggle="modal" class="config"> </a>
              <a href="" class="reload"> </a>
              <a href="" class="remove"> </a>
          </div>
      </div>
      <div class="portlet-body form">




          <form role="form" method="post">
              <div class="form-body">

                <div class="row">
                  <div class="col-sm-4"><h3><b><?php echo $nomor_bom;?></b></h3></div>
                  <div class="col-sm-8">
                    <div class="form-group">
                          <label>Tanggal</label>
                          <div class="input-group">
                            <span class="input-group-addon input-circle-left">
                                <i class="fa fa-calendar"></i>
                            </span>
                            <input class="form-control form-control-inline input-medium date-picker input-circle-right" size="16" type="text" name="tanggal" value="<?php echo date('m/d/Y');?>" readonly required/>
                            <span class="help-block"></span>
                          </div>
                    </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-sm-4">
                      <div class="form-group">
                            <label>Kode Biaya</label>
                            <div class="input-group">
                               <select id="select2-single-input-sm" class="form-control input-sm select2-multiple" name="kode_biaya" required>
                                 <option value=""></option>
                                 <option value=1>PRODUKSI WIP</option>
                                 <option value=2>PRODUKSI BARANG JADI</option>
                               </select>
                         </div>
                       </div>
                    </div>

                      <div class="col-sm-8">
                        <div class="form-group">
                            <label>Keterangan</label>
                            <input type="text" class="form-control" placeholder="Keterangan" id="keterangan" name="keterangan" required>
                            <input type="hidden" class="form-control" name="transfer_no" value='<?php echo $nomor_bom;?>'>
                          </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-sm-4"><input type="hidden" class="form-control" id="cocokan_dari_item" name="cocokan_dari_item" value=0 required></div>
                      <div class="col-sm-4"><input type="hidden" class="form-control" id="cocokan_hasil_item" name="cocokan_hasil_item" value=0 required></div>
                      <div class="col-sm-4"><input type="hidden" class="form-control" id="cocokan_total_persen" name="cocokan_total_persen" value=0 required></div>
                    </div>

                    <hr / style="border-color: #3598DC;">
                    <table  id="datatable" class="table striped hovered cell-hovered">
                        <h4><b>DARI ITEM</b></h4>
                        <a class="btn blue" data-toggle="modal" href="#responsive" id="tambahdata"><i class="fa fa-plus"></i> Tambah Data </a>
            						<thead>
            							<tr>
                            <td width="40%">Item</td>
            								<td width="20%">Qty</td>
            								<td width="20%">Gudang</td>
                            <td width="10%">Action</td>
                            <td width="5%"></td>
                            <td width="5%"></td>
            							</tr>

                          </thead>
                          <tbody id="container">

             						  </tbody>


            				</table>


                    <hr / style="border-color: #3598DC;">
                    <table  id="datatable_hasil" class="table striped hovered cell-hovered">
                        <h4><b>MENJADI ITEM</b></h4>
                        <a class="btn blue" data-toggle="modal" href="#responsive_hasil" id="tambahdata_hasil"><i class="fa fa-plus"></i> Tambah Data </a>
            						<thead>
            							<tr>
                            <td width="20%">Item</td>
            								<td width="15%">Qty</td>
            								<td width="15%">Biaya</td>
                            <td width="10%">Gudang</td>
            								<td width="20%">% Hasil</td>
                            <td width="10%">Action</td>
                            <td width="5%"></td>
                            <td width="5%"></td>

            							</tr>
                         <tbody id="container_hasil">

                         </tbody>
            				</table>
              </div>
              <div class="form-actions">
                  <button type="submit" class="btn red" id="submit_transfer" name="submit_transfer">Submit</button>
                  <a class="btn green" id="cekdata" onclick="validasi_produksi();"><i class="fa fa-plus"></i> Validasi </a>
              </div>
          </form>
  </div>
</div>


<!--------------------------- MODAL ITEM ASAL--------------------------------------------->
<div id="responsive" class="modal fade" tabindex="-1" data-width="760">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Item Asal</h4>
    </div>
    <div class="modal-body">
        <div class="row">
          <div class="col-sm-4">
              <!--<div name="txtItemAsal" id="txtItemAsal"><b>Item</b></div>-->
            <div class="form-group">
                <label>Item</label>
                <select id='item_modal' class='form-control input-sm select2-multiple' name='item_modal'>
                  <option value=""></option>
                  <?php  foreach($list_item->result_array() as $row){ echo '<option value='.$row['item_id'].'>'.$row['nama'].'</option>';} ?>
                </select>
            </div>
          </div>

          <div class="col-sm-4">
            <div class="form-group">
                <label>Qty</label>
                <input type='text' class='form-control' placeholder="Qty" name='qty_modal' id='qty_modal'>
            </div>
          </div>

          <div class="col-sm-4">
            <div class="form-group">
            <label>Gudang</label>
            <select id='gudang_modal' class='form-control input-sm select2-multiple' name='gudang_modal'>
              <option value=""></option>
              <?php 	foreach($list_gudang as $row){ ?>
                <option value="<?php echo $row['gudang_id'];?>"><?php echo $row['nama'];?></option>
              <?php } ?>
            </select>
            </div>
          </div>
        </div>

    </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
            <button type="button" class="btn green" name="add_btn" id="add_btn">Insert</button>
        </div>
</div>
<!----------------------------------------------------------------------------->


<!--------------------------- MODAL ITEM HASIL --------------------------------------------->
<div id="responsive_hasil" class="modal fade" tabindex="-1" data-width="760">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Item Hasil</h4>
    </div>
    <div class="modal-body">
        <div class="row">
          <div class="col-sm-4">
            <!--<div name="txtItemHasil" id="txtItemHasil"><b>Item Hasil</b></div>-->
            <div class="form-group">
              <label>Item</label>
              <select id='item_modal_hasil' class='form-control input-sm select2-multiple' name='item_modal_hasil'>
                <option value=""><?php //echo "- Pilih Item -";?></option>
                <?php  foreach($list_item->result_array() as $row){ echo '<option value='.$row['item_id'].'>'.$row['nama'].'</option>';} ?>
              </select>
            </div>
          </div>

          <div class="col-sm-4">
            <div class="form-group">
                <label>Qty</label>
                <input type='text' class='form-control' placeholder="Qty" name='qty_modal_hasil' id='qty_modal_hasil'>
            </div>
          </div>

          <div class="col-sm-4">
            <div class="form-group">
              <label>Gudang</label>
              <select id='gudang_modal_hasil' class='form-control input-sm select2-multiple' name='gudang_modal_hasil'>
                <option value="">- Pilih Gudang -</option>
                <?php 	foreach($list_gudang as $row){ ?>
                  <option value="<?php echo $row['gudang_id'];?>"><?php echo $row['nama'];?></option>
                <?php } ?>
              </select>
            </div>
          </div>
          </div>


          <div class ="row">
            <div class="col-sm-6">
                <div class="form-group">
                <label>Biaya</label>
                <input type='text' class='form-control' placeholder="Biaya" name='biaya_modal_hasil' id='biaya_modal_hasil' required>
                </div>
            </div>

            <div class="col-sm-6">
              <div class="form-group">
                <label>% Prosentase</label>
              <input type='text' class='form-control' placeholder="% Hasil" name='persen_modal_hasil' id='persen_modal_hasil'>
              </div>
            </div>
          </div>

    </div>
    <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
        <button type="button" class="btn green" name="add_btn_hasil" id="add_btn_hasil">Insert</button>
    </div>
</div>
<!----------------------------------------------------------------------------->

<!-----------------------------ITEM ASAL----------------------------------------->
    <script>
    var yy = document.getElementById("submit_transfer");
    yy.style.display = 'none';

    $(document).ready(function() {
        		var count = 0;

        		$("#add_btn").click(function(){
    					count += 1;

              var item = document.getElementById('item_modal');
              var qty = document.getElementById('qty_modal');
              var gudang = document.getElementById('gudang_modal');
              var namaItem = $('#item_modal option:selected').text();
              var namaGudang = $('#gudang_modal option:selected').text();

              if (item.value == ""){
                alert("Silahkan isi item..!!!");
                item.focus();
                return false;
              }else if(qty.value == ""){
                alert("Silahkan isi qty..!!!");
                qty.focus();
                return false;
              }else if(gudang.value == ""){
                alert("Silahkan isi gudang..!!!");
                gudang.focus();
                return false;
              }else{

        		   		$('#container').append(
        							 '<tr class="records" id="'+count+'">'

        						 + '<td><input class="form-control" id="item_' + count + '" name="item_'+count+'" type="text" value="'+namaItem+'" readonly></td>'
        						 + '<td><input class="form-control" id="qty_' + count + '" name="qty_'+count+'" type="text" value="'+qty.value+'" readonly></td>'
                     + '<td><input class="form-control" id="gudang_' + count + '" name="gudang_'+count+'" type="text" value="'+namaGudang+'" readonly></td>'
                     + '<td><button type="button" class="btn red" onclick="hapus('+count+')">X</button></td>'
                     + '<td><input class="form-control" id="item_id_' + count + '" name="item_id_'+count+'" type="hidden" value="'+item.value+'" readonly></td>'
                     + '<td><input class="form-control" id="gudang_id_' + count + '" name="gudang_id_'+count+'" type="hidden" value="'+gudang.value+'" readonly></td>'
                     + '<td><input id="rows_' + count + '" name="rows[]" value="'+ count +'" type="hidden"></td></tr>'
        					);
                  autoCocokan_dari_item(qty.value);
                  eraseText();
                  $('#responsive').modal('hide');
            }
    				});


            var count_hasil = 0;

        		$("#add_btn_hasil").click(function(){
    					count_hasil += 1;

              var item_hasil = document.getElementById('item_modal_hasil');
              var qty_hasil = document.getElementById('qty_modal_hasil');
              var gudang_hasil = document.getElementById('gudang_modal_hasil');
              var biaya_hasil = document.getElementById('biaya_modal_hasil');
              var persen_hasil = document.getElementById('persen_modal_hasil');
              var namaItem_hasil = $('#item_modal_hasil option:selected').text();
              var namaGudang_hasil = $('#gudang_modal_hasil option:selected').text();

              if (item_hasil.value == ""){
                alert("Silahkan isi item..!!!");
                item_hasil.focus();
                return false;
              }else if(qty_hasil.value == ""){
                alert("Silahkan isi qty..!!!");
                qty_hasil.focus();
                return false;
              }else if(gudang_hasil.value == ""){
                alert("Silahkan isi gudang..!!!");
                gudang_hasil.focus();
                return false;
              }else if(biaya_hasil.value == ""){
                alert("Silahkan isi biaya..!!!");
                biaya_hasil.focus();
                return false;
              }else if(persen_hasil.value == ""){
                alert("Silahkan isi % hasil..!!!");
                persen_hasil.focus();
                return false;
              }else{

        		   		$('#container_hasil').append(
        							 '<tr class="records" id="'+count_hasil+'">'

        						 + '<td><input class="form-control" id="item_hasil_' + count_hasil + '" name="item_hasil_'+count_hasil+'" type="text" value="'+namaItem_hasil+'" readonly></td>'
        						 + '<td><input class="form-control" id="qty_hasil_' + count_hasil + '" name="qty_hasil_'+count_hasil+'" type="text" value="'+qty_hasil.value+'" readonly></td>'
                     + '<td><input class="form-control" id="biaya_hasil_' + count_hasil + '" name="biaya_hasil_'+count_hasil+'" type="text" value="'+biaya_hasil.value+'" readonly></td>'
                     + '<td><input class="form-control" id="gudang_hasil_' + count_hasil + '" name="gudang_hasil_'+count_hasil+'" type="text" value="'+namaGudang_hasil+'" readonly></td>'
                     + '<td><input class="form-control" id="persen_hasil_' + count_hasil + '" name="persen_hasil_'+count_hasil+'" type="text" value="'+persen_hasil.value+'" readonly></td>'
                     + '<td><button type="button" class="btn red" onclick="hapus_hasil('+count_hasil+')">X</button></td>'
                     + '<td><input class="form-control" id="item_hasil_id_' + count_hasil + '" name="item_hasil_id_'+count_hasil+'" type="hidden" value="'+item_hasil.value+'" readonly></td>'
                     + '<td><input class="form-control" id="gudang_hasil_id_' + count_hasil + '" name="gudang_hasil_id_'+count_hasil+'" type="hidden" value="'+gudang_hasil.value+'" readonly></td>'
                     + '<td><input id="rows_hasil_' + count_hasil + '" name="rows_hasil[]" value="'+ count_hasil +'" type="hidden"></td></tr>'
        					);

                  autoCocokan_hasil_item(qty_hasil.value);
                  autoCocokan_total_persen(persen_hasil.value);
                  eraseText_hasil();
                  $('#responsive_hasil').modal('hide');
            }
    				});


    		});

        function eraseText() {
         document.getElementById("item_modal").value = "";
         document.getElementById("qty_modal").value = "";
         document.getElementById("gudang_modal").value = "";
        }


        function hapus(rowid)
        {
            autoCocokan_dari_item_min("qty_"+rowid);

            var row = document.getElementById(rowid);
            row.parentNode.removeChild(row);
        }

        function eraseText_hasil() {
         document.getElementById("item_modal_hasil").value = "";
         document.getElementById("qty_modal_hasil").value = "";
         document.getElementById("gudang_modal_hasil").value = "";
         document.getElementById("biaya_modal_hasil").value = "";
         document.getElementById("persen_modal_hasil").value = "";
        }


        function hapus_hasil(rowid)
        {
            var qh = document.getElementById("qty_hasil_"+rowid).value;
            autoCocokan_hasil_item_min(qh);

            var ph = document.getElementById("persen_hasil_"+rowid).value;
            autoCocokan_total_persen_min(ph);

            var row = document.getElementById(rowid);
            row.parentNode.removeChild(row);

            var totalQty = document.getElementById('cocokan_hasil_item').value;
            if (totalQty == 0){
              var yy = document.getElementById("submit_transfer");
              yy.style.display = 'none';

              var zz = document.getElementById("cekdata");
              zz.style.display = 'block';
            }
        }

        function autoCocokan_dari_item(xqty){
          var totalQty = document.getElementById('cocokan_dari_item').value;
          var qty = xqty;

          var tb_ex = totalQty.split('.').join('');
          var b_ex = qty.split('.').join('');

          var tb = tb_ex.split(',').join('.');
          var b = b_ex.split(',').join('.');

          document.getElementById('cocokan_dari_item').value = (tb*1) + (b*1);
        }


        function autoCocokan_dari_item_min(xqty){
          var totalQty = document.getElementById('cocokan_dari_item').value;
          var qty = document.getElementById(xqty).value;

          var tb_ex = totalQty.split('.').join('');
          var b_ex = qty.split('.').join('');

          var tb = tb_ex.split(',').join('.');
          var b = b_ex.split(',').join('.');

          document.getElementById('cocokan_dari_item').value = (tb*1) - (b*1);
        }


        function autoCocokan_hasil_item(xqty){
          var totalQty = document.getElementById('cocokan_hasil_item').value;
          var qty = xqty;

          var tb_ex = totalQty.split('.').join('');
          var b_ex = qty.split('.').join('');

          var tb = tb_ex.split(',').join('.');
          var b = b_ex.split(',').join('.');

          document.getElementById('cocokan_hasil_item').value = (tb*1) + (b*1);
        }

        function autoCocokan_hasil_item_min(xqty){
          var totalQty = document.getElementById('cocokan_hasil_item').value;
          var qty = xqty;

          var tb_ex = totalQty.split('.').join('');
          var b_ex = qty.split('.').join('');

          var tb = tb_ex.split(',').join('.');
          var b = b_ex.split(',').join('.');

          document.getElementById('cocokan_hasil_item').value = (tb*1) - (b*1);
        }

        function autoCocokan_total_persen(xpersen){
          var totalPersen = document.getElementById('cocokan_total_persen').value;
          var persen = xpersen;

          var tb_ex = totalPersen.split('.').join('');
          var b_ex = persen.split('.').join('');

          var tb = tb_ex.split(',').join('.');
          var b = b_ex.split(',').join('.');

          document.getElementById('cocokan_total_persen').value = (tb*1) + (b*1);
        }

        function autoCocokan_total_persen_min(xpersen){
          var totalPersen = document.getElementById('cocokan_total_persen').value;
          var persen = xpersen;

          var tb_ex = totalPersen.split('.').join('');
          var b_ex = persen.split('.').join('');

          var tb = tb_ex.split(',').join('.');
          var b = b_ex.split(',').join('.');

          document.getElementById('cocokan_total_persen').value = (tb*1) - (b*1);
        }


    </script>
    <!--------------------------------- END ITEM ASAL --------------------------------------->

    <script type="text/javascript">

    function validasi_produksi(){
      var aa = document.getElementById('cocokan_dari_item').value;
      var bb = document.getElementById('cocokan_hasil_item').value;

      if (aa == 0 || bb == 0){
        alert("Isian data belum lengkap...!!!");
      }else{
        var yy = document.getElementById("submit_transfer");
        yy.style.display = 'block';

        var zz = document.getElementById("cekdata");
        zz.style.display = 'none';
      }
    }

    var qty_modal = document.getElementById('qty_modal');
      qty_modal.addEventListener('keyup', function(e){
      qty_modal.value = formatRupiah(this.value, 'Rp. ');
    });

    var qty_modal_hasil = document.getElementById('qty_modal_hasil');
      qty_modal_hasil.addEventListener('keyup', function(e){
      qty_modal_hasil.value = formatRupiah(this.value, 'Rp. ');
    });

    var biaya_modal_hasil = document.getElementById('biaya_modal_hasil');
      biaya_modal_hasil.addEventListener('keyup', function(e){
      biaya_modal_hasil.value = formatRupiah(this.value, 'Rp. ');
    });

    var persen_modal_hasil = document.getElementById('persen_modal_hasil');
      persen_modal_hasil.addEventListener('keyup', function(e){
        if (persen_modal_hasil.value >= 101){
          alert("Tidak bisa lebih dari 100%...!");
          persen_modal_hasil.value = 0;
        }
      persen_modal_hasil.value = formatRupiah(this.value, 'Rp. ');

    });

    /* Fungsi formatRupiah */
    function formatRupiah(angka, prefix){
      var number_string = angka.replace(/[^,\d]/g, '').toString(),
      split   		= number_string.split(','),
      sisa     		= split[0].length % 3,
      rupiah     		= split[0].substr(0, sisa),
      ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);

      // tambahkan titik jika yang di input sudah menjadi angka ribuan
      if(ribuan){
        separator = sisa ? '.' : '';
        rupiah += separator + ribuan.join('.');
      }

      rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
      return prefix == undefined ? rupiah : (rupiah ? rupiah : '');
    }
    </script>

    <script>
				function tampilItemAsal(str)
				{
				if (str=="")
				  {
				  document.getElementById("txtItemAsal").innerHTML="";
				  return;
				  }
				if (window.XMLHttpRequest)
				  {// code for IE7+, Firefox, Chrome, Opera, Safari
				  xmlhttp=new XMLHttpRequest();
				  }
				else
				  {// code for IE6, IE5
				  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
				  }
				xmlhttp.onreadystatechange=function()
				  {
				  if (xmlhttp.readyState==4 && xmlhttp.status==200)
					{
					document.getElementById("txtItemAsal").innerHTML=xmlhttp.responseText;
					}
				  }
				xmlhttp.open("GET","Transfer_controller/get_item_asal?q="+str,true);
				xmlhttp.send();
				}
    </script>

    <script>
        function tampilItemHasil(str)
				{
				if (str=="")
				  {
				  document.getElementById("txtItemHasil").innerHTML="";
				  return;
				  }
				if (window.XMLHttpRequest)
				  {// code for IE7+, Firefox, Chrome, Opera, Safari
				  xmlhttp=new XMLHttpRequest();
				  }
				else
				  {// code for IE6, IE5
				  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
				  }
				xmlhttp.onreadystatechange=function()
				  {
				  if (xmlhttp.readyState==4 && xmlhttp.status==200)
					{
					document.getElementById("txtItemHasil").innerHTML=xmlhttp.responseText;
					}
				  }
				xmlhttp.open("GET","Transfer_controller/get_item_hasil?z="+str,true);
				xmlhttp.send();
				}
		</script>
