<?php
  $tgl_awal = $_GET['tglawal'];
  $tgl_akhir = $_GET['tglakhir'];
  $item_id_stock = $_GET['item_id'];
  $tipe_item = $_GET['tipe_item'];

  $thn_awal = substr($tgl_awal,6,4);
  $bln_awal = substr($tgl_awal,0,2);
  $day_awal = substr($tgl_awal,3,2);
  $tgl_awal_formated = $thn_awal."-".$bln_awal."-".$day_awal;

  $thn_akhir = substr($tgl_akhir,6,4);
  $bln_akhir = substr($tgl_akhir,0,2);
  $day_akhir = substr($tgl_akhir,3,2);
  $tgl_akhir_formated = $thn_akhir."-".$bln_akhir."-".$day_akhir;

  $tgl_awal_tahun = $thn_awal."-01-"."01";
?>
          <h1>
              <?php
                if ($tipe_item == 1){
                    $nama_kartu_stock = "Bahan Baku";
                }else if ($tipe_item == 2){
                    $nama_kartu_stock = "Barang WIP";
                }else if ($tipe_item == 3){
                    $nama_kartu_stock = "Barang Jadi";
                }else{
                    $nama_kartu_stock = "";
                }
              ?>
              <center><b>Report Kartu Stock <?php echo $nama_kartu_stock;?></b></center>
          </h1>
          <hr />

              <div class="row">
                  <div class="col-xs-12">
                    <?php
                    if ($item_id_stock == 0 && $tipe_item == 0){//filter semua item atau tertentu
                        //$list_item = $this->db->query("SELECT DISTINCT v.item_id, i.nama FROM public.beone_inventory v INNER JOIN public.beone_item i ON v.item_id = i.item_id");
                        $list_item = $this->db->query("SELECT item_id, nama, item_code, saldo_qty, saldo_idr, keterangan, flag, item_type_id, hscode FROM public.beone_item ORDER BY item_type_id, nama ASC");
                    }else if ($item_id_stock == 0 && $tipe_item != 0){
                        //$list_item = $this->db->query("SELECT DISTINCT v.item_id, i.nama FROM public.beone_inventory v INNER JOIN public.beone_item i ON v.item_id = i.item_id WHERE v.item_id = $item_id_stock");
                        $list_item = $this->db->query("SELECT item_id, nama, item_code, saldo_qty, saldo_idr, keterangan, flag, item_type_id, hscode FROM public.beone_item WHERE item_type_id =".intval($tipe_item)." ORDER BY item_type_id, nama ASC");
                    }else if ($item_id_stock != 0 && $tipe_item == 0){
                        $list_item = $this->db->query("SELECT item_id, nama, item_code, saldo_qty, saldo_idr, keterangan, flag, item_type_id, hscode FROM public.beone_item WHERE item_id =".intval($item_id_stock)." ORDER BY item_type_id, nama ASC");
                    }else if ($item_id_stock != 0 && $tipe_item != 0){
                        $list_item = $this->db->query("SELECT item_id, nama, item_code, saldo_qty, saldo_idr, keterangan, flag, item_type_id, hscode FROM public.beone_item WHERE item_type_id =".intval($tipe_item)." AND item_id =".intval($item_id_stock)." ORDER BY item_type_id, nama ASC");
                    }

                		foreach($list_item->result_array() as $row){
/*************************** SALDO AWAL ITEM **************************************************************/
                      //SALDO AWAL ITEM
                      $sai = $this->db->query("SELECT * FROM public.beone_item WHERE item_id =".intval($row['item_id']));
                      $saldo_awal_item = $sai->row_array();

                      //menanggulangi error pembagian nol 0
                      if ($saldo_awal_item['saldo_idr'] == 0 OR $saldo_awal_item['saldo_qty'] == 0){
                          $saldo_awal_unit_price = 0;
                      }else{
                          $saldo_awal_unit_price = $saldo_awal_item['saldo_idr'] / $saldo_awal_item['saldo_qty'];
                      }

                      $saldo_awal_qty = $saldo_awal_item['saldo_qty'];
                      $saldo_awal_idr = $saldo_awal_item['saldo_idr'];
/***************************** END SALDO AWAL ITEM ************************************************************/
                      echo "<h4><b>".$row['nama']."</b></h4>";
                    ?>
                      <table class="table table-striped table-hover">
                          <thead>
                              <tr>
                                  <th><small> Tanggal </small></th>
                                  <th><small> Nomor </small></th>
                                  <th><small> IN QTY </small></th>
                                  <th><small> PRICE </small></th>
                                  <th><small> IN VALUE </small></th>
                                  <th><small> OUT QTY </small></th>
                                  <th><small> PRICE </small></th>
                                  <th><small> OUT VALUE </small></th>
                                  <th><small> SA QTY</small></th>
                                  <th><small> PRICE </small></th>
                                  <th><small> SA IDR</small></th>
                              </tr>
                          </thead>

                          <tbody>
                              <tr>
                                  <td><small></small></td>
                                  <td><small></small></td>
                                  <td><small></small></td>
                                  <td><small></small></td>
                                  <td><small></small></td>
                                  <td><small></small></td>
                                  <td><small></small></td>
                                  <td><small></small></td>
                                  <td><small><?php echo number_format($saldo_awal_qty,2);?></small></td>
                                  <td><small><?php echo number_format($saldo_awal_unit_price,2);?></small></td>
                                  <td><small><?php echo number_format($saldo_awal_idr,2);?></small></td>
                                </tr>

                            <?php

                                $total_qty_in = 0;
                                $total_amount_in = 0;
                                $total_qty_out = 0;
                                $total_amount_out = 0 ;
                                $total_saldo_akhir_qty = 0;
                                $total_saldo_akhir_amount = 0;


                                $mutasi_item = $this->db->query("SELECT v.intvent_trans_id, v.intvent_trans_no, v.item_id, i.nama, v.trans_date, v.keterangan, v.qty_in, v.value_in, v.qty_out, v.value_out, v.sa_unit_price, v.sa_amount,v.flag, v.update_by, v.update_date
                                                                	FROM public.beone_inventory v INNER JOIN public.beone_item i ON v.item_id = i.item_id
                                                                  WHERE trans_date BETWEEN '$tgl_awal_formated' AND '$tgl_akhir_formated' AND v.item_id = $row[item_id] ORDER BY v.trans_date ASC");
                                $qty_sebelumnya = 0;
                                $unit_price_sebelumnya = 0;
                                $amount_sebelumnya = 0;

                                $counter_item = 0;
                                foreach($mutasi_item->result_array() as $row_mutasi){

                                        $mutasi_qty_in  = $row_mutasi['qty_in'];
                                        $unit_price_in = $row_mutasi['value_in'];
                                        $mutasi_amount_in = $mutasi_qty_in * $unit_price_in;
                                        $mutasi_qty_out = $row_mutasi['qty_out'];
                                        $unit_price_out = $row_mutasi['value_out'];
                                        $mutasi_amount_out = $mutasi_qty_out * $unit_price_out;

                                        if ($counter_item == 0){//item belum pernah ditransaksikan, maka ambil dari saldo awal
                                              $mutasi_saldo_akhir_qty = ($saldo_awal_qty + $mutasi_qty_in) - $mutasi_qty_out;
                                        }else{//item sudah pernah ditransaksikan, maka ambil dari mutasi terakhir
                                              $mutasi_saldo_akhir_qty = ($qty_sebelumnya + $mutasi_qty_in) - $mutasi_qty_out;
                                        }

                                        $mutasi_unit_price = $row_mutasi['sa_unit_price'];
                                        $mutasi_saldo_akhir_amount = $row_mutasi['sa_amount'];

                              ?><tr>
                                  <td><small><?php echo $row_mutasi['trans_date'];?></small></td>
                                  <td><small><?php echo $row_mutasi['intvent_trans_no'];?></small></td>
                                  <td><small><?php echo number_format($mutasi_qty_in ,2);?></small></td>
                                  <td><small><?php echo number_format($unit_price_in,2);?></small></td>
                                  <td><small><?php echo number_format($mutasi_amount_in,2);?></small></td>
                                  <td><small><?php echo number_format($mutasi_qty_out,2);?></small></td>
                                  <td><small><?php echo number_format($unit_price_out,2);?></small></td>
                                  <td><small><?php echo number_format($mutasi_amount_out,2);?></small></td>
                                  <td><small><?php echo number_format($mutasi_saldo_akhir_qty,2);?></small></td>
                                  <td><small><?php echo number_format($mutasi_unit_price,2);?></small></td>
                                  <td><small><?php echo number_format($mutasi_saldo_akhir_amount,2);?></small></td>
                                </tr>
                              <?php
                                    $qty_sebelumnya = $mutasi_saldo_akhir_qty;
                                    $unit_price_sebelumnya = $mutasi_unit_price;
                                    $amount_sebelumnya = $mutasi_saldo_akhir_amount;

                                    $total_qty_in = $total_qty_in + $mutasi_qty_in;
                                    $total_amount_in = $total_amount_in + $mutasi_amount_in;
                                    $total_qty_out = $total_qty_out + $mutasi_qty_out;
                                    $total_amount_out = $total_amount_out + $mutasi_amount_out;
                                    //$total_saldo_akhir_qty = $total_saldo_akhir_qty + $mutasi_saldo_akhir_qty;
                                    //$total_saldo_akhir_amount = $total_saldo_akhir_amount + $mutasi_saldo_akhir_amount;
                                    $total_saldo_akhir_qty = ($saldo_awal_qty + $total_qty_in) - $total_qty_out;
                                    $total_saldo_akhir_amount = ($saldo_awal_idr + $total_amount_in) - $total_amount_out;

                                    $counter_item = $counter_item + 1;
                                }

                              ?>
                                <td><small></small></td>
                                <td><small></small></td>
                                <td><b><small><?php echo number_format($total_qty_in,2);?></small></b></td>
                                <td><small></small></td>
                                <td><b><small><?php echo number_format($total_amount_in,2);?></small></b></td>
                                <td><b><small><?php echo number_format($total_qty_out,2);?></small></b></td>
                                <td><small></small></td>
                                <td><b><small><?php echo number_format($total_amount_out,2);?></small></b></td>
                                <td><b><small><?php echo number_format($total_saldo_akhir_qty,2);?></small></b></td>
                                <td><small></small></td>
                                <td><b><small><?php echo number_format($total_saldo_akhir_amount,2);?></small></b></td>

                          </tbody>
                      </table>
                      <br />
                      <?php
                        }
                        ?>
                  </div>
              </div>


          </div>
