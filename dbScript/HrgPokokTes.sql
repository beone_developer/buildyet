DROP TABLE IF EXISTS temp_trans;

SELECT beh.export_header_id as trans_id,
       beh.delivery_no      as trans_no,
       beh.delivery_date    as trans_date,
       'EXPORT'             as keterangan,
       bed.gudang_id        as gudang_id,
       bed.item_id          as item_id,
       bed.qty * -1         as qty,
       bed.price            as price
INTO TEMPORARY temp_trans
FROM beone_export_header beh
         INNER JOIN beone_export_detail bed
                    on beh.export_header_id = bed.export_header_id
WHERE coalesce(beh.delivery_no, '') <> ''
UNION ALL
SELECT beh.import_header_id,
       beh.receive_no,
       beh.receive_date,
       'IMPORT' as keterangan,
       bed.gudang_id,
       bed.item_id,
       bed.qty,
       bed.price
FROM beone_import_header beh
         INNER JOIN beone_import_detail bed
                    on beh.import_header_id = bed.import_header_id
WHERE coalesce(beh.receive_no, '') <> ''
UNION ALL
SELECT beh.konversi_stok_header_id,
       beh.konversi_stok_no,
       beh.konversi_stok_date,
       'KONVERSI STOK' as keterangan,
       bed.gudang_id,
       bed.item_id,
       bed.qty_real * -1,
       0
FROM beone_konversi_stok_header beh
         INNER JOIN beone_konversi_stok_detail bed
                    on beh.konversi_stok_header_id = bed.konversi_stok_header_id
UNION ALL
SELECT beh.konversi_stok_header_id,
       beh.konversi_stok_no,
       beh.konversi_stok_date,
       'KONVERSI STOK IN' as keterangan,
       beh.gudang_id,
       beh.item_id,
       beh.qty_real,
       0
FROM beone_konversi_stok_header beh
UNION ALL
SELECT beh.transfer_stock_id,
       beh.transfer_no,
       beh.transfer_date,
       'TRANSFER STOCK'                                                                       as keterangan,
       bed.gudang_id,
       bed.item_id,
       case (bed.tipe_transfer_stock) when 'BB' then bed.qty * -1 when 'WIP' then bed.qty end as qty,
       0
FROM beone_transfer_stock beh
         INNER JOIN beone_transfer_stock_detail bed
                    on beh.transfer_stock_id = bed.transfer_stock_header_id
ORDER BY trans_date asc;

SELECT *
FROM temp_trans;
