-- CREATE TRIGGER beone_transfer_stock_detail ON CREATE
CREATE OR REPLACE FUNCTION trg_create_beone_transfer_stock_detail() RETURNS trigger AS
$$
DECLARE
    v_trans_id   int;
    v_trans_date date;
    v_trans_no   varchar(200);
    v_keterangan varchar(200);
BEGIN
    select transfer_stock_id, transfer_no, transfer_date, 'TRANSFER STOCK' as keterangan
    into v_trans_id, v_trans_no, v_trans_date, v_keterangan
    from beone_transfer_stock
    where transfer_stock_id = NEW.transfer_stock_header_id;

    if (coalesce(v_trans_no, '') != '') then
        call set_stok(NEW.item_id, NEW.gudang_id, v_trans_date, v_trans_no, v_keterangan, 0, NEW.qty, 0);
    end if;

    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS trg_create_beone_transfer_stock_detail on beone_transfer_stock_detail;

CREATE TRIGGER trg_create_beone_transfer_stock_detail
    BEFORE INSERT OR UPDATE
    ON beone_transfer_stock_detail
    FOR EACH ROW
EXECUTE PROCEDURE trg_create_beone_transfer_stock_detail();

-- CREATE TRIGGER beone_transfer_stock_detail ON DELETE
CREATE OR REPLACE FUNCTION trg_delete_beone_transfer_stock_detail() RETURNS trigger AS
$$
DECLARE
    v_trans_id   int;
    v_trans_date date;
    v_trans_no   varchar(200);
    v_keterangan varchar(200);
BEGIN
    select transfer_stock_id, transfer_no, transfer_date, 'TRANSFER STOCK' as keterangan
    into v_trans_id, v_trans_no, v_trans_date, v_keterangan
    from beone_transfer_stock
    where transfer_stock_id = OLD.transfer_stock_header_id;

    if (coalesce(v_trans_no, '') != '') then
        call set_stok(OLD.item_id, OLD.gudang_id, v_trans_date, v_trans_no, v_keterangan, 0, 0, 0);
    end if;

    RETURN OLD;
END;
$$ LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS trg_delete_beone_transfer_stock_detail on beone_transfer_stock_detail;

CREATE TRIGGER trg_delete_beone_transfer_stock_detail
    BEFORE DELETE
    ON beone_transfer_stock_detail
    FOR EACH ROW
EXECUTE PROCEDURE trg_delete_beone_transfer_stock_detail();
