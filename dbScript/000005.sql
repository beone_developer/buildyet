ALTER TABLE beone_konversi_stok_header
    ADD COLUMN IF NOT EXISTS gudang_id INTEGER NOT NULL DEFAULT 1;
ALTER TABLE beone_konversi_stok_header
    ADD COLUMN IF NOT EXISTS qty_real INTEGER NOT NULL DEFAULT 0;
ALTER TABLE beone_konversi_stok_detail
    ADD COLUMN IF NOT EXISTS qty_allocation INTEGER NOT NULL DEFAULT 0;

-- CREATE TRIGGER beone_konversi_stok_header ON CREATE
CREATE OR REPLACE FUNCTION trg_create_beone_konversi_stok_header() RETURNS trigger AS
$$
DECLARE
    v_trans_id   int;
    v_trans_date date;
    v_trans_no   varchar(200);
    v_keterangan varchar(200);
BEGIN
    call set_stok(NEW.item_id, NEW.gudang_id, v_trans_date, v_trans_no, v_keterangan, NEW.qty_real, 0);
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

-- CREATE TRIGGER beone_konversi_stok_detail ON DELETE
CREATE OR REPLACE FUNCTION trg_delete_beone_konversi_stok_header() RETURNS trigger AS
$$
DECLARE
    v_trans_id   int;
    v_trans_date date;
    v_trans_no   varchar(200);
    v_keterangan varchar(200);
BEGIN
    call set_stok(OLD.item_id, OLD.gudang_id, v_trans_date, v_trans_no, v_keterangan, 0, 0);
    RETURN OLD;
END;
$$ LANGUAGE plpgsql;
