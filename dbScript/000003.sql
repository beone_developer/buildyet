CREATE TABLE IF NOT EXISTS public.beone_komposisi (
   komposisi_id SERIAL PRIMARY KEY,
   item_jadi_id INTEGER,
   item_baku_id INTEGER,
   qty_item_baku FLOAT,
   flag INTEGER
);

CREATE TABLE IF NOT EXISTS public.beone_konversi_stok_header (
   konversi_stok_header_id SERIAL PRIMARY KEY,
   item_id INTEGER,
   konversi_stok_no VARCHAR(255),
   konversi_stok_date DATE,
   flag INTEGER,
   qty FLOAT,
   satuan_qty INTEGER
);

CREATE TABLE IF NOT EXISTS public.beone_konversi_stok_detail (
   konversi_stok_detail_id SERIAL PRIMARY KEY,
   konversi_stok_header_id INTEGER,
   item_id INTEGER,
   qty FLOAT,
   qty_real FLOAT,
   satuan_qty INTEGER
);